import { AUTHENTICATE, DEAUTHENTICATE, SET_USER, SET_ENDPOINT } from '../actionTypes';

const authReducer = (state = { token: null, user: null, endpoint: null }, action) => {
  switch (action.type) {
  case AUTHENTICATE:
    return { ...state, token: action.payload };
  case SET_ENDPOINT:
    return { ...state, endpoint: action.payload };
  case DEAUTHENTICATE:
    return { ...state, token: null, user: null };
  case SET_USER:
    return { ...state, user: action.payload };
  default:
    return state;
  }
};

export default authReducer;
