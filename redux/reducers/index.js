import { combineReducers } from 'redux'
import authReducer from './authReducers'
import lakkonReducer from './lakkonReducers'

const rootReducer = combineReducers({
  authentication: authReducer,
  lakkonState: lakkonReducer
});

export default rootReducer
