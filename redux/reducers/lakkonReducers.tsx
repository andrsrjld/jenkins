import { LAKKON_AUTHENTICATE, LAKKON_DEAUTHENTICATE, LAKKON_SET_USER } from '../actionTypes';

const lakkonReducer = (state = { token: null, user: null }, action) => {
  switch (action.type) {
    case LAKKON_AUTHENTICATE:
      return { ...state, token: action.payload };
    case LAKKON_DEAUTHENTICATE:
      return { ...state, token: null, user: null };
    case LAKKON_SET_USER:
      return { ...state, user: action.payload };
    default:
      return state;
  }
};

export default lakkonReducer;
