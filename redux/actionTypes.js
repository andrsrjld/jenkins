export const AUTHENTICATE = 'AUTHENTICATE';
export const DEAUTHENTICATE = 'DEAUTHENTICATE';
export const SET_USER = 'SET_USER';
export const SET_ENDPOINT = 'SET_ENDPOINT';

export const LAKKON_AUTHENTICATE = 'LAKKON_AUTHENTICATE';
export const LAKKON_DEAUTHENTICATE = 'LAKKON_DEAUTHENTICATE';
export const LAKKON_SET_USER = 'LAKKON_SET_USER';
