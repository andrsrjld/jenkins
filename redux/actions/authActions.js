import Router from 'next/router';
import fetch from 'isomorphic-unfetch';
import { AUTHENTICATE, DEAUTHENTICATE, SET_USER, SET_ENDPOINT,
LAKKON_AUTHENTICATE, LAKKON_DEAUTHENTICATE, LAKKON_SET_USER } from '../actionTypes';
import { login, loginLakkon, getProfile } from '~/util/ServiceHelper';
import { setCookie, removeCookie, getCookie } from '~/util/helper';

export const authenticate = (user) => async (dispatch) => {
  const res = await login(user);
  if (res.code === 400 || res.code === 500) {
    return res;
  }
  // if(!res.data.is_verified) {
  //   return res
  // }
  const { token } = res.data;
  localStorage.setItem('user', JSON.stringify(res.data));
  setCookie('token', token);
  dispatch({ type: AUTHENTICATE, payload: token });
  dispatch({ type: SET_USER, payload: res.data });
  return res;
};

export const authenticateLakkon = (user) => async (dispatch) => {
  const res = await loginLakkon(user);
  if (res.code === 400 || res.code === 500) {
    return res;
  }
  const { token } = res.data;
  localStorage.setItem('lakkon_user', JSON.stringify(res.data));
  setCookie('lakkon_token', token);
  dispatch({ type: LAKKON_AUTHENTICATE, payload: token });
  dispatch({ type: LAKKON_SET_USER, payload: res.data });
  return res;
};

export const socialAuthenticate = (user) => async (dispatch) => {
  localStorage.setItem('user', JSON.stringify(user));
  setCookie('token', user.token);
  dispatch({ type: AUTHENTICATE, payload: user.token });
  dispatch({ type: SET_USER, payload: user });
  Router.push('/')
}

export const reauthenticate = (token) => async (dispatch) => {
  dispatch({ type: AUTHENTICATE, payload: token });
};
export const reauthenticateLakkon = (token) => async (dispatch) => {
  dispatch({ type: LAKKON_AUTHENTICATE, payload: token });
};

export const reauthenticateEndpoint = () => async (dispatch) => {
  dispatch({type: SET_ENDPOINT, payload: {
    cloudinaryUrl: process.env.CLOUDINARY_URL,
    apiUrl: process.env.API_URL
  }});
};

// removing the token
export const deauthenticate = () => (dispatch) => {
  removeCookie('token');
  Router.push('/');
  dispatch({ type: DEAUTHENTICATE });
};
export const deauthenticateLakkon = () => (dispatch) => {
  removeCookie('lakkon_token');
  Router.push('/');
  dispatch({ type:LAKKON_DEAUTHENTICATE });
};


const privateRoutes = [
  '/transaction-history'
]
export const checkServerSideCookie = async (ctx) => {
  if (ctx.isServer) {
    await ctx.store.dispatch(await reauthenticateEndpoint());
    if (ctx.req.headers.cookie) {
      const token = getCookie('token', ctx.req);
      const tokenLakkon = getCookie('lakkon_token', ctx.req);

      if (token && (ctx.pathname === '/signin' || ctx.pathname === '/register')) {
        /*
        * If `ctx.req` is available it means we are on the server.
        * Additionally if there's no token it means the user is not logged in.
        */
        
        if (ctx.req && token) {
          ctx.res.writeHead(302, { Location: '/' });
          ctx.res.end();
        } else if(privateRoutes.includes(ctx.pathname)) {
          ctx.res.writeHead(302, { Location: '/signin' });
          ctx.res.end();
        }
      }

      await ctx.store.dispatch(await reauthenticateLakkon(tokenLakkon));
      await ctx.store.dispatch(await reauthenticate(token));
    }
  } else {
    const { token } = ctx.store.getState().authentication;
    const tokenLakkon = ctx.store.getState().lakkonState.token;

    if (token && (ctx.pathname === '/signin' || ctx.pathname === '/register')) {
      /*
      * If `ctx.req` is available it means we are on the server.
      * Additionally if there's no token it means the user is not logged in.
      */
      if (ctx.req && token) {
        ctx.res.writeHead(302, { Location: '/' });
        ctx.res.end();
      } else if(privateRoutes.includes(ctx.pathname)) {
        ctx.res.writeHead(302, { Location: '/signin' });
        ctx.res.end();
      }
    }
  }
};

export const setUpUser = (user) => (dispatch) => {
  dispatch({ type: SET_USER, payload: user });
};

export const register = (data) => (dispatch) => fetch(`${API_URL_v2}auth/register`, {
  method: 'POST',
  mode: 'cors',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify(data)
})
  .then((data) => data.json())
  .then((response) => {
    console.log('register response', response);
    if (response.meta.code === 200) {
      return response;
    }
    throw response;
  })
  .catch((err) => {
    console.log('register error', err);
    throw err;
  });
