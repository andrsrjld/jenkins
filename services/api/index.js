import axios from 'axios';

const https = require("https");

const axiosSetup = (token) => {
    let headers;
    if(token){
        headers = {
            "Authorization": "Bearer " + token
        }
    }
    
    const api = axios.create({
        baseURL: 'http://156.0.100.89:5050/api/v1',
        timeout: 3000,
        headers: headers,
        // httpsAgent: new https.Agent({  
        // rejectUnauthorized: false
        // })
    });

    return api
}


export default axiosSetup;