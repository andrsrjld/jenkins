import nodemailer from 'nodemailer';
import { google } from 'googleapis';

const GMAIL_ADDRESS = "admin@lakkon.id"
const GMAIL_OAUTH_CLIENT_ID = "389007976132-dlscnnlnbi474rp55g90d9400tm5rsga.apps.googleusercontent.com"
const GMAIL_OAUTH_PROJECT_ID = "phrasal-bank-282007"
const GMAIL_OAUTH_CLIENT_SECRET = "53NSoNG_Qq63alS4ihe15PRr"
const GMAIL_OAUTH_REDIRECT_URL = "http://staging.lakkon.id:333"

const code = "4/1gGZmb0Wl5wli_dssI6Yg8W_CXZ6B0jNvzFikKPW8CPIxg84fgrx68qAU8iJbOQqH5cFgtAo1n3gZladuIwS1Dw";

const GMAIL_OAUTH_REFRESH_TOKEN = "ya29.a0AfH6SMDNfsV4pTdNr3ojYG-a-8DSMUcOTsZsX-ZeVIQD4KPd2bxI_TBt9wfyL7BwDYFgs2Da6e8IdTPA59IjXqK9OT9rXHUmi4rLqrDsvKJsB-Ggtu5Ld50dbhV13Cc6NTVJvslx-c4tXwJrN1XOFI8Xffe60lcDI8o"
const GMAIL_OAUTH_ACCESS_TOKEN = "1//0gi5hqQjHsCh4CgYIARAAGBASNwF-L9IrzB4Rv2FVfs_d5rg1PfRVVoA9CjhxxLmOKrF3ieH6oivISlDSBOrUmgFsdlsiOnN2TIw"
const GMAIL_OAUTH_TOKEN_EXPIRE = 1594205409865

const oauth2Client = new google.auth.OAuth2(
    GMAIL_OAUTH_CLIENT_ID,
    GMAIL_OAUTH_CLIENT_SECRET,
    GMAIL_OAUTH_REDIRECT_URL,
);

// Generate a url that asks permissions for Gmail scopes

const GMAIL_SCOPES = [
    'https://mail.google.com/',
    'https://www.googleapis.com/auth/gmail.modify',
    'https://www.googleapis.com/auth/gmail.compose',
    'https://www.googleapis.com/auth/gmail.send',
];

	
const url = oauth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: GMAIL_SCOPES,
    prompt: 'consent'
});

// const getToken = async () => {
//     const { tokens } = await oauth2Client.getToken(code);
//     console.log(tokens);
// };

// export default getToken;

// const testAuth = () => {
//     console.info(`authUrl: ${url}`);
// }

// export default testAuth;


const transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 465,
    secure: true,
    auth:{
        type: 'OAuth2',
        user: GMAIL_ADDRESS,
        clientId: GMAIL_OAUTH_CLIENT_ID,
        clientSecret: GMAIL_OAUTH_CLIENT_SECRET,
        refreshToken: GMAIL_OAUTH_REFRESH_TOKEN,
        accessToken: GMAIL_OAUTH_ACCESS_TOKEN,
        expires: Number.parseInt(GMAIL_OAUTH_TOKEN_EXPIRE, 10),
    }
})

export const sendEmail = mailOptions => new Promise((resolve, reject) => {
    try{
        transporter.sendMail(mailOptions, (error, info) => {	
            if (error) {	
              console.log(error.stack || error);	
              return resolve(error);
            }
            return resolve(info);
          });
    } catch(error){
        return resolve(error);
    }
});