import css from 'styled-jsx/css';

export default css.global`

@font-face {
  font-family: 'Rubik';
  src: url('/fonts/Rubik-Regular.ttf')  format('truetype');
  font-weight: 500;
  font-style: normal;
}

@font-face {
  font-family: 'Rubik';
  src: url('/fonts/Rubik-Bold.ttf')  format('truetype');
  font-weight: 700;
  font-style: normal;
}

/* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* Firefox */
input[type=number] {
  -moz-appearance: textfield;
}

input {
  font-family: 'Rubik', sans-serif !important;
}

textarea:focus, input:focus{
  outline: none;
}

html,
body {
  padding: 0;
  margin: 0;
  font-family: 'Rubik', sans-serif !important;
}

* {
  box-sizing: border-box;
}

::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
  color: #868686;
  opacity: 1; /* Firefox */
}

:-ms-input-placeholder { /* Internet Explorer 10-11 */
  color: #868686;
}

::-ms-input-placeholder { /* Microsoft Edge */
  color: #868686;
}

.w-100 {
  width: 100%;
}

.text-center {
  text-align: center;
}

.p-3{
  padding: 3rem;
}

.pointer {
  cursor: pointer;
}

.text-primary-temp {
  font-weight: bold;
  font-size: 26px;
  line-height: 25px;
  letter-spacing: 0.04em;
  color: #730C07;
}

.text-error {
  font-size: 14px;
  color: #010101;
  font-weight: 300;
}

.text-secondary {
  font-family: Gotham;
  font-size: 14px;
  line-height: 13px;
  color: #AFAFAF;
}

.form-control {
  background: #FFFFFF;
  box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.25);
  border-radius: 7px;
  padding: 15px 18px;
  font-size: 16px;
  line-height: 19px;
  color: #000000;
  border: 0px;
  height: 100%;
}

.form-control:disabled {
  background: #DADADA;
}

.form-helper {
  margin-top: 10px;
  font-size: 16px;
  line-height: 15px;
  color: #BDBDBD;
}

.btn-primary {
  background: #010101;
  box-shadow: 0px 7px 6px rgba(0, 0, 0, 0.17);
  border-radius: 7px;
  width: 100%;
  padding: 15px 20px;
  font-weight: 500;
  font-size: 18px;
  line-height: 26px;
  color: #FFFFFF;
  border: 0px;
  cursor: pointer;
  transition: background 0.8s;
  background-position: center;
}

.btn-primary:disabled {
  background: #AFAFAF;
  cursor: default;
  pointer-events: none;
}

.btn-primary:hover {
  background: #730C07 radial-gradient(circle, transparent 1%, #730C07 1%) center/15000%;
}

.btn-primary:active {
  background-color: #010101;
  background-size: 100%;
  transition: background 0s;
}

.btn-secondary {
  background: #848484;
  box-shadow: 0px 7px 6px rgba(0, 0, 0, 0.17);
  border-radius: 7px;
  width: 100%;
  padding: 1.2rem;
  font-weight: 500;
  font-size: 22px;
  line-height: 26px;
  color: #FFFFFF;
  border: 0px;
  cursor: pointer;
  transition: background 0.8s;
  background-position: center;
}

.btn-clean {
  background: #FFFFFF;
  border: 0.5px solid #8A8A8A;
  box-sizing: border-box;
  border-radius: 7px;
  font-family: Gotham;
  font-size: 20px;
  line-height: 19px;
  color: #484848;
  cursor: pointer;
}

.breadcumb {
  font-size: 14px;
  color: #696969;
}

.card-modal {
  background: #FFFFFF;
  box-shadow: 0px 1px 12px rgba(0, 0, 0, 0.25);
  border-radius: 10px;
}

.card-modal .title{
  font-weight: 500;
  font-size: 24px;
  line-height: 23px;
  text-align: center;
  letter-spacing: 0.04em;
  color: #696969;
}

.card-modal divider {
  height: 1px;
  width: 100%;
  background: #C2C2C2;
}

.table {
  width: 100%;
  border-collapse: collapse;
}

.th {
  font-weight: 500;
  font-size: 18px;
  line-height: 17px;
  letter-spacing: 0.05em;
  color: #696969;
  margin: 0px;
  padding: 8px;
  border-bottom: 1px solid #CCCCCC;
}

.td {
  padding: 24px 16px;
  font-size: 16px;
  line-height: 15px;
  letter-spacing: 0.05em;
  color: #696969;
  
}

/* The container */
.c-radio {
  display: block;
  position: relative;
  padding-left: 35px;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 22px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default radio button */
.c-radio input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
}

/* Create a custom radio button */
.checkmark {
  position: absolute;
  top: 0;
  left: 0;
  height: 25px;
  width: 25px;
  background-color: #eee;
  border-radius: 50%;
}

/* On mouse-over, add a grey background color */
.c-radio:hover input ~ .checkmark {
  background-color: #ccc;
}

/* When the radio button is checked, add a blue background */
.c-radio input:checked ~ .checkmark {
  background-color: #ccc;
}

/* Create the indicator (the dot/circle - hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the indicator (dot/circle) when checked */
.c-radio input:checked ~ .checkmark:after {
  display: block;
}

/* Style the indicator (dot/circle) */
.c-radio .checkmark:after {
 	top: 3px;
	left: 3px;
	width: 19px;
	height: 19px;
	border-radius: 50%;
	background: #010101;
}

.pac-container {
  z-index: 16000 !important;
}

`;
