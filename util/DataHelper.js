export const dummySlidersData = () => (
  [
    {
      image: '/images/dummy_slider.png',
      header: 'LAKKON <br> TRENDING 2020',
      description: 'Discount 20% Off Untung Anggota Lakkon',
      action: 'Jelajah Sekarang',
      link: 'https://lakkon.id'
    },
    {
      image: '/images/dummy_slider.png',
      header: 'INI HEADER',
      description: 'INI DESCRIPTION',
      action: 'INI ACTION',
      link: 'https://lakkon.id'
    }
  ]
);

export const dummySaleData = () => (
  [
    {
      image: '/images/dummy-sale.png',
      title: 'Big Sale Product',
      description: ' Berbagai Penawaran Terbaik'
    },
    {
      image: '/images/dummy-sale2.png',
      title: 'Big Sale Product',
      description: ' Berbagai Penawaran Terbaik'
    },
    {
      image: '/images/dummy-sale3.png',
      title: 'Big Sale Product',
      description: ' Berbagai Penawaran Terbaik'
    },
    {
      image: '/images/dummy-sale4.png',
      title: 'Big Sale Product',
      description: ' Berbagai Penawaran Terbaik'
    }
  ]
);

export const dummyTodayDealsData = () => (
  [
    {
      discount: 6,
      image: '/images/product1.png',
      productName: 'Flores Colol Coffee Beans Terbaru',
      merchant: 'ANOMALI COFFEE',
      rating: 4,
      comments: 7,
      price: 150000,
      priceSelling: 130000
    },
    {
      discount: 6,
      image: '/images/product2.png',
      productName: 'Bokasso #3',
      merchant: 'TITIK TEMU ROASTERY',
      rating: 4,
      comments: 7,
      price: 150000,
      priceSelling: 130000
    },
    {
      discount: 6,
      image: '/images/product3.png',
      productName: 'Ciwidey West Java Frinsa',
      merchant: 'REIROM COFFEE SOLUTION',
      rating: 4,
      comments: 7,
      price: 150000,
      priceSelling: 130000
    },
    {
      discount: 6,
      image: '/images/product4.png',
      productName: 'Espresso Blend - Kungfu Kicks',
      merchant: 'G COFFEE ROASTERY',
      rating: 4,
      comments: 7,
      price: 150000,
      priceSelling: 130000
    }
  ]
);

export const dummyPelakkonData = () => (
  [
    {
      image: '/images/pelakkon1.png'
    },
    {
      image: '/images/pelakkon2.png'
    },
    {
      image: '/images/pelakkon3.png'
    },
    {
      image: '/images/pelakkon4.png'
    }
  ]
);

export const dummyCustomerSaysData = () => ([
  {
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tortor tortor, dapibus non luctus ut, efficitur a tortor. Proin diam diam, dignissim nec leo sit amet, eleifend egestas lorem.',
    image: '/images/dummy_customer.png',
    name: 'MAYDINA SOEN / CEO OF CSC'
  },
  {
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tortor tortor, dapibus non luctus ut, efficitur a tortor. Proin diam diam, dignissim nec leo sit amet, eleifend egestas lorem.',
    image: '/images/dummy_customer.png',
    name: 'MAYDINA SOEN / CEO OF CSC'
  }
]);

export const dummyArticlesData = () => ([
  {
    image: '/images/dummy_article1.png',
    title: 'Teknik Kopi Terbaik',
    date: '09 July 2020',
    author: 'admin',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tortor tortor, dapibus non luctus ut, efficitur a tortor. Proin diam diam, dignissim nec leo sit amet, eleifend egestas lorem.',
    likes: '7'
  },
  {
    image: '/images/dummy_article2.png',
    title: 'Teknik Kopi Terbaik',
    date: '09 July 2020',
    author: 'admin',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tortor tortor, dapibus non luctus ut, efficitur a tortor. Proin diam diam, dignissim nec leo sit amet, eleifend egestas lorem.',
    likes: '7'
  },
  {
    image: '/images/dummy_article3.png',
    title: 'Teknik Kopi Terbaik',
    date: '09 July 2020',
    author: 'admin',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tortor tortor, dapibus non luctus ut, efficitur a tortor. Proin diam diam, dignissim nec leo sit amet, eleifend egestas lorem.',
    likes: '7'
  }
]);

export const dummyInstagramData = () => ([
  {
    image: '/images/dummy_instagram1.png'
  },
  {
    image: '/images/dummy_instagram2.png'
  },
  {
    image: '/images/dummy_instagram3.png'
  },
  {
    image: '/images/dummy_instagram4.png'
  }
]);


export const BankList = () => ([
  {
    label: 'BTPN',
    value: 'BTPN'
  },
  {
    label: 'BTPN Wow!',
    value: 'BTPN Wow!'
  },
  {
    label: 'BTPN Syariah',
    value: 'BTPN Syariah'
  },
  {
    label: 'BCA',
    value: 'BCA'
  },
  {
    label: 'Mandiri',
    value: 'Mandiri'
  },
  {
    label: 'Mandiri',
    value: 'Mandiri'
  },
  {
    label: 'CIMB Niaga',
    value: 'CIMB Niaga'
  },
  {
    label: 'BNI',
    value: 'BNI'
  },
  {
    label: 'BRI',
    value: 'BRI'
  },
  {
    label: 'Permata',
    value: 'Permata'
  },
  {
    label: 'Danamon',
    value: 'Danamon'
  },
  {
    label: 'Commonwealth Bank',
    value: 'Commonwealth Bank'
  },
  {
    label: 'BTN',
    value: 'BTN'
  },
  {
    label: 'Bank Mega',
    value: 'Bank Mega'
  },
  {
    label: 'ANZ Indonesia',
    value: 'ANZ Indonesia'
  },
  {
    label: 'ATMB Plus',
    value: 'ATMB Plus'
  },
  {
    label: 'Bank Agris',
    value: 'Bank Agris'
  },
  {
    label: 'Bank Amar',
    value: 'Bank Amar'
  },
  {
    label: 'Bank Anda',
    value: 'Bank Anda'
  },
  {
    label: 'Bank Andara',
    value: 'Bank Andara'
  },
  {
    label: 'Bank Artha Graha',
    value: 'Bank Artha Graha'
  },
  {
    label: 'Bank Artos',
    value: 'Bank Artos'
  },
  {
    label: 'Bank Banten',
    value: 'Bank Banten'
  },
  {
    label: 'Bank Bengkulu',
    value: 'Bank Bengkulu'
  },
  {
    label: 'Bank Bisnis',
    value: 'Bank Bisnis'
  },
  {
    label: 'Bank BJB',
    value: 'Bank BJB'
  },
  {
    label: 'Bank BJB Syariah',
    value: 'Bank BJB Syariah'
  },
  {
    label: 'Bank BNP',
    value: 'Bank BNP'
  },
  {
    label: 'Bank CNB',
    value: 'Bank CNB'
  },
  {
    label: 'Bank Bumi Artha',
    value: 'Bank Bumi Artha'
  },
  {
    label: 'Bank Capital',
    value: 'Bank Capital'
  },
  {
    label: 'Bank CTBC Indonesia ',
    value: 'Bank CTBC Indonesia '
  },
  {
    label: 'Bank Dinar',
    value: 'Bank Dinar'
  },
  {
    label: 'Bank DKI',
    value: 'Bank DKI'
  },
  {
    label: 'Bank DKI Syariah',
    value: 'Bank DKI Syariah'
  },
  {
    label: 'Bank Eka',
    value: 'Bank Eka'
  },
  {
    label: 'Bank Fama',
    value: 'Bank Fama'
  },
  {
    label: 'Bank Ganesha',
    value: 'Bank Ganesha'
  },
  {
    label: 'Bank Harda',
    value: 'Bank Harda'
  },
  {
    label: 'Bank ICBC',
    value: 'Bank ICBC'
  },
  {
    label: 'Bank Ina',
    value: 'Bank Ina'
  },
  {
    label: 'Bank Index',
    value: 'Bank Index'
  },
  {
    label: 'Bank J Trust',
    value: 'Bank J Trust'
  },
  {
    label: 'Bank Jasa Jakarta',
    value: 'Bank Jasa Jakarta'
  },
  {
    label: 'Bank Kesejahteraan',
    value: 'Bank Kesejahteraan'
  },
  {
    label: 'Bank Maspion',
    value: 'Bank Maspion'
  },
  {
    label: 'Bank Mayora',
    value: 'Bank Mayora'
  },
  {
    label: 'Bank Mega Syariah',
    value: 'Bank Mega Syariah'
  },
  {
    label: 'Bank Mestika',
    value: 'Bank Mestika'
  },
  {
    label: 'Bank Shinhan',
    value: 'Bank Shinhan'
  },
  {
    label: 'Bank Mitraniaga',
    value: 'Bank Mitraniaga'
  },
  {
    label: 'Bank Mizuho',
    value: 'Bank Mizuho'
  },
  {
    label: 'Bank Muamalat',
    value: 'Bank Muamalat'
  },
  {
    label: 'Bank Multiarta Sentosa',
    value: 'Bank Multiarta Sentosa'
  },
  {
    label: 'Bank Nagari',
    value: 'Bank Nagari'
  },
  {
    label: 'Bank Nagari Syariah',
    value: 'Bank Nagari Syariah'
  },
  {
    label: 'Bank of America',
    value: 'Bank of America'
  },
  {
    label: 'Bank of India Indonesia',
    value: 'Bank of India Indonesia'
  },
  {
    label: 'Bank Resona Perdania',
    value: 'Bank Resona Perdania'
  },
  {
    label: 'Bank Royal Indonesia',
    value: 'Bank Royal Indonesia'
  },
  {
    label: 'Bank Sampoerna',
    value: 'Bank Sampoerna'
  },
  {
    label: 'Bank SBI',
    value: 'Bank SBI'
  },
  {
    label: 'Bank Sinar',
    value: 'Bank Sinar'
  },
  {
    label: 'Bank Sinarmas',
    value: 'Bank Sinarmas'
  },
  {
    label: 'Bank Sinarmas Syariah',
    value: 'Bank Sinarmas Syariah'
  },
  {
    label: 'Bank Sulselbar',
    value: 'Bank Sulselbar'
  },
  {
    label: 'Bank Sulselbar Syariah',
    value: 'Bank Sulselbar Syariah'
  },
  {
    label: 'Bank Victoria',
    value: 'Bank Victoria'
  },
  {
    label: 'Bank Victoria Syariah',
    value: 'Bank Victoria Syariah'
  },
  {
    label: 'Bank Windu',
    value: 'Bank Windu'
  },
  {
    label: 'Bank Woori Saudara',
    value: 'Bank Woori Saudara'
  },
  {
    label: 'Bank Yudha Bhakti',
    value: 'Bank Yudha Bhakti'
  },
  {
    label: 'BCA Syariah',
    value: 'BCA Syariah'
  },
  {
    label: 'BNI Syariah',
    value: 'BNI Syariah'
  },
  {
    label: 'BNP PARIBAS',
    value: 'BNP PARIBAS'
  },
  {
    label: 'BOC (Bank of China)',
    value: 'BOC (Bank of China)'
  },
  {
    label: 'BPD Aceh',
    value: 'BPD Aceh'
  },
  {
    label: 'BPD Bali',
    value: 'BPD Bali'
  },
  {
    label: 'BPD Jambi',
    value: 'BPD Jambi'
  },
  {
    label: 'BPD Jambi Syariah',
    value: 'BPD Jambi Syariah'
  },
  {
    label: 'BPD Jateng',
    value: 'BPD Jateng'
  },
  {
    label: 'BPD Jateng Syariah',
    value: 'BPD Jateng Syariah'
  },
  {
    label: 'BPD Jatim',
    value: 'BPD Jatim'
  },
  {
    label: 'BPD Jatim Syariah',
    value: 'BPD Jatim Syariah'
  },
  {
    label: 'BPD Kalbar',
    value: 'BPD Kalbar'
  },
  {
    label: 'BPD Kalbar Syariah',
    value: 'BPD Kalbar Syariah'
  },
  {
    label: 'BPD Kalsel',
    value: 'BPD Kalsel'
  },
  {
    label: 'BPD Kalses Syariah',
    value: 'BPD Kalses Syariah'
  },
  {
    label: 'BPD Kalteng',
    value: 'BPD Kalteng'
  },
  {
    label: 'BPD Kaltim',
    value: 'BPD Kaltim'
  },
  {
    label: 'BPD Kaltim Syariah',
    value: 'BPD Kaltim Syariah'
  },
  {
    label: 'BPD Lampung',
    value: 'BPD Lampung'
  },
  {
    label: 'BPD Maluku',
    value: 'BPD Maluku'
  },
  {
    label: 'Bank NTB',
    value: 'Bank NTB'
  },
  {
    label: 'BPD NTT',
    value: 'BPD NTT'
  },
  {
    label: 'BPD Papua',
    value: 'BPD Papua'
  },
  {
    label: 'BPD Riau',
    value: 'BPD Riau'
  },
  {
    label: 'BPD Riau Syariah',
    value: 'BPD Riau Syariah'
  },
  {
    label: 'BPD Sulteng',
    value: 'BPD Sulteng'
  },
  {
    label: 'BPD Sultra',
    value: 'BPD Sultra'
  },
  {
    label: 'BPD Sulut',
    value: 'BPD Sulut'
  },
  {
    label: 'BPD Sumsel Babel',
    value: 'BPD Sumsel Babel'
  },
  {
    label: 'BPD Sumsel Babel Syariah',
    value: 'BPD Sumsel Babel Syariah'
  },
  {
    label: 'BPD SUMUT',
    value: 'BPD SUMUT'
  },
  {
    label: 'BPD SUMUT Syariah',
    value: 'BPD SUMUT Syariah'
  },
  {
    label: 'BPD Yogyakarta',
    value: 'BPD Yogyakarta'
  },
  {
    label: 'BPD Yogyakarta Syariah',
    value: 'BPD Yogyakarta Syariah'
  },
  {
    label: 'BPR',
    value: 'BPR'
  },
  {
    label: 'BPR KS',
    value: 'BPR KS'
  },
  {
    label: 'BRI Agro',
    value: 'BRI Agro'
  },
  {
    label: 'BRI Syariah',
    value: 'BRI Syariah'
  },
  {
    label: 'BTMU',
    value: 'BTMU'
  },
  {
    label: 'BTN Syariah',
    value: 'BTN Syariah'
  },
  {
    label: 'Bukopin',
    value: 'Bukopin'
  },
  {
    label: 'Bukopin Syariah',
    value: 'Bukopin Syariah'
  },
  {
    label: 'CIMB Niaga Syariah',
    value: 'CIMB Niaga Syariah'
  },
  {
    label: 'CITIBANK',
    value: 'CITIBANK'
  },
  {
    label: 'Danamon Syariah',
    value: 'Danamon Syariah'
  },
  {
    label: 'DBS',
    value: 'DBS'
  },
  {
    label: 'Deutsche Bank',
    value: 'Deutsche Bank'
  },
  {
    label: 'HSBC',
    value: 'HSBC'
  },
  {
    label: 'JP Morgan Chase',
    value: 'JP Morgan Chase'
  },
  {
    label: 'KEB Hana Bank',
    value: 'KEB Hana Bank'
  },
  {
    label: 'Mandiri Syariah',
    value: 'Mandiri Syariah'
  },
  {
    label: 'Maybank',
    value: 'Maybank'
  },
  {
    label: 'Maybank Syariah',
    value: 'Maybank Syariah'
  },
  {
    label: 'MNC Bank',
    value: 'MNC Bank'
  },
  {
    label: 'Nobu Bank',
    value: 'Nobu Bank'
  },
  {
    label: 'OCBC NISP',
    value: 'OCBC NISP'
  },
  {
    label: 'OCBC NISP Syariah',
    value: 'OCBC NISP Syariah'
  },
  {
    label: 'Panin',
    value: 'Panin'
  },
  {
    label: 'Panin Syariah',
    value: 'Panin Syariah'
  },
  {
    label: 'Permata Syariah',
    value: 'Permata Syariah'
  },
  {
    label: 'Prima Master Bank',
    value: 'Prima Master Bank'
  },
  {
    label: 'QNB',
    value: 'QNB'
  },
  {
    label: 'Rabobank',
    value: 'Rabobank'
  },
  {
    label: 'Standard Chartered',
    value: 'Standard Chartered'
  },
  {
    label: 'UOB Indonesia',
    value: 'UOB Indonesia'
  },
])