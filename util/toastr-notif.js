import toastr from 'toastr';
import 'toastr/build/toastr.min.css';

export const successNotification = (message) => {
    toastr.clear();
    setTimeout(() => {toastr.success(message), 3000});
}

export const errorNotification = (message) => {
    toastr.clear();
    setTimeout(() => {toastr.error(message), 3000});
}