import cookie from 'js-cookie';

export const validateEmail = (mail) => {
  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,10})+$/.test(mail)) {
    return (true);
  }
  return (false);
};

export const formatNumber = (x) => x ? x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') : 0;

export const formatCurrencyToNumber = (value) => {
  let valWithNoPrefix = value.replace('Rp', '').trim()
  while(valWithNoPrefix.charAt(0) === '0')
  {
    valWithNoPrefix = valWithNoPrefix.substr(1);
  }
  const val = valWithNoPrefix.split('.').join('')
  return val
}

export const formatCurrencyToPercent = (value) => {
  let valWithNoPrefix = value.replace('%', '').trim()
  while(valWithNoPrefix.charAt(0) === '0')
  {
    valWithNoPrefix = valWithNoPrefix.substr(1);
  }
  const val = valWithNoPrefix.split('.').join('')
  return val
}

export const  isEmpty = (obj) =>  {
  for(var key in obj) {
      if(obj.hasOwnProperty(key))
          return false;
  }
  return true;
}


/**
 * cookie helper methods
 */
export const setCookie = (key, value) => {
  if (process.browser) {
    cookie.set(key, value, {
      expires: 1,
      path: '/'
    });
  }
};

export const removeCookie = (key) => {
  if (process.browser) {
    cookie.remove(key, {
      expires: 1
    });
  }
};

export const getCookie = (key, req) => (process.browser ? getCookieFromBrowser(key) : getCookieFromServer(key, req));

const getCookieFromBrowser = (key) => cookie.get(key);

const getCookieFromServer = (key, req) => {
  if (!req.headers.cookie) {
    return undefined;
  }
  const rawCookie = req.headers.cookie.split(';').find((c) => c.trim().startsWith(`${key}=`));
  if (!rawCookie) {
    return undefined;
  }
  return rawCookie.split('=')[1];
};
/** END COOKIE */


// Bank Image
export const getBankImage = (name) => {
  switch (name) {
    case 'bca':
      return '/images/ic_bank_bca.png';
    case 'permata':
      return '/images/ic_bank_permata.png';
    case 'bni':
      return '/images/ic_bank_bni.png';
    default:
      return '/images/ic_bank_mandiri.png';
  }
}

// Bank ATM
export const getAtmGuide = (name) => {
  switch (name) {
    case 'bca':
      return [
        `Masukkan kartu ATM dan PIN`,
        `Pilih menu Transaksi Lainnya`,
        `Pilih Transfer`,
        `Pilih Ke rekening BCA Virtual Account`,
        `Masukkan Nomor Virtual Account, misalnya 123456789012XXXX`,
        `Pilih Benar`,
        `Pilih Ya`,
        `Ambil bukti bayar`,
        `Selesai`,
      ];
    case 'permata':
      return [
        `Masukkan kartu ATM dan PIN`,
        `Pilih menu Transaksi Lainnya`,
        `Pilih Pembayaran`,
        `Pilih Pembayaran Lain-Lain`,
        `Pilih Virtual Account`,
        `Masukkan Nomor Virtual Account, misalnya 8625XXXXXXXXXXXX`,
        `Pilih Benar`,
        `Pilih Ya`,
        `Ambil bukti bayar`,
        `Selesai`,
      ];
    case 'bni':
      return [
        `Masukkan Kartu`,
        `Pilih Bahasa`,
        `Masukkan PIN ATM`,
        `Pilih menu Lainnya`,
        `Pilih Transfer`,
        `Pilih Jenis rekening yang akan kamu gunakan (contoh: “Dari Rekening Tabungan”)`,
        `Pilih Virtual Account Billing`,
        `Masukkan Nomor Virtual Account (contoh: 123456789012XXXX).`,
        `Tagihan yang harus dibayarkan akan muncul pada layar konfirmasi`,
        `Konfirmasi jika sudah sesuai, lanjutkan transaksi`,
        `Transaksi selesai.`,
      ];
    default:
      return [
        `Masukkan kartu ATM dan PIN`,
        `Pilih Lainnya`,
        `Pilih Lainnya sekali lagi`,
        `Pilih Multi Payment`,
        `Masukkan 70012 sebagai Kode Perusahaan/Institusi`,
        `Pilih Benar`,
        `Masukkan Kode Bayar Midtrans, misalnya 70012XXXXXXXXXXX`,
        `Pilih Benar`,
        `Tekan tombol angka 1`,
        `Pilih Ya`,
        `Ambil bukti bayar`,
        `Selesai`,
      ];
  }
}
export const getInternetGuide = (name) => {
  switch (name) {
    case 'bca':
      return [
        `Login Internet Banking`,
        `Pilih Transaksi Dana`,
        `Pilih Transfer Ke BCA Virtual Account`,
        `Masukkan Nomor Virtual Account, misalnya 123456789012XXXX, sebagai No. Virtual Account`,
        `Klik Lanjutkan`,
        `Masukkan Respon KeyBCA Appli 1`,
        `Klik Kirim`,
        `Bukti bayar ditampilkan`,
        `Selesai`,
      ];
    case 'permata':
      return [
        `Login Internet Banking`,
        `Pilih Pembayaran Tagihan`,
        `Pilih Virtual Account`,
        `Masukkan Nomor Virtual Account, misalnya 8625XXXXXXXXXXXX, sebagai No. Virtual Account`,
        `Masukkan Nominal, misalnya 10000`,
        `Klik Kirim`,
        `Masukkan Token`,
        `Klik Kirim`,
        `Bukti bayar akan ditampilkan`,
        `Selesai`,
      ];
    case 'bni':
      return [
        `Ketik alamat https://ibank.bni.co.id pada browser, kemudian klik Enter`,
        `Masukkan User ID dan Password`,
        `Pilih menu Transfer`,
        `Pilih Virtual Account Billing`,
        `Masukan Nomor Virtual Account (contoh: 123456789012XXXX), lalu pilih rekening debet yang akan digunakan`,
        `Klik lanjut`,
        `Tagihan yang harus dibayarkan akan muncul pada layar konfirmasi`,
        `Masukkan Kode Otentikasi Token`,
        `Pembayaran berhasil`,
      ];
    default:
      return [
        `Login Internet Banking`,
        `Pilih Bayar`,
        `Pilih Multi Payment`,
        `Masukkan Midtrans sebagai Penyedia Jasa`,
        `Masukkan Nomor Virtual Account, misalnya 70012XXXXXXXXXXX, sebagai Kode Bayar`,
        `Ceklis IDR`,
        `Klik Lanjutkan`,
        `Bukti bayar ditampilkan`,
        `Selesai`,
      ];
  }
}

// Export
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';

const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const fileExtension = '.xlsx';

export const exportData = (csvData, fileName) => {
  const ws = XLSX.utils.json_to_sheet(csvData);
  const wb = { Sheets: { 'data': ws }, SheetNames: ['data'] };
  const excelBuffer = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });
  const data = new Blob([excelBuffer], {type: fileType});
  FileSaver.saveAs(data, fileName + fileExtension);
}
//\\

// https
const https = require("https");
export const options = {
  // agent: new https.Agent({
  //   rejectUnauthorized: false
  // })
};
//\\


// Image Compression
import imageCompression from 'browser-image-compression';

export const compressImage = async (file, size) => {
 
  const imageFile = file;
  console.log('originalFile instanceof Blob', imageFile instanceof Blob); // true
  console.log(`originalFile size ${imageFile.size / 1024 / 1024} MB`);
 
  const options = {
    maxSizeMB: size ? size : 0.5,
    maxWidthOrHeight: size < 1 ? 1920 : 1920,
    useWebWorker: true
  }
  try {
    const compressedFile = await imageCompression(imageFile, options);
    console.log('compressedFile instanceof Blob', compressedFile instanceof Blob); // true
    console.log(options)
    console.log(`compressedFile size ${compressedFile.size / 1024 / 1024} MB`); // smaller than maxSizeMB
 
    return compressedFile; // write your own logic
  } catch (error) {
    console.log(error);
    return null;
  }
}

// Moment
import moment from 'moment'

export const formatDate = (date, format) => moment(date).format(format)