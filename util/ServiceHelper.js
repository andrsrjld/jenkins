import fetch from 'isomorphic-unfetch';
const https = require("https");

const endpoint = 'http://156.0.100.89:5050/api/v1/';

const options = {
  // agent: new https.Agent({
  //   rejectUnauthorized: false
  // })
};

const api = async ({
  url, method, headers, body
}) => {
  const prom = await fetch(`${endpoint}${url}`, {
    method,
    headers: {
      'Content-Type': 'application/json',
      ...headers
    },
    body: JSON.stringify(body),
    ...options
  });
  return prom.json();
};

const getHeader = (token) => ({
  Authorization: `Bearer ${token}`
});

// Authentication
export const login = (body) => api({
  url: 'auth/login',
  method: 'POST',
  body
});

export const loginLakkon = (body) => api({
  url: 'auth/employee_login',
  method: 'POST',
  body
});

export const apiRegister = (body) => api({
  url: 'auth/register',
  method: 'POST',
  body
});

export const apiRegisterSeller = (body) => api({
  url: 'auth/seller_register',
  method: 'POST',
  body
});

export const getProfile = (token) => api({
  url: 'restricted/user',
  method: 'GET',
  headers: getHeader(token)
});


export const apiSearchAddress = (keyword) => api({
  url: `search_city_district?keyword=${keyword}`,
  method: 'GET'
});

export const requestEmailVerification = (email) =>  api({
  url: `auth/verification/request`,
  method: 'POST',
  body: {
    email
  }
})

export const confirmEmailVerification = (email, token) =>  api({
  url: `auth/verification/validation`,
  method: 'POST',
  body: {
    email,
    token_verification: token
  }
})

export const authLookup = (phoneNumber) => api({
  url: `auth/lookup`,
  method: 'POST',
  body: {
    phone: phoneNumber
  }
})

export const authRegister = (body) => api({
  url: `auth/register`,
  method: 'POST',
  body: body
})