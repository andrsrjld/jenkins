
const withPWA = require('next-pwa');
const withCSS = require('@zeit/next-css');
const path = require('path');

module.exports = withCSS(
  withPWA({
    webpack: (config) => {
      config.resolve = {
        ...config.resolve,
        alias: {
          ...config.resolve.alias,
          '~': path.resolve(__dirname)
        }

      };
      // config.resolve.alias['~'] = path.resolve(__dirname);
      return config;
    },
    pwa: {
      disable: true,
      dest: 'public'
    }
  })
);
