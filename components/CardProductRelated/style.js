import css from 'styled-jsx/css';

export default css`
.container {
  padding: 10px;
  background: #FFFFFF;
  border: 1px solid #ECECEC;
  box-sizing: border-box;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.13);
  border-radius: 4px;
  margin-bottom: 16px;
  margin-top: 8px;
  max-width:100%;
}

.product-name {
  margin-top: 20px;
  font-weight: bold;
  font-size: 16px;
  text-align: center;
  color: #696969;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}

.merchant {
  font-weight: 500;
  font-size: 16px;
  text-align: center;
  color: #696969;
  margin-top: 8px;
}

.comments {
  font-size: 10px;
  text-align: center;
  color: #868686;
  margin-left: 5px;
}

.container-price {
  margin-top: 10px;
  text-align: center;
}

.price {
  font-weight: 500;
  font-size: 14px;
  text-align: center;
  color: #BDBDBD;
  margin-right: 10px;
  text-decoration-line: line-through;
}

.price-selling {
  font-weight: 500;
  font-size: 18px;
  color: #010101;
}
.product-item img,
.product-name,
.merchant{
  cursor:pointer;
}

@media (min-width: 768px) and (max-width: 1024px) {
}

@media (min-width: 320px) and (max-width: 767px) {
  .product-grid{
    padding: 20px;
  }
  .product-name{
    font-size:16px;
  }
  .merchant{
    font-size:14px;
  }
  .price, .price-selling{
    display:block;
  }
  .product-item{
    min-height:350px;
  }
}
`;
