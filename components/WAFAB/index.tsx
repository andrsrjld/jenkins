import React from 'react'

const WAFAB = () => {
  return (
    <div
        className="mb-4 mb-md-0"
        style={{
          position: 'fixed',
          bottom: 32,
          right: 32,
          zIndex: 99999,
          backgroundColor: '#FFF',
          borderRadius: '100%',
          cursor: 'pointer'
        }}
        onClick={() => {
          window.open(
            'https://api.whatsapp.com/send?phone=&text=',
            '_blank' // <- This is what makes it open in a new window.
          );
        }}
      >
        <img src="/images/ic_wa_icon.png" alt="" />
      </div>
  )
}

export default WAFAB