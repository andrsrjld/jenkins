import React, { useEffect, useState, useRef } from 'react';
import { connect } from 'react-redux';
import Link from 'next/link';
import Head from 'next/head';
import BaseLayout from '~/components/BaseLayout';
import Modal from '~/components/Modal';
import { formatNumber } from '~/util/helper';
import { getShipping } from '~/data/api/shipping';
import { getPaymentMethods, postPayment, getPaymentTypes } from '~/data/api/payment';
import { createBigIntLiteral } from 'typescript';
import { getVoucherById, getVoucherByCode, postValidateVoucher, getAvailableVouchers } from '~/data/api/voucher';
import ReactPlaceholder from 'react-placeholder';
import Loading from '~/components/Loading/';

// Toastr Notification
import {successNotification, errorNotification} from '~/util/toastr-notif';

const index = ({ token, addresses, cart, handleBack, TselectedAddress, Tsellers, Tshippings, cbPayment, authentication, serviceCharge, simulation }) => {
  const [modalAddress, setModalAddress] = useState(false);
  const [modalCourir, setModalCourir] = useState(false);

  const [addressData, setAddressData] = useState(addresses.data);
  const [selectedAddress, setSelectedAddress] = useState(TselectedAddress.data);
  const [cartData, setCartData] = useState(cart);
  const [sellers, setSellers] = useState(Tsellers.data);
  const [shippingData, setShippingData] = useState(Tshippings.data);
  const [paymentMethods, setPaymentMethods] = useState([]);
  const [paymentTypes, setPaymentTypes] = useState([]);
  const [selectedPayment, setSelectedPayment] = useState(null);
  const [tokenidCC, setTokenIdCC] = useState('');
  const [activePayment, setActivePayment] = useState(0);
  const [ccLogo, setCCLogo] = useState('');
  const [loading, setLoading] = useState(true);
  const [loadingCart, setLoadingCart] = useState(true);
  const [voucherDiscount, setVoucherDiscount] = useState(0);
  const [voucherID, setVoucherID] = useState(null);
  const [voucherPaymentMethod, setVoucherPaymentMethod] = useState(null);

  const cardNumberRef = useRef(null);
  const cardExpMonthRef = useRef(null);
  const cardExpYearRef = useRef(null);
  const cardCvvRef = useRef(null);

  const [modalVoucher, setModalVoucher] = useState(false)
  const [availableVouchers, setAvailableVouchers] = useState([])

  const [formCC, setFormCC] = useState({
    card_number: '',
    card_exp_month: '',
    card_exp_year: '',
    card_cvv: '',
  });

  const [voucherCode, setVoucherCode] = useState('');

  useEffect(() => {
    handleGetPaymentMethods();
    fetchVouchers();
  }, []);

  const fetchVouchers = async () => {
    const res = await getAvailableVouchers(token)
    setAvailableVouchers(res.data ? res.data.filter(v => v.notes && v.notes.includes('term')).map(v => ({
      ...v,
      parsedNotes: v.notes ? v.notes.includes('term') ? JSON.parse(v.notes) : '' : {}
    })) : [])
  }

  const handleGetPaymentMethods = async () => {
    const paymentTypes = await getPaymentTypes(token);
    const res = await getPaymentMethods(token);
    
    setPaymentTypes(paymentTypes.data.map(v => ({...v})));
    setPaymentMethods(res.data.map(v => ({...v, image: getImage(v.name)})));
    setLoading(false);
    setLoadingCart(false);
  }

  const handleOrder = async () => {
    //validasi voucher
    if (voucherPaymentMethod != null & voucherPaymentMethod !== '') {
      if (voucherPaymentMethod !== selectedPayment.id.toString()) {
          console.log({voucherPaymentMethod, selectedPayment})
        errorNotification("Maaf voucher hanya bisa digunakan menggunakan kartu debit Mandiri");
        return
      } else {
        // validasi bin mandiri
        const acceptedBin = ['409766', '461699', '461700', '483795', '483796']
        const userBin = formCC.card_number.substr(0,6)
        if (acceptedBin.includes(userBin)) {
          // console.log({acceptedBin, userBin})
        } else {
          errorNotification("Maaf voucher hanya bisa digunakan menggunakan kartu debit Mandiri"); return
        }
      }
    }
    // order
    setLoading(true);
    const total_price = shippingData.reduce((a,b) => a + b.products.reduce((x, y) => x + y.total_prices, 0), 0);
    const total_shipping = shippingData.reduce((a,b) => a + b.shippingPrice, 0);
    const total_layanan = serviceCharge;

    const body = {
      user_name: authentication.user.name,
      address_id: selectedAddress.id,
      total_price: total_price,
      total_shipping: total_shipping,
      total_paid: total_price + total_shipping + total_layanan - voucherDiscount,
      total_discount: voucherDiscount,
      voucher_id: voucherID,
      service_charge: serviceCharge,
      payment_method: selectedPayment.slug,
      payment_method_id: selectedPayment.id,
      payment_type: paymentTypes.find(v => v.id == selectedPayment.payment_type_id).name,
      order_sellers: shippingData.map(v => {
        const products = v.products;
        return {
          seller_name: v.seller_name,
          seller_id: v.seller_id,
          status: "Menunggu Konfirmasi",
          shipping: v.selectedShipping,
          total_quantity: products.reduce((a, b) => a + b.quantity, 0),
          total_weight: v.weight,
          total_shipping: v.shippingPrice,
          total_price: products.reduce((a,b) => a + b.total_prices, 0),
          total_product_discount: products.reduce((a, b) => a + (b.product_prices - (b.total_prices / b.quantity)), 0),
          order_items: products.map(k => {
            return {
              product_id: k.product_id,
              variant_id: k.variant_id,
              product_name:k.product_name,
              weight: k.weight,
              quantity: k.quantity,
              price: k.product_prices,
              subtotal: k.total_prices,
              product_discount: k.product_prices - (k.total_prices / k.quantity),
              notes: k.notes
          }
          })
        }
      })
    }
    console.log(body);

    if(selectedPayment.payment_type_id === 4){
      await handleCreditCardPayment(body)
    } else {
      const res = await postPayment(token, body);
      if(res.code == 200){
        setLoading(false);

        if(res.error && (res.error == 'Stok Habis' || res.error == 'Produk Tidak Tersedia')) {
          errorNotification("Maaf pesanan anda gagal diproses, karena terdapat produk yang kekurangan stok / tidak tersedia");
        } else {
          if(selectedPayment.payment_type_id === 3){
            window.location = res.data.redirect;
          } else {
            cbPayment(res.data);
          }
        }
      } else {
        setLoading(false);
        errorNotification(res.error);
      }
      // window.location.href = /payment/snap
    }
  }

  const getImage = (name) => {
    switch (name) {
      case 'BCA':
        return '/images/ic_bank_mandiri.png';
      case 'Permata':
        return '/images/ic_bank_permata.png';
      default:
        return '/images/ic_bank_mandiri.png';
    }
  }

  useEffect(() => {
    Tshippings.callback(shippingData)
  }, [shippingData])

  useEffect(() => {
    addresses.callback(addressData)
  }, [addressData])

  const handleChangeShipping= (sellerId) => (e) => {
    setShippingData(shippingData.map(v => ({...v, selectedShipping: v.seller_id === sellerId ? e.target.value : v.selectedShipping})))
  }

  const handleChangeSelectedAddress = (index) => {
    setAddressData(addressData.map((v,i) => ({...v, isChecked: index === i})))
  }

  const handleSubmitChangeAddress = () => {
    setSelectedAddress(addressData.find(v => v.isChecked))
    setModalAddress(false)
  }

  const handleChangePayment = (value) => (e) => {
    setSelectedPayment(value);
  }

  const handleChangeFormCC = (name, id) => (e) => {
    const { value } = e.target;
    setFormCC({ ...formCC, [name]: value });

    if(name === "card_number" && value.length === 16){
      cardExpMonthRef.current.focus();
    }

    if(name === "card_exp_month" && value.length === 2){
      cardExpYearRef.current.focus();
    }
    
    if(name === "card_exp_year" && value.length === 2){
      cardCvvRef.current.focus();
    }

    if(name === "card_cvv" && value.length === 3){
      cardCvvRef.current.blur();
    }
    
    const selectedPayment = paymentMethods.find(item => item.id === id);
    if(formCC.card_number !== "" && formCC.card_exp_month !== "" && formCC.card_exp_year !== "" && formCC.card_cvv !== ""){
      setSelectedPayment(selectedPayment);
    }

  };

  const handleCreditCardPayment = (body) => {
    if(formCC.card_number !== "" && formCC.card_exp_month !== "" && formCC.card_exp_year !== "" && formCC.card_cvv !== ""){
      const options = {
        onSuccess: async function(response){
          // Success to get card token_id, implement as you wish here
          console.log('Success to get card token_id, response:', response);
          console.log('This is the card token_id:', response.token_id);

          body.token_id = response.token_id;
          const res = await postPayment(token, body);
          if(res.code == 200){
            window.location = res.data.redirect
            setLoading(false);
            // console.log(res);
            // cbPayment(res.data);
          } else {
            setLoading(false);
            errorNotification(res.error);
          }
        },
        onFailure: async function(response){
          setLoading(false);
          // Fail to get card token_id, implement as you wish here
          console.log('Fail to get card token_id, response:', response);
          errorNotification("Card Invalid!");
        }
      };
      window.MidtransNew3ds.getCardToken({...formCC, card_exp_year: `20${formCC.card_exp_year}`}, options);
    }
  }

  const handleChangeActivePayment = (id) => (e) => {
    setActivePayment(id);
    if(id === 4){
      const selectedPayment = paymentMethods.find(item => item.payment_type_id === id);
      if(formCC.card_number !== "" && formCC.card_exp_month !== "" && formCC.card_exp_year !== "" && formCC.card_cvv !== ""){
        setSelectedPayment(selectedPayment);
      }
    } else {
      setSelectedPayment(null);
    }
  }

  const handleCCLogo = (e) => {
    if(formCC.card_number.length >= 15){
      const visaRegex = new RegExp(`^4[0-9]{6,}$`);
      const mastercardRegex = new RegExp(`^5[1-5][0-9]{14}$`);
      if(visaRegex.test(formCC.card_number)){
        setCCLogo(`/images/ic_visa.png`);
      } else if(mastercardRegex.test(formCC.card_number)){
        setCCLogo('/images/ic_mastercard.png');
      } else {
        setCCLogo('');
      }
    }
  }

  const handleChangeVoucher = (e) => {
    setVoucherCode(e.currentTarget.value);
  }

  const handleSubmitVoucher = async (e) => {
    setLoadingCart(true);
    const total_price = shippingData.reduce((a,b) => a + b.products.reduce((x, y) => x + y.total_prices, 0), 0);
    const total_shipping = shippingData.reduce((a,b) => a + b.shippingPrice, 0);
    const resVoucher = await getVoucherByCode(token, voucherCode);
    if(resVoucher.code == 200){
      const body = {
        id: resVoucher.data.id,
        user_spend: total_price
      }
      const resValidate = await postValidateVoucher(token, body);
      if(resValidate.code == 200 && resValidate.data.status){
        let discountAmount = 0;
        const discountTarget = resVoucher.data.voucher_type === "1" ? total_price : total_shipping;
        discountAmount = resVoucher.data.enum_discount_type === "1" ? resVoucher.data.discount : resVoucher.data.discount * discountTarget / 100;
        if(discountAmount > discountTarget){
          discountAmount = discountTarget;
        }
        setVoucherDiscount(discountAmount);
        setVoucherID(resVoucher.data.id)
      } else {
        errorNotification("Voucher tidak valid");
      }
    } else {
      errorNotification("Voucher tidak valid");
    }
    setLoadingCart(false);
  }

  const handleSeeVoucher = () => {
    setModalVoucher(true)
  }

  const handleSelectedVoucher = async (voucher) => {
    setLoading(true)
    const total_price = shippingData.reduce((a,b) => a + b.products.reduce((x, y) => x + y.total_prices, 0), 0);
    const total_shipping = shippingData.reduce((a,b) => a + b.shippingPrice, 0);
    const body = {
      id: voucher.id,
      user_spend: total_price,
      payment_method: voucher.payment_method
    }
    const resValidate = await postValidateVoucher(token, body);
      if(resValidate.code == 200 && resValidate.data.status){
        let discountAmount = 0;
        const discountTarget = voucher.voucher_type === "1" ? total_price : total_shipping;
        discountAmount = voucher.enum_discount_type === "1" ? voucher.discount : (voucher.discount * discountTarget / 100 > voucher.max_discount ? voucher.max_discount : voucher.discount * discountTarget / 100);
        if(discountAmount > discountTarget){
          discountAmount = discountTarget;
        }
        setVoucherDiscount(discountAmount);
        setVoucherID(voucher.id)
        setVoucherPaymentMethod(voucher.payment_method)
        setModalVoucher(false)
        setVoucherCode(voucher.code)
      } else {
        errorNotification("Sorry u can't use this voucher");
      }
    setLoading(false)
  }

  return (
    <div>
      <Head>
        <script id="midtrans-script" type="text/javascript"
        src="https://api.midtrans.com/v2/assets/js/midtrans-new-3ds.min.js" 
        data-environment="production" 
        data-client-key="Mid-client-CkOpJCyIOhbaY8Q2"></script>
      </Head>
      <Loading show={loading} />
      <div id="breadcumb" className="payment-content">
        {/* <div className="breadcumb">
            Home &#x0226B;
          {' '}
            Keranjang &#x0226B;
          {' '}
            <span className="cursor-pointer" onClick={handleBack}>Shipping</span> &#x0226B;
          {' '}
          <span style={{ color: '#010101' }}>Review & Payment</span>
        </div> */}

        <div className="c">
          <div className="title voucher-title" style={voucherDiscount > 0 ? {display: 'block'} : {display: 'block'}}>
              Gunakan
            {' '}
            <span style={{ color: '#010101' }}>Voucher</span>
          </div>
          <div className="c-content flex flex-wrap">
            <div className="w-3/4 xs:w-full sm:w-full list-payment-wrapper">
              <div className="c-1">
                <div className="w-full flex flex-wrap items-center voucher-wrapper" style={voucherDiscount > 0 ? {display: 'block'} : {display: 'block'}}>
                  <label className="label w-full">Kode Voucher</label>
                  <div className="w-full flex justify-between items-center xs:flex-wrap sm:flex-wrap space-x-4" style={{ marginTop: '1rem' }} >
                    <div className="form-control xs:w-full sm:w-full w-3/5">
                      <input className="w-full" placeholder="Masukkan Voucher Discount" onChange={handleChangeVoucher} value={voucherCode}/>
                    </div>
                    {/* <button className="btn-primary btn-this voucher-btn w-full xs:w-full sm:w-full uppercase h-full" onClick={handleSeeVoucher}>Available Voucher</button> */}
                    <button className="btn-primary btn-this voucher-btn w-2/5 xs:w-full sm:w-full h-full" disabled={voucherCode.length > 0 ? false : true} onClick={handleSubmitVoucher}>VALIDASI VOUCHER</button>
                  </div>
                </div>
                <div className="title" style={{ marginTop: '3rem' }}>
                    Pilih Metode
                  {' '}
                  <span style={{ color: '#010101' }}>Pembayaran</span>
                </div>
                <ReactPlaceholder ready={!loading} showLoadingAnimation={true} rows={6} style={{margin: '10px 0'}}>
                <div className="payment-card-wrapper" style={{ marginTop: '2rem' }}>
                  {
                    (voucherPaymentMethod == null || voucherPaymentMethod == '') &&
                    paymentTypes.filter(v => ([1, 2].includes(v.id))).map(item => {
                      return (
                        <div>
                          <div className="parent-card" onClick={handleChangeActivePayment(item.id)}>
                            <div className="parent-first md:flex-wrap lg:flex-wrap">
                              <div  className="parent-image">
                                <img className="img-payment" src={item.id === 1 ? '/images/ic_koperasi.png' : '/images/ic_cicilan.svg'} alt="" />
                              </div>
                              <div className="parent-card-text w-full">
                                <div className="w-100 child-title"><b>{item.id === 3 ? "QR Payment" : item.name}</b></div>
                                <div className="w-100 child-description">{item.description}</div>
                              </div>
                            </div>
                            <img src={activePayment === item.id ? "/images/ic_arrow_up.png" : "/images/ic_arrow_down.png"} alt="" width="12px" />
                          </div>
                          {
                            paymentMethods.filter(w => (w.payment_type_id === item.id) && (w.id != 7)).map(w => {
                              return (
                                <div>
                                {w.payment_type_id == 4 ? (
                                  <div className="parent-card" style={activePayment === item.id ? {display: "block"} : {display: "none"}}>
                                    <div className="parent-first w-full flex-wrap">
                                      <div className="flex w-full form-row">
                                        <div className="form-group w-full">
                                          <div className="fake-input flex items-center">
                                            <input className="cc-input-text" type="text" placeholder="Card Number" ref={cardNumberRef} value={formCC.card_number} onChange={handleChangeFormCC('card_number', w.id)} onBlur={handleCCLogo} maxLength={16} required />
                                            <img className="cc-logo" src={ccLogo} alt="" />
                                          </div>
                                        </div>
                                      </div>
                                      <div className="flex xs:flex-wrap sm:flex-wrap w-full form-row cc-expiry-wrapper">
                                        <div className="form-group w-4/5 xs:w-full sm:w-full">
                                          <div className="flex">
                                            <div className="w-2/5">
                                              <input type="text" placeholder="Expiry Date Month (ex: 02, 04, 06)" ref={cardExpMonthRef} value={formCC.card_exp_month} onChange={handleChangeFormCC('card_exp_month', w.id)} required maxLength={2} />
                                            </div>
                                            <div className="w-1/5" style={{textAlign: "center"}}>
                                              /
                                            </div>
                                            <div className="w-2/5">
                                              <input type="text" placeholder="Expiry Date Year (ex: 23, 25)" ref={cardExpYearRef} value={formCC.card_exp_year} onChange={handleChangeFormCC('card_exp_year', w.id)} required maxLength={2} />
                                            </div>
                                          </div>
                                        </div>
                                        <div className="form-group w-1/5 xs:w-full sm:w-full">
                                          <input type="password" placeholder="CVV" ref={cardCvvRef} value={formCC.card_cvv} onChange={handleChangeFormCC('card_cvv', w.id)} required maxLength={3} />
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                ) : (
                                  <div className="parent-card" style={activePayment === item.id ? {display: "block"} : {display: "none"}}>
                                    <div className="parent-first child-payment md:flex-wrap lg:flex-wrap">
                                      <label className="c-radio payment-radio">
                                        <input id={`payment-${w.id}`} type="radio" name="bank" onChange={handleChangePayment(w)} />
                                        <span className="checkmark" />
                                      </label>
                                      <img className="img-payment" src={w.logo_url} alt="" />
                                      <div className="parent-card-text w-full">
                                        <label for={`payment-${w.id}`}>
                                          <div className="w-100 child-title"><b>{w.payment_type_id === 3 ? "QR Payment" : w.name}</b></div>
                                          <div className="w-100 child-description">{w.description}</div>
                                        </label>
                                      </div>
                                      {
                                        ['3x Cicilan', '6x Cicilan', '12x Cicilan'].includes(w.name) && (
                                          <div style={{minWidth: '150px'}}>
                                                <div className="w-100 child-title"><b>
                                                  {
                                                    w.name === '3x Cicilan' ? simulation['3x']
                                                      : w.name === '6x Cicilan' ? simulation['6x']
                                                        : w.name === '12x Cicilan' ? simulation['12x']
                                                          : ''
                                                  }
                                                </b></div>
                                          </div>
                                        )     
                                      }
                                    </div>
                                  </div>
                                )}
                                </div>
                              )
                            })
                          }
                        </div>
                      )
                    })
                  }
                  {
                    voucherPaymentMethod === "7" &&
                    paymentTypes.filter(v => ([4].includes(v.id))).map(item => {
                      return (
                        <div>
                          <div className="parent-card" onClick={handleChangeActivePayment(item.id)}>
                            <div className="parent-first md:flex-wrap lg:flex-wrap">
                              <img className="img-payment" src="/images/ic_bank_mandiri.png" alt="" />
                              <div className="parent-card-text w-full">
                                <div className="w-100 child-title"><b>Debit Card Bank Mandiri</b></div>
                                <div className="w-100 child-description">{item.description} BANK MANDIRI</div>
                              </div>
                            </div>
                            <img src={activePayment === item.id ? "/images/ic_arrow_up.png" : "/images/ic_arrow_down.png"} alt="" width="12px" />
                          </div>
                          {
                            paymentMethods.filter(w => (w.payment_type_id === item.id) && (w.id == 7)).map(w => {
                              return (
                                <div>
                                {w.payment_type_id == 4 ? (
                                  <div className="parent-card" style={activePayment === item.id ? {display: "block"} : {display: "none"}}>
                                    <div className="parent-first w-full flex-wrap">
                                      <div className="flex w-full form-row">
                                        <div className="form-group w-full">
                                          <div className="fake-input flex items-center">
                                            <input className="cc-input-text" type="text" placeholder="Card Number" ref={cardNumberRef} value={formCC.card_number} onChange={handleChangeFormCC('card_number', w.id)} onBlur={handleCCLogo} maxLength={16} required />
                                            <img className="cc-logo" src={ccLogo} alt="" />
                                          </div>
                                        </div>
                                      </div>
                                      <div className="flex xs:flex-wrap sm:flex-wrap w-full form-row cc-expiry-wrapper">
                                        <div className="form-group w-4/5 xs:w-full sm:w-full">
                                          <div className="flex">
                                            <div className="w-2/5">
                                              <input type="text" placeholder="Expiry Date Month (ex: 02, 04, 06)" ref={cardExpMonthRef} value={formCC.card_exp_month} onChange={handleChangeFormCC('card_exp_month', w.id)} required maxLength={2} />
                                            </div>
                                            <div className="w-1/5" style={{textAlign: "center"}}>
                                              /
                                            </div>
                                            <div className="w-2/5">
                                              <input type="text" placeholder="Expiry Date Year (ex: 23, 25)" ref={cardExpYearRef} value={formCC.card_exp_year} onChange={handleChangeFormCC('card_exp_year', w.id)} required maxLength={2} />
                                            </div>
                                          </div>
                                        </div>
                                        <div className="form-group w-1/5 xs:w-full sm:w-full">
                                          <input type="password" placeholder="CVV" ref={cardCvvRef} value={formCC.card_cvv} onChange={handleChangeFormCC('card_cvv', w.id)} required maxLength={3} />
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                ) : (
                                  <div className="parent-card" style={activePayment === item.id ? {display: "block"} : {display: "none"}}>
                                    <div className="parent-first child-payment md:flex-wrap lg:flex-wrap">
                                      <label className="c-radio payment-radio">
                                        <input id={`payment-${w.id}`} type="radio" name="bank" onChange={handleChangePayment(w)} />
                                        <span className="checkmark" />
                                      </label>
                                      <img className="img-payment" src={w.logo_url} alt="" />
                                      <div className="parent-card-text w-full">
                                        <label for={`payment-${w.id}`}>
                                          <div className="w-100 child-title"><b>{w.payment_type_id === 3 ? "QR Payment" : w.slug}</b></div>
                                          <div className="w-100 child-description">{w.description}</div>
                                        </label>
                                      </div>
                                    </div>
                                  </div>
                                )}
                                </div>
                              )
                            })
                          }
                        </div>
                      )
                    })
                  }
                </div>
                </ReactPlaceholder>

                <div style={{ textAlign: 'center', marginTop: '2rem', padding: '0px 3rem' }}>
                    <button className="btn-primary btn-this xs:hidden sm:hidden" style={{ width: '100%' }} disabled={selectedPayment == null} onClick={() => handleOrder()}>Lanjut Pembayaran</button>
                </div>
              </div>
            </div>
            <div className="w-1/4 xs:w-full sm:w-full">
              <div className="c-2">
                <div className="card-2">
                  <div className="summary-header">
                    Ringkasan
                  </div>
                  <ReactPlaceholder ready={!loadingCart} showLoadingAnimation={true} rows={5} style={{margin: '10px 0'}}>
                    <div className="c-content-summary">
                      <div className="key">
                        Subtotal
                      </div>
                      <div className="value">
                        {`Rp ${formatNumber(shippingData.reduce((a, b) => a + b.products.reduce((x, y) => x + y.total_prices, 0), 0))}`}
                      </div>
                    </div>

                    {/* <div className="c-content-summary">
                      <div className="key">
                        Biaya Layanan
                      </div>
                      <div className="value">
                      {`Rp ${formatNumber(serviceCharge)}`}
                      </div>
                    </div> */}


                    <div className="c-content-summary">
                      <div className="key">
                        Pengiriman
                      </div>
                      <div className="value">
                        {`Rp ${formatNumber(shippingData.reduce((a,b) => a + b.shippingPrice, 0))}`}
                      </div>
                    </div>

                    <div className="c-content-summary">
                      <div className="key">
                        Diskon
                      </div>
                      <div className="value">
                        {`Rp ${formatNumber(voucherDiscount)}`}
                      </div>
                    </div>

                    <div className="c-content-summary">
                      <div className="key">
                        Total Pesanan
                      </div>
                      <div className="value">
                        <span style={{ color: '#FF8242', fontWeight: "900", fontSize:"20px" }}>{`Rp ${formatNumber(shippingData.reduce((a,b) => a + b.products.reduce((x, y) => x + y.total_prices, 0), 0)+serviceCharge+shippingData.reduce((a,b) => a + b.shippingPrice, 0) - voucherDiscount)}`}</span>
                      </div>
                    </div>
                  </ReactPlaceholder>

                  <div className="c-content-summary">
                    <div className="key">
                      {`${shippingData.reduce((a, b) => a + b.products.length, 0)} Produk di Keranjang`}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="next-button-mobile" style={{ textAlign: 'center', marginTop: '2rem' }}>
              <button className="btn-primary hidden btn-this xs:block sm:block md:hidden lg:hidden" style={{ width: '100%' }} disabled={selectedPayment == null} onClick={() => handleOrder()}>Lanjut Pembayaran</button>
            </div>
          </div>
        </div>
      </div>

      <Modal open={modalVoucher} onClose={() => setModalVoucher(false)}>
        <div className="card-modal" style={{ padding: '16px 26px' }}>
          <div style={{ textAlign: 'right' }}>
            <div style={{ display: 'inline-block' }} onClick={() => setModalVoucher(false)} onKeyDown={() => setModalVoucher(false)} className="pointer" role="button" tabIndex={0}>
              <img src="/images/ic_close_red.png" alt="" />
            </div>
          </div>
          <div className="title" style={{ marginTop: '16px' }}>
            Available Voucher
          </div>
          <div style={{ marginTop: '24px' }} className="w-full">
            <div className="w-full mt-4 flex flex-wrap">
                {
                  availableVouchers.map((v, i) => (
                    <div className={`w-full lg:w-1/2 xl:w-1/2 h-full p-2`}>
                      <div className="w-full h-full shadow-md px-4 py-3 rounded cursor-pointer border border-gray-400 hover:border-primary hover:border" onClick={(e) => { e.stopPropagation(); handleSelectedVoucher(v)}}>
                        <div className="w-full flex items-start text-sm relative">
                          <div className="flex-auto pr-2">
                            {v.parsedNotes.title}
                          </div>

                          {v.payment_method === "7" && (
                            <div className="absolute top-0 right-0 w-10 h-10 flex-none cursor-pointer flex items-center justify-center">
                              <img src={'images/ic_bank_mandiri.png'}/>
                            </div>
                          )
                          }
                        </div>
                        <div className="w-full mt-2 text-sm">
                          {v.parsedNotes.term}
                        </div>
                        <div className="w-full text-xs font-bold mt-1 italic" v-if="">
                          {v.payment_method === "7" ? 'Debit Bank Mandiri Only' : ''}
                        </div>

                        <div className="w-full text-sm mt-3">
                          Kode Promo: <b className="text-primary">{v.code}</b> <span className="text-xs"> (klik kode)</span>
                        </div>
                        <div className="w-full text-xs font-bold mt-1 italic" v-if="">
                          {v.parsedNotes.date}
                        </div>
                      </div>
                    </div>
                  ))
                }
            </div>
          </div>
          <div className="divider" style={{ margin: '24px' }} />
        </div>
      </Modal>


      <Modal open={modalAddress} onClose={() => setModalAddress(false)}>
        <div className="card-modal" style={{ padding: '16px 26px' }}>
          <div style={{ textAlign: 'right' }}>
            <div style={{ display: 'inline-block' }} onClick={() => setModalAddress(false)} onKeyDown={() => setModalAdd(false)} className="pointer" role="button" tabIndex={0}>
              <img src="/images/ic_close_red.png" alt="" />
            </div>
          </div>
          <div className="title" style={{ marginTop: '16px' }}>
            Pilih Alamat Lain
          </div>
          <div style={{ marginTop: '24px' }}>
            <table className="table">
              <thead>
                <th className="th bg-white" />
                <th className="th bg-white">Penerima</th>
                <th className="th bg-white">Alamat Pengirim</th>
                <th className="th bg-white">Daerah Pengiriman</th>
              </thead>
              <tbody>
                {
                  addressData.map((v, i) => (
                    <tr>
                      <td className="td" style={{ padding: '0px 1rem' }}>
                        <label className="c-radio" style={{ marginTop: '-9px' }}>
                          <input type="radio" checked={v.isChecked} name="address" onChange={() => handleChangeSelectedAddress(i) }/>
                          <span className="checkmark" />
                        </label>
                      </td>
                      <td className="td" style={{ minWidth: '250px' }}>
                        <b>{v.name}</b>
                        <br />
                        <div style={{ marginTop: '1rem' }}>
                          {v.phone}
                        </div>
                      </td>
                      <td className="td">
                        <b>{v.address_name}</b>
                        <br />
                        <div style={{ marginTop: '1rem', maxWidth: '450px' }}>
                          {v.address}
                        </div>
                      </td>
                      <td className="td">
                        {`${v.province}, ${v.city}, ${v.district}, ${v.postal_code} ${v.country}`}
                      </td>
                    </tr>
                  ))
                }
              </tbody>
            </table>
          </div>
          <div style={{ marginTop: '24px', padding: '1rem 3rem', textAlign: 'center' }}>
            <button className="btn-primary btn-this" onClick={handleSubmitChangeAddress}>SIMPAN</button>
          </div>
          <div className="divider" style={{ margin: '24px' }} />
        </div>
      </Modal>

      <Modal open={modalCourir} onClose={() => setModalCourir(false)}>
        <div className="card-modal" style={{ padding: '16px 26px' }}>
          <div style={{ textAlign: 'right' }}>
            <div style={{ display: 'inline-block' }} onClick={() => setModalCourir(false)} onKeyDown={() => setModalCourir(false)} className="pointer" role="button" tabIndex={0}>
              <img src="/images/ic_close_red.png" alt="" />
            </div>
          </div>
          <div className="title" style={{ marginTop: '16px' }}>
            Pilih Metode Pengiriman Lain
          </div>
          <div style={{ marginTop: '24px' }}>
            <table>
              <thead>
                <th />
                <th>G Coffee Roastery</th>
                <th />
                <th />
              </thead>
              <tbody>
                <tr>
                  <td style={{ padding: '0px 1rem' }}>
                    <label className="c-radio" style={{ marginTop: '-9px' }}>
                      <input type="radio" checked name="courir_change" />
                      <span className="checkmark" />
                    </label>
                  </td>
                  <td>
                      Rp 13.000
                  </td>
                  <td>
                    <b>TIKI | Economy Service</b>
                  </td>
                  <td>
                      4 Hari Kerja
                  </td>
                </tr>
                <tr>
                  <td style={{ padding: '0px 1rem' }}>
                    <label className="c-radio" style={{ marginTop: '-9px' }}>
                      <input type="radio" name="courir_change" />
                      <span className="checkmark" />
                    </label>
                  </td>
                  <td>
                      Rp 16.000
                  </td>
                  <td>
                    <b>TIKI | Regular Service</b>
                  </td>
                  <td>
                      2 Hari Kerja
                  </td>
                </tr>
                <tr>
                  <td style={{ padding: '0px 1rem' }}>
                    <label className="c-radio" style={{ marginTop: '-9px' }}>
                      <input type="radio" name="courir_change" />
                      <span className="checkmark" />
                    </label>
                  </td>
                  <td>
                      Rp 17.000
                  </td>
                  <td>
                    <b>JNE | Economy Service</b>
                  </td>
                  <td>
                      2-3 Hari Kerja
                  </td>
                </tr>

              </tbody>
            </table>
          </div>
          <div style={{ marginTop: '24px', padding: '1rem 3rem', textAlign: 'center' }}>
            <button className="btn-primary btn-this">SIMPAN</button>
          </div>
          <div className="divider" style={{ margin: '24px' }} />
        </div>
      </Modal>

      <style jsx>
        {`
          .c{
            margin-top:0;
          }
          .title {
            font-weight: bold;
            font-size: 24px;
            line-height: 23px;
            letter-spacing: 0.05em;
            color: #696969;
            padding: 0px;
          }

          .c-content {
            display: flex;
            flex-direction: row;
          }

          .c-1 {
            width: 100%;
            margin-right: 28px;
          }

          .c-2 {
            width: 100%;
          }
          .card-2 {
            background: #FFFFFF;
            box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.25);
            height: auto;
          }

          .summary-header {
            padding: 1rem;
            font-weight: bold;
            font-size: 18px;
            line-height: 17px;
            color: #868686;
            box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.25);
          }

          .c-content-summary {
            font-size: 16px;
            line-height: 15px;
            color: #696969;
            display: flex;
            justify-content: space-between;
            padding: 0.7rem;
            margin: 0.8rem 0.5rem;
            border-bottom: 1px solid #CCCCCC;
          }

          .key {
            display: flex;
          }

          .key img {
            width: 85px;
          }

          .card {
            background: #FFFFFF;
            border: 0.5px solid #010101;
            box-sizing: border-box;
            box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.25);
            border-radius: 5px;
          }

          th {
            text-align: left;
            padding: 1rem 1rem;
          }

          .btn-action {
            background: #FFFFFF;
            border: 0.5px solid #696969;
            box-sizing: border-box;
            border-radius: 7px;
            font-weight: 500;
            font-size: 18px;
            line-height: 17px;
            text-align: center;
            color: #696969;
            padding: 1rem 2rem;
            cursor: pointer;
          }

          td {
            border-bottom: 1px solid #CCCCCC;
          }

          .btn-this {
            width: auto;
            font-weight: 500;
            font-size: 16px;
            line-height: 17px;
            text-align: center;
            color: #FFFFFF;
            padding: 15px 20px;
          }

          .voucher-btn{
            font-size: 14px;
            padding: 8px 12px;
          }

          .label {
            font-weight: 500;
            font-size: 16px;
            line-height: 19px;
            color: #868686;
          }
          .payment-card-wrapper > div:first-child > .parent-card{
            border-top:1px solid #CCCCCC;
          }
          .parent-card {
            display: flex;
            justify-content: space-between;
            align-items: center;
            border-bottom: 1px solid #CCCCCC;
            margin: 1rem 0px;
            padding: 0.5rem 1px;
            cursor: pointer;
          }

          .img-payment {
            width: 50px;
            margin: 0px 20px;
            object-fit: contain;
          }

          .parent-image {
            width: 120px !important;
            display: inline-block;
          }

          .parent-card-text {
            font-size: 16px;
            line-height: 22px;
            letter-spacing: 0.05em;
            color: #696969;
          }

          .parent-first {
            display: flex;
            align-items: center;
            width: 400px;
          }
          
          .form-group input{
            width:100%;
          }
          .fake-input{
            border-bottom:1px solid #e2e8f0;
          }
          .cc-input-text{
            width:90% !important;
            display:inline-block;
            border:none;
            box-shadow:none;
            padding:10px;
          }
          .cc-logo{
            width:10%;
            display:inline-block;
            padding:0 35px;
          }
          .form-group{
            padding:15px 10px;
          }
          .form-row{
            padding:0 15px;
          }
          .cc-expiry-wrapper input{
            border-bottom:1px solid #e2e8f0;
          }
          .list-payment-wrapper{
            padding-right:28px;
          }
          .payment-content{
            padding: 20px 0;
          }
          .loading{
            width:100%;
            position:fixed;
            top:0;
            left:0;
            height:100%;
            background:rgba(255,255,255,0.5);
          }
          .loading .loading-img-wrapper{
            width:100%;
            position:absolute;
            top:50%;
            transform: translateY(-50%);
            -webkit-transform: translateY(-50%);
            -moz-transform: translateY(-50%);
            -o-transform: translateY(-50%);
            -ms-transform: translateY(-50%);
          }
          .loading img{
            margin:0 auto;
          }
          .voucher-wrapper{
            margin-top: 2em;
          }
          .voucher-title{
            margin-top: 0;
          }
          .payment-radio{
            margin-top: -20px;
            margin-left: 3rem
          }
          @media (min-width: 320px) and (max-width: 767px) {
            .payment-content{
              padding: 20px
            }
            .td{
              padding: 10px;
            }
            .c-cart, .bulk-action {
              width: 100%;
            }
            .c-1{
              padding-right:0;
            }
            .next-button-mobile{
              width:100%;
            }
            .next-button-mobile button{
              margin:0 auto;
            }
            .cc-logo{
              width:auto;
            }
            .c{
              margin-top:0;
            }
            .title{
              text-align:center;
            }
            .voucher-btn{
              margin:15px auto 0;
            }
            .parent-first > .img-payment{
              width:40px;
            }
            .parent-card-text{
              font-size:14px;
            }
            .payment-radio{
              margin-left: 1rem
            }
            .child-payment .img-payment{
              width:60px;
            }
            .child-description{
              font-weight:400;
            }
            .child-title{
              font-size:16px;
            }
            .voucher-title{
              margin-top: 0;
            }
          }
        `
        }
      </style>
    </div>
  );
};

export default connect(
  (state) => state, {}
)(index);
