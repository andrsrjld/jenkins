import css from 'styled-jsx/css';

export default css`
.container-sales {
    width: 100%;
    display: flex;
    justify-content: center;
    margin-top: 8px;
    max-width: 1366px;
}

@media (min-width: 768px) and (max-width: 1024px) {
}

@media (min-width: 320px) and (max-width: 767px) {
}
`;
