import React from 'react';
import PropTypes from 'prop-types';
import style from './style.js';
import SaleItem from '~/components/SaleItem';

const SaleItems = ({ data }) => {
  return(
  <div className="container-sales flex flex-wrap container mx-auto">
    {
      data.map((v) => (<SaleItem data={v} key={v.id}/>))
    }

    <style jsx>{style}</style>
  </div>
)};

SaleItems.propTypes = {
  data: PropTypes.array
};

export default SaleItems;
