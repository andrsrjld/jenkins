import {STATUS} from 'data/api/order'
import moment from 'moment'
import { formatNumber } from 'util/helper'
import {successNotification, errorNotification} from 'util/toastr-notif';

const CardTransactionHistory = ({ data, callbackLacak, callbackDetail, callbackBeliLagi, callbackRefund }) => {
  const copyToClipboardResi = str => {
    const el = document.createElement('textarea');
    el.value = str;
    el.setAttribute('readonly', '');
    el.style.position = 'absolute';
    el.style.left = '-9999px';
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    successNotification("Berhasil menyalin nomor resi")
  };

  return (
    <div className="card-transaction mx-8 xs:mx-2 sm:mx-2 w-full my-2">
      <div className="header w-full">
        { moment(data.date).format('DD MMMM YYYY') }
      </div>
      <div className="section flex flex-wrap">
        <div className="w-1/4 xs:w-full sm:w-full">
          <div><b>Nomor Pesanan</b></div>
          <div>{ data.invoice_no }</div>
        </div>
        <div className="w-1/4 xs:w-full sm:w-full xs:mt-4 sm:mt-4">
          <div>Status Pesanan</div>
          <div style={{ color: '#010101' }}><b>{data.status}</b></div>
        </div>
        <div className="w-1/4 xs:w-full sm:w-full xs:mt-4 sm:mt-4">
          <div>Total Pesanan</div>
          <div style={{ color: '#FF8242' }}><b>Rp {formatNumber(data.total_price + (data.total_shipping ? data.total_shipping : 0))}</b></div>
        </div>
        <div className="w-1/4 xs:w-full sm:w-full">
          <div><b>Nomor Resi</b></div>
          <div className="cursor-pointer" onClick={() => copyToClipboardResi(data.airway_bill) }>{ data.airway_bill }</div>
        </div>
      </div>
      <div className="divider" />
      <div className="section flex flex-wrap">
        <div className="w-1/3 xs:w-full sm:w-full xs:mb-4 sm:mb-4">
          {
            data.shipping.includes('COD') ?(
                <>
                  <div>COD ambil di tempat</div>
                  <div><b>{data.shipping.substring(data.shipping.indexOf(',') + 1, data.shipping.length)}</b></div>
                </>
              )
            :(
                <>
                  <div>Dikirim ke</div>
                  <div><b>{data.receiver_name}</b></div>
                </>
              )
          }
        </div>
        <div className="w-2/3 xs:w-full sm:w-full order-detail-action">
          {
            !data.shipping.includes('COD') ? (
              <button className="btn-action btn-primary btn-tracking" disabled={data.status === STATUS.MENUNGGU_KONFIRMASI || data.status === STATUS.DIPROSES || data.status === STATUS.DIBATALKAN || data.status === STATUS.DIBAYAR} onClick={() => callbackLacak(data)}>LACAK PESANAN</button>
            ) : null
          }
          
          <button className="btn-action btn-primary ml-2" style={{ background: '#FF8242' }} onClick={() => callbackDetail(data)}>LIHAT DETAIL</button>
          {/* <button className="btn-action btn-primary" style={{ marginLeft: '1rem' }}>BELI LAGI</button> */}
          
          {/* refund */}
          {
            (data.status == STATUS.DIBAYAR  || data.status === STATUS.DIPROSES || data.status === STATUS.DIKIRIM) &&
            (
              <button className="btn-action btn-primary ml-2" style={{ background: '#FF8242' }} onClick={() => callbackRefund(data)}>REFUND</button>
            )
          }
        </div>
      </div>

      <style jsx>
        {
          `
          .btn-tracking {
            background-color: #3FAE5E;
          }
          .btn-tracking:disabled {
            background-color: #C4C4C4;
          }
        .c {
          width: 100%;
          display: flex;
        }

        .c-filter {
          width: 325px;
        }
        .divider {
          width: 100%;
          margin-top: 12px;
          height: 1px;
          background: #E1E1E1;
        }

        .c-menu {
          font-weight: 500;
          font-size: 18px;
          line-height: 17px;
          letter-spacing: 0.05em;
          display: flex;
          justify-content: space-between;
          align-items: center;
          flex-wrap: wrap;
          padding: 10px 0px;
          cursor: pointer
        }

        .active {
          color: #010101;
        }

        .card-content {
          background: #FFFFFF;
          box-shadow: 0px 0px 4px rgba(0, 0, 0, 0.25);
          border-radius: 0px 7px 7px 7px;
          padding: 26px 32px;
        }

        .header {
          display: flex;
          justify-content: space-between;
        }

        .title {
          font-weight: 500;
          font-size: 22px;
          line-height: 21px;
          letter-spacing: 0.05em;
          color: #696969;
        }

        .btn-action {
          width: auto;
          font-size: 16px;
          padding: 14px 24px;
        }

        .btn-table {
          font-size: 16px;
          line-height: 15px;
          letter-spacing: 0.05em;
          color: #696969;
          background: #FFFFFF;
          border: 1px solid #696969;
          box-sizing: border-box;
          border-radius: 5px;
          padding: 12px 18px;
          cursor: pointer;
        }

        .sub-header {
          font-size: 16px;
          line-height: 15px;
          color: #868686;
        }

        .card-pin {
          background: #FFFFFF;
          box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.25);
          border-radius: 7px;
          display: flex;
          padding: 16px 24px;
          align-items: center;
        }

        .card-pin .text{
          font-size: 16px;
          line-height: 15px;
          color: #BDBDBD;
          padding: 0px 16px;
        }

        .btn-action-outline {
          background: #FFFFFF;
          border: 0.5px solid #010101;
          box-sizing: border-box;
          border-radius: 2px;
          font-size: 16px;
          line-height: 15px;
          color: #010101;
          padding: 12px 18px;
          cursor: pointer;
        }

        
          .c-filter-page {
            font-size: 16px;
            color: #696969;
          }
        
          .form-filter-page {
            background: rgba(248, 248, 248, 0.75);
            border: 1px solid #F2F2F2;
            box-sizing: border-box;
            padding: 4px 4px;
            font-size: 16px;
            color: #696969;
            margin: 0px 4px;
            font-family: 'Gotham';
          }

          .card-transaction {
            background: #FFFFFF;
            box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.15);
            border-radius: 7px;
          }

          .card-transaction .header {
            background: #FAFAFA;
            border-radius: 7px 7px 0px 0px;
            font-size: 16px;
            color: #696969;
            padding: 1rem;
          }
          .card-transaction .section {
            display: flex;
            font-size: 16px;
            color: #696969;
            padding: 1.5rem 2rem;
          }

          .td-small {
            padding: 0.2rem 1rem;
            border: 0px;
          }

          .td-med {
            padding: 0.5rem 1rem;
            border: 0px;
          }

          thead {
            background: #E8E8E8;
            border-radius: 7px;
          }

          th {
            padding: 1rem 1rem;
          }

          .lacak-header {
            display: flex;
            justify-content: space-between;
            align-items: center;
            padding: 1rem 1.5rem;
            background: #FFFFFF;
            box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.15);
            border-radius: 7px;
          }

          .lacak-sub-header {
            display: flex;
            font-size: 16px;
            line-height: 19px;
            letter-spacing: 0.05em;
            color: #696969;
          }
          @media (min-width: 320px) and (max-width: 767px) {
            .order-detail-action button{
              width:100%;
              margin-top:15px;
            }
          }
        `
        }
      </style>
    </div>
  )
}

export default CardTransactionHistory