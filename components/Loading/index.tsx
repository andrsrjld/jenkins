import { motion } from "framer-motion"

interface TProps {
  show: boolean,
  opacity?: boolean
}

const Loading = ({ show = false, opacity = true } : TProps) => {
  const className = opacity ? 
    `w-full h-screen fixed top-0 left-0 right-0 border-0 bg-gray-600 bg-opacity-50 flex justify-center items-center z-50` :
    `w-full h-screen fixed top-0 left-0 right-0 border-0 bg-white flex justify-center items-center z-50`;

  return (
    show && (
      <div className={className} style={{ zIndex: 15000 }}>
        <div className="flex flex-wrap p-4 rounded">
          <div className="w-full flex justify-center">
            {/* <motion.div
              animate={{ rotate: 360 }}
              transition={{ duration: 2, loop: Infinity, }}
            > */}
              <img src="/images/ic_bapera.png" alt="" className="h-16"/>
            {/* </motion.div> */}
          </div>
          <div className="w-full text-center mt-4 text-text-primary text-lg">
            
            <motion.div
                animate={{ y: 10 }}
                transition={{ 
                  duration: 0.8,
                  yoyo: Infinity, 
                }}
              >
              {/* Loading */}
            </motion.div>
          </div>
        </div>
      </div>
    )
  )
}

export default Loading