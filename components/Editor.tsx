import dynamic from 'next/dynamic';

const HtmlEditor = dynamic(
  import('react-quill'),
  {
    ssr: false,
    loading: () => null,
  }
);

export default HtmlEditor;