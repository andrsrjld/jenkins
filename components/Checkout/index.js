import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import Link from 'next/link';
import BaseLayout from '~/components/BaseLayout';
import Modal from '~/components/Modal';
import { getAddresses, insertAddress, deleteAddress, getAddress, updateAddress } from '~/data/api/address';
import { getCart } from '~/data/api/cart';
import { formatNumber } from '~/util/helper';
import { getShipping } from '~/data/api/shipping';
import MapPicker from 'components/MapPicker'
import ReactPlaceholder from 'react-placeholder';

// Toastr Notification
import {successNotification, errorNotification} from '~/util/toastr-notif';

// Add Address Modal
import Payment from '~/components/Payment';
import Select from 'react-select/async';
import { apiSearchAddress } from '~/util/ServiceHelper';

const index = ({ token, addresses, cart, handleNext, TselectedAddress, Tsellers, Tshippings, serviceCharge }) => {
  const [modalAddress, setModalAddress] = useState(false);
  const [addressData, setAddressData] = useState(addresses.data);
  const [selectedAddress, setSelectedAddress] = useState(TselectedAddress.data);
  const [cartData, setCartData] = useState(cart);
  const [sellers, setSellers] = useState(Tsellers.data);
  const [shippingData, setShippingData] = useState(Tshippings.data);
  const [openAddressModal, setOpenAddressModal] = useState(false);
  const [openAddAddressModal, setOpenAddAddressModal] = useState(selectedAddress ? false : true);
  const [addressAction, setAddressAction] = useState("insert");

  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const tempSeller = [];
    cart.forEach((v) => {
      if (tempSeller.filter((k) => v.seller_id === k.seller_id).length === 0) {
        // Get Product data @seller
        const product = [];
        v.carts.map(item => {
          if(item.checkout){
            product.push({
              product_id: item.product_id,
              variant_id: item.variant_id,
              image_url: item.image_url,
              weight: typeof item.weight != 'undefined' ? item.weight : 0,
              product_name: item.product_name,
              quantity: item.quantity,
              product_prices: item.product_prices,
              product_discount: item.product_discount,
              total_prices: item.total_prices,
              district_id: item.district_id,
              notes: item.notes,
              variant_name: item.variant_name
            });
          }
        });
        if(product.length > 0){
          tempSeller.push({ 
            lat_long: v.lat_long,
            seller_name: v.seller_name,
            seller_address: v.seller_address,
            district_id: product[0].district_id,
            seller_id: parseInt(v.seller_id),
            shipping: [],
            shippingPrice: 0,
            selectedShipping: '',
            weight: product.reduce((a,b) => a + (b.weight * b.quantity), 0),
            products: product
          });
        }
      }
    });
    setSellers([...tempSeller])
    setLoading(false);
  }, []);

  useEffect(() => {
    TselectedAddress.callback(selectedAddress)
    if (sellers.length > 0) {
      console.log(sellers);
      setSellers([...sellers.map(v => {
        // console.log(window.document.querySelector(`input[name="${v.seller_id}"]`).checked = false)
        return ({...v, selectedShipping:'', shippingPrice: 0});
      })])
    }
  }, [selectedAddress])

  useEffect(() => {
    if(selectedAddress) {
      if (sellers.length > 0) {
        const proms = [];
        console.log(sellers)
        sellers.forEach(async (v) => {
          if(v.products.length > 0){
            const body = {
              seller_id: v.seller_id,
              origin_lat_long: v.lat_long,
              destination_lat_long: `${selectedAddress ? selectedAddress.lat_long : '0,0'}`,
              origin: `${v.district_id}`,
              origin_type: 'subdistrict',
              destination: `${selectedAddress ? selectedAddress.district_id : 0}`,
              destination_type: 'subdistrict',
              weight: v.weight,
              customer_address_id: `${selectedAddress ? Number(selectedAddress.id) : null}`,
              product_amount: v.products.reduce((a,b) => a + b.total_prices, 0)
            };
            // const api = axiosSetup(token)
            // const listShipping = await api.get("/restricted/shipping", {
            //   body: body
            // });
            // console.log(listShipping);
            proms.push(getShipping(token, body));
          }
        });
        let temp = [...sellers];
        Promise.all(proms)
          .then((res) => {
            res.forEach((v,i) => {
              temp[i].shipping = v.data;
            });
            setShippingData(temp)
          })
      }
    }
  }, [sellers]);

  useEffect(() => {
    Tshippings.callback(shippingData)
  }, [shippingData])

  useEffect(() => {
    addresses.callback(addressData)
  }, [addressData])

  const handleChangeShipping= (e) => {
    setShippingData(shippingData.map(v => ({...v, selectedShipping: v.seller_id === parseInt(e.currentTarget.value) ? e.target.value : v.selectedShipping, shippingPrice: v.seller_id === parseInt(e.currentTarget.value) ? parseFloat(e.currentTarget.selectedOptions[0].attributes.price.value) : v.shippingPrice})))
  }

  const handleChangeSelectedAddress = (index) => {
    setAddressData(addressData.map((v,i) => ({...v, isChecked: index === i})))
  }

  const handleSubmitChangeAddress = () => {
    setSelectedAddress(addressData.find(v => v.isChecked))
    setModalAddress(false)
  }

  const reloadAddressData = async () => {
    const addresses = await getAddresses(token);
    setAddressData(addresses.data);
    setSelectedAddress(addresses.data[addresses.data.length - 1]);
  }

  const closeModal = () => {
    setOpenAddressModal(false);
  }

  const closeAddModal = () => {
    setOpenAddAddressModal(false);
  }

  return (
    <div>
        <div id="breadcumb" className="checkout-content">
          
          {/* <div className="breadcumb">
            Home &#x0226B;
            {' '}
            Keranjang &#x0226B;
            {' '}
            <span style={{ color: '#010101' }}>Shipping</span>
          </div> */}

          <div className="c">

            <div className="c-content flex flex-wrap">
              <div className="w-3/4 xs:w-full sm:w-full">
                <div className="c-1">
                  <div className="title">
                    Alat Pengiriman
                    {' '}
                    <span style={{ color: '#010101' }}>Belanja</span>
                  </div>
                  <ReactPlaceholder ready={!loading} showLoadingAnimation={true} rows={3} style={{width: '100%', marginTop: '30px'}}>
                    <div className="card" style={{ marginTop: '24px' }}>
                      <div className="address-title">
                        <div className="flex xs:hidden sm:hidden w-full pt-4">
                          <div className="th bg-white" style={{width: "50px"}}></div>
                          <div className="th bg-white w-1/3">Penerima</div>
                          <div className="th bg-white w-1/3">Alamat Pengirim</div>
                          <div className="th bg-white w-1/3">Daerah Pengiriman</div>
                        </div>
                        <div className="address-content">
                          <div className="flex items-center xs:flex-wrap sm:flex-wrap">
                            <div className="td xs:hidden sm:hidden" style={{ padding: '0px 0px 0px 1rem' }}>
                              <img src="/images/ic_check_red.png" alt="" />
                            </div>
                            <div className="td w-1/3 xs:w-full sm:w-full">
                              <b>{selectedAddress ? selectedAddress.name : "-"}</b>
                              <div className="address-long-content">
                                {selectedAddress ? selectedAddress.phone : "-"}
                              </div>
                            </div>
                            <div className="td w-1/3 xs:w-full sm:w-full">
                              <b>{selectedAddress ? selectedAddress.address_name : "-"}</b>
                              <br />
                              <div className="address-long-content">
                                {selectedAddress ? selectedAddress.address : "-"}
                              </div>
                            </div>
                            <div className="td w-1/3 xs:w-full sm:w-full">
                              {`${selectedAddress ? selectedAddress.province : "-"}, ${selectedAddress ? selectedAddress.city : "-"}, ${selectedAddress ? selectedAddress.district : "-"}, ${selectedAddress ? selectedAddress.postal_code : "-"} ${selectedAddress ? selectedAddress.country : "-"}`}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </ReactPlaceholder>

                  <div className="address-action xs:flex sm:flex xs:justify-between sm:justify-between" style={{ marginTop: '1rem' }}>
                    <button className="btn-action" onClick={() => setOpenAddAddressModal(true)}>TAMBAH ALAMAT</button>
                    <button className="btn-action" style={{ marginLeft: '1rem' }} onClick={() => setOpenAddressModal(true)}>UBAH ALAMAT</button>
                    <button className="btn-action" style={{ marginLeft: '1rem' }} onClick={() => setModalAddress(true)}>PILIH ALAMAT LAIN</button>
                  </div>

                  <div style={{ textAlign: 'right', marginTop: '2rem' }}>
                    <button className="btn-primary btn-this xs:hidden sm:hidden" onClick={handleNext} disabled={selectedAddress ? false : true}>SELANJUTNYA</button>
                  </div>
                </div>
              </div>
              <div className="w-1/4 xs:w-full sm:w-full">
                <div className="c-2">
                  <div className="card-2">
                    <div className="summary-header">
                      Ringkasan
                    </div>
                    
                    <ReactPlaceholder ready={!loading} showLoadingAnimation={true} rows={5} style={{margin: '10px 0'}}>
                      <div className="c-content-summary">
                        <div className="key">
                          Subtotal
                        </div>
                        <div className="value">
                          {`Rp ${formatNumber(shippingData.reduce((a, b) => a + b.products.reduce((x, y) => x + y.total_prices, 0), 0))}`}
                        </div>
                      </div>

                      {/* <div className="c-content-summary">
                        <div className="key">
                          Biaya Layanan
                        </div>
                        <div className="value">
                        {`Rp ${formatNumber(serviceCharge)}`}
                        </div>
                      </div> */}

                      <div className="c-content-summary">
                        <div className="key">
                          Pengiriman
                        </div>
                        <div className="value">
                          {`Rp ${formatNumber(shippingData.reduce((a,b) => a + b.shippingPrice, 0))}`}
                        </div>
                      </div>

                      <div className="c-content-summary">
                        <div className="key">
                          Total Pesanan
                        </div>
                        <div className="value">
                          <span style={{ color: '#FF8242', fontWeight: "900", fontSize:"20px" }}>{`Rp ${formatNumber(shippingData.reduce((a,b) => a + b.products.reduce((x, y) => x + y.total_prices, 0), 0) + serviceCharge + shippingData.reduce((a,b) => a + b.shippingPrice, 0))}`}</span>
                        </div>
                      </div>
                    </ReactPlaceholder>

                    <div className="c-content-summary">
                      <div className="key">
                        {`${shippingData.reduce((a, b) => a + b.products.length, 0)} Produk di Keranjang`}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="next-button-mobile" style={{ textAlign: 'center', marginTop: '2rem' }}>
                <button className="btn-primary btn-this xs:block hidden sm:block md:hidden lg:hidden" onClick={handleNext} disabled={selectedAddress ? false : true}>SELANJUTNYA</button>
              </div>
            </div>
          </div>
        </div>

      <AddAddressModal
        isOpen={openAddAddressModal}
        token={token}
        reloadAddress={reloadAddressData}
        idAddress={selectedAddress ? selectedAddress.id : 0}
        closeModal={closeAddModal}
      />

      <UpdateAddressModal
        isOpen={openAddressModal}
        token={token}
        reloadAddress={reloadAddressData}
        idAddress={selectedAddress ? selectedAddress.id : 0}
        closeModal={closeModal}
        selectedAddress={selectedAddress}
      />

      <Modal open={modalAddress} onClose={() => setModalAddress(false)}>
        <div className="card-modal" style={{ padding: '16px 26px' }}>
          <div style={{ textAlign: 'right' }}>
            <div style={{ display: 'inline-block' }} onClick={() => setModalAddress(false)} onKeyDown={() => setModalAddress(false)} className="pointer" role="button" tabIndex={0}>
              <img src="/images/ic_close_red.png" alt="" />
            </div>
          </div>
          <div className="overflow-address">
            <div className="title" style={{ marginTop: '16px' }}>
              Pilih Alamat Lain
            </div>
            <div style={{ marginTop: '24px' }}>
              <div className="w-full">
                <div className="w-full flex xs:hidden sm:hidden">
                  <div className="th bg-white w-1/6"></div>
                  <div className="th bg-white w-full">Penerima</div>
                  <div className="th bg-white w-full">Alamat Pengirim</div>
                  <div className="th bg-white w-full">Daerah Pengiriman</div>
                </div>
                <div className="w-full addresses-overflow">
                  {
                    addressData.map((v, i) => (
                      <div className="flex address-row">
                        <div className="td w-1/8 flex items-center" style={{ padding: '0px 1rem' }}>
                          <label className="c-radio" style={{ marginTop: '-9px' }}>
                            <input id={`address${v.id}`} type="radio" checked={v.isChecked} name="address" onChange={() => handleChangeSelectedAddress(i) }/>
                            <span className="checkmark" />
                          </label>
                        </div>
                        <div className="w-full flex xs:flex-wrap sm:flex-wrap">
                          <div className="td w-full">
                            <label for={`address${v.id}`}>
                              <b>{v.name}</b>
                              <br />
                              <div style={{ marginTop: '1rem' }}>
                                {v.phone}
                              </div>
                            </label>
                          </div>
                          <div className="td w-full">
                            <label for={`address${v.id}`}>
                              <b>{v.address_name}</b>
                              <br />
                              <div style={{ marginTop: '1rem', maxWidth: '450px' }}>
                                {v.address}
                              </div>
                            </label>
                          </div>
                          <div className="td w-full">
                            <label for={`address${v.id}`}>
                              {`${v.province}, ${v.city}, ${v.district}, ${v.postal_code} ${v.country}`}
                            </label>
                          </div>
                        </div>
                      </div>
                    ))
                  }
                </div>
              </div>
            </div>
          </div>
          <div style={{ marginTop: '24px', padding: '1rem 3rem', textAlign: 'center' }}>
            <button className="btn-primary btn-this" onClick={handleSubmitChangeAddress}>SIMPAN</button>
          </div>
          <div className="divider" style={{ margin: '24px' }} />
        </div>
      </Modal>

      <style jsx>
        {`

          .title {
            font-weight: bold;
            font-size: 24px;
            line-height: 23px;
            letter-spacing: 0.05em;
            color: #696969;
            padding: 0px;
          }

          .c-content {
            display: flex;
            flex-direction: row;
          }

          .c-1 {
            width: 100%;
            padding-right: 28px;
          }

          .c-2 {
            width: 100%;
          }
          .card-2 {
            background: #FFFFFF;
            box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.25);
            height: auto;
          }

          .summary-header {
            padding: 1rem;
            font-weight: bold;
            font-size: 18px;
            line-height: 17px;
            color: #868686;
            box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.25);
          }

          .c-content-summary {
            font-size: 16px;
            line-height: 15px;
            color: #696969;
            display: flex;
            justify-content: space-between;
            padding: 0.7rem;
            margin: 0.8rem 0.5rem;
            border-bottom: 1px solid #CCCCCC;
          }

          .key {
            display: flex;
          }

          .key img {
            width: 85px;
          }

          .card {
            background: #FFFFFF;
            border: 0.5px solid #010101;
            box-sizing: border-box;
            box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.25);
            border-radius: 5px;
          }

          th {
            text-align: left;
            padding: 1rem 1rem;
          }

          .seller-wrapper{
            padding:15px 20px;
          }
          .subtotal-wrapper{
            padding:15px 20px;
            border-top:1px solid #ccc;
          }
          .subtotal-wrapper .subtotal{
            text-align:right;
          }
          
          .seller-wrapper p{
            font-weight:400;
            font-size:14px;
            margin-top:10px;
          }

          .btn-action {
            background: #FFFFFF;
            border: 0.5px solid #696969;
            box-sizing: border-box;
            border-radius: 7px;
            font-weight: 500;
            font-size: 16px;
            line-height: 17px;
            text-align: center;
            color: #696969;
            padding: 10px 20px;
            cursor: pointer;
          }

          td {
            border-bottom: 1px solid #CCCCCC;
          }

          .btn-this {
            width: auto;
            font-weight: 500;
            font-size: 18px;
            line-height: 17px;
            text-align: center;
            color: #FFFFFF;
            padding: 1rem 2rem;
          }
          .product-item{
            padding: 10px 20px
          }
          .product-wrapper{
            padding: 15px;
          }
          .checkout-content{
            padding: 20px 0;
          }
          .address-long-content{
            margin-top:24px;
          }
          .breadcumb{
            padding:0 30px;
          }
          label {
            font-weight:400;
          }
          .addresses-overflow{
            max-height: 560px;
            overflow-y: auto;
          }
          @media (min-width: 320px) and (max-width: 767px) {
            .c{
              margin-top:0;
            }
            .checkout-content{
              padding: 20px;
            }
            .address-long-content{
              margin-top:5px;
            }
            .td{
              padding:10px 16px;
            }
            .next-button-mobile{
              width:100%;
            }
            .next-button-mobile button{
              margin:0 auto;
            }
            .address-action button{
              padding:10px 15px;
              font-size:14px;
            }
            .c-1 {
              padding-right: 0;
            }
            .address-row{
              margin-bottom:15px;
              padding-bottom:15px;
              border-bottom: 1px solid #ddd;
            }
            .overflow-address{
              overflow-y: auto;
              max-height:500px;
            }
            .title{
              text-align:center;
            }
          }
        `
        }
      </style>
    </div>
  );
};

const UpdateAddressModal = ({ isOpen, token, reloadAddress, closeModal, selectedAddress }) => {
  const latLng = selectedAddress ? selectedAddress.lat_long.split(',') : []
  const [form, setForm] = useState({
    id: selectedAddress ? selectedAddress.id : null,
    addressName: selectedAddress ? selectedAddress.address_name : '',
    name: selectedAddress ? selectedAddress.name : '',
    phone: selectedAddress ? selectedAddress.phone : '',
    address: selectedAddress ? selectedAddress.address : '',
    districtId: selectedAddress ? {value: selectedAddress.district_id, label: `${selectedAddress.province}, ${selectedAddress.city}, ${selectedAddress.district}`} : null,
    postcode: selectedAddress ? selectedAddress.postal_code : ''
  });
  const [map, setMap] = useState(false)
  const [coordinate, setCoordinate] = useState(latLng.length > 0 ? {
    lat: latLng[0],
    lng: latLng[1]
  }:null)

  const handleInsertAddress = async (e) => {
    e.preventDefault();
    if(!form.districtId){
      errorNotification("Mohon isi kota / kecamatan")
    } else {
      let body = {
        district_id: form.districtId.value,
        address_name: form.addressName,
        name: form.name,
        phone: form.phone,
        address: form.address,
        lat_long: `${coordinate.lat},${coordinate.lng}`,
        // lat_long: `0,0`,
        postal_code: form.postcode
      };
  
      let res;
      body.id = selectedAddress.id;
      body.district_id = form.districtId.value;
      body.is_default = selectedAddress.is_default;
      body.lat_long = selectedAddress.lat_long;
      res = await updateAddress(token, body);
      await reloadAddress();
  
      if (res.code === 201 || res.code === 200) {
        successNotification(`Berhasil ${res.code === 201 ? "menambahkan" : "mengubah"} alamat`);
        closeModal();
      } else {
        errorNotification(res.data.message);
      }
    }
  };

  const handleChangeForm = (name) => (e) => {
    if(name === "phone" || name === "postcode"){
      const re = /^[0-9\b]+$/;
      if (e.target.value === '' || re.test(e.target.value)) {
        const { value } = e.target;
        setForm({ ...form, [name]: value });
      }
    } else {
      const { value } = e.target;
      setForm({ ...form, [name]: value });
    }
  };

  const loadOptions = (inputValue, callback) => {
    if (inputValue.length > 3) {
      setTimeout(() => {
        apiSearchAddress(inputValue)
          .then((res) => {
            callback(res.data.map((v) => ({
              label: v.result,
              value: v.id_district
            })));
          }).catch(() => callback([]));
      }, 500);
    } else {
      callback([]);
    }
  };

  const handleSubmitMap = (coor) => {
    setMap(false)
    setCoordinate({...coor})
  }


  return (
    <Modal open={isOpen} onClose={closeModal}>
      
      {
         map
         ?
           <MapPicker 
            onClose={() => setMap(false)} 
            onSubmit={(coor) => handleSubmitMap(coor)} 
            defaultCenter={coordinate}
            reverseGeolocation={form.districtId ? form.districtId.label : null}
          /> 
         :
         <div className="card-modal" style={{ padding: '16px 26px' }}>
        <div style={{ textAlign: 'right' }}>
          <div style={{ display: 'inline-block' }} onClick={closeModal} className="pointer" role="button" tabIndex={0}>
            <img src="/images/ic_close_red.png" alt="" />
          </div>
        </div>
        <div className="addresses-overflow">
          <div className="title" style={{ marginTop: '16px' }}>
            Ubah Alamat
          </div>
          <form onSubmit={handleInsertAddress}>
            <div style={{ marginTop: '24px' }}>
              <div className="form-control w-full">
                <input className="w-full" placeholder="Nama Alamat" style={{ width: '100%' }} value={form.addressName} onChange={handleChangeForm('addressName')} required />
              </div>
              <div className="form-helper">Contoh: Alamat Rumah, Kantor, Apartemen, Dropship</div>
            </div>
            <div style={{ marginTop: '24px' }}>
              <div className="xs:flex-wrap" style={{ width: '100%', display: 'flex' }}>
                <div style={{ width: '100%' }}>
                  <div className="form-control w-100">
                    <input className="w-100" placeholder="Nama Penerima" value={form.name} onChange={handleChangeForm('name')} required />
                  </div>
                </div>
                <div className="ml-3 xs:ml-0 xs:mt-4" style={{ width: '100%' }}>
                  <div className="form-control w-100">
                    <input className="w-100" type="number" placeholder="Nomor HP" value={form.phone} onChange={handleChangeForm('phone')} required />
                  </div>
                </div>
              </div>
            </div>
            <div style={{ marginTop: '24px' }}>
              <div className="xs:flex-wrap" style={{ width: '100%', display: 'flex' }}>
                <div style={{ width: '100%' }}>
                  <Select
                    styles={{
                      control: (provided) => ({
                        ...provided,
                        background: '#FFFFFF',
                        boxShadow: '0px 1px 4px rgba(0, 0, 0, 0.25)',
                        borderRadius: '7px',
                        padding: '20px 18px',
                        border: '0px'
                      }),
                      valueContainer: (provided) => ({
                        ...provided,
                        padding: '0px'
                      }),
                      menu: (provided) => ({
                        ...provided,
                        padding: 'px'
                      }),
                      indicatorsContainer: (provider) => ({
                        ...provider,
                        display: 'none'
                      })
                    }}
                    placeholder="Kota / Kecamatan"
                    loadOptions={loadOptions}
                    cacheOptions
                    defaultOptions={selectedAddress ? [{ label: `${selectedAddress.province}, ${selectedAddress.city}, ${selectedAddress.district}`, value: selectedAddress.districtId }] : null}
                    value={form.districtId}
                    onChange={(value) => setForm({ ...form, districtId: value })}
                    required
                  />
                </div>
                <div className="ml-3 w-1/3 xs:ml-0 xs:mt-4 xs:w-full">
                  <div className="form-control w-100">
                    <input className="w-100 h-full" placeholder="Kode Pos" value={form.postcode} onChange={handleChangeForm('postcode')} required />
                  </div>
                </div>
              </div>
            </div>
            <div style={{ marginTop: '24px' }}>
              <div className="form-control w-full">
                <textarea rows="4" className="w-full" placeholder="Alamat" style={{ width: '100%' }} value={form.address} onChange={handleChangeForm('address')} required />
              </div>
            </div>
            <div id="storeDescription" className="w-full flex flex-wrap mt-4">
              <label className="w-full">Pin Location<span className="text-primary">*</span></label>
              <div className="w-full mt-1">
                <div className="w-full flex rounded items-center shadow p-4 xs:flex-wrap">
                  <div className="text w-full flex items-center">
                    <img className="w-6 mr-4" src="/images/ic_map_red.png" alt="" />
                    <span>Tandai lokasi dalam peta jika anda mengirim dengan layanan Pick Up (seperti Go Send, Grab Express)</span>
                  </div>
                  <div className="border border-primary text-primary px-4 py-2 rounded cursor-pointer xs:w-full xs:mt-4 text-center" onClick={() => setMap(true)}>
                    Tandai Lokasi
                  </div>
                </div>
              </div>
            </div>
            <div style={{ marginTop: '24px' }}>
              <button type="submit" className="btn-primary">SIMPAN</button>
            </div>
          </form>
        </div>
        <div className="divider" style={{ margin: '24px' }} />
        
        <style jsx>
          {
            `
            .addresses-overflow{
              max-height: 560px;
              overflow-y: auto;
            }`
          }
        </style>
      </div>
      }
    </Modal>
  )
}

const AddAddressModal = ({ isOpen, token, reloadAddress, closeModal }) => {
  const [form, setForm] = useState({});
  const [map, setMap] = useState(false)
  const [coordinate, setCoordinate] = useState(null)

  const handleInsertAddress = async (e) => {
    e.preventDefault();
    if(!form.districtId){
      errorNotification("Mohon isi kota / kecamatan");
    } else {
      let body = {
        district_id: form.districtId.value,
        address_name: form.addressName,
        name: form.name,
        phone: form.phone,
        address: form.address,
        lat_long: `${coordinate.lat},${coordinate.lng}`,
        // lat_long: `0,0`,
        postal_code: form.postcode
      };
  
      let res;
      res = await insertAddress(token, body);
      await reloadAddress();
  
      if (res.code === 201 || res.code === 200) {
        successNotification(`Berhasil ${res.code === 201 ? "menambahkan" : "mengubah"} alamat`);
        closeModal();
      } else {
        errorNotification(res.data.message);
      }
    }
  };

  const handleChangeForm = (name) => (e) => {
    if(name === "phone" || name === "postcode"){
      const re = /^[0-9\b]+$/;
      if (e.target.value === '' || re.test(e.target.value)) {
        const { value } = e.target;
        setForm({ ...form, [name]: value });
      }
    } else {
      const { value } = e.target;
      setForm({ ...form, [name]: value });
    }
  };

  const loadOptions = (inputValue, callback) => {
    if (inputValue.length > 3) {
      setTimeout(() => {
        apiSearchAddress(inputValue)
          .then((res) => {
            callback(res.data.map((v) => ({
              label: v.result,
              value: v.id_district
            })));
          }).catch(() => callback([]));
      }, 500);
    } else {
      callback([]);
    }
  };

  const handleSubmitMap = (coor) => {
    setMap(false)
    setCoordinate({...coor})
  }

  return (
    <Modal open={isOpen} onClose={closeModal}>
      {
         map
         ?
           <MapPicker 
            onClose={() => setMap(false)} 
            onSubmit={(coor) => handleSubmitMap(coor)} 
            defaultCenter={coordinate ? {
              lat: coordinate.lat,
              lng: coordinate.lng
            } : null}
            reverseGeolocation={form.districtId ? form.districtId.label : null}
          /> 
         :<div className="card-modal" style={{ padding: '16px 26px' }}>
         <div style={{ textAlign: 'right' }}>
           <div style={{ display: 'inline-block' }} onClick={closeModal} className="pointer" role="button" tabIndex={0}>
             <img src="/images/ic_close_red.png" alt="" />
           </div>
         </div>
         <div className="addresses-overflow">
          <div className="title" style={{ marginTop: '16px' }}>
            Tambah Alamat
          </div>
          <form onSubmit={handleInsertAddress}>
            <div style={{ marginTop: '24px' }}>
              <div className="form-control w-full">
                <input className="w-full" placeholder="Nama Alamat" style={{ width: '100%' }} value={form.addressName} onChange={handleChangeForm('addressName')} required />
              </div>
              <div className="form-helper">Contoh: Alamat Rumah, Kantor, Apartemen, Dropship</div>
            </div>
            <div style={{ marginTop: '24px' }}>
              <div className="xs:flex-wrap" style={{ width: '100%', display: 'flex' }}>
                <div style={{ width: '100%' }}>
                  <div className="form-control w-100">
                    <input className="w-100" placeholder="Nama Penerima" value={form.name} onChange={handleChangeForm('name')} required />
                  </div>
                </div>
                <div className="ml-3 xs:ml-0 xs:mt-4" style={{ width: '100%' }}>
                  <div className="form-control w-100">
                    <input className="w-100" type="number" placeholder="Nomor HP" value={form.phone} onChange={handleChangeForm('phone')} required />
                  </div>
                </div>
              </div>
            </div>
            <div style={{ marginTop: '24px' }}>
            <div className="xs:flex-wrap" style={{ width: '100%', display: 'flex' }}>
                <div style={{ width: '100%' }}>
                  <Select
                    styles={{
                      control: (provided) => ({
                        ...provided,
                        background: '#FFFFFF',
                        boxShadow: '0px 1px 4px rgba(0, 0, 0, 0.25)',
                        borderRadius: '7px',
                        padding: '20px 18px',
                        border: '0px'
                      }),
                      valueContainer: (provided) => ({
                        ...provided,
                        padding: '0px'
                      }),
                      menu: (provided) => ({
                        ...provided,
                        padding: 'px'
                      }),
                      indicatorsContainer: (provider) => ({
                        ...provider,
                        display: 'none'
                      })
                    }}
                    placeholder="Kota / Kecamatan"
                    loadOptions={loadOptions}
                    cacheOptions
                    value={form.districtId}
                    onChange={(value) => setForm({ ...form, districtId: value })}
                  />
                </div>
                <div className="ml-3 w-1/3 xs:ml-0 xs:mt-4 xs:w-full">
                  <div className="form-control w-100">
                    <input className="w-100 h-full" type="number" placeholder="Kode Pos" value={form.postcode} onChange={handleChangeForm('postcode')} required />
                  </div>
                </div>
              </div>
            </div>
            <div style={{ marginTop: '24px' }}>
              <div className="form-control w-full">
                <textarea rows="4" className="w-full" placeholder="Alamat" style={{ width: '100%' }} value={form.address} onChange={handleChangeForm('address')} required />
              </div>
            </div>
              <div id="storeDescription" className="w-full flex flex-wrap mt-4">
                  <label className="w-full">Pin Location<span className="text-primary">*</span></label>
                  <div className="w-full mt-1">
                    <div className="w-full flex rounded items-center shadow p-4 xs:flex-wrap">
                      <div className="text w-full flex items-center">
                        <img className="w-6 mr-4" src="/images/ic_map_red.png" alt="" />
                        <span>Tandai lokasi dalam peta jika anda mengirim dengan layanan Pick Up (seperti Go Send, Grab Express)</span>
                      </div>
                      <div className="border border-primary text-primary px-4 py-2 rounded cursor-pointer xs:w-full xs:mt-4 text-center" onClick={() => setMap(true)}>
                        Tandai Lokasi
                      </div>
                    </div>
                  </div>
                </div>
            <div style={{ marginTop: '24px' }}>
              <button type="submit" className="btn-primary">SIMPAN</button>
            </div>
          </form>
         </div>
         <div className="divider" style={{ margin: '24px' }} />
         
        <style jsx>
          {
            `
            .addresses-overflow{
              max-height: 560px;
              overflow-y: auto;
            }`
          }
        </style>
       </div> 
      }
      </Modal>
  )
}

export default index
