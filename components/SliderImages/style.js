import css from 'styled-jsx/css';

export default css`
.container {
    width: 100%;
    padding: 0px 50px;
}

@media (min-width: 768px) and (max-width: 1024px) {
}

@media (min-width: 320px) and (max-width: 767px) {
}
`;
