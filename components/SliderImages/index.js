import React, { useState } from 'react';
import PropTypes from 'prop-types';
import ItemsCarousel from 'react-items-carousel';
import { useMediaQuery } from 'react-responsive';
import style from './style.js';


const SaleItems = ({ data, onClick }) => {
  const chevronWidth = 50;
  const [activeItemIndex, setActiveItemIndex] = useState(0);
  const isTablet = useMediaQuery({ query: '(max-width: 1024px)' });
  const isMobile = useMediaQuery({ query: '(max-width: 480px)' });

  const getCountContent = () => {
    if (isMobile) { return 3; }
    if (isTablet) return 3;
    return 3;
  };

  const ButtonNavigation = (icon) => (
    <div
      className="pointer "
      style={{
        border: '8px', backgroundColor: '#FFFFFF'
      }}
    >
      {
        icon === 'left'
          ? (
            <svg id="i-chevron-left" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" width="24" height="24" fill="none" stroke="#BDBDBD" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2">
              <path d="M20 30 L8 16 20 2" />
            </svg>
          )
          : (
            <svg id="i-chevron-right" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" width="24" height="24" fill="none" stroke="#BDBDBD" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2">
              <path d="M12 30 L24 16 12 2" />
            </svg>
          )
      }
      {/* <img src={`${icon}`} alt={icon} style={{ height: '24px' }} /> */}
    </div>
  );

  return (
    <div>
      <ItemsCarousel
        requestToChangeActive={setActiveItemIndex}
        activeItemIndex={activeItemIndex}
        numberOfCards={getCountContent()}
        gutter={10}
        leftChevron={ButtonNavigation('left')}
        rightChevron={ButtonNavigation('right')}
        outsideChevron
        chevronWidth={chevronWidth}
        infiniteLoop
      >
        {
          data.map((v) => (
            <img className="m-1 p-1 cursor-pointer hover:shadow" src={v ==="" ? "/images/dummy_product.png" : v} alt="" width="100" onClick={() => onClick(v)}/>
          ))
        }
      </ItemsCarousel>
    <style jsx>{style}</style>
    </div>
  );
};

SaleItems.propTypes = {
  data: PropTypes.object
};

export default SaleItems;
