import React, { useState, useCallback } from 'react'
import ReactDOM from 'react-dom'
import Cropper from 'react-easy-crop'
import Slider from '@material-ui/core/Slider'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import { withStyles } from '@material-ui/core/styles'
// import ImgDialog from './ImgDialog'
import getCroppedImg from './cropImage'
import { styles } from './styles'

const dogImg =
  'https://img.huffingtonpost.com/asset/5ab4d4ac2000007d06eb2c56.jpeg?cache=sih0jwle4e&ops=1910_1000'

const CropPicker = ({onClosed, onSubmit, classes, ratio, image, onCrop, onCropped}) => {
  const [crop, setCrop] = useState({ x: 0, y: 0 })
  const [rotation, setRotation] = useState(0)
  const [zoom, setZoom] = useState(1)
  const [croppedAreaPixels, setCroppedAreaPixels] = useState(null)
  const [croppedImage, setCroppedImage] = useState(null)

  const onCropComplete = useCallback((croppedArea, croppedAreaPixels) => {
    setCroppedAreaPixels(croppedAreaPixels)
  }, [])

  const showCroppedImage = useCallback(async () => {
    onCrop()
    try {
      const croppedImage = await getCroppedImg(
        image,
        croppedAreaPixels,
        rotation
      )
      // console.log('donee', { croppedImage })
      // setCroppedImage(croppedImage)
      onSubmit(croppedImage)
    } catch (e) {
      console.error(e)
    }
    onCropped()
  }, [croppedAreaPixels, rotation])

  const onClose = useCallback(() => {
    setCroppedImage(null)
  }, [])

  return (
    <div className="bg-white p-4 shadow rounded relative" style={{ width: '768px' }}>
      <div className="w-full flex justify-end">
        <img className="cursor-pointer" src="/images/ic_close_red.png" alt="" onClick={onClosed}/>
      </div>
      <div className="w-full relative h-64 mt-4">
        <Cropper
          image={image}
          crop={crop}
          rotation={rotation}
          zoom={zoom}
          aspect={ratio}
          onCropChange={setCrop}
          onRotationChange={setRotation}
          onCropComplete={onCropComplete}
          onZoomChange={setZoom}
        />
      </div>
      <div className="w-full">
        <div className={classes.controls}>
          <div className="w-full flex flex-wrap">
            <div className="w-full">
              <div className={classes.sliderContainer}>
                <div className="mr-4">
                  <Typography
                    variant="overline"
                    classes={{ root: classes.sliderLabel }}
                  >
                    Zoom
                  </Typography>
                </div>
                <Slider
                  value={zoom}
                  min={1}
                  max={3}
                  step={0.1}
                  aria-labelledby="Zoom"
                  // @ts-ignore
                  classes={{ container: classes.slider }}
                  // @ts-ignore
                  onChange={(e, zoom) => setZoom(zoom)}
                />
              </div>
            </div>
            <div className="w-full">
              <div className={classes.sliderContainer}>
                <div className="mr-4">
                  <Typography
                    variant="overline"
                    classes={{ root: classes.sliderLabel }}
                  >
                    Rotation
                  </Typography>  
                </div>
                <Slider
                  value={rotation}
                  min={0}
                  max={360}
                  step={1}
                  aria-labelledby="Rotation"
                  // @ts-ignore
                  classes={{ container: classes.slider }}
                  // @ts-ignore
                  onChange={(e, rotation) => setRotation(rotation)}
                />
              </div>
            </div>
            <div className="w-full">
              <Button
                onClick={showCroppedImage}
                variant="contained"
                color="primary"
                classes={{ root: classes.cropButton }}
              >
                Show Result
              </Button>
            </div>
          </div>
        </div>
      </div>
      {/* <ImgDialog img={croppedImage} onClose={onClose} /> */}
    </div>
  )
}

// @ts-ignore
export default withStyles(styles)(CropPicker)