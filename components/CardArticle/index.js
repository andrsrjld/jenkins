import React, { useState } from 'react';
import Link from 'next/link';
import PropTypes from 'prop-types';
import style from './style';
import { formatNumber } from '~/util/helper';
import moment from 'moment';


const CardTodayDeals = ({ data }) => {
  const {
    image_url, title, publish_date, author_name, content, likes, slug, highlight, created_at
  } = data;
  return (
    <Link href={`/article/${slug}`}>
      <div className="container flex xs:flex-wrap sm:flex-wrap">
        <div className="xs:w-full sm:w-full w-64">
          <img src={image_url} alt="" className="w-full"/>
        </div>
        <div className="xs:w-full sm:w-full w-full">
          <div className="c-content xs:flex sm:flex xs:flex-wrap sm:flex-wrap xs:items-center sm:items-center">
            <div className="title xs:w-full sm:w-full">
              { title }
            </div>
            <div className="merchant xs:w-full sm:w-full">
              {`${moment(created_at).format("DD MMMM YYYY")} / Oleh: `}
              <span style={{ color: '#010101' }}>{author_name}</span>
            </div>
            <div className="desc xs:w-full sm:w-full">
              {highlight.toString().split(" ").splice(0, 30).join(" ")}
            </div>
          </div>
        </div>
        <style jsx>{style}</style>
      </div>
    </Link>
  );
};

CardTodayDeals.propTypes = {
  data: PropTypes.object
};

export default CardTodayDeals;
