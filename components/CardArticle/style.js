import css from 'styled-jsx/css';

export default css`
.container {
  cursor: pointer;
  width: 100%;
  padding: 10px;
  background: #FFFFFF;
  border-radius: 4px;
  margin-bottom: 16px;
  margin-top: 8px;
  display: flex;
}

.c-content {
  width: 100%;
  padding: 0px 0px 0px 24px;
}

.title {
  font-weight: bold;
  font-size: 24px;
  letter-spacing: 0.05em;
  color: #2E2E2E;
}

.merchant {
  margin-top: 14px;
  font-weight: 500;
  font-size: 16px;
  letter-spacing: 0.05em;
  color: #A5A5A5;
}

.desc {
  margin-top: 14px;
  font-weight: 500;
  font-size: 18px;
  letter-spacing: 0.05em;
  color: #696969;
}

@media (min-width: 768px) and (max-width: 1024px) {
}

@media (min-width: 320px) and (max-width: 767px) {
  .container{
    max-width: 100%;
  }
  .title, .merchant{
    margin-top: 5px;
  }
  .c-content{
    padding:0;
  }
}
`;
