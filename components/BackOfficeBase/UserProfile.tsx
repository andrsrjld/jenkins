
const UserProfile = (props: { name : string, status: boolean }) => {
  return (
    <div className="w-full flex px-2 items-center justify-start flex-row">
      <div>
        <img src="/images/ic_profile_rounded.png" alt="" />
      </div>
      <div className="w-full pl-2">
        <div className="w-full font-medium text-md text-gray-700">
          { props.name }
        </div>
        <div className="w-full text-xs text-gray-500 flex items-center">
          <img className="mr-1" src="/images/ic_online.png" alt="" />
          { props.status ? 'Online' : 'Offline' }
        </div>
      </div>
    </div>
  )
}

export default UserProfile;