import Link from 'next/link'

type TBreadcumb = {
  title: string,
  link: string
}

type TProps = {
  title: string,
  breadcumb: Array<TBreadcumb>
}

const ContentHeader = (props : TProps) => {
  return (
    <div className="w-full flex">
      <div className="w-full font-medium text-text-primary text-xl">
        {props.title}
      </div>
      <div className="w-full flex items-center justify-end">
        {
          props.breadcumb.map((v, i) => (
            <div className="flex items-center">
              <Link href={v.link}>
                <div className={`text-sm ${i != props.breadcumb.length - 1 ? 'text-text-primary' : 'text-primary'}`}>
                  {v.title}
                </div>
              </Link>
              {
                i != props.breadcumb.length - 1 && (
                  <img className="mx-2" src="/images/ic_breadcumb_right.png" alt="" />
                )
              }
            </div>
          ))
        }
      </div>
    </div>
  )
}

export default ContentHeader;