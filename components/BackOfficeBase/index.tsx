import Head from 'next/head'
import UserProfile from './UserProfile'
import SideMenus from './SideMenus'
import Header from './Header'
import '~/styles/index.css'

const index = (props: {children : any}) => {
  return (
    <div className="w-full h-screen flex">

      <Head>
        <title>Bapera</title>
        <link rel="icon" href="/favicon.ico" />
        <meta name="viewport" content="width=1024"></meta>
      </Head>

      {/* SIDENAV */}
      <div className="max-w-sm overflow-y-auto max-h-screen">
        <img className="py-4 px-12 mt-5 h-16" src="/images/logo_bapera.png" alt=""/>
        <div className="mt-6">
          <UserProfile name="Bapera Official" status />
        </div>

        <div className="mt-4">
          <SideMenus />
        </div>
      </div>

      {/* CONTENT */}
      <div className="w-full bg-gray-200 overflow-y-auto">
        {/* HEADER */}
        <div>
          <Header />
        </div>

        {/* CONTENT */}
        <div className="p-5 flex">
          {props.children}
        </div>
      </div>
    </div>
  )
}

export default index