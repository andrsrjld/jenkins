import { useState } from 'react'
import Router, { useRouter } from 'next/router'
import Link from 'next/link'
import { motion } from "framer-motion"

type TMenu = {
  image: string, 
  imageActive: string, 
  title: string,
  url: string,
  isOpen: boolean,
  subMenus: Array<TMenu>
}

const menu: TMenu[] = [
  {
    image: '/images/ic_dashboard.png',
    imageActive: '/images/ic_dashboard_active.png',
    title: 'Dashboard',
    url: '/backoffice/dashboard',
    subMenus: [],
    isOpen: false
  },
  {
    image: '/images/ic_sales.png',
    imageActive: '/images/ic_sales_active.png',
    title: 'Sales',
    url: '/backoffice/sales',
    subMenus: [],
    isOpen: false
  },
  
  {
    image: '/images/ic_catalog.png',
    imageActive: '/images/ic_catalog_active.png',
    title: 'Catalog',
    url: '',
    isOpen: false,
    subMenus: [
      {
        image: '',
        imageActive: '',
        title: 'Add Product',
        url: '/backoffice/products/add',
        isOpen: false,
        subMenus: []
      },
      {
        image: '',
        imageActive: '',
        title: 'Manage Products',
        url: '/backoffice/products',
        isOpen: false,
        subMenus: []
      },
    ]

  },
  
  {
    image: '/images/ic_sales.png',
    imageActive: '/images/ic_sales_active.png',
    title: 'Report',
    url: '',
    isOpen: false,
    subMenus: [
      {
        image: '',
        imageActive: '',
        title: 'Transaction',
        url: '/backoffice/report/transaction',
        isOpen: false,
        subMenus: []
      }
    ]
  },
  {
    image: '/images/ic_settings.png',
    imageActive: '/images/ic_settings_active.png',
    title: `Seller's Page`,
    url: '/backoffice/settings',
    subMenus: [],
    isOpen: false
  },
]

const variants = {
  open: { opacity: 1, y: 0, height: "auto" },
  closed: { opacity: 0, y: "-50%", height: 0 },
}

const variantsArrow = {
  open: { rotate: 0 },
  closed: { rotate: 180 },
}

const Menu = (props : {menu : TMenu, router: any, cbClick: any }) => {
  const {menu, router, cbClick} = props
  return (
    <div>
      <div onClick={() => cbClick(menu.title)}>
        <div onClick={() => menu.url ? Router.push(menu.url) : null} >
          <div className={`px-3 py-4 flex items-center font-medium cursor-pointer ${router.pathname === menu.url ? 'text-primary' : 'text-gray-900'}`}>
            <div className="w-full flex">
              <img className="mr-3" src={router.pathname === menu.url ? menu.imageActive : menu.image} />
              { menu.title }
            </div>
            {
              menu.subMenus.length > 0 && (
                <motion.nav
                  animate={menu.isOpen ? "open" : "closed"}
                  variants={variantsArrow}
                >
                  <img className="mr-3" src={'/images/ic_arrow_down.png'} />
                </motion.nav>
              )
            }
          </div>
        </div>
      </div>
      <motion.nav
          animate={menu.isOpen ? "open" : "closed"}
          variants={variants}
        >
        { menu.isOpen && menu.subMenus.map((submenu, i) => (
            <Link href={submenu.url}>
              <div className={`px-3 py-4 flex items-center font-medium cursor-pointer ${i%2 != 0 ? 'bg-white' : 'bg-gray-50'} ${router.pathname === submenu.url ? 'text-primary' : 'text-gray-900'}`}>
                <div className="ml-8">
                  { submenu.title }
                </div>
              </div>
            </Link>
          ))
        }
      </motion.nav>
    </div>
  )
}

const SideMenus = () => {
  const router = useRouter()
  const [menus, setMenus] = useState(menu.map(v => ({...v, isOpen: router.pathname == v.url || v.subMenus.filter(k => k.url == router.pathname).length > 0 ? true : false})))

  const handleChange = (title) => {
    setMenus(v => v.map(v => ({...v, isOpen: v.title == title ? !v.isOpen : v.isOpen })))
  }

  return (
    <div className="divide-y divide-gray-400">
      {
        menus.map(v => {
          return (
            <Menu key={v.title} menu={v} router={router} cbClick={handleChange}/>
          )
        })
      }
    </div>
  )
}

export default SideMenus;
