import { useState } from 'react'
import { connect } from 'react-redux'
import Link from 'next/link'
import { deauthenticate } from 'redux/actions/authActions'

const Header = (props) => {
  const [isOpen, setOpen] = useState(false)

  return (
    <div className="bg-primary h-16 flex items-center px-5">
      <img className="mx-2" src="/images/ic_hamburger_white.png" alt="" />
      <div className="w-full flex justify-end items-center">
        <Link href="/">
          <img className="pointer mx-3" src="/images/ic_home_white.png" alt=""/>
        </Link>
        {/* <img className="mx-3" src="/images/ic_bell_white.png" alt=""/> */}
        <div className="relative">
          <div className="flex items-center" onClick={() => setOpen(v => !v)}>
            <img className="ml-3" src="/images/ic_person_white.png" alt=""/>
            <img src="/images/ic_triangle_down_white.png" alt=""/>
          </div>
          {/* dropdown */}
          {
            isOpen && (
              <div className="divide-y-2 divide-gray-100 absolute bg-white right-0 mt-2 shadow-md w-56 text-text-primary rounded z-10">
                <Link href="/account">
                  <div className="w-full px-3 py-2 flex items-center cursor-pointer hover:bg-gray-100">
                    <img className="mr-3" src="/images/ic_account.png" alt=""/>
                    Akun
                  </div>
                </Link>
                <Link href="/backoffice/dashboard">
                  <div className="w-full px-3 py-2 flex items-center cursor-pointer hover:bg-gray-100">
                    <img className="mr-3" src="/images/ic_dashboard.png" alt=""/>
                    Dashboard
                  </div>
                </Link>
                <div className="w-full px-3 py-2 flex items-center cursor-pointer hover:bg-gray-100" onClick={props.deauthenticate}>
                  <img className="mr-3" src="/images/ic_logout.png" alt=""/>
                    Keluar
                </div>
              </div>
            )
          }
        </div>
      </div>
    </div>
  )
} 

const mapStateToProps = (state) => ({
  isAuthenticated: !!state.authentication.token,
  authentication: state.authentication
});

export default connect(
  mapStateToProps,
  {
    deauthenticate
  }
)(Header);
