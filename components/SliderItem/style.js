import css from 'styled-jsx/css';

export default css`
.content {
	position: absolute;
	bottom: 10%;
	left: 10%;
	text-align: left;
	color: #696969;
}
.title {
	font-size: 24px;
	font-weight: 500;
	border-left: solid 5px #9B9B9B;
	padding-left: 16px;
}
.header {
	margin-top: 20px;
	font-weight: 500;
	font-size: 48px;
}
.description {
	margin-top: 20px;
	font-size: 22px;
}
.btn-action {
	margin-top: 20px;
	width: auto;
	font-weight: 500;
	font-size: 18px;
}

@media (min-width: 768px) and (max-width: 1024px) {
}

@media (min-width: 320px) and (max-width: 767px) {
	.title {
		margin-top: 1rem;
		font-size: 13px;
		border-left: solid 2px #9B9B9B;
		padding-left: 5px;
	}
	.header {
		font-size: 10px;
	}
	.description {
		margin-top: 5px;
		font-size: 10px;
	}
	.btn-action {
		margin-top: 5px;
		font-size: 10px;
		padding: 5px 10px;
	}
}
`;
