import React from 'react';
import PropTypes from 'prop-types';
import style from './style';
import { indexOf, split } from 'lodash';
import ImageLoading from '~/components/ImageLoading';

const SliderItem = ({ data }) => {
  const {
    image_url, title, description, action, link
  } = data;
  const splitedTitle = title.split(" ");
  const lastWordTitle = splitedTitle[splitedTitle.length - 1];
  const indexLastWord = indexOf(lastWordTitle);
  splitedTitle.splice(indexLastWord, 1);
  const titleResult = splitedTitle.join(' ');

  return (
    <div className="">
      <div className="w-full">
        <div style={{ maxHeight: '768px' }}>
          <img className={`w-full object-cover`} src={image_url} alt={title} />
        </div>
        {/* <img className="w-full object-cover" style={{ maxHeight: '768px' }} src={`${image_url}`} alt={title} /> */}
        {/* <div className="content">
          <div className="title">
            {titleResult}
            {' '}
            <span style={{ color: '#010101' }}>{lastWordTitle}</span>
          </div>
          <div
            className="description"
            dangerouslySetInnerHTML={{ __html: description }}
          />
          <a href={link}><button className="btn-action btn-primary">{action !== "" ? action : "Jelajah"}</button></a>
        </div> */}
      </div>

      <style jsx>
        {style}
      </style>
    </div>
  );
};

SliderItem.propTypes = {
  data: PropTypes.object
};

export default SliderItem;
