import { AnyNaptrRecord } from 'dns'
import React, { useState, useEffect } from 'react'
import ReactPlaceholder from 'react-placeholder';
import 'react-placeholder/lib/reactPlaceholder.css';

interface Props {
  src: string,
  alt: string,
  ratio: any,
  className: string
}

const ImageLoading = (props : Props) => {
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    window.addEventListener('load', function () {
      console.log('finish')
      setLoading(false);
    })
  }, [loading])

  return (
    <>
      {/* {
        !loading ?
          <ReactPlaceholder ready={!loading} showLoadingAnimation>
          <div className={`w-full bg-white`} style={{ paddingTop: `${props.ratio * 100}%` }} />
          </ReactPlaceholder>
        : <></>
      } */}
      {/* <img className={props.className} src={props.src} alt={props.alt} onLoad={() => setLoading(true)}/> */}
      <ReactPlaceholder type={'rect'} ready={!loading} showLoadingAnimation={true} rows={1} style={{height: '250px'}}>
        <img className={`${props.className}`} src={props.src} alt={props.alt} />
      </ReactPlaceholder>
    </>
  )
}

export default ImageLoading