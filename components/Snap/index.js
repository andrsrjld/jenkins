import React, {useState} from 'react';
import { formatNumber, getBankImage, getAtmGuide, getInternetGuide } from '~/util/helper';
import moment from 'moment';

import {successNotification, errorNotification} from 'util/toastr-notif';

const copyToClipboard = str => {
  const el = document.createElement('textarea');
  el.value = str;
  el.setAttribute('readonly', '');
  el.style.position = 'absolute';
  el.style.left = '-9999px';
  document.body.appendChild(el);
  el.select();
  document.execCommand('copy');
  document.body.removeChild(el);
  successNotification("Berhasil menyalin nomor VA")
};

const index = ({Tpayment, simulation}) => {
  const [openATM, setOpenATM] = useState(false)
  const [openInternet, setOpenInternet] = useState(false)
  
  return (
  <div>
      <div className="c">
        <div className="c-card">
          <div className="title">
            Transaksi Berhasil
          </div>
          <div className="subtitle">
           {/* Transaksi masih dapat dibatalk {moment(Tpayment.max_paid_date).format('DD MMMM YYYY HH:mm')} WIB */}
            <br />
            {/* dengan rincian sebagai berikut. */}
          </div>
          <div className="divider" style={{ margin: '2rem 3rem' }} />
          <div className="c-code flex flex-wrap text-center">
            <div className="item">
              <img src='/images/ic_bapera.png' alt="" style={{width: "105px", margin: "0 auto 20px"}} />
            </div>
            {/* <div className="item">
              <div className="desc">Kode Unik Pembayaran</div>
              <div className="subtitle" style={{ marginTop: '1.5rem', color: '#010101' }}><b>{Tpayment.payment_method == 'echannel' ? `${Tpayment.bill_key}` : Tpayment.va_number}</b></div>
            </div>
            <div className="item" style={{ display: 'flex' }}>
              <div className="desc cursor-pointer" style={{ color: '#FF8242', alignSelf: 'flex-end', margin: "0 auto", textAlign: 'center' }} onClick={() => copyToClipboard(Tpayment.payment_method == 'echannel' ? `${Tpayment.bill_key}` : Tpayment.va_number)}>SALIN</div>
            </div> */}
          </div>
          {/* <div className="divider" style={{ margin: '2rem 3rem' }} /> */}
          <div className="c-code">
            <div className="item">
              <div className="desc">Jumlah terbayar</div>
              <div className="subtitle" style={{ marginTop: '1.5rem', color: '#FF8242' }}><b>{`Rp ${formatNumber(Tpayment.total_paid)}`}</b></div>

              <div className="title" style={{ marginTop: '16px' }}>
                Detail Pesanan
              </div>
              <div style={{ marginTop: '24px', display: 'flex' }}>
                <div className="w-100">
                  <div className="detail-order-wrapper">
                    <div className="flex xs:flex-wrap sm:flex-wrap">
                      <div className="td-small w-1/3 xs:w-1/2 sm:w-1/2">Nomor Pesanan : </div>
                      <div className="td-small w-2/3 xs:w-1/2 sm:w-1/2">{Tpayment?.payment_number}</div>
                    </div>
                    <div className="flex xs:flex-wrap sm:flex-wrap">
                      <div className="td-small w-1/3 xs:w-1/2 sm:w-1/2">Status Pesanan :</div>
                      <div className="td-small w-2/3 xs:w-1/2 sm:w-1/2">Menunggu Konfirmasi</div>
                    </div>
                    <div className="flex xs:flex-wrap sm:flex-wrap">
                      <div className="td-small w-1/3 xs:w-1/2 sm:w-1/2">Pembayaran :</div>
                      <div className="td-small w-2/3 xs:w-1/2 sm:w-1/2">{
                        Tpayment?.payment_method === 'full-payment' ? 'Saldo Belanja'
                          : Tpayment?.payment_method === '3-cicilan' ? `3x Cicilan (${simulation['3x']})`
                            : Tpayment?.payment_method === '6-cicilan' ? `6x Cicilan (${simulation['6x']})`
                              : Tpayment?.payment_method === '12-cicilan' ? `12x Cicilan (${simulation['12x']})`
                                : ''
                       }</div>
                    </div>
                  </div>
                </div>
              </div>
              <div style={{ marginTop: '3rem' }}>
                <div className="table">
                  <div className="flex xs:flex-wrap sm:flex-wrap xs:hidden sm:hidden table-header">
                    <div className="text-white w-1/4">Merk</div>
                    <div className="text-white w-1/4">Nama Produk</div>
                    <div className="text-white w-1/4">Harga</div>
                    <div className="text-white w-1/4">Jumlah</div>
                    <div className="text-white w-1/4">Subtotal</div>
                  </div>
                  {
                Tpayment.order_sellers.map(detailData => (
                  <div className="order-product-table">
                    {
                      detailData && detailData.order_items ? detailData.order_items.map(v => (
                        <div className="flex xs:flex-wrap sm:flex-wrap order-detail-item" key={v.product_name}>
                          <div className="td w-1/4 xs:w-1/2 sm:w-1/2">{detailData.seller_name}</div>
                          <div className="td w-1/4 xs:w-1/2 sm:w-1/2">{v.product_name}</div>
                          <div className="td w-1/4 xs:w-1/2 sm:w-1/2" style={{ whiteSpace: 'nowrap' }}>Rp {formatNumber(v.price)}</div>
                          <div className="td w-1/4 xs:w-1/2 sm:w-1/2" style={{ whiteSpace: 'nowrap' }}>Jumlah: {v.quantity}</div>
                          <div className="td w-1/4 xs:w-1/2 sm:w-1/2" style={{ whiteSpace: 'nowrap' }}>Rp {formatNumber(v.subtotal)}</div>
                        </div>
                      )) : ""
                    }
                  </div>

                ))
              }
                </div>
                <div className="divider" />
              </div>
              <div style={{ display: 'flex', justifyContent: 'flex-end', marginTop: '1rem' }}>
                <div>
                  <table>
                    <tr>
                      <td className="td-med" style={{ textAlign: 'right' }}>Total Potongan Produk</td>
                      <td className="td-med" style={{ textAlign: 'right' }}>- Rp {formatNumber(Tpayment?.total_discount)}</td>
                    </tr>
                    <tr>
                      <td className="td-med" style={{ textAlign: 'right' }}>Subtotal</td>
                      <td className="td-med" style={{ textAlign: 'right' }}>Rp {formatNumber(Tpayment?.total_price)}</td>
                    </tr>
                    <tr>
                      <td className="td-med" style={{ textAlign: 'right' }}>Pengiriman</td>
                      <td className="td-med" style={{ textAlign: 'right' }}>Rp {formatNumber(Tpayment?.total_shipping)}</td>
                    </tr>
                    <tr>
                      <td className="td-med" style={{ textAlign: 'right' }}><b>Grand Total</b></td>
                      <td className="td-med" style={{ textAlign: 'right', color: '#FF8242' }}>Rp {formatNumber(Tpayment?.total_paid)}</td>
                    </tr>
                  </table>
                </div>
              </div>

              <div className="divider" style={{ margin: '1rem', width: 'auto' }} />

              <div className="desc" style={{ marginTop: '1.5rem' }}>
                Silahkan tekan tombol dibawah ini jika ingin melihat riwayat pembelian
                <br />
                
              </div>
              <div className="desc" style={{ marginTop: '3.5rem' }}>
                <a className="btn btn-primary" href="/transaction-history">Pesanan Saya</a>
              </div>
              {/* <div className="c-row bg-row" style={{ marginTop: '3rem' }}>
                <div className="title">
                  Cara Membayar
                </div>
              </div>
              <div className="c-row cursor-pointer" onClick={() => setOpenATM(!openATM)}>
                <div className="title">
                  ATM
                </div>
                <img src="/images/ic_arrow_up.png" alt="" />
              </div>
              {
                openATM && (
                  <div className="c-row">
                    <ol className="list-decimal">
                      {
                        getAtmGuide(Tpayment.payment_method).map(v => (
                          <li>{v}</li>
                        ))
                      }
                    </ol>
                  </div>
                )
              }
              <div className="c-row cursor-pointer" onClick={() => setOpenInternet(!openInternet)}>
                <div className="title">
                  Internet Banking
                </div>
                <img src="/images/ic_arrow_up.png" alt="" />
              </div>
              {
                openInternet && (
                  <div className="c-row">
                    <ol className="list-decimal">
                      {
                        getInternetGuide(Tpayment.payment_method).map(v => (
                          <li>{v}</li>
                        ))
                      }
                    </ol>
                  </div>
                )
              } */}
            </div>
          </div>
        </div>
      </div>
    <style jsx>
      {
        `
        .c {
          display: flex;
          padding: 2rem 0px;
          justify-content: center;
          flex-wrap: wrap;
        }

        .c-card {
          background: #FFFFFF;
          box-shadow: 0px 1px 12px rgba(0, 0, 0, 0.25);
          border-radius: 10px;
          padding: 3rem;
          margin-top: 2rem;
        }

        .title {
          font-weight: bold;
          font-size: 24px;
          line-height: 23px;
          letter-spacing: 0.05em;
          color: #696969;
          text-align: center;
        }

        .subtitle {
          margin-top: 1rem;
          font-size: 24px;
          line-height: 29px;
          text-align: center;
          letter-spacing: 0.05em;
          color: #696969;
          text-align: center;
        }

        .divider {
          height: 1px;
          background: #CCCCCC;
        }

        .c-code {
          display: flex;
          margin: 1rem 3rem;
        }

        .c-code .item {
          width: 100%;
        }

        .desc {
          font-size: 20px;
          line-height: 24px;
          text-align: center;
          letter-spacing: 0.05em;
          color: #696969;          
        }

        .bg-row {
          background: #F5F5F5;
          box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.06);
          border-radius: 5px;
        }

        .c-row {
          display: flex;
          justify-content: space-between;
          align-items: center;
          padding: 1.2rem 2.5rem;
          border-bottom: 1px solid #CCCCCC;
        }
        .c-row .title{
          font-weight: 500;
          font-size: 20px;
          line-height: 24px;
          letter-spacing: 0.05em;
          color: #696969;
        }
          .table-header div{
            background:#010101;
            padding:10px 15px;
          }

        `
      }
    </style>
  </div>
)};

export default index;
