import { connect } from 'react-redux'
import Link from 'next/link'
import ProfileDropdown from 'components/ProfileDropdown'

const HeaderMenu = (props: any) => {
  if (!props.isAuthenticated) {
    return (
      <div className="w-full flex items-center justify-end">
        <Link href="/signin">
          <button type="button" className="rounded-lg border-2 border-text-primary cursor-pointer px-4 py-2 text-lg text-text-primary" style={{ marginRight: '16px' }}>Masuk</button>
        </Link>
        {/* <Link href="/cart">
          <img className="pointer" src="/images/ic_cart.png" alt="" style={{ margin: '0px 8px' }} />
        </Link> */}
      </div>
    )
  } else {
    return (
      <div className="w-full flex items-center justify-end">
        <Link href="/wishlist">
          <img className="pointer" src="/images/ic_love.png" alt="" style={{ margin: '0px 8px' }} />
        </Link>
        <Link href="/cart">
          <div className="cart-wrapper">
            <img className="pointer" src="/images/ic_cart.png" alt="" style={{ margin: '0px 8px' }} />
            <label>{props.cart ? props.cart.reduce((a, b) => {return a + b.carts.length}, 0) : 0}</label>
          </div>
        </Link>
        <ProfileDropdown token={props.authentication.token}/>
        <style jsx>
        {`
          .cart-wrapper{
            position:relative;
          }
          .cart-wrapper label{
            position:absolute;
            top:-13px;
            right:-6px;
            background:#010101;
            border-radius:30px;
            -webkit-border-radius:30px;
            -moz-border-radius:30px;
            -ms-border-radius:30px;
            -o-border-radius:30px;
            color:#fff;
            width:25px;
            text-align:center;
          }
        `}
        </style>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  isAuthenticated: !!state.authentication.token,
  authentication: state.authentication
});

export default connect(
  mapStateToProps
)(HeaderMenu);