import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import Link from 'next/link';
import BaseLayout from '~/components/BaseLayout';
import Modal from '~/components/Modal';
import ShippingModal from '~/components/ShippingModal';
import { getAddresses, insertAddress, deleteAddress, getAddress, updateAddress } from '~/data/api/address';
import { getCart } from '~/data/api/cart';
import { formatNumber } from '~/util/helper';
import { getShipping } from '~/data/api/shipping';
import shippingStyle from './shipping-style';
import ReactPlaceholder from 'react-placeholder';

// Toastr Notification
import {successNotification, errorNotification} from '~/util/toastr-notif';

// Add Address Modal
import Payment from '~/components/Payment';
import Select from 'react-select/async';
import { apiSearchAddress } from '~/util/ServiceHelper';

// Axios API Request
import axiosSetup from '../../services/api';
import { format } from 'util';

const index = ({ token, addresses, cart, handleNext, TselectedAddress, Tsellers, Tshippings, serviceCharge, event }) => {
  const [modalAddress, setModalAddress] = useState(false);
  const [addressData, setAddressData] = useState(addresses.data);
  const [selectedAddress, setSelectedAddress] = useState(TselectedAddress.data);
  const [cartData, setCartData] = useState(cart);
  const [sellers, setSellers] = useState(Tsellers.data);
  const [shippingData, setShippingData] = useState(Tshippings.data);
  const [openAddressModal, setOpenAddressModal] = useState(selectedAddress ? false : true);
  const [addressAction, setAddressAction] = useState("insert");
  const [modalShipping, setModalShipping] = useState(false);
  const [openedShipping, setOpenedShipping] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const tempSeller = [];
    cart.forEach((v) => {
      if (tempSeller.filter((k) => v.seller_id === k.seller_id).length === 0) {
        // Get Product data @seller
        const product = [];
        v.carts.map(item => {
          if(item.checkout){
            product.push({
              product_id: item.product_id,
              variant_id: item.variant_id,
              image_url: item.image_url,
              weight: item.weight ? item.weight : 0,
              product_name: item.product_name,
              quantity: item.quantity,
              product_prices: item.product_prices,
              product_discount: item.product_discount,
              total_prices: item.total_prices,
              district_id: item.district_id,
              notes: item.notes,
              variant_name: item.variant_name
            });
          }
        });
        if(product.length > 0){
          tempSeller.push({
            lat_long: v.lat_long,
            seller_name: v.seller_name,
            seller_address: v.seller_address,
            district_id: product[0].district_id,
            seller_id: parseInt(v.seller_id),
            shipping: [],
            shippingPrice: 0,
            selectedShipping: '',
            weight: product.reduce((a,b) => a + (b.weight * b.quantity), 0),
            products: product
          });
        }
      }
    });
    setSellers([...tempSeller])
  }, []);

  useEffect(() => {
    TselectedAddress.callback(selectedAddress)
    if (sellers.length > 0) {
      console.log(sellers);
      setSellers([...sellers.map(v => {
        // console.log(window.document.querySelector(`input[name="${v.seller_id}"]`).checked = false)
        return ({...v, selectedShipping:'', shippingPrice: 0});
      })])
    }
  }, [selectedAddress])

  useEffect(() => {
    if(selectedAddress) {
      if (sellers.length > 0) {
        const proms = [];
        sellers.forEach(async (v) => {
          if(v.products.length > 0){
            const body = {
              seller_id: v.seller_id,
              origin_lat_long: v.lat_long,
              destination_lat_long: `${selectedAddress ? selectedAddress.lat_long : '0,0'}`,
              origin: `${v.district_id}`,
              origin_type: 'subdistrict',
              destination: `${selectedAddress ? selectedAddress.district_id : 0}`,
              destination_type: 'subdistrict',
              weight: v.weight,
              customer_address_id: `${selectedAddress ? Number(selectedAddress.id) : null}`,
              product_amount: v.products.reduce((a,b) => a + b.total_prices, 0)
            };
            // const api = axiosSetup(token)
            // const listShipping = await api.get("/restricted/shipping", {
            //   body: body
            // });
            // console.log(listShipping);
            proms.push(getShipping(token, body));
          }
        });
        let temp = [...sellers];
        Promise.all(proms)
          .then((res) => {
            res.forEach((v,i) => {
              temp[i].shipping = v.data;
            });
            setShippingData(temp)
          })
      }
    }
    setLoading(false);
  }, [sellers]);

  useEffect(() => {
    console.log(shippingData);
    Tshippings.callback(shippingData)
  }, [shippingData])

  useEffect(() => {
    addresses.callback(addressData)
  }, [addressData])

  const handleChangeShipping= (sellerId, title, price) => (e) => {
    setLoading(true);
    setShippingData(shippingData.map(v => ({
      ...v,
      selectedShipping: v.seller_id === parseInt(sellerId) ? title : v.selectedShipping,
      shippingPrice: v.seller_id === parseInt(sellerId) ? parseFloat(price) : v.shippingPrice})))
    setModalShipping(false);
    setLoading(false);
  }

  const handleChangeSelectedAddress = (index) => {
    setAddressData(addressData.map((v,i) => ({...v, isChecked: index === i})))
  }

  const handleSubmitChangeAddress = () => {
    setSelectedAddress(addressData.find(v => v.isChecked))
    setModalAddress(false)
  }

  const reloadAddressData = async () => {
    const addresses = await getAddresses(token);
    setAddressData(addresses.data);
    setSelectedAddress(addresses.data[addresses.data.length - 1]);
  }

  const changeAddressAction = () => {
    setAddressAction("update");
    setOpenAddressModal(true);
  }

  const closeModal = () => {
    setOpenAddressModal(false);
  }

  const handleLoadShippingModal = (shipping, seller_id, selectedShipping) => (e) => {
    const listShipping = [];
    if(!shipping.gosend && !shipping.rajaongkir && !shipping.cod){
      listShipping.push(<h4>Tidak ada pilihan pengiriman untuk toko ini</h4>)
    } else {
      if(shipping.cod && event) {
        const listCOD = []
        listCOD.push(
          <div className="w-full flex shipping-item items-center" style={{ color: "#000" }}>
            <div className="radio-shipping">
              <label className="c-radio">
                <input type="radio" name="shipping" checked={selectedShipping === `COD`} value={seller_id} onChange={handleChangeShipping(seller_id, `COD, ${event.deskripsi}`, 0)} />
                <span className="checkmark" />
              </label>
            </div>
            <div className="w-4/6">
              {/* <label style={{fontSize: "16px"}}><span className="text-primary">Barang Diambil di tempat</span></label> */}
              <p style={{color: "#000"}}>{event.deskripsi}</p>
            </div>
            <div className="shipping-price w-2/6">
              Rp. 0
            </div>
            <style jsx>{shippingStyle}</style>
          </div>
        )
        listShipping.push(
          <div className="shipping-name-wrapper">
            <label style={{fontSize: "18px"}}>COD</label>
            {listCOD}
            <style jsx>{shippingStyle}</style>
          </div>
        )
      }
      if(shipping.gosend){
        const listGoSend = []
        listGoSend.push(
          <div className="w-full flex shipping-item items-center" style={shipping.gosend.SameDay.serviceable ? {color: "#000"} : {color: "#ddd"}}>
            <div className="radio-shipping">
              <label className="c-radio">
                <input type="radio" name="shipping" checked={selectedShipping === `Go-send - ${shipping.gosend.SameDay.shipment_method}`} value={seller_id} disabled={!shipping.gosend.SameDay.serviceable} onChange={handleChangeShipping(seller_id, `Go-send - ${shipping.gosend.SameDay.shipment_method}`, shipping.gosend.SameDay.price.total_price)} />
                <span className="checkmark" />
              </label>
            </div>
            <div className="w-4/6">
              <label style={{fontSize: "16px"}}>Go-send - {shipping.gosend.SameDay.shipment_method}</label>
              <p style={shipping.gosend.SameDay.serviceable ? {color: "#000"} : {color: "red"}}>{shipping.gosend.SameDay.shipment_method_description}</p>
            </div>
            <div className="shipping-price w-2/6">
              Rp. {formatNumber(shipping.gosend.SameDay.price.total_price)}
            </div>
            <style jsx>{shippingStyle}</style>
          </div>
        )
        listGoSend.push(
          <div className="w-full flex shipping-item items-center" style={shipping.gosend.Instant.serviceable ? {color: "#000"} : {color: "#ddd"}}>
            <div className="radio-shipping">
              <label className="c-radio">
                <input type="radio" name="shipping" checked={selectedShipping === `Go-send - ${shipping.gosend.Instant.shipment_method}`} value={seller_id} disabled={!shipping.gosend.SameDay.serviceable} onChange={handleChangeShipping(seller_id, `Go-send - ${shipping.gosend.Instant.shipment_method}`, shipping.gosend.Instant.price.total_price)} />
                <span className="checkmark" />
              </label>
            </div>
            <div className="w-4/6">
              <label style={{fontSize: "16px"}}>Go-send - {shipping.gosend.Instant.shipment_method}</label>
              <p style={shipping.gosend.Instant.serviceable ? {color: "#000"} : {color: "red"}}>{shipping.gosend.Instant.shipment_method_description}</p>
            </div>
            <div className="shipping-price w-2/6">
              Rp. {formatNumber(shipping.gosend.Instant.price.total_price)}
            </div>
            <style jsx>{shippingStyle}</style>
          </div>
        )
        listShipping.push(
          <div className="shipping-name-wrapper">
            <label style={{fontSize: "18px"}}>Go-Send</label>
            {listGoSend}
            <style jsx>{shippingStyle}</style>
          </div>
        )
      }
      if(shipping.rajaongkir){
        shipping.rajaongkir.map(k => {
          const listRajaongkir = [];
          k.costs.map(x => {
            x.cost.map(y => {
              listRajaongkir.push(
                <div className="w-full flex shipping-item items-center">
                <div className="radio-shipping">
                  <label className="c-radio">
                    <input type="radio" checked={selectedShipping === `${k.code.toUpperCase()} - ${x.service.toUpperCase()}`} name="shipping" value={seller_id} onChange={handleChangeShipping(seller_id, `${k.code.toUpperCase()} - ${x.service.toUpperCase()}`, y.value)} />
                    <span className="checkmark" />
                  </label>
                </div>
                <div className="w-4/6">
                <label style={{fontSize: "16px"}}>{k.code.toUpperCase()} - {x.service.toUpperCase()}</label>
                <p>{y.etd} Hari Kerja</p>
                </div>
                <div className="shipping-price w-2/6">
                  Rp. {formatNumber(y.value)}
                </div>
                <style jsx>{shippingStyle}</style>
              </div>
              )
            });
          });
          if(k.costs.length > 0){
            listShipping.push(
              <div className="shipping-name-wrapper">
                <label style={{fontSize: "18px"}}>{k.code.toUpperCase()}</label>
                {listRajaongkir}
                <style jsx>{shippingStyle}</style>
              </div>
            )
          }
        });
      }
    }
    setOpenedShipping(listShipping);
    setModalShipping(true);
  }

  return (
    <div>
        <div id="breadcumb" className="shipping-content">
          {/* <div className="breadcumb">
            Home &#x0226B;
            {' '}
            Keranjang &#x0226B;
            {' '}
            <span style={{ color: '#010101' }}>Shipping</span>
          </div> */}

          <div className="c">
            <div className="title" style={{ marginBottom: '3rem'}}>
              Metode
              {' '}
              <span style={{ color: '#010101' }}>Pengiriman</span>
            </div>
            {/* <ReactPlaceholder ready={!loading} showLoadingAnimation={true} rows={6} style={{marginTop: '30px'}}>test</ReactPlaceholder> */}
            <div className="c-content flex flex-wrap">
              <div className="w-3/4 xs:w-full sm:w-full">
              <ReactPlaceholder ready={!loading} showLoadingAnimation={true} rows={6} style={{marginTop: '30px'}}>
                <div className="c-1">
                  {/* <div className="title">
                    Alat Pengiriman
                    {' '}
                    <span style={{ color: '#010101' }}>Belanja</span>
                  </div> */}
                  
                  {
                      shippingData.map(v => {
                        return (
                          <div className="card">
                            <div className="table">
                              <div className="th seller-wrapper">
                                <h4>{v.seller_name}</h4>
                                {/* <p>{v.seller_address}</p> */}
                              </div>
                              <div className="flex flex-wrap product-wrapper">
                                <div className="w-full flex">
                                    <div className="w-full">
                                      {
                                        v.products.map(item => {
                                          return (
                                            <div className="product-item flex items-center xs:items-start sm:items-start">
                                              <div>
                                                <div className="product-image">
                                                  <img src={item.image_url} />
                                                </div>
                                              </div>
                                              <div className="w-full flex flex-wrap p-0">
                                                <div className="w-1/4 xs:w-full sm:w-full product-info">
                                                  <h4>{item.product_name}</h4>
                                                  {/* <label className="product-price">Rp. {formatNumber(item.total_prices)}</label> */}
                                                  <p className="product-total-weight">{item.quantity} barang ({item.quantity * item.weight} gr)</p>
                                                </div>
                                                <div className="w-1/4 xs:w-full sm:w-full product-info product-variant text-center xs:text-left sm:text-left md:text-center lg:text-center product-variant">
                                                  <h4>Varian : {item.variant_name}</h4>
                                                </div>
                                                <div className="w-1/4 xs:hidden sm:hidden product-info product-unit-price">
                                                  <span className="price-discount" style={item.product_discount <= 0 ? {display:"none"} : {display:"block"}}>{`Rp ${formatNumber(item.quantity * item.product_prices)}`}</span>
                                                  <h4>{item.quantity} x Rp. {formatNumber(item.product_prices - item.product_discount)}</h4>
                                                </div>
                                                <div className="w-1/4 xs:w-full sm:w-full product-info text-right" style={{color: "#010101"}}>
                                                  <h4>Rp. {formatNumber(item.total_prices)}</h4>
                                                </div>
                                              </div>
                                            </div>
                                          )
                                        })
                                      }
                                      
                                    </div>
                                    
                                </div>
                                <div className="w-full flex flex-wrap product-shipping">
                                  <div className="w-1/3">
                                      <p><b>Pengiriman</b></p>
                                      <p>{v.selectedShipping !== "" ? v.selectedShipping : (<b className="text-red-600">Belum ada pengiriman yang dipilih</b>)}</p>
                                  </div>
                                  <div className="w-1/3">
                                    <button type="button" className="border-2 border-red-600 px-3 py-2 rounded text-red-600 mt-2 hover:bg-gray-200" onClick={handleLoadShippingModal(v.shipping, v.seller_id, v.selectedShipping)}>{v.selectedShipping === "" ? "Pilih" : "Ganti"} Pengiriman</button>
                                  </div>
                                  <div className="w-1/3 text-right" style={{color: "#010101"}}>
                                    Rp. {formatNumber(v.shippingPrice)}
                                  </div>
                                </div>
                              </div>
                              <div className="th subtotal-wrapper flex">
                                <div className="w-1/2"><h4>Subtotal</h4></div>
                                <div className="w-1/2">
                                  <h4 className="subtotal" style={{color: "#010101"}}>
                                    Rp. {
                                      formatNumber(v.products.reduce((a, b) => a + b.total_prices, 0) + v.shippingPrice)
                                    }
                                  </h4>
                                </div>
                              </div>
                            </div>
                          </div>      
                        )
                      })
                    }
                  

                  <div style={{ textAlign: 'right', marginTop: '2rem' }}>
                    <button className="btn-primary btn-this xs:hidden sm:hidden" onClick={handleNext} disabled={shippingData.filter(v => v.selectedShipping == '').length > 0}>SELANJUTNYA</button>
                  </div>
                </div>
                </ReactPlaceholder>
            </div>
            <div className="w-1/4 xs:w-full sm:w-full">
              <div className="c-2">
                <div className="card-2">
                  <div className="summary-header">
                    Ringkasan
                  </div>

                  <ReactPlaceholder ready={!loading} showLoadingAnimation={true} rows={5} style={{margin: '10px 0'}}>
                    <div className="c-content-summary">
                      <div className="key">
                        Subtotal
                      </div>
                      <div className="value">
                        {`Rp ${formatNumber(shippingData.reduce((a, b) => a + b.products.reduce((x, y) => x + y.total_prices, 0), 0))}`}
                      </div>
                    </div>

                    {/* <div className="c-content-summary">
                      <div className="key">
                        Biaya Layanan
                      </div>
                      <div className="value">
                      {`Rp ${formatNumber(serviceCharge)}`}
                      </div>
                    </div> */}

                    <div className="c-content-summary">
                      <div className="key">
                        Pengiriman
                      </div>
                      <div className="value">
                        {`Rp ${formatNumber(shippingData.reduce((a,b) => a + b.shippingPrice, 0))}`}
                      </div>
                    </div>

                    <div className="c-content-summary">
                      <div className="key">
                        Total Pesanan
                      </div>
                      <div className="value">
                        <span style={{ color: '#FF8242', fontWeight: "900", fontSize:"20px" }}>{`Rp ${formatNumber(shippingData.reduce((a,b) => a + b.products.reduce((x, y) => x + y.total_prices, 0), 0) + serviceCharge + shippingData.reduce((a,b) => a + b.shippingPrice, 0))}`}</span>
                      </div>
                    </div>
                  </ReactPlaceholder>

                  <div className="c-content-summary">
                    <div className="key">
                      {`${shippingData.reduce((a, b) => a + b.products.length, 0)} Produk di Keranjang`}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="next-button-mobile" style={{ textAlign: 'center', marginTop: '2rem' }}>
              <button className="btn-primary btn-this xs:block hidden sm:block md:hidden lg:hidden" onClick={handleNext} disabled={shippingData.filter(v => v.selectedShipping == '').length > 0}>SELANJUTNYA</button>
            </div>
            </div>
          </div>
        </div>

      <AddAddressModal
        isOpen={openAddressModal}
        token={token}
        reloadAddress={reloadAddressData}
        idAddress={selectedAddress ? selectedAddress.id : 0}
        closeModal={closeModal}
        action={addressAction}
        selectedAddress={selectedAddress}
      />

      <Modal open={modalAddress} onClose={() => setModalAddress(false)}>
        <div className="card-modal" style={{ padding: '16px 26px' }}>
          <div style={{ textAlign: 'right' }}>
            <div style={{ display: 'inline-block' }} onClick={() => setModalAddress(false)} onKeyDown={() => setModalAdd(false)} className="pointer" role="button" tabIndex={0}>
              <img src="/images/ic_close_red.png" alt="" />
            </div>
          </div>
          <div className="title" style={{ marginTop: '16px' }}>
            Pilih Alamat Lain
          </div>
          <div style={{ marginTop: '24px' }}>
            <table className="table">
              <thead>
                <th className="th bg-white" />
                <th className="th bg-white">Penerima</th>
                <th className="th bg-white">Alamat Pengirim</th>
                <th className="th bg-white">Daerah Pengiriman</th>
              </thead>
              <tbody>
                {
                  addressData.map((v, i) => (
                    <tr>
                      <td className="td" style={{ padding: '0px 1rem' }}>
                        <label className="c-radio" style={{ marginTop: '-9px' }}>
                          <input type="radio" checked={v.isChecked} name="address" onChange={() => handleChangeSelectedAddress(i) }/>
                          <span className="checkmark" />
                        </label>
                      </td>
                      <td className="td" style={{ minWidth: '250px' }}>
                        <b>{v.name}</b>
                        <br />
                        <div style={{ marginTop: '1rem' }}>
                          {v.phone}
                        </div>
                      </td>
                      <td className="td">
                        <b>{v.address_name}</b>
                        <br />
                        <div style={{ marginTop: '1rem', maxWidth: '450px' }}>
                          {v.address}
                        </div>
                      </td>
                      <td className="td">
                        {`${v.province}, ${v.city}, ${v.district}, ${v.postal_code} ${v.country}`}
                      </td>
                    </tr>
                  ))
                }
              </tbody>
            </table>
          </div>
          <div style={{ marginTop: '24px', padding: '1rem 3rem', textAlign: 'center' }}>
            <button className="btn-primary btn-this" onClick={handleSubmitChangeAddress}>SIMPAN</button>
          </div>
          <div className="divider" style={{ margin: '24px' }} />
        </div>
      </Modal>

      <ShippingModal open={modalShipping} onClose={() => setModalShipping(false)}>
        <div className="card-modal" style={{ padding: '16px 26px' }}>
          <div style={{ textAlign: 'right' }}>
            <div style={{ display: 'inline-block' }} onClick={() => setModalShipping(false)} onKeyDown={() => setModalShipping(false)} className="pointer" role="button" tabIndex={0}>
              <img src="/images/ic_close_red.png" alt="" />
            </div>
          </div>
          <div className="select-shipping-content">
            <p>Pilih Pengiriman</p>
            <div className="shipping-overflow">    
                {openedShipping}
            </div>
          </div>
        </div>
      </ShippingModal>
      <style jsx>
        {`
          .title {
            font-weight: bold;
            font-size: 24px;
            line-height: 23px;
            letter-spacing: 0.05em;
            color: #696969;
            padding: 0px;
          }

          .c-content {
            display: flex;
            flex-direction: row;
          }

          .c-1 {
            width: 100%;
            padding-right:28px
          }

          .c-2 {
            width: 100%;
          }
          .card-2 {
            background: #FFFFFF;
            box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.25);
            height: auto;
          }

          .summary-header {
            padding: 1rem;
            font-weight: bold;
            font-size: 18px;
            line-height: 17px;
            color: #868686;
            box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.25);
          }

          .c-content-summary {
            font-size: 16px;
            line-height: 15px;
            color: #696969;
            display: flex;
            justify-content: space-between;
            padding: 0.7rem;
            margin: 0.8rem 0.5rem;
            border-bottom: 1px solid #CCCCCC;
          }

          .key {
            display: flex;
          }

          .key img {
            width: 85px;
          }

          .card {
            background: #FFFFFF;
            border: 0.5px solid #010101;
            box-sizing: border-box;
            box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.25);
            border-radius: 5px;
            margin-bottom: 20px;
          }

          th {
            text-align: left;
            padding: 1rem 1rem;
          }

          .seller-wrapper{
            padding:15px 20px;
          }
          .subtotal-wrapper{
            padding:15px 20px;
            border-top:1px solid #ccc;
          }
          .subtotal-wrapper .subtotal{
            text-align:right;
          }
          
          .seller-wrapper p{
            font-weight:400;
            font-size:14px;
            margin-top:10px;
          }

          .btn-action {
            background: #FFFFFF;
            border: 0.5px solid #696969;
            box-sizing: border-box;
            border-radius: 7px;
            font-weight: 500;
            font-size: 18px;
            line-height: 17px;
            text-align: center;
            color: #696969;
            padding: 1rem 2rem;
            cursor: pointer;
          }

          td {
            border-bottom: 1px solid #CCCCCC;
          }

          .btn-this {
            width: auto;
            font-weight: 500;
            font-size: 18px;
            line-height: 17px;
            text-align: center;
            color: #FFFFFF;
            padding: 1rem 2rem;
          }
          .product-item{
            padding:0 15px;
          }
          .product-shipping{
            padding:0 15px;
            background:#FAFDFF;
          }
          .subtotal-wrapper{
            background: #FAFDFF;
          }
          .checkout-step{
            padding:15px 20px;
            display:inline-block;
            border:1px solid #ccc;
          }
          .checkout-step.active-step{
            background:#010101;
            color:#fff;
          }
          .product-image{
            width:100px;
          }
          .shipping-content{
            padding: 20px 0;
          }
          .product-item{
            font-size:16px;
          }
          .product-item > div{
            padding:15px;
          }
          .shipping-item div{
            padding:20px
          }
          .product-shipping div{
            padding: 15px;
          }
          .select-shipping-content > p{
            font-size:24px;
            font-weight: bold;
          }
          .shipping-overflow{
            overflow-y:auto;
            height:550px;
          }
          .price-discount{
            text-decoration: line-through;
            line-height:20px;
          }
          .breadcumb{
            padding:0 30px;
          }
          .product-unit-price{
            text-align:center;
          }

          @media (min-width: 320px) and (max-width: 767px) {
            .shipping-content{
              padding: 20px
            }
            .td{
              padding: 10px;
            }
            .c-cart, .bulk-action {
              width: 100%;
            }
            .c-1{
              padding-right:0;
            }
            .next-button-mobile{
              width:100%;
            }
            .next-button-mobile button{
              margin:0 auto;
            }
            .c{
              margin-top:0;
            }
            .title{
              text-align:center;
            }
            .product-variant{
              text-align:left;
            }
            .product-unit-price{
              text-align:left;
            }
            .product-total-weight,
            .product-variant h4{
              color:#777;
            }
          }
        `
        }
      </style>
    </div>
  );
};

const AddAddressModal = ({ isOpen, token, reloadAddress, closeModal, action, selectedAddress }) => {
  const [form, setForm] = useState({
    id: selectedAddress ? selectedAddress.id : null,
    addressName: selectedAddress ? selectedAddress.address_name : '',
    name: selectedAddress ? selectedAddress.name : '',
    phone: selectedAddress ? selectedAddress.phone : '',
    address: selectedAddress ? selectedAddress.address : '',
    districtId: selectedAddress ? selectedAddress.district_id : null,
    postcode: selectedAddress ? selectedAddress.postal_code : ''
  });

  const handleInsertAddress = async () => {
    let body = {
      district_id: form.districtId.value,
      address_name: form.addressName,
      name: form.name,
      phone: form.phone,
      address: form.address,
      lat_long: '0,0',
      postal_code: form.postcode
    };

    let res;
    if(action === "insert"){
      res = await insertAddress(token, body);
    } else {
      body.id = selectedAddress.id;
      body.district_id = selectedAddress.district_id;
      body.is_default = selectedAddress.is_default;
      body.lat_long = selectedAddress.lat_long;
      res = await updateAddress(token, body);
    }
    await reloadAddress();

    if (res.code === 201 || res.code === 200) {
      successNotification(`Berhasil ${res.code === 201 ? "menambahkan" : "mengubah"} alamat`);
      closeModal();
    } else {
      errorNotification(res.data.message);
    }
  };

  const handleChangeForm = (name) => (e) => {
    const { value } = e.target;
    setForm({ ...form, [name]: value });
  };

  const loadOptions = (inputValue, callback) => {
    if (inputValue.length > 3) {
      setTimeout(() => {
        apiSearchAddress(inputValue)
          .then((res) => {
            callback(res.data.map((v) => ({
              label: v.result,
              value: v.id_district
            })));
          }).catch(() => callback([]));
      }, 500);
    } else {
      callback([]);
    }
  };

  return (
    <Modal open={isOpen} onClose={closeModal}>
      <div className="card-modal" style={{ padding: '16px 26px' }}>
        <div style={{ textAlign: 'right' }}>
          <div style={{ display: 'inline-block' }} onClick={closeModal} className="pointer" role="button" tabIndex={0}>
            <img src="/images/ic_close_red.png" alt="" />
          </div>
        </div>
        <div className="title" style={{ marginTop: '16px' }}>
          {action === "insert" ? "Tambah Alamat Baru" : "Ubah Alamat"}
        </div>
        <div style={{ marginTop: '24px' }}>
          <input className="form-control" placeholder="Nama Alamat" style={{ minWidth: '450px', width: '100%' }} value={form.addressName} onChange={handleChangeForm('addressName')} />
          <div className="form-helper">Contoh: Alamat Rumah, Kantor, Apartemen, Dropship</div>
        </div>
        <div style={{ marginTop: '24px' }}>
          <div style={{ minWidth: '450px', width: '100%', display: 'flex' }}>
            <div style={{ width: '100%' }}>
              <input className="form-control w-100" placeholder="Nama Penerima" type="text" value={form.name} onChange={handleChangeForm('name')} />
            </div>
            <div style={{ marginLeft: '10px', width: '100%' }}>
              <input className="form-control w-100" placeholder="Nomor HP" value={form.phone} onChange={handleChangeForm('phone')} />
            </div>
          </div>
        </div>
        <div style={{ marginTop: '24px' }}>
          <div style={{ minWidth: '450px', width: '100%', display: 'flex' }}>
            <div style={{ width: '100%' }}>
              <Select
                styles={{
                  control: (provided) => ({
                    ...provided,
                    background: '#FFFFFF',
                    boxShadow: '0px 1px 4px rgba(0, 0, 0, 0.25)',
                    borderRadius: '7px',
                    padding: '20px 18px',
                    border: '0px'
                  }),
                  valueContainer: (provided) => ({
                    ...provided,
                    padding: '0px'
                  }),
                  menu: (provided) => ({
                    ...provided,
                    padding: 'px'
                  }),
                  indicatorsContainer: (provider) => ({
                    ...provider,
                    display: 'none'
                  })
                }}
                placeholder="Kota / Kecamatan"
                loadOptions={loadOptions}
                cacheOptions
                defaultOptions={selectedAddress ? [{ label: `${selectedAddress.province}, ${selectedAddress.city}, ${selectedAddress.district}`, value: selectedAddress.districtId }] : null}
                value={form.districtId}
                onChange={(value) => setForm({ ...form, districtId: value })}
              />
            </div>
            <div style={{ marginLeft: '10px', width: '30%' }}>
              <input className="form-control w-100" placeholder="Kode Pos" value={form.postcode} onChange={handleChangeForm('postcode')} />
            </div>
          </div>
        </div>
        <div style={{ marginTop: '24px' }}>
          <textarea rows="4" className="form-control" placeholder="Alamat" style={{ minWidth: '450px', width: '100%' }} value={form.address} onChange={handleChangeForm('address')} />
        </div>
        {/* <div style={{ marginTop: '24px' }}>
          <span className="subheader">Pin Alamat (Opsional)</span>
        </div>
        <div style={{ marginTop: '10px' }}>
          <div className="card-pin">
            <div>
              <img src="/images/ic_map_red.png" alt="" />
            </div>
            <div className="text" style={{ width: '320px' }}>
              Tandai lokasi dalam peta jika anda mengirim dengan layanan Pick Up (seperti Go Send, Grab Express)
            </div>
            <div className="btn-action-outline">
              Tandai Lokasi
            </div>
          </div>
        </div> */}
        <div style={{ marginTop: '24px' }}>
          <button className="btn-primary" onClick={handleInsertAddress}>SIMPAN</button>
        </div>
        <div className="divider" style={{ margin: '24px' }} />
      </div>
    </Modal>
  )
}

export default index
