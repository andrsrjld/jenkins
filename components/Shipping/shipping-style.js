import css from 'styled-jsx/css';

export default css`
  .shipping-name-wrapper{
      margin: 7px 0;
      border-bottom:1px solid #e2e8f0;
      padding: 8px 0;
  }
  .shipping-name-wrapper:first-child{
      border-top:1px solid #e2e8f0;
  }
  .shipping-item{
      padding:7px 25px;
  }
  .shipping-name-wrapper > label{
      border-bottom:1px solid #e2e8f0;
      padding-bottom:15px;
  }
`;
