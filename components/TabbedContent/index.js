import React, { useState } from 'react';
import PropTypes from 'prop-types';
import style from './style.js';
import TodayDeals from '~/components/TodayDeals';
import Pelakkon from '~/components/Pelakkon';

const TabbedContent = ({ todayDeals, pelakkon, token }) => {
  const [titles, setTitles] = useState([
    {
      title: `Produk Unggulan`,
      active: true
    },
    {
      title: 'Merk',
      active: false
    }
  ]);

  const handleClickTab = (index) => {
    setTitles((v) => v.map((k, i) => ({ ...k, active: index === i })));
  };

  const renderContent = () => {
    if (titles[0].active) {
      return <TodayDeals data={todayDeals} token={token} />;
    }
    return <Pelakkon data={pelakkon} />;
  };

  return (
    <div className="c container mx-auto">
      <div className="container-tabs">
        {
          titles.map((v, i) => (
            <div
              role="button"
              className={`tab ${v.active ? 'active' : ''} focus:outline-none`}
              onClick={() => handleClickTab(i)}
              onKeyDown={() => handleClickTab(i)}
              tabIndex={0}
              key={v.title}
            >
              {v.title}
            </div>
          ))
        }
      </div>
      <div className="content">
        {renderContent()}
      </div>
      <style jsx>{style}</style>
    </div>
  );
};

TabbedContent.propTypes = {
  todayDeals: PropTypes.array,
  pelakkon: PropTypes.array
};

export default TabbedContent;
