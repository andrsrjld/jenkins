import css from 'styled-jsx/css';

export default css`
.c {
  display: flex;
  flex-wrap: wrap;
  margin-top: 50px;
  width: 100%;
}

.container-tabs {
  display: flex;
  justify-content: center;
  width: 100%;
}

.tab {
  padding: 10px;
  cursor: pointer;
  margin: 0px 5px;
  font-weight: bold;
  font-size: 22px;
  text-align: center;
  color: #A8A8A8;
}

.active {
  border-bottom: solid 4px #010101;
  color: #696969;
}

.content {
  width: 100%;
  margin-top: 50px;
}
.flashsale{
  background: #f3f3f3;
}

@media (min-width: 768px) and (max-width: 1024px) {
}

@media (min-width: 320px) and (max-width: 767px) {
}
`;
