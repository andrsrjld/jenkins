import React, { useState, useEffect } from 'react';
import ReactPlaceholder from 'react-placeholder';
import style from '../style';
import Link from 'next/link';
import moment from 'moment';

const CardArticle = ({ data }) => {
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setLoading(false);
    }, 2000);
  },[]);
  
  return (
    <div className="xs:w-full sm:w-full">
      <Link href={`/article/${data.slug}`}>
        <div className="container-content">
          <ReactPlaceholder type={'rect'} ready={!loading} showLoadingAnimation={true} rows={1} style={{height: '250px'}}>
            <img src={data.image_url} alt="" className="image" />
          </ReactPlaceholder>
          <div className="title">
            <ReactPlaceholder ready={!loading} showLoadingAnimation={true} rows={1} style={{width: '100%'}}>
              {data.title}
            </ReactPlaceholder>
          </div>
          <div className="date">
            <ReactPlaceholder ready={!loading} showLoadingAnimation={true} rows={1} style={{width: '100%'}}>
              {`${moment(data.created_at).format("DD MMMM YYYY")} / `}
            </ReactPlaceholder>
            <ReactPlaceholder ready={!loading} showLoadingAnimation={true} rows={1} style={{width: '100%'}}>
              <span className="author">{data.author_name}</span>
            </ReactPlaceholder>
          </div>
          <div className="desc">
          <ReactPlaceholder ready={!loading} showLoadingAnimation={true} rows={3} style={{width: '100%'}}>
            {data.highlight.toString().split(" ").splice(0, 30).join(" ")}
          </ReactPlaceholder>
          </div>
          <div>
            <hr className="divider-bottom" />
          </div>
          <div className="container-bottom">
            <div>
              <span>
                  Lanjutkan Membaca &gt;
              </span>
            </div>
          </div>
        </div>
      </Link>
      <style jsx>{style}</style>
    </div>
  );
}

export default CardArticle;
