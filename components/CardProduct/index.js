import React, { useState, useEffect } from 'react';
import Link from 'next/link';
import PropTypes from 'prop-types';
import style from './style';
import { formatNumber } from '~/util/helper';
import ReactPlaceholder from 'react-placeholder';

const CardTodayDeals = ({ data, loading }) => {
  const {
    slug, name, seller, rating, comments, price, images, discount_amount
  } = data;

  return (
    <div className="w-1/4 xs:w-1/2 sm:w-1/2 md:1/4 lg:w-1/4 p-2 xs:p-1 sm:p-1 md:p-2 lg:p-2">      
      <div className="container text-center product-item">
        <ReactPlaceholder type={'rect'} ready={!loading} showLoadingAnimation={true} rows={1} style={{height: '250px'}}>
          <Link href={`/product/${slug}`}>
            <img className="w-full" src={!images ? "/images/dummy_product.png" : images.find(v => v.is_primary).image_url} alt="" />
          </Link>
        </ReactPlaceholder>
        <Link href={`/product/${slug}`}>
          <div className="product-name">
            <ReactPlaceholder ready={!loading} showLoadingAnimation={true} rows={1} style={{width: '100%'}}>
              { name }
            </ReactPlaceholder>
          </div>
        </Link>
        <Link href={`/brand/${data.seller.slug}`}>
          <div className="merchant">
            <ReactPlaceholder ready={!loading} showLoadingAnimation={true} rows={1} style={{width: '100%'}}>
              { seller ? seller.store_name : "-" }
            </ReactPlaceholder>
          </div>
        </Link>
        <div className="container-price">
          <ReactPlaceholder ready={!loading} showLoadingAnimation={true} rows={2} style={{width: '100%'}}>
            <span className="price" hidden={discount_amount == 0 ? true : false}>{`Rp ${formatNumber(price)}`}</span>
            <span className="price-selling">{`Rp ${formatNumber(price - discount_amount)}`}</span>
          </ReactPlaceholder>
        </div>
        <style jsx>{style}</style>
      </div>
    </div>
  );
};

CardTodayDeals.propTypes = {
  data: PropTypes.object
};

export default CardTodayDeals;
