import css from 'styled-jsx/css';

export default css`
.c {
    width: 100%;
    padding: 0px 50px;
    margin-top: 80px;
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
}

.title {
    font-weight: bold;
    font-size: 22px;
    color: #696969;
}

.divider {
    height: 4px;
    background-color: #010101;
    border: none;
    border-radius: 9px;
    width: 100px;
}

.content {
    margin-top: 40px;
}

.container-content {
    display: flex;
    width: 100%;
    flex-wrap: wrap;
    justify-content: center;
}

.text {
    margin-top: 40px;
    font-size: 18px;
    text-align: center;
    color: #696969;
    font-style:italic;
}

.name {
    wdith: 100%;
    font-weight: 500;
    font-size: 16px;
    text-align: center;
    color: #696969;
    margin-top: 20px;
}

@media (min-width: 768px) and (max-width: 1024px) {
}

@media (min-width: 320px) and (max-width: 767px) {
}
`;
