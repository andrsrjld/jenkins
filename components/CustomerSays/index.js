import React, { useState } from 'react';
import PropTypes from 'prop-types';
import ItemsCarousel from 'react-items-carousel';
import { useMediaQuery } from 'react-responsive';
import style from './style.js';
import CardTodayDeals from '~/components/CardTodayDeals';

const CustomerSays = ({ data }) => {
  const chevronWidth = 50;
  const [activeItemIndex, setActiveItemIndex] = useState(0);
  const isTablet = useMediaQuery({ query: '(max-width: 1024px)' });
  const isMobile = useMediaQuery({ query: '(max-width: 480px)' });

  const getCountContent = () => {
    if (isMobile) { return 1; }
    if (isTablet) return 1;
    return 1;
  };

  const ButtonNavigation = (icon) => (
    <div
      className="pointer "
      style={{
        border: '8px', backgroundColor: '#FFFFFF'
      }}
    >
      {
        icon === 'left'
          ? (
            <svg id="i-chevron-left" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" width="32" height="32" fill="none" stroke="#BDBDBD" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2">
              <path d="M20 30 L8 16 20 2" />
            </svg>
          )
          : (
            <svg id="i-chevron-right" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" width="32" height="32" fill="none" stroke="#BDBDBD" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2">
              <path d="M12 30 L24 16 12 2" />
            </svg>
          )
      }
      {/* <img src={`${icon}`} alt={icon} style={{ height: '24px' }} /> */}
    </div>
  );

  return (
    <div className="c container mx-auto">
      <div className="flex flex-wrap justify-center">
        <span className="w-full title text-center">WHAT OUR CUSTOMERS SAYS ?</span>
        <hr className="divider" />
      </div>
      <div className="content w-100">
        <ItemsCarousel
          requestToChangeActive={setActiveItemIndex}
          activeItemIndex={activeItemIndex}
          numberOfCards={getCountContent()}
          gutter={10}
          leftChevron={ButtonNavigation('left')}
          rightChevron={ButtonNavigation('right')}
          outsideChevron
          chevronWidth={chevronWidth}
          infiniteLoop
        >
          {
            data.map((v) => (
              <div className="container-content">
                <img src="/images/ic_quote.png" alt="" />
                <div className="text w-full">
                  {v.content}
                </div>
                <div className="w-100 text-center flex justify-center">
                  <img src={v.photo.includes("https://") ? v.photo : "/images/dummy_customer.png"} alt="" style={{ marginTop: '40px' }} />
                </div>
                <div className="name">
                  {v.name}
                </div>
              </div>
            ))
          }
        </ItemsCarousel>
      </div>
      <style jsx>{style}</style>
    </div>
  );
};

CustomerSays.propTypes = {
  data: PropTypes.object
};

export default CustomerSays;
