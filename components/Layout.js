import Head from 'next/head';
import globalStyles from '../styles/global.js';

function Layout(props) {
  return (
    <div className="page-layout">
      <Head>
        <title>Bapera</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      {props.children}
      <style jsx global>
        {globalStyles}
      </style>
    </div>
  );
}

export default Layout;
