import React, { useState } from 'react';
import PropTypes from 'prop-types';
import ItemsCarousel from 'react-items-carousel';
import { useMediaQuery } from 'react-responsive';
import style from './style.js';
import Router from 'next/router';

const SaleItems = ({ data }) => {
  const { image, title, description } = data;
  const chevronWidth = 50;
  const [activeItemIndex, setActiveItemIndex] = useState(0);
  const isTablet = useMediaQuery({ query: '(max-width: 1024px)' });
  const isMobile = useMediaQuery({ query: '(max-width: 480px)' });

  const getCountContent = () => {
    if (isMobile) { return 2; }
    if (isTablet) return 3;
    return 4;
  };

  const ButtonNavigation = (icon) => (
    <div
      className="pointer "
      style={{
        border: '8px', backgroundColor: '#FFFFFF'
      }}
    >
      {
        icon === 'left'
          ? (
            <svg id="i-chevron-left" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" width="32" height="32" fill="none" stroke="#BDBDBD" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2">
              <path d="M20 30 L8 16 20 2" />
            </svg>
          )
          : (
            <svg id="i-chevron-right" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" width="32" height="32" fill="none" stroke="#BDBDBD" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2">
              <path d="M12 30 L24 16 12 2" />
            </svg>
          )
      }
      {/* <img src={`${icon}`} alt={icon} style={{ height: '24px' }} /> */}
    </div>
  );

  return (
    <div className="container">
      <ItemsCarousel
        requestToChangeActive={setActiveItemIndex}
        activeItemIndex={activeItemIndex}
        numberOfCards={getCountContent()}
        gutter={10}
        leftChevron={ButtonNavigation('left')}
        rightChevron={ButtonNavigation('right')}
        outsideChevron
        chevronWidth={chevronWidth}
        infiniteLoop
      >
        {
          data.map((v) => (
            // <div style={{ height: 200, background: '#EEE' }}>First card</div>
            <img className="p-8 sm:p-0 cursor-pointer" src={v.image_url} alt="" onClick={() => Router.push(`/brand/${v.slug}`)}/>
            // <CardExpert expert={v} />
            // v.productName
          ))
        }
      </ItemsCarousel>
      <style jsx>{style}</style>
    </div>
  );
};

SaleItems.propTypes = {
  data: PropTypes.object
};

export default SaleItems;
