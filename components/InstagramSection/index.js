import React, { useState, useEffect } from 'react';
import Head from 'next/head';
import PropTypes from 'prop-types';
import style from './style';
import { useMediaQuery } from 'react-responsive';
import ItemsCarousel from 'react-items-carousel';
// import Feed from "react-instagram-authless-feed";

const ArticleSection = ({ data }) => {
  const {
    image
  } = data;

  const [activeItemIndex, setActiveItemIndex] = useState(0);
  const chevronWidth = 50;
  const isTablet = useMediaQuery({ query: '(max-width: 1024px)' });
  const isMobile = useMediaQuery({ query: '(max-width: 480px)' });

  const getCountContent = () => {
    if (isMobile) { return 1; }
    if (isTablet) return 2;
    return 4;
  };

  useEffect(() => {
    new InstagramFeed({
        'username': 'lakkon_id',
        'container': document.getElementById("instagram-feed"),
        'display_profile': false,
        'display_biography': false,
        'display_gallery': true,
        'callback': null,
        'styling': true,
        'items': 8,
        'items_per_row': 4,
        'margin': 1 
    });
  })

  const ButtonNavigation = (icon) => (
    <div
      className="pointer "
      style={{
        border: '8px', backgroundColor: '#FFFFFF'
      }}
    >
      {
        icon === 'left'
          ? (
            <svg id="i-chevron-left" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" width="32" height="32" fill="none" stroke="#BDBDBD" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2">
              <path d="M20 30 L8 16 20 2" />
            </svg>
          )
          : (
            <svg id="i-chevron-right" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" width="32" height="32" fill="none" stroke="#BDBDBD" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2">
              <path d="M12 30 L24 16 12 2" />
            </svg>
          )
      }
      {/* <img src={`${icon}`} alt={icon} style={{ height: '24px' }} /> */}
    </div>
  );

  return (
    <div>
      <Head>
        <script src="/js/InstagramFeed.min.js"></script>
      </Head>
      <div className="c container mx-auto">
        <div className="flex flex-wrap justify-center">
          <span className="w-full title text-center">INSTAGRAM @LAKKON_ID</span>
          <hr className="divider" />
        </div>

        <div className="w-100" />

        <div className="container carousel-wrapper">
          {
            // <ItemsCarousel
            //   requestToChangeActive={setActiveItemIndex}
            //   activeItemIndex={activeItemIndex}
            //   numberOfCards={getCountContent()}
            //   gutter={10}
            //   leftChevron={ButtonNavigation('left')}
            //   rightChevron={ButtonNavigation('right')}
            //   outsideChevron
            //   chevronWidth={chevronWidth}
            //   infiniteLoop
            // >
            // {
            //   data.map((v) => (
            //     <div>
            //       <div className="container-content">
            //         <img src={v.image} alt="" className="image" />
            //       </div>
            //     </div>
            //   ))
            // }
            // </ItemsCarousel>
            // <Feed userName="lakkon_id" className="Feed" classNameLoading="Loading"/>
            <div id="instagram-feed">
            </div>
          }
        </div>
        <div style={{ marginTop: '50px' }}>
          <a className="btn-primary btn-action" href="https://www.instagram.com/lakkon_id/">SELENGKAPNYA</a>
        </div>
        <style jsx>{style}</style>
      </div>
    </div>
  );
};

ArticleSection.propTypes = {
  data: PropTypes.array
};

export default ArticleSection;
