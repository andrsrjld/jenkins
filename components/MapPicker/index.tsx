import {useState, useEffect} from 'react'
import GoogleMapReact from 'google-map-react';
import fetch from 'node-fetch'

import SearchPlace from 'components/SearcPlace'

const AnyReactComponent = ({ lat, lng, text }) => <div>
  <img src="/images/ic_pin.png" alt="" />
</div>;


const MapPicker = ({onClose, onSubmit, defaultCenter, reverseGeolocation}) => { 
  const [address, setAddress] = useState('')
  const [center, setCenter] = useState<any>({
    lat: defaultCenter ? Number(defaultCenter.lat) : -6.22,
    lng: defaultCenter ? Number(defaultCenter.lng) : 106.84
  })
  const [api, setApi] = useState({
    mapsApiLoaded: false,
    mapInstance: null,
    mapsapi: null,
  })

  useEffect(() => {
    getLocation()
    if (defaultCenter == null && reverseGeolocation) {
      getReverseGeolocation(reverseGeolocation)
    }
  }, [])

  function getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(coor => {
        setCenter({
          lat: defaultCenter ? Number(defaultCenter.lat) : coor.coords.latitude,
          lng: defaultCenter ? Number(defaultCenter.lng) : coor.coords.longitude
        })
      });
    }
  }

  const getReverseGeolocation = async (address) => {
    const res = await fetch(`https://maps.googleapis.com/maps/api/geocode/json?address=${address}&sensor=true&key=AIzaSyBPXQYyA1_p8j6tEzaIzG6Qk4WW4lkNcts`)
    const resObj = await res.json()
    if(resObj.results.length > 0) {
      setCenter({
        lat: resObj.results[0].geometry.location.lat,
        lng: resObj.results[0].geometry.location.lng,
      })
    } else {
      // setAddress('-')
    }
  }
  
  const handleChange = async ({center, zoom, bounds, marginBounds}) => {
    setCenter(center)
    const res = await fetch(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${center.lat},${center.lng}&sensor=true&key=AIzaSyBPXQYyA1_p8j6tEzaIzG6Qk4WW4lkNcts`)
    const resObj = await res.json()
    if(resObj.results.length > 0) {
      setAddress(resObj.results[0].formatted_address)
    } else {
      setAddress('-')
    }
  }

  const apiLoaded = (map, maps) => {
    setApi({
      mapsApiLoaded: true,
      mapInstance: map,
      mapsapi: maps,
    });
  }

  return (
    <div className="bg-white p-4 shadow rounded">
      <div className="w-full flex justify-end">
        <img className="cursor-pointer" src="/images/ic_close_red.png" alt="" onClick={onClose}/>
      </div>
      <div className="w-full text-center font-medium text-text-primary text-2xl mt-4">
        Tambah Alamat Baru
      </div>
      <div className="border-b-2 text-text-primary mt-4"></div>
      <div className="text-text-primary mt-4">
      {/* {mapsApiLoaded && <SearchBox map={mapInstance} mapsapi={mapsapi} />} */}
      {api.mapsApiLoaded && <SearchPlace placeholder="Cari Tempat" onPlacesChanged={(place) => {
        setCenter({
          lat: place[0].geometry.location.lat(),
          lng: place[0].geometry.location.lng()
        })
      }} map={api.mapInstance} mapsapi={api.mapsapi}/> }
      </div>
      <div className="relative mt-4 map-wrapper">
        <GoogleMapReact
            bootstrapURLKeys={{ key: 'AIzaSyBPXQYyA1_p8j6tEzaIzG6Qk4WW4lkNcts', libraries: ['places'] }}
            center={center}
            defaultZoom={15}
            onChange={handleChange}
            yesIWantToUseGoogleMapApiInternals
            onGoogleApiLoaded={({ map, maps }) => {
              apiLoaded(map, maps);
            }}
          >
          </GoogleMapReact>
          <div className="absolute bg-white py-2 px-4 rounded w-5/6 truncate shadow" style={{ top: '50%', left: '8.3%', marginTop: '-70px' }}>{address}</div>
          <img className="absolute" src="/images/ic_pin.png" alt="" style={{ top: '50%', left: '50%', marginLeft: '-19px', marginTop: '-19px' }}/>
      </div>
      <div className="w-full">
        <button className="bg-primary w-full px-4 py-3 font-medium text-white rounded mt-4" onClick={() => onSubmit(center, address)}>SIMPAN</button>
      </div>
      <style jsx>
      {`
        .map-wrapper{
          height: 300px;
          width: 500px;
        }
        @media (min-width: 320px) and (max-width: 767px) {
          .map-wrapper{
            width:100%;
            height:400px;
          }
        }
      `}
      </style>
    </div>
  )
}

export default MapPicker