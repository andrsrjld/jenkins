import css from 'styled-jsx/css';

export default css`
.c {
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  width: 100%;
}

.title {
  font-weight: bold;
  font-size: 22px;
  color: #696969;
}

.divider {
  height: 4px;
  background-color: #010101;
  border: none;
  border-radius: 9px;
  width: 100px;
}

.content {
  width: 100%;
  display: flex;
  justify-content: center;
  margin-top: 60px;
}

.container-content {
  text-align: center;
  width: 100%;
}

.image {
  height: 100px;
  margin:0 auto;
}

.step {
  margin-top: 20px;
  font-weight: 500;
  font-size: 22px;  
  color: #010101;
}

.title {
  font-weight: bold;
  font-size: 22px;
  color: #696969;
  margin-top: 10px;
}

.desc {
  margin-top: 10px;
  font-size: 16px;
  color: #696969;
}

.btn-action {
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  color: #FFFFFF;
  padding: 0.5rem 2rem !important;
}

@media (min-width: 768px) and (max-width: 1024px) {
}

@media (min-width: 320px) and (max-width: 767px) {
  .container-content{
    width: 100%;
    margin:0;
    padding: 15px;
  }
  .image {
    height: 70px;
  }
  .desc{
    font-size:14px;
  }
}
`;
