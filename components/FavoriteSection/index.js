import React from 'react';
import Router from 'next/router'
import style from './style';

const FavoriteSection = () => {
  const data = [
    {
      image: '/images/ic_favorit1.png',
      step: 'LANGKAH 1',
      title: 'Coba Isi Quiz',
      desc: 'Sama seperti mencari jodoh, mari menjawab pertanyaan ini untuk mencari tau karakter kopi kesukaan Kamu.'
    },
    {
      image: '/images/ic_favorit2.png',
      step: 'LANGKAH 2',
      title: 'Menemukan Kecocokan',
      desc: 'Cek langsung kopi yang di rekomendasikan sesuai dengan selera Kamu.'
    },
    {
      image: '/images/ic_favorit3.png',
      step: 'LANGKAH 3',
      title: 'Coba Isi Quiz',
      desc: 'Lakukan pemesanan dari Pelakkon yang sudah di rekomendasikan.'
    },
    {
      image: '/images/ic_favorit4.png',
      step: 'LANGKAH 4',
      title: 'Coba Isi Quiz',
      desc: 'Seduh kopi Kamu dan nikmati.'
    }
  ];

  return (
    <div className="c container mx-auto">
      <div className="flex flex-wrap justify-center">
        <div className="w-full text-center">
          <span className="title">PETA JEJAK KOPI</span>
        </div>
        <hr className="divider" />
      </div>

      <div className="w-100" />

      <div className="content flex flex-wrap">
        {
          data.map((v) => (
            <div className="w-1/4 xs:w-1/2 sm:w-1/2" key={v.step}>
              <div className="container-content">
                <img src={v.image} alt="" className="image" />
                <div className="step">
                  {v.step}
                </div>
                {/* <div className="title">
                  {v.title}
                </div> */}
                <div className="desc">
                  {v.desc}
                </div>
              </div>
            </div>
          ))
        }
      </div>
      <div style={{ marginTop: '50px' }}>
        <button className="btn-primary btn-action" type="button" onClick={() => Router.push('/wheels')}>COBA SEKARANG</button>
      </div>
      <style jsx>{style}</style>
    </div>
  );
};
export default FavoriteSection;
