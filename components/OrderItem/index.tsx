import React, { useRef, useState } from 'react'
import {connect} from 'react-redux'
import { useReactToPrint } from 'react-to-print';
import moment from 'moment'

import { STATUS } from 'data/api/order'
import { formatNumber } from 'util/helper'
import { bookingDriver, TGosendShipmentMethod } from 'data/api/gosend';
import { bookingStatus } from 'data/api/gosend'

import Modal from 'components/Modal'

import {successNotification, errorNotification} from 'util/toastr-notif';

type TProps = {
  data: any,
  action: any,
  callback: any,
  callbackDetail: any,
  authentication: any,
  callbackReload: any,
  isAdmin: boolean
}

class Invoice extends React.Component<any> {
  render() {
    return (
    <div className="w-full flex flex-wrap p-4">

      <div className="w-full flex">
        <div className="w-full flex items-center ml-4">
          <img src={this.props.profile.seller.photo} alt="Seller Logo" className="w-24 h-24 border"/>
        </div>
        <div className="w-full flex flex-wrap text-right">
          <div className="w-full text-3xl font-medium">Invoice</div>
          <div className="w-full">{this.props.data.invoice_no}</div>
          <div className="w-full">{moment(this.props.data.date).format('DD MMMM YYYY HH:mm')}</div>
        </div>
      </div>

      <div className="w-full flex mt-4">
        <div className="w-full mr-4">
          <div className="w-full bg-gray-300 p-2 rounded-t">
            <b>{this.props.profile.seller.store_name}</b>
          </div>
          <div className="w-full border border-gray-300 p-2 rounded-b">
            {this.props.profile.seller.about_us}
          </div>
        </div>
        <div className="w-full ml-4">
          <div className="w-full bg-gray-300 p-2 rounded-t">
            <b>{this.props.data.receiver_name}</b>
          </div>
          <div className="w-full border border-gray-300 p-2 rounded-b">
            {/* {this.props.data.detail?.address} */}
            {`${this.props.data.detail?.address}, ${this.props.data.detail?.address_detail.province}, ${this.props.data.detail?.address_detail.city}, ${this.props.data.detail?.address_detail.district}, ${this.props.data.detail?.address_detail.postal_code}`}
            <br></br>
            {this.props.data.detail?.phone}
            <br></br>
            <br></br>
            <b>Status :</b> {this.props.data.status}
            <br></br>
            <b>Keterangan :</b> {this.props.data.notes}
          </div>
        </div>
      </div>

      <div className="w-full mt-4">
        <table className="w-full">
          <thead>
            <th className="bg-gray-300 text-black text-center">No</th>
            <th className="bg-gray-300 text-black text-center">Produk</th>
            <th className="bg-gray-300 text-black text-center">Jumlah</th>
            <th className="bg-gray-300 text-black text-center">Harga</th>
            <th className="bg-gray-300 text-black text-center">Diskon</th>
            <th className="bg-gray-300 text-black text-center">Subtotal</th>
          </thead>
          {
            this.props.data.detail && (
              this.props.data.detail.order_items.map((v, i) => (
                <tr key={i}>
                  <td className="border">{i+1}</td>
                  <td className="border">{v.product_name + ' - ' + v.variant_name}</td>
                  <td className="text-right border">{v.quantity}</td>
                  <td className="text-right border">{formatNumber(v.price)}</td>
                  <td className="text-right border">{formatNumber(v.product_discount)}</td>
                  <td className="text-right border">{formatNumber(v.subtotal)}</td>
                </tr>
              ))
            )
          }
        </table>
      </div>

      <div className="w-full flex mt-4">
        <div className="w-full">

        </div>
        <div className="w-full">
          <div className="w-full flex">
            <div className="w-full text-right">
              Sebelum Diskon
            </div>
            <div className="w-full text-right">
              {formatNumber(this.props.data.total_price + this.props.data.total_product_discount)}
            </div>
          </div>
          <div className="w-full flex">
            <div className="w-full text-right">
              Diskon Produk
            </div>
            <div className="w-full text-right">
              {formatNumber(this.props.data.total_product_discount)}
            </div>
          </div>
          <div className="w-full flex mt-4">
            <div className="w-full text-right">
              Setelah Diskon
            </div>
            <div className="w-full text-right">
              {formatNumber(this.props.data.total_price)}
            </div>
          </div>
          <div className="w-full flex">
            <div className="w-full text-right">
              Biaya Pengiriman
            </div>
            <div className="w-full text-right">
              {formatNumber(this.props.data.total_shipping)}
            </div>
          </div>
          <div className="w-full flex mt-4 text-xl font-bold">
            <div className="w-full text-right">
              Grand Total
            </div>
            <div className="w-full text-right">
              {formatNumber(this.props.data.total_price + (this.props.data.total_shipping ? this.props.data.total_shipping : 0))}
            </div>
          </div>
        </div>
      </div>

      <div className="w-full mt-4 flex justify-center">
        <img src="/images/ic_bapera.png" alt="Logo Lakkon" className="h-6 w-6"/>
      </div>

      <div className="w-full mt-2 text-center font-bold text-sm">
        Powered by LAKKON.ID
      </div>

    </div>
    );
  }
}

class Pesanan extends React.Component<any> {
  render() {
    return (
    <div className="w-full flex p-4 border-b-2 text-sm">

      <div className="w-full flex">
        <div className="w-full text-center mx-2">
          <div className="w-full flex justify-center mb-4">
            <img src={this.props.profile.seller.photo} alt="Logo Seller" className="h-24 w-24 border"/>
          </div>
          <b>Packing Slip {this.props.data.invoice_no}</b>
          <br></br>
          {moment(this.props.data.date).format('DD MMMM YYYY')}
          <br></br>
          <b>{this.props.profile.seller.store_name}</b>
          <br></br>
          <b>Phone : </b> {this.props.profile.seller.phone}
          <br></br>
          <div className="w-full flex justify-center mt-4">
            <img src='/images/ic_bapera.png' alt="Logo Lakkon" className="h-6 w-6"/>
          </div>
          <div className="font-medium">powered by lakkon.id</div>
        </div>
        <div className="w-full mx-2">
          <div className="w-full border text-center p-4">
            <b>Tujuan Pengiriman</b>
          </div>
          <div className="w-full border p-4">
            Penerima : {this.props.data.receiver_name}
          </div>
          <div className="w-full border p-4">
            {/* Alamat : {this.props.data.detail?.address} */}
            Alamat : {`${this.props.data.detail?.address}, ${this.props.data.detail?.address_detail.province}, ${this.props.data.detail?.address_detail.city}, ${this.props.data.detail?.address_detail.district}, ${this.props.data.detail?.address_detail.postal_code}`}
          </div>
          <div className="w-full border p-4">
            Telepon : {this.props.data.detail?.phone}
          </div>
          <div className="w-full border p-4">
            Pengiriman : {this.props.data.shipping}
          </div>
        </div>
        <div className="w-full mx-2">
          <div className="w-full font-medium px-4 border-t border-b py-2">
            Checklist {this.props.data.invoice_no} - {this.props.data.receiver_name} - {moment(this.props.data.date).format('DD MMMM YYYY')}
          </div>
          {
            this.props.data.detail && (
              this.props.data.detail.order_items.map(v => (
                <div className="w-full border-t border-b px-4 py-2">{v.quantity} x {v.product_name + ' - ' + v.variant_name}</div>
              ))
            )
          }
          {
            this.props.data.detail && (
              <div className="w-full px-4 py-2">
                Total Quantity {this.props.data.detail.order_items.reduce((a,b) => a + Number(b.quantity), 0)}
              </div>
            )
          }
        </div>
      </div>

    </div>
    );
  }
}

const OrderItem = (props : TProps) => {  
  const invoiceRef = useRef(null)
  const pesananRef = useRef(null)

  // Lacak Gosend
  const [gosendData, setGosendData] = useState(null)

  const handleCetakInvoice = useReactToPrint({
    content: () => invoiceRef.current,
  });

  const handleCetakPesanan = useReactToPrint({
    content: () => pesananRef.current,
  });

  const handleBookingGosend = async () => {
    const res = await bookingDriver(props.authentication.endpoint.apiUrl, props.authentication.token, {
      orderSellerId: props.data.id,
      sellerId: props.authentication.user.seller.id,
      userId: props.data.address_detail.user_id,
      paymentType: 3,
      deviceToken: "",
      collection_location: 'pickup',
      shipment_method: props.data.shipping.toLowerCase().includes('instant') ? TGosendShipmentMethod.INSTANT : TGosendShipmentMethod.SAMEDAY, //TODO Dinamic
      routes: [
        {
          originName: '',
          originNote: '',
          originContactName: props.authentication.user.name,
          originContactPhone: props.authentication.user.seller.phone,
          originLatLong: props.authentication.user.seller.address.lat_long,
          originAddress: props.authentication.user.seller.address.address,
          destinationName: '',
          destinationNote: '',
          destinationContactName: props.data.receiver_name,
          destinationContactPhone: props.data.address_detail.phone,
          destinationLatLong: props.data.address_detail.lat_long,
          destinationAddress: props.data.address_detail.address,
          item: props.data.user_name + ' Items',
          storeOrderId: props.data.slug,
          insuranceDetails: {
              applied: "false",
              fee: "0",
              product_description: props.data.user_name + ' Items',
              product_price: "0"
          }
        }
      ]
    })

    if(res.code == 400) {
      errorNotification(res.message)
    } else {
      props.callbackReload()

    }
  }

  const handleGetBookingGosend = async (code) => {
    const res = await bookingStatus(props.authentication.endpoint.apiUrl, props.authentication.token, code)
    setGosendData(res.data ? res.data : null)
  }
  
  return (
    <div className="w-full">
      <div className="w-full bg-white shadow-sm p-4 mt-4 rounded flex flex-wrap">
        <div className="w-full flex text-text-primary font-medium">
          <div className="w-full px-4 border-r-2 flex">
            <input className="mr-4 mt-1" type="checkbox" />
            <div className="w-full flex flex-wrap">
              <div className="w-full">
                {props.data.user_name}
              </div>
              <div className="w-full">
                No Transaksi <span className="font-normal">{props.data.invoice_no}</span>
              </div>
              <div className="w-full flex font-normal items-start">
                {
                  props.data.is_po && (
                    <div className="h-auto px-4 py-1 bg-gray-300 rounded text-text-primary">
                      PREORDER : {props.data.po_duration} Hari
                    </div>
                  )
                }
              </div>
            </div>
          </div>
          <div className="w-full px-4 border-r-2">
            Pengiriman <br></br>
            <div className="font-normal">
              <table>
                <tr>
                  <tr>
                    <td className="pt-1 pb-0 px-0">Penerima</td>
                    <td className="pt-1 pb-0 px-2">:</td>
                    <td className="pt-1 pb-0 px-0">{props.data.receiver_name}</td>
                  </tr>
                  <tr>
                    <td className="pt-1 pb-0 px-0">{props.data.shipping ? (props.data.shipping.includes('COD') ? `COD` : `Kurir`) : 'Kurir'}</td>
                    <td className="pt-1 pb-0 px-2">:</td>
                    <td className="pt-1 pb-0 px-0">{props.data.shipping ? (props.data.shipping.includes('COD') ? props.data.shipping.substring(props.data.shipping.indexOf(`,`) +1, props.data.shipping.length) : props.data.shipping ) :props.data.shipping}</td>
                  </tr>
                </tr>
              </table>
            </div>
          </div>
          <div className="w-full px-4 flex flex-wrap flex-col">
            <div className="w-full">
              Status Transaksi
            </div>
            {
              props.data.status == STATUS.DIBAYAR && (
                <div className="w-full flex mt-2">
                  <div className="mx-1 px-2 py-1 rounded-full bg-primary text-white flex items-center">
                    <img src="/images/ic_check_white.png" alt="" />
                  </div>
                  <div className="mx-1 px-3 py-1 rounded-full bg-gray-300">
                    2
                  </div>
                  <div className="mx-1 px-3 py-1 rounded-full bg-gray-300">
                    3
                  </div>
                  <div className="mx-1 px-3 py-1 rounded-full bg-gray-300">
                    4
                  </div>
                </div>   
              )
            }
            {
              props.data.status == STATUS.DIPROSES && (
                <div className="w-full flex mt-2">
                  <div className="mx-1 px-2 py-1 rounded-full bg-primary text-white flex items-center">
                    <img src="/images/ic_check_white.png" alt="" />
                  </div>
                  <div className="mx-1 px-2 py-1 rounded-full bg-primary text-white flex items-center">
                    <img src="/images/ic_check_white.png" alt="" />
                  </div>
                  <div className="mx-1 px-3 py-1 rounded-full bg-gray-300">
                    3
                  </div>
                  <div className="mx-1 px-3 py-1 rounded-full bg-gray-300">
                    4
                  </div>
                </div> 
              )
            }
            {
              props.data.status == STATUS.DIKIRIM && (
                <div className="w-full flex mt-2">
                  <div className="mx-1 px-2 py-1 rounded-full bg-primary text-white flex items-center">
                    <img src="/images/ic_check_white.png" alt="" />
                  </div>
                  <div className="mx-1 px-2 py-1 rounded-full bg-primary text-white flex items-center">
                    <img src="/images/ic_check_white.png" alt="" />
                  </div>
                  <div className="mx-1 px-2 py-1 rounded-full bg-primary text-white flex items-center">
                    <img src="/images/ic_check_white.png" alt="" />
                  </div>
                  <div className="mx-1 px-3 py-1 rounded-full bg-gray-300">
                    4
                  </div>
                </div>
              )
            }
            {
              props.data.status == STATUS.SELESAI && (
                <div className="w-full flex mt-2">
                  <div className="mx-1 px-2 py-1 rounded-full bg-primary text-white flex items-center">
                    <img src="/images/ic_check_white.png" alt="" />
                  </div>
                  <div className="mx-1 px-2 py-1 rounded-full bg-primary text-white flex items-center">
                    <img src="/images/ic_check_white.png" alt="" />
                  </div>
                  <div className="mx-1 px-2 py-1 rounded-full bg-primary text-white flex items-center">
                    <img src="/images/ic_check_white.png" alt="" />
                  </div>
                  <div className="mx-1 px-2 py-1 rounded-full bg-primary text-white flex items-center">
                    <img src="/images/ic_check_white.png" alt="" />
                  </div>
                  <div className="mx-1 px-3 py-1 rounded-full bg-gray-300 opacity-0">
                    4
                  </div>
                </div>
              )
            }
            <div className="w-full mt-4">
              <button className="bg-gray-300 px-3 py-1 w-auto font-medium rounded cursor-default">{props.data.status}</button>
            </div>
            <div className="w-full mt-4 font-normal">
              {props.data.timeLimit}
            </div>
          </div>
          
          <div className="w-full px-4 items-center">
            {
              props.data.status == STATUS.DIBAYAR && (
                <>
                  {
                     (
                      <button className="w-full border rounded py-2 font-medium" onClick={() => props.action(props.data, STATUS.DIPROSES)}>
                        PROSES PESANAN
                      </button>
                    )
                  }
                  {
                    (
                      <button className="w-full border rounded py-2 font-medium mt-2" onClick={() => props.action(props.data, STATUS.DIBATALKAN)}>
                        TOLAK PESANAN
                      </button>
                    )
                  }
                  {/* <button className="w-full border rounded py-2 font-medium mt-2" onClick={() => props.action(props.data, STATUS.DIBATALKAN)}>
                    TOLAK PESANAN
                  </button> */}
                </>
              )
            }
            
            {
              (props.data.status == STATUS.DIPROSES || props.data.status == STATUS.DIKIRIM) && (props.data.shipping && !props.data.shipping.toLowerCase().includes('go') && !props.data.shipping.toLowerCase().includes('cod')) && (
                <>
                  <button className="w-full border rounded py-2 font-medium">
                    <input className="text-center" type="text" placeholder="Masukkan Nomor Resi" value={props.data.airway_bill ? props.data.airway_bill : ''} onChange={props.callback(props.data.id)}/>
                  </button>
                  
                  <button className="w-full border rounded py-2 font-medium mt-2 hover:bg-gray-200" onClick={() => props.action(props.data, STATUS.DIKIRIM)}>
                    SIMPAN RESI
                  </button>
                </>
              )
            }
            {
              (props.data.status == STATUS.DIPROSES || props.data.status == STATUS.DIKIRIM) && (props.data.shipping && props.data.shipping.toLowerCase().includes('go')) && (
                <>
                  <button className="w-full border rounded font-medium items-center flex">
                    <input className="text-center py-4 w-full" type="text" value={props.data.airway_bill ? props.data.airway_bill : ''} onChange={props.callback(props.data.id)} disabled/>
                    {
                      props.data.airway_bill 
                      ? <button className="px-4 border border-gray-400 py-1 rounded hover:bg-gray-400" onClick={() => handleGetBookingGosend(props.data.airway_bill ? props.data.airway_bill : '')}>Lacak</button>
                      : null
                    }
                  </button>
                  {
                    !props.isAdmin && props.data.status == STATUS.DIPROSES && (
                      <button className="w-full border rounded py-2 font-medium mt-2 hover:bg-gray-200" onClick={() => handleBookingGosend()}>
                        BOOKING DRIVER
                      </button>
                    )
                  }
                </>
              )
            }
            {
              !props.isAdmin && (props.data.status == STATUS.DIPROSES) && (props.data.shipping && props.data.shipping.toLowerCase().includes('cod')) && (
                <>
                  <button className="w-full border rounded py-2 font-medium mt-2 hover:bg-gray-200" onClick={() => props.action(props.data, STATUS.SELESAI)}>
                    SELESAIKAN PESANAN
                  </button>
                </>
              )
            }
            {/* {
              props.isAdmin && (props.data.status != STATUS.DIBATALKAN || props.data.status != STATUS.SELESAI) && (
                <button className="w-full border rounded py-2 font-medium mt-2" onClick={() => props.action(props.data, STATUS.DIBATALKAN)}>
                  TOLAK PESANAN
                </button>
              )
            } */}
            {
              props.isAdmin && (props.data.status == STATUS.DIPROSES || props.data.status == STATUS.DIKIRIM) &&  (
                <button className="w-full border rounded py-2 font-medium mt-2 hover:bg-gray-200" onClick={() => props.action(props.data, STATUS.SELESAI)}>
                  SELESAIKAN PESANAN
                </button>
              )
            }
            <button className="w-full rounded py-2 font-medium mt-2 flex justify-center items-center text-primary" onClick={() => props.callbackDetail(props.data)}>
              LIHAT DETAIL <img className="ml-2" src="/images/ic_arrow_down_red.png" alt=""/>
            </button>
          </div>
        </div>
      </div>
      
      <div className={`w-full flex text-text-primary ${props.data.detail ? '' : 'hidden'}`}>
        <div className="w-full pl-4 pr-2">
          <div className="w-full bg-white shadow-sm p-4 mt-4 rounded">
            <div>
              <span className="font-medium">Detail Barang</span>
            </div>
            {
              typeof props.data.detail?.order_items != 'undefined' && (
                <>
                {
                  props.data.detail?.order_items.map(v => (
                    <div key={v.product_name} className={`${v.quantity < 0 ? 'text-primary' : ''} w-full flex mt-4 items-center`}>
                      <div className="w-full flex">
                        <img className="w-24 mr-4 self-center" src={v.img_url} alt=""></img>
                        <div>
                          {`${v.product_name} - ${v.variant_name}`}
                          <br></br>
                          <span className="font-bold text-sm">Notes: {v.notes}</span>
                        </div>
                      </div>
                      <div className="w-1/2 text-right">
                        {v.quantity} Barang
                      </div>
                      <div className="w-1/2 text-right">
                        {`Rp ${formatNumber(v.subtotal)}`}
                      </div>
                    </div>
                  ))
                }
                </>
              )
            }
            <div className="mt-4 w-full h-px bg-text-primary"/>
            <div className="w-full mt-4 flex justify-end">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Total Berat Barang
              </div>
              <div className="w-1/2 text-right">
                {props.data.detail?.total_weight} Gram
              </div>
            </div>
            <div className="w-full mt-4 flex justify-end">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Ongkos Kirim
              </div>
              <div className="w-1/2 text-right">
                {`Rp ${formatNumber(props.data.detail?.total_shipping)}`}
              </div>
            </div>
            <div className="w-full mt-4 flex justify-end">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Diskon
              </div>
              <div className="w-1/2 text-right">
              {`- Rp ${formatNumber(props.data.total_product_discount)}`}
              </div>
            </div>
            <div className="w-full mt-4 flex justify-end font-medium">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Grand Total
              </div>
              <div className="w-1/2 text-right">
              {`Rp ${formatNumber(props.data.detail?.total_price + (props.data.detail?.total_shipping ? props.data.detail?.total_shipping : 0 ))}`}
              </div>
            </div>
          </div>
        </div>
        <div className="w-full pl-2 flex flex-col">
          <div className="w-full bg-white shadow-sm p-4 mt-4 rounded">
            <div>
              <span className="font-medium">Alamat Pengiriman</span>
            </div>
            <div className="mt-4">
              {props.data.buyerName} <br></br>
              {`${props.data.detail?.address}, ${props.data.detail?.address_detail.province}, ${props.data.detail?.address_detail.city}, ${props.data.detail?.address_detail.district}, ${props.data.detail?.address_detail.postal_code}`}
              <br></br>
              <span className="font-medium">Telepon</span> : {props.data.detail?.phone}
            </div>
          </div>
          {
            !props.isAdmin && (
              <div className="w-full bg-white shadow-sm p-4 mt-4 rounded">
                <div>
                  <span className="font-medium">Tindakan</span>
                </div>
                <div className="mt-4">
                  <div>
                    <button className="bg-gray-400 rounded font-medium px-3 py-2" onClick={handleCetakPesanan}>CETAK PESANAN</button>
                    <button className="bg-gray-400 rounded font-medium px-3 py-2 ml-4" onClick={handleCetakInvoice}>CETAK INVOICE</button>
                  </div>
                </div>
              </div>
            )
          }
          {/* <div className="w-full bg-white shadow-sm p-4 mt-4 rounded">
            <div>
              <span className="font-medium">Catatan Pembelian</span>
            </div>
            <div className="mt-4">
              {props.data.detail?.notes ? props.data.detail?.notes : 'Tidak ada catatan' }
            </div>
          </div> */}
        </div>
      </div>
      
      <div className="hidden">
        {
          !props.isAdmin && (
            <Invoice data={props.data} profile={props.authentication.user} ref={invoiceRef}/>
          )
        }
      </div>
      <div className="hidden">
        {
          !props.isAdmin && (
            <Pesanan data={props.data} profile={props.authentication.user} ref={pesananRef}/>
          )
        }
      </div>

      <Modal open={gosendData} onClose={() => setGosendData(null)}>
        <div className="card-modal" style={{ padding: '16px 26px' }}>
          <div style={{ textAlign: 'right' }}>
            <div style={{ display: 'inline-block' }} onClick={() => setGosendData(null)} onKeyDown={() => setGosendData(null)} className="pointer" role="button" tabIndex={0}>
              <img src="/images/ic_close_red.png" alt="" />
            </div>
          </div>
          <div className="title" style={{ marginTop: '16px' }}>
            Pelacakan
          </div>
          <div style={{ marginTop: '24px', display: 'flex' }}>
            <div className="w-100">
              <table>
                <tr>
                  <td className="td-small">Nomor Pelacakan</td>
                  <td className="td-small">:</td>
                  <td className="td-small">{gosendData?.orderNo}</td>
                </tr>
                <tr>
                  <td className="td-small">Status Driver</td>
                  <td className="td-small">:</td>
                  <td className="td-small">{gosendData?.status}</td>
                </tr>
                <tr>
                  <td className="td-small">Nama Driver</td>
                  <td className="td-small">:</td>
                  <td className="td-small">{ gosendData?.driverName }</td>
                </tr>
                <tr>
                  <td className="td-small">Telepon Driver</td>
                  <td className="td-small">:</td>
                  <td className="td-small">{ gosendData?.driverPhone }</td>
                </tr>
                <tr>
                  <td className="td-small">Nomor Kendaraan</td>
                  <td className="td-small">:</td>
                  <td className="td-small">{ gosendData?.vehicleNumber }</td>
                </tr>
                <tr>
                  <td className="td-small">Nama Penerima</td>
                  <td className="td-small">:</td>
                  <td className="td-small">{ gosendData?.receiverName }</td>
                </tr>
                <tr>
                  <td className="td-small">Alasan Pembatalan</td>
                  <td className="td-small">:</td>
                  <td className="td-small">{ gosendData?.cancelDescription }</td>
                </tr>
                <tr>
                  <td className="td-small text-center" colSpan={3}><a target="_blank" href={gosendData?.liveTrackingUrl} className="text-center text-white bg-primary px-2 py-2 rounded">Lihat Live Tracking</a></td>
                </tr>
              </table>
            </div>
          </div>
          <div className="divider" style={{ margin: '1rem', width: 'auto' }} />
        </div>
      </Modal>
    </div>
  )
}

export default connect(
  (state) => state, {}
)(OrderItem);