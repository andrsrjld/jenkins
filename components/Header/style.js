import css from 'styled-jsx/css';

export default css`
  .c {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }

  .c-action {
    display: flex;
    align-items: center;
  }

  .c-search {
    display: flex;
    align-items: center;
    margin-right: 24px;
  }

  .form-search {
    padding: 12px 16px;
    font-size: 16px;
    background: #FFFFFF;
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.1);
    border-radius: 9px;
    border-top-right-radius: 0px;
    border-bottom-right-radius: 0px;
  }

  .btn-search {
    background: #010101;
    border-radius: 0px 9px 9px 0px;
    border: 0px;
    padding: 0px 16px;
    height: 42px;
    cursor: pointer;
  }

  .c-menu {
    display: flex;
    justify-content: flex-start;
    align-items: center;
    font-weight: 500;
    font-size: 18px;
    color: #696969;
    background-color: #F5F5F5;
    position: relative;
  }

  .menu {
    display: flex;
    align-items: center;
    padding: 15px 28px;
    cursor: pointer;
  }

  .active {
    background-color: #010101;
    color: #FFFFFF;
  }

  .container-belanja {
    position: absolute;
    background: #FFFFFF;
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    border-radius: 4px;
    top: 21px;
    width: 100%;
    max-width: 700px;
    display: none;
    padding: 24px;
    color: #696969;
    z-index: 5;
  }

  .dropdown-belanja:hover .container-belanja{
    display: flex;
    flex-wrap: wrap;
  }

  .card-category{
    width: 21%;
    background: #FFFFFF;
    box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.13);
    display: flex;
    flex-wrap: wrap;
    justify-content: space-evenly;
    align-items:center;
    padding: 10px;
    margin: 10px;
    font-weight: 500;
    font-size: 14px;
    text-align: center;
    color: #696969;
  }
  @media (min-width: 320px) and (max-width: 767px) {
    .form-search{
      padding: 0;
      border:none;
      box-shadow:none;
    }
  }
`;
