import React, { useEffect, useState } from 'react';
import Link from 'next/link';
import style from './style';
import ProfileDropdown from '~/components/ProfileDropdown';
import MobileNav from '~/components/MobileNav';
import HeaderMenu from '~/components/HeaderMenu';

const Header = ({ productType, token, cart }) => {

  const [keyword, setKeyword] = useState('');

  const handleSearch = (e) => {
    setKeyword(e.currentTarget.value);
  }

  const handleSubmitSearch = (e) => {
    e.preventDefault();
    if(keyword !== ""){
      window.location = `/search/${keyword}`;
    }
  }

  return (
    <div>
      {/* {Desktop Header} */}
      <div className="desktop-header block xs:hidden sm:hidden md:block lg:block">
        <div className="c container mx-auto" style={{ padding: '20px 0px' }}>
          <Link href="/">
            <img className="pointer h-16" src="/images/logo_bapera.png" alt="" height="60" />
          </Link>

          <div className="c-action">
            <div className="c-search">
              <form className="flex" onSubmit={handleSubmitSearch}>
                <input className="form-control form-search" placeholder="Cari Produk" onChange={handleSearch} required />
                <button type="submit" className="btn-search">
                  <img src="/images/ic_search_white.png" alt="search" />
                </button>
              </form>
            </div>
            <div>
              <HeaderMenu cart={cart} />
            </div>
          </div>
        </div>
        
        <div className="c-menu" style={{ padding: '0px' }}>
          <div className="container mx-auto flex">
            <div className="menu active dropdown-belanja">
              BELANJA
              {' '}
              <img style={{ marginLeft: '6px' }} src="/images/arrow_down_white.png" alt="arrow-down" />
              <div className="container-belanja" style={{ zIndex: '10' }}>
                {
                  productType ? productType.map((v) => (
                    <Link href={v.slug !== 'brand' ? `/category/${v.slug}` : '/brand'} key={v.slug}>
                      <div className="card-category">
                        <div className="text-center w-100 flex justify-center"><img src={`/images/ic_${v.name.toLowerCase().split(' ').join('-')}.png`} alt={v.slug} style={{ height: 48}} /></div>
                        <div style={{ marginTop: '8px' }}>{v.name}</div>
                      </div>
                    </Link>
                  )) : ""
                }
                <Link href={'/brand'}>
                  <div className="card-category">
                    <div className="text-center w-100 flex justify-center"><img src={`/images/ic_brand.png`} alt="brand" style={{ height: 48}}/></div>
                    <div style={{ marginTop: '8px' }}>Merk</div>
                  </div>
                </Link>
              </div>
            </div>

            <Link href="/brand">
              <div className="menu">
              MERK
              </div>
            </Link>
            <Link href="/about">
              <div className="menu">
                TENTANG BAPERA
              </div>
            </Link>
          </div>
        </div>
      </div>
      
      {/* {Mobile Header} */}
      <div className="mobile-header hidden xs:flex sm:flex">
        <MobileNav productType={productType} token={token} />
        <div className="w-full fixed top-0 left-0 z-10 flex flex-wrap pt-4 mobilenav-wrapper" style={{background: "#fff"}}>
          {/* Mobile Nav */}
          <div className="w-full flex items-center justify-between">
            
            <div className="w-full">
              &nbsp;
            </div>
            
            <div className="flex w-full justify-center">
              <Link href="/">
                <img src="/images/logo_bapera.png" alt="Logo" className="h-16" />
              </Link>
            </div>

            <div className="px-8 w-full">
              <div className="w-auto flex py-1 items-center border-b-2 border-text-primary max-w-xs">
                <form className="flex" onSubmit={handleSubmitSearch}>
                  <div>
                    <input className="form-control form-search w-100" placeholder="Cari" onChange={handleSearch} required/>
                  </div>
                    <img src="/images/ic_search.png" alt="search" />
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="hidden xs:block sm:block md:hidden lg:hidden" style={{height: "100px"}}></div>
      <style jsx>
        {style}
      </style>
    </div>
  );
}

export default Header;
