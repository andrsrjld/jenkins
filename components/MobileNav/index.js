import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux'
import Link from 'next/link';
import { slide as MobileNav } from "react-burger-menu";
import { deauthenticate } from 'redux/actions/authActions'

let styles = {
    bmBurgerButton: {
      position: 'fixed',
      width: '26px',
      height: '22px',
      left: '15px',
      top: '20px'
    },
    bmBurgerBars: {
      background: '#373a47'
    },
    bmBurgerBarsHover: {
      background: '#a90000'
    },
    bmCrossButton: {
      height: '24px',
      width: '24px'
    },
    bmCross: {
      background: '#bdc3c7'
    },
    bmMenuWrap: {
      position: 'fixed',
      height: '100%'
    },
    bmMenu: {
      background: '#fff',
      padding: '2.5em 1em 0',
      fontSize: '1.15em'
    },
    bmMorphShape: {
      fill: '#373a47'
    },
    bmItemList: {
      color: '#b8b7ad',
      padding: '0.8em'
    },
    bmItem: {
      display: 'inline-block'
    },
    bmOverlay: {
      background: 'rgba(0, 0, 0, 0.3)'
    }
  }

const MobileMenu = ({ productType, token, deauthenticate, authentication }) => {
  let listProductType = [];
  if(productType){
    productType.forEach((v) => {
      listProductType.push(
        <li style={{borderTop: "1px solid #cdcdcd", padding:"5px 10px"}} key={v.slug}>
          <Link href={v.slug !== 'brand' ? `/category/${v.slug}` : '/brand'}>
            <div className="flex items-center" style={{cursor: "pointer"}}>
              <div className="text-center w-1/6"><img style={{width: '25px'}} src={`/images/ic_${v.name.toLowerCase().split(' ').join('-')}.png`} alt={v.slug} /></div>
              <div className="w-5/6" style={{ padding: '0 15px' }}>{v.name}</div>
            </div>
          </Link>
        </li>
      )
    })
    listProductType.push(
      <li style={{borderTop: "1px solid #cdcdcd", padding:"5px 10px"}} key={'brands'}>
        <Link href={'/brand'}>
          <div className="flex items-center" style={{cursor: "pointer"}}>
            <div className="text-center w-1/6"><img style={{width: '25px'}} src={`/images/ic_brand.png`} alt="brand" /></div>
            <div className="w-5/6" style={{ padding: '0 15px' }}>Brands</div>
          </div>
        </Link>
      </li>
    )
  }
  return (
      <div className="hidden xs:block sm:block md:hidden lg:hidden">
          <MobileNav styles={styles}>
              <div className="menu-wrapper w-full outline-none">
                <label>Belanja</label>
                <ul className="sub-menu">
                  {listProductType}
                </ul>
              </div>
              <div className="menu-wrapper w-full" style={{cursor: "pointer"}}>
                <Link href="/brand">
                  <label style={{cursor: "pointer"}}>Brand</label>
                </Link>
              </div>
              <div className="menu-wrapper w-full" style={{cursor: "pointer"}}>
                <Link href="/about">
                  <label style={{cursor: "pointer"}}>Tentang BAPERA</label>
                </Link>
              </div>
              <Link href={authentication.token ? "/account" : "/signin"}>
                <div className="menu-wrapper w-full" style={{cursor: "pointer"}}>
                  <button><label style={{cursor: "pointer"}}>Akun Saya</label></button>
                </div>
              </Link>
              {
                authentication.token && (
                  <div className="menu-wrapper w-full" style={{cursor: "pointer"}} onClick={deauthenticate}>
                    <button><label style={{cursor: "pointer"}}>Keluar</label></button>
                  </div>
                )
              }
              {
                authentication.user && authentication.user.seller.id != 0 ? (
                  <Link href="/backoffice/dashboard">
                    <div className="menu-wrapper w-full" style={{cursor: "pointer"}}>
                      <button><label style={{cursor: "pointer"}}>Dashboard Seller</label></button>
                    </div>
                  </Link>
                ) : ""
              }
          </MobileNav>

          <style jsx>
            {`
              .menu-wrapper{
                padding: 10px 0;
                color:#696969;
                border-bottom: 1px solid #cdcdcd;
                font-size:16px;
              }
              .menu-wrapper:first-child{
                padding: 10px 0 0;
              }
              .sub-menu{
                margin: 5px 0;
              }
            `}
          </style>
      </div>
  )
}

const mapStateToProps = (state) => ({
  isAuthenticated: !!state.authentication.token,
  authentication: state.authentication
});


export default connect(
  mapStateToProps,
  {
    deauthenticate
  }
)(MobileMenu);