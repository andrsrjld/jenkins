import Head from 'next/head'
import {connect} from 'react-redux'
import UserProfile from 'components/BackOfficeBase/UserProfile'
import SideMenus from './SideMenus'
import Header from 'components/BackOfficeBase/Header'
import '~/styles/index.css'
import Link from 'next/link'
import { deauthenticate } from 'redux/actions/authActions'

const index = (props: {children : any}) => {
  return (
    <div className="w-full h-screen flex">

      <Head>
        <title>Bapera</title>
        <link rel="icon" href="/favicon.ico" />
        <meta name="viewport" content="width=1024"></meta>
      </Head>

      {/* SIDENAV */}
      <div className="max-w-sm overflow-y-auto max-h-screen">
        <Link href="/">
          <img className="py-4 px-12 mt-5 h-24 w-auto cursor-pointer" src="/images/logo_bapera.png" alt=""/>
        </Link>
        <div className="mt-6">
          <UserProfile name="Bapera Official" status />
        </div>

        <div className="mt-4">
          <SideMenus />
        </div>
      </div>

      {/* CONTENT */}
      <div className="w-full bg-gray-100 overflow-y-auto">
        {/* HEADER */}
        <div>
          <Header />
        </div>

        {/* CONTENT */}
        <div className="p-5 flex">
          {props.children}
        </div>
      </div>
    </div>
  )
}


const mapStateToProps = (state) => ({
  isAuthenticated: !!state.authentication.token,
  authentication: state.authentication
});

export default connect(
  mapStateToProps,
  {
    deauthenticate
  }
)(index);