import { useState } from 'react'
import Router, { useRouter } from 'next/router'
import Link from 'next/link'
import { motion } from "framer-motion"

type TMenu = {
  image: string, 
  imageActive: string, 
  title: string,
  url: string,
  isOpen: boolean,
  subMenus: Array<TMenu>
}

const menu: TMenu[] = [
  {
    image: '/images/ic_dashboard.png',
    imageActive: '/images/ic_dashboard_active.png',
    title: 'Dashboard',
    url: '/admin/dashboard',
    subMenus: [],
    isOpen: false
  },
  {
    image: '/images/ic_sales.png',
    imageActive: '/images/ic_sales_active.png',
    title: 'Pembelian',
    url: '/admin/order',
    subMenus: [],
    isOpen: false
  },
  {
    image: '/images/ic_sales.png',
    imageActive: '/images/ic_sales_active.png',
    title: 'Refund',
    url: '/admin/refund',
    subMenus: [],
    isOpen: false
  },
  {
    image: '/images/ic_sales.png',
    imageActive: '/images/ic_sales_active.png',
    title: 'Laporan',
    url: '',
    isOpen: false,
    subMenus: [
      {
        image: '',
        imageActive: '',
        title: 'Transaksi',
        url: '/admin/report/transaction',
        isOpen: false,
        subMenus: []
      },
      {
        image: '',
        imageActive: '',
        title: 'Transaksi Penjualan',
        url: '/admin/report/transaction-seller',
        isOpen: false,
        subMenus: []
      },
    ]
  },
  {
    image: '/images/ic_catalog.png',
    imageActive: '/images/ic_catalog_active.png',
    title: 'Konten',
    url: '',
    isOpen: false,
    subMenus: [
      {
        image: '',
        imageActive: '',
        title: 'Artikel',
        url: '/admin/content/article',
        isOpen: false,
        subMenus: []
      },
      {
        image: '',
        imageActive: '',
        title: 'Banner',
        url: '/admin/content/banner',
        isOpen: false,
        subMenus: []
      },
      {
        image: '',
        imageActive: '',
        title: 'Halaman',
        url: '/admin/content/page',
        isOpen: false,
        subMenus: []
      },
    ]
  },
  {
    image: '/images/ic_settings.png',
    imageActive: '/images/ic_settings_active.png',
    title: 'CRM',
    url: '',
    isOpen: false,
    subMenus: [
      {
        image: '',
        imageActive: '',
        title: 'Produk Unggulan',
        url: '/admin/crm/flashsale',
        isOpen: false,
        subMenus: []
      },
      {
        image: '',
        imageActive: '',
        title: 'Brand Populer',
        url: '/admin/crm/pelakon',
        isOpen: false,
        subMenus: []
      },
      {
        image: '',
        imageActive: '',
        title: 'Iklan',
        url: '/admin/crm/iklan',
        isOpen: false,
        subMenus: []
      },
      {
        image: '',
        imageActive: '',
        title: 'Voucher',
        url: '/admin/crm/voucher',
        isOpen: false,
        subMenus: []
      },
      // {
      //   image: '',
      //   imageActive: '',
      //   title: 'Events',
      //   url: '/admin/crm/event',
      //   isOpen: false,
      //   subMenus: []
      // },
    ]
  },
  {
    image: '/images/ic_sales.png',
    imageActive: '/images/ic_sales_active.png',
    title: 'Akun',
    url: '',
    isOpen: false,
    subMenus: [
      {
        image: '',
        imageActive: '',
        title: 'Akun User',
        url: '/admin/account/customer',
        isOpen: false,
        subMenus: []
      },
      {
        image: '',
        imageActive: '',
        title: 'Akun Penjual',
        url: '/admin/account/seller',
        isOpen: false,
        subMenus: []
      },
    ]
  },
  {
    image: '/images/ic_catalog.png',
    imageActive: '/images/ic_catalog_active.png',
    title: 'Katalog',
    url: '',
    isOpen: false,
    subMenus: [
      {
        image: '',
        imageActive: '',
        title: 'Tambah Produk',
        url: '/admin/catalog/add',
        isOpen: false,
        subMenus: []
      },
      {
        image: '',
        imageActive: '',
        title: 'Produk',
        url: '/admin/catalog/product',
        isOpen: false,
        subMenus: []
      },
    ]
  },
  {
    image: '/images/ic_profile.png',
    imageActive: '/images/ic_profile_red.png',
    title: 'User',
    url: '',
    isOpen: false,
    subMenus: [
      {
        image: '',
        imageActive: '',
        title: 'Pegawai',
        url: '/admin/user/pegawai',
        isOpen: false,
        subMenus: []
      },
    ]
  },
  {
    image: '/images/ic_settings.png',
    imageActive: '/images/ic_settings_active.png',
    title: 'Pengaturan',
    url: '',
    isOpen: false,
    subMenus: [
      {
        image: '',
        imageActive: '',
        title: 'Role',
        url: '/admin/settings/role',
        isOpen: false,
        subMenus: []
      },
      {
        image: '',
        imageActive: '',
        title: 'Biaya Layanan',
        url: '/admin/settings/service-fee',
        isOpen: false,
        subMenus: []
      }
    ]
  },
]

const variants = {
  open: { opacity: 1, y: 0, height: "auto" },
  closed: { opacity: 0, y: "-50%", height: 0 },
}

const variantsArrow = {
  open: { rotate: 0 },
  closed: { rotate: 180 },
}

const Menu = (props : {menu : TMenu, router: any, cbClick: any }) => {
  const {menu, router, cbClick} = props
  return (
    <div>
      <div onClick={() => cbClick(menu.title)}>
        <div onClick={() => menu.url ? Router.push(menu.url) : null} >
          <div className={`px-3 py-4 flex items-center font-medium cursor-pointer ${router.pathname === menu.url ? 'text-primary' : 'text-gray-900'}`}>
            <div className="w-full flex">
              <img className="mr-3" src={router.pathname === menu.url ? menu.imageActive : menu.image} />
              { menu.title }
            </div>
            {
              menu.subMenus.length > 0 && (
                <motion.nav
                  animate={menu.isOpen ? "open" : "closed"}
                  variants={variantsArrow}
                >
                  <img className="mr-3" src={'/images/ic_arrow_down.png'} />
                </motion.nav>
              )
            }
          </div>
        </div>
      </div>
      <motion.nav
          animate={menu.isOpen ? "open" : "closed"}
          variants={variants}
        >
        {
          menu.isOpen
          ? menu.subMenus.map((submenu, i) => (
            <Link href={submenu.url}>
              <div className={`px-3 py-4 flex items-center font-medium cursor-pointer ${i%2 != 0 ? 'bg-white' : 'bg-gray-50'} ${router.pathname === submenu.url ? 'text-primary' : 'text-gray-900'}`}>
                <div className="ml-8">
                  { submenu.title }
                </div>
              </div>
            </Link>
          ))
          :null
        }
      </motion.nav>
    </div>
  )
}

const SideMenus = () => {
  const router = useRouter()
  const [menus, setMenus] = useState(menu.map(v => ({...v, isOpen: router.pathname == v.url || v.subMenus.filter(k => k.url == router.pathname).length > 0 ? true : false})))

  const handleChange = (title) => {
    setMenus(v => v.map(v => ({...v, isOpen: v.title == title ? !v.isOpen : v.isOpen })))
  }

  return (
    <div className="divide-y divide-gray-400">
      {
        menus.map(v => {
          return (
            <Menu key={v.title} menu={v} router={router} cbClick={handleChange}/>
          )
        })
      }
    </div>
  )
}

export default SideMenus;