import css from 'styled-jsx/css';

export default css`
.c {
  cursor: pointer;
  position: relative;
  margin: 6px 6px;
}

.c img{
  width:100%;
}

.content {
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  position: absolute;
  top: 0;
  width: 100%;
  height: 100%;
  // background: rgba(140, 140, 140, 0.59);
  padding: 20px;
}

.title {
  font-weight: 400;
  font-size: 24px;
  color: #FFFFFF;
}

.description {
  font-weight: 400;
  font-size: 20px;
  color: #FFFFFF;
}

.action {
  width: max-content;
  font-weight: 400;
  font-size: 14px;
  padding-bottom: 5px;
  color: #FFFFFF;
  border-bottom: solid 4px #010101;
}


@media (min-width: 768px) and (max-width: 1024px) {
}

@media (min-width: 320px) and (max-width: 767px) {
  .title{
    font-size:20px;
  }
  .description {
    font-size: 16px;
  }
}
`;
