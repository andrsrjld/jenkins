import React, { useState, useEffect } from 'react';
import ReactGA from 'react-ga';
import PropTypes from 'prop-types';
import style from './style.js';

const SaleItems = ({ data }) => {
  const { thumbnail, title, description, link, action, slug } = data;
  
  return (
    <div className="xs:w-1/2 sm:w-1/2 sm:mb-2 md:w-1/2 lg:w-1/5 xl:w-1/5">
        <ReactGA.OutboundLink
          eventLabel={link}
          to={link}
          target="_blank"
          trackerNames={[link]}
        >
        <div className="c">
          {/* <ReactPlaceholder type={'rect'} ready={!loading} showLoadingAnimation={true} rows={1} style={{height: '250px'}}> */}
            <img src={thumbnail} alt={title} />
          
          <div className="content">
            <div className="title">
              {title}
            </div>
            <div className="description">
              {description}
            </div>
            <div className="action">
              {action}
            </div>
          </div>
          {/* </ReactPlaceholder> */}
          <style jsx>{style}</style>
        </div>
        </ReactGA.OutboundLink>
    </div>
    
  );
};

SaleItems.propTypes = {
  data: PropTypes.object
};

export default SaleItems;
