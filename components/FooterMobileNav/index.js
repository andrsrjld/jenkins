import React, { useState, useEffect } from 'react';
import Link from 'next/link';
import ProfileDropdown from '~/components/ProfileDropdown';

const FooterMobileMenu = ({ token }) => {
  return (
    <div className="hidden xs:block sm:block md:hidden lg:hidden">
        <div className="footer-mobile-nav-wrapper flex items-center w-full">
            <Link href="/">
                <div className="w-1/4 text-center footer-menu flex items-center justify-center">
                    <img className="pointer" src="/images/ic_home.png" alt="" width="20" style={{ margin: '0px auto' }} />
                </div>
            </Link>
            <Link href="/wishlist">
                <div className="w-1/4 text-center footer-menu flex items-center">
                    <img className="pointer" src="/images/ic_love.png" alt="" width="20" style={{ margin: '0px auto' }} />
                </div>
            </Link>
            <Link href="/cart">
                <div className="w-1/4 justify-center footer-menu flex items-center">
                    <img className="pointer" src="/images/ic_cart.png" alt="" width="20" style={{ margin: '0px auto' }} />
                </div>
            </Link>
            <Link href={token ? "/account" : "/signin"}>
                <div className="w-1/4 text-center footer-menu flex items-center justify-center">
                    <ProfileDropdown token={token} mobile={true} />
                </div>
            </Link>
        </div>

        <style jsx>
        {`
            .footer-mobile-nav-wrapper{
                background:#fff;
                position:fixed;
                bottom:0;
                left:0;
                border-top:1px solid #ddd;
            }
            .footer-menu{
                border-right:1px solid #cdcdcd;
                height:40px;
            }
        `}
        </style>
    </div>
  )
}

export default FooterMobileMenu;