import React, { useState } from 'react';
import PropTypes from 'prop-types';

const modal = ({ open, onClose, children }) => {
  const handleModalClose = (event) => {
    event.preventDefault();
    if (event.target.id === 'parent') { onClose(); }
  };

  return (
    <div id="parent" className={`c ${open ? 'active' : ''}`} style={{ zIndex: '9999' }}>
      <div className="c2 z-10 bg-red-500" onClick={handleModalClose} onKeyDown={handleModalClose} role="button" tabIndex={-1} style={{ zIndex: '10000' }}/>
      <div className="z-20 wide-modal overflow-auto" style={{ zIndex: '10001' }}>
        {children}
      </div>
      <style jsx>
        {
          `
            .c {
              position: fixed;
              width: 100%;
              display: none;
              background: rgba(118, 118, 118, 0.57);
              top: 0;
              left: 0;
              bottom: 0;
              right: 0;
            }
            .c2 {
              position: fixed;
              height: 100vh;
              width: 100%;
              display: block;
              background: rgba(118, 118, 118, 0.57);
              top: 0;
              left: 0;
              bottom: 0;
              right: 0;
            }
            .active {
              display: flex;
              flex-wrap: wrap;
              justify-content: center;
              align-items: center;
            }
            .digit-group{
              padding:0 20px;
            }
            @media (min-width: 320px) and (max-width: 767px) {
              .wide-modal{
                width:90%;
                height:80%;
              }
            }
            .wide-modal{
              max-width: 80%;
              min-width: 50%;
              height:80%;
            }
          `
        }
      </style>
    </div>
  );
};

modal.propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func,
  children: PropTypes.any
};

export default modal;
