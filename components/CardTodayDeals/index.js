import React, { useState, useEffect } from 'react';
import Link from 'next/link';
import PropTypes from 'prop-types';
import style from './style';
import { formatNumber } from '~/util/helper';

// Toastr Notification
import {successNotification, errorNotification} from '~/util/toastr-notif';

// Axios API Request
import axiosSetup from '../../services/api';

const CardTodayDeals = ({ data, token }) => {
  const {
    product_name, seller_name, variant_name, discount, price, image_url, variant_prices, slug, seller_slug
  } = data;

  const handleAddToWishlist = (data) => (e) => {
    let isExistWishlist;
    if(!token){
      window.location = `/`;
    } else {
      const api = axiosSetup(token);
      api.get("/restricted/customer/wishlist?layout_type=list_layout&order=product_name,ASC&page=1&limit=10000").then((response) => {
        const userWishlist = response.data.data;
        if(userWishlist){
          const existWishlist = userWishlist.find(wishlist => {
            return wishlist.product.id === data.id
          });
          isExistWishlist = existWishlist ? existWishlist.id : 0;

          if(isExistWishlist != 0){
            errorNotification("Produk sudah ada dalam wishlist anda")
          } else {
            const api = axiosSetup(token);
            api.post("/restricted/customer/wishlist", {
              product_id: data.id,
            }).then((response) => {
              console.log(response.data.data);
              successNotification("Berhasil menambahkan product ke wishlist");
            }).catch((error) => {
              console.log(error.response);
              errorNotification(error.response ? error.response.data.message : error.response)
            });
          }
        }
      }).catch(error => {
        console.log(error.response);
      });
    }
  }

  const handleAddToCart = (data) => (e) => {
    const api = axiosSetup(token);
    if(!token){
      window.location = `/`;
    } else {
      api.post("/restricted/customer/cart", {
        product_id: data.id,
        quantity: 1,
        product_prices: data.variant_prices - data.discount,
        variant_id: data.variant_id,
        notes: ""
      }).then((response) => {
        console.log(response.data.data);
        successNotification("Produk berhasil ditambahkan ke keranjang")
      }).catch((error) => {
        console.log(error.response);
        errorNotification(error.response ? error.response.data.message : error.response);
      });
    }
  }

  return (
    <div className="container text-center h-full">
      <div className="product-image">
        <Link href={`/product/${slug}`}>
          {/* <ReactPlaceholder type={'rect'} ready={!loading} showLoadingAnimation={true} rows={1} style={{height: '250px'}}> */}
            <img src={image_url} alt="" />
          {/* </ReactPlaceholder> */}
          {/* <ImageLoading
            className={`w-full object-cover`}
            src={`${image_url}`}
            alt={``}
            ratio={1/1}
          /> */}
        </Link>
        <div className="mini-action">
          <div>
            <button onClick={handleAddToCart(data)}><img src="/images/ic_cart.png" /></button>
            <button onClick={handleAddToWishlist(data)}><img src="/images/ic_action_like.png" /></button>
          </div>
        </div>
      </div>
      <Link href={`/product/${slug}`}>
        <div className="product-name" style={{minHeight: '48px'} }>
          { product_name }
        </div>
      </Link>
      <Link href={`/brand/${seller_slug}`}>
        <div className="merchant">
          { seller_name }
        </div>
      </Link>
      <div className="container-price">
        <span className="price" style={discount > 0 ? {display: "inline"} : {display: "none"}}>{`Rp ${formatNumber(price)}`}</span>
        <span className="price-selling">{`Rp ${formatNumber(price - discount)}`}</span>
      </div>
      <style jsx>{style}</style>
    </div>
  );
};

CardTodayDeals.propTypes = {
  data: PropTypes.object
};

export default CardTodayDeals;
