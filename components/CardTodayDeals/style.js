import css from 'styled-jsx/css';

export default css`
.container {
  cursor: pointer;
  width: 100%;
  padding: 10px;
  background: #FFFFFF;
  border: 1px solid #ECECEC;
  box-sizing: border-box;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.13);
  border-radius: 4px;
  margin-bottom: 5px;
  max-width:100%;
}

.product-name {
  margin-top: 20px;
  font-weight: bold;
  font-size: 16px;
  text-align: center;
  color: #696969;
}
.variant-name{
  font-size:16px;
  color:#696969;
}

.merchant {
  font-weight: 500;
  font-size: 16px;
  text-align: center;
  color: #696969;
  margin-top: 8px;
}

.comments {
  font-size: 10px;
  text-align: center;
  color: #868686;
  margin-left: 5px;
}

.container-price {
  margin-top: 10px;
  text-align: center;
}

.price {
  font-weight: 500;
  font-size: 14px;
  text-align: center;
  color: #BDBDBD;
  margin-right: 10px;
  text-decoration-line: line-through;
}

.price-selling {
  font-weight: 500;
  font-size: 18px;
  color: #010101;
}
.product-image{
  position:relative;
}
.mini-action{
  position:absolute;
  bottom:0;
  right:5px;
  opacity:0;
  transition:0.2s all ease;
  -webkit-transition:0.2s all ease;
  -moz-transition:0.2s all ease;
  -ms-transition:0.2s all ease;
  -o-transition:0.2s all ease;
}
.mini-action button{
  background:#ccc;
  width:50px;
  height:50px;
  padding:5px;
  display:block;
  margin-bottom:5px;
}
.mini-action button img{
  display:block;
  position:relative;
  margin:0 auto;
}
.container:hover .mini-action{
  opacity:1;
}

@media (min-width: 768px) and (max-width: 1024px) {
}

@media (min-width: 320px) and (max-width: 767px) {
}
`;
