import css from 'styled-jsx/css';

export default css`
	.c {
		width: 100%;
		display: flex;
		flex-wrap: wrap;
		margin: 10px 0px;
	}

	.c-title {
		width: 100%;
		background: rgba(245, 245, 245, 0.75);
		font-weight: 500;
		font-size: 18px;
		color: #696969;
		padding: 12px 14px;
		display: flex;
		flex-wrap: wrap;
		justify-content: space-between;
		align-items: center;
	}

	.c-divider {
		width: 100%;
		height: 1px;
		background: rgba(217, 217, 217, 0.75);
	}

	.c-content {
		width: 100%;
		font-size: 14px;
		color: #868686;
	}

	.content {
		padding: 6px 8px;
		width: 100%;
		display: flex;
		flex-wrap: wrap;
		justify-content: space-between;
	}
`;
