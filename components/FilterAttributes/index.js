import React, { useState } from 'react';
import style from './style';

const Index = ({ title, data, handleFilter, valuesFilter }) => {
  const x = {};

  const onChecked = (event) => {
    const valueObj = {
      attribute: title,
      value: event.currentTarget.value
    };

    handleFilter(valueObj, event.currentTarget.checked);
  }

  return (
    <div className="c">
      <div className="c-title">
      	{title}
        <img src="/images/ic_arrow_up.png" alt="" />
      </div>
      <div className="c-divider" />
      <div className="c-content">
        {
          data ? data.map((v) => (
            <div className="content" key={v.value}>
              <div>
              <input type="checkbox" onChange={onChecked} id={v} value={v.value}
                checked={valuesFilter && valuesFilter.find(data => data.value.includes(v.value)) ? true : false}
              />
                <span style={{ marginLeft: '4px' }}>{v.value}</span>
              </div>
              {`(${v.product_count ? v.product_count : 0})`}
            </div>
          )) : ""
        }
      </div>

      <style jsx>{style}</style>
    </div>
  );
};

export default Index;
