import { useState } from 'react'
import { connect } from 'react-redux'
import Link from 'next/link'
import { deauthenticate } from 'redux/actions/authActions'

const ProfileDropdown = ({ token, mobile, deauthenticate, authentication } : any) => {
  const [isOpen, setOpen] = useState(false)

  return (
    <div className="relative">
      {
        mobile ? (
            <Link href={token ? "/account" : "/signin"}>
              <img className="pointer" src="/images/ic_profile.png" alt="" width="20" style={mobile ? { margin: '0px auto' } : { margin: '0px 8px' }} />
            </Link>
        ) : (
          token ? (
            <img className="pointer" src="/images/ic_profile.png" alt="" style={mobile ? { margin: '0px auto' } : { margin: '0px 8px' }} onClick={() => setOpen(v => !v)}/>
          ) : (
            <Link href="/signin">
              <img className="pointer" src="/images/ic_profile.png" alt="" style={mobile ? { margin: '0px auto' } : { margin: '0px 8px' }} />
            </Link>
          )
        )
      }
      
      {/* dropdown */}
      {
        isOpen && (
          <div className="divide-y-2 divide-gray-100 absolute bg-white right-0 mt-2 shadow-md w-56 text-text-primary rounded z-10 dropdown-user-menu">
            <Link href="/account">
              <div className="w-full px-3 py-2 flex items-center cursor-pointer hover:bg-gray-100">
                <img className="mr-3" src="/images/ic_account.png" alt=""/>
                Akun Saya
              </div>
            </Link>
            {
              authentication.user.seller.id != 0 && (
                <Link href="/backoffice/dashboard">
                  <div className="w-full px-3 py-2 flex items-center cursor-pointer hover:bg-gray-100">
                    <img className="mr-3" src="/images/ic_dashboard.png" alt=""/>
                    Dashboard Seller
                  </div>
                </Link>
              )
            }
            <div className="w-full px-3 py-2 flex items-center cursor-pointer hover:bg-gray-100" onClick={deauthenticate}>
              <img className="mr-3" src="/images/ic_logout.png" alt=""/>
              Keluar
            </div>
          </div>
        )
      }
      <style jsx>
      {`
        @media (min-width: 320px) and (max-width: 767px) {
            .dropdown-user-menu{
                top:-150px;
            }
        }
      `}
      </style>
    </div>
  )
}

const mapStateToProps = (state) => ({
  isAuthenticated: !!state.authentication.token,
  authentication: state.authentication
});


export default connect(
  mapStateToProps,
  {
    deauthenticate
  }
)(ProfileDropdown);
