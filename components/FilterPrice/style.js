import css from 'styled-jsx/css';

export default css`
  .c {
    width: 100%;
    margin-top: 20px;
    display: flex;
    flex-wrap: wrap;
    font-weight: 500;
    font-size: 18px;
    color: #696969;
  }

  .c-form {
    display: flex;
    justify-content: center;
    font-size: 12px;
    color: #868686;
    align-items: center;
    margin: 10px 0px;
  }

  .form-price {
    width: 70px;
    background: rgba(248, 248, 248, 0.75);
    border: 1px solid #F2F2F2;
    box-sizing: border-box;
    font-size: 12px;
    color: #868686;
    padding: 8px 8px;
  }
`;
