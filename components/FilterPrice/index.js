import React, { useState } from 'react';
import Slider, { Range } from 'rc-slider';
import style from './style';
import { formatNumber } from '~/util/helper';

const Index = ({callbackSlider, priceRange}) => {

  const [formPrice, setFormPrice] = useState({
    min: priceRange ? priceRange.min : 0,
    max: priceRange ? priceRange.max : 1000000
  })

  const handleChangePrice = (name) => (e) => {
    const { value } = e.target;
    setFormPrice({ ...formPrice, [name]: value });
    callbackSlider(name === "min" ? value : formPrice.min, name === "max" ? value : formPrice.max)
  }

  const handleSlidePrice = (event) => {
    setFormPrice({
      min: event[0],
      max: event[1]
    })
    callbackSlider(event[0], event[1]);
  }

  return (
  <div className="c">
    Harga
    <div className="w-100" style={{ marginTop: '8px' }}>
      <Range
        min={0}
        max={1000000}
        defaultValue={[formPrice.min, formPrice.max]}
        trackStyle={[{ backgroundColor: '#010101' }]}
        handleStyle={[{ backgroundColor: '#010101', borderColor: '#010101' }, { backgroundColor: '#010101', borderColor: '#010101' }]}
        onAfterChange={handleSlidePrice}
      />
    </div>
    <div className="w-100 c-form">
      <span style={{ marginRight: '8px' }}>Rp</span>
      <input className="form-price" type="number" id="price-min" value={formPrice.min} max={formPrice.max} min={0} onChange={handleChangePrice("min")} />
      <span style={{ margin: '0px 8px' }}> - </span>
      <span style={{ marginRight: '8px' }}>Rp </span>
      <input className="form-price" type="number" id="price-max" value={formPrice.max} onChange={handleChangePrice("max")} />
    </div>
    <style jsx>{style}</style>
  </div>
)
};

export default Index;
