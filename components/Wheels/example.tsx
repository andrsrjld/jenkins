import { useEffect } from 'react'
import CircleType from 'circletype'

const Wheels = () => {

  useEffect(() => {
    circularAdvance('circTxt', "ADVANCE", 200, 0, 'red',);
    circularAdvance('advance', "", 100, 0, 'white');

    circularBeginner('circTxtBeginner', "BEGINNER", 200, 0, 'red',);
    circularBeginner('beginner', "", 100, 0, 'white');
  }, []);

  function circularAdvance(id, txt, radius, classIndex, color) {
    txt = txt.split(""),
      classIndex = document.getElementsByClassName(id)[classIndex];

    var deg = 1,
      origin = 0;

    let foo = new Array(180).fill('');
    foo.forEach((ea) => {
      ea = `<span style='background-color:${color};width:4px;height:${radius}px;position:absolute;transform:rotate(${origin}deg);transform-origin: bottom center;'>${ea}</span>`;
      classIndex.innerHTML += ea;
      origin += deg;
    });

    var deg2 = 180 / txt.length,
      origin2 = deg2  / 2;

    txt.forEach((ea) => {
      ea = `<span style='color:white;font-weight:bold;vertical-align: middle;line-height: ${radius/2}px;height:${radius}px;position:absolute;transform:rotate(${origin2}deg);transform-origin: bottom center;'>${ea}</span>`;
      classIndex.innerHTML += ea;
      origin2 += deg2;
    });
  }

  function circularBeginner(id, txt, radius, classIndex, color) {
    txt = txt.split(""),
      classIndex = document.getElementsByClassName(id)[classIndex];

    var deg = 1,
      origin = 180;

    let foo = new Array(180).fill('');
    foo.forEach((ea) => {
      ea = `<span style='background-color:${color};width:4px;height:${radius}px;position:absolute;transform:rotate(${origin}deg);transform-origin: bottom center;'>${ea}</span>`;
      classIndex.innerHTML += ea;
      origin += deg;
    });

    var deg2 = 180 / txt.length,
      origin2 = deg2 / 2 + 180;

    txt.forEach((ea) => {
      ea = `<span style='color:white;font-weight:bold;vertical-align: middle;line-height: ${radius/2}px;height:${radius}px;position:absolute;transform:rotate(${origin2}deg);transform-origin: bottom center;'>${ea}</span>`;
      classIndex.innerHTML += ea;
      origin2 += deg2;
    });
  }
  
  return (
    // <!--just a container used to position in the page-->
    <div className="w-100 flex justify-center h-screen text-2xl">
      {/* <!--the holders for the text, reuse as desired--> */}
      <div className="w-full flex justify-end mr-2 h-full items-center bg-red-200">
        <div className="cursor-pointer">
          <div className="circTxtBeginner" id="test" style={{ marginTop: '-200px' }}></div>
        </div>
        <div className="w-auto">
          <div className="beginner" id="test" style={{ marginTop: '-100px' }}></div>
        </div>
      </div>
      <div className="w-full flex justify-start ml-2 h-full items-center bg-red-300">
        <div className="cursor-pointer">
          <div className="circTxt" id="test" style={{ marginTop: '-200px' }}></div>
        </div>
        <div className="w-auto">
          <div className="advance" id="test" style={{ marginTop: '-100px' }}></div>
        </div>
      </div>
    </div>
  )
}

export default Wheels