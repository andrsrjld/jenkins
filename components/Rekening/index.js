import React, { useState, useRef, useEffect } from 'react';
import Flicking from "@egjs/react-flicking";
import "@egjs/react-flicking/dist/flicking.css";
import { formatNumber, formatCurrencyToNumber } from '~/util/helper';
import Modal from '~/components/Modal';
import { transferByRekening, transferByPhone } from '~/data/api/user';
import {successNotification, errorNotification} from '~/util/toastr-notif';

export default (props) => {
  const [panels, setPanels] = useState(props.data);
  const [modal, setModal] = useState(false)
  const [form, setForm] = useState({
    destination: '',
    amount: 0,
    deskripsi_transaksi: '',
    type: '1',
    phone: ''
  })

  useEffect(() => {
    setPanels(props.data)
  }, [props.data])

  const handleClick = () => {
    setModal(true)
  }

  const submit = async (e) => {
    const body = {
      destination: form.destination,
      amount: Number(form.amount),
      deskripsi_transaksi: form.deskripsi_transaksi
    }
    try {
      let res = ""
      if (form.type === '1') {
        res = await transferByRekening(props.token, body)
      } else {
        body.destination = form.phone
        res = await transferByPhone(props.token, body)
      }
      if (res.code === 500) {
        errorNotification(res.error)
      } else {
        successNotification("Sukses Transfer")
        props.callback()
      setModal(false)
      }
    } catch (error) {
      errorNotification(error.response ? error.response.data.message : error.response)
    }
  }

  const handleChangeFormPersonal = (name) => (e) => {
    const { value } = e.target;
    setForm({ ...form, [name]: value });
  };

  const handleChangePrice = (e) => {
    const { value } = e.target
    var reg = /^\d+$/;
    if (reg.test(value.substr(value.length - 1)) || value.length === 3) {
      const val = formatCurrencyToNumber(value)
      setForm({...form, amount: val})
    }
  }

  const setSelectedRekening = value => {
    setForm({ ...form, destination: value });
  }

  const setSelectedRekeningType = value => {
    setForm({ ...form, type: value });
  }

  const handleChangeFormPhone = (e) => {
    setForm({...form, phone: e.target.value})
  }

  return <>
    <div>

    <Flicking renderOnlyVisible={true} hideBeforeInit={true}>
      {
        panels.map(data =>
          <div className="w-3/4 lg:w-1/2 xl:w-1/2 account-content mb-4 border border-text-primary mx-3 rounded-lg h-full">
            <div className="card-content h-full">
              <div className="section h-full">
                <div className="header flex flex-col justify-between" style={{marginBottom: '0px'}}>
                  <div className="title w-full mb-4 flex justify-between">
                    <div>
                      {data.deskripsi_produk}
                    </div>
                    <div>
                      No. Rekening
                    </div>
                  </div>
                  <div className="title w-full flex justify-between" style={{ fontWeight: 'bold' }}>
                    <div>
                      Rp. {formatNumber(data.saldo_akhir)}
                    </div>
                    <div>
                      {data.no_rekening}
                    </div>
                  </div>
                </div>
                {
                  data.deskripsi_produk === "Saldo Belanja" && (
                    <div className="w-full mt-4 flex justify-end">
                      <button className="btn btn-primary border border-primary" style={{ width: 'auto', padding: '6px 12px', fontSize: '14px' }} onClick={handleClick}>Transfer</button>
                    </div>
                  )
                }
              </div>
            </div>
          </div>
        )
      }
    </Flicking>
      <Modal open={modal} onClose={() => setModal(false)}>
        <div className="card-modal" style={{ padding: '16px 26px' }}>
          <div style={{ textAlign: 'right' }}>
            <div style={{ display: 'inline-block' }} onClick={() => setModal(false)} onKeyDown={() => setModal(false)} className="pointer" role="button" tabIndex={0}>
              <img src="/images/ic_close_red.png" alt="" />
            </div>
          </div>
          <div className="title" style={{ marginTop: '16px' }}>
            Transfer
          </div>
          <form className="w-full">
            <div style={{ marginTop: '24px' }} className="w-full">
              Tujuan Transfer
              <div className="w-full flex mt-2 text-text-primary">
                <div className="border border-gray-400 rounded-l flex items-center h-full w-full">
                    <select className="py-2 px-5 w-full" value={form.type} onChange={(e) => setSelectedRekeningType(e.target.value)}>
                      <option value="1">Antar Rekening</option>
                      <option value="2">Antar Anggota</option>
                    </select>
                </div>
              </div>
            </div>
            {
              form.type === '1' && (
                <div style={{ marginTop: '24px' }} className="w-full">
                  Tujuan Rekening
                  <div className="w-full flex mt-2 text-text-primary">
                    <div className="border border-gray-400 rounded-l flex items-center h-full w-full">
                        <select className="py-2 px-5 w-full" value={form.destination} onChange={(e) => setSelectedRekening(e.target.value)}>
                          <option value="" disabled>Pilih Rekening Tujuan</option>
                          {
                            props.data.filter(v => v.deskripsi_produk === 'Simpanan Pokok' || v.deskripsi_produk === 'Simpanan Wajib').map(v => (
                              <option key={v.no_rekening} value={v.no_rekening}>{v.deskripsi_produk} ({ v.no_rekening })</option>
                            ))
                          }
                        </select>
                    </div>
                  </div>
                </div>
              )
            }
            {
              form.type === '2' && (
                <div style={{ marginTop: '24px' }}>
                  Nomor Telepon Tujuan
                  <input type="number" className="form-control w-full mt-2" placeholder="08----" value={form.phone} onChange={handleChangeFormPhone} />
                </div>
              )
            }
            <div style={{ marginTop: '24px' }}>
              Jumlah
              {/* <input className="form-control w-full mt-2" placeholder="Jumlah" type="number" value={form.amount} onChange={handleChangeFormPersonal("amount")} /> */}
              <input className="form-control w-full mt-2" placeholder="Rp" value={'Rp ' + formatNumber(form.amount)} onChange={(e) => handleChangePrice(e)}/>
            </div>
            <div style={{ marginTop: '24px' }}>
              Deskripsi
              <input className="form-control w-full mt-2" placeholder="Deskripsi" value={form.deskripsi_transaksi} onChange={handleChangeFormPersonal("deskripsi_transaksi")} />
            </div>
            <div style={{ marginTop: '24px' }}>
              <button className="btn-primary" type="button" onClick={submit} >TRANSFER</button>
            </div>
          </form>
          <div className="divider" style={{ margin: '24px' }} />
        </div>
      </Modal>
    </div>
    <style jsx>
    {`.card-content {
        background: #FFFFFF;
        box-shadow: 0px 0px 4px rgba(0, 0, 0, 0.25);
        border-radius: 0px 7px 7px 7px;
        padding: 26px 32px;
      }

      .header {
        display: flex;
        justify-content: space-between;
      }

      .title {
        font-weight: 500;
        font-size: 22px;
        line-height: 21px;
        letter-spacing: 0.05em;
        color: #696969;
      }`}
    </style>
  </>
};