import React, { useEffect, useState } from 'react';
import ReactGA from 'react-ga';
import Header from '~/components/Header';
import Footer from '~/components/Footer';
import FooterMobileNav from '~/components/FooterMobileNav';
import FooterMobile from '~/components/FooterMobile';
import 'react-placeholder/lib/reactPlaceholder.css';
import WAFAB from '~/components/WAFAB'

const BaseLayout = ({ children, productType, token, cart }) => {
  const [loading, setLoading] = useState(true);

  // useEffect(() => {
  //   // Google Analytic Initialize
  //   ReactGA.initialize('UA-178768959-1');
  //   ReactGA.pageview(window.location.pathname + window.location.search);

  //   window.addEventListener('load', () => {
  //     setLoading(false);
  //   });
  // },[])
  
  return (
    <div>
      <Header productType={productType} token={token} cart={cart} />
      <div className="container mx-auto">
        {children}
      </div>
      <Footer />
      <FooterMobile />
      <FooterMobileNav token={token} />

      <WAFAB />
    </div>
  );
}

export default BaseLayout;
