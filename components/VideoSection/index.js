import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import style from './style';
import { useMediaQuery } from 'react-responsive';
import ItemsCarousel from 'react-items-carousel';
import ReactPlaceholder from 'react-placeholder';

const ArticleSection = ({ data }) => {  

    const [activeItemIndex, setActiveItemIndex] = useState(0);
  const chevronWidth = 50;
  const isTablet = useMediaQuery({ query: '(max-width: 1024px)' });
  const isMobile = useMediaQuery({ query: '(max-width: 480px)' });

  const [loading, setLoading] = useState(true);

  useEffect(() => {
    window.addEventListener('load', () => {
      setLoading(false);
    });
  },[]);

  const getCountContent = () => {
    if (isMobile) { return 1; }
    if (isTablet) return 1;
    return 1;
  };

  const ButtonNavigation = (icon) => (
    <div
      className="pointer "
      style={{
        border: '8px', backgroundColor: '#FFFFFF'
      }}
    >
      {
        icon === 'left'
          ? (
            <svg id="i-chevron-left" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" width="32" height="32" fill="none" stroke="#BDBDBD" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2">
              <path d="M20 30 L8 16 20 2" />
            </svg>
          )
          : (
            <svg id="i-chevron-right" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" width="32" height="32" fill="none" stroke="#BDBDBD" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2">
              <path d="M12 30 L24 16 12 2" />
            </svg>
          )
      }
      {/* <img src={`${icon}`} alt={icon} style={{ height: '24px' }} /> */}
    </div>
  );

  return (
    <div className="c container mx-auto">
        <div className="flex flex-wrap justify-center">
            <span className="w-full title text-center">VIDEO</span>
            <hr className="divider" />
        </div>

      <div className="w-100" />

      <div className="container carousel-wrapper">
        <ItemsCarousel
            requestToChangeActive={setActiveItemIndex}
            activeItemIndex={activeItemIndex}
            numberOfCards={getCountContent()}
            gutter={10}
            leftChevron={ButtonNavigation('left')}
            rightChevron={ButtonNavigation('right')}
            outsideChevron
            chevronWidth={chevronWidth}
            infiniteLoop
        >
            {
                data ? data.map((v) => (
                    <div key={v.url}>
                        <div className="container-content">
                          <ReactPlaceholder type={'rect'} ready={!loading} showLoadingAnimation={true} rows={1} style={{height: '250px'}}>
                            <iframe src={v.url} frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
                          </ReactPlaceholder>
                        </div>
                    </div>
                )) : ""
            }
        </ItemsCarousel>
      </div>
      <style jsx>{style}</style>
    </div>
  );
};

ArticleSection.propTypes = {
  data: PropTypes.array
};

export default ArticleSection;
