import css from 'styled-jsx/css';

export default css`

.container {
  width: 100%;
  padding: 0px 50px;
  max-width: 1200px;
}

.c {
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  width: 100%;
  padding:0 !important;
}

.title {
  font-weight: bold;
  font-size: 22px;
  color: #696969;
}

.divider {
  height: 4px;
  background-color: #010101;
  border: none;
  border-radius: 9px;
  width: 100px;
}

.content {
  width: 100%;
  display: flex;
  justify-content: center;
  margin-top: 60px;
}

.container-content {
  margin: 10px 0;
  text-align: left;
  cursor: pointer;
  padding: 0 15px;
}

.image {
    width: 100%;
}

.date {
    margin-top: 8px;
    font-size: 18px;
    color: #696969;
    
}

.author {
    color: #010101;
}

.title {
    font-weight: bold;
    font-size: 22px;
    color: #696969;
    margin-top: 10px;
}

.desc {
  margin-top: 10px;
  font-size: 20px;
    color: #696969; 
}

.btn-action {
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  color: #FFFFFF;
  padding: 0.5rem 2rem !important;
}

.divider-bottom {
    height: 1px;
    background-color: #010101;
    border: none;
    border-radius: 9px;
    margin-top: 8px;
  }

.container-bottom {
    width: 100%;
    margin-top: 8px;
    display: flex;
    justify-content: space-between;
    font-size: 18px;
    color: #696969;
}
.carousel-wrapper{
  margin-top:30px;
  padding:0;
}
.container-content iframe{
  width:100%;
  height:600px;
}

@media (min-width: 768px) and (max-width: 1024px) {
}

@media (min-width: 320px) and (max-width: 767px) {
  .c{
    padding:0 50px !important;
  }
  .container-content iframe{
    width:100%;
    height:160px;
  }
}
`;
