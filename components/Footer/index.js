import React from 'react';
import { connect } from 'react-redux'
import Link from 'next/link';
import style from './style.js';

const Footer = (props) => (
  <div className="w-full p-0 block xs:hidden sm:hidden md:block lg:block" style={{ background: '#F5F5F5' }}>
    <div className="w-full p-8 xs:p-0 sm:p-0 flex flex-wrap items-start container mx-auto">
      <div className="flex flex-wrap mt-4 w-1/3 justify-center text-center">
        <div className="xs:text-left sm:text-left">
          <span className="menu-title"><b>Bapera</b></span>
          <Link href="/about">
            <div style={{ marginTop: '12px', padding: '6px' }}>
              <span className="menu">Tentang BAPERA</span>
            </div>
          </Link>
          <Link href="/terms">
            <div style={{ padding: '6px' }}>
              <span className="menu">Syarat & Ketentuan</span>
            </div>
          </Link>
          <Link href="/privacy">
            <div style={{ padding: '6px' }}>
              <span className="menu">Kebijakan Privasi</span>
            </div>
          </Link>
          <Link href="/seller-guidance">
            <div style={{ padding: '6px' }}>
              <span className="menu">Panduan Penjual</span>
            </div>
          </Link>
          <Link href="/buyer-guidance">
            <div style={{ padding: '6px' }}>
              <span className="menu">Panduan Pembeli</span>
            </div>

          </Link>
        </div>
      </div>
      <div className="w-1/3 flex justify-center flex-wrap text-center items-center">
        <div>
          <div className="flex justify-center flex-wrap text-center items-center pb-3">
            <img src="/images/ic_bapera.png" alt="Bapera" className="h-16" />
          </div>
          <span className="menu-title">BERLANGGANAN NEWSLETTER</span>
          <div style={{ marginTop: '12px', padding: '6px' }}>
            <span className="menu">Berlangganan newsletter untuk</span>
          </div>
          <div style={{ padding: '6px' }}>
            <span className="menu">mendapatkan informasi tentang</span>
          </div>
          <div style={{ padding: '6px' }}>
            <span className="menu">promo dan produk terbaru</span>
          </div>
          <div style={{ padding: '6px' }}>
            <input
              className="form-control"
              placeholder="Alamat Email Anda"
              style={{
                width: '100%', padding: '16px 20px', fontWeight: '300', fontSize: '12px'
              }}
            />
          </div>
        </div>
      </div>
      <div className="w-1/3 flex justify-center flex-wrap text-center mt-4">
        <div>
          <span className="menu-title">LAYANAN</span>
          {
            props.isAuthenticated && (
              <Link href="/account">
                <div style={{ marginTop: '12px', padding: '6px' }}>
                  <span className="menu">Akun Saya</span>
                </div>
              </Link>
            )
          }
          <Link href="/contact-us">
            <div style={{ padding: '6px' }}>
              <span className="menu">Hubungi Kami</span>
            </div>
          </Link>
          <Link href="/how-to-pay">
            <div style={{ padding: '6px' }}>
              <span className="menu">Cara Pembayaran</span>
            </div>
          </Link>
          {/* <Link href="/seller-agreements">
            <div style={{ padding: '6px' }}>
              <span className="menu">Seller Agreements</span>
            </div>
          </Link> */}

        </div>
      </div>
    </div>

    {/* <div className="block w-100 text-center">
      <span className="menu-title">INFORMASI</span>
      <br />
      <div className="w-full flex justify-center items-center mt-2">
        <img src="/images/ic_twitter.png" alt="twitter"/>
        <img src="/images/ic_facebook_outline.png" alt="facebook" style={{ marginLeft: '10px' }} />
        <a href="https://www.instagram.com/lakkon_id"><img src="/images/ic_instagram.png" alt="instagram" style={{ marginLeft: '10px' }} /></a>
        <img src="/images/ic_wa.png" alt="whatsapp" style={{ marginLeft: '16px' }} />
      </div>
    </div> */}
    <div className="w-100 text-center" style={{ padding: '26px' }}>
      <span className="menu-title">
        Copyright &copy; 2020
        {' '}
        <span style={{ color: '#010101' }}>Bapera</span>
        {' '}
        . All rights reserved
      </span>
    </div>
    <style jsx>{style}</style>
  </div>
);

const mapStateToProps = (state) => ({
  isAuthenticated: !!state.authentication.token,
  authentication: state.authentication
});

export default connect(
  mapStateToProps
)(Footer);