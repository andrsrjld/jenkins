import css from 'styled-jsx/css';

export default css`

.container-menu {
    display: flex;
    text-align:center;
    align-items: center;
    justify-content:space-evenly;
}

.menu-title {
    font-weight: bold;
    font-size: 14px;
    text-align: center;
    color: #555555;
}

.menu {
    font-size: 14px;
    text-align: center;
    color: #555555;
    padding: 20px;
    cursor: pointer
}

@media (min-width: 768px) and (max-width: 1024px) {
}

@media (min-width: 320px) and (max-width: 767px) {
    .menu{
        padding: 20px 0;
    }
}
`;
