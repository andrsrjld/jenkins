import React, { useState, useEffect } from 'react';
import ReactGA from 'react-ga';
import Head from 'next/head';
import Link from 'next/link';
import { connect } from 'react-redux';
import { Carousel } from 'react-responsive-carousel';
import SliderItem from '~/components/SliderItem';
import SaleItem from '~/components/SaleItems';
import TabbedContent from '~/components/TabbedContent';
import FavoriteSection from '~/components/FavoriteSection';
import CustomerSays from '~/components/CustomerSays';
import ArticleSection from '~/components/ArticleSection';
import InstagramSection from '~/components/InstagramSection';
import VideoSection from '~/components/VideoSection';
import Footer from '~/components/Footer';
import FooterMobile from '~/components/FooterMobile';
import ProfileDropdown from '~/components/ProfileDropdown';
import { getProductType } from '~/data/api/product-type';
import { getBanners } from '~/data/api/banner';
import { getAds } from '~/data/api/ads';
import { getFlashSaleProduct, getFlashSaleSeller } from '~/data/api/flashsale';
import { getTestimonials } from '~/data/api/testimonial';
import { getArticles } from '~/data/api/article';
import MobileNav from '~/components/MobileNav';
import FooterMobileNav from '~/components/FooterMobileNav';
import Loading from '~/components/Loading/LoadingFull';
import ReactPlaceholder from 'react-placeholder';
import 'react-placeholder/lib/reactPlaceholder.css';
import WAFAB from '~/components/WAFAB'
import CardProduct from '~/components/CardProduct';
import "@egjs/react-flicking/dist/flicking.css";

import { getCart } from '~/data/api/cart';
import axiosSetup from '~/services/api';


// Toastr Notification
import {successNotification, errorNotification} from '~/util/toastr-notif';

import {
  dummySlidersData,
  dummySaleData,
  dummyTodayDealsData,
  dummyPelakkonData,
  dummyCustomerSaysData,
  dummyArticlesData,
  dummyInstagramData
} from '~/util/DataHelper';

const Home = ({ authentication, productType, ads, flashProduct, flashSeller, testimonial, article, cart, video, products }) => {
  const [sliders, setSliders] = useState([]);
  const [saleItems, setSaleItems] = useState(ads);
  const [todayDeals, setTodalDeals] = useState(flashProduct);
  const [pelakkonData, setPelakkonData] = useState(flashSeller);
  const [customer, customerData] = useState(testimonial);
  const [articles, setArticles] = useState(article);
  const [instagram, setInstagram] = useState([]);
  const [loading, setLoading] = useState(true);

  const [activeScroll, setActiveScroll] = useState(false);
  const [scrollTop, setScrollTop] = useState(0);

  const {token} = authentication
  const {apiUrl} = authentication?.endpoint

  const onScroll = (e) => {
    setScrollTop(e.target.documentElement.scrollTop);
    setActiveScroll(e.target.documentElement.scrollTop > 150);
  }

  // useEffect(() => {
  //   // Google Analytic Initialize
  //   ReactGA.initialize('UA-178768959-1');
  //   ReactGA.pageview(window.location.pathname + window.location.search);
    
  //   window.addEventListener('scroll', onScroll);
  //   // setTimeout(() => {
  //     // var feed = new Instafeed({
  //     //   accessToken: 'e979dbeea289bbd1eb4bc39bd2efe6fc'
  //     // });
  //     // feed.run();

  //     // console.log("FEED")
  //   // }, 2000);
  // },[]);

  useEffect(() => {
    getSliders()
    getInstagram();
    setTimeout(() => {
      setLoading(false);
    }, 1000);
  }, []);

  const getSliders = async () => {
    const params = {
      layout_type: "list_layout",
      status: "published"
    }

    const res = await getBanners(apiUrl, token, params);
    if(res.code == 200){
      setSliders(res.data);
    } else {
      errorNotification(res.error);
    }
    // setLoading(false);
  };

  const getInstagram = () => {
    setInstagram(dummyInstagramData);
  };

  const [keyword, setKeyword] = useState('');

  const handleSearch = (e) => {
    setKeyword(e.currentTarget.value);
  }

  const handleSubmitSearch = (e) => {
    e.preventDefault();
    if(keyword !== ""){
      window.location = `/search/${keyword}`;
    }
  }

  return (
    <div className="w-full">
      <Loading show={loading}/>
      <Head>
        <title>Bapera</title>
        <link rel="icon" href="/favicon.ico" />
        <script type="text/javascript" src="https://cdn.jsdelivr.net/gh/stevenschobert/instafeed.js@2.0.0rc1/src/instafeed.min.js"></script>
        <script src="/js/InstagramFeed.min.js"></script>
      </Head>

      {/* <Loading show={loading} opacity={false} /> */}
      <div className="w-full">
        <MobileNav productType={productType} />
        <div className="w-full absolute top-0 left-0 z-10 flex flex-wrap pt-4 xs:hidden sm:hidden md:block lg:block">
          <div className="w-full flex items-center xs:hidden sm:hidden md:block lg:block">
            <div className="w-full px-8">
              <div className="w-auto flex py-1 items-center border-b-2 border-text-primary max-w-xs">
                <form className="flex w-full items-center" onSubmit={handleSubmitSearch}>
                  <input className="form-search w-full" placeholder="Cari Produk" onChange={handleSearch} required />
                  <button type="submit" className="btn-search">
                    <img src="/images/ic_search.png" alt="search" />
                  </button>
                </form>
              </div>
            </div>

            <div className="w-full flex justify-center">
              <img src="/images/logo_bapera.png" alt="Logo" className="h-16" />
            </div>

            <div className="w-full flex px-8">
              {
                authentication.token == null
                  ? (
                    <div className="w-full flex items-center justify-end">
                      <Link href="/signin">
                        <button type="button" className="btn-login" style={{ marginRight: '16px' }}>Masuk</button>
                      </Link>
                      <Link href="/cart">
                        <img className="pointer" src="/images/ic_cart.png" alt="" style={{ margin: '0px 8px' }} />
                      </Link>
                    </div>
                  )
                  : (
                    <div className="w-full flex items-center justify-end">
                      <Link href="/wishlist"><img className="pointer" src="/images/ic_love.png" alt="" style={{ margin: '0px 8px' }} /></Link>
                      <Link href="/cart">
                        <div className="cart-wrapper">
                          <img className="pointer" src="/images/ic_cart.png" alt="" style={{ margin: '0px 8px' }} />
                          <label>{cart ? cart.reduce((a, b) => {return a + b.carts.length}, 0) : 0}</label>
                        </div>
                      </Link>
                      <ProfileDropdown token={authentication.token} />
                    </div>
                  )
              }
            </div>
          </div>

          <div className="w-full">
            <div className="navigation">
              <div className="menu dropdown-belanja">
                BELANJA
                {' '}
                <img style={{ marginLeft: '6px' }} src="/images/arrow_down.png" alt="arrow-down" />

                <div className="container-belanja">
                  {
                    productType ? productType.map((v) => (
                      <Link href={v.slug !== 'brand' ? `/category/${v.slug}` : '/brand'} key={v.id}>
                        <div className="card-category">
                          <div className="text-center w-100 flex justify-center"><img src={`/images/ic_${v.name.toLowerCase().split(' ').join('-')}.png`} alt={v.slug} style={{ height: 48}}/></div>
                          <div style={{ marginTop: '8px' }}>{v.name}</div>
                        </div>
                      </Link>
                    )) : ""
                  }
                  <Link href={'/brand'}>
                    <div className="card-category">
                      <div className="text-center w-100 flex justify-center"><img src={`/images/ic_favorit3.png`} alt="brand" style={{ height: 48}}/></div>
                      <div style={{ marginTop: '8px' }}>Merk</div>
                    </div>
                  </Link>
                </div>
              </div>
              <Link href="/brand">
                <div className="menu">
                  MERK
                </div>
              </Link>
              <Link href="/about">
                <div className="menu">
                  TENTANG BAPERA
                </div>
              </Link>
            </div>
          </div>
        </div>
        <div className="mobile-header hidden xs:flex sm:flex">
          <MobileNav productType={productType} />
          <div className="w-full fixed top-0 left-0 z-10 flex flex-wrap pt-4 mobilenav-wrapper" style={{background: "#fff"}}>
            {/* Mobile Nav */}
            <div className="w-full flex items-center justify-between">
              
              <div className="w-full">
                &nbsp;
              </div>
              
              <div className="flex w-full justify-center">
                <Link href="/">
                  <img src="/images/logo_bapera.png" alt="Logo" className="h-16" />
                </Link>
              </div>

              <div className="px-8 w-full">
                <div className="w-auto flex py-1 items-center border-b-2 border-text-primary max-w-xs">
                  <form className="flex" onSubmit={handleSubmitSearch}>
                    <div>
                      <input className="form-control form-search w-100" placeholder="Cari" onChange={handleSearch} required />
                    </div>
                    <button type="submit" className="btn-search">
                      <img src="/images/ic_search.png" alt="search" />
                    </button>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <div className="hidden xs:block sm:block md:hidden lg:hidden" style={{height: "100px"}}></div>
        </div>

        <div className="container-slider">
          <ReactPlaceholder ready={!loading} type={'rect'} showLoadingAnimation delay={1000} style={{width: '100%', height:'400px'}}>
            <Carousel
              showArrows
              onChange={() => {}}
              onClickItem={(index) => {
                // window.location.href = `${sliders[index].link}`;
              }}
              onClickThumb={() => {}}
              showThumbs={false}
            >
              {
                sliders.map((v) => (<SliderItem data={v} key={v.id}/>))
              }
            </Carousel>
          </ReactPlaceholder>
        </div>
        
        <section className="homeblock-section">
          <SaleItem data={saleItems} />
        </section>
        
        <section className="flashsale-section">
          <TabbedContent todayDeals={todayDeals} pelakkon={pelakkonData} token={token} />
        </section>

        <div className="c-content-products">
            {
              products ? products.map((v) => (
                <CardProduct data={v} loading={loading} key={v.id}/>
              )) : <h4 style={{marginTop: "30px", marginBottom:"30px", textAlign:"center", fontSize:"20px", width: "100%"}}>Produk yang anda cari tidak ada</h4>
            }
        </div>
        
        {/* <section className="favorite-section">
          <FavoriteSection />
        </section> */}
        
        {/* <section className="testimonial-section">
          <CustomerSays data={customer} />
        </section> */}

        {/* <section className="article-section">
          <ArticleSection data={articles} />
        </section> */}

        {/* <section className="video-section">
          <VideoSection data={video} />
        </section> */}
        
        {/* <section className=".">
          <InstagramSection data={instagram} />
        </section> */}

        <div id="instafeed"></div>

        {/* <div style={{
          display: 'flex', justifyContent: 'center', width: '100%', marginTop: '80px'
        }}
        >
          <div>
            <span className="title">
              DAPATKAN
              {' '}
              <span style={{ color: '#010101' }}>DISKON 10%</span>
              {' '}
              UNTUK BELANJA SELANJUTNYA
            </span>
            <hr className="divider" />
          </div>
        </div>

        <div style={{
          display: 'flex', justifyContent: 'center', width: '100%', marginTop: '80px'
        }}
        >
          <div>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <img src="/images/ic_mail.png" alt="email" />
              <input className="form" placeholder="Masukkan Email Anda" />
              <span className="subscribe">SUBSCRIBE</span>
            </div>
            <hr className="divider-newsletter" />
          </div>
        </div> */}

      </div>

      <div style={{ height: '100px', width: '100%' }} />

      <Footer />

      <FooterMobile />

      <FooterMobileNav token={token} />

      <WAFAB />
      
      <style jsx>
        {`
        .c {
          width: 100%;
          display: flex;
          flex-wrap: wrap;
        }

        .title {
          font-weight: bold;
          font-size: 22px;
          color: #696969;
        }
        
        .divider {
          height: 4px;
          background-color: #010101;
          border: none;
          border-radius: 9px;
          width: auto;
        }

        .divider-newsletter {
          height: 4px;
          background-color: #696969;
          border: none;
          border-radius: 9px;
          margin-top: 8px;
        }
        
        .form {
          font-size: 18px;
          color: #696969;
          border: 0px;
          margin-left: 10px;
          min-width: 500px;
        }

        .subscribe {
          font-weight: 500;
          font-size: 18px;
          color: #696969;
          cursor: pointer;
        }

        .subscribe:hover {
          color: #010101;
        }

        .form-search {
          border: 0px;
          font-size: 16px;
          color: #696969;
          padding: 4px 16px;
          background: transparent;
        }

        .divider-search {
          height: 2px;
          background-color: #696969;
          border: none;
          border-radius: 9px;
          margin-top: 8px;
        }

        .navigation {
          width: 100%;
          display: flex;
          justify-content: center;
          margin-top: 20px;
          position: relative;
        }

        .menu {
          margin: 0px 16px;
          font-weight: 500;
          font-size: 20px;
          color: #696969;
          display: flex;
          align-items: center;
          cursor: pointer;
        }

        .btn-login {
          border: 2px solid #696969;
          box-sizing: border-box;
          border-radius: 7px;
          font-size: 18px;
          color: #696969;
          padding: 6px 18px;
          cursor: pointer;
          background: transparent;
        }

        .container-belanja {
          position: absolute;
          background: #FFFFFF;
          box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
          border-radius: 4px;
          top: 21px;
          width: 100%;
          max-width: 700px;
          display: none;
          padding: 24px;
        }

        .dropdown-belanja:hover .container-belanja{
          display: flex;
          flex-wrap: wrap;
        }

        .card-category{
          width: 21%;
          background: #FFFFFF;
          box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.13);
          display: flex;
          flex-wrap: wrap;
          justify-content: space-evenly;
          align-items:center;
          padding: 10px;
          margin: 10px;
          font-weight: 500;
          font-size: 14px;
          text-align: center;
          color: #696969;
        }
        .mobilenav-wrapper{
          padding:10px 0;
          transition: all 0.2s ease;
          -moz-transition: all 0.2s ease;
          -webkit-transition: all 0.2s ease;
          -o-transition: all 0.2s ease;
          -ms-transition: all 0.2s ease;
        }
        .cart-wrapper{
          position:relative;
        }
        .cart-wrapper label{
          position:absolute;
          top:-13px;
          right:-6px;
          background:#010101;
          border-radius:30px;
          -webkit-border-radius:30px;
          -moz-border-radius:30px;
          -ms-border-radius:30px;
          -o-border-radius:30px;
          color:#fff;
          width:25px;
          text-align:center;
        }
        .favorite-section, .video-section{
          background:#f5f5f5;
          padding:40px 0;
          margin-top:50px;
        }
        .article-section, .video-section, .instagram-section {
          padding:40px 0;
        }
        @media (min-width: 320px) and (max-width: 767px) {
          .form-search{
            padding: 0;
            border:none;
            box-shadow:none;
          }
        }

        .c-content-products {
          display: flex;
          flex-wrap: wrap;
        }
      `}

      </style>
    </div>
  );
};

Home.getInitialProps = async (ctx) => {
  const { token } = ctx.store.getState().authentication;
  const authentication = ctx.store.getState().authentication;

  const api = axiosSetup(token);

  const productTypeResp = await getProductType();
  const productType = productTypeResp.data;

  const adsRes = await getAds(token);
  const ads = adsRes.data ? adsRes.data : [];

  const flashProductRes = await getFlashSaleProduct(token);
  const flashProduct = flashProductRes.data ? flashProductRes.data : [];

  const flashSellerRes = await getFlashSaleSeller(token);
  const flashSeller = flashSellerRes.data ? flashSellerRes.data : [];

  const testimonialRes = await getTestimonials(token);
  const testimonial = testimonialRes.data ? testimonialRes.data : [];

  const articleRes = await getArticles(token, 'created_at', 'desc', '1', '12');
  const article = articleRes.data ? articleRes.data : [];

  const cartResp = await getCart(token);
  const cart = cartResp.data ? cartResp.data.cart : [];

  const productParam = `layout_type=list_layout&type_slug=elektronik-10&status=active&page=1&limit=12&order=date,DESC`;
  const productResp = await api.get('/products?' + productParam);
  const products = productResp.data.data;

  const video = [
    {
        title: "Lakkon (lapak kopi nusantara)",
        url: "https://www.youtube.com/embed/20Q16tRA87A"
    },
    // {
    //     title: "JELAJAH KOPI SEASON 1 - GAYO SERAMBI PENIKMAT KOPI PART 1",
    //     url: "https://www.youtube.com/embed/To2bUOVsGeM"
    // },
    // {
    //     title: "JELAJAH KOPI SEASON 1 - GAYO SERAMBI PENIKMAT KOPI PART 4",
    //     url: "https://www.youtube.com/embed/1Voq4So11QE"
    // }
  ]

  return {
    authentication,
    productType,
    ads,
    flashProduct,
    flashSeller,
    testimonial,
    article,
    cart,
    video,
    token,
    products
  };
}

export default connect(
  (state) => state, {}
)(Home);
