import React, { useEffect, useState, useRef } from 'react';
import Router from 'next/router';
import { useRouter } from 'next/router';
import {connect} from 'react-redux'
import Head from 'next/head';
import Link from 'next/link';
import Select from 'react-select/async';
import Modal from '~/components/Modal';
import MapPicker from '~/components/MapPicker';
import { validateEmail } from '~/util/helper';
import { apiRegister, apiRegisterSeller, apiSearchAddress, requestEmailVerification, confirmEmailVerification, authLookup, authRegister } from '~/util/ServiceHelper';
import {
  socialAuthenticate
} from '~/redux/actions/authActions';
import { requestOTP, validationOTP } from '~/data/api/otp'
import { GoogleLogin } from 'react-google-login';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'
import { validateSocial, loginSocial } from '~/data/api/social'

// Toastr Notification
import {successNotification, errorNotification} from '~/util/toastr-notif';

const Home = (props) => {
  const router = useRouter();
  const param = router.query;
  const {apiUrl} = props.authentication.endpoint
  const [step, setStep] = useState(1);
  const [form, setForm] = useState({
    firstName: props.firstName,
    lastName: props.lastName,
    email: props.email,
    phone: '',
    password: '',
    confirmPassword: '',
    isSeller: false,
    merchantName: '',
    identityNumber: '',
    districtId: null,
    postcode: '',
    agreement: false,
    otp: {
      first: '',
      second: '',
      third: '',
      fourth: ''
    }
  });
  const [isLoading, setLoading] = useState(false);
  const [errorRegister, setErrorRegister] = useState({
    show: false,
    message: ''
  });
  const [errorRegisterSeller, setErrorRegisterSeller] = useState({
    show: false,
    message: ''
  });
  const [otp, setOtp] = useState(null)
  const otp1Ref = useRef(null)
  const otp2Ref = useRef(null)
  const otp3Ref = useRef(null)
  const otp4Ref = useRef(null)
  const [otpWrong, setOtpWrong] = useState(false)

  const [address, setAddress] = useState('')
  const [modalMap, setModalMap] = useState(false)
  const [coordinate, setCoordinate] = useState(null)

  const [tooglePass, setTooglePass] = useState(false)
  const [tooglePassConfirm, setTooglePassConfirm] = useState(false)

  useEffect(() => {
    if(form.password !== form.confirmPassword) {
      setErrorRegister((v) => ({ ...v, show: true, message: "Password dan Konfirmasi Password tidak sama!" }));
    } else if (form.password.length < 8) {
      setErrorRegister((v) => ({ ...v, show: true, message: "Minimal password 8 huruf!" }));
    } else {
      setErrorRegister((v) => ({ ...v, show: false, message: '' }));
    }
  }, [form])

  useEffect(() => {
    if(otp)
    setStep(4)
  }, [otp])

  const doGetEmailVerification = async () => {
    const res = await requestEmailVerification(form.email)
    if(res.code === 200) {
      successNotification('Kode Verifikasi Berhasil Dikirim ke Email Anda!');
      setOtp(res)
    } else {
      setErrorRegister((v) => ({ ...v, show: true, message: res.message }));
    }
  }

  const doRegister = async () => {
    setLoading(true);
    const body = {
      name: `${form.firstName} ${form.lastName}`,
      email: form.email,
      password: form.password,
      phone: `62${form.phone}`,
    };
    const res = await apiRegister(body);

    if (res.code === 201) {
      // successNotification('Sukses registrasi silahkan login untuk melanjutkan');
      // setTimeout(() => {
      //   // window.location = '/signin';
      //   window.location = param.redirect ? `/signin?redirect=${param.redirect}` : '/signin';
      // }, 1500);
      doGetEmailVerification()
    } else {
      setErrorRegister((v) => ({ ...v, show: true, message: res.message }));
    }
    setLoading(false);
  };

  const doRegisterSeller = async () => {
    setLoading(true);
    const body = {
      name: `${form.firstName} ${form.lastName}`,
      email: form.email,
      password: form.password,
      phone: `62${form.phone}`,
      seller: {
        store_name: form.merchantName,
        identity_number: form.identityNumber,
        phone: `62${form.phone}`,
        address: {
          address: address,
          district_id: form.districtId.value,
          postal_code: form.postcode,
          lat_long: `${coordinate.lat},${coordinate.lng}`
        }
      }
    };
    const res = await apiRegisterSeller(body);
    if (res.code === 201) {
      // successNotification('Sukses registrasi silahkan login untuk melanjutkan');
      // setTimeout(() => {
      //   window.location = '/signin';
      // }, 1500);
      doGetEmailVerification()
    } else {
      setErrorRegisterSeller((v) => ({ ...v, show: true, message: res.message }));
      errorNotification(res.message);
    }

    setLoading(false);
  };

  const doRequestOTP = async () => {
    // const res = await requestOTP(apiUrl, `0${form.phone}`, form.email, `${form.firstName} ${form.lastName}`)
    // if(res.code===409) {
    //   setErrorRegister((v) => ({ ...v, show: true, message: "Phone number already registered!" }));
    // } else {
    //   setOtp(res.data)
    // }
    if(form.isSeller) {
      doRegisterSeller();
    } else {
      doRegister();
    }
  }

  const loadOptions = (inputValue, callback) => {
    if (inputValue.length > 3) {
      setTimeout(() => {
        apiSearchAddress(inputValue)
          .then((res) => {
            callback(res.data.map((v) => ({
              label: v.result,
              value: v.id_district
            })));
          }).catch(() => callback([]));
      }, 500);
    } else {
      callback([]);
    }
  };

  const handleChange = (name) => (e) => {
    setErrorRegister((v) => ({ ...v, show: false }));
    setErrorRegisterSeller((v) => ({ ...v, show: false }));
    const { target } = e;
    const checked = ['isSeller', 'agreement'];
    let value = checked.includes(name) ? target.checked : target.value;
    if(name == 'phone' && value.charAt(0) == '0') {
      value = value.substr(1, value.length)
    }
    setForm((v) => ({
      ...v,
      [name]: value
    }));
  };

  const handleRegister = () => {
    if (form.isSeller) {
      setStep(5);
    } else {
      // doRegister();
      doRequestOTP();
    }
  };

  const handleAfterOtp = async () => {
    const {first, second, third, fourth} = form.otp
    // const res = await validationOTP(apiUrl, otp.request_id, `${first}${second}${third}${fourth}`) 
    // if(res.code == 200){
    //   if(form.isSeller) {
    //     doRegisterSeller();
    //   } else {
    //     doRegister();
    //   }
    // } else {
    //   setOtpWrong(true)
    // }
    const res = await confirmEmailVerification(form.email, `${first}${second}${third}${fourth}`)
    if(res.code == 200){
      successNotification('Sukses registrasi silahkan login untuk melanjutkan');
      setTimeout(() => {
        window.location = '/signin';
      }, 1500);
    } else {
      setOtpWrong(true)
    }
  }

  const handleKeyOtp = (name) => (e) => {
    switch (name) {
      case 'first':
        if(e.target.value.length > 0) {
          otp2Ref.current.focus()
        } else {
          otp1Ref.current.focus()
        }
        break;
      case 'second':
        if(e.target.value.length > 0) {
          otp3Ref.current.focus()
        } else {
          otp1Ref.current.focus()
        }
        break;
      case 'third':
        if(e.target.value.length > 0) {
          otp4Ref.current.focus()
        } else {
          otp2Ref.current.focus()
        }
        break;
      case 'fourth':
        if(e.target.value.length > 0) {
          otp4Ref.current.focus()
        } else {
          otp3Ref.current.focus()
        }
        break;
      default:
        break;
    }
    setForm({...form, otp: {...form.otp, [name]: e.target.value}})
  }

  const handleSubmitMap = (coor, address) => {
    setModalMap(false)
    setCoordinate({...coor, address: address})
  }

  const renderContent = () => {
    switch (step) {
    case 1:
      return renderStep1();
    case 2:
      return renderStep2();
    case 3:
      return renderStep3();
    case 4:
      return renderStepOTP();
    case 5:
      return renderStepAggreement();
    default:
      return (<></>);
    }
  };

  const handleLookup = async () => {
    const res = await authLookup(form.firstName)
    if (res.code == 200) {
      const res = await authRegister({
        phone: form.firstName,
        password: form.password,
        password_confirmation: form.confirmPassword
      })
      if (res.code == 200 || res.code == 201) {
        Router.push(`/signin`)
      } else {
        setErrorRegister((v) => ({ ...v, show: true, message: "Gagal mendaftar" }));  
      }
    } else {
      setErrorRegister((v) => ({ ...v, show: true, message: "Nomor tidak terdaftar" }));
      // setOtpWrong(true)
    }
  }

  const renderStep1 = () => (
    <div className="container-login">
      <div className="text-primary-dark text-3xl font-bold">Daftar Sekarang</div>

      <div style={{ marginTop: '50px' }}>
        <div className="form-control w-100">
          <input
            className="w-100"
            placeholder="Nomor Telepon"
            value={form.firstName}
            onChange={handleChange('firstName')}
          />
        </div>
      </div>
      {/* <div style={{ marginTop: '20px' }}>
        <div className="form-control w-100">
          <input
            className="w-100"
            placeholder="Password"
            value={form.lastName}
            onChange={handleChange('lastName')}
          />
        </div>
      </div>
      <div style={{ marginTop: '20px' }}>
        <div className="form-control w-100">
          <input
            className="w-100"
            placeholder="Email"
            value={form.email}
            onChange={handleChange('email')}
          />
        </div>
      </div> */}
      <div style={{ marginTop: '20px' }}>
        <div className="form-control w-100 flex items-center">
          <input
            className="w-100"
            placeholder="Kata Sandi"
            type={`${tooglePass ? '' : "password"}`}
            value={form.password}
            onChange={handleChange('password')}
          />
          <div className="cursor-pointer text-primary" onClick={() => setTooglePass(!tooglePass)}>
            {tooglePass ? 'Tutup' : 'Tampilkan'}
          </div>
        </div>
      </div>
      <div style={{ marginTop: '20px' }}>
        <div className="form-control w-100 flex items-center">
          <input
            className="w-100"
            placeholder="Konfirmasi Kata Sandi"
            type={`${tooglePassConfirm ? '' : "password"}`}
            value={form.confirmPassword}
            onChange={handleChange('confirmPassword')}
          />
          <div className="cursor-pointer text-primary" onClick={() => setTooglePassConfirm(!tooglePassConfirm)}>
            {tooglePassConfirm ? 'Tutup' : 'Tampilkan'}
          </div>
        </div>
      </div>
      <div className="text-error" style={{ marginTop: '16px', display: errorRegister.show ? 'block' : 'none' }}>
        {errorRegister.message}
      </div>

      <div style={{ marginTop: '38px' }}>
        <button
          type="button"
          className="btn-primary"
          onClick={handleLookup}
          disabled={form.firstName.length < 8 || form.password.length < 8 || form.password != form.confirmPassword}
        >
          Daftar
        </button>
      </div>

      <hr style={{ margin: '30px 10px 10px 10px' }} />

      {/* <div className="text-center">
        <span className="text-secondary">Atau gunakan akun sosial agar lebih mudah Log In dan Registrasi</span>
      </div> */}

      <div className="container-social-media xs:flex-wrap">
        {/* <div className="xs:w-full">
          <GoogleLogin
            clientId="758631049915-emusji58lsj6ku98u42d2ducc43hpi6r.apps.googleusercontent.com"
            buttonText="Login"
            render={renderProps => (
              
              <button data-onsuccess="onSignIn" id="btn-google" type="button" className="btn-clean g-signin2 font-medium p-4 xs:w-full xs:flex xs:justify-center" style={{ display: 'flex', alignItems: 'center' }} onClick={renderProps.onClick} disabled={renderProps.disabled}>
                <img className="w-8" src="/images/ic_google.png" alt=""  style={{ marginRight: '8px' }} />
                <div className="text-base">Google</div>
              </button>
            )}
            onSuccess={responseGoogle}
            onFailure={responseGoogle}
            cookiePolicy={'single_host_origin'}
          />
        </div> */}
        {/* <div className="xs:w-full xs:mt-4">
          <FacebookLogin
            appId="564906621085215"
            callback={responseFacebook}
            fields="first_name, last_name, name,email,picture"
            render={renderProps => (
              <button type="button" className="btn-clean font-medium p-4 xs:w-full xs:flex xs:justify-center" style={{ display: 'flex', alignItems: 'center' }} onClick={renderProps.onClick}>
                <img className="w-8" src="/images/ic_facebook.png" alt=""  style={{ marginRight: '8px' }} />
                <div className="text-base">Facebook</div>
              </button>
            )}
          />
        </div> */}
      </div>

      <div className="text-center" style={{ marginTop: '30px' }}>
        <span className="text-register">Tidak punya akun bapera?</span>
        <a href="https://play.google.com/store/apps/details?id=id.or.bapera.mobile" target="_blank">
          <span className="text-register-action"> Register Bapera</span>
        </a>
      </div>

      <div className="text-center" style={{ marginTop: '30px' }}>
        <span className="text-register">Sudah punya akun?</span>
        <Link href="/signin">
          <span className="text-register-action"> Masuk</span>
        </Link>
      </div>

    </div>
  );

  const renderStep2 = () => (
    <div className="container-login">
      <div
        className="text-primary-dark text-3xl font-bold"
        style={{
          display: 'flex', alignItems: 'center', fontSize: '18px', cursor: 'pointer'
        }}
        onClick={() => setStep(1)}
        onKeyDown={() => setStep(1)}
        role="button"
        tabIndex={0}
      >
        <img src="/images/ic_arrow_left.png" style={{ marginRight: '10px' }} alt="" />
        Kembali
      </div>

      <div className="flex rounded items-center px-4" style={{ marginTop: '50px', boxShadow: '0px 1px 4px rgba(0, 0, 0, 0.25)' }}>
        62
        <input
          className="w-100 px-4 py-4 rounded"
          placeholder="Nomor Telepon"
          value={form.phone}
          onChange={handleChange('phone')}
          type="number"
        />
      </div>
      <div style={{ marginTop: '20px' }}>
        <div className="form-control w-100 flex items-center">
          <input
            className="w-100"
            placeholder="Password"
            type={`${tooglePass ? '' : "password"}`}
            value={form.password}
            onChange={handleChange('password')}
          />
          <div className="cursor-pointer text-primary" onClick={() => setTooglePass(!tooglePass)}>
            {tooglePass ? 'Hide' : 'Show'}
          </div>
        </div>
      </div>
      <div style={{ marginTop: '20px' }}>
        <div className="form-control w-100 flex items-center">
          <input
            className="w-100"
            placeholder="Konfirmasi Password"
            type={`${tooglePassConfirm ? '' : "password"}`}
            value={form.confirmPassword}
            onChange={handleChange('confirmPassword')}
          />
          <div className="cursor-pointer text-primary" onClick={() => setTooglePassConfirm(!tooglePassConfirm)}>
            {tooglePassConfirm ? 'Hide' : 'Show'}
          </div>
        </div>
      </div>
      <div className="text-error" style={{ marginTop: '16px', display: errorRegister.show ? 'block' : 'none' }}>
        {errorRegister.message}
      </div>
      <div className="w-full flex items-center" style={{ marginTop: '20px' }}>
        <input
          type="checkbox"
          id="check"
          checked={form.isSeller}
          onChange={handleChange('isSeller')}
          clasName="form-control"
        />
        <label htmlFor="check" className="text-is-seller" style={{ marginLeft: '6px' }}>Buat juga Toko Mu</label>
      </div>
      <div className="w-full flex items-center" style={{ marginTop: '20px' }}>
        <input
          type="checkbox"
          id="checkAggreement"
          checked={form.agreement}
          onChange={handleChange('agreement')}
          clasName="form-control"
        />
        <label htmlFor="checkAggreement" className="text-is-seller" style={{ marginLeft: '6px' }}>
          Saya setuju dengan
          <br />
          <a href="/terms" target="_blank">
            <span style={{ fontWeight: 'bold', color: '#730C07' }}>Syarat dan Ketentuan serta Kebijakan Privasi</span>
          </a>
        </label>
      </div>

      <div style={{ marginTop: '38px' }}>
        <button
          type="button"
          className="btn-primary"
          onClick={() => handleRegister()}
          disabled={form.phone.length < 8 || form.password === '' || form.confirmPassword === '' || form.password !== form.confirmPassword || isLoading || !form.agreement}
        >
            Selanjutnya
        </button>
      </div>

      <hr style={{ margin: '30px 10px 10px 10px' }} />

      <div className="text-center" style={{ marginTop: '30px' }}>
        <span className="text-register">Sudah punya akun?</span>
        <Link href="/signin">
          <span className="text-register-action"> Masuk</span>
        </Link>
      </div>

      <div className="text-center" style={{ marginTop: '20px' }}>
        <span className="text-secondary">
          Dengan mendaftar, saya menyetujui
          {' '}
          <br />
          Syarat dan Ketentuan serta Kebijakan Privasi.
        </span>
      </div>

    </div>
  );

  const renderStep3 = () => (
    <div className="container-login">

      <div style={{ display: 'flex', justifyContent: 'space-between' }}>
        <div className="text-primary-dark text-3xl font-bold">Daftar Seller</div>
        <div
          className="text-primary-dark text-3xl font-bold"
          style={{
            display: 'flex', alignItems: 'center', fontSize: '18px', cursor: 'pointer'
          }}
          onClick={() => setStep(5)}
          onKeyDown={() => setStep(5)}
          role="button"
          tabIndex={0}
        >
          <img src="/images/ic_arrow_left.png" style={{ marginRight: '10px' }} alt="" />
          Kembali
        </div>
      </div>

      <div style={{ marginTop: '50px' }}>
        <div className="form-control w-100">
          <input
            className="w-100"
            placeholder="Nama Toko"
            value={form.merchantName}
            onChange={handleChange('merchantName')}
          />
        </div>
      </div>
      <div style={{ display: 'flex', marginTop: '20px' }}>
        <div style={{ width: '75%', paddingRight: '10px' }}>
          {/* <input className="form-control w-100" placeholder="Kota / Kecamatan" /> */}
          <Select
            styles={{
              control: (provided) => ({
                ...provided,
                background: '#FFFFFF',
                boxShadow: '0px 1px 4px rgba(0, 0, 0, 0.25)',
                borderRadius: '7px',
                padding: '20px 18px',
                border: '0px'
              }),
              valueContainer: (provided) => ({
                ...provided,
                padding: '0px'
              }),
              menu: (provided) => ({
                ...provided,
                padding: 'px'
              }),
              indicatorsContainer: (provider) => ({
                ...provider,
                display: 'none'
              })
            }}
            placeholder="Kota / Kecamatan"
            loadOptions={loadOptions}
            cacheOptions
            defaultOptions
            value={form.districtId}
            onChange={(value) => setForm({ ...form, districtId: value })}
          />
        </div>
        <div style={{ width: '35%' }}>
          <div className="form-control w-100">
            <input
              className="w-100"
              placeholder="Kode Pos"
              value={form.postcode}
              onChange={handleChange('postcode')}
              type="number"
            />
          </div>
        </div>
      </div>
      <div className="w-full mt-4">
        <div className="w-full shadow rounded"> 
          <textarea className="w-full p-4" placeholder="Alamat Detail" onChange={(e) => setAddress(e.target.value)}></textarea>
        </div>
      </div>
      <div className="w-full mt-4">
        <div className="w-full flex rounded items-center shadow p-4">
          <div>
            <img src="/images/ic_map_red.png" alt="" />
          </div>
          <div className="text-sm text-text-primary ml-4" style={{ width: '320px' }}>
            {coordinate ? coordinate.address : 'Tandai lokasi untuk alamat yang lebih tepat' }
          </div>
          <div className="border border-primary text-primary px-4 py-2 rounded cursor-pointer" onClick={() => setModalMap(true)}>
            Tandai Lokasi
          </div>
        </div>
      </div>
      <div className="text-error" style={{ marginTop: '16px', display: errorRegister.show ? 'block' : 'none' }}>
        {errorRegister.message}
      </div>
      {/* <div className="w-full flex items-center" style={{ marginTop: '20px' }}>
        <input
          type="checkbox"
          id="check"
          checked={form.agreement}
          onChange={handleChange('agreement')}
          clasName="form-control"
        />
        <label htmlFor="check" className="text-is-seller" style={{ marginLeft: '6px' }}>
          Saya setuju dengan
          {' '}
          <a href="/terms" target="_blank">
            <span style={{ fontWeight: 'bold' }}>Syarat dan Ketentuan serta Kebijakan Privasi</span>
          </a>
        </label>
      </div> */}

      <div style={{ marginTop: '38px' }}>
        <button
          type="button"
          className="btn-primary"
          disabled={form.merchantName === '' || form.districtId === null || form.postcode === '' || isLoading || coordinate == null || address == ''}
          onClick={doRequestOTP}
        >
            Selesai
        </button>
      </div>

      <hr style={{ margin: '30px 10px 10px 10px' }} />

      <div className="text-center" style={{ marginTop: '30px' }}>
        <span className="text-register">Sudah punya akun?</span>
        <Link href="/signin">
          <span className="text-register-action"> Masuk</span>
        </Link>
      </div>

      <Modal open={modalMap} onClose={() => setModalMap(false)}>
        {
          modalMap && (
            <MapPicker 
              onClose={() => setModalMap(false)} 
              onSubmit={(coor, address) => handleSubmitMap(coor, address)}
              defaultCenter={coordinate ? {
                lat: coordinate.lat,
                lng: coordinate.lng
              } : null}
              reverseGeolocation={form.districtId ? form.districtId.label : null}
            /> 
          )
        }
      </Modal>

    </div>
  );

  const renderStepOTP = () => (
    <div className="container-login">
      <div
        className="text-primary-dark text-3xl font-bold"
        style={{
          display: 'flex', alignItems: 'center', fontSize: '18px', cursor: 'pointer'
        }}
        onClick={() => setStep(1)}
        onKeyDown={() => setStep(1)}
        role="button"
        tabIndex={0}
      >
        <img src="/images/ic_arrow_left_red.png" style={{ marginRight: '10px' }} alt="" />
      </div>
      <div className="w-full justify-center flex flex-wrap">
        <img src="/images/ic_otp.png" alt=""></img>
      </div>
      <div className="w-full flex justify-center text-text-primary font-bold text-xl mt-4">
        Masukkan Kode Verifikasi
      </div>
      <div className="w-full flex justify-center text-center text-text-primary text-lg tracking-wide">
        Kode verifikasi telah dikirimkan melalui<br></br>
        Email ke {form.email}<br></br>
        {/* Jika kode verifikasi tidak didapatkan, silahkan cek email { form.email } */}
      </div>
      <div className="w-full flex justify-center text-center text-text-primary text-lg tracking-wide mt-2">
        Kode Verifikasi
      </div>
      <div>
        <form method="get" class="digit-group flex justify-center mt-4" data-group-name="digits" data-autosubmit="false" autocomplete="off">
          <input className="border border-gray-300 text-lg" type="text" id="digit-1" name="digit-1" data-next="digit-2" maxLength="1" value={form.otp.first} onChange={handleKeyOtp('first')} ref={otp1Ref}/>
          <input className="border border-gray-300 text-lg" type="text" type="text" id="digit-2" name="digit-2" data-next="digit-3" data-previous="digit-1" maxLength="1" value={form.otp.second} onChange={handleKeyOtp('second')} ref={otp2Ref}/>
          <input className="border border-gray-300 text-lg" type="text" type="text" id="digit-3" name="digit-3" data-next="digit-4" data-previous="digit-2" maxLength="1" value={form.otp.third} onChange={handleKeyOtp('third')} ref={otp3Ref}/>
          <input className="border border-gray-300 text-lg" type="text" type="text" id="digit-4" name="digit-4" data-next="digit-5" data-previous="digit-3" maxLength="1" value={form.otp.fourth} onChange={handleKeyOtp('fourth')} ref={otp4Ref}/>
        </form>
      </div>
      {
        otpWrong && (
        <div className="w-full text-center mt-4 text-primary font-medium">
          Kode verifikasi salah
        </div>
        )
      }
      <div style={{ marginTop: '38px' }}>
        <button
          type="button"
          className="btn-primary"
          onClick={() => handleAfterOtp()}
          disabled={form.otp.first == ''|| form.otp.second == '' || form.otp.third == '' || form.otp.fourth == ''}
        >
          Verifikasi
        </button>
      </div>
      <div className="w-full flex justify-center text-center text-text-primary text-lg tracking-wide mt-4">
        Tidak Menerima Kode?
      </div>
      <div className="w-full flex justify-center text-center text-text-primary text-lg tracking-wide">
        <button className="text-primary" onClick={() => doGetEmailVerification()}>Kirim Ulang</button>
      </div>
    </div>
  )

  const renderStepAggreement = () => (
    <div className="container-login mt-4">
      <div className="text-primary-dark text-3xl font-bold">Seller Agreements</div>
      <div className="w-full mt-4 text-text-primary" style={{ lineHeight: "36px" }}>
        <b>Hal-hal penting yang perlu dipahami oleh Penjual:</b>
        <br />
        1. Penjual memahami dan menyetujui bahwa untuk pertama kali, apabila ingin menjual biji kopi sangray (roasted beans) wajib mengirimkan sample kopi ke pihak Lakkon terlebih dahulu sebanyak 50gram untuk 1 jenis nya. Ke alamat:
          <br />
          <div style={{ marginLeft: '48px' }}>
            <b>
            Lakkon / Bersahaja (+62 822.9998.5858)
              <br />
            Jalan Pinang Emas V Nomor 7, Pondok Pinang – Kebayoran lama 
              <br />
            Jakarta Selatan 12310
              <br />
            </b>
            <i>(mohon melakukan konfirmasi ke nomor hp kami setelah melakukan pengiriman)</i>
            <br />
          </div>
          2. Penjual memahami dan menyetujui untuk berjualan produk makanan dan minuman dianjurkan untuk memiliki izin PIRT.
          <br />
          3. Penjual memahami dan menyetujui untuk memberikan komisi kepada Lakkon sebesar 10% dari harga jual barang antara 70.000 rupiah – 1.000.000 rupiah, dan sebesar 5% dari harga jual barang antara 1.001.000 rupiah – seterusnya, untuk 1 barang.(tidak berlaku akumulasi atau kelipatannya).
          <br />
          4. Penjual memahami dan menyetujui harga barang yang akan dijual di website lakkon harus dengan minimum harga 70.000 rupiah per barang.
          <br />
          5. Penjual memahami dan menyetujui bahwa Penjual wajib memberikan balasan untuk menerima (dengan melakukan konfirmasi pesanan dan memasukan nomor resi di lakkon) atau menolak pesanan barang pihak Pembeli dalam batas waktu 2x24 Jam terhitung sejak adanya notifikasi pesanan barang dari lakkon. Jika dalam batas waktu tersebut tidak ada balasan dari Penjual maka secara otomatis pesanan akan dibatalkan.
          <br />
          6. Apabila terjadi penolakan pesanan dari pihak penjual, maka penjual setuju untuk mengembalikan dana ke Pembeli dengan perhitungan sebagai berikut:
          <br />
          <div style={{ marginLeft: '48px' }}>
            - Harga barang
            <br />
          - Biaya ongkos kirim pembeli
            <br />
          - Biaya penanganan/convenient fee sebesar Rp 5.000,- / Invoice.
            <br />
          </div>

          7. Penjual memahami dan menyetujui bahwa, biaya – biaya seperti yang sudah dijelaskan di nomor 5 akan dibebankan ke Penjual dan dipotong dari rekap penjualan berikutnya.
          <br />
          8. Apabila setelah terjadi penolakan pesanan kepada pembeli dan Penjual belum memiliki transaksi apapun di laman lakkon, maka kami akan mengirimkan invoice terkait dengan Biaya penanganan untuk dibayarkan ke pihak Lakkon melalui bank transfer ke rekening resmi lakkon.
          <br />
      </div>
      <div className="mt-4 flex justify-center items-center">
        <div className="border border-primary rounded py-2 px-4 bg-primary mr-2 text-white cursor-pointer" onClick={() => setStep(3)}>
          Setuju
        </div>
        <div className="border border-primary rounded py-2 px-4 bg-white ml-2 text-text-primary cursor-pointer" onClick={() => setStep(2)}>
          Tidak Setuju
        </div>
      </div>
    </div>
  )

  const responseGoogle = async (response) => {
    if(!response.error) {
      const res = await validateSocial(apiUrl, response.profileObj.email)
      if(!res.data.registered) {
        Router.push(`/register?email=${response.profileObj.email}&firstName=${response.profileObj.givenName}&lastName=${response.profileObj.familyName}`)
      } else {
        const res = await loginSocial(apiUrl, response.profileObj.email, response.tokenObj.id_token)
        if(res.code !== 400 || res.code !== 500) {
          props.socialAuthenticate(res.data)
        }
      }
    }
  }

  const responseFacebook = async (response) => {
    if(!response.status) {
      const res = await validateSocial(apiUrl, response.email)
      if(!res.data.registered) {
        Router.push(`/register?email=${response.email}&firstName=${response.first_name}&lastName=${response.last_name}`)
      } else {
        const res = await loginSocial(apiUrl, response.email, response.accessToken)
        if(res.code !== 400 || res.code !== 500) {
          props.socialAuthenticate(res.data)
        }
      }
    }
  }


  return (
    <div className="c">
      <Head>
        <title>Lakkon</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className="c">
        <div className={`w-full text-center flex justify-center ${step == 4 ? 'hidden' : 'block'}`}>
          <Link href="/"><img className="h-16" src="/images/logo_bapera.png" alt="" /></Link>
        </div>

        { renderContent() }
      </div>


      <style jsx global>
        {`
        .c {
          width: 100%;
          height: 100vh;
          display: flex;
          align-items: center;
          flex-wrap: wrap;
          justify-content: center;
        }

        .container-login {
          min-width: 568px;
          max-width: 568px;
          min-height: 596px;
          background: #FFFFFF;
          box-shadow: 0px 1px 12px rgba(0, 0, 0, 0.25);
          border-radius: 10px;
          padding: 2.5rem 2.5rem;
        }

        .break {
          flex-basis: 100%;
          height: 0;
        }

        .text-forgot-password {
          font-size: 12px;
          line-height: 14px;
          color: #6F0C06;
          margin-top: 20px;
          text-align: right;
        }

        .container-social-media {
          display: flex;
          justify-content: space-evenly;
          margin-top: 30px;
        }

        .text-register {
          font-size: 16px;
          line-height: 19px;
          color: #AFAFAF;
        }

        .text-register-action {
          font-weight: bold;
          font-size: 16px;
          line-height: 19px;
          color: #730C07;
          cursor: pointer;
        }

        .text-is-seller {
          font-size: 16px;
          line-height: 19px;
          color: #868686;
          font-weight: 300;
        }

        .digit-group .splitter {
            padding: 0 5px;
            color: white;
            font-size: 24px;
        }

        .digit-group input {
            width: 40px;
            height: 50px;
            text-align: center;
            margin: 0 2px;
        }

        @media only screen and (max-width: 600px) {
          .c {
            width: 100%;
            display: flex;
            align-items: center;
            flex-wrap: wrap;
            justify-content: center;
          }
          
          .container-login {
            margin-top: 12px;
            min-width: 90%;
            min-height: 596px;
            background: #FFFFFF;
            box-shadow: 0px 1px 12px rgba(0, 0, 0, 0.25);
            border-radius: 10px;
            padding: 2.5rem 2.5rem;
          }
        }
      `}

      </style>
    </div>
  );
}

Home.getInitialProps = (ctx) => {
  const { query } = ctx
  return {
    firstName: query.firstName ? query.firstName : '',
    lastName: query.lastName ? query.lastName : '',
    email: query.email ? query.email : '',
  }
}

export default connect(
  (state) => state, 
  { socialAuthenticate }
)(Home);
