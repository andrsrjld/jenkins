import React from 'react';
import { getProductType } from '~/data/api/product-type';

const Index = () => (
  <div className="c">
    <div className="text-center">
      <img src="/images/ic_404.png" alt="" />
    </div>
    <div className="desc w-100">
        Oops halaman sedang dalam gangguan, mohon coba beberapa saat lagi.
    </div>
    <style jsx>
      {
        `
        .c {
          display: flex;
          flex-wrap: wrap;
          flex-direction: column;
          justify-content: center;
          height: 100vh;
        }

        .desc {
          font-weight: 500;
          font-size: 22px;
          line-height: 26px;
          text-align: center;
          letter-spacing: 0.05em;
          color: #696969;
          margin-top: 20px;
        }
        `
      }
    </style>
  </div>
);

Index.getInitialProps = async (ctx) => {
  const { token } = ctx.store.getState().authentication;

  const productTypeResp = await getProductType();
  const productType = productTypeResp.data;

  return { 
    productType,
    token,
  };
};

export default Index;
