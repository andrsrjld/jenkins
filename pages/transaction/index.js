import React from 'react';
import BaseLayout from '~/components/BaseLayout';
import { getProductType } from '~/data/api/product-type';
import { getCart } from '~/data/api/cart';

const index = ({ productType, token, cart }) => (
  <div>
    <BaseLayout productType={productType} token={token} cart={cart}>
      <div className="c">
        <div className="w-full flex justify-center">
          <img src="/images/ic_thankyou.png" alt="" />
        </div>
        <div style={{ margin: '3rem 0px' }}>
          Terima kasih, pesanan Anda akan segera di proses. Ayo Belanja Lagi!
        </div>
      </div>
    </BaseLayout>

    <style jsx>
      {
        `
        .c {
          padding: 2rem 70px;
          display: flex;
          justify-content: center;
          flex-wrap: wrap;
        }

        .c  div {
          font-weight: bold;
          font-size: 24px;
          line-height: 23px;
          text-align: center;
          letter-spacing: 0.05em;
          color: #696969;
          width: 100%;
        }
        `
      }
    </style>
  </div>
);

index.getInitialProps = async (ctx) => {
  const { slug } = ctx.query;
  const { token } = ctx.store.getState().authentication;

  const productTypeResp = await getProductType();
  const productType = productTypeResp.data;

  const cartResp = await getCart(token);
  const cart = cartResp.data ? cartResp.data.cart : [];

  return { 
    slug,
    productType,
    token,
    cart
  };
};

export default index;
