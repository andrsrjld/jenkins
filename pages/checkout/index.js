import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import Link from 'next/link';
import BaseLayout from '~/components/BaseLayout';
import Modal from '~/components/Modal';
import { getAddresses } from '~/data/api/address';
import { getCart } from '~/data/api/cart';
import { formatNumber } from '~/util/helper';
import { getShipping } from '~/data/api/shipping';
import { getProductType } from '~/data/api/product-type';
import { getServiceCharge } from '~/data/api/order';
import { getEvents } from '~/data/api/event';
import Payment from '~/components/Payment';
import Checkout from '~/components/Checkout';
import Shipping from '~/components/Shipping';
import Snap from '~/components/Snap';
import moment from 'moment'

const index = ({ token, addresses, cart, productType, serviceCharge, event, simulation }) => {
  const [addressData, setAddressData] = useState(addresses);
  const [selectedAddress, setSelectedAddress] = useState(addresses[0]);
  const [cartData, setCartData] = useState(cart);
  const [sellers, setSellers] = useState([]);
  const [shippingData, setShippingData] = useState([]);
  const [paymentResponse, setPaymentResponse] = useState(null);

  const [step, setStep] = useState(1);
  const [lastStep, setLastStep] = useState(1);

  const handleNext = () => {
    setStep(step + 1);
    if(lastStep <= step){
      setLastStep(step + 1);
    }
    window.scrollTo(0,0);
  }

  useEffect(() => {
    if(paymentResponse){
      if(paymentResponse.payment_type === "credit_card"){
        window.location = "/transaction";
      } else {
        setStep(4)
        console.log(paymentResponse)
      }
    }
  }, [paymentResponse])

  const changeStep = (step) => (e) => {
    if(step <= lastStep){
      setStep(step);
      setLastStep(step);
    }
  }

  return (
    <div>
      <BaseLayout productType={productType} token={token} cart={cart}>
        <div className="checkout-step-wrapper flex xs:hidden sm:hidden" style={{padding: '20px 0'}}>
          <div className="checkout-step" style={{ zIndex: '5' }}><Link href="/cart">Keranjang</Link></div>
          <div className={step === 1 ? "active-step" : 1 > lastStep ? "checkout-step disabled-step" : "checkout-step"} style={{ marginLeft: '-1.5rem', zIndex: '4' }} onClick={changeStep(1)}>Alamat</div>
          <div className={step === 2 ? "active-step" : 2 > lastStep ? "checkout-step disabled-step" : "checkout-step"} style={{ marginLeft: '-1.8rem', zIndex: '3' }} onClick={changeStep(2)}>Shipping</div>
          <div className={step === 3 ? "active-step" : 3 > lastStep ? "checkout-step disabled-step" : "checkout-step"} style={{ marginLeft: '-2rem', zIndex: '2' }} onClick={changeStep(3)}>Metode Pembayaran</div>
          <div className={step === 4 ? "active-step" : 4 > lastStep ? "checkout-step disabled-step" : "checkout-step"} style={{ marginLeft: '-2.4rem', zIndex: '1' }}>Review Pembayaran</div>
        </div>
        {
          step == 1 && (
            <Checkout 
              token={token} 
              addresses={{ data: addressData, callback: (data) => setAddressData(data) }} 
              cart={cart} 
              TselectedAddress={{ data: selectedAddress, callback: (data)=> setSelectedAddress(data) }} 
              Tsellers={{ data: sellers, callback: (data) => setSellers(data) }}
              Tshippings={{ data: shippingData, callback: (data) => setShippingData(data)}}
              handleNext={handleNext}
              serviceCharge={serviceCharge.charge}
            />
          )
        }
        {
          step == 2 && (
            <Shipping
              token={token} 
              addresses={{ data: addressData, callback: (data) => setAddressData(data) }} 
              cart={cart} 
              TselectedAddress={{ data: selectedAddress, callback: (data)=> setSelectedAddress(data) }} 
              Tsellers={{ data: sellers, callback: (data) => setSellers(data) }}
              Tshippings={{ data: shippingData, callback: (data) => setShippingData(data)}}
              handleNext={handleNext}
              serviceCharge={serviceCharge.charge}
              event={event}
            />
          )
        }
        {
          step == 3 && (
            <Payment 
              token={token} 
              addresses={{ data: addressData, callback: (data) => setAddressData(data) }} 
              cart={cart} 
              TselectedAddress={{ data: selectedAddress, callback: (data)=> setSelectedAddress(data) }} 
              Tsellers={{ data: sellers, callback: (data) => setSellers(data) }}
              Tshippings={{ data: shippingData, callback: (data) => setShippingData(data)}}
              handleBack={handleNext}
              cbPayment={(data) => {
                setPaymentResponse(data)
              }}
              serviceCharge={serviceCharge.charge}
              simulation={simulation}
            />
          )
        }
        {
          step == 4 && (
            <Snap 
              simulation={simulation}
              Tpayment={paymentResponse}
            />
          )
        }

      </BaseLayout>
      <style jsx>
        {
          `
          .title {
            font-weight: bold;
            font-size: 24px;
            line-height: 23px;
            letter-spacing: 0.05em;
            color: #696969;
            padding: 0px 2rem;
          }

          .c-content {
            display: flex;
            flex-direction: row;
          }

          .c-1 {
            width: 100%;
            margin-right: 28px;
          }

          .c-2 {
            width: 550px;
          }
          .card-2 {
            background: #FFFFFF;
            box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.25);
            height: auto;
          }

          .summary-header {
            padding: 1rem;
            font-weight: bold;
            font-size: 18px;
            line-height: 17px;
            color: #868686;
            box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.25);
          }

          .c-content-summary {
            font-size: 16px;
            line-height: 15px;
            color: #696969;
            display: flex;
            justify-content: space-between;
            padding: 0.7rem;
            margin: 0.8rem 0.5rem;
            border-bottom: 1px solid #CCCCCC;
          }

          .key {
            display: flex;
          }

          .key img {
            width: 85px;
          }

          .card {
            background: #FFFFFF;
            border: 0.5px solid #010101;
            box-sizing: border-box;
            box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.25);
            border-radius: 5px;
          }

          th {
            text-align: left;
            padding: 1rem 1rem;
          }

          .btn-action {
            background: #FFFFFF;
            border: 0.5px solid #696969;
            box-sizing: border-box;
            border-radius: 7px;
            font-weight: 500;
            font-size: 16px;
            line-height: 17px;
            text-align: center;
            color: #696969;
            padding: 1rem;
            cursor: pointer;
          }

          td {
            border-bottom: 1px solid #CCCCCC;
          }

          .btn-this {
            width: auto;
            font-weight: 500;
            font-size: 18px;
            line-height: 17px;
            text-align: center;
            color: #FFFFFF;
            padding: 1rem 2rem;
          }
          .checkout-step{
            min-width: 160px;
            width: auto;
            padding:15px 3.5rem;
            display:block;
            background-image: url('/images/ic_milestone_right.png');
            background-size: 100% 100%;

            display: flex;
            justify-content: center;
            align-items:center;
          }
          .active-step{
            min-width: 160px;
            width: auto;
            padding:15px 2.5rem;
            display:block;
            color: white;
            background-image: url('/images/ic_milestone_right_red.png');
            background-size: 100% 100%;

            display: flex;
            justify-content: center;
            align-items:center;
          }
          .disabled-step{
            background-image: url('/images/ic_milestone_right_grey.png');
            color:#868686;
          }

          .checkout-step-wrapper{
            padding: 20px 0;
          }
          @media (min-width: 320px) and (max-width: 767px) {
            .checkout-step-wrapper{
              padding: 20px;
            }
          }
        `
        }
      </style>
    </div>
  );
};


index.getInitialProps = async (ctx) => {
  const { token, endpoint } = ctx.store.getState().authentication;
  if(!token){
    ctx.res.writeHead(302, {
      Location: `/signin?redirect=checkout`
    });
    ctx.res.end();
  }
  
  const addresses = await getAddresses(token);
  const cart = await getCart(token);

  const productTypeResp = await getProductType();
  const productType = productTypeResp.data;

  const serviceChargeResp = await getServiceCharge(token);
  const serviceCharge = serviceChargeResp.data;

  const eventsResp = await getEvents(endpoint.apiUrl, 
  {
    layout_type: 'list_layout',
    limit: 100,
    page: 1
  });
  const events = eventsResp.data;
  let event =  null;

  console.log(events)
  if (events && events.length > 0)  {
    const tempEvent = events[events.length - 1]
    const startDate= moment(tempEvent.start_date)
    const endDate= moment(tempEvent.end_date)
    const now = moment(moment().format('YYYY-MM-DD'))
    
    if(startDate.diff(now) <= 0 && endDate.diff(now) >= 0) {
      event = tempEvent
    }
  }


  return {
    token,
    addresses: addresses.data ? addresses.data.map((v, i) => ({...v, isChecked: i == 0 })) : [],
    cart: cart.data ? cart.data.cart ? cart.data.cart : [] : [],
    simulation: cart.data ? cart.data.simulation : { },
    event,
    productType,
    serviceCharge
  };
};

export default connect(
  (state) => state, {}
)(index);
