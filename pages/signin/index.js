import React, { useState, useRef} from 'react';
import Router from 'next/router';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { connect } from 'react-redux';
import Link from 'next/link';
import { login, requestEmailVerification, confirmEmailVerification } from '../../util/ServiceHelper';
import {
  authenticate, socialAuthenticate
} from '~/redux/actions/authActions';
import { GoogleLogin } from 'react-google-login';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'
import { validateSocial, loginSocial } from '~/data/api/social'
import { postForgotPassword } from '~/data/api/user';
import Modal from '~/components/Modal';

// Toastr Notification
import {successNotification, errorNotification} from '~/util/toastr-notif';

const Home = ({ authenticate, apiUrl, socialAuthenticate }) => {
  const router = useRouter();
  const param = router.query;
  
  const [form, setForm] = useState({
    email: '',
    password: '',
    emailForgot: '',
    otp: {
      first: '',
      second: '',
      third: '',
      fourth: ''
    }
  });

  const [isLoading, setLoading] = useState(false);
  const [error, setError] = useState({
    show: false,
    message: 'Nomor telepon dan kata sandi tidak cocok'
  });
  const [forgotPassword, setForgotPassword] = useState(false);

  const [tooglePass, setTooglePass] = useState(false)

  const firstOtpRef = useRef(null);
  const secondOtpRef = useRef(null);
  const thirdOtpRef = useRef(null);
  const fourthOtpRef = useRef(null);
  const [modalOtp, setModalOtp] = useState(false);


  const doLogin = async (e) => {
    e.preventDefault();
    setLoading(true);
    const body = {
      phone: form.email,
      password: form.password
    };
    const res = await authenticate(body);
    if (res.code === 500 || res.code === 400) {
      setError((v) => ({ ...v, show: true }));
    } else {
      // if(res.data.is_verified) {
        window.location = param.redirect ? `/${param.redirect}` : '/account';
      // } else {
      //   handleEmailVerification()
      // }
    }
    setLoading(false);
  };

  const handleChange = (name) => (e) => {
    const { value } = e.target;
    setError((v) => ({ ...v, show: false }));
    setForm((v) => ({ ...v, [name]: value }));
  };

  const responseGoogle = async (response) => {
    if(!response.error) {
      const res = await validateSocial(apiUrl, response.profileObj.email)
      if(!res.data.registered) {
        Router.push(`/register?email=${response.profileObj.email}&firstName=${response.profileObj.givenName}&lastName=${response.profileObj.familyName}`)
      } else {
        const res = await loginSocial(apiUrl, response.profileObj.email, response.tokenObj.id_token)
        if(res.code !== 400 || res.code !== 500) {
          socialAuthenticate(res.data)
        }
      }
    }
  }

  const responseFacebook = async (response) => {
    if(!response.status) {
      const res = await validateSocial(apiUrl, response.email)
      if(!res.data.registered) {
        Router.push(`/register?email=${response.email}&firstName=${response.first_name}&lastName=${response.last_name}`)
      } else {
        const res = await loginSocial(apiUrl, response.email, response.accessToken)
        if(res.code !== 400 || res.code !== 500) {
          socialAuthenticate(res.data)
        }
      }
    }
  }

  const doForgotPassword = async () => {
    setLoading(true);
    const body = {
      email: form.emailForgot,
    };
    const res = await postForgotPassword(body);
    if (res.code === 500 || res.code === 400) {
      errorNotification(res.message);
      setForgotPassword(false);
    } else {
      console.log(res)
      successNotification("Permintaan reset password berhasil, check email kamu untuk langkah selanjutnya. Reset password dikirimkan melalui email mohon cek inbox atau spam");
    }
    setLoading(false);
  };

  const handleEmailVerification = async () => {
    setLoading(true);
    const res = await requestEmailVerification(form.email);
    if(res.code == 200){
      firstOtpRef.current.focus();
      setLoading(false);
      
      setModalOtp(true);
    } else {
      setLoading(false);
      console.log(error.response);
      errorNotification(error.response ? error.response.data.message : error.response);
    }
  }

  const handleAfterOtp = async () => {
    const {first, second, third, fourth} = form.otp
      const body = {
        email: form.email,
        token_verification: `${first}${second}${third}${fourth}`
      }
      const res = await confirmEmailVerification(form.email, `${first}${second}${third}${fourth}`); 
      if(res.code == 200){
        successNotification("Verifikasi akun email berhasil, Silahkan Login Kembali!");
        setModalOtp(false);
      } else {
        console.log(res);
        errorNotification("Kode yg anda masukan salah");
      }
  }

  const handleKeyOtp = (name) => (e) => {
    setForm({...form, otp: {...form.otp, [name]: e.target.value}})
    
    if(name === "first" && e.target.value.length == 1){
      secondOtpRef.current.focus();
    }
    if(name === "second" && e.target.value.length == 1){
      thirdOtpRef.current.focus();
    }
    if(name === "third" && e.target.value.length == 1){
      fourthOtpRef.current.focus();
    }
  }

  const handleResendEmailVerification = async () => {
    setLoading(true);
    const res = await requestEmailVerification(form.email);
    setLoading(false);
    if(res.code == 200){
      successNotification("Kode otp telah dikirim ulang");
    } else {
      console.log(error.response);
      errorNotification(error.response ? error.response.data.message : error.response);
    }
  }

  return (
    <div className="c">
      <Head>
        <title>Bapera</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className="c">
        <div className="w-100 flex justify-center">
          <Link href="/"><img src="/images/logo_bapera.png" alt="" className="h-16"/></Link>
        </div>

        <div className="container-login" style={!forgotPassword ? {display: "block"} : {display: "none"}}>
          <div className="text-primary-dark text-3xl font-bold">Masuk</div>
          <form onSubmit={doLogin}>
            <div style={{ marginTop: '50px' }}>
              <div className="form-control w-100">
                <input
                  className="w-100"
                  placeholder="Nomor Telepon"
                  type="text"
                  value={form.email}
                  onChange={handleChange('email')}
                  disabled={isLoading}
                />
              </div>
            </div>
            <div style={{ marginTop: '20px' }}>
              <div className="form-control w-100 flex items-center">
                <input
                  className="w-100"
                  placeholder="Kata Sandi"
                  type={`${tooglePass ? 'text' : "password"}`}
                  value={form.password}
                  onChange={handleChange('password')}
                  disabled={isLoading}
                />
                <div className="cursor-pointer text-primary" onClick={() => setTooglePass(!tooglePass)}>
                  {tooglePass ? 'Tutup' : 'Tampilkan'}
                </div>
              </div>
            </div>
            <div className="text-error" style={{ marginTop: '16px', display: error.show ? 'block' : 'none' }}>
              {error.message}
            </div>

            <div className="text-forgot-password">
              <span className="forgot-password cursor-pointer" onClick={() => {setForgotPassword(true)}}>Lupa Password?</span>
            </div>

            <div style={{ marginTop: '38px' }}>
              <button
                type="submit"
                className="btn-primary"
                disabled={isLoading || form.email === '' || form.password === ''}
              >
                Masuk
              </button>
            </div>
          </form>

          <hr style={{ margin: '30px 10px 10px 10px' }} />

          {/* <div className="text-center">
            <span className="text-secondary">Atau gunakan akun sosial agar lebih mudah Log In dan Registrasi</span>
          </div> */}

          <div className="container-social-media xs:flex-wrap">
            {/* <div className="xs:w-full">
              <GoogleLogin
                clientId={'758631049915-emusji58lsj6ku98u42d2ducc43hpi6r.apps.googleusercontent.com'}
                buttonText="Login"
                render={renderProps => (
                  <button data-onsuccess="onSignIn" id="btn-google" type="button" className="btn-clean g-signin2 font-medium p-4 xs:w-full xs:flex xs:justify-center" style={{ display: 'flex', alignItems: 'center' }} onClick={renderProps.onClick} disabled={renderProps.disabled}>
                    <img className="w-8" src="/images/ic_google.png" alt="" style={{ marginRight: '8px' }} />
                    <div className="text-base">Google</div>
                  </button>
                )}
                onSuccess={responseGoogle}
                onFailure={responseGoogle}
                cookiePolicy={'single_host_origin'}
              />
            </div> */}
            {/* <div className="xs:w-full xs:mt-4">
              <FacebookLogin
                appId="564906621085215"
                callback={responseFacebook}
                fields="first_name, last_name, name,email,picture"
                render={renderProps => (
                  <button type="button" className="btn-clean font-medium p-4 xs:w-full xs:flex xs:justify-center" style={{ display: 'flex', alignItems: 'center' }} onClick={renderProps.onClick}>
                    <img className="w-8" src="/images/ic_facebook.png" alt="" style={{ marginRight: '8px' }} />
                    <div className="text-base">Facebook</div>
                  </button>
                )}
              />
            </div> */}
          </div>

          <div className="text-center" style={{ marginTop: '30px' }}>
            <span className="text-register">Belum punya akun?</span>
            <Link href={param.redirect ? `register?redirect=${param.redirect}` : `register`}>
              <span className="text-register-action"> Daftar Sekarang</span>
            </Link>
          </div>

        </div>
        <div className="container-login" style={forgotPassword ? {display: "block"} : {display: "none"}}>
          <div className="text-primary-dark text-3xl font-bold">Lupa Password</div>

          <div style={{ marginTop: '50px' }}>
            <input
              className="form-control w-100"
              placeholder="Email Address"
              value={form.emailForgot}
              onChange={handleChange('emailForgot')}
              disabled={isLoading}
            />
          </div>

          <div className="text-error" style={{ marginTop: '16px', display: error.show ? 'block' : 'none' }}>
            {error.message}
          </div>

          <div style={{ marginTop: '38px' }}>
            <button
              type="button"
              className="btn-primary"
              onClick={doForgotPassword}
              disabled={isLoading || form.emailForgot === ''}
            >
              Kirim
            </button>
          </div>

          <hr style={{ margin: '30px 10px 10px 10px' }} />

        </div>
      </div>

      <Modal open={modalOtp} onClose={() => setModalOtp(false)}>
        <div className="card-modal" style={{ maxWidth: '568px' }}>
          <div className="container-login">
            <div style={{ textAlign: 'right' }}>
              <div style={{ display: 'inline-block' }} onClick={() => setModalOtp(false)} onKeyDown={() => setModalOtp(false)} className="pointer" role="button" tabIndex={0}>
                <img src="/images/ic_close_red.png" alt="" />
              </div>
            </div>
            <div className="w-full justify-center flex flex-wrap">
              <img src="/images/ic_otp.png" alt=""></img>
            </div>
            <div className="w-full flex justify-center text-text-primary font-bold text-xl mt-4">
              Masukkan Kode Verifikasi
            </div>
            <div className="w-full flex justify-center text-center text-text-primary text-lg tracking-wide">
              Kode verifikasi telah dikirimkan melalui <br/><br/> 
              Email ke {form.email}
            </div>
            <div className="w-full flex justify-center text-center text-text-primary text-lg tracking-wide mt-2">
              Kode Verifikasi
            </div>
            <div className="w-full">
              <form method="get" className="digit-group flex justify-center mt-4" data-group-name="digits" data-autosubmit="false" autocomplete="off">
                <input style={{ width: '25%' }} className="border border-gray-300 text-lg" type="text" id="digit-1" name="digit-1" data-next="digit-2" maxLength="1" ref={firstOtpRef} required value={form.otp.first} onChange={handleKeyOtp('first')}/>
                <input style={{ width: '25%' }} className="border border-gray-300 text-lg" type="text" type="text" id="digit-2" name="digit-2" data-next="digit-3" data-previous="digit-1" maxLength="1" required ref={secondOtpRef} value={form.otp.second} onChange={handleKeyOtp('second')}/>
                <input style={{ width: '25%' }} className="border border-gray-300 text-lg" type="text" type="text" id="digit-3" name="digit-3" data-next="digit-4" data-previous="digit-2" maxLength="1" required ref={thirdOtpRef} value={form.otp.third} onChange={handleKeyOtp('third')}/>
                <input style={{ width: '25%' }} className="border border-gray-300 text-lg" type="text" type="text" id="digit-4" name="digit-4" data-next="digit-5" data-previous="digit-3" maxLength="1" required ref={fourthOtpRef} value={form.otp.fourth} onChange={handleKeyOtp('fourth')}/>
              </form>
            </div>
            <div style={{ marginTop: '38px' }}>
              <button
                type="button"
                className="btn-primary"
                onClick={() => handleAfterOtp()}
                disabled={form.otp.first == ''|| form.otp.second == '' || form.otp.third == '' || form.otp.fourth == ''}
              >
                Verifikasi
              </button>
            </div>
            <div className="w-full flex justify-center text-center text-text-primary text-lg tracking-wide mt-4">
              Tidak Menerima Kode?
            </div>
            <div className="w-full flex justify-center text-center text-text-primary text-lg tracking-wide">
              <button className="text-primary" onClick={handleResendEmailVerification}>Kirim Ulang</button>
            </div>
          </div>
        </div>
      </Modal>

      <style jsx>
        {`
        .c {
          width: 100%;
          height: 100vh;
          display: flex;
          align-items: center;
          flex-wrap: wrap;
          justify-content: center;
        }

        .container-login {
          min-width: 568px;
          min-height: 596px;
          background: #FFFFFF;
          box-shadow: 0px 1px 12px rgba(0, 0, 0, 0.25);
          border-radius: 10px;
          padding: 2.5rem 2.5rem;
        }

        .break {
          flex-basis: 100%;
          height: 0;
        }

        .text-forgot-password {
          font-size: 12px;
          line-height: 14px;
          color: #6F0C06;
          margin-top: 20px;
          text-align: right;
        }

        .container-social-media {
          display: flex;
          justify-content: space-evenly;
          margin-top: 30px;
        }

        .text-register {
          font-size: 16px;
          line-height: 19px;
          color: #AFAFAF;
        }

        .text-register-action {
          font-weight: bold;
          font-size: 16px;
          line-height: 19px;
          color: #730C07;
          cursor: pointer;
        }

        @media only screen and (max-width: 600px) {
          .c {
            width: 100%;
            display: flex;
            align-items: center;
            flex-wrap: wrap;
            justify-content: center;
          }

          .container-login {
            margin-top: 12px;
            min-width: 90%;
            min-height: 596px;
            background: #FFFFFF;
            box-shadow: 0px 1px 12px rgba(0, 0, 0, 0.25);
            border-radius: 10px;
            padding: 2.5rem 2.5rem;
            width: 90%;
          }
        }
      `}

      </style>
    </div>
  );
};

Home.getInitialProps = (ctx) => {  
  const { token, endpoint } = ctx.store.getState().authentication;
  return {
      apiUrl: endpoint.apiUrl,
      token: token
  }
}

export default connect(
  (state) => state,
  { authenticate, socialAuthenticate }
)(Home);
