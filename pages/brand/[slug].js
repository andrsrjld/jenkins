import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import BaseLayout from '~/components/BaseLayout';
import FilterPrice from '~/components/FilterPrice';
import FilterAttributes from '~/components/FilterAttributes';
import CardProduct from '~/components/CardProduct';
import { getProductType } from '~/data/api/product-type';
import { getSellerDetail } from '~/data/api/seller';
import { getCart } from '~/data/api/cart';
import Modal from '~/components/Modal';
import moment from 'moment';


// Axios API Request
import axiosSetup from '../../services/api';

const origins = [
  {
    isChecked: false,
    name: 'Aceh',
    total: 8
  },
  {
    isChecked: false,
    name: 'Semarang',
    total: 2
  },
  {
    isChecked: false,
    name: 'Bandung',
    total: 7
  },
  {
    isChecked: false,
    name: 'Jawa',
    total: 5
  },
  {
    isChecked: false,
    name: 'Amerika Selatan',
    total: 6
  },
  {
    isChecked: false,
    name: 'Lain - lain',
    total: 8
  }
];

const Category = ({
  token, slug, products, filters, productType, sellerDetail, cart
}) => {
  console.log(sellerDetail)
  const data = {};
  const listAttribute = [];

  const [sort, setSort] = useState('date,DESC');
  const [valuesFilter, setValuesFilter] = useState([]);
  const [limit, setLimit] = useState(12);
  const [page, setPage] = useState(1);
  const [listProduct, setListProduct] = useState(products);
  const [loadMore, setLoadMore] = useState(true);
  const [modalFilter, setModalFilter] = useState(false);
  const [priceRange, setPriceRange] = useState({
    min: 0,
    max: 1000000
  })

  const handleSort = (event) => {
    setSort(event.currentTarget.value);
    loadFilteredProduct(valuesFilter, priceRange, event.currentTarget.value, limit);
    setLoadMore(true);
  }

  const handleLimit = (event) => {
    setLimit(event.currentTarget.value);
    loadFilteredProduct(valuesFilter, priceRange, sort, event.currentTarget.value);
    setLoadMore(true);
  }

  const handleChangeSlider = (min, max) => {
    console.log(min + " - " + max);
    setPriceRange({
      min: min,
      max: max
    })
    setTimeout(() => {
      loadFilteredProduct(valuesFilter, {min, max}, sort, limit);
    }, 1000)
    setLoadMore(true);
  }

  const handleFilter = (value, checked) => {
    let updatedFilter = [...valuesFilter];
    if(!checked){
      updatedFilter.map(item => {
        if(item.attribute === value.attribute){
          const removedIndex = updatedFilter.indexOf(value.value);
          item.value.splice(removedIndex, 1);
        }
      });
    } else {
      let existAttribute = 0;
      updatedFilter.map(item => {
        if(item.attribute === value.attribute){
          existAttribute += 1;
          item.value.push(value.value);
        }
      });

      if(existAttribute <= 0){
        updatedFilter.push({
          attribute: value.attribute,
          value: [value.value]
        });
      }
    }
    setValuesFilter(updatedFilter);
    loadFilteredProduct(updatedFilter, priceRange, sort, limit);
    setLoadMore(false)
  }

  const hanldeLoadMoreProduct = () => {
    setPage(page + 1);
    const api = axiosSetup(token);
    let valueFilterString = '';
    valuesFilter.map(item => {  
      let attributeName;
      if(item.value.length > 0){
        if(item.attribute.toLowerCase() === "bean species"){
          attributeName = "species";
        } else {
          attributeName = item.attribute.toLowerCase()
        }
        valueFilterString += `&${attributeName}=${item.value.join(",")}`;
      }
    });
    setTimeout(() => {
      const productParam = `layout_type=list_layout&type_slug=All&seller_slug=${slug}&status=active${valueFilterString}&page=${page + 1}&limit=${limit}&order=${sort}&price=${priceRange.min},${priceRange.max}`;
      api.get('/products?' + productParam).then((response) => {
        if(response.data.data){
          setListProduct(listProduct.concat(response.data.data));
        } else {
          setLoadMore(false);
        }
      });
    }, 500);
  }

  const loadFilteredProduct = (updatedFilter, updatedPrice, updatedSort, updatedLimit) => {
    const api = axiosSetup(token);
    let valueFilterString = '';
    updatedFilter.map(item => {  
      let attributeName;
      if(item.value.length > 0){
        if(item.attribute.toLowerCase() === "bean species"){
          attributeName = "species";
        } else {
          attributeName = item.attribute.toLowerCase()
        }
        valueFilterString += `&${attributeName}=${item.value.join(",")}`;
      }
    });
    setPage(1);
    const productParam = `layout_type=list_layout&type_slug=All&seller_slug=${slug}&status=active${valueFilterString}&page=${page}&limit=${updatedLimit}&order=${updatedSort}&price=${updatedPrice.min},${updatedPrice.max}`;
    api.get('/products?' + productParam).then((response) => {
      setListProduct(response.data.data);
    });
  }

  if(filters){
    filters.map(attribute => {
      const attributeBlock = (<FilterAttributes title={attribute.name} data={attribute.values} handleFilter={handleFilter} />);
      listAttribute.push(attributeBlock);
    });
  }

  return (
    <div>
      <BaseLayout productType={productType} token={token} cart={cart}>
        <div id="breadcumb" className="breadcrumb-content">
          <div className="breadcumb">
            Home &#x0226B; Brand &#x0226B;
            {' '}
            <span style={{ color: '#010101' }}>{slug}</span>
          </div>
        </div>
        <div className="w-full banner-seller">
          <img src={sellerDetail.seller.url_banner} />
        </div>
        <div className="c-content seller-detail-content flex flex-wrap">
          <div className="w-1/5 xs:w-full sm:w-full md:w-1/5 lg:w-1/5">
            <div className="c-filter">
              <div className="card-brand">
                <img className="card-brand-img" src={sellerDetail.seller.photo} alt="" />
                <div className="card-brand-name">
                  {sellerDetail.seller.store_name}
                </div>
                <div className="card-brand-address">
                  {sellerDetail.seller.address.province}, {sellerDetail.seller.address.country} Joined since {moment(sellerDetail.seller.created_at).format("DD MMM YYYY")}
                </div>
                <div className="card-brand-address" style={{color: "#444"}}>
                  Jam Operasional : {sellerDetail.seller.operation_hours}
                </div>
                {/* <div style={{ display: 'flex', justifyContent: 'center', margin: '16px 0px' }}>
                  <img src="/images/ic_facebook_outline.png" alt="" width="24" height="24" />
                  <img src="/images/ic_instagram.png" alt="" width="24" height="24" style={{ marginLeft: '12px' }} />
                </div> */}
              </div>
              <div className="c-filter xs:hidden sm:hidden md:flex lg:flex">
                <span className="w-100 title-filter" style={{ marginTop: '80px' }}>Urutkan Berdasarkan</span>
                <FilterPrice callbackSlider={handleChangeSlider} priceRange={priceRange} />
                {/* {listAttribute} */}
              </div>
            </div>
          </div>
          <div className="mobile-filter w-full hidden xs:flex sm:flex md:hidden lg:hidden">
              <button className="mobile-filter-button btn-default" onClick={() => setModalFilter(true)}>Filter</button>
            </div>
          <div className="w-4/5 xs:w-full sm:w-full md:w-4/5 lg:w-4/5">
            <div className="c-products">
              <div className="c-filter-product">
                <div className="c-filter-page flex items-center">
                  Menampilkan
                  <select onChange={handleLimit} className="form-filter-page">
                    <option value="12">12</option>
                    <option value="24">24</option>
                    <option value="36">36</option>
                  </select>
                  dari <span>&nbsp;{listProduct ? listProduct.length : 0}</span>
                </div>
                <div className="c-filter-page flex items-center">
                  Urutkan
                  <select onChange={handleSort} className="form-filter-page">
                    <option value="product_name">Nama Produk</option>
                    <option value="popular">Popularitas</option>
                    <option value="price,ASC">Harga Terendah</option>
                    <option value="price,DESC">Harga Tertinggi</option>
                  </select>
                </div>
              </div>

              <div className="c-content-products flex flex-wrap">
                {
                  listProduct ? listProduct.map((v) => (
                    <CardProduct data={v} />
                  )) : ""
                }
              </div>
            </div>
          </div>
        </div>

        <Modal open={modalFilter} onClose={() => setModalFilter(false)}>
          <div className="card-modal card-modal-filter">
            <div style={{ textAlign: 'right' }}>
              <div style={{ display: 'inline-block' }} onClick={() => setModalFilter(false)} onKeyDown={() => setModalFilter(false)} className="pointer" role="button" tabIndex={0}>
                <img src="/images/ic_close_red.png" alt="" />
              </div>
            </div>
            <div className="c-filter">
              <FilterPrice />
              {listAttribute}
            </div>
          </div>
        </Modal>

      </BaseLayout>
      <style jsx>
        {`
        .breadcumb {
          font-size: 14px;
          color: #696969;
        }
      
        .c-content {
          width: 100%;
          display: flex;
        }
      
        .c-filter {
          width: 100%;
          flex-wrap: wrap;
          justify-content: flex-start;
          flex-direction: column;
        }
      
        .title-filter {
          font-weight: 500;
          font-size: 18px;
          color: #696969;
        }
      
        .c-products {
          width: 100%;
        }
      
        .c-filter-product {
          width: 100%;
          display: flex;
          justify-content: space-between;
        }
      
        .c-filter-page {
          font-size: 14px;
          color: #696969;
        }
      
        .form-filter-page {
          background: rgba(248, 248, 248, 0.75);
          border: 1px solid #F2F2F2;
          box-sizing: border-box;
          padding: 4px 4px;
          font-size: 16px;
          color: #696969;
          margin: 0px 4px;
          font-family: 'Gotham';
        }
      
        .c-content-products {
          display: flex;
          flex-wrap: wrap;
          padding: 0 0 0 24px;
        }
      
        .c-brand {
          width: 32%;
          background: #FFFFFF;
          box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
          border-radius: 5px;
          margin-top: 8px;
          margin-bottom: 16px;
          display: flex;
          justify-content: center;
          padding: 8px;
          cursor: pointer;
          flex-shrink: 1;
          align-self:center;
        }

        .c-brand img{
          align-self:center;
          flex-shrink: 1;
        }
      
        .card-brand {
          width: 100%;
          background: #FFFFFF;
          box-shadow: 0px 0px 4px rgba(0, 0, 0, 0.25);
          border-radius: 5px;
        }
      
        .card-brand-img {
          width: 80%;
          margin: 16px 10%;
        }
      
        .card-brand-name {
          font-weight: bold;
          font-size: 18px;
          line-height: 17px;
          text-align: center;
          color: #696969;
        }
      
        .card-brand-address{
          font-weight: 500;
          font-size: 14px;
          line-height: 17px;
          text-align: center;
          color: #9C9C9C;
          margin: 4px 10px;
        }
        .banner-seller img {
          width: 100%;
        }
        .breadcrumb-content{
          padding: 20px 0;
        }
        .seller-detail-content{
          padding: 20px 0;
        }
        .c-filter-product{
          padding-left:24px;
        }
        .card-brand-img{
          width:200px;
          margin:0 auto;
        }
        .mobile-filter-button{
          padding:10px 15px;
          border:1px solid #ddd;
          border-radius:10px;
          margin:30px auto;
          display:block;
        }
        .c-filter .card-brand{
          padding: 15px 10px;
        }
        @media (min-width: 320px) and (max-width: 767px) {
          .breadcrumb-content{
            padding: 0 20px;
          }
          .banner-seller{
            padding: 40px 0;
          }
          .seller-detail-content{
            padding: 0 20px;
          }
          .card-brand-img{
            width:250px;
            margin:0 auto;
          }
          .c-filter-product, .c-content-products{
            padding-left: 0;
          }
          .form-filter-page{
              padding:4px 0;
              font-size: 12px;
          }
          .c-filter-page{
              font-size: 12px
          }
        }
        `}
      </style>
    </div>
  );
};

Category.propTypes = {
  slug: PropTypes.string,
  product: PropTypes.array,
  filters: PropTypes.array
};

Category.getInitialProps = async (ctx) => {
  const { slug } = ctx.query;
  const { token } = ctx.store.getState().authentication;
  const api = axiosSetup(token);

  const sellerDetailResp = await getSellerDetail(token, slug);
  const sellerDetail = sellerDetailResp.data;

  const productParam = `layout_type=list_layout&type_slug=All&seller_slug=${slug}&status=active&page=1&limit=100&order=product_name,ASC`;
  const productResp = await api.get('/products?' + productParam);
  const products = productResp.data.data;
  console.log(products)

  const attributeResp = await api.get(`/product_types_attributes/seller?seller_slug=${slug}`);
  const filters = attributeResp.data.data;

  const productTypeResp = await getProductType();
  const productType = productTypeResp.data;

  const cartResp = await getCart(token);
  const cart = cartResp.data ? cartResp.data.cart : [];

  return {
    token,
    slug,
    products,
    filters,
    productType,
    sellerDetail,
    cart
  };
};

export default Category;
