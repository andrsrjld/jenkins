import React, { useState, useRef, useEffect } from 'react';
import Link from 'next/link';
import PropTypes from 'prop-types';
import InfiniteScroll from 'react-infinite-scroll-component';
import BaseLayout from '~/components/BaseLayout';
import { getProductType } from '~/data/api/product-type';
import { getSellers } from '~/data/api/seller';
import { getCart } from '~/data/api/cart';

// Axios API Request
import axiosSetup from '../../services/api';

const Category = ({
  slug, productType, sellers, token, cart
}) => {
  const data = {};

  const [sort, setSort] = useState('store_name,ASC');
  const [limit, setLimit] = useState(12);
  const [page, setPage] = useState(1);
  const [listSeller, setListSeller] = useState(sellers);
  const [loadMore, setLoadMore] = useState(true);

  const handleSort = (event) => {
    setSort(event.currentTarget.value);
  }

  const handleLimit = (event) => {
    setLimit(event.currentTarget.value);
  }

  const hanldeLoadMoreSeller = () => {
    const api = axiosSetup(token);

    setTimeout(() => {
      const productParam = `layout_type=list_layout&status=Approved&order=${sort}&page=${page + 1}&limit=${limit}`;
      api.get('/seller/account?' + productParam).then((response) => {
        if(response.data.data){
          setListSeller(listSeller.concat(response.data.data));
        } else {
          setLoadMore(false);
        }
      });
    }, 500);

    setPage(page + 1);
  }

  return (
    <div>
      <BaseLayout productType={productType} token={token}>
        <div id="breadcumb" className="breadcrumb-content">
          <div className="breadcumb">
            Home &#x0226B;
            {' '}
            <span style={{ color: '#010101' }}>Merk</span>
          </div>
        </div>

        <div className="c-content seller-content">
          <div className="c-products">
            <div className="c-filter-product">
              <div className="c-filter-page flex items-center">
                Menampilkan
                <select className="form-filter-page" onChange={handleLimit}>
                  <option value="12">12</option>
                  <option value="24">24</option>
                  <option value="36">36</option>
                </select>
                dari <span>&nbsp;{listSeller ? listSeller.length : 0}</span>
              </div>
              <div className="c-filter-page flex items-center">
                Urutkan
                <select className="form-filter-page" onChange={handleSort}>
                  <option value="store_name,ASC">Nama Toko</option>
                </select>
              </div>
            </div>

            <div className="c-content-products">
              <InfiniteScroll
                dataLength={listSeller ? listSeller.length : 0}
                next={hanldeLoadMoreSeller}
                hasMore={loadMore}
                className="flex flex-wrap w-full"
              >
                {
                  listSeller ? listSeller.map((v) => (
                    <Link href={`/brand/${v.seller.slug}`}>
                      <div className="w-1/3 xs:w-1/2 sm:w-1/2 md:1/3 lg:w-1/3 p-2 xs:p-1 sm:p-1 md:p-2 lg:p-2">
                        <div className="c-brand">
                          <img src={v.seller.photo ? v.seller.photo : "/images/dummy.png"} alt="" />
                          {v.seller.photo ? '' : (
                            <h4>{v.name}</h4>
                          )}
                        </div>
                      </div>
                    </Link>
                  )) : ""
                }
              </InfiniteScroll>
            </div>
          </div>
        </div>
      </BaseLayout>
      <style jsx>
        {`
        .breadcumb {
          font-size: 14px;
          color: #696969;
        }
      
        .c-content {
          width: 100%;
          display: flex;
        }
      
        
        .c-filter {
          width: 350px;
          display: flex;
          flex-wrap: wrap;
          justify-content: flex-start;
          flex-direction: column;
        }
      
        .title-filter {
          font-weight: 500;
          font-size: 18px;
          color: #696969;
        }
      
        .c-products {
          width: 100%;
        }
      
        .c-filter-product {
          width: 100%;
          display: flex;
          justify-content: space-between;
        }
      
        .c-filter-page {
          font-size: 16px;
          color: #696969;
        }
      
        .form-filter-page {
          background: rgba(248, 248, 248, 0.75);
          border: 1px solid #F2F2F2;
          box-sizing: border-box;
          padding: 4px 4px;
          font-size: 16px;
          color: #696969;
          margin: 0px 4px;
          font-family: 'Gotham';
        }
      
        .c-content-products {
          display: flex;
          flex-wrap: wrap;
          justify-content: space-between;
          padding: 48px 0;
        }
      
        .c-brand {
          background: #FFFFFF;
          box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
          border-radius: 5px;
          margin-top: 8px;
          margin-bottom: 16px;
          display: flex;
          justify-content: center;
          padding: 8px;
          cursor: pointer;
          flex-wrap: wrap;
        }
        .c-brand h4{
          position:absolute;
          align-self: center;
        }
      
        .card-brand {
          width: 100%;
          background: #FFFFFF;
          box-shadow: 0px 0px 4px rgba(0, 0, 0, 0.25);
          border-radius: 5px;
        }
      
        .card-brand-img {
          width: 80%;
          margin: 16px 10%;
        }
      
        .card-brand-name {
          font-weight: bold;
          font-size: 18px;
          line-height: 17px;
          text-align: center;
          color: #696969;
        }
      
        .card-brand-address{
          font-weight: 500;
          font-size: 14px;
          line-height: 17px;
          text-align: center;
          color: #9C9C9C;
          margin: 4px 10px;
        }
        .breadcrumb-content{
          padding: 20px 0;
        }
        .seller-content{
          padding: 0;
        }
        .c-brand img{
          object-fit: contain;
        }
        @media (min-width: 768px) and (max-width: 1024px) {
        }
        
        @media (min-width: 320px) and (max-width: 767px) {
          .breadcrumb-content{
            padding: 0 20px;
          }
          .seller-content{
            padding: 0 20px;
          }
          .c-products{
            padding:0;
          }
          .c-content-products{
            padding:0 0 30px;
          }
          .c-brand{
            margin-bottom:8px;
            min-height: 1px;
          }
        }
        @media (min-width: 0) and (max-width:372px){
          .c-filter-page{
            font-size:12px;
          }
          .c-filter-page .form-filter-page{
            font-size:14px;
          }
        }
        `}
      </style>
    </div>
  );
};

Category.propTypes = {
  slug: PropTypes.string,
  productType: PropTypes.array,
  sellers: PropTypes.array
};

Category.getInitialProps = async (ctx) => {
  const { token } = ctx.store.getState().authentication;

  const productTypeResp = await getProductType();
  const productType = productTypeResp.data;

  const sellerResp = await getSellers(token, 'store_name,asc', '1', '12', 'Approved');
  const sellers = sellerResp.data;

  const cartResp = await getCart(token);
  const cart = cartResp.data ? cartResp.data.cart : [];

  return { 
    productType,
    token,
    sellers,
    cart
  };
};

export default Category;
