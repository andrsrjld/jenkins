import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import BaseLayout from '~/components/BaseLayout';
import InfiniteScroll from 'react-infinite-scroll-component';
import FilterPrice from '~/components/FilterPrice';
import FilterAttributes from '~/components/FilterAttributes';
import CardProduct from '~/components/CardProduct';
import Modal from '~/components/Modal';

// Axios API Request
import axiosSetup from '../../services/api';

const products = ({products, filters, token, keyword}) => {
    const listAttribute = [];

    const [sort, setSort] = useState('date,DESC');
    const [valuesFilter, setValuesFilter] = useState([]);
    const [limit, setLimit] = useState(12);
    const [page, setPage] = useState(1);
    const [listProduct, setListProduct] = useState(products)
    const [loadMore, setLoadMore] = useState(true);
    const [modalFilter, setModalFilter] = useState(false);
    const [priceRange, setPriceRange] = useState({
        min: 0,
        max: 1000000
      })

    const handleSort = (event) => {
        setSort(event.currentTarget.value);
        loadFilteredProduct(valuesFilter, priceRange, event.currentTarget.value, limit);
        setLoadMore(true);
    }

    const handleLimit = (event) => {
        setLimit(event.currentTarget.value);
        loadFilteredProduct(valuesFilter, priceRange, sort, event.currentTarget.value);
        setLoadMore(true);
    }

    const handleChangeSlider = (min, max) => {
        console.log(min + " - " + max);
        setPriceRange({
        min: min,
        max: max
        })
        setTimeout(() => {
        loadFilteredProduct(valuesFilter, {min, max}, sort, limit);
        }, 1000)
        setLoadMore(true);
    }

    const handleFilter = (value, checked) => {
        let updatedFilter = [...valuesFilter];
        if(!checked){
            updatedFilter.map(item => {
                if(item.attribute === value.attribute){
                    const removedIndex = updatedFilter.indexOf(value.value);
                    item.value.splice(removedIndex, 1);
                }
            });
        } else {
            let existAttribute = 0;
            updatedFilter.map(item => {
                if(item.attribute === value.attribute){
                    existAttribute += 1;
                    item.value.push(value.value);
                }
            });

            if(existAttribute <= 0){
                updatedFilter.push({
                    attribute: value.attribute,
                    value: [value.value]
                });
            }
        }
        setValuesFilter(updatedFilter);
        loadFilteredProduct(updatedFilter, priceRange, sort, limit);
        setLoadMore(true);
    }

    const hanldeLoadMoreProduct = () => {
        const api = axiosSetup(token);
        let valueFilterString = '';
        valuesFilter.map(item => {  
            let attributeName;
            if(item.value.length > 0){
                if(item.attribute.toLowerCase() === "bean species"){
                    attributeName = "species";
                } else {
                    attributeName = item.attribute.toLowerCase()
                }
                valueFilterString += `&${attributeName}=${item.value.join(",")}`;
            }
        });
        setTimeout(() => {
            const productParam = `layout_type=list_layout&type_slug=All&status=active${valueFilterString}&page=${page + 1}&limit=${limit}&order=${sort}&price=${priceRange.min},${priceRange.max}&keyword=${keyword}`;
            api.get('/products?' + productParam).then((response) => {
                if(response.data.data){
                    setListProduct(listProduct.concat(response.data.data));
                } else {
                    setLoadMore(false);
                }
            });
        }, 500);
        setPage(page + 1);
    }

    const loadFilteredProduct = (updatedFilter, updatedPrice, updatedSort, updatedLimit) => {
        const api = axiosSetup(token);
        let valueFilterString = '';
        updatedFilter.map(item => {  
            let attributeName;
            if(item.value.length > 0){
                if(item.attribute.toLowerCase() === "bean species"){
                    attributeName = "species";
                } else {
                    attributeName = item.attribute.toLowerCase()
                }
                valueFilterString += `&${attributeName}=${item.value.join(",")}`;
            }
        });
        setPage(1);
        const productParam = `layout_type=list_layout&type_slug=All&status=active${valueFilterString}&page=${page}&limit=${updatedLimit}&order=${updatedSort}&price=${updatedPrice.min},${updatedPrice.max}&keyword=${keyword}`;
        api.get('/products?' + productParam).then((response) => {
            setListProduct(response.data.data);
        });
    }

    if(filters){
        filters.map(attribute => {
            const attributeBlock = (<FilterAttributes title={attribute.name} data={attribute.values} handleFilter={handleFilter} />);
            listAttribute.push(attributeBlock);
        });
    }

    return (
        <div className="flex flex-wrap w-full">
            <div className="w-1/5 xs:w-full sm:w-full md:w-1/5 lg:w-1/5">
                <div className="mobile-filter hidden xs:flex sm:flex md:hidden lg:hidden">
                    <button className="mobile-filter-button btn-default" onClick={() => setModalFilter(true)}>Filter</button>
                </div>
                <div className="c-filter xs:hidden sm:hidden md:flex lg:flex">
                    <span className="w-100 title-filter">Urutkan Berdasarkan</span>
                    <FilterPrice callbackSlider={handleChangeSlider} priceRange={priceRange} />
                    {listAttribute}
                </div>
            </div>
            <div class="w-4/5 xs:w-full sm:w-full md:w-4/5 lg:w-4/5">
                <div className="c-products">
                    <div className="c-filter-product">
                        <div className="c-filter-page flex items-center">
                            Menampilkan
                            <select onChange={handleLimit} className="form-filter-page">
                            <option value="12">12</option>
                            <option value="24">24</option>
                            <option value="36">36</option>
                            </select>
                            dari <span>&nbsp;{listProduct ? listProduct.length : 0}</span>
                        </div>
                        <div className="c-filter-page flex items-center">
                            Urutkan
                            <select onChange={handleSort} className="form-filter-page">
                            <option value="product_name">Nama Produk</option>
                            <option value="popular">Popularitas</option>
                            <option value="price,ASC">Harga Terendah</option>
                            <option value="price,DESC">Harga Tertinggi</option>
                            </select>
                        </div>
                    </div>

                    <div className="c-content-products">
                        <InfiniteScroll
                            dataLength={listProduct ? listProduct.length : 0}
                            next={hanldeLoadMoreProduct}
                            hasMore={loadMore}
                            className="flex flex-wrap w-full"
                        >
                            {
                            listProduct ? listProduct.map((v) => (
                                <CardProduct data={v} />
                            )) : <h4 style={{marginTop: "30px", marginBottom:"30px", textAlign:"center", fontSize:"20px", width: "100%"}}>Produk yang anda cari tidak ada</h4>
                            }
                        </InfiniteScroll>
                    </div>
                </div>
            </div>
            <Modal open={modalFilter} onClose={() => setModalFilter(false)}>
                <div className="card-modal card-modal-filter">
                    <div style={{ textAlign: 'right' }}>
                        <div style={{ display: 'inline-block' }} onClick={() => setModalFilter(false)} onKeyDown={() => setModalFilter(false)} className="pointer" role="button" tabIndex={0}>
                            <img src="/images/ic_close_red.png" alt="" />
                        </div>
                    </div>
                    <div className="c-filter">
                        <FilterPrice />
                        {listAttribute}
                    </div>
                </div>
            </Modal>
            <style jsx>
            {`
                .breadcumb {
                    font-size: 14px;
                    color: #696969;
                  }
                
                  .c-content {
                    width: 100%;
                    display: flex;
                  }
                
                  .c-filter {
                    width: 100%;
                    flex-wrap: wrap;
                    justify-content: flex-start;
                    flex-direction: column;
                  }
                
                  .title-filter {
                    font-weight: 500;
                    font-size: 18px;
                    color: #696969;
                  }
                
                  .c-products {
                    width: 100%;
                    padding: 0px 0px 0px 24px;
                  }
                
                  .c-filter-product {
                    width: 100%;
                    display: flex;
                    justify-content: space-between;
                  }
                
                  .c-filter-page {
                    font-size: 16px;
                    color: #696969;
                  }
                
                  .form-filter-page {
                    background: rgba(248, 248, 248, 0.75);
                    border: 1px solid #F2F2F2;
                    box-sizing: border-box;
                    padding: 4px 4px;
                    font-size: 16px;
                    color: #696969;
                    margin: 0px 4px;
                    font-family: 'Gotham';
                  }
                
                  .c-content-products {
                    display: flex;
                    flex-wrap: wrap;
                    justify-content: space-between;
                  }
                  .c-content-products > div:first-child{
                    width:100%;
                  }
                  .product-content{
                    padding:0 70px;
                  }
                  .mobile-filter-button{
                    padding:10px 15px;
                    border:1px solid #ddd;
                    border-radius:10px;
                    margin:0 auto;
                    display:block;
                  }
                  @media (min-width: 768px) and (max-width: 1024px) {
                  }
                  
                  @media (min-width: 320px) and (max-width: 767px) {
                    .product-content{
                      padding:0 20px;
                    }
                    .c-products{
                      padding:0;
                      margin-top:30px;
                    }
                    .card-modal-filter{
                        padding:20px;
                        height:100%;
                    }
                    .card-modal-filter .c-filter{
                        padding:0 20px;
                        height:95%;
                        overflow-y: auto;
                    }
                    .form-filter-page{
                        padding:4px 0;
                        font-size: 12px;
                    }
                    .c-filter-page{
                        font-size: 12px
                    }
                  }
            `}
            </style>
        </div>
    )
}

export default products;