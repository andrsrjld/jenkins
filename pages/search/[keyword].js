import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import BaseLayout from '~/components/BaseLayout';
import FilterPrice from '~/components/FilterPrice';
import FilterAttributes from '~/components/FilterAttributes';
import CardProduct from '~/components/CardProduct';
import { getProductType } from '~/data/api/product-type';
import { getSellers } from '~/data/api/seller';
import Products from './products';
import Sellers from './sellers';
import { getCart } from '~/data/api/cart';

// Axios API Request
import axiosSetup from '../../services/api';

// const origins = [
//   {
//     isChecked: false,
//     name: 'Aceh',
//     total: 8
//   },
//   {
//     isChecked: false,
//     name: 'Semarang',
//     total: 2
//   },
//   {
//     isChecked: false,
//     name: 'Bandung',
//     total: 7
//   },
//   {
//     isChecked: false,
//     name: 'Jawa',
//     total: 5
//   },
//   {
//     isChecked: false,
//     name: 'Amerika Selatan',
//     total: 6
//   },
//   {
//     isChecked: false,
//     name: 'Lain - lain',
//     total: 8
//   }
// ];

const Category = ({
  keyword, products, filters, productType, sellers, token, cart
}) => {
  const [activeTab, setActiveTab] = useState("product");

  const handleActiveTab = (e) => {
    setActiveTab(e.currentTarget.value);
  }

  const data = {};
  return (
    <div>
      <BaseLayout productType={productType} token={token} cart={cart}>
        <div id="breadcumb" className="breadcrumb-wrapper">
          <div className="breadcumb">
            Home &#x0226B; Search &#x0226B;
            {' '}
            <span style={{ color: '#010101' }}>{keyword}</span>
          </div>
        </div>

        <div className="c-content flex flex-wrap search-content">
          <div className="w-full flex flex-wrap justify-start xs:justify-center sm:justify-center tab-content-nav">
            <button className="flex tab-nav-menu items-center" value="product" onClick={handleActiveTab} style={activeTab === "product" ? {borderBottom: "3px solid #010101"} : {}}>
              <img src="/images/ic_product.png" className="tab-nav-icon" />
              <h3>Product</h3>
            </button>
            <button className="flex flex tab-nav-menu items-center" value="seller" onClick={handleActiveTab} style={activeTab === "seller" ? {borderBottom: "3px solid #010101"} : {}}>
              <img src="/images/ic_toko.png" className="tab-nav-icon" />
              <h3>Merk</h3>
            </button>
          </div>
          <div className="w-full" style={activeTab !== "product" ? {display: "none"} : {display: "block"}}>
            <Products products={products} token={token} filters={filters} keyword={keyword} />
          </div>
          <div className="w-full" style={activeTab !== "seller" ? {display: "none"} : {display: "block"}}>
            <Sellers sellers={sellers} token={token} keyword={keyword} />
          </div>
        </div>
      </BaseLayout>
      <style jsx>
        {`
        .breadcrumb-wrapper{
          padding:20px 0;
        }
        .breadcumb {
          font-size: 14px;
          color: #696969;
        }
      
        .c-content {
          width: 100%;
          display: flex;
        }
      
        .c-filter {
          width: 350px;
          display: flex;
          flex-wrap: wrap;
          justify-content: flex-start;
          flex-direction: column;
        }
      
        .title-filter {
          font-weight: 500;
          font-size: 18px;
          color: #696969;
        }
      
        .c-products {
          width: 100%;
          padding: 0px 0px 0px 24px;
        }
      
        .c-filter-product {
          width: 100%;
          display: flex;
          justify-content: space-between;
        }
      
        .c-filter-page {
          font-size: 16px;
          color: #696969;
        }
      
        .form-filter-page {
          background: rgba(248, 248, 248, 0.75);
          border: 1px solid #F2F2F2;
          box-sizing: border-box;
          padding: 4px 4px;
          font-size: 16px;
          color: #696969;
          margin: 0px 4px;
          font-family: 'Gotham';
        }
      
        .c-content-products {
          display: flex;
          flex-wrap: wrap;
          justify-content: space-between;
        }
        .tab-nav-icon{
          width: 48px;
        }
        .tab-nav-menu{
          padding: 15px 40px;
          cursor:pointer;
        }
        .tab-nav-menu img{
          padding-right: 20px
        }
        .tab-nav-menu h3{
          font-size:24px;
        }
        .tab-content-nav{
          border-bottom:1px solid #ccc;
          margin-bottom:20px;
        }
        .search-content{
          padding: 20px 0;
        }
        @media (min-width: 320px) and (max-width: 767px) {
          .breadcrumb-content{
            padding: 0 20px;
          }
          .seller-content{
            padding: 0 20px;
          }
          .c-products{
            padding:0;
          }
          .c-content-products{
            padding:0 0 30px;
          }
          .c-brand{
            margin-bottom:8px;
          }
          .product-content{
            padding:0 20px;
          }
          .search-content{
            padding: 0 20px;
          }
          .breadcrumb-wrapper{
            padding:0 20px;
          }
          .breadcumb{
            text-align:center;
          }
          .tab-nav-icon{
            width: 35px;
          }
          .tab-nav-menu h3{
            font-size:16px;
          }
          .tab-nav-menu{
            padding: 15px 20px;
          }
        }
        `}
      </style>
    </div>
  );
};

Category.propTypes = {
  keyword: PropTypes.string
};

Category.getInitialProps = async (ctx) => {
  const { keyword } = ctx.query;
  const { token } = ctx.store.getState().authentication;
  const api = axiosSetup(token);

  const productParam = `layout_type=list_layout&type_slug=All&status=active&page=1&limit=12&order=date,DESC&keyword=${keyword}`;
  const productResp = await api.get('/products?' + productParam);
  const products = productResp.data.data;

  let filters = [];
  const attributeResp = await api.get(`/attribute?layout_type=list_layout`);
  filters = attributeResp.data.data;

  for(let i=0; i < filters.length; i++) {
    const valueResp = await api.get(`/value?layout_type=list_layout&slug=${filters[i].slug}`);
    filters[i].values = valueResp.data.data;
  }

  const productTypeResp = await getProductType();
  const productType = productTypeResp.data;
  
  const sellerResp = await getSellers(token, "created_at,DESC", "1", "12", null, keyword);
  const sellers = sellerResp.data;
  console.log(sellerResp);

  const cartResp = await getCart(token);
  const cart = cartResp.data ? cartResp.data.cart : [];

  return { 
    keyword,
    products,
    filters,
    productType,
    sellers,
    token,
    cart
  };
};

export default Category;
