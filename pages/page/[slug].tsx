import React from 'react';
import BaseLayout from 'components/BaseLayout';
import { getProductType } from 'data/api/product-type';

import { getCart } from 'data/api/cart';
import { getOnePage } from 'data/api/pages';
import CardProduct from 'components/CardProduct';

const data = [
  {
    image: '/images/ic_helpful.png',
    step: 'Helpful',
    title: 'Coba Isi Quiz',
    desc: 'Kami senang membantu anda untuk mendapatkan produk-produk yang dapat diandalkan'
  },
  {
    image: '/images/ic_insightful.png',
    step: 'Insightful',
    title: 'Menemukan Kecocokan',
    desc: 'Kami senang menjadi salah satu sumber informasi yang dapat menambah wawasan kita bersama'
  },
  {
    image: '/images/ic_trustworthy.png',
    step: 'Trustworthy',
    title: 'Coba Isi Quiz',
    desc: 'Kami selalu berusaha menjadi sumber informasi dan produk yang terpercaya'
  },
  {
    image: '/images/ic_supportive.png',
    step: 'Supportive',
    title: 'Coba Isi Quiz',
    desc: 'kami sangat antusias turut mendukung kemajuan industri kopi, teh dan cokelat nusantara'
  }
];

const About = ({productType, token, cart, content}) => (
  <div>
    <BaseLayout productType={productType} token={token} cart={cart}>
      <div id="breadcumb" className="breadcrumb-wrapper">
        <div className="breadcumb">
            Home &#x0226B;
          {' '}
          <span style={{ color: '#010101' }}>{content.title}</span>
        </div>
      </div>
      <div className="c">
        
        {
          content.images && content.images.map(v => (
            <div className="w-full px-2">
              {
                v.link ? 
                <a href={v.link}>
                  <img className={`w-full h-auto object-contain ${v.link ? 'cursor-pointer' : ''}`} src={v.url} alt="banner"/>
                </a>
                :
                <img className={`w-full h-auto object-contain ${v.link ? 'cursor-pointer' : ''}`} src={v.url} alt="banner"/>
              }
            </div>
          ))
        }

        {
          content.products && content.products.length > 0
          ?
            <div className="mt-8" style={{ display: 'flex', flexWrap: 'wrap' }}>
              {
                content.products.map((v) => (
                  <CardProduct data={v} loading={false} />
                ))
              }
            </div>
          : null
        }

        {
          content.content && content.content.length > 0
          ?
            <div className="mt-8">
              <div dangerouslySetInnerHTML={{__html: content.content}} />
            </div>
          : null
        }

      </div>
    </BaseLayout>
    <style jsx>
      {`
        .breadcrumb-wrapper{
          padding: 20px 0;
        }
        .image {
          height: 150px;
          margin:0 auto;
        }

        .c {
          padding: 20px 0 80px 0;
        }

        .c-content {
          display: flex;
          margin: 32px 0px;
        }

        .c-card {
          background: #FFFFFF;
          box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.1);
          border-radius: 5px;
          padding: 24px;
          margin: 0px 10px;
        }

        .card-title {
          font-weight: 500;
          font-size: 20px;
          line-height: 26px;
          text-align: center;
          letter-spacing: 0.05em;
          color: #696969;
        }

        .title {
          font-weight: bold;
          font-size: 18px;
          line-height: 23px;
          letter-spacing: 0.05em;
          color: #696969;
        }
        
        .divider {
          height: 4px;
          background-color: #010101;
          border: none;
          border-radius: 9px;
          width: 100px;
        }

        .desc {
          font-size: 16px;
          line-height: 28px;
          letter-spacing: 0.05em;
          color: #696969;
        }

        .card {
          background: #FFFFFF;
          box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.1);
          border-radius: 5px;
        }
        @media (min-width: 320px) and (max-width: 767px) {
          .c{
            padding:0 20px;
            margin-bottom:30px;
          }
          .breadcrumb-wrapper{
            padding: 0 20px;
            margin-bottom:20px;
            text-align:center;
          }
          .title{
            text-align:center;
          }
          .content{
            flex-wrap: wrap;
          }
          .content img{
            width: 100%;
          }
        }
        `
      }
    </style>
  </div>
);

About.getInitialProps = async (ctx) => {
  const { token, endpoint } = ctx.store.getState().authentication;
  const { apiUrl } = endpoint

  const productTypeResp = await getProductType();
  const productType = productTypeResp.data;

  const cartResp = await getCart(token);
  const cart = cartResp.data ? cartResp.data.cart : [];

  const contentRes = await getOnePage(apiUrl, ctx.query.slug );
  const content = contentRes.data;
  console.log(content)

  return { 
    productType,
    token,
    cart,
    content
  };
};

export default About;
