import { sendEmail } from '~/services/email-service';

export default async (req, res) => {
    const { body } = req;
    const options = {
        from: 'admin@lakkon.id',
        to: 'vaxtramaendhapaskha@gmail.com',
        subject: `Contact us from ${body.name}`,
        html: `
            <p>Customer ${body.name} (${body.email}) mengirimkan pesan kepada anda</p>
            <label>Pesan : </label>
            <p>${body.message}</p>
        `
      };
    const emailRes = await sendEmail(options);
    try{
        if(emailRes.accepted){
            res.status(200).json({
                status: "OK",
                message: "Email berhasil dikirim"
            })
        } else {
            res.status(500).json({
                status: "failed",
                code: emailRes.responseCode,
                message: emailRes.response
            })
        }
    } catch(error){
        res.status(500).json({
            status: "failed",
            code: emailRes.responseCode,
            message: emailRes.response
        })
    }
}
  