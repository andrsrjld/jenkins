import React, { useState } from 'react';
import BaseLayout from '~/components/BaseLayout';
import { getProductType } from '~/data/api/product-type';
import fetch from 'node-fetch';

import { getCart } from '~/data/api/cart';

// Toastr Notification
import {successNotification, errorNotification} from '~/util/toastr-notif';

const ContactUs = ({productType, token, cart}) => {
  const [form, setForm] = useState({
    name: '',
    email: '',
    message: ''
  });

  const handleChangeForm = (name) => (e) => {
    const { value } = e.target;
    setForm({ ...form, [name]: value });
  }

  const submitForm = async (e) => {
    e.preventDefault();
    const prom = await fetch(`/api/contact`, {
      method: 'POST',
      body : JSON.stringify(form),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      }
    })
    console.log(prom);
    if(prom.code === 200){
      successNotification(prom.message);
    } else {
      errorNotification(prom.message);
    }
  }

  return (
    <div>
      <BaseLayout productType={productType} token={token} cart={cart}>
        <div id="breadcumb" className="contact-wrapper">
          <div className="breadcumb">
              Home &#x0226B;
            {' '}
            <span style={{ color: '#010101' }}>Hubungi Kami</span>
          </div>

          <div className="c flex flex-wrap">
            <div className="w-1/2 xs:w-full sm:w-full">
              <div className="c-information">
                <div className="c-information-title">
                  HUBUNGI
                  {' '}
                  <span style={{ color: '#010101' }}>KAMI</span>
                </div>
                <div className="c-title-company">
                  BAPERA
                </div>
                <div className="text-information">
                  Anda bisa menghubungi kami melalui kontak berikut ini:
                </div>
                <div className="c-contact" style={{ marginTop: '48px' }}>
                  <img src="/images/ic_mail.png" alt="" />
                  hello@bapera.id
                </div>
                {/* <div className="c-contact" style={{ marginTop: '12px' }}>
                  <img src="/images/ic_wa_filled.png" alt="" />
                  Whatsapp : +62 822 9998 5858
                </div> */}
                {/* <div className="c-contact" style={{ marginTop: '12px' }}>
                  <img src="/images/ic_ig_filled.png" alt="" />
                  Instagram : @LAKKON.ID
                </div> */}
              </div>
            </div>
            <div className="w-1/2 xs:w-full sm:w-full">
              <div className="c-form">
                <form onSubmit={submitForm}>
                  <div>
                    <input className="form-control w-100" placeholder="Nama" value={form.name} onChange={handleChangeForm("name")} />
                  </div>
                  <div style={{ marginTop: '16px' }}>
                    <input className="form-control w-100" placeholder="Email" value={form.email} onChange={handleChangeForm("email")} />
                  </div>
                  <div style={{ marginTop: '16px' }}>
                    <textarea className="form-control w-100" placeholder="Pesan" rows="4" value={form.message} onChange={handleChangeForm("message")} />
                  </div>
                  <div style={{ marginTop: '16px' }}>
                    <button className="btn-primary" type="submit">KIRIM</button>
                  </div>
                </form>
              </div>
            </div>
          </div>

        </div>
      </BaseLayout>
      <style jsx>
        {
          `
          .breadcumb {
            font-size: 14px;
            color: #696969;
          }
          .contact-wrapper{
            padding: 20px 0;
          }
          .c {
            width: 100%;
            display: flex;
            padding: 60px 0;
          }

          .c-information {
            width: 100%;
          }

          .c-information-title {
            font-weight: bold;
            font-size: 20px;
            line-height: 23px;
            letter-spacing: 0.05em;
            color: #696969;
          }

          .c-title-company {
            font-weight: 500;
            font-size: 18px;
            line-height: 26px;
            letter-spacing: 0.05em;
            color: #696969;
            margin-top: 20px;
          }

          .text-information {
            font-size: 18px;
            line-height: 26px;
            letter-spacing: 0.05em;
            color: #868686;
            margin-top: 20px;
            width: 100%;
          }

          .c-contact {
            display: flex;
            align-items: center;
            font-weight: 500;
            font-size: 16px;
            line-height: 26px;
            letter-spacing: 0.05em;
            color: #696969;
          }

          .c-contact img {
            width: 20px;
            height: 20px;
            margin-right: 26px;
          }
          
          .c-form {
            width: 100%;
          }
          @media (min-width: 320px) and (max-width: 767px) {
            .contact-wrapper{
              padding: 0 20px;
              margin-bottom: 30px;
            }
            .c-information{
              margin-bottom:50px;
            }
            .text-information{
              margin-top:20px;
            }
          }
          `
        }
      </style>
    </div>
  );
}

ContactUs.getInitialProps = async (ctx) => {
  const { token } = ctx.store.getState().authentication;

  const productTypeResp = await getProductType();
  const productType = productTypeResp.data;

  const cartResp = await getCart(token);
  const cart = cartResp.data ? cartResp.data.cart : [];

  return { 
    productType,
    token,
    cart
  };
};

export default ContactUs;
