import React, { useState } from 'react';
import PropTypes from 'prop-types';
import InfiniteScroll from 'react-infinite-scroll-component';
import BaseLayout from '~/components/BaseLayout';
import CardArticle from '~/components/CardArticle';
import { getArticles } from '~/data/api/article';
import { getProductType } from '~/data/api/product-type';
import { getCart } from '~/data/api/cart';

// Axios API Request
import axiosSetup from '../../services/api';

const Category = ({
  slug, articles, productType, token, cart
}) => {

  const [sort, setSort] = useState('desc');
  const [sortBy, setSortBy] = useState('created_at')
  const [limit, setLimit] = useState(12);
  const [page, setPage] = useState(1);
  const [listArticle, setListArticle] = useState(articles);
  const [loadMore, setLoadMore] = useState(true);

  const handleSort = (event) => {
    setSort(event.currentTarget.attributes.sortType.value);
    setSortBy(event.currentTarget.attributes.sortBy.value);
    refreshArticle(sort, event.currentTarget.attributes.sortBy.value);
    setLoadMore(true);
  }

  const hanldeLoadMoreArticle = () => {
    setTimeout(async () => {
      const articles = await getArticles(token, sortBy, sort, page + 1, limit);
      if(articles.data){
        setListArticle(listArticle.concat(articles.data))
      } else {
        setLoadMore(false);
      }
    }, 500);
    setPage(page + 1);
  }

  const refreshArticle = (sort, sortBy) => {
    const api = axiosSetup(token);
    const productParam = `layout_type=list_layout&status=all&sort_by=${sortBy}&sort_type=${sort}&page=1&limit=${limit}`;
    api.get('/article?' + productParam).then((response) => {
      setListArticle(response.data.data);
    });
  }
  
  return (
    <div>
      <BaseLayout productType={productType} token={token} cart={cart}>
        <div id="breadcumb" className="breadcrumb-wrapper">
          <div className="breadcumb">
            Home &#x0226B;
            {' '}
            <span style={{ color: '#010101' }}>Baca - Baca</span>
          </div>
        </div>

        <div className="c-content flex flex-wrap">
          <div className="w-1/6 xs:w-full sm:w-full">
            <div className="c-filter">
              <div style={{
                display: 'flex', justifyContent: 'space-between', alignItems: 'center', margin: '8px 0px'
              }}
              onClick={handleSort} sortType="asc" sortBy="created_at"
              >
                <span className="title-filter" style={sort === "asc" && sortBy === "created_at" ? {color: "#010101"} : {}}>Terlama</span>
                <img src={sort === "asc" && sortBy === "created_at" ? "/images/ic_arrow_right_red.png" : "/images/ic_arrow_right.png"} alt="" />
              </div>
              <div className="divider-filter" />
              <div style={{
                display: 'flex', justifyContent: 'space-between', alignItems: 'center', margin: '8px 0px'
              }}
              onClick={handleSort} sortType="desc" sortBy="created_at"
              >
                <span className="title-filter" style={sort === "desc" && sortBy === "created_at" ? {color: "#010101"} : {}}>Terbaru</span>
                <img src={sort === "desc" && sortBy === "created_at" ? "/images/ic_arrow_right_red.png" : "/images/ic_arrow_right.png"} alt="" />
              </div>
              <div className="divider-filter" />
              <div style={{
                display: 'flex', justifyContent: 'space-between', alignItems: 'center', margin: '8px 0px'
              }}
              onClick={handleSort} sortType="desc" sortBy="view_count"
              >
                <span className="title-filter" style={sort === "desc" && sortBy === "view_count" ? {color: "#010101"} : {}}>Terpopuler</span>
                <img src={sort === "desc" && sortBy === "view_count" ? "/images/ic_arrow_right_red.png" : "/images/ic_arrow_right.png"} alt="" />
              </div>
              <div className="divider-filter" />
              <div style={{
                display: 'flex', justifyContent: 'space-between', alignItems: 'center', margin: '8px 0px'
              }}
              onClick={handleSort} sortType="asc" sortBy="title"
              >
                <span className="title-filter" style={sort === "asc" && sortBy === "title" ? {color: "#010101"} : {}}>Judul</span>
                <img src={sort === "asc" && sortBy === "title" ? "/images/ic_arrow_right_red.png" : "/images/ic_arrow_right.png"} alt="" />
              </div>
              <div className="divider-filter" />
            </div>
          </div>
          <div className="w-5/6 xs:w-full sm:w-full">
            <div className="c-products">

              <div className="c-products-title">
                Artikel
                {' '}
                <span style={{ color: '#010101' }}>Terkini</span>
              </div>

              <div className="c-content-products">
                <InfiniteScroll
                    dataLength={listArticle ? listArticle.length : 0}
                    next={hanldeLoadMoreArticle}
                    hasMore={loadMore}
                    className="flex flex-wrap w-full"
                >
                  {
                    listArticle ? listArticle.map((v) => (
                      <CardArticle data={v} />
                    )) : <h1>Belum ada artikel</h1>
                  }
                </InfiniteScroll>
              </div>
            </div>
          </div>
        </div>
      </BaseLayout>
      <style jsx>
        {`
        .breadcrumb-wrapper{
          padding:20px 0;
        }
        .breadcumb {
          font-size: 14px;
          color: #696969;
        }
      
        .c-content {
          width: 100%;
          display: flex;
          padding:0;
        }
      
        .c-filter {
          width: 100%s;
          display: flex;
          flex-wrap: wrap;
          justify-content: flex-start;
          flex-direction: column;
          padding-top: 48px;
        }
      
        .title-filter {
          font-weight: 500;
          font-size: 18px;
          color: #696969;
        }
      
        .c-products {
          width: 100%;
          padding: 0px 0px 0px 24px;
        }
      
        .c-filter-product {
          width: 100%;
          display: flex;
          justify-content: space-between;
        }
      
        .c-filter-page {
          font-size: 16px;
          color: #696969;
        }
      
        .form-filter-page {
          background: rgba(248, 248, 248, 0.75);
          border: 1px solid #F2F2F2;
          box-sizing: border-box;
          padding: 4px 4px;
          font-size: 16px;
          color: #696969;
          margin: 0px 4px;
          font-family: 'Gotham';
        }
      
        .c-content-products {
          display: flex;
          flex-wrap: wrap;
          justify-content: space-between;
        }
        .divider-filter {
          height: 1px;
          margin: 8px 0px;
          background: #F2F1F1;
        }

        .c-products-title {
          font-weight: bold;
          font-size: 22px;
          letter-spacing: 0.05em;
          color: #696969;
          margin-bottom: 12px;
        }
        .c-filter > div{
          cursor:pointer;
        }
        @media (min-width: 320px) and (max-width: 767px) {
          .c-content{
            padding: 0 20px;
            margin-bottom: 30px;
          }
          .container{
            max-width:100% !important;
          }
          .breadcrumb-wrapper{
            padding:0 20px;
            margin-bottom:20px;
            text-align:center;
          }
          .c-filter{
            padding-top:0;
          }
        }
        `}
      </style>
    </div>
  );
};

Category.propTypes = {
  slug: PropTypes.string
};

Category.getInitialProps = async (ctx) => {
  const { slug } = ctx.query;

  const { token } = ctx.store.getState().authentication;

  const productTypeResp = await getProductType();
  const productType = productTypeResp.data;
  
  const articlesResp = await getArticles(token, 'created_at', 'desc', '1', '12');
  const articles = articlesResp.data;

  const cartResp = await getCart(token);
  const cart = cartResp.data ? cartResp.data.cart : [];

  return { 
    slug,
    articles,
    productType,
    token,
    cart
  };
};

export default Category;
