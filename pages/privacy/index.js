import React, { useEffect } from 'react';
import BaseLayout from '~/components/BaseLayout';
import { getProductType } from '~/data/api/product-type';
import { getCart } from '~/data/api/cart';

const About = ({productType, token, cart}) => {
  useEffect(() => {
    const acc = document.getElementsByClassName('c-accordion');
    let i;

    for (i = 0; i < acc.length; i++) {
      acc[i].addEventListener('click', function () {
        this.classList.toggle('active');
        const panel = this.nextElementSibling;
        if (panel.style.display === 'flex') {
          panel.style.display = 'none';
        } else {
          panel.style.display = 'flex';
        }
      });
    }
  }, []);

  return (
    <div>
      <BaseLayout productType={productType} token={token} cart={cart}>
        <div id="breadcumb" className="breadcrumb-wrapper">
          <div className="breadcumb">
            Beranda &#x0226B;
            {' '}
            <span style={{ color: '#010101' }}>Kebijakan Privasi</span>
          </div>
        </div>
        <div className="c">
          <div className="title">
          Kebijakan
            {' '}
            <span style={{ color: '#010101' }}>Privasi</span>
          </div>

          <div className="desc" style={{ marginTop: '24px' }}>
          Adanya Kebijakan Privasi ini adalah komitmen nyata dari BAPERA untuk menghargai dan melindungi setiap data atau informasi pribadi Pengguna situs www.bapera.id, situs-situs turunannya, serta aplikasi gawai BAPERA (selanjutnya disebut sebagai "Situs").
          </div>

          <div className="desc" style={{ marginTop: '24px' }}>
          Kebijakan Privasi ini (beserta syarat-syarat penggunaan dari situs BAPERA sebagaimana tercantum dalam "Syarat & Ketentuan" dan informasi lain yang tercantum di Situs) menetapkan dasar atas perolehan, pengumpulan, pengolahan, penganalisisan, penampilan, pembukaan, dan/atau segala bentuk pengelolaan yang terkait dengan data atau informasi yang Pengguna berikan kepada BAPERA atau yang BAPERA kumpulkan dari Pengguna, termasuk data pribadi Pengguna, baik pada saat Pengguna melakukan pendaftaran di Situs, mengakses Situs, maupun mempergunakan layanan-layanan pada Situs (selanjutnya disebut sebagai "data").
          </div>

          <div className="desc" style={{ marginTop: '24px' }}>
           Dengan mengakses dan/atau mempergunakan layanan BAPERA, Pengguna menyatakan bahwa setiap data Pengguna merupakan data yang benar dan sah, serta Pengguna memberikan persetujuan kepada BAPERA untuk memperoleh, mengumpulkan, menyimpan, mengelola dan mempergunakan data tersebut sebagaimana tercantum dalam Kebijakan Privasi maupun Syarat dan Ketentuan BAPERA.
          </div>

          <div className="desc" style={{ marginTop: '24px' }}>
          Kebijakan Privasi ini adalah merupakan bagian dan satu kesatuan dari “Syarat & Ketentuan” yang tercantum di Situs. Dengan menyetujui “Syarat & Ketentuan” maka Pengguna dianggap turut telah membaca, memahami dan menyetujui seluruh ketentuan yang tercantum dalam Kebijakan Privasi ini.
          </div>

          <div className="c-content" style={{ marginTop: '24px' }}>

            <div className="c-accordion" tabIndex="0" role="button">
              <div className="w-100 c-accordion-header">
                <div>Perolehan dan Pengumpulan Data Pengguna</div>
                <div>
                  <img src="/images/ic_arrow_down.png" alt="" />
                </div>
              </div>
              <div className="accordion-divider" />
            </div>
            <div className="c-accordion-desc">
              <span>
                Content
              </span>
              <div className="accordion-divider" />
            </div>

            <div className="c-accordion" tabIndex="0" role="button">
              <div className="w-100 c-accordion-header">
                <div>Penggunaan Data</div>
                <div>
                  <img src="/images/ic_arrow_down.png" alt="" />
                </div>
              </div>
              <div className="accordion-divider" />
            </div>
            <div className="c-accordion-desc">
              <span>
                Content
              </span>
              <div className="accordion-divider" />
            </div>

            <div className="c-accordion" tabIndex="0" role="button">
              <div className="w-100 c-accordion-header">
                <div>Pengungkapan Data Pribadi Pengguna</div>
                <div>
                  <img src="/images/ic_arrow_down.png" alt="" />
                </div>
              </div>
              <div className="accordion-divider" />
            </div>
            <div className="c-accordion-desc">
              <span>
                Content
              </span>
              <div className="accordion-divider" />
            </div>

            <div className="c-accordion" tabIndex="0" role="button">
              <div className="w-100 c-accordion-header">
                <div>Cookies</div>
                <div>
                  <img src="/images/ic_arrow_down.png" alt="" />
                </div>
              </div>
              <div className="accordion-divider" />
            </div>
            <div className="c-accordion-desc">
              <span>
                Content
              </span>
              <div className="accordion-divider" />
            </div>

            <div className="c-accordion" tabIndex="0" role="button">
              <div className="w-100 c-accordion-header">
                <div>Pilihan Pengguna dan Transparansi</div>
                <div>
                  <img src="/images/ic_arrow_down.png" alt="" />
                </div>
              </div>
              <div className="accordion-divider" />
            </div>
            <div className="c-accordion-desc">
              <span>
                Content
              </span>
              <div className="accordion-divider" />
            </div>

            <div className="c-accordion" tabIndex="0" role="button">
              <div className="w-100 c-accordion-header">
                <div>Penyimpanan dan Penghapusan Informasi</div>
                <div>
                  <img src="/images/ic_arrow_down.png" alt="" />
                </div>
              </div>
              <div className="accordion-divider" />
            </div>
            <div className="c-accordion-desc">
              <span>
                Content
              </span>
              <div className="accordion-divider" />
            </div>

          </div>
        </div>
      </BaseLayout>
      <style jsx>
        {`
        .breadcrumb-wrapper{
          padding: 20px 0;
        }
        .c {
          padding: 20px 0 80px 0;
        }

        .c-content {
          background: #FFFFFF;
          box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.1);
          border-radius: 5px;
          font-weight: 500;
          font-size: 18px;
          line-height: 26px;
          letter-spacing: 0.05em;
          color: #696969;
        }

        .c-accordion {
          display: flex;
          justify-content: space-between;
          align-items: center;
          cursor: pointer;
          flex-wrap: wrap;
        }

        .c-accordion-header {
          display: flex;
          justify-content: space-between;
          margin: 25px 48px;
          align-items: center;
        }

        .c-accordion-desc {
          display: none;
          font-weight: normal;
          font-size: 22px;
          line-height: 32px;
          letter-spacing: 0.05em;
          color: #696969;
          width: 100%;
          flex-wrap: wrap;
        }

        .c-accordion-desc span {
          margin: 25px 48px;
        }

        .accordion-divider {
          width: 100%;
          height: 2px;
          background: #E7E7E7;
        }

        .title {
          font-weight: bold;
          font-size: 20px;
          line-height: 23px;
          letter-spacing: 0.05em;
          color: #696969;
        }

        .desc {
          font-size: 16px;
          line-height: 28px;
          letter-spacing: 0.05em;
          color: #696969;
        }

        .card {
          background: #FFFFFF;
          box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.1);
          border-radius: 5px;
        }
        @media (min-width: 320px) and (max-width: 767px) {
          .c{
            padding: 0 20px;
            margin-bottom: 30px;
          }
          .breadcrumb-wrapper{
            padding: 0 20px;
            margin-bottom:20px;
            text-align:center;
          }
          .title{
            text-align:center;
          }
        }
        `
        }
      </style>
    </div>
  );
};

About.getInitialProps = async (ctx) => {
  const { token } = ctx.store.getState().authentication;

  const productTypeResp = await getProductType();
  const productType = productTypeResp.data;

  const cartResp = await getCart(token);
  const cart = cartResp.data ? cartResp.data.cart : [];

  return { 
    productType,
    token
  };
};

export default About;
