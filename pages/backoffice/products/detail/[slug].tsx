import {useEffect, useState} from 'react'
import {connect} from 'react-redux'
import BackOfficeBase from 'components/BackOfficeBase'
import CropPicker from 'components/CropPicker'
import Modal from 'components/Modal'
import { getCategories, getOrigins, getSpecies, getProcessing, getTasted, getRoastLevels, getRequests, getTeaTypes, getChocolateTypes, getSizes } from 'data/api'
import { uploadImage } from 'data/api/image'
import { postProduct, getOne, TUpdateProduct, updateProduct, updateProductVariant, addProductVariant, updateProductInformation, deleteProductImage, insertProductImage } from 'data/api/product'
import Loading from 'components/Loading'
import moment from 'moment'
import {formatNumber, formatCurrencyToNumber, compressImage} from 'util/helper'
import { promises } from 'fs'

type TProp = {
  product: any,
  requests: Array<any>,
  categories: Array<any>, 
  origins: Array<any>,
  species: Array<any>,
  processing: Array<any>,
  tasted: Array<any>,
  roastLevels: Array<any>,
  teaTypes: Array<any>,
  chocolateTypes: Array<any>,
  sizes: Array<any>,
  cloudinaryUrl: string,
  apiUrl: string,
  token: string
}

const STATEFORM = [
  {
    id: '1',
    name: 'Green Bean',
    form: ['enable', 'name', 'sku', 'price', 'discount', 'qty', 'weight', 'origins', 'species', 'processing', 'description', 'images', 'po', 'estimated']
  },
  {
    id: '2',
    name: 'Roasted Bean',
    form: ['enable', 'name', 'sku', 'price', 'discount', 'qty', 'weight', 'origins', 'species', 'processing', 'tasted', 'roastLevel', 'roastDate', 'description', 'images', 'request', 'po', 'estimated']
  },
  {
    id: '3',
    name: 'Alat Seduh Manual',
    form: ['enable', 'name', 'sku', 'price', 'discount', 'qty', 'weight', 'description', 'images', 'po', 'estimated']
  },
  {
    id: '4',
    name: 'Alat Seduh Elektrik',
    form: ['enable', 'name', 'sku', 'price', 'discount', 'qty', 'weight', 'description', 'images', 'po', 'estimated']
  },
  {
    id: '5',
    name: 'Tea',
    form: ['enable', 'name', 'sku', 'price', 'discount', 'qty', 'weight', 'teaTypes','description', 'images', 'po', 'estimated']
  },
  {
    id: '6',
    name: 'Cokelat',
    form: ['enable', 'name', 'sku', 'price', 'discount', 'qty', 'weight', 'chocolateTypes', 'origins', 'processing', 'tasted', 'roastLevel','description', 'images', 'po', 'estimated']
  },
  {
    id: '7',
    name: 'Merchandise',
    form: ['enable', 'name', 'sku', 'price', 'discount', 'qty', 'weight', 'sizes', 'description', 'images', 'po', 'estimated']
  }
]

const dashboard = ({product, requests, categories, origins, species, processing, tasted, roastLevels, cloudinaryUrl, apiUrl, token, chocolateTypes, teaTypes, sizes} : TProp) => {
  const [loading, setLoading] = useState(false)
  const [requestOptions, setRequestOptions] = useState(requests.map(v=>({...v, checked: product.information ? product.information.filter(k=>k.value_id===v.id).length > 0 : false})))
  const [tastedOptions, setTastedOptions] = useState(tasted.map(v=>({...v, checked: product.information ? product.information.filter(k=>k.value_id===v.id).length > 0 : false })))
  const [selectedCategory, setSelectedCategory] = useState(product.product_type_id+'')
  const [enabled, setEnabled] = useState(true);
  const [name, setName] = useState(product.name)
  const [sku, setSku] = useState(product.variants ? product.variants[0].sku : "")
  const [price, setPrice] = useState(product.variants ? product.variants[0].price : '')
  const [discount, setDiscount] = useState({
    start: product.variants ? product.variants[0].discount_from == '0001-01-01' ? null : product.variants[0].discount_from : '',
    end: product.variants ? product.variants[0].discount_to  == '0001-01-01' ? null : product.variants[0].discount_to : "",
    discount: product.variants ? product.variants[0].discount_amount : "",
  })
  const [qty, setQty] = useState(product.variants ? product.variants[0].quantity : '')
  const [weight, setWeight] = useState({
    netWeight: product.variants ? product.variants[0].net_weight : '',
    grossWeight: product.variants ? product.variants[0].gross_weight : ''
  })
  const [selectedOrigin, setSelectedOrigin] = useState(product.information ? product.information.find(v=>v.name == 'Origin')?.value_id : '')
  const [selectedSpecies, setSelectedSpecies] = useState(product.information ? product.information.find(v=>v.name == 'Bean Species')?.value_id : '')
  const [selectedProcessing, setSelectedProcessing] = useState(product.information ? product.information.find(v=>v.name == 'Processing')?.value_id : '')
  const [selectedRoastLevel, setSelectedRoastLevel] = useState(product.information ? product.information.find(v=>v.name == 'Roast Level')?.value_id : '')
  const [rostDate, setRoastDate] = useState({
    date: product.information ? product.information.find(v=>v.name == 'Roast Date')?.value : '',
    fresh: false,
    desc: product.information ? product.information.find(v=>v.name == 'Roast Day')?.value : ''
  })
  const [shortDesc, setShortDesc] = useState(product.short_description)
  const [desc, setDesc] = useState(product.description)
  const [images, setImages] = useState(product.images ? product.images?.map(v => ({...v,base64: v.image_url, file: null})) : [])
  const [selectedTeaType, setSelectedTeaType] = useState(product.information ? product.information.find(v=>v.name == 'Tea Types')?.value_id : '')
  const [selectedChocolateType, setSelectedChocolateType] = useState(product.information ? product.information.find(v=>v.name == 'Chocolate Species')?.value_id : '')
  const [selectedSize, setSelectedSize] = useState(product.information ? product.information.find(v=>v.name == 'Size')?.value_id : '')
  const [variants, setVariants] = useState(product.variants ? product.variants.slice(1,product.variants.length).map(v => ({
    id: v.id,
    product_id: v.product_id,
    sku: v.sku,
    name: v.name,
    price: v.price,
    quantity: v.quantity,
    netWeight: v.net_weight,
    grossWeight: v.gross_weight,
    discountValue: v.discount_amount,
    discountStart: v.discount_from == '0001-01-01' ? null : v.discount_from,
    discountEnd: v.discount_to == '0001-01-01' ? null : v.discount_to,
    details: v.details,
    selectedSize: selectedCategory == '7' ? sizes.find(k => k.value == v.details[0].attribute_value).id : ''
  })) : [])

  
  const [po, setPO] = useState(product.is_po)
  const [estimated, setEstimated] = useState(product.po_duration)

  const [modalCrop, setModalCrop] = useState({
    image: '',
    open: false,
    cb: (props: any) => {}
  })

  const handleSaveProduct = async () => {
    setLoading(true)
    const bodyParent : TUpdateProduct = {
      id: product.id,
      name: name,
      price: Number(price),
      description: desc,
      short_description: shortDesc,
      discount_from: discount.start ? moment(discount.start).format('YYYY-MM-DD') : '',
      discount_to: discount.end ? moment(discount.end).format('YYYY-MM-DD') : '',
      discount_amount: Number(discount.discount),
      is_po: po,
      po_duration: Number(estimated),
    }
    // Update Product
    const resProduct = await updateProduct(apiUrl, bodyParent, token)

    // Update Variant
    const promisesVariant = [
      updateProductVariant(apiUrl, {
        id: product.variants[0].id,
        product_id: product.variants[0].product_id,
        discount_amount: Number(discount.discount),
        discount_from: discount.start ? moment(discount.start).format('YYYY-MM-DD') : '',
        discount_to: discount.end ? moment(discount.end).format('YYYY-MM-DD') : '',
        gross_weight: Number(weight.grossWeight),
        name: weight.netWeight + '',
        net_weight:Number(weight.netWeight),
        price: Number(price),
        quantity: Number(qty),
        sku: sku,
        details: selectedCategory == '7' ? [{
          ...product.variants[0].details[0],
          attribute_name: "size",
          attribute_value: sizes.find(v => v.id == selectedSize) ? sizes.find(v => v.id == selectedSize).value : ""
        }] : []
      }, token)
    ]
    variants.forEach(v => {
      if(v.id) {
        promisesVariant.push(updateProductVariant(apiUrl, {
          id: v.id,
          product_id: v.product_id,
          discount_amount: Number(v.discountValue),
          discount_from: v.discountStart ? moment(v.discountStart).format('YYYY-MM-DD') : '',
          discount_to: v.discountEnd ? moment(v.discountEnd).format('YYYY-MM-DD') : '',
          gross_weight: Number(v.grossWeight),
          name: v.netWeight + '',
          net_weight:Number(v.netWeight),
          price: Number(v.price),
          quantity: Number(v.quantity),
          sku: v.sku,
          details: selectedCategory == '7' ? [{
            ...v.details[0],
            attribute_name: "size",
            attribute_value: sizes.find(k => k.id == v.selectedSize) ? sizes.find(k => k.id == v.selectedSize).value : ""
          }] : []
        }, token))
      } else {
        promisesVariant.push(addProductVariant(apiUrl, {
          product_id: product.id,
          discount_amount: Number(v.discountValue),
          discount_from: v.discountStart ? moment(v.discountStart).format('YYYY-MM-DD') : '',
          discount_to: v.discountEnd ? moment(v.discountEnd).format('YYYY-MM-DD') : '',
          gross_weight: Number(v.grossWeight),
          name: v.netWeight + '',
          net_weight:Number(v.netWeight),
          price: Number(v.price),
          quantity: Number(v.quantity),
          sku: v.sku,
          details: selectedCategory == '7' ? [{
            attribute_name: "size",
            attribute_value: sizes.find(k => k.id == v.selectedSize) ? sizes.find(k => k.id == v.selectedSize).value : ""
          }] : []
        }, token))
      }
    });

    // Update Information
    const bodyInformation = {
      product_id: product.id,
      information: []
    }
    const selectedType = STATEFORM.find(v => v.id === selectedCategory);
    if(selectedType?.form.includes('origins')) {
      const origin = origins.find((v) => v.id == selectedOrigin);
      bodyInformation.information.push({
        id: product.information.find(v=>v.name == 'Origin')?.id,
        value_id: origin?.id,
        name: 'Origin',
        value: origin?.value
      })
    }
    if(selectedType?.form.includes('species')) {
      const spec = species.find(v => v.id == selectedSpecies)
      bodyInformation.information.push({
        id: product.information.find(v=>v.name == 'Bean Species')?.id,
        value_id: spec?.id,
        name: 'Bean Species',
        value: spec?.value
      })
    }
    if(selectedType?.form.includes('processing')) {
      const process = processing.find(v => v.id == selectedProcessing);
      bodyInformation.information.push({
        id: product.information.find(v=>v.name == 'Processing')?.id,
        value_id: process?.id,
        name: 'Processing',
        value: process?.value
      })
    }
    if(selectedType?.form.includes('request')) {
      requestOptions.filter(v => v.checked).forEach(v => {
        bodyInformation.information.push({
          id: product.information.find(k=>k.name == 'Request' && v.id == k.value_id)?.id,
          value_id: v.id,
          name: 'Request',
          value: v.value
        })
      })
    }
    if(selectedType?.form.includes('tasted')) {
      tastedOptions.filter(v => v.checked).forEach(v => {
        bodyInformation.information.push({
          id: product.information.find(k=>k.name == 'Tasted' && v.id == k.value_id)?.id,
          value_id: v.id,
          name: 'Tasted',
          value: v.value
        })
      })
    }
    if(selectedType?.form.includes('roastLevel')) {
      const roastLevel = roastLevels.find(v => v.id == selectedRoastLevel)
      bodyInformation.information.push({
        id: product.information.find(v=>v.name == 'Roast Level')?.id,
        value_id: roastLevel?.id,
        name: 'Roast Level',
        value: roastLevel?.value
      })
    }
    if(selectedType?.form.includes('roastDate')) {
      bodyInformation.information.push({
        id: product.information.find(v=>v.name == 'Roast Date')?.id,
        name: 'Roast Date',
        value: rostDate.date ? moment(rostDate.date).format("YYYY-MM-DD") : '',
      })
      bodyInformation.information.push({
        id: product.information.find(v=>v.name == 'Roast Day')?.id,
        name: 'Roast Day',
        value: rostDate.desc,
      })
    }
    if(selectedType?.form.includes('teaTypes')) {
      const teaType = teaTypes.find(v => v.id == selectedTeaType)
      bodyInformation.information.push({
        id: product.information.find(v=>v.name == 'Tea Types')?.id,
        value_id: teaType?.id,
        name: 'Tea Types',
        value: teaType?.value
      })
    }
    if(selectedType?.form.includes('chocolateTypes')) {
      const chocoType = chocolateTypes.find(v => v.id == selectedChocolateType)
      bodyInformation.information.push({
        id: product.information.find(v=>v.name == 'Chocolate Species')?.id,
        value_id: chocoType?.id,
        name: 'Chocolate Species',
        value: chocoType?.value
      })
    }
    if(selectedType?.form.includes('sizes')) {
      if(selectedSize != '') {
        const size = sizes.find(v => v.id == selectedSize)
        bodyInformation.information.push({
          id: product.information.find(v=>v.name == 'Size')?.id,
          value_id: size?.id,
          name: 'Size',
          value: size?.value
        })
      }
    }
    const resInformation = await updateProductInformation(apiUrl, bodyInformation, token)


    if(resProduct.code == 200) window.location.href = '/backoffice/products'
  }

  const handleInsertImage = async (e) => {
    // const file = e.target.files[0]
    const file = await compressImage(e.target.files[0], 1)
    getBase64(file, (result) => {
      // setImages([...images, {base64: result, file: file}])
      setModalCrop({
        image: result,
        open: true,
        cb: (props : any) => setImages([...images, {base64: props.croppedImage, file: props.blobCompressed}]),
      })
    });
  }

  const getBase64 = (file, cb) =>{
    let reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = function () {
        cb(reader.result)
    }
    reader.onerror = function (error) {
        console.log('Error: ', error)
    }
  }

  const handleSelectedRequest = (id) => {
    setRequestOptions(requestOptions.map(v => ({...v, checked: v.id === id ? !v.checked : v.checked})))
  }

  const handleChangeTasted = (id) => {
    setTastedOptions(tastedOptions.map(v => ({...v, checked: v.id === id ? !v.checked : v.checked})))
  }
  
  const handleAddVariant = () => {
    setVariants([...variants, {
      sku: '',
      price: '',
      quantity: '',
      netWeight: '',
      grossWeight: '',
      discountStart: '',
      discountEnd: '',
      discountValue :'',
      selectedSize: ''
    }])
  }

  const handleDeleteVariant = (index) => {
    const temp = [...variants]
    temp.splice(index, 1)
    setVariants(temp)
  }

  const handleSetVariant = (name, value, index) => {
    let val = value
    if(name == 'price' || name == 'discountValue') {
      val = formatCurrencyToNumber(value)
    }
    setVariants(variants.map((v, i) => ({
      ...v,
      [name]: i===index ? val : v[name]
    })))
  }

  const handleChangePrice = (e) => {
    const {value} = e.target
    const val = formatCurrencyToNumber(value)
    setPrice(val)
  }

  const handleChangePriceDiscount = (e) => {
    const {value} = e.target
    const val = formatCurrencyToNumber(value)
    setDiscount({...discount, discount: val})
  }

  const handleCroppedImage = async (croppedImage, cb) => {
    setLoading(true)
    try {
      console.log(croppedImage)
      let blobCompressed = await fetch(croppedImage).then(r => r.blob());
      // const blobCompressed = await compressImage(blob)
      // Uploading Image
      const promiseImage = await uploadImage(cloudinaryUrl, {
        file: blobCompressed,
        folder: 'products',
        tag: ''
      })
      console.log(promiseImage)
      // Insert tImage
      const res = await insertProductImage(apiUrl, token, product.id, promiseImage.secure_url)
      modalCrop.cb({croppedImage, blobCompressed})
      setModalCrop({
        ...modalCrop,
        open: false
      })
    } catch(e) {
      console.log(e)
    }
    setLoading(false)
  }

  const handleDeleteImage = async (image) => {
    setLoading(true)
    if(image.id) {
      const res = await deleteProductImage(apiUrl, token, image.id)
    }
    setImages(images.filter(v => v != image))

    setLoading(false)
  }

  return (
    <BackOfficeBase>
      <Loading show={loading}/>
      <div className="w-full flex flex-wrap">
        <div className="w-full flex">
          <div className="w-full font-medium text-2xl text-text-primary">
            {product.name}
          </div>
          <div className="w-full">
            <div className="flex justify-end">
              <button className="bg-gray-500 text-text-primary px-5 py-1 mx-1 rounded">BACK</button>
              <button className="bg-green-500 text-white px-5 py-1 mx-1 rounded" onClick={handleSaveProduct}>SAVE PRODUCT</button>
            </div>
          </div>
        </div>
        <div className="w-full">
          <div className="bg-white shadow-lg py-12 mt-12 rounded-lg flex justify-center flex-wrap">
            <div className="w-1/2">

              <div className="flex">
                <label className="w-1/2 text-right text-text-primary">Category<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                    <div className="w-full flex">
                      <select className="w-full px-4 py-2 border border-gray-300 rounded-l" value={selectedCategory} onChange={(e) => setSelectedCategory(e.target.value)}>
                        <option value="" disabled selected>Select Category</option>
                        {
                          categories.map(({id, name}) => {
                            return (
                              <option key={id} value={id}>{name}</option>
                            )
                          })
                        }
                      </select>
                      <div className="">
                        <div className="rounded-r px-3 py-1 bg-gray-300 items-center flex h-full">
                          <img src="/images/ic_arrow_down.png" alt=""/>
                        </div>
                      </div>
                  </div>
                </div>
              </div>

              {/* {
                STATEFORM.find(v=>v.id === selectedCategory)?.form.includes('enable') && (
                  <div className="flex mt-8">
                    <label className="w-1/2 text-right text-text-primary">Enable Product</label>
                    <div className="w-full pl-5">
                      <input type="checkbox" checked={enabled} onChange={() => setEnabled(v => !v)}/>
                    </div>
                  </div>
                )
              } */}

              {
                STATEFORM.find(v=>v.id === selectedCategory)?.form.includes('name') && (
                  <div className="flex mt-8">
                    <label className="w-1/2 text-right text-text-primary">Product Name<span className="text-primary">*</span></label>
                    <div className="w-full pl-5">
                      <input className="border border-gray-300 px-4 py-2 w-full" type="text" value={name} onChange={(e)=>setName(e.target.value)}/>
                    </div>
                  </div>
                )
              }

              {
                STATEFORM.find(v=>v.id === selectedCategory)?.form.includes('sku') && (
                  <div className="flex mt-8">
                    <label className="w-1/2 text-right text-text-primary">SKU<span className="text-primary">*</span></label>
                    <div className="w-full pl-5">
                      <input type="text" className="w-full border border-gray-300 px-4 py-2" value={sku} onChange={(e) => setSku(e.target.value)} />
                    </div>
                  </div>
                )
              }

              {
                STATEFORM.find(v=>v.id === selectedCategory)?.form.includes('price') && (
                  <div className="flex mt-8">
                    <label className="w-1/2 text-right text-text-primary">Price<span className="text-primary">*</span></label>
                    <div className="w-full pl-5">
                      <input type="text" className="w-full border border-gray-300 px-4 py-2" placeholder="Rp" value={'Rp ' +  formatNumber(price)} onChange={handleChangePrice}/>
                    </div>
                  </div>
                )
              }

              {
                STATEFORM.find(v=>v.id === selectedCategory)?.form.includes('price') && (
                  <div className="flex mt-8">
                    <label className="w-1/2 text-right text-text-primary">Discount Price</label>
                    <div className="w-full pl-5 flex flex-wrap">
                      <div className="w-full flex items-center">
                        <input type="date" className="w-full border border-gray-300 px-4 py-2" placeholder="From" value={discount.start ? discount.start : null} onChange={(e) => setDiscount({...discount, start: e.target.value})}/>
                        <div className="m-3">To</div>
                        <input type="date" className="w-full border border-gray-300 px-4 py-2" placeholder="To" value={discount.end ? discount.end : null} onChange={(e) => setDiscount({...discount, end: e.target.value})}/>
                      </div>
                      <input type="text" className="w-full mt-3 border border-gray-300 px-4 py-2" placeholder="Rp" value={'Rp ' + formatNumber(discount.discount)} onChange={handleChangePriceDiscount}/>
                    </div>
                  </div>
                )
              }

              {
                STATEFORM.find(v=>v.id === selectedCategory)?.form.includes('qty') && (
                  <div className="flex mt-8">
                    <label className="w-1/2 text-right text-text-primary">Quantity<span className="text-primary">*</span></label>
                    <div className="w-full pl-5 flex">
                      <input type="text" className="w-full border border-gray-300 px-4 py-2" value={qty} onChange={(e) => setQty(e.target.value)} />
                      <div className="h-full flex items-center px-2 bg-gray-300 text-text-primary rounded-r">
                        PCS
                      </div>
                    </div>
                  </div>
                )
              }

              {
                STATEFORM.find(v=>v.id === selectedCategory)?.form.includes('weight') && (
                  <div className="flex mt-8">
                  <label className="w-1/2 text-right text-text-primary">Net Weight<span className="text-primary">*</span></label>

                  <div className="flex w-full">
                    <div className="w-full pl-5 flex">
                      <input type="text" className="w-full border border-gray-300 px-4 py-2" value={weight.netWeight} onChange={(e) => setWeight(({...weight, netWeight: e.target.value}))}/>
                      <div className="h-full flex items-center px-2 bg-gray-300 text-text-primary rounded-r">
                        GRAM
                      </div>
                    </div>
                    <label className="w-1/2 text-right text-text-primary">Gross Weight<span className="text-primary">*</span></label>
                    <div className="w-full pl-5 flex">
                      <input type="text" className="w-full border border-gray-300 px-4 py-2" value={weight.grossWeight} onChange={(e) => setWeight(({...weight, grossWeight: e.target.value}))}/>
                      <div className="h-full flex items-center px-2 bg-gray-300 text-text-primary rounded-r">
                        GRAM
                      </div>
                    </div>
                  </div>
                </div>
                )
              }
              
              {
                STATEFORM.find(v=>v.id === selectedCategory)?.form.includes('request') && (
                  <div className="flex mt-8">
                    <label className="w-1/2 text-right text-text-primary">Request<span className="text-primary">*</span></label>
                    <div className="w-full pl-5">
                      {
                        requestOptions.map(({id, value, checked}) => {
                          return (
                            <div key={id} >
                              <input className="border border-gray-300 px-4 py-2 mr-3" type="checkbox" value={id} checked={checked} onChange={() => handleSelectedRequest(id)} />{value}
                            </div>
                          )
                        })
                      }
                    </div>
                  </div>
                )
              }

              {
                STATEFORM.find(v=>v.id === selectedCategory)?.form.includes('teaTypes') && (
                  <div className="flex mt-8">
                    <label className="w-1/2 text-right text-text-primary">Tea Types<span className="text-primary">*</span></label>
                    <div className="w-full pl-5 flex">
                      <select className="w-full border border-gray-300 px-4 py-2" value={selectedTeaType} onChange={(e) => setSelectedTeaType(e.target.value)}>
                        <option value="" disabled selected>Select Tea Type</option>
                        {
                          teaTypes.map(({id, value}) => {
                            return (
                              <option key={id} value={id}>{value}</option>
                            )
                          })
                        }
                      </select>
                      <div className="">
                        <div className="rounded-r px-3 py-1 bg-gray-300 items-center flex h-full">
                          <img src="/images/ic_arrow_down.png" alt=""/>
                        </div>
                      </div>
                    </div>
                  </div>
                )
              }
              
              {
                STATEFORM.find(v=>v.id === selectedCategory)?.form.includes('chocolateTypes') && (
                  <div className="flex mt-8">
                    <label className="w-1/2 text-right text-text-primary">Chocolate Species<span className="text-primary">*</span></label>
                    <div className="w-full pl-5 flex">
                      <select className="w-full border border-gray-300 px-4 py-2" value={selectedChocolateType} onChange={(e) => setSelectedChocolateType(e.target.value)}>
                        <option value="" disabled selected>Select Chocolate Species</option>
                        {
                          chocolateTypes.map(({id, value}) => {
                            return (
                              <option key={id} value={id}>{value}</option>
                            )
                          })
                        }
                      </select>
                      <div className="">
                        <div className="rounded-r px-3 py-1 bg-gray-300 items-center flex h-full">
                          <img src="/images/ic_arrow_down.png" alt=""/>
                        </div>
                      </div>
                    </div>
                  </div>
                )
              }

              {
                STATEFORM.find(v=>v.id === selectedCategory)?.form.includes('sizes') && (
                  <div className="flex mt-8">
                    <label className="w-1/2 text-right text-text-primary">Size</label>
                    <div className="w-full pl-5 flex">
                      <select className="w-full border border-gray-300 px-4 py-2" value={selectedSize} onChange={(e) => setSelectedSize(e.target.value)}>
                        <option value="" disabled selected>Select Size</option>
                        {
                          sizes.map(({id, value}) => {
                            return (
                              <option key={id} value={id}>{value}</option>
                            )
                          })
                        }
                      </select>
                      <div className="">
                        <div className="rounded-r px-3 py-1 bg-gray-300 items-center flex h-full">
                          <img src="/images/ic_arrow_down.png" alt=""/>
                        </div>
                      </div>
                    </div>
                  </div>
                )
              }

              {
                STATEFORM.find(v=>v.id === selectedCategory)?.form.includes('origins') && (
                  <div className="flex mt-8">
                    <label className="w-1/2 text-right text-text-primary">Origins<span className="text-primary">*</span></label>
                    <div className="w-full pl-5 flex">
                      <select className="w-full border border-gray-300 px-4 py-2" value={selectedOrigin} onChange={(e) => setSelectedOrigin(e.target.value)}>
                        <option value="" disabled selected>Select Origin</option>
                        {
                          origins.map(({id, value}) => {
                            return (
                              <option key={id} value={id}>{value}</option>
                            )
                          })
                        }
                      </select>
                      <div className="">
                        <div className="rounded-r px-3 py-1 bg-gray-300 items-center flex h-full">
                          <img src="/images/ic_arrow_down.png" alt=""/>
                        </div>
                      </div>
                    </div>
                  </div>
                )
              }
              
              {
                STATEFORM.find(v=>v.id === selectedCategory)?.form.includes('species') && (
                  <div className="flex mt-8">
                  <label className="w-1/2 text-right text-text-primary">Species<span className="text-primary">*</span></label>
                  <div className="w-full pl-5 flex">
                    <select className="w-full border border-gray-300 px-4 py-2" value={selectedSpecies} onChange={(e) => setSelectedSpecies(e.target.value)}>
                      <option value="" disabled selected>Select Species</option>
                      {
                        species.map(({id,value}) => {
                          return (
                            <option key={id} value={id}>{value}</option>
                          )
                        })
                      }
                    </select>
                    <div className="">
                        <div className="rounded-r px-3 py-1 bg-gray-300 items-center flex h-full">
                          <img src="/images/ic_arrow_down.png" alt=""/>
                        </div>
                      </div>
                  </div>
                </div>
                )
              }

              {
                STATEFORM.find(v=>v.id === selectedCategory)?.form.includes('processing') && (
                  <div className="flex mt-8">
                  <label className="w-1/2 text-right text-text-primary">Processing<span className="text-primary">*</span></label>
                  <div className="w-full pl-5 flex">
                    <select className="w-full border border-gray-300 px-4 py-2" value={selectedProcessing} onChange={(e) => setSelectedProcessing(e.target.value)}>
                      <option value="" disabled selected>Select Processing</option>
                      {
                        processing.map(({id,value}) => {
                          return (
                            <option key={id} value={id}>{value}</option>
                          )
                        })
                      }
                    </select>
                      <div className="">
                        <div className="rounded-r px-3 py-1 bg-gray-300 items-center flex h-full">
                          <img src="/images/ic_arrow_down.png" alt=""/>
                        </div>
                      </div>
                  </div>
                </div>
                )
              }

              {
                STATEFORM.find(v=>v.id === selectedCategory)?.form.includes('tasted') && (
                  <div className="flex mt-8">
                    <label className="w-1/2 text-right text-text-primary">Tasted<span className="text-primary">*</span></label>
                    <div className="w-full pl-5">
                      {
                        tastedOptions.map(({id,value, checked}) => {
                          return (
                            <div key={id} >
                              <input className="mr-3" type="checkbox" value={id} checked={checked} onChange={() => handleChangeTasted(id)}/>{value}
                            </div>
                          )
                        })
                      }
                    </div>
                  </div>
                )
              }

              {
                STATEFORM.find(v=>v.id === selectedCategory)?.form.includes('roastLevel') && (
                <div className="flex mt-8">
                  <label className="w-1/2 text-right text-text-primary">Roast Level<span className="text-primary">*</span></label>
                  <div className="w-full pl-5 flex">
                    <select className="w-full border border-gray-300 px-4 py-2" value={selectedRoastLevel} onChange={(e) => setSelectedRoastLevel(e.target.value)}>
                      <option value="" disabled selected>Select Roast Level</option>
                      {
                        roastLevels.map(({id,value}) => {
                          return (
                            <option key={id} value={id}>{value}</option>
                          )
                        })
                      }
                    </select>
                      <div className="">
                        <div className="rounded-r px-3 py-1 bg-gray-300 items-center flex h-full">
                          <img src="/images/ic_arrow_down.png" alt=""/>
                        </div>
                      </div>
                  </div>
                </div>
                )
              }

              {
                STATEFORM.find(v=>v.id === selectedCategory)?.form.includes('roastDate') && (
                  <div className="flex mt-8">
                    <label className="w-1/2 text-right text-text-primary">Roast Date</label>
                    <div className="w-full pl-5">
                      <div className="w-full flex flex-wrap">
                        <div className="w-full">
                          <input type="date" className="border border-gray-300 px-4 py-2" value={rostDate.date} onChange={(e) => setRoastDate({...rostDate,date: e.target.value})}/>
                        </div>
                      </div>
                    </div>
                  </div>
                )
              }

{
                STATEFORM.find(v=>v.id === selectedCategory)?.form.includes('roastDate') && (
                  <div className="flex mt-8">
                    <label className="w-1/2 text-right text-text-primary">Roast Day <br></br> (Freshly Roasted)</label>
                    <div className="w-full pl-5">
                      <div className="w-full flex flex-wrap">
                        <div className="w-full">
                        <input className="w-full border border-gray-300 px-4 py-2" type="text" placeholder="(Monday, Sunday, Tuesday, Wednesday, Thursday, Friday, Saturday)" value={rostDate.desc} onChange={(e) => setRoastDate({...rostDate, desc: e.target.value })}/>
                        </div>
                      </div>
                    </div>
                  </div>
                )
              }

              {
                STATEFORM.find(v=>v.id === selectedCategory)?.form.includes('description') && (
                  <div className="flex mt-8">
                    <label className="w-1/2 text-right text-text-primary">Short Description<span className="text-primary">*</span></label>
                    <div className="w-full pl-5">
                      <textarea className="w-full border border-gray-300 px-4 py-2" onChange={(e) => setShortDesc(e.target.value)} maxLength={255}>{shortDesc}</textarea>
                      <label className="w-1/2 text-sm text-text-primary"><span className="text-primary">Max Character 255</span></label>
                    </div>
                  </div>
                )
              }

              {
                STATEFORM.find(v=>v.id === selectedCategory)?.form.includes('description') && (
                  <div className="flex mt-8">
                    <label className="w-1/2 text-right text-text-primary">Description<span className="text-primary"></span></label>
                    <div className="w-full pl-5">
                      <textarea className="w-full border border-gray-300 px-4 py-2" onChange={(e) => setDesc(e.target.value)}>{desc}</textarea>
                    </div>
                  </div>
                )
              }

{
                STATEFORM.find(v=>v.id === selectedCategory)?.form.includes('po') && (
                  <div className="flex mt-8">
                    <label className="w-1/2 text-right text-text-primary">Preorder</label>
                    <div className="w-full pl-5">
                      <input type="checkbox" checked={po} onChange={(e) => setPO(!po)} />
                    </div>
                  </div>
                )
              }
              
              {
                STATEFORM.find(v=>v.id === selectedCategory)?.form.includes('estimated') && (
                  <div className="flex mt-8">
                    <label className="w-1/2 text-right text-text-primary">Preorder Estimated Time</label>
                    <div className="w-full pl-5">
                        <div className="w-full">
                          <input className="w-full border border-gray-300 px-4 py-2" type="text" placeholder="Estimated Time in day, example : 1, 14, 30" value={estimated} onChange={(e) => setEstimated(e.target.value)}/>
                        </div>
                    </div>
                  </div>
                )
              }
            </div>
            
            {/* variant */}
            {
              STATEFORM.find(v=>v.id === selectedCategory)?.form.includes('images') && (
                <div className="w-full flex flex-wrap justify-center">
                  {
                    variants.map((v, i) => (
                      <div className="w-full flex flex-wrap justify-center" key={i}>
                      <div className="border-t w-full mt-8 mx-8" />
                      <div className="w-1/2">
                        <div className="flex justify-end">
                          <img className="cursor-pointer mt-4" src="/images/ic_close.png" alt="" onClick={() => handleDeleteVariant(i)}/>
                        </div>
                        {
                          STATEFORM.find(v=>v.id === selectedCategory)?.form.includes('sku') && (
                            <div className="flex mt-4">
                              <label className="w-1/2 text-right text-text-primary">SKU<span className="text-primary">*</span></label>
                              <div className="w-full pl-5">
                                <input type="text" className="w-full border border-gray-300 px-4 py-2" value={v.sku} onChange={(e) => handleSetVariant('sku', e.target.value, i)} />
                              </div>
                            </div>
                          )
                        }
    
                        {
                          STATEFORM.find(v=>v.id === selectedCategory)?.form.includes('price') && (
                            <div className="flex mt-8">
                              <label className="w-1/2 text-right text-text-primary">Price<span className="text-primary">*</span></label>
                              <div className="w-full pl-5">
                                <input type="text" className="w-full border border-gray-300 px-4 py-2" placeholder="Rp" value={'Rp ' + formatNumber(v.price)} onChange={(e) => handleSetVariant('price', e.target.value, i)} />
                              </div>
                            </div>
                          )
                        }
                        
                        {
                          STATEFORM.find(v=>v.id === selectedCategory)?.form.includes('price') && (
                            <div className="flex mt-8">
                              <label className="w-1/2 text-right text-text-primary">Discount Price</label>
                              <div className="w-full pl-5 flex flex-wrap">
                                <div className="w-full flex items-center">
                                  <input type="date" className="w-full border border-gray-300 px-4 py-2" placeholder="From" value={v.discountStart} onChange={(e) => handleSetVariant('discountStart', e.target.value, i)}/>
                                  <div className="m-3">To</div>
                                  <input type="date" className="w-full border border-gray-300 px-4 py-2" placeholder="To" value={v.discountEnd} onChange={(e) => handleSetVariant('discountEnd', e.target.value, i)}/>
                                </div>
                                <input type="text" className="w-full mt-3 border border-gray-300 px-4 py-2" placeholder="Rp" value={'Rp ' + formatNumber(v.discountValue)} onChange={(e) => handleSetVariant('discountValue', e.target.value, i)}/>
                              </div>
                            </div>
                          )
                        }
                        
                        {
                          STATEFORM.find(v=>v.id === selectedCategory)?.form.includes('qty') && (
                            <div className="flex mt-8">
                              <label className="w-1/2 text-right text-text-primary">Quantity<span className="text-primary">*</span></label>
                              <div className="w-full pl-5 flex">
                                <input type="text" className="w-full border border-gray-300 px-4 py-2" value={v.quantity} onChange={(e) => handleSetVariant('quantity', e.target.value, i)} />
                                <div className="h-full flex items-center px-2 bg-gray-300 text-text-primary rounded-r">
                                  PCS
                                </div>
                              </div>
                            </div>
                          )
                        }
    
                        {
                          STATEFORM.find(v=>v.id === selectedCategory)?.form.includes('weight') && (
                            <div className="flex mt-8">
                            <label className="w-1/2 text-right text-text-primary">Net Weight<span className="text-primary">*</span></label>
    
                            <div className="flex w-full">
                              <div className="w-full pl-5 flex">
                                <input type="text" className="w-full border border-gray-300 px-4 py-2" value={v.netWeight} onChange={(e) => handleSetVariant('netWeight', e.target.value, i)} />
                                <div className="h-full flex items-center px-2 bg-gray-300 text-text-primary rounded-r">
                                  GRAM
                                </div>
                              </div>
                              <label className="w-1/2 text-right text-text-primary">Gross Weight<span className="text-primary">*</span></label>
                              <div className="w-full pl-5 flex">
                                <input type="text" className="w-full border border-gray-300 px-4 py-2" value={v.grossWeight} onChange={(e) => handleSetVariant('grossWeight', e.target.value, i)} />
                                <div className="h-full flex items-center px-2 bg-gray-300 text-text-primary rounded-r">
                                  GRAM
                                </div>
                              </div>
                            </div>
                          </div>
                          )
                        }
                        {
                            STATEFORM.find(v=>v.id === selectedCategory)?.form.includes('sizes') && (
                              <div className="flex mt-8">
                                <label className="w-1/2 text-right text-text-primary">Size</label>
                                <div className="w-full pl-5 flex">
                                  <select className="w-full border border-gray-300 px-4 py-2" value={v.selectedSize} onChange={(e) => handleSetVariant('selectedSize', e.target.value, i)}>
                                    <option value="" disabled selected>Select Size</option>
                                    {
                                      sizes.map(({id, value}) => {
                                        return (
                                          <option key={id} value={id}>{value}</option>
                                        )
                                      })
                                    }
                                  </select>
                                  <div className="">
                                    <div className="rounded-r px-3 py-1 bg-gray-300 items-center flex h-full">
                                      <img src="/images/ic_arrow_down.png" alt=""/>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            )
                          }
                      </div>
                     
                      </div>
                    ))
                  } 
                  <div className="border-t w-full mt-8 mx-8" />
                  <div className="w-full mt-4 flex justify-end mx-8">
                    <button className="text-green-500 font-medium" onClick={handleAddVariant}>+ Add More Variant</button>
                  </div>
                </div>
              )
            }
            
            {/* images */}
            {
              STATEFORM.find(v=>v.id === selectedCategory)?.form.includes('images') && (
                <div className="w-full flex flex-wrap">
                  <div className="border-t w-full mt-8 mx-8" />
                  <div className="w-full flex justify-between my-2 mx-8 items-center">
                    <div className="font-medium text-text-primary">Images <span className="text-primary text-sm">*Minimal Resolution 700px X 700px</span></div>
                    <div className="mr-5">
                      <img src="/images/ic_arrow_up.png" alt="" />
                    </div>
                  </div>
                  <div className="border-t w-full mx-8" />

                  <div className="w-full flex flex-wrap mx-8 py-3">
                    {
                      images.map((image) => (
                        <div className="w-40 px-1">
                          <div className="w-full cursor-pointer border border-gray-500 h-40 flex flex-col justify-center items-center hover:bg-gray-100 overflow-hidden">
                            <img src={image.base64} alt="" className="object-contain"/>
                          </div>
                          <div className="w-full">
                            <button className="w-full bg-primary text-white" onClick={() => handleDeleteImage(image)}>HAPUS</button>
                          </div>
                        </div>
                      ))
                    }
                    <div className="w-40 px-1">
                      <div className="w-full cursor-pointer border border-gray-500 h-40 flex flex-col justify-center items-center p-0 hover:bg-gray-100" onClick={() => {
                        const inpImageRef = window.document.getElementById('input-file')
                        if(inpImageRef) inpImageRef.click()
                      }}>
                        <img className="w-20" src="/images/ic_camera.png" alt=""/>
                        <div className="text-text-primary text-sm text-center mt-2">
                          Browse to find or drag image here
                        </div>
                        <input id="input-file" type="file" name="image" className="hidden" onChange={handleInsertImage}/>
                      </div>
                    </div>
                  </div>
                </div>
              )
            }
          </div>

          <Modal open={modalCrop.open} onClose={() => setModalCrop({...modalCrop, open: false})}>
            <CropPicker 
              ratio={1/1} 
              onClosed={() => setModalCrop({...modalCrop, open: false})} 
              onSubmit={(croppedImage) => handleCroppedImage(croppedImage, modalCrop.cb)} 
              image={modalCrop.image}
              onCrop={() => setLoading(true)}
              onCropped={() => setLoading(true)}
            />
          </Modal>

        </div>
      </div>
    </BackOfficeBase>
  )
}

// export async function getStaticProps() {
dashboard.getInitialProps = async (ctx) => {
  const { slug } = ctx.query;
  const { token, endpoint } = ctx.store.getState().authentication;
  const requests = await getRequests(token);
  const categories = await getCategories(token);
  const origins = await getOrigins(token);
  const species = await getSpecies(token);
  const processing = await getProcessing(token);
  const tasted = await getTasted(token);
  const roastLevels = await getRoastLevels(token);
  const teaTypes = await getTeaTypes(token);
  const chocolateTypes = await getChocolateTypes(token);
  const sizes = await getSizes(token);
  const product = await getOne(endpoint.apiUrl, slug, token);
  return {
    product: product.data,
    requests: requests.data.map(v => ({...v, checked: false})),
    categories: categories.data,
    origins: origins.data,
    species: species.data,
    processing: processing.data,
    tasted: tasted.data.map(v => ({...v, checked: false})),
    roastLevels: roastLevels.data,
    teaTypes: teaTypes.data,
    chocolateTypes: chocolateTypes.data,
    sizes: sizes.data,
    cloudinaryUrl: endpoint.cloudinaryUrl,
    apiUrl: endpoint.apiUrl,
    token: token
  }
}

export default connect(
  (state) => state, {}
)(dashboard);