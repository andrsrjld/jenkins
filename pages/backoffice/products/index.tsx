import { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import BackOfficeBase from 'components/BackOfficeBase'
import ContentHeader from 'components/BackOfficeBase/ContentHeader'
import { getProducts, TParams, ORDER, ORDERTYPE, STATE, deleteProduct, updateStatusProduct } from 'data/api/product'
import { formatNumber } from 'util/helper'
import Router from 'next/router'

const breadcumb = [
  {
    title: 'Home',
    link: '/',
  },
  {
    title: 'Catalog',
    link: '/',
  },
  {
    title: 'Manage Products',
    link: '/',
  },
]

type TProps = {
  apiUrl: string,
  token: string,
  authentication: any
}

const Status = ({status}) => {
  switch (status) {
    case STATE.ACTIVE:
      return (
        <div className="bg-green-500 text-white px-3 py-1 text-center font-medium rounded-md text-sm">
          {status}
        </div>
      )
    case STATE.DRAFT:
      return (
        <div className="bg-gray-400 text-text-primary px-3 py-1 text-center font-medium rounded-md text-sm">
          {status}
        </div>
      )
    case STATE.NonActive:
      return (
        <div className="bg-primary text-white px-3 py-1 text-center font-medium rounded-md text-sm">
          {status}
        </div>
      )
    default:
      return (
        <div className="bg-primary text-white px-3 py-1 text-center font-medium rounded-md text-sm">
          {status}
        </div>
      )
  }
}

const Product = ({apiUrl, token, authentication}: TProps) => {
  const [page, setPage] = useState(1)
  const [limit, setLimit] = useState(10)
  const [keyword, setKeyword] = useState('')
  const [tabledDatas, setTableDatas] = useState([])

  useEffect(() => {
    if(typeof authentication.user.seller.shipment_method == 'undefined') {
      Router.push('/backoffice/settings?warning=true')
    } else if(authentication.user.seller.status == 'Pending') {
      Router.push('/backoffice/settings?approved=false')
    } else {
      fetchProducts()
    }
  }, [])

  useEffect(() => {
    fetchProducts()
  }, [page,limit])

  
  useEffect(() => {
    if(page != 1) {
      setPage(1)
    } else {
      fetchProducts()
    }
  }, [keyword])

  const fetchProducts = async () => {
    const params:TParams = {
      layout_type: 'list_layout',
      seller_slug: authentication.user.seller.slug,
      type_slug: 'All',
      keyword: keyword,
      status: STATE.ALL,
      origin: '',
      species: '',
      tasted: '',
      roast_level: '',
      price: '',
      page: page,
      limit: limit,
      order: `${ORDERTYPE.DATE},${ORDER.DESC}`
    }
    const res = await getProducts(apiUrl, params, token)
    setTableDatas(res.data ? res.data.map(v => ({...v, isChecked: false})) : [])
  }

  const handleDelete = () => {
    const promises = [];
    tabledDatas.filter(v => v.isChecked).forEach(v => {
      promises.push(deleteProduct(apiUrl, {id: v.id}, token))
    })
    Promise.all(promises)
      .then(res => {
        fetchProducts()
      }).catch(err => {});
  }
  
  const handleUpdateStatus = (status) => {
    const promises = [];
    tabledDatas.filter(v => v.isChecked).forEach(v => {
      promises.push(updateStatusProduct(apiUrl, {id: v.id, status: status}, token))
    })
    Promise.all(promises)
      .then(res => {
        fetchProducts()
      }).catch(err => {});
  }

  const handleChangeChecked = (index) => {
    setTableDatas(tabledDatas.map((v,i) => ({...v, isChecked: index === i ? !v.isChecked : v.isChecked})))
  }

  const handleChangeAction = (e) => {
    const { value } = e.target;
    switch (value) {
      case 'active':
        handleUpdateStatus(STATE.ACTIVE)
        break;
      case 'draft':
        handleUpdateStatus(STATE.DRAFT)
        break;
      case 'non-active':
        handleUpdateStatus(STATE.NonActive)
        break;
      case 'delete':
        handleDelete()
        break;
      default:
        break;
    }
  }

  return (
    <BackOfficeBase>
      <div className="w-full flex flex-wrap">
        <ContentHeader title="Manage Products" breadcumb={breadcumb} />

        <div className="w-full">
          <div className="bg-white shadow-lg p-4 mt-12 rounded-lg flex flex-wrap">

            <div id="filters" className="w-full flex items-center">
              <div className="w-full">
                <input className="border border-gray-300" type="text" placeholder="search" value={keyword} onChange={(e) => setKeyword(e.target.value)}/>
              </div>
              <div className="w-full flex justify-end">
                {/* <button className="py-1 px-3 border-2 border-gray-300 bg-white rounded text-text-primary flex items-center mr-2">
                  <img className="mr-2" src="/images/ic_filter.png" alt="" />
                  FILTERS
                </button>
                <button className="py-1 px-3 border-2 border-gray-300 bg-white rounded text-text-primary flex items-center">
                  <img className="mr-2" src="/images/ic_export.png" alt="" />
                  EXPORT
                </button> */}
              </div>
            </div>

            <div id="sortings" className="w-full flex items-center mt-4">
              <div className="w-full flex flex-wrap items-center">
                <div className="flex">
                  <div className="border border-gray-400 rounded-l flex items-center h-full">
                    <select className="py-2 px-5 text-text-primary" value="" onChange={handleChangeAction}>
                      <option value="" disabled>Action</option>
                      <option value="active">Change to Active</option>
                      <option value="draft">Change to Draft</option>
                      <option value="non-active">Change to Non Active</option>
                      <option value="delete">Delete</option>
                    </select>
                  </div>
                  <div className="rounded-r px-3 py-1 bg-gray-300 items-center flex">
                    <img src="/images/ic_arrow_down.png" alt=""/>
                  </div>
                </div>
                <div className="text-text-primary font-medium ml-3">
                  {/* 106 records found */}
                </div>
              </div>
              <div className="w-full flex justify-end items-center">
                {/* <select className="w-auto">
                  <option>10</option>
                </select>
                <div className="text-text-primary font-medium ml-3">
                  Per Page
                </div> */}
                <button className="px-3 py-2 bg-gray-400 rounded text-text-primary flex items-center ml-4" onClick={() => setPage(page - 1)} disabled={page == 1}>
                  <img src="/images/ic_chevron_left.png" alt="" />
                </button>
                <input className="ml-3 w-12 text-center border border-gray-300" type="text" value={page}/>
                {/* <div className="text-text-primary font-medium ml-3">
                  Of 6
                </div> */}
                <button className="px-3 py-2 bg-gray-400 rounded text-text-primary flex items-center ml-4" onClick={() => setPage(page + 1 )}>
                  <img src="/images/ic_arrow_right.png" alt="" />
                </button>
              </div>
            </div>

            <div id="table" className="w-full mt-8">
              <table className="w-full">
                <thead>
                  <th className="text-center"></th>
                  <th>ID</th>
                  <th>Thumbnail</th>
                  <th>Name</th>
                  <th>Type</th>
                  {/* <th>Attribute Set</th>
                  <th>SKU</th> */}
                  <th>Price</th>
                  {/* <th>Quantity</th> */}
                  <th className="text-center">Status</th>
                </thead>
                <tbody>
                  {
                    tabledDatas.map((v, i) => (
                      <tr key={v.id} className={`${i%2 == 0 ? 'bg-gray-200' : 'bg-white'} cursor-pointer hover:bg-gray-300`} onClick={(e) => {
                        const target : any = e.target;
                        if(target.id === 'checkbox') return
                        window.location.href = `/backoffice/products/detail/${v.slug}`
                      }}>
                        <td className="text-center" id="checkbox">
                          <input id="checkbox" type="checkbox" checked={v.isChecked} onChange={() => handleChangeChecked(i)}/>
                        </td>
                        <td>
                          {v.id}
                        </td>
                        <td>
                          <img className="h-auto w-10" src={v.images ? v.images[0].image_url : ''} />
                        </td>
                        <td>
                          {v.name}
                        </td>
                        <td>
                          {v.product_type_name}
                        </td>
                        <td className="text-right">
                          {`Rp ${formatNumber(v.price)}`}
                        </td>
                        <td>
                          <Status status={v.status}/>
                        </td>
                      </tr>
                    ))
                  }
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </BackOfficeBase>
  )
}

Product.getInitialProps = (ctx) => {
  const { token, endpoint } = ctx.store.getState().authentication;
  return {
      apiUrl: endpoint.apiUrl,
      token: token
  }
}

export default connect(
  (state) => state, {}
)(Product);
