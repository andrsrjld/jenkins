import { useState, useEffect } from 'react'
import {connect} from 'react-redux'
import BackOfficeBase from 'components/BackOfficeBase'
import ContentHeader from 'components/BackOfficeBase/ContentHeader'
import { motion } from "framer-motion"
import { getOrdersSeller, updateStatusOrder, getOrderDetail, STATUS } from 'data/api/order'
import {formatNumber} from 'util/helper'
import Router from 'next/router'
import OrderItem from 'components/OrderItem'
import Modal from 'components/Modal'
import moment from 'moment'
import { postRefund } from 'data/api/refund'
import Loading from 'components/Loading'

// Toastr Notification
import {successNotification, errorNotification} from 'util/toastr-notif';

const breadcumb = [
  {
    title: 'Home',
    link: '/',
  },
  {
    title: 'Sales',
    link: '/',
  }
]

const sliderMenu = [
  {
    content: 1,
    name: 'Paid',
    qty: '1',
    isActive: true,
    status: 'Pesanan Sudah Dibayar'
  },
  {
    content: 2,
    name: 'Processed',
    qty: '1',
    isActive: false,
    status: 'Pesanan Diproses'
  },
  {
    content: 3,
    name: 'Shipped',
    qty: '1',
    isActive: false,
    status: 'Pesanan Dikirim'
  },
  {
    content: 4,
    name: 'Completed',
    qty: '2',
    isActive: false,
    status: 'Pesanan Selesai'
  },
  {
    content: 5,
    name: 'Cancelled',
    qty: '3',
    isActive: false,
    status: 'Pesanan Dibatalkan'
  },
]

const SliderItem = ({data, cbClick}) => (
  <div className={`font-medium px-2 py-2 bg-white cursor-pointer border-b-2 ${data.isActive ? 'border-primary text-primary' : 'border-gray-500 text-text-primary'} hover:text-primary`}
    onClick={() => cbClick(data)}>
    {`${data.name}`}
  </div>
)

const ItemProses = ({data, callback, action, callbackDetail}) => (
  <div className="w-full">
    <div className="w-full bg-white shadow-sm p-4 mt-4 rounded flex flex-wrap">
      <div className="w-full flex text-text-primary font-medium">
        <div className="w-full px-4 border-r-2 flex">
          <input className="mr-4 mt-1" type="checkbox" />
          <div className="w-full flex flex-wrap">
            <div className="w-full">
              {data.user_name}
            </div>
            <div className="w-full">
              No Transaksi <span className="font-normal">{data.invoice_no}</span>
            </div>
            {/* <div className="w-full flex mt-2 font-normal">
              <img className="w-24" src={data.image} alt=""/>
              <div>
                {`${data.sellerName} - ${data.productName}`}
              </div>
            </div> */}
          </div>
        </div>
        <div className="w-full px-4 border-r-2">
          Pengiriman <br></br>
          <div className="font-normal">
            <table>
              <tr>
                <tr>
                  <td className="pt-1 pb-0 px-0">Penerima</td>
                  <td className="pt-1 pb-0 px-2">:</td>
                  <td className="pt-1 pb-0 px-0">{data.receiver_name}</td>
                </tr>
                <tr>
                  <td className="pt-1 pb-0 px-0">Kurir</td>
                  <td className="pt-1 pb-0 px-2">:</td>
                  <td className="pt-1 pb-0 px-0">{data.shipping}</td>
                </tr>
              </tr>
            </table>
          </div>
        </div>
        <div className="w-full px-4 flex flex-wrap flex-col">
          <div className="w-full">
            Status Transaksi
          </div>
          <div className="w-full flex mt-2">
            <div className="mx-1 px-2 py-1 rounded-full bg-primary text-white flex items-center">
              <img src="/images/ic_check_white.png" alt="" />
            </div>
            <div className="mx-1 px-2 py-1 rounded-full bg-primary text-white flex items-center">
              <img src="/images/ic_check_white.png" alt="" />
            </div>
            <div className="mx-1 px-3 py-1 rounded-full bg-gray-300">
              3
            </div>
            <div className="mx-1 px-3 py-1 rounded-full bg-gray-300">
              4
            </div>
          </div>
          <div className="w-full mt-4">
            <button className="bg-gray-300 px-3 py-1 w-auto font-medium rounded">{data.status}</button>
          </div>
          <div className="w-full mt-4 font-normal">
            {data.timeLimit}
          </div>
        </div>
        <div className="w-full px-4 items-center spac">
          <button className="w-full border rounded py-2 font-medium">
            <input type="text" placeholder="Masukkan Nomor Resi" value={data.airway_bill ? data.airway_bill : ''} onChange={callback(data.id)}/>
          </button>
          <button className="w-full border rounded py-2 font-medium mt-2" onClick={() => action(data, STATUS.DIKIRIM)}>
            SIMPAN RESI
          </button>
          <button className="w-full rounded py-2 font-medium mt-2 flex justify-center items-center text-primary" onClick={() => callbackDetail(data)}>
            LIHAT DETAIL <img className="ml-2" src="/images/ic_arrow_down_red.png" alt="" />
          </button>
        </div>
      </div>
    </div>
    
    <div className={`w-full flex text-text-primary ${data.detail ? '' : 'hidden'}`}>
        <div className="w-full pl-4 pr-2">
          <div className="w-full bg-white shadow-sm p-4 mt-4 rounded">
            <div>
              <span className="font-medium">Detail Barang</span>
            </div>
            {
              data.detail?.order_items.map(v => (
                <div key={v.product_name} className="w-full flex mt-4">
                  <div className="w-full flex">
                    <img className="w-24 mr-4" src={v.img_url} alt=""></img>
                    {`${v.product_name}`}
                  </div>
                  <div className="w-1/2 text-right">
                    {v.quantity} Barang
                  </div>
                  <div className="w-1/2 text-right">
                    {`Rp ${formatNumber(v.subtotal)}`}
                  </div>
                </div>
              ))
            }
            <div className="mt-4 w-full h-px bg-text-primary"/>
            <div className="w-full mt-4 flex justify-end">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Total Berat Barang
              </div>
              <div className="w-1/2 text-right">
                {data.detail?.total_weight} Gram
              </div>
            </div>
            <div className="w-full mt-4 flex justify-end">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Ongkos Kirim
              </div>
              <div className="w-1/2 text-right">
                {`Rp ${formatNumber(data.detail?.total_shipping)}`}
              </div>
            </div>
            {/* <div className="w-full mt-4 flex justify-end">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Biaya Layanan
              </div>
              <div className="w-1/2 text-right">
                Rp 5.000
              </div>
            </div> */}
            <div className="w-full mt-4 flex justify-end">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Diskon
              </div>
              <div className="w-1/2 text-right">
                - Rp 0
              </div>
            </div>
            <div className="w-full mt-4 flex justify-end font-medium">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Grand Total
              </div>
              <div className="w-1/2 text-right">
              {`Rp ${formatNumber(data.detail?.total_paid)}`}
              </div>
            </div>
          </div>
        </div>
        <div className="w-full pl-2 flex flex-col">
          <div className="w-full bg-white shadow-sm p-4 mt-4 rounded">
            <div>
              <span className="font-medium">Alamat Pengiriman</span>
            </div>
            <div className="mt-4">
              {data.buyerName} <br></br>
              {data.detail?.address}
              <br></br>
              <span className="font-medium">Telepon</span> : {data.detail?.phone}
            </div>
          </div>
          <div className="w-full bg-white shadow-sm p-4 mt-4 rounded">
            <div>
              <span className="font-medium">Tindakan</span>
            </div>
            <div className="mt-4">
              <div>
                <button className="bg-gray-400 rounded font-medium px-3 py-2">CETAK PESANAN</button>
                <button className="bg-gray-400 rounded font-medium px-3 py-2 ml-4">CETAK BUKTI PEMBAYARAN</button>
              </div>
            </div>
          </div>
          <div className="w-full bg-white shadow-sm p-4 mt-4 rounded">
            <div>
              <span className="font-medium">Catatan Pembelian</span>
            </div>
            <div className="mt-4">
              Tidak ada catatan
            </div>
          </div>
        </div>
      </div>
  
  </div>
)

const ItemDikirim = ({data, callback, action, callbackDetail}) => (
  <div className="w-full">
    <div className="w-full bg-white shadow-sm p-4 mt-4 rounded flex flex-wrap">
      <div className="w-full flex text-text-primary font-medium">
        <div className="w-full px-4 border-r-2 flex">
          <input className="mr-4 mt-1" type="checkbox" />
          <div className="w-full flex flex-wrap">
            <div className="w-full">
              {data.user_name}
            </div>
            <div className="w-full">
              No Transaksi <span className="font-normal">{data.invoice_no}</span>
            </div>
            {/* <div className="w-full flex mt-2 font-normal">
              <img className="w-24" src={data.image} alt=""/>
              <div>
                {`${data.sellerName} - ${data.productName}`}
              </div>
            </div> */}
          </div>
        </div>
        <div className="w-full px-4 border-r-2">
          Pengiriman <br></br>
          <div className="font-normal">
            <table>
              <tr>
                <tr>
                  <td className="pt-1 pb-0 px-0">Penerima</td>
                  <td className="pt-1 pb-0 px-2">:</td>
                  <td className="pt-1 pb-0 px-0">{data.receiver_name}</td>
                </tr>
                <tr>
                  <td className="pt-1 pb-0 px-0">Kurir</td>
                  <td className="pt-1 pb-0 px-2">:</td>
                  <td className="pt-1 pb-0 px-0">{data.shipping}</td>
                </tr>
              </tr>
            </table>
          </div>
        </div>
        <div className="w-full px-4 flex flex-wrap flex-col">
          <div className="w-full">
            Status Transaksi
          </div>
          <div className="w-full flex mt-2">
            <div className="mx-1 px-2 py-1 rounded-full bg-primary text-white flex items-center">
              <img src="/images/ic_check_white.png" alt="" />
            </div>
            <div className="mx-1 px-2 py-1 rounded-full bg-primary text-white flex items-center">
              <img src="/images/ic_check_white.png" alt="" />
            </div>
            <div className="mx-1 px-2 py-1 rounded-full bg-primary text-white flex items-center">
              <img src="/images/ic_check_white.png" alt="" />
            </div>
            <div className="mx-1 px-3 py-1 rounded-full bg-gray-300">
              4
            </div>
          </div>
          <div className="w-full mt-4">
            <button className="bg-gray-300 px-3 py-1 w-auto font-medium rounded">{data.status}</button>
          </div>
          <div className="w-full mt-4 font-normal">
            {data.timeLimit}
          </div>
        </div>
        <div className="w-full px-4 items-center">
          <button className="w-full border rounded py-2 font-medium">
            <input type="text" placeholder="Masukkan Nomor Resi" value={data.airway_bill ? data.airway_bill : ''} onChange={callback(data.id)} disabled/>
          </button>
          <button className="w-full rounded py-2 font-medium mt-2 flex justify-center items-center text-primary" onClick={() => callbackDetail(data)}>
            LIHAT DETAIL <img className="ml-2" src="/images/ic_arrow_down_red.png" alt="" />
          </button>
        </div>
      </div>
    </div>

    <div className={`w-full flex text-text-primary ${data.detail ? '' : 'hidden'}`}>
        <div className="w-full pl-4 pr-2">
          <div className="w-full bg-white shadow-sm p-4 mt-4 rounded">
            <div>
              <span className="font-medium">Detail Barang</span>
            </div>
            {
              data.detail?.order_items.map(v => (
                <div key={v.product_name} className="w-full flex mt-4">
                  <div className="w-full flex">
                    <img className="w-24 mr-4" src={v.img_url} alt=""></img>
                    {`${v.product_name}`}
                  </div>
                  <div className="w-1/2 text-right">
                    {v.quantity} Barang
                  </div>
                  <div className="w-1/2 text-right">
                    {`Rp ${formatNumber(v.subtotal)}`}
                  </div>
                </div>
              ))
            }
            <div className="mt-4 w-full h-px bg-text-primary"/>
            <div className="w-full mt-4 flex justify-end">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Total Berat Barang
              </div>
              <div className="w-1/2 text-right">
                {data.detail?.total_weight} Gram
              </div>
            </div>
            <div className="w-full mt-4 flex justify-end">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Ongkos Kirim
              </div>
              <div className="w-1/2 text-right">
                {`Rp ${formatNumber(data.detail?.total_shipping)}`}
              </div>
            </div>
            {/* <div className="w-full mt-4 flex justify-end">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Biaya Layanan
              </div>
              <div className="w-1/2 text-right">
                Rp 5.000
              </div>
            </div> */}
            <div className="w-full mt-4 flex justify-end">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Diskon
              </div>
              <div className="w-1/2 text-right">
                - Rp 0
              </div>
            </div>
            <div className="w-full mt-4 flex justify-end font-medium">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Grand Total
              </div>
              <div className="w-1/2 text-right">
              {`Rp ${formatNumber(data.detail?.total_paid)}`}
              </div>
            </div>
          </div>
        </div>
        <div className="w-full pl-2 flex flex-col">
          <div className="w-full bg-white shadow-sm p-4 mt-4 rounded">
            <div>
              <span className="font-medium">Alamat Pengiriman</span>
            </div>
            <div className="mt-4">
              {data.buyerName} <br></br>
              {data.detail?.address}
              <br></br>
              <span className="font-medium">Telepon</span> : {data.detail?.phone}
            </div>
          </div>
          <div className="w-full bg-white shadow-sm p-4 mt-4 rounded">
            <div>
              <span className="font-medium">Tindakan</span>
            </div>
            <div className="mt-4">
              <div>
                <button className="bg-gray-400 rounded font-medium px-3 py-2">CETAK PESANAN</button>
                <button className="bg-gray-400 rounded font-medium px-3 py-2 ml-4">CETAK BUKTI PEMBAYARAN</button>
              </div>
            </div>
          </div>
          <div className="w-full bg-white shadow-sm p-4 mt-4 rounded">
            <div>
              <span className="font-medium">Catatan Pembelian</span>
            </div>
            <div className="mt-4">
              Tidak ada catatan
            </div>
          </div>
        </div>
      </div>

  </div>
)

const ItemSelesai = ({data, callback, action, callbackDetail}) => (
  <div className="w-full">
    <div className="w-full bg-white shadow-sm p-4 mt-4 rounded flex flex-wrap">
      <div className="w-full flex text-text-primary font-medium">
        <div className="w-full px-4 border-r-2 flex">
          <input className="mr-4 mt-1" type="checkbox" />
          <div className="w-full flex flex-wrap">
            <div className="w-full">
              {data.user_name}
            </div>
            <div className="w-full">
              No Transaksi <span className="font-normal">{data.invoice_no}</span>
            </div>
            {/* <div className="w-full flex mt-2 font-normal">
              <img className="w-24" src={data.image} alt=""/>
              <div>
                {`${data.sellerName} - ${data.productName}`}
              </div>
            </div> */}
          </div>
        </div>
        <div className="w-full px-4 border-r-2">
          Pengiriman <br></br>
          <div className="font-normal">
            <table>
              <tr>
                <tr>
                  <td className="pt-1 pb-0 px-0">Penerima</td>
                  <td className="pt-1 pb-0 px-2">:</td>
                  <td className="pt-1 pb-0 px-0">{data.receiver_name}</td>
                </tr>
                <tr>
                  <td className="pt-1 pb-0 px-0">Kurir</td>
                  <td className="pt-1 pb-0 px-2">:</td>
                  <td className="pt-1 pb-0 px-0">{data.shipping}</td>
                </tr>
              </tr>
            </table>
          </div>
        </div>
        <div className="w-full px-4 flex flex-wrap flex-col">
          <div className="w-full">
            Status Transaksi
          </div>
          <div className="w-full flex mt-2">
            <div className="mx-1 px-2 py-1 rounded-full bg-primary text-white flex items-center">
              <img src="/images/ic_check_white.png" alt="" />
            </div>
            <div className="mx-1 px-2 py-1 rounded-full bg-primary text-white flex items-center">
              <img src="/images/ic_check_white.png" alt="" />
            </div>
            <div className="mx-1 px-2 py-1 rounded-full bg-primary text-white flex items-center">
              <img src="/images/ic_check_white.png" alt="" />
            </div>
            <div className="mx-1 px-2 py-1 rounded-full bg-primary text-white flex items-center">
              <img src="/images/ic_check_white.png" alt="" />
            </div>
            <div className="mx-1 px-3 py-1 rounded-full bg-gray-300 opacity-0">
              4
            </div>
          </div>
          <div className="w-full mt-4">
            <button className="bg-gray-300 px-3 py-1 w-auto font-medium rounded">{data.status}</button>
          </div>
          <div className="w-full mt-4 font-normal">
            {data.timeLimit}
          </div>
        </div>
        <div className="w-full px-4 items-center">
          {/* <button className="w-full border rounded py-2 font-medium">
            <input type="text" placeholder="Masukkan Nomor Resi" value={data.airway_bill ? data.airway_bill : ''} onChange={callback(data.id)} disabled/>
          </button> */}
          {/* <button className="w-full border rounded py-2 font-medium mt-2" onClick={() => action(data, STATUS.DIKIRIM)}>
            SIMPAN RESI
          </button> */}
          <button className="w-full rounded py-2 font-medium mt-2 flex justify-center items-center text-primary"onClick={() => callbackDetail(data)}>
            LIHAT DETAIL <img className="ml-2" src="/images/ic_arrow_down_red.png" alt="" />
          </button>
        </div>
      </div>
    </div>

    <div className={`w-full flex text-text-primary ${data.detail ? '' : 'hidden'}`}>
        <div className="w-full pl-4 pr-2">
          <div className="w-full bg-white shadow-sm p-4 mt-4 rounded">
            <div>
              <span className="font-medium">Detail Barang</span>
            </div>
            {
              data.detail?.order_items.map(v => (
                <div key={v.product_name} className="w-full flex mt-4">
                  <div className="w-full flex">
                    <img className="w-24 mr-4" src={v.img_url} alt=""></img>
                    {`${v.product_name}`}
                  </div>
                  <div className="w-1/2 text-right">
                    {v.quantity} Barang
                  </div>
                  <div className="w-1/2 text-right">
                    {`Rp ${formatNumber(v.subtotal)}`}
                  </div>
                </div>
              ))
            }
            <div className="mt-4 w-full h-px bg-text-primary"/>
            <div className="w-full mt-4 flex justify-end">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Total Berat Barang
              </div>
              <div className="w-1/2 text-right">
                {data.detail?.total_weight} Gram
              </div>
            </div>
            <div className="w-full mt-4 flex justify-end">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Ongkos Kirim
              </div>
              <div className="w-1/2 text-right">
                {`Rp ${formatNumber(data.detail?.total_shipping)}`}
              </div>
            </div>
            {/* <div className="w-full mt-4 flex justify-end">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Biaya Layanan
              </div>
              <div className="w-1/2 text-right">
                Rp 5.000
              </div>
            </div> */}
            <div className="w-full mt-4 flex justify-end">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Diskon
              </div>
              <div className="w-1/2 text-right">
                - Rp 0
              </div>
            </div>
            <div className="w-full mt-4 flex justify-end font-medium">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Grand Total
              </div>
              <div className="w-1/2 text-right">
              {`Rp ${formatNumber(data.detail?.total_paid)}`}
              </div>
            </div>
          </div>
        </div>
        <div className="w-full pl-2 flex flex-col">
          <div className="w-full bg-white shadow-sm p-4 mt-4 rounded">
            <div>
              <span className="font-medium">Alamat Pengiriman</span>
            </div>
            <div className="mt-4">
              {data.buyerName} <br></br>
              {data.detail?.address}
              <br></br>
              <span className="font-medium">Telepon</span> : {data.detail?.phone}
            </div>
          </div>
          <div className="w-full bg-white shadow-sm p-4 mt-4 rounded">
            <div>
              <span className="font-medium">Tindakan</span>
            </div>
            <div className="mt-4">
              <div>
                <button className="bg-gray-400 rounded font-medium px-3 py-2">CETAK PESANAN</button>
                <button className="bg-gray-400 rounded font-medium px-3 py-2 ml-4">CETAK BUKTI PEMBAYARAN</button>
              </div>
            </div>
          </div>
          <div className="w-full bg-white shadow-sm p-4 mt-4 rounded">
            <div>
              <span className="font-medium">Catatan Pembelian</span>
            </div>
            <div className="mt-4">
              Tidak ada catatan
            </div>
          </div>
        </div>
      </div>
  </div>
)

const dashboard = ({authentication, trx}) => {
  const [loading, setLoading] = useState(false)
  const {token} = authentication
  const {apiUrl} = authentication.endpoint
  const [sliders, setSliders] = useState(sliderMenu)
  const [selectedContent, setSelectedContent] = useState(null)
  const [tableDatas, setTableDatas] = useState([])
  const [filteredDatas, setFilteredDatas] = useState([])
  const [keyword, setKeyword] = useState(trx)

  useEffect(() => {
    if(typeof authentication.user.seller.shipment_method == 'undefined') {
      Router.push('/backoffice/settings?warning=true')
    } else if(authentication.user.seller.status == 'Pending') {
      Router.push('/backoffice/settings?approved=false')
    } else {
      doFetchOrders()
    }
  }, [sliders])

  useEffect(() => {
    setFilteredDatas(tableDatas)
  }, [tableDatas]);

  useEffect(() => {
    setFilteredDatas(tableDatas.filter(v => v.user_name.toLowerCase().includes(keyword.toLowerCase()) || v.invoice_no.toLowerCase().includes(keyword.toLowerCase())))
  }, [keyword]);

  const doFetchOrders = async () => {
    const res = await getOrdersSeller(apiUrl, token, sliders.find(v => v.isActive).status)
    setTableDatas(res.data ? res.data : [])
  }

  const doUpdateStatus = async (data, status) => {
    if(status == STATUS.DIBATALKAN) {
      setSelectedDataRefund(data)
    } else {
      const res = await updateStatusOrder(apiUrl, token, {
        airway_bill: data.airway_bill ? data.airway_bill : '',
        id: data.id,
        status: status
      })
      doFetchOrders()
    }
  }

  const handleChangeSlider = (data) => {
    setSelectedContent(data.content)
    setSliders(sliders.map(v => ({...v, isActive: v==data })))
  }

  const handleChangeResi = (id) => (e) => {
    setTableDatas(tableDatas.map((v) => ({
      ...v, airway_bill: id === v.id ? e.target.value : v.airway_bill
    })))
  }

  //REFUND
  const [selectedDataRefund, setSelectedDataRefund] = useState(null)
  const [detailDataRefund, setDetailDataRefund] = useState(null)
  const [modalDetailRefund, setModalDetailRefund] = useState(false);

  const [refundCheckAll, setRefundCheckAll] = useState(false);
  const [refundNotes, setRefundNotes] = useState('')

  useEffect(() => {
    if(selectedDataRefund) {
      doGetOrderDetailRefund()
    }
  }, [selectedDataRefund]);

  useEffect(() => {
    if(detailDataRefund) setModalDetailRefund(true)
  }, [detailDataRefund]);

  const doGetOrderDetailRefund = async () => {
    const res = await getOrderDetail(apiUrl, token, selectedDataRefund.id)
    setDetailDataRefund(res.data ? {...res.data, order_items: res.data.order_items.map(v => ({...v, checked: false}))} : null)
  }

  const handleRefundCheckAll = () => {
    const val = !refundCheckAll
    const newData = {
      ...detailDataRefund,
      order_items: detailDataRefund?.order_items.map(v => ({
       ...v,
       checked: val
      }))
    }

    setDetailDataRefund(newData)
    setRefundCheckAll(val)
  }

  const handleCheckedRefund = (index) => {
    const newData = {
      ...detailDataRefund,
      order_items: detailDataRefund?.order_items.map((v, i) => ({
       ...v,
       checked: i === index ? !v.checked : v.checked
      }))
    }
    setDetailDataRefund(newData)
  }

  const handleSubmitRefund = () => {
    setLoading(true)

    const promises = detailDataRefund?.order_items.filter(v => v.checked).map(v => 
      postRefund(token, {
        "order_seller_id": detailDataRefund?.id,
        "product_id": v.product_id,
        "variant_id": v.variant_id,
        "user_id": detailDataRefund?.address_detail.user_id,
        "seller_id": selectedDataRefund.seller_id,
        "quantity": v.quantity,
        "subtotal":v.subtotal,
        "status": "PENDING",
        "picture": "",
        "notes": `[Dibatalkan oleh SELLER] ${refundNotes}`
      })
    )

    Promise.all(promises)
      .then((e) => {})
      .catch((err) => {}) 
      .then((e) => {
        setLoading(false)
        setModalDetailRefund(false)
        setRefundNotes('')
        doFetchOrders()
        successNotification('Permohonan REFUND berhasil dibuat!')
      })
  }

  const handleDetail = async(data) => {
    if(tableDatas.find(v => v.id == data.id).detail) {
      setTableDatas(tableDatas.map(v => ({...v, detail: null })))
    } else {
      const res = await getOrderDetail(apiUrl, token, data.id)
      setTableDatas(tableDatas.map(v => ({...v, detail: data.id == v.id ? res.data : null })))
    }
  }

  const handleSubmitAllResi = async () => {
    const promises = []
    const selected = tableDatas.filter(v => v.airway_bill).forEach(v => {
      promises.push(updateStatusOrder(apiUrl, token, {
        airway_bill: v.airway_bill ? v.airway_bill : '',
        id: v.id,
        status: STATUS.DIKIRIM
      }))
    })

    Promise.all(promises)
      .then(v => doFetchOrders())
      .catch(() => {})
  }

  const Item = ({data}) => (
    <div className="w-full">
      <div className="w-full bg-white shadow-sm p-4 mt-4 rounded flex flex-wrap">
        <div className="w-full flex text-text-primary font-medium">
          <div className="w-full px-4 border-r-2 flex">
            <input className="mr-4 mt-1" type="checkbox" />
            <div className="w-full flex flex-wrap">
              <div className="w-full">
                {data.user_name}
              </div>
              <div className="w-full">
                No Transaksi <span className="font-normal">{data.invoice_no}</span>
              </div>
              <div className="w-full flex mt-2 font-normal">
                {/* <img className="w-24" src={data.image} alt=""/>
                <div>
                  {`${data.sellerName} - ${data.productName}`}
                </div> */}
              </div>
            </div>
          </div>
          <div className="w-full px-4 border-r-2">
            Pengiriman <br></br>
            <div className="font-normal">
              <table>
                <tr>
                  <tr>
                    <td className="pt-1 pb-0 px-0">Penerima</td>
                    <td className="pt-1 pb-0 px-2">:</td>
                    <td className="pt-1 pb-0 px-0">{data.receiver_name}</td>
                  </tr>
                  <tr>
                    <td className="pt-1 pb-0 px-0">Kurir</td>
                    <td className="pt-1 pb-0 px-2">:</td>
                    <td className="pt-1 pb-0 px-0">{data.shipping}</td>
                  </tr>
                </tr>
              </table>
            </div>
          </div>
          <div className="w-full px-4 flex flex-wrap flex-col">
            <div className="w-full">
              Status Transaksi
            </div>
            <div className="w-full flex mt-2">
              <div className="mx-1 px-2 py-1 rounded-full bg-primary text-white flex items-center">
                <img src="/images/ic_check_white.png" alt="" />
              </div>
              <div className="mx-1 px-3 py-1 rounded-full bg-gray-300">
                2
              </div>
              <div className="mx-1 px-3 py-1 rounded-full bg-gray-300">
                3
              </div>
              <div className="mx-1 px-3 py-1 rounded-full bg-gray-300">
                4
              </div>
            </div>
            <div className="w-full mt-4">
              <button className="bg-gray-300 px-3 py-1 w-auto font-medium rounded">{data.status}</button>
            </div>
            <div className="w-full mt-4 font-normal">
              {data.timeLimit}
            </div>
          </div>
          <div className="w-full px-4 items-center">
            <button className="w-full border rounded py-2 font-medium" onClick={() => doUpdateStatus(data, STATUS.DIPROSES)}>
              PROSES PESANAN
            </button>
            <button className="w-full border rounded py-2 font-medium mt-2" onClick={() => doUpdateStatus(data, STATUS.DIBATALKAN)}>
              TOLAK PESANAN
            </button>
            <button className="w-full rounded py-2 font-medium mt-2 flex justify-center items-center text-primary" onClick={() => handleDetail(data)}>
              LIHAT DETAIL <img className="ml-2" src="/images/ic_arrow_down_red.png" alt=""/>
            </button>
          </div>
        </div>
      </div>
      
      <div className={`w-full flex text-text-primary ${data.detail ? '' : 'hidden'}`}>
        <div className="w-full pl-4 pr-2">
          <div className="w-full bg-white shadow-sm p-4 mt-4 rounded">
            <div>
              <span className="font-medium">Detail Barang</span>
            </div>
            {
              data.detail?.order_items.map(v => (
                <div key={v.product_name} className="w-full flex mt-4">
                  <div className="w-full flex">
                    <img className="w-24 mr-4" src={v.img_url} alt=""></img>
                    {`${v.product_name} - ${v.variant_name}`}
                  </div>
                  <div className="w-1/2 text-right">
                    {v.quantity} Barang
                  </div>
                  <div className="w-1/2 text-right">
                    {`Rp ${formatNumber(v.subtotal)}`}
                  </div>
                </div>
              ))
            }
            <div className="mt-4 w-full h-px bg-text-primary"/>
            <div className="w-full mt-4 flex justify-end">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Total Berat Barang
              </div>
              <div className="w-1/2 text-right">
                {data.detail?.total_weight} Gram
              </div>
            </div>
            <div className="w-full mt-4 flex justify-end">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Ongkos Kirim
              </div>
              <div className="w-1/2 text-right">
                {`Rp ${formatNumber(data.detail?.total_shipping)}`}
              </div>
            </div>
            <div className="w-full mt-4 flex justify-end">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Diskon
              </div>
              <div className="w-1/2 text-right">
              {`- Rp ${formatNumber(data.detail?.total_product_discount)}`}
              </div>
            </div>
            <div className="w-full mt-4 flex justify-end font-medium">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Grand Total
              </div>
              <div className="w-1/2 text-right">
              {`Rp ${formatNumber(data.detail?.total_price + data.detail?.total_shipping)}`}
              </div>
            </div>
          </div>
        </div>
        <div className="w-full pl-2 flex flex-col">
          <div className="w-full bg-white shadow-sm p-4 mt-4 rounded">
            <div>
              <span className="font-medium">Alamat Pengiriman</span>
            </div>
            <div className="mt-4">
              {data.buyerName} <br></br>
              {data.detail?.address}
              <br></br>
              <span className="font-medium">Telepon</span> : {data.detail?.phone}
            </div>
          </div>
          <div className="w-full bg-white shadow-sm p-4 mt-4 rounded">
            <div>
              <span className="font-medium">Tindakan</span>
            </div>
            <div className="mt-4">
              <div>
                <button className="bg-gray-400 rounded font-medium px-3 py-2">CETAK PESANAN</button>
                <button className="bg-gray-400 rounded font-medium px-3 py-2 ml-4">CETAK INVOICE</button>
              </div>
            </div>
          </div>
          <div className="w-full bg-white shadow-sm p-4 mt-4 rounded">
            <div>
              <span className="font-medium">Catatan Pembelian</span>
            </div>
            <div className="mt-4">
              {data.detail?.notes ? data.detail?.notes : 'Tidak ada catatan' }
            </div>
          </div>
        </div>
      </div>
    
    </div>
  )

  const renderContent = (content) => {
    switch (content) {
      case 1:
        return contentDibayar()
      case 2:
        return contentDiproses()
      case 3:
        return contentDikirim()
      case 4:
        return contentSelesai()
      case 5:
        return contentDikembalikan()
      default:
        return contentDibayar()
    }
  }

  const contentDibayar = () => (
    tableDatas.map((v, i) => (<Item data={v} />))
  )

  const contentDiproses = () => (
    tableDatas.map((v, i) => (<ItemProses key={v.id} data={v} callback={(id) => handleChangeResi(id)} action={(data, status) => doUpdateStatus(data, status)} callbackDetail={(data) => handleDetail(data)}/>))
  )
  
  const contentDikirim = () => (
    tableDatas.map((v, i) => (<ItemDikirim key={v.id} data={v} callback={(id) => handleChangeResi(id)} action={(data, status) => doUpdateStatus(data, status)} callbackDetail={(data) => handleDetail(data)}/>))
  )

  const contentSelesai = () => (
    tableDatas.map((v, i) => (<ItemSelesai key={v.id} data={v} callback={(id) => handleChangeResi(id)} action={(data, status) => doUpdateStatus(data, status)} callbackDetail={(data) => handleDetail(data)}/>))
  )
  
  const contentDikembalikan = () => (
    tableDatas.map((v, i) => (<ItemSelesai key={v.id} data={v} callback={(id) => handleChangeResi(id)} action={(data, status) => doUpdateStatus(data, status)} callbackDetail={(data) => handleDetail(data)}/>))
  )

  return (
    <BackOfficeBase>
      <Loading show={loading}/>
      <div className="w-full flex flex-wrap">

        <div className="w-full">
          <ContentHeader title="Orders" breadcumb={breadcumb} />
        </div>

        {/* <div className="mt-3 text-text-primary flex items-center">
          <img className="mr-1" src="/images/ic_download.png" alt=""/> Download File Transaksi
        </div> */}

        <div className="w-full">
          <motion.div
            animate={{ y: 10 }}
            transition={{ ease: "easeOut", duration: 1 }}
          >
            <div className="w-full bg-white shadow-sm p-4 mt-4 rounded flex flex-wrap">
              <div className="w-full flex">
                {
                  sliders.map(v => <SliderItem key={v.name} data={v} cbClick={handleChangeSlider}/>)
                }
                <div className="w-full border-b-2 border-gray-500"/>
              </div>
              <div className="w-full mt-4 flex">
                <div className="border border-gray-400 rounded flex items-center">
                  <img className="mx-3 my-1" src="/images/ic_search.png" alt="" />
                  <input className="border-0 shadow-none" type="text" placeholder="Search" value={keyword} onChange={(e) => setKeyword(e.target.value)}></input>
                </div>
                {/* <div className="ml-1 flex items-center h-full">
                  <div className="border border-gray-400 rounded-l flex items-center h-full">
                    <select className="py-1 px-5">
                      <option>Urutkan</option>
                    </select>
                  </div>
                  <div className="rounded-r px-3 py-1 bg-gray-500 h-full items-center flex">
                    <img src="/images/ic_arrow_down.png" alt=""/>
                  </div>
                </div> */}
                {
                  selectedContent == 2 && (
                    <div className="ml-auto flex justify-end">
                      <button className="px-4 py-2 bg-primary text-white rounded font-medium" onClick={handleSubmitAllResi}>Simpan Semua Resi</button>
                    </div>
                  )
                }
              </div>
              <div className="h-px bg-gray-500 w-full mt-4"></div>
              <div className="w-full mt-4 flex text-text-primary font-medium">
                <div className="w-full px-4">
                  <input className="mr-4" type="checkbox" />
                  Pilih Semua Transaksi
                </div>
                <div className="w-full px-4">
                  Pengiriman
                </div>
                <div className="w-full px-4">
                  Status
                </div>
                <div className="w-full px-4">
                  Transaksi
                </div>
              </div>
            </div>
            
            {/* {renderContent(selectedContent)} */}
            {
              filteredDatas.map(v => (
                <OrderItem
                  key={v.id} 
                  data={v} 
                  callback={(id) => handleChangeResi(id)} 
                  action={(data, status) => doUpdateStatus(data, status)} 
                  callbackDetail={(data) => handleDetail(data)}
                  callbackReload={() => doFetchOrders()}
                />
              ))
            }
          </motion.div>
        </div>

        <Modal open={modalDetailRefund} onClose={() => setModalDetailRefund(false)}>
            <div className="card-modal" style={{ padding: '16px 26px' }}>
              <div style={{ textAlign: 'right' }}>
                <div style={{ display: 'inline-block' }} onClick={() => setModalDetailRefund(false)} onKeyDown={() => setModalDetailRefund(false)} className="pointer" role="button" tabIndex={0}>
                  <img src="/images/ic_close_red.png" alt="" />
                </div>
              </div>
              <div className="title" style={{ marginTop: '16px' }}>
                Refund Pesanan
              </div>
              <div style={{ marginTop: '24px', display: 'flex' }}>
                <div className="w-100">
                  <div className="detail-order-wrapper">
                    <div className="flex xs:flex-wrap sm:flex-wrap">
                      <div className="td-small w-1/3 xs:w-1/2 sm:w-1/2">Nomor Pesanan : </div>
                      <div className="td-small w-2/3 xs:w-1/2 sm:w-1/2">{selectedDataRefund?.invoice_no}</div>
                    </div>
                    <div className="flex xs:flex-wrap sm:flex-wrap">
                      <div className="td-small w-1/3 xs:w-1/2 sm:w-1/2">Status Pesanan :</div>
                      <div className="td-small w-2/3 xs:w-1/2 sm:w-1/2">{selectedDataRefund?.status}</div>
                    </div>
                    <div className="flex xs:flex-wrap sm:flex-wrap">
                      <div className="td-small w-1/3 xs:w-1/2 sm:w-1/2">Tanggal Pesan :</div>
                      <div className="td-small w-2/3 xs:w-1/2 sm:w-1/2">{ moment(selectedDataRefund?.date).format('DD MMMM YYYY') }</div>
                    </div>
                    <div className="flex xs:flex-wrap sm:flex-wrap">
                      <div className="td-small w-1/3 xs:w-1/2 sm:w-1/2">Alamat :</div>
                      <div className="td-small w-2/3 xs:w-1/2 sm:w-1/2">{ selectedDataRefund?.address_detail.address }</div>
                    </div>
                    {/* <div className="flex xs:flex-wrap sm:flex-wrap">
                      <div className="td-small w-1/3 xs:w-1/2 sm:w-1/2">Pembayaran :</div>
                      <div className="td-small w-2/3 xs:w-1/2 sm:w-1/2">{ selectedDataRefund?.payment_method }</div>
                    </div>
                    <div className="flex xs:flex-wrap sm:flex-wrap">
                      <div className="td-small w-1/3 xs:w-1/2 sm:w-1/2">Nama Toko :</div>
                      <div className="td-small w-2/3 xs:w-1/2 sm:w-1/2">{ selectedDataRefund?.seller_name }</div>
                    </div> */}
                  </div>
                </div>
              </div>
              <div style={{ marginTop: '3rem' }}>
                <div className="table">
                  <div className="flex xs:flex-wrap sm:flex-wrap xs:hidden sm:hidden table-header">
                    <div className="text-white w-1/12">
                      <input type="checkbox" checked={refundCheckAll} onChange={(e) => handleRefundCheckAll()}/>
                    </div>
                    <div className="text-white w-1/4">Nama Produk</div>
                    <div className="text-white w-1/4">Harga</div>
                    <div className="text-white w-1/4">Jumlah</div>
                    <div className="text-white w-1/4">Subtotal</div>
                  </div>
                  <div className="order-product-table">
                    {
                      detailDataRefund?.order_items.map((v, i) => (
                        <div className="flex xs:flex-wrap sm:flex-wrap order-detail-item" key={v.product_name}>
                          <div className="td w-1/12 xs:w-1/2 sm:w-1/2">
                            <input type="checkbox" checked={v.checked} onChange={(e) => handleCheckedRefund(i)}/>
                          </div>
                          <div className="td w-1/4 xs:w-1/2 sm:w-1/2">{v.product_name}</div>
                          <div className="td w-1/4 xs:w-1/2 sm:w-1/2" style={{ whiteSpace: 'nowrap' }}>Rp {formatNumber(v.price)}</div>
                          <div className="td w-1/4 xs:w-1/2 sm:w-1/2" style={{ whiteSpace: 'nowrap' }}>Jumlah: {v.quantity}</div>
                          <div className="td w-1/4 xs:w-1/2 sm:w-1/2" style={{ whiteSpace: 'nowrap' }}>Rp {formatNumber(v.subtotal)}</div>
                        </div>
                      ))
                    }
                  </div>
                </div>
                <div className="divider" />
              </div>

              {/* <div className="flex-wrap" style={{ display: 'flex', justifyContent: 'center', marginTop: '1rem' }}>
                <button className="w-full py-2 px-2 rounded-md bg-white border-text-primary border " onClick={() => setRefundNotes(`Ingin mengubah rincian & membuat pesanan baru`)}>Ingin mengubah rincian & membuat pesanan baru</button>
                <button className="w-full py-2 px-2 rounded-md bg-white border-text-primary border mt-4" onClick={() => setRefundNotes('Lainnya/berubah pikiran')}>Lainnya/berubah pikiran</button>
              </div> */}

              {/* {
                refundNotes && ( */}
                  <div className="flex-wrap" style={{ display: 'flex', marginTop: '1rem' }}>
                    <div className="self-start">Alasan Refund :</div>
                    <br></br>
                    <textarea className="w-full p-2 border border-gray-400" rows={5} value={refundNotes} onChange={(e) => setRefundNotes(e.target.value)}>
                      
                    </textarea>
                  </div>
                {/* )
              } */}

              <div className="divider" style={{ margin: '1rem', width: 'auto' }} />

              
              <button
                type="submit"
                className="btn-primary"
                disabled={detailDataRefund?.order_items.filter(v => v.checked).length == 0 || refundNotes == ''}
                onClick={() => handleSubmitRefund()}
              >
                REFUND
              </button>
            </div>
          </Modal>
      </div>
      <style jsx>
        {
          `
        .c {
          width: 100%;
          display: flex;
          padding: 48px 0;
        }

        .c-filter {
          width: 100%;
        }
        .divider {
          width: 100%;
          margin-top: 12px;
          height: 1px;
          background: #E1E1E1;
        }

        .c-menu {
          font-weight: 500;
          font-size: 16px;
          line-height: 17px;
          letter-spacing: 0.05em;
          display: flex;
          justify-content: space-between;
          align-items: center;
          flex-wrap: wrap;
          padding: 10px 0px;
          cursor: pointer
        }

        .active {
          color: #010101;
        }

        .card-content {
          background: #FFFFFF;
          box-shadow: 0px 0px 4px rgba(0, 0, 0, 0.25);
          border-radius: 0px 7px 7px 7px;
          padding: 26px 32px;
        }

        .header {
          display: flex;
          justify-content: space-between;
        }

        .title {
          font-weight: 500;
          font-size: 22px;
          line-height: 21px;
          letter-spacing: 0.05em;
          color: #696969;
        }

        .btn-action {
          width: auto;
          font-size: 16px;
          padding: 10px 15px;
        }

        .btn-table {
          font-size: 16px;
          line-height: 15px;
          letter-spacing: 0.05em;
          color: #696969;
          background: #FFFFFF;
          border: 1px solid #696969;
          box-sizing: border-box;
          border-radius: 5px;
          padding: 12px 18px;
          cursor: pointer;
        }

        .sub-header {
          font-size: 16px;
          line-height: 15px;
          color: #868686;
        }

        .card-pin {
          background: #FFFFFF;
          box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.25);
          border-radius: 7px;
          display: flex;
          padding: 16px 24px;
          align-items: center;
        }

        .card-pin .text{
          font-size: 16px;
          line-height: 15px;
          color: #BDBDBD;
          padding: 0px 16px;
        }

        .btn-action-outline {
          background: #FFFFFF;
          border: 0.5px solid #010101;
          box-sizing: border-box;
          border-radius: 2px;
          font-size: 16px;
          line-height: 15px;
          color: #010101;
          padding: 12px 18px;
          cursor: pointer;
        }

        
          .c-filter-page {
            font-size: 16px;
            color: #696969;
          }
        
          .form-filter-page {
            background: rgba(248, 248, 248, 0.75);
            border: 1px solid #F2F2F2;
            box-sizing: border-box;
            padding: 4px 4px;
            font-size: 16px;
            color: #696969;
            margin: 0px 4px;
            font-family: 'Gotham';
          }
          .card-transaction {
            background: #FFFFFF;
            box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.15);
            border-radius: 7px;
          }
          .card-transaction .header {
            background: #FAFAFA;
            border-radius: 7px 7px 0px 0px;
            font-size: 16px;
            color: #696969;
            padding: 1rem;
          }
          .card-transaction .section {
            display: flex;
            font-size: 16px;
            color: #696969;
            padding: 1.5rem 2rem;
          }
          .td-small {
            padding: 0.2rem 1rem;
            border: 0px;
          }
          .td-med {
            padding: 0.5rem 1rem;
            border: 0px;
          }

          thead {
            background: #E8E8E8;
            border-radius: 7px;
          }
          .table-header div{
            background:#010101;
            padding:10px 15px;
          }

          th {
            padding: 1rem 1rem;
          }

          .lacak-header {
            display: flex;
            justify-content: space-between;
            align-items: center;
            padding: 1rem 1.5rem;
            background: #FFFFFF;
            box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.15);
            border-radius: 7px;
          }

          .lacak-sub-header {
            display: flex;
            font-size: 16px;
            line-height: 19px;
            letter-spacing: 0.05em;
            color: #696969;
          }
          .account-content{
            padding-left: 24px;
          }
          .order-detail-action .btn-action{
            background: #FF8242;
          }
          .order-detail-action button{
            margin-left: 5px;
          }
          .order-detail-action .btn-finish-transaction{
            background: #2FC142;
          }
          .btn-tracking {
            background-color: #3FAE5E;
          }
          .btn-tracking:disabled {
            background-color: #C4C4C4 !important;
          }
          @media (min-width: 320px) and (max-width: 767px) {
            .c{
              padding: 0 20px;
              margin-bottom: 30px;
            }
            .c-filter{
              margin-bottom:30px;
            }
            .account-content{
              padding-left: 0;
            }
            .order-detail-action .btn-action{
              margin-left:0;
              padding:10px 5px;
              margin-right:5px;
              width:100%;
            }
            .order-detail-action button{
              margin-top:15px;
              width:100%;
            }
            .transaction-menu-wrapper{
              width: auto;
              min-width:200%;
            }
            .overflow-tab{
              width:100%;
              overflow-x: auto;
              overflow: -moz-scrollbars-none;
              -ms-overflow-style: none;
            }
            .overflow-tab::-webkit-scrollbar {
              width: 0px;
              background: transparent;
            }
            .card-transaction .section{
              font-size:14px;
              padding:1rem;
            }
            .order-detail-item{
              border-top:1px solid #eee;
            }
            .td{
              padding:10px;
            }
            .td-small{
              padding:0.2rem 10px;
            }
            .{
              height:80px;
              overflow-y:auto;
            }
          }
        `
        }
      </style>
    </BackOfficeBase>
  )
}


dashboard.getInitialProps = async (ctx) => {
  const trx = typeof ctx.query.trx != 'undefined' ? ctx.query.trx : ''

  return {
    trx
  }
}
export default connect(
  (state) => state, {}
)(dashboard);
