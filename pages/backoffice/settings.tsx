import {useEffect, useState, useRef} from 'react'
import { connect } from 'react-redux'
import fetch from 'node-fetch'
import BackOfficeBase from 'components/BackOfficeBase'
import Modal from 'components/Modal'
import MapPicker from 'components/MapPicker'
import { uploadImage } from 'data/api/image'
import { updateAccount, TRequest, TRequestAddress, updateAddressAccount } from 'data/api/seller'
import Loading from 'components/Loading'
import { getShippingList, addShipping, deleteShipping } from 'data/api/shipping'
import { api } from 'data/api/base'
import { setUpUser } from 'redux/actions/authActions'
import { getProfile } from 'util/ServiceHelper';
import CropPicker from 'components/CropPicker'
import Select from 'react-select/async'
import { apiSearchAddress } from 'util/ServiceHelper'
import { compressImage } from 'util/helper'

type TProp = {
  shippingData: any,
  user: any,
  cloudinaryUrl: string,
  apiUrl: string,
  token: string,
  warning: boolean,
  approved: boolean,
  setUpUser: any
}

const dashboard = ({shippingData, user, cloudinaryUrl, apiUrl, token, warning, setUpUser, approved} : TProp) => {
  const logoRef = useRef(null)
  const bannerRef = useRef(null)
  const [loading, setLoading] = useState(false)
  const latLng = user.seller.address.lat_long.split(',')

  const [shippingList, setShippingList] = useState(shippingData.map(v => ({...v, isChecked: user.seller.shipment_method?.filter(k=>k.id===v.id).length > 0})))
  const [images, setImages] = useState([])
  const [logoImage, setLogoImage] = useState(user.seller.photo ? { base64:user.seller.photo, file: null } : null)
  const [bannerType, setBannerType] = useState('')
  const [bannerImage, setBannerImage] = useState(user.seller.url_banner ? { base64:user.seller.url_banner, file: null } : null)
  const [storeName, setStoreName] = useState(user.seller.store_name)
  const [storeDescription, setStoreDescription] = useState(user.seller.about_us)
  const [storePhone, setStorePhone] = useState(user.seller.phone)
  const [storeHours, setStoreHours] = useState(user.seller.operation_hours)
  const [address, setAddress] = useState(user.seller.address.address)
  const [districtId, setDistrictId] = useState({
    label: `${user.seller.address.province}, ${user.seller.address.city}, ${user.seller.address.district}`,
    value: user.seller.address.district_id
  })

  const [coordinate, setCoordinate] = useState({
    lat: latLng[0],
    lng: latLng[1],
    address: user.seller.address.address
  })
  const [modalMap, setModalMap] = useState(false)

  const [modalCrop, setModalCrop] = useState({
    image: '',
    open: false,
    cb: (props: any) => {},
    ratio: 1/1
  })

  useEffect(() => {
    console.log(shippingList)
  }, []);

  const doUpdateProfile = async() => {
    const res = await getProfile(token);
    setUpUser(res.data)
  }

  const handleSaveConfig = async () => {
    setLoading(true)
    let request : TRequest = {
      about_us: storeDescription,
      auto_chat: '',
      banner_type: bannerType,
      operation_hours: storeHours,
      phone: storePhone,
      store_name: storeName,
      photo: user.seller.photo,
      url_banner: user.seller.url_banner
    }

    try {

      // Upload Logo IF any
      if(logoImage.file) {
        const resLogo = await uploadImage(cloudinaryUrl, {
          file: logoImage.file,
          folder: 'logo',
          tag: ''
        })
        request.photo = resLogo.secure_url
      }

      //Upload Banner IF any
      if(bannerImage?.file) {
        const resBanner = await uploadImage(cloudinaryUrl, {
          file: bannerImage.file,
          folder: 'banner',
          tag: ''
        })
        request.url_banner = resBanner.secure_url
      }

      // Update Account
      const res = await updateAccount(apiUrl, token, request)

      const reqAddress: TRequestAddress = {
        address: address,
        district_id: districtId.value,
        id: user.seller.address.id,
        postal_code: user.seller.address.postal_code,
        lat_long: `${coordinate.lat},${coordinate.lng}`
      }
      const resAddress = await updateAddressAccount(apiUrl, token, reqAddress)

      doUpdateProfile()
    } catch (err) {
      // TODO HANDLE ERR
    }

    setLoading(false)
  }

  const getBase64 = (file, cb) =>{
    let reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = function () {
        cb(reader.result)
    }
    reader.onerror = function (error) {
        console.log('Error: ', error)
    }
  }

  const handleChangeLogoImage = (e) => {
    const file = e.target.files[0]
    getBase64(file, (result) => {
      // setLogoImage({base64: result, file: file})
      setModalCrop({
        image: result,
        open: true,
        cb: (props : any) => setLogoImage({base64: props.croppedImage, file: props.blobCompressed}),
        ratio: 1/1
      })
    });
  }

  const handleChangeBannerImage = (e) => {
    const file = e.target.files[0]
    getBase64(file, (result) => {
      // setBannerImage({base64: result, file: file})
      setModalCrop({
        image: result,
        open: true,
        cb: (props : any) => setBannerImage({base64: props.croppedImage, file: props.blobCompressed}),
        ratio: 3/1
      })
    });
  }

  const handleChangeShipping = (id) => async (e) => {
    setShippingList(shippingList.map(v => ({...v, isChecked: v.id === id ? !v.isChecked : v.isChecked})))

    if(e.target.checked) {
      // Do insert
      const res = await addShipping(apiUrl, token, id)
      doUpdateProfile()
    } else {
      // Do delete
      const res = await deleteShipping(apiUrl, token, id)
      doUpdateProfile()
    }
  }

  const handleSubmitMap = (coor, address) => {
    setModalMap(false)
    setCoordinate({...coor, address: address})
  }

  const handleCroppedImage = async (croppedImage, cb) => {
    setLoading(true)
    let blob = await fetch(croppedImage).then(r => r.blob());
    const blobCompressed = await compressImage(blob, 1)
    modalCrop.cb({croppedImage, blobCompressed})
    setModalCrop({
      ...modalCrop,
      open: false
    })
    setLoading(false)
  }

  const loadOptions = (inputValue, callback) => {
    if (inputValue.length > 3) {
      setTimeout(() => {
        apiSearchAddress(inputValue)
          .then((res) => {
            callback(res.data.map((v) => ({
              label: v.result,
              value: v.id_district
            })));
          }).catch(() => callback([]));
      }, 500);
    } else {
      callback([]);
    }
  };

  return (
    <BackOfficeBase>
      <Loading show={loading}/>
      <div className="w-full flex flex-wrap">
        <div className="w-full flex">
          <div className="w-full font-medium text-2xl text-text-primary">
            Seller's Page
          </div>
          <div className="w-full">
            <div className="flex justify-end">
              <button className="bg-green-500 text-white font-bold px-5 py-1 mx-1 rounded" onClick={handleSaveConfig}>SAVE CONFIG</button>
            </div>
          </div>
        </div>

        {
          approved && (
            <div className="w-full border border-primary rounded mt-4 p-4 text-center bg-white text-primary font-bold">
              Akun anda akan segera diaktifkan oleh tim Lakkon, <a className="text-primary-dark" href="https://bapera.id">Hubungi disini</a>
            </div>
          )
        }

        {
          warning && (
            <div className="w-full border border-primary rounded mt-4 p-4 text-center bg-white text-primary font-bold">
              Harap Lengkapi Data Seller!
            </div>
          )
        }

        <div className="w-full text-text-primary">
          <div className="bg-white shadow-lg py-12 mt-4 rounded-lg flex justify-center flex-wrap">
            <div className="w-1/2">

              <div id="logo" className="flex">
                <label className="w-1/2 text-right">
                  Logo<span className="text-primary">*</span>
                  <br></br>
                  <span className="text-primary text-xs">Minimal resolution 300px X 300px</span>
                </label>
                <div className="w-full pl-5">
                  <div className="border-2 border-gray-200 w-10 h-10">
                    <img className="w-full h-full object-cover" src={logoImage ? logoImage.base64 : 'https://via.placeholder.com/40x40?text=No%20Image'} alt="logo image"/>
                  </div>
                  <div className="text-sm ">
                    <button className="border-2 bg-white px-3 rounded-lg mt-2" onClick={() => {logoRef.current.click()}} >Browse...</button>
                    {`${logoImage ? '' : ' No File Selected'}`}
                    <input ref={logoRef} className="hidden" type="file" name="logo" onChange={handleChangeLogoImage}/>
                  </div>
                  {/* <input type="checkbox" name="delete" id="delete" className="mt-2"/> Delete Image */}
                </div>
              </div>

              {/* <div id="bannerType" className="flex mt-8">
                <label className="w-1/2 text-right">
                  Banner Type<span className="text-primary">*</span>
                </label>
                <div className="w-full pl-5 flex">
                  <select className="w-full border border-gray-300 px-4 py-2" value={bannerType} onChange={(e) => setBannerType(e.target.value)}>
                    <option value="Static Image">Static Image</option>
                  </select>
                  <div className="">
                    <div className="rounded-r px-3 py-1 bg-gray-300 items-center flex h-full">
                      <img src="/images/ic_arrow_down.png" alt=""/>
                    </div>
                  </div>
                </div>
              </div> */}

              <div id="banner" className="flex mt-8">
                <label className="w-1/2 text-right">
                  Banner<span className="text-primary">*</span>
                  <br></br>
                  <span className="text-primary text-xs">Minimal resolution 1440px X 480px</span>
                </label>
                <div className="w-full pl-5">
                  <div className="border-2 border-gray-200" style={{ width: '300px', height: '100px' }}>
                    <img className="w-full h-full object-cover" src={bannerImage ? bannerImage.base64 : 'https://via.placeholder.com/300x100?text=No%20Image'} alt="logo image"/>
                  </div>
                  <div className="text-sm ">
                    <button className="border-2 bg-white px-3 rounded-lg mt-2" onClick={() => {bannerRef.current.click()}} >Browse...</button>
                    {`${bannerImage ? '' : ' No File Selected'}`}
                    <input ref={bannerRef} className="hidden" type="file" name="banner" onChange={handleChangeBannerImage}/>
                  </div>
                  {/* <div className="mt-1 text-sm">
                    Choose banner type: static or banner slider
                  </div> */}
                  <div className="text-sm">
                    Allowed file types: PNG, GIF, JPG, JPEG
                  </div>
                </div>
              </div>

              <div id="storeName" className="flex mt-8">
                <label className="w-1/2 text-right">Store Name<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <input type="text" className="border border-gray-300 px-4 py-2 w-full" placeholder="Store Name" value={storeName} onChange={(e) => setStoreName(e.target.value)}/>
                </div>
              </div>

              <div id="storeDescription" className="flex mt-8">
                <label className="w-1/2 text-right">Store Description<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <textarea className="border border-gray-300 px-4 py-2 focus:outline-none w-full rounded" value={storeDescription} onChange={(e) => setStoreDescription(e.target.value)}></textarea>
                </div>
              </div>

              <div className="flex mt-8">
                <label className="w-1/2 text-right">Store Phone Number<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <input type="text" className="border border-gray-300 px-4 py-2 w-full" placeholder="62" value={storePhone} onChange={(e) => setStorePhone(e.target.value)}/>
                </div>
              </div>

              <div className="flex mt-8">
                <label className="w-1/2 text-right">Store Hours of Operation<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <input type="text" className="border border-gray-300 px-4 py-2 w-full" placeholder="00.00 to 24.00" value={storeHours} onChange={(e) => setStoreHours(e.target.value)}/>
                </div>
              </div>

              <div id="district" className="flex mt-8">
                <label className="w-1/2 text-right">City / Subdistrict<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <Select
                    styles={{
                      control: (provided) => ({
                        ...provided,
                        background: '#FFFFFF',
                        boxShadow: '0px 1px 4px rgba(0, 0, 0, 0.25)',
                        borderRadius: '7px',
                        padding: '20px 18px',
                        border: '0px'
                      }),
                      valueContainer: (provided) => ({
                        ...provided,
                        padding: '0px'
                      }),
                      menu: (provided) => ({
                        ...provided,
                        padding: 'px'
                      }),
                      indicatorsContainer: (provider) => ({
                        ...provider,
                        display: 'none'
                      })
                    }}
                    placeholder="Kota / Kecamatan"
                    loadOptions={loadOptions}
                    cacheOptions
                    defaultOptions
                    value={districtId}
                    onChange={(value) => setDistrictId(value)}
                  />
                </div>
              </div>

              <div id="storeDescription" className="flex mt-8">
                <label className="w-1/2 text-right">Address<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <textarea className="border border-gray-300 px-4 py-2 focus:outline-none w-full rounded" value={address} onChange={(e) => setAddress(e.target.value)}></textarea>
                </div>
              </div>

              <div id="storeDescription" className="flex mt-8">
                <label className="w-1/2 text-right">
                  Pin Location<span className="text-primary">*</span>
                  <br></br>
                  <span className="text-primary text-xs">Location for Go-Send</span>
                </label>
                <div className="w-full pl-5">
                  <div className="w-full flex rounded items-center shadow p-4">
                    <div>
                      <img src="/images/ic_map_red.png" alt="" />
                    </div>
                    <div className="text-sm text-text-primary ml-4" style={{ width: '320px' }}>
                      {coordinate ? coordinate.address : 'Tandai lokasi untuk alamat yang lebih tepat' }
                    </div>
                    <div className="border border-primary text-primary px-4 py-2 rounded cursor-pointer" onClick={() => setModalMap(true)}>
                      Tandai Lokasi
                    </div>
                  </div>
                </div>
              </div>

              <div className="flex mt-8">
                <label className="w-1/2 text-right">Shipment Methods<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  {
                    shippingList.map(v => (
                      <div key={v.id}>
                        <input type="checkbox" name="shipping" value={v.id} checked={v.isChecked} onChange={(e) => handleChangeShipping(v.id)(e)}/> 
                        {
                          v.name == 'COD' 
                          ?  (
                            <>
                              {v.name}
                              <span className="text-gray-500 text-sm"> (Barang diambil saat event!)</span>
                            </>
                          )
                          : v.name
                        }
                      </div>
                    ))
                  }
                </div>
              </div>
          
            </div>

          </div>

          <Modal open={modalCrop.open} onClose={() => setModalCrop({...modalCrop, open: false})}>
            <CropPicker 
              ratio={modalCrop.ratio} 
              onClosed={() => setModalCrop({...modalCrop, open: false})} 
              onSubmit={(croppedImage) => handleCroppedImage(croppedImage, modalCrop.cb)} 
              image={modalCrop.image}
              onCrop={() => setLoading(true)}
              onCropped={() => setLoading(false)}
            />
          </Modal>

          <Modal open={modalMap} onClose={() => setModalMap(false)}>
            {
              modalMap && (
                <MapPicker 
                  onClose={() => setModalMap(false)} 
                  onSubmit={(coor, address) => handleSubmitMap(coor, address)}
                  defaultCenter={{
                    lat: coordinate.lat,
                    lng: coordinate.lng
                  }}
                  reverseGeolocation={null}
                /> 
              )
            }
          </Modal>

        </div>
      </div>
    </BackOfficeBase>
  )
}

dashboard.getInitialProps = async (ctx) => {
  const { query } = ctx
  const { token, endpoint, user } = ctx.store.getState().authentication
  const resShippingList = await getShippingList(endpoint.apiUrl, token)

  return {
      shippingData: resShippingList.data,
      user: user,
      cloudinaryUrl: endpoint.cloudinaryUrl,
      apiUrl: endpoint.apiUrl,
      token: token,
      warning: query.warning ? true : false,
      approved: query.approved ? true : false,
  }
}

export default connect(
  (state) => state, 
  {setUpUser}
)(dashboard);
