import React, { useState } from 'react';
import Head from 'next/head';
import Link from 'next/link';

export default function Home() {
  const [step, setStep] = useState(1);

  return (
    <div className="container">
      <Head>
        <title>Lakkon</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className="container">
        <div className="w-100 text-center">
          <img src="/images/logo_bapera.png" alt="" className="h-16"/>
        </div>

        <div className="container-login">
          <div className="text-primary">Daftar Seller</div>

          <div style={{ marginTop: '50px' }}>
            <input className="form-control w-100" placeholder="Nama Toko" />
          </div>
          <div style={{ marginTop: '20px' }}>
            <input className="form-control w-100" placeholder="Nomor NIK" />
          </div>
          <div style={{ display: 'flex', marginTop: '20px' }}>
            <div style={{ width: '75%', paddingRight: '10px' }}>
              <input className="form-control w-100" placeholder="Kota / Kecamatan" />
            </div>
            <div style={{ width: '35%' }}>
              <input className="form-control w-100" placeholder="Kode Pos" />
            </div>
          </div>

          <div style={{ marginTop: '38px;' }}>
            <button className="btn-primary">Selesai</button>
          </div>

          <hr style={{ margin: '30px 10px 10px 10px' }} />

          <div className="text-center" style={{ marginTop: '30px' }}>
            <span className="text-register">Sudah punya akun?</span>
            <Link href="/signin">
              <span className="text-register-action"> Masuk</span>
            </Link>
          </div>

          <div className="text-center" style={{ marginTop: '20px' }}>
            <span className="text-secondary">
Dengan mendaftar, saya menyetujui
              {' '}
              <br />
Syarat dan Ketentuan serta Kebijakan Privasi.
            </span>
          </div>

        </div>
      </div>


      <style jsx>
        {`
        .container {
          width: 100%;
          height: 100vh;
          display: flex;
          align-items: center;
          flex-wrap: wrap;
          justify-content: center;
        }

        .container-login {
          min-width: 568px;
          min-height: 596px;
          background: #FFFFFF;
          box-shadow: 0px 1px 12px rgba(0, 0, 0, 0.25);
          border-radius: 10px;
          padding: 2.5rem 2.5rem;
        }

        .break {
          flex-basis: 100%;
          height: 0;
        }

        .text-forgot-password {
          font-size: 12px;
          line-height: 14px;
          color: #6F0C06;
          margin-top: 20px;
          text-align: right;
        }

        .container-social-media {
          display: flex;
          justify-content: space-evenly;
          margin-top: 30px;
        }

        .text-register {
          font-size: 16px;
          line-height: 19px;
          color: #AFAFAF;
        }

        .text-register-action {
          font-weight: bold;
          font-size: 16px;
          line-height: 19px;
          color: #730C07;
          cursor: pointer;
        }
      `}

      </style>
    </div>
  );
}
