import React, { useState, useEffect, useRef } from 'react';
import { useRouter } from "next/router";
import PropTypes from 'prop-types';
import InfiniteScroll from 'react-infinite-scroll-component';
import BaseLayout from '~/components/BaseLayout';
import FilterPrice from '~/components/FilterPrice';
import FilterAttributes from '~/components/FilterAttributes';
import CardProduct from '~/components/CardProduct';
import { getProductType } from '~/data/api/product-type';
import Modal from '~/components/Modal';
import ReactPlaceholder from 'react-placeholder';

import { getCart } from '~/data/api/cart';

// Axios API Request
import axiosSetup from '../../services/api';

const Category = ({ slug, products, filters, token, productType, cart, origin, species, processing, tasted }) => {
  const listAttribute = [];

  let initialFilter = [];
  if(origin && species && processing && tasted){
    initialFilter = [
      {
        attribute: "Origin",
        value: [origin]
      },
      {
        attribute: "Bean Species",
        value: [species]
      },
      {
        attribute: "Processing",
        value: [processing]
      },
      {
        attribute: "Tasted",
        value: [tasted]
      }
    ]
  }

  const [sort, setSort] = useState('created_at,DESC');
  const [valuesFilter, setValuesFilter] = useState(initialFilter);
  const [limit, setLimit] = useState(12);
  const [page, setPage] = useState(1);
  const [listProduct, setListProduct] = useState(products)
  const [loadMore, setLoadMore] = useState(true);
  const [modalFilter, setModalFilter] = useState(false);
  const [priceRange, setPriceRange] = useState({
    min: 0,
    max: 1000000
  })
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setLoading(false);
    }, 2000);
  },[]);

  const handleSort = (event) => {
    setLoading(true);
    setSort(event.currentTarget.value);
    loadFilteredProduct(valuesFilter, priceRange, event.currentTarget.value, limit);
    setLoadMore(true);
  }

  const handleLimit = (event) => {
    setLimit(event.currentTarget.value);
    loadFilteredProduct(valuesFilter, priceRange, sort, event.currentTarget.value);
    setLoadMore(true);
  }

  const handleChangeSlider = (min, max) => {
    console.log(min + " - " + max);
    setPriceRange({
      min: min,
      max: max
    })
    setTimeout(() => {
      loadFilteredProduct(valuesFilter, {min, max}, sort, limit);
    }, 1000)
    setLoadMore(true);
  }

  const handleFilter = (value, checked) => {
    let updatedFilter = [...valuesFilter];
    if(!checked){
      updatedFilter.map(item => {
        if(item.attribute === value.attribute){
          const removedIndex = updatedFilter.indexOf(value.value);
          item.value.splice(removedIndex, 1);
        }
      });
    } else {
      let existAttribute = 0;
      updatedFilter.map(item => {
        if(item.attribute === value.attribute){
          existAttribute += 1;
          item.value.push(value.value);
        }
      });

      if(existAttribute <= 0){
        updatedFilter.push({
          attribute: value.attribute,
          value: [value.value]
        });
      }
    }
    setValuesFilter(updatedFilter);
    loadFilteredProduct(updatedFilter, priceRange, sort, limit);
    setLoadMore(true);
  }

  const hanldeLoadMoreProduct = () => {
    setPage(page + 1);
    const api = axiosSetup(token);
    let valueFilterString = '';
    valuesFilter.map(item => {  
      let attributeName;
      if(item.value.length > 0){
        if(item.attribute.toLowerCase() === "bean species"){
          attributeName = "species";
        } else {
          attributeName = item.attribute.toLowerCase()
        }
        valueFilterString += `&${attributeName}=${item.value.join(",")}`;
      }
    });
    setTimeout(() => {
      const productParam = `layout_type=list_layout&type_slug=${slug}&status=active${valueFilterString}&page=${page + 1}&limit=${limit}&order=${sort}&price=${priceRange.min},${priceRange.max}`;
      api.get('/products?' + productParam).then((response) => {
        if(response.data.data){
          setListProduct(listProduct.concat(response.data.data));
        } else {
          setLoadMore(false);
        }
      });
    }, 500);
  }

  const loadFilteredProduct = (updatedFilter, updatedPrice, updatedSort, updatedLimit) => {
    setLoading(true);
    const api = axiosSetup(token);
    let valueFilterString = '';
    updatedFilter.map(item => {  
      let attributeName;
      if(item.value.length > 0){
        if(item.attribute.toLowerCase() === "bean species"){
          attributeName = "species";
        } else {
          attributeName = item.attribute.toLowerCase()
        }
        valueFilterString += `&${attributeName}=${item.value.join(",")}`;
      }
    });
    setPage(1);
    const productParam = `layout_type=list_layout&type_slug=${slug}&status=active${valueFilterString}&page=${page}&limit=${updatedLimit}&order=${updatedSort}&price=${updatedPrice.min},${updatedPrice.max}`;
    api.get('/products?' + productParam).then((response) => {
      setListProduct(response.data.data);
      setLoading(false)
    });
  }

  if(filters){
    filters.map(attribute => {
      const attributeBlock = (
        <FilterAttributes 
          title={attribute.name}
          data={attribute.values}
          handleFilter={handleFilter}
          valuesFilter={valuesFilter}
          key={attribute.id}
        />
      );
      listAttribute.push(attributeBlock);
    });
  }

  return (
    <div>
      <BaseLayout productType={productType} token={token} cart={cart}>
        <div id="breadcumb" className="breadcrumb-wrapper">
          <div className="breadcumb">
            Home &#x0226B; Produk &#x0226B;
            {' '}
            <span style={{ color: '#010101' }}>{productType.find(v => v.slug == slug).name}</span>
          </div>
        </div>

        <div className="c-content flex flex-wrap product-content">
          <div className="w-1/5 xs:w-full sm:w-full md:w-1/4 lg:w-1/5">
            <div className="mobile-filter hidden xs:flex sm:flex md:hidden lg:hidden">
              <button className="mobile-filter-button btn-default" onClick={() => setModalFilter(true)}>Filter</button>
            </div>
            <div className="c-filter xs:hidden sm:hidden md:flex lg:flex">
              <span className="w-100 title-filter">Urutkan Berdasarkan</span>
              <FilterPrice callbackSlider={handleChangeSlider} priceRange={priceRange} />
              {listAttribute}
            </div>
          </div>
          <div className="w-4/5 xs:w-full sm:w-full md:w-4/5 lg:w-4/5">
            <div className="c-products">
              <div className="c-filter-product">
                <div className="c-filter-page flex items-center">
                  Menampilkan
                  <select onChange={handleLimit} className="form-filter-page">
                    <option value="12">12</option>
                    <option value="24">24</option>
                    <option value="36">36</option>
                  </select>
                  dari <span>&nbsp;{listProduct ? listProduct.length : 0}</span>
                </div>
                <div className="c-filter-page flex items-center">
                  Urutkan
                  <select onChange={handleSort} className="form-filter-page">
                    <option value="date,DESC">Terbaru</option>
                    <option value="product_name,ASC">Nama Produk</option>
                    <option value="popular,DESC">Popularitas</option>
                    <option value="price,ASC">Harga Terendah</option>
                    <option value="price,DESC">Harga Tertinggi</option>
                  </select>
                </div>
              </div>

              <div className="c-content-products">
                <InfiniteScroll
                  dataLength={listProduct ? listProduct.length : 0}
                  next={hanldeLoadMoreProduct}
                  hasMore={loadMore}
                  className="flex flex-wrap w-full"
                >
                  {
                    listProduct ? listProduct.map((v) => (
                      <CardProduct data={v} loading={loading} key={v.id}/>
                    )) : <h4 style={{marginTop: "30px", marginBottom:"30px", textAlign:"center", fontSize:"20px", width: "100%"}}>Produk yang anda cari tidak ada</h4>
                  }
                </InfiniteScroll>
              </div>
            </div>
          </div>
        </div>
        
        <Modal open={modalFilter} onClose={() => setModalFilter(false)}>
          <div className="card-modal card-modal-filter">
            <div style={{ textAlign: 'right' }}>
              <div style={{ display: 'inline-block' }} onClick={() => setModalFilter(false)} onKeyDown={() => setModalFilter(false)} className="pointer" role="button" tabIndex={0}>
                <img src="/images/ic_close_red.png" alt="" />
              </div>
            </div>
            <div className="c-filter">
              <FilterPrice />
              {listAttribute}
            </div>
          </div>
        </Modal>

      </BaseLayout>
      <style jsx>
        {`
        .breadcrumb-wrapper{
          padding: 20px 0;
        }
        .breadcumb {
          font-size: 14px;
          color: #696969;
        }
      
        .c-content {
          width: 100%;
          display: flex;
        }
      
        .c-filter {
          width: 100%;
          flex-wrap:wrap;
          justify-content: flex-start;
          flex-direction: column;
        }
      
        .title-filter {
          font-weight: 500;
          font-size: 18px;
          color: #696969;
        }
      
        .c-products {
          width: 100%;
          padding: 0px 0px 0px 24px;
        }
      
        .c-filter-product {
          width: 100%;
          display: flex;
          justify-content: space-between;
        }
      
        .c-filter-page {
          font-size: 14px;
          color: #696969;
        }
      
        .form-filter-page {
          background: rgba(248, 248, 248, 0.75);
          border: 1px solid #F2F2F2;
          box-sizing: border-box;
          padding: 4px 4px;
          font-size: 16px;
          color: #696969;
          margin: 0px 4px;
          font-family: 'Gotham';
        }
      
        .c-content-products {
          display: flex;
          flex-wrap: wrap;
          justify-content: space-between;
        }
        .c-content-products > div:first-child{
          width:100%;
        }
        .mobile-filter-button{
          padding:10px 15px;
          border:1px solid #ddd;
          border-radius:10px;
          margin:0 auto;
          display:block;
        }
        @media (min-width: 768px) and (max-width: 1024px) {
        }

        @media (min-width: 0) and (max-width:372px){
          .c-filter-page{
            font-size:12px;
          }
          .c-filter-page .form-filter-page{
            font-size:14px;
          }
        }
        
        @media (min-width: 320px) and (max-width: 767px) {
          .product-content{
            padding:0 20px;
          }
          .c-products{
            padding:0;
            margin-top:30px;
          }
          .card-modal-filter{
            padding:20px;
            height:100%;
        }
        .card-modal-filter .c-filter{
            padding:0 20px;
            height:95%;
            overflow-y: auto;
        }
          .card-modal-filter .c-filter{
            padding:0 20px;
          }
          .breadcrumb-wrapper{
            padding: 0 20px;
            text-align:center;
            margin-bottom: 20px;
          }
          .form-filter-page{
              padding:4px 0;
              font-size: 12px;
          }
          .c-filter-page{
              font-size: 12px
          }
        }
        `}
      </style>
    </div>
  );
};

Category.propTypes = {
  slug: PropTypes.string,
  products: PropTypes.array,
  filters: PropTypes.array,
  token: PropTypes.string
};

Category.getInitialProps = async (ctx) => {
  const { slug, origin, species, processing, tasted } = ctx.query;
  const { token } = ctx.store.getState().authentication;

  const api = axiosSetup(token);

  let filterQuery = ''
  if(origin || species || processing || tasted){
    filterQuery = `&origin=${origin}&species=${species}&processing=${processing}&tasted=${tasted}`
  }

  const productParam = `layout_type=list_layout&type_slug=${slug}&status=active${filterQuery}&page=1&limit=12&order=date,DESC`;
  const productResp = await api.get('/products?' + productParam);
  const products = productResp.data.data;

  console.log(JSON.stringify(products))

  const attributeResp = await api.get(`/product_types_attributes?slug=${slug}`);
  const filters = attributeResp.data.data;

  const productTypeResp = await getProductType();
  const productType = productTypeResp.data;

  const cartResp = await getCart(token);
  const cart = cartResp.data ? cartResp.data.cart : [];

  return {
    slug,
    products,
    filters,
    productType,
    token,
    cart,
    origin, species, processing, tasted
  };
};

export default Category;
