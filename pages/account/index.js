import React, { useState, useRef } from 'react';
import Router from 'next/router';
import Link from 'next/link';
import {connect} from 'react-redux'
import BaseLayout from '~/components/BaseLayout';
import Modal from '~/components/Modal';
import moment from 'moment';
import { getProductType } from '~/data/api/product-type';
import { getUser, getBalance, postVerificationRequest, postVerificationValidation, updateUsers, updateUserPassword, updateUserEmailPhone } from '~/data/api/user';
import { setCookie } from '~/util/helper';
import { requestOTP, validationOTP } from '~/data/api/otp';
import Loading from '~/components/Loading/';
import { getCart } from '~/data/api/cart';
import { formatNumber } from '~/util/helper';
import Rekening from '~/components/Rekening'

// Toastr Notification
import {successNotification, errorNotification} from '~/util/toastr-notif';

const index = ({productType, token, user, authentication, cart, Tbalance}) => {
  const {apiUrl} = authentication?.endpoint
  const [modalPersonal, setModalPersonal] = useState(false);
  const [modalKontak, setModalKontak] = useState(false);
  const [modalKeamanan, setModalKeamanan] = useState(false);
  const [modalOtp, setModalOtp] = useState(false);
  const [userData, setUserData] = useState(user);
  const [loading, setLoading] = useState(false);
  const [formPersonal, setFormPersonal] = useState({
    firstName: userData.name.split(" ")[0],
    lastName: userData.name.split(" ")[1],
  });

  const [otpType, setOtpType] = useState('');
  const [otpm, setOtpm] = useState(null)
  const [balance, setBalance] = useState(Tbalance)

  const [formPassword, setFormPassword] = useState({
    oldPassword: '',
    newPassword: '',
    confirmPassword: ''
  });

  const [form, setForm] = useState({
    otp: {
      first: '',
      second: '',
      third: '',
      fourth: ''
    }
  });

  const firstOtpRef = useRef(null);
  const secondOtpRef = useRef(null);
  const thirdOtpRef = useRef(null);
  const fourthOtpRef = useRef(null);

  const [formEmailPhone, setFormEmailPhone] = useState({
    email: userData.email,
    phone: userData.phone,
    password: ''
  });

  const refreshBalance = async () => {
    const balanceResp = await getBalance(token);
    console.log('called')
    setBalance(balanceResp.data.detail)
  }

  const reloadUserData = async () => {
    const res = await getUser(token);
    if(res.code == 200){
      setUserData(res.data);
    } else {
      errorNotification(res.message);
    }
  }

  const handleChangeFormPersonal = (name) => (e) => {
    const { value } = e.target;
    setFormPersonal({ ...formPersonal, [name]: value });
  };

  const handleSubmitPersonal = async (e) => {
    e.preventDefault();
    const body = {
      name: formPersonal.firstName + " " + formPersonal.lastName,
    }

    const res = await updateUsers(token, body);
    if(res.code == 200){
      successNotification("Data berhasil diupdate");
      await reloadUserData();
      setModalPersonal(false);
    } else {
      errorNotification(res.message);
    }
  }

  const handleChangeFormPassword = (name) => (e) => {
    const { value } = e.target;
    setFormPassword({ ...formPassword, [name]: value });
  }

  const submitChangePassword = async (e) => {
    e.preventDefault();
    if(formPassword.newPassword !== formPassword.confirmPassword){
      errorNotification("Konfirmasi password tidak sama dengan password baru");
    } else {
      const body = {
        old_password: formPassword.oldPassword,
        new_password: formPassword.newPassword
      }
      const res = await updateUserPassword(token, body);
      if(res.code == 200){
        successNotification("Password berhasil dirubah");
        setFormPassword({
          oldPassword: '',
          newPassword: '',
          confirmPassword: ''
        })
        setModalKeamanan(false);
      } else {
        errorNotification(res.message);
      }
    }
  }

  const handleChangeFormEmailPhone = (name) => (e) => {
    const { value } = e.target;
    setFormEmailPhone({ ...formEmailPhone, [name]: value });
  }

  const submitChangeEmailPhone = async (e) => {
    e.preventDefault();
    const oldPhone = userData.phone;
    // if(oldPhone !== formEmailPhone.phone){
    //   const otpRes = await requestOTP(apiUrl, formEmailPhone.phone, '', '');
    //   console.log(otpRes)
    //   if(otpRes.code == 200){
    //     setOtpType("phone");
    //     setModalOtp(true);
    //     setOtpm(otpRes.data);
    //   }
    // } else {
      const res = await updateUserEmailPhone(token, formEmailPhone);
      if(res.code == 200){
        successNotification("Data berhasil diupdate");
        setModalKontak(false);
        localStorage.setItem('user', JSON.stringify(res.data));
        setCookie('token', token);
        reloadUserData();
        setFormEmailPhone({
          email: userData.email,
          phone: userData.phone,
          password: ''
        })
      } else {
        errorNotification(res.message);
      }
    // }
  }

  const validatePhoneOtp = async () => {
    const {first, second, third, fourth} = form.otp
    const res = await validationOTP(apiUrl, otpm.request_id, `${first}${second}${third}${fourth}`) 
    if(res.code == 200){
      const res = await updateUserEmailPhone(token, formEmailPhone);
      if(res.code == 200){
        successNotification("Data berhasil diupdate");
        setModalOtp(false);
        setModalKontak(false);
        localStorage.setItem('user', JSON.stringify(res.data));
        setCookie('token', token);
        reloadUserData();
        setFormEmailPhone({
          email: userData.email,
          phone: userData.phone,
          password: ''
        })
      } else {
        errorNotification(res.message);
      }
    } else {
      errorNotification(res.message);
    }
  }

  const handleEmailVerification = async () => {
    setLoading(true);
    const body = {
      email: userData.email
    }
    const res = await postVerificationRequest(token, body);
    if(res.code == 200){
      firstOtpRef.current.focus();
      setLoading(false);
      setOtpType("email");
      setModalOtp(true);
    } else {
      setLoading(false);
      console.log(error.response);
      errorNotification(error.response ? error.response.data.message : error.response);
    }
  }

  const handleKeyOtp = (name) => (e) => {
    setForm({...form, otp: {...form.otp, [name]: e.target.value}})
    
    if(name === "first" && e.target.value.length == 1){
      secondOtpRef.current.focus();
    }
    if(name === "second" && e.target.value.length == 1){
      thirdOtpRef.current.focus();
    }
    if(name === "third" && e.target.value.length == 1){
      fourthOtpRef.current.focus();
    }
  }

  const handleAfterOtp = async () => {
    const {first, second, third, fourth} = form.otp
    if(otpType === "email"){
      const body = {
        email: userData.email,
        token_verification: `${first}${second}${third}${fourth}`
      }
      const res = await postVerificationValidation(token, body); 
      if(res.code == 200){
        successNotification("Verifikasi akun email berhasil");
        setModalOtp(false);
      } else {
        console.log(res);
        errorNotification("Kode yg anda masukan salah");
      }
    } else {
      validatePhoneOtp();
    }
  }

  const handleResendEmailVerification = async () => {
    setLoading(true);
    const body = {
      email: userData.email
    }
    const res = await postVerificationRequest(token, body);
    setLoading(false);
    if(res.code == 200){
      successNotification("Kode otp telah dikirim ulang");
    } else {
      console.log(error.response);
      errorNotification(error.response ? error.response.data.message : error.response);
    }
  }

  return (
    <div>
      <Loading show={loading} />
      <BaseLayout productType={productType} token={token} cart={cart}>
        <div className="c flex flex-wrap">
          <div className="w-1/6 xs:w-full sm:w-full">
            <div className="c-filter">
              <Link href="/account">
                <div className="c-menu active">
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <img src="/images/ic_account_red.png" alt="" />
                    <span style={{ marginLeft: '16px' }}>Detail Akun</span>
                  </div>
                  <img src="/images/ic_arrow_right_red.png" alt="" />
                  <div className="divider" />
                </div>
              </Link>

              <Link href="/address">
                <div className="c-menu">
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <img src="/images/ic_map.png" alt="" />
                    <span style={{ marginLeft: '16px' }}>Daftar Alamat</span>
                  </div>
                  <img src="/images/ic_arrow_right.png" alt="" />
                  <div className="divider" />
                </div>
              </Link>
              <Link href="/transaction-history">
                <div className="c-menu">
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <img src="/images/ic_truck.png" alt="" />
                    <span style={{ marginLeft: '16px' }}>Pesanan Saya</span>
                  </div>
                  <img src="/images/ic_arrow_right.png" alt="" />
                  <div className="divider" />
                </div>
              </Link>

              <Link href="/history">
                <div className="c-menu">
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <img src="/images/ic_truck.png" alt="" />
                    <span style={{ marginLeft: '16px' }}>Mutasi Rekening</span>
                  </div>
                  <img src="/images/ic_arrow_right.png" alt="" />
                  <div className="divider" />
                </div>
              </Link>

              <Link href="/wishlist">
                <div className="c-menu">
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <img src="/images/ic_love.png" alt="" />
                    <span style={{ marginLeft: '16px' }}>Wishlist</span>
                  </div>
                  <img src="/images/ic_arrow_right.png" alt="" />
                  <div className="divider" />
                </div>
              </Link>

            </div>
          </div>
          <div className="w-5/6 xs:w-full sm:w-full">

            {/* <div className="w-full flex flex-wrap lg:flex-no-wrap">
              <div className="w-full lg:w-1/2 xl:w-1/2 account-content mb-4">
                <div className="card-content">
                  <div className="section h-full">
                    <div className="header flex flex-col justify-between" style={{marginBottom: '0px'}}>
                      <div className="title w-full mb-4">Saldo Belanja</div>
                      <div className="title w-full" style={{fontWeight: 'bold'}}>
                        Rp. {formatNumber(balance.saldo_akhir)}
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="w-full lg:w-1/2 xl:w-1/2 account-content mb-4">
                <div className="card-content">
                  <div className="section h-full">
                    <div className="header flex flex-col justify-between" style={{marginBottom: '0px'}}>
                      <div className="title w-full mb-4">Rekening Saldo Belanja</div>
                      <div className="title w-full" style={{fontWeight: 'bold'}}>
                        {balance2.no_rekening}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div> */}

            {/* <div className="w-full flex flex-wrap lg:flex-no-wrap">
              <div className="w-full lg:w-1/2 xl:w-1/2 account-content mb-4">
                <div className="card-content">
                  <div className="section h-full">
                    <div className="header flex flex-col justify-between" style={{marginBottom: '0px'}}>
                      <div className="title w-full mb-4">Simpanan Pokok</div>
                      <div className="title w-full" style={{fontWeight: 'bold'}}>
                        Rp. {formatNumber(balance2.saldo_akhir)}
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="w-full lg:w-1/2 xl:w-1/2 account-content mb-4">
                <div className="card-content">
                  <div className="section h-full">
                    <div className="header flex flex-col justify-between" style={{marginBottom: '0px'}}>
                      <div className="title w-full mb-4">Rekening Simpanan Pokok</div>
                      <div className="title w-full" style={{fontWeight: 'bold'}}>
                        {balance2.no_rekening}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div> */}


            <div className="w-full mb-4">
              <Rekening data={balance} token={token} callback={() => refreshBalance()}/>
            </div>

            <div className="w-100 account-content">
              <div className="card-content">
                <div className="section">
                  <div className="header">
                    <div className="title">Data Personal</div>
                    <div className="action" onClick={() => setModalPersonal(true)} role="button" tabIndex={0} onKeyDown={() => setModalPersonal(true)}>
                      <img src="/images/ic_edit_red.png" alt="" style={{ marginRight: '14px' }} />
                    Ubah
                    </div>
                  </div>
                  <div className="c-c">
                    <div className="text-key">
                    Nama
                    </div>
                    <div className="text-value">
                      {userData.name}
                    </div>
                  </div>
                  <div className="c-c">
                    <div className="text-key">
                    Tanggal Daftar
                    </div>
                    <div className="text-value">
                      {moment(userData.created_at).format("DD MMMM YYYY")}
                    </div>
                  </div>
                  <div className="c-c">
                    <div className="text-key">
                    Status Verifikasi
                    </div>
                    <div className="text-value">
                      {userData.is_verified ? "Terverifikasi" : "Belum terverifikasi"}
                    </div>
                  </div>
                  <div className={userData.is_verified ? "hidden" : "flex"}>
                      <button className="btn-verification" onClick={handleEmailVerification}>Kirim email untuk verifikasi</button>
                    </div>
                  <div className="divider" style={{ marginTop: '48px' }} />
                </div>

                <div className="section" style={{ marginTop: '48px' }}>
                  <div className="header">
                    <div className="title">Kontak</div>
                    <div className="action" onClick={() => setModalKontak(true)} role="button" tabIndex={0} onKeyDown={() => setModalKontak(true)}>
                      <img src="/images/ic_edit_red.png" alt="" style={{ marginRight: '14px' }} />
                    Ubah
                    </div>
                  </div>
                  <div className="c-c">
                    <div className="text-key">
                    Email
                    </div>
                    <div className="text-value">
                    {userData.email}
                    </div>
                  </div>
                  <div className="c-c">
                    <div className="text-key">
                  Nomor HP
                    </div>
                    <div className="text-value">
                    {userData.phone}
                    </div>
                  </div>
                  <div className="divider" style={{ marginTop: '48px' }} />
                </div>

                <div className="section" style={{ marginTop: '48px' }}>
                  <div className="header">
                    <div className="title">Keamanan</div>
                  </div>

                  <div className="action" style={{ marginTop: '24px' }} onClick={() => setModalKeamanan(true)} role="button" tabIndex={0} onKeyDown={() => setModalKeamanan(true)}>
                    <img src="/images/ic_lock_red.png" alt="" style={{ marginRight: '14px' }} />
                    Ubah Password
                  </div>
                </div>
              </div>
            </div>
          </div>

          <Modal open={modalPersonal} onClose={() => setModalPersonal(false)}>
            <div className="card-modal" style={{ padding: '16px 26px' }}>
              <div style={{ textAlign: 'right' }}>
                <div style={{ display: 'inline-block' }} onClick={() => setModalPersonal(false)} onKeyDown={() => setModalPersonal(false)} className="pointer" role="button" tabIndex={0}>
                  <img src="/images/ic_close_red.png" alt="" />
                </div>
              </div>
              <div className="title" style={{ marginTop: '16px' }}>
                Ubah Data Personal
              </div>
              <form onSubmit={handleSubmitPersonal}>
                <div style={{ marginTop: '24px' }}>
                  <input className="form-control" placeholder="Nama Depan" value={formPersonal.firstName} onChange={handleChangeFormPersonal("firstName")} />
                </div>
                <div style={{ marginTop: '12px' }}>
                  <input className="form-control" placeholder="Nama Belakang" value={formPersonal.lastName} onChange={handleChangeFormPersonal("lastName")} />
                </div>
                <div style={{ marginTop: '24px' }}>
                  <button className="btn-primary" type="submit">SIMPAN</button>
                </div>
              </form>
              <div className="divider" style={{ margin: '24px' }} />
            </div>
          </Modal>

          <Modal open={modalKontak} onClose={() => setModalKontak(false)}>
            <div className="card-modal" style={{ padding: '16px 26px' }}>
              <div style={{ textAlign: 'right' }}>
                <div style={{ display: 'inline-block' }} onClick={() => setModalKontak(false)} onKeyDown={() => setModalKontak(false)} className="pointer" role="button" tabIndex={0}>
                  <img src="/images/ic_close_red.png" alt="" />
                </div>
              </div>
              <div className="title" style={{ marginTop: '16px' }}>
                Ubah Data Kontak
              </div>
              <form onSubmit={submitChangeEmailPhone}>
                <div style={{ marginTop: '24px' }}>
                  <input className="form-control" placeholder="Email" type="email" value={formEmailPhone.email} onChange={handleChangeFormEmailPhone("email")} />
                </div>
                <div style={{ marginTop: '12px' }}>
                  <input className="form-control" placeholder="Nomor Telepon" maxLength="13" value={formEmailPhone.phone} onChange={handleChangeFormEmailPhone("phone")} />
                </div>
                <div style={{ marginTop: '12px' }}>
                  <input className="form-control" placeholder="Password Sekarang" type="password" value={formEmailPhone.password} onChange={handleChangeFormEmailPhone("password")} />
                </div>
                <div style={{ marginTop: '24px' }}>
                  <button className="btn-primary">SIMPAN</button>
                </div>
              </form>
              <div className="divider" style={{ margin: '24px' }} />
            </div>
          </Modal>

          <Modal open={modalKeamanan} onClose={() => setModalKeamanan(false)}>
            <div className="card-modal" style={{ padding: '16px 26px' }}>
              <div style={{ textAlign: 'right' }}>
                <div style={{ display: 'inline-block' }} onClick={() => setModalKeamanan(false)} onKeyDown={() => setModalKeamanan(false)} className="pointer" role="button" tabIndex={0}>
                  <img src="/images/ic_close_red.png" alt="" />
                </div>
              </div>
              <div className="title" style={{ marginTop: '16px' }}>
                Ubah Password
              </div>
              <form onSubmit={submitChangePassword}>
                <div style={{ marginTop: '24px' }}>
                  <input className="form-control" type="password" placeholder="Password Lama" value={formPassword.oldPassword} onChange={handleChangeFormPassword("oldPassword")} />
                </div>
                <div style={{ marginTop: '12px' }}>
                  <input className="form-control" type="password" placeholder="Password Baru" value={formPassword.newPassword} onChange={handleChangeFormPassword("newPassword")} />
                </div>
                <div style={{ marginTop: '12px' }}>
                  <input className="form-control" type="password" placeholder="Konfirmasi Password Baru" value={formPassword.confirmPassword} onChange={handleChangeFormPassword("confirmPassword")} />
                </div>
                <div style={{ marginTop: '24px' }}>
                  <button className="btn-primary">SIMPAN</button>
                </div>
              </form>
              <div className="divider" style={{ margin: '24px' }} />
            </div>
          </Modal>

          <Modal open={modalOtp} onClose={() => setModalOtp(false)}>
            <div className="card-modal" style={{ maxWidth: '568px' }}>
              <div className="container-login">
                <div style={{ textAlign: 'right' }}>
                  <div style={{ display: 'inline-block' }} onClick={() => setModalOtp(false)} onKeyDown={() => setModalOtp(false)} className="pointer" role="button" tabIndex={0}>
                    <img src="/images/ic_close_red.png" alt="" />
                  </div>
                </div>
                <div className="w-full justify-center flex flex-wrap">
                  <img src="/images/ic_otp.png" alt=""></img>
                </div>
                <div className="w-full flex justify-center text-text-primary font-bold text-xl mt-4">
                  Masukkan Kode Verifikasi
                </div>
                <div className="w-full flex justify-center text-center text-text-primary text-lg tracking-wide">
                  Kode verifikasi telah dikirimkan melalui {otpType === "email" ? `email` : `sms`}<br/><br/> 
                  {otpType === "email" ? `Email ke ${userData.email}` : `ke nomor ${formEmailPhone.phone}`}
                </div>
                <div className="w-full flex justify-center text-center text-text-primary text-lg tracking-wide mt-2">
                  Kode Verifikasi
                </div>
                <div className="w-full">
                  <form method="get" className="digit-group flex justify-center mt-4" data-group-name="digits" data-autosubmit="false" autocomplete="off">
                    <input style={{ width: '25%' }} className="border border-gray-300 text-lg" type="text" id="digit-1" name="digit-1" data-next="digit-2" maxLength="1" ref={firstOtpRef} required value={form.otp.first} onChange={handleKeyOtp('first')}/>
                    <input style={{ width: '25%' }} className="border border-gray-300 text-lg" type="text" type="text" id="digit-2" name="digit-2" data-next="digit-3" data-previous="digit-1" maxLength="1" required ref={secondOtpRef} value={form.otp.second} onChange={handleKeyOtp('second')}/>
                    <input style={{ width: '25%' }} className="border border-gray-300 text-lg" type="text" type="text" id="digit-3" name="digit-3" data-next="digit-4" data-previous="digit-2" maxLength="1" required ref={thirdOtpRef} value={form.otp.third} onChange={handleKeyOtp('third')}/>
                    <input style={{ width: '25%' }} className="border border-gray-300 text-lg" type="text" type="text" id="digit-4" name="digit-4" data-next="digit-5" data-previous="digit-3" maxLength="1" required ref={fourthOtpRef} value={form.otp.fourth} onChange={handleKeyOtp('fourth')}/>
                  </form>
                </div>
                <div style={{ marginTop: '38px' }}>
                  <button
                    type="button"
                    className="btn-primary"
                    onClick={() => handleAfterOtp()}
                    disabled={form.otp.first == ''|| form.otp.second == '' || form.otp.third == '' || form.otp.fourth == ''}
                  >
                    Verifikasi
                  </button>
                </div>
                <div className="w-full flex justify-center text-center text-text-primary text-lg tracking-wide mt-4">
                  Tidak Menerima Kode?
                </div>
                <div className="w-full flex justify-center text-center text-text-primary text-lg tracking-wide">
                  <button className="text-primary" onClick={handleResendEmailVerification}>Kirim Ulang</button>
                </div>
              </div>
            </div>
          </Modal>
        </div>
      </BaseLayout>
      <style jsx>
        {
          `
        .c {
          width: 100%;
          display: flex;
          padding: 48px 0;
        }

        .c-filter {
          width: 100%;
        }
        .divider {
          width: 100%;
          margin-top: 12px;
          height: 1px;
          background: #E1E1E1;
        }
        .c-menu {
          font-weight: 500;
          font-size: 16px;
          line-height: 17px;
          letter-spacing: 0.05em;
          display: flex;
          justify-content: space-between;
          align-items: center;
          flex-wrap: wrap;
          padding: 10px 0px;
          cursor: pointer;
        }

        .active {
          color: #010101;
        }

        .card-content {
          background: #FFFFFF;
          box-shadow: 0px 0px 4px rgba(0, 0, 0, 0.25);
          border-radius: 0px 7px 7px 7px;
          padding: 26px 32px;
        }

        .header {
          display: flex;
          justify-content: space-between;
        }

        .title {
          font-weight: 500;
          font-size: 22px;
          line-height: 21px;
          letter-spacing: 0.05em;
          color: #696969;
        }

        .action {
          font-weight: 500;
          font-size: 16px;
          line-height: 19px;
          letter-spacing: 0.05em;
          color: #010101;
          display: flex;
          align-items: center;
          cursor: pointer;
        }

        .c-c {
          font-size: 16px;
          line-height: 19px;
          letter-spacing: 0.05em;
          color: #696969;
          display: flex;
          margin-top:15px;
        }
        .account-content .header{
          margin-bottom:30px;
        }

        .text-key {
          width: 25%;
        }
        .btn-verification{
          padding:10px 20px;
          background: #010101;
          border-radius: 10px;
          color:#fff;
          margin-top:20px;
        }
        .text-forgot-password {
          font-size: 12px;
          line-height: 14px;
          color: #6F0C06;
          margin-top: 20px;
          text-align: right;
        }

        .container-social-media {
          display: flex;
          justify-content: space-evenly;
          margin-top: 30px;
        }

        .text-register {
          font-size: 16px;
          line-height: 19px;
          color: #AFAFAF;
        }

        .text-register-action {
          font-weight: bold;
          font-size: 16px;
          line-height: 19px;
          color: #730C07;
          cursor: pointer;
        }

        .text-is-seller {
          font-size: 16px;
          line-height: 19px;
          color: #868686;
          font-weight: 300;
        }

        .digit-group .splitter {
            padding: 0 5px;
            color: white;
            font-size: 24px;
        }

        .digit-group input {
            width: 50px;
            height: 50px;
            text-align: center;
            margin: 0 2px;
        }
        .card-modal{
          min-width:450px;
          padding:20px;
        }
        .account-content{
          padding-left: 24px;
        }
        .card-modal input{
          width:100%;
        }
        @media (min-width: 300px) and (max-width: 767px) {
          .c{
            padding: 0 20px;
            margin-bottom: 30px;
          }
          .c-filter{
            margin-bottom:30px;
          }
          .card{
            width: 90%;
            padding:30px;
          }
          .account-content{
            padding-left: 0;
          }
          .c-c{
            margin-top:15px;
            align-items: center;
          }
          .c-c .text-value{
            width:70%;
          }
          .c-c .text-key{
            width:30%
          }
          .c-c .text-key{
            font-weight:bold;
          }
          .form-control{
            width:100%;
          }
          .account-content .title{
            font-size:20px;
          }
          .account-content .text-key{
            font-size:16px;
          }
          .account-content .text-value{
            font-size:14px;
          }
          .card-modal{
            min-width:inherit;
            width: 100%;
          }
          .card-modal input{
            min-width:inherit;
            width:100%;
          }
          .card-modal .digit-group input{
            width: 50px;
          }
          
        }
        `
        }
      </style>
    </div>
  );
};

index.getInitialProps = async (ctx) => {
  const { token } = ctx.store.getState().authentication;
  if(!token){
    ctx.res.writeHead(301, {
      Location: '/signin'
    })
    ctx.res.end();
  }
  const productTypeResp = await getProductType();
  const productType = productTypeResp.data;

  const userResp = await getUser(token);
  const user = userResp.data;

  const balanceResp = await getBalance(token);
  const Tbalance = balanceResp.data.detail;

  const cartResp = await getCart(token);
  const cart = cartResp.data ? cartResp.data.cart : [];

  return { 
    productType,
    token,
    user,
    cart,
    Tbalance
  };
};

export default connect(
  (state) => state, {}
)(index);
