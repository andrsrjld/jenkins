import React, { useEffect, useState, useCallback } from 'react';
import { connect } from 'react-redux';
import Link from 'next/link';
import Router from 'next/router';
import BaseLayout from '~/components/BaseLayout';
import Modal from 'components/Modal';
import { getCart, deleteCart, updateCart, updateCartNote, updateCartCheckout } from '~/data/api/cart';
import { formatNumber } from '~/util/helper';
import { getProductType } from '~/data/api/product-type';
import { getServiceCharge } from '~/data/api/order';
import ReactPlaceholder from 'react-placeholder';
import Loading from '~/components/Loading'
import AwesomeDebouncePromise from 'awesome-debounce-promise';
import { debounce } from "lodash";

// Toastr Notification
import {successNotification, errorNotification} from 'util/toastr-notif';


const index = ({ token, cartData, authentication, productType, firstCheckedProduct, serviceCharge }) => {
  const [cart, setCart] = useState(cartData);
  const [selectedProduct, setSelectedProduct] = useState(firstCheckedProduct);
  const [modalDelete, setModalDelete] = useState(false);
  const [productDelete, setProductDelete] = useState();

  const [loading, setLoading] = useState(true);
  const [loadingCart, setLoadingCart] = useState(false);
  const [func, setfunc] = useState(() => {})
  const handler = useCallback(debounce((idItem, quantity, total_prices) => handleChangeQuantity(idItem, quantity, total_prices), 1000), []);

  useEffect(() => {
    setTimeout(() => {
      setLoading(false);
    }, 2000);
  },[]);

  const handleGetCart = async () => {
    const res = await getCart(token);
    setCart(res.data && res.data.cart ? res.data.cart : []);
    setLoading(false)
  };

  const handleSelectProduct = (event) => {
    const checkedProducts = selectedProduct;
    if(event.currentTarget.checked){
      checkedProducts.push(parseInt(event.currentTarget.value));
    } else {
      const indexProduct = checkedProducts.indexOf(parseInt(event.currentTarget.value))
      checkedProducts.splice(indexProduct, 1);
    }
    setSelectedProduct(checkedProducts);

    handleGetCart();
  }

  const handleBulkSelectProduct = (event) => {
    const checkedProducts = selectedProduct;
    if(event.currentTarget.checked){
      cart.map(seller => {
        seller.carts.map(item => {
          if(!checkedProducts.includes(item.variant_id)){
            checkedProducts.push(item.variant_id);
          }
        });
      });
    } else {
      checkedProducts.splice(0, checkedProducts.length);
    }
    
    setSelectedProduct(checkedProducts);

    handleGetCart();
  }

  const handleDeleteCart = async () => {
    setLoading(true);
    const res = await deleteCart(token, authentication.user.id, productDelete);
    setModalDelete(false);
    handleGetCart();
  };

  const handleChangeQuantity = async (idItem, quantity, total_prices) => {
    if(quantity > 0){
      setLoadingCart(true);
      setLoading(true)
      const body = {
        id: idItem,
        quantity: quantity,
        total_prices: total_prices
      }
      
      await updateCart(token, body);
      
      handleGetCart();
      setLoadingCart(false);
      setLoading(false);
    }
  }

  const handleCheckoutSelectedProduct = async () => {
    const checkedProducts = selectedProduct;
    for(let a=0; a < cart.length; a++){
      for(let i=0;i < cart[a].carts.length; i++){
        let body = {
          id: cart[a].carts[i].id,
          checkout: true
        }
        if(!checkedProducts.includes(cart[a].carts[i].variant_id)){
          body.checkout = false;
          await updateCartCheckout(token, body);
        } else {
          await updateCartCheckout(token, body);
        }
      }
    }

    if(checkedProducts.length == 0 || cart.length == 0) {
      errorNotification("Harap pilih produk terlebih dahulu")
      return
    }

    window.location = "/checkout";
  }

  const handleChangeProductNotes = async (event) => {
    const body = {
      id: parseInt(event.currentTarget.attributes.cardid.value),
      notes: event.currentTarget.value
    }
    await updateCartNote(token, body);

    handleGetCart();
  }

  const openDeleteModal = (id) => (e) => {
    setProductDelete(id);
    setModalDelete(true);
  }

  const handleInputQty = async (event, item) => {
    event.persist()

    setCart(v => v.map(k => ({...k, carts: k.carts.map(z => z.id == item.id ? ({...item, quantity: Number(event.target.value)}) : z)})))
    if(Number(event.target.value) > 0) {
      handler(item.id, Number(event.target.value), (Number(event.target.value)) * (item.product_prices - item.product_discount))
    }
  }

  return (
    <div>
      <BaseLayout productType={productType} token={token} cart={cartData}>
        <Loading show={loading}/>
        <div className="cart-wrapper xs:p-2 sm:p-2">
        <div className="breadcumb flex xs:hidden sm:hidden">
            <div className="checkout-step active-step" style={{ marginLeft: '-1.8rem', zIndex: '5' }}>Keranjang</div>
            <div className="checkout-step disabled-step" style={{ marginLeft: '-1.8rem', zIndex: '4' }}>Alamat</div>
            <div className="checkout-step disabled-step" style={{ marginLeft: '-1.8rem', zIndex: '3' }}>Shipping</div>
            <div className="checkout-step disabled-step" style={{ marginLeft: '-1.8rem', zIndex: '2' }}>Metode Pembayaran</div>
            <div className={"checkout-step disabled-step"} style={{ marginLeft: '-2.4rem', zIndex: '1' }}>Review Pembayaran</div>
          </div>
          <div className="c" style={{ padding: '20px 0px 80px 0px' }}>
            <div className="title">
              Keranjang
              {' '}
              <span style={{ color: '#010101' }}>Belanja</span>
            </div>

            <div className="c-content flex flex-wrap">
              <div className="w-3/4 xs:w-full sm:w-full md:w-3/4 lg:w-3/4">
                <div className="th bg-white bulk-action"><input type="checkbox" className="checkbox-bulk-product" onChange={handleBulkSelectProduct} checked={selectedProduct.length === cart.reduce((a, b) => a + b.carts.length, 0) ? true : false} /></div>
                {
                  cart.length > 0 ? cart.map((v, i) => (
                    <div className="c-cart" key={i}>
                      <div className="table">
                        <div className="flex">
                          <div className="th bg-white w-full">
                            <h3 className="seller-name">{v.seller_name}</h3>
                            {/* <label className="seller-address">{v.seller_address}</label> */}
                          </div>
                        </div>
                        <div>
                          {
                            v.carts.map((item, index) => (
                              <div key={i} className="flex xs:flex-wrap sm:flex-wrap product-items">
                                <div className="td xs:w-1/8 sm:w-1/8">
                                  <input type="checkbox" className="checkbox-product" onChange={handleSelectProduct} value={item.variant_id} checked={selectedProduct.includes(item.variant_id) ? true : false} />
                                </div>
                                <div className="td xs:w-2/6 sm:w-2/6">
                                  <ReactPlaceholder type={'rect'} ready={!loading} showLoadingAnimation={true} rows={1} style={{width: '100px', height: '100px'}}>
                                    <img className="img-product" src={item.image_url} alt="" />
                                  </ReactPlaceholder>
                                </div>
                                <ReactPlaceholder ready={!loading} showLoadingAnimation={true} rows={2} style={{marginTop: '30px'}}>
                                  <div className="w-full xs:flex-1 sm:flex-1 md:w-full lg:w-full">
                                      <div className="w-full flex xs:flex-wrap sm:flex-wrap items-center xs:items-start sm:items-start text-center xs:text-left md:text-left">
                                        <div className="td w-full xs:w-full sm:w-full product-name-wrapper">
                                          <label>{item.product_name}</label>
                                          <span>Varian : {item.variant_name}</span>
                                          <br />
                                        </div>
                                        <div className="td w-3/6 xs:hidden sm:hidden">
                                          <span className="price-discount" style={item.product_discount <= 0 ? {display:"none"} : {display:"block"}}>{`Rp ${formatNumber(item.quantity * item.product_prices)}`}</span>                          
                                          <span>{item.product_prices ? `Rp ${formatNumber(item.product_prices - item.product_discount)}` : 0}</span>
                                        </div>
                                        <div className="td w-3/6 xs:w-full sm:w-full">
                                          <div className="justify-center xs:justify-start sm:justify-start" style={{ display: 'flex' }}>
                                            <button type="button" className="btn-add-min" onClick={() => handleChangeQuantity(item.id, item.quantity - 1, (item.quantity - 1) * (item.product_prices - item.product_discount))} disabled={item.quantity <= 1}>
                                              <img src="/images/ic_minus.png" alt="" style={{ maxWidth: 'initial' }}/>
                                            </button>

                                            <input className="form-qty" value={Number(item.quantity)} type="number" onChange={(e) => handleInputQty(e,item)} min={1}/>

                                            <button type="button" className="btn-add-min" onClick={() => handleChangeQuantity(item.id, item.quantity + 1, (item.quantity + 1) * (item.product_prices - item.product_discount))}>
                                              <img src="/images/ic_plus.png" alt="" style={{ maxWidth: 'initial' }}/>
                                            </button>
                                          </div>
                                        </div>
                                        <div className="td w-3/6 xs:w-full sm:w-full xs:text-left sm:text-left" style={{color: "#010101"}}>
                                          {item.total_prices ? `Rp ${formatNumber(item.total_prices)}` : 0}
                                        </div>
                                        <div className="td w-3/6 xs:hidden sm:hidden text-center xs:text-left sm:text-left">
                                          <button type="button" className="px-3 py-2 rounded text-text-primary mt-2 hover:bg-gray-200" onClick={openDeleteModal(item.variant_id)}>
                                            <img width="30px" src="/images/ic_delete.png" />
                                          </button>
                                        </div>
                                      </div>
                                      <div className="w-full flex xs:flex-wrap sm:flex-wrap product-action-wrapper">
                                        <div className="td w-1/2 xs:hidden sm:hidden flex items-center">
                                          <label className="product-note-title">Catatan :</label>
                                          <textarea className="product-note" onBlur={handleChangeProductNotes} cardId={item.id}>{item.notes}</textarea>
                                        </div>
                                      </div>
                                    
                                  </div>
                                  
                                  <div className="w-full hidden xs:flex sm:flex">
                                      <div className="td xs:w-1/8 sm:w-1/8">
                                        &nbsp;
                                      </div>
                                      <div className="td w-1/2 hidden xs:block sm:block md:hidden lg:hidden xs:w-4/8 sm:w-1/8 mobile-cart-note">
                                        <p>Catatan :</p>
                                        <textarea className="product-note" onBlur={handleChangeProductNotes} cardId={item.id}>{item.notes}</textarea>
                                      </div>
                                      <div className="td w-1/2 xs:w-3/8 sm:w-3/8 hidden xs:block sm:block md:hidden lg:hidden text-right">
                                        <button type="button" className="px-3 py-2 rounded text-text-primary mt-2 hover:bg-gray-200" onClick={openDeleteModal(item.variant_id)}>
                                          <img width="30px" src="/images/ic_delete.png" />
                                        </button>
                                      </div>
                                  </div>
                                </ReactPlaceholder>
                              </div>
                            ))
                          }
                        </div>
                      </div>
                    </div>
                  )) : <h3>Belum ada produk dalam keranjang anda</h3>
                }
              </div>
              <div className="w-1/4 xs:w-full sm:w-full md:w-full lg:w-full">
                <div className="c-summary">
                  <div className="summary-header">
                    RINGKASAN
                  </div>

                  <ReactPlaceholder ready={!loadingCart} showLoadingAnimation={true} rows={4}>
                    <div className="c-content-summary">
                      <div className="key">
                        Subtotal
                      </div>
                      <div className="value">
                        {`Rp ${formatNumber(cart.reduce((a, b) => a + b.carts.reduce((x, y) => x + y.total_prices, 0), 0))}`}
                      </div>
                    </div>

                    {/* <div className="c-content-summary">
                      <div className="key">
                        Biaya Layanan
                      </div>
                      <div className="value">
                          Rp {formatNumber(serviceCharge.charge)}
                      </div>
                    </div> */}


                    {/* <div className="c-content-summary">
                      <div className="key">
                        Tax
                      </div>
                      <div className="value">
                        Rp 0
                      </div>
                    </div> */}


                    <div className="c-content-summary">
                      <div className="key">
                        Total Pesanan
                      </div>
                      <div className="value">
                        <span style={{ color: '#FF8242', fontWeight: "900", fontSize:"18px" }}>
                          {`Rp ${formatNumber(cart.reduce((a, b) => a + b.carts.reduce((x, y) => x + y.total_prices, 0), Number(serviceCharge.charge)))}`}
                        </span>
                      </div>
                    </div>
                  </ReactPlaceholder>

                  <div style={{ display: 'flex', padding: '0px 8px', margin: '1rem 0px' }}>
                    <button className="btn-primary btn-this" onClick={handleCheckoutSelectedProduct}>
                      LANJUT KE CHECKOUT
                    </button>
                  </div>

                </div>
              </div>
            </div>

          </div>

          <Modal open={modalDelete} onClose={() => setModalDelete(false)}>
            <div className="card-modal" style={{ padding: '16px 26px' }}>
              <div style={{ textAlign: 'right' }}>
                <div style={{ display: 'inline-block' }} onClick={() => setModalDelete(false)} onKeyDown={() => setModalDelete(false)} className="pointer" role="button" tabIndex={0}>
                  <img src="/images/ic_close_red.png" alt="" />
                </div>
              </div>
              <div className="title" style={{ marginTop: '16px' }}>
                Apakah Anda yakin mau
                <br />
                menghapus data ini?
              </div>
              <div style={{ marginTop: '24px', display: 'flex' }}>
                <button className="btn-primary">BATALKAN</button>
                <button className="btn-secondary" style={{ marginLeft: '4px' }} onClick={handleDeleteCart}>HAPUS</button>
              </div>
              <div className="divider" style={{ margin: '24px' }} />
            </div>
          </Modal>

        </div>
      </BaseLayout>

      <style jsx>
        {
          `
          .title {
            font-weight: bold;
            font-size: 24px;
            line-height: 23px;
            letter-spacing: 0.05em;
            color: #696969;
          }

          .c-content {
            margin-top: 2rem;
            display: flex;
          }

          .c-cart {
            width: 97%;
            background: #FFFFFF;
            box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.25);
          }

          .c-summary {
            width: 100%;
            background: #FFFFFF;
            box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.25);
            padding:0 0 10px;
          }

          th {
            padding: 1rem 0px;
            text-align: center;
          }

          th.bulk-action{
            text-align:center;
          }

          td {
            text-align: center;
          }

          .btn-add-min {
            background: #FFFFFF;
            border: 1px solid #F1F1F1;
            box-sizing: border-box;
            padding: 10px;
            display: flex;
            flex-wrap: wrap;
            align-items: center;
          }

          .form-qty {
            background: #FFFFFF;
            border: 1px solid #F1F1F1;
            box-sizing: border-box;
            font-weight: 500;
            font-size: 22px;
            color: #979797;
            padding: 10px;
            width: 70px;
            text-align: center;
          }

          .summary-header {
            padding: 1rem;
            font-weight: bold;
            font-size: 18px;
            line-height: 17px;
            color: #868686;
            box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.25);
          }

          .c-content-summary {
            font-size: 16px;
            line-height: 15px;
            color: #696969;
            display: flex;
            justify-content: space-between;
            padding: 0.7rem;
            margin: 0.8rem 0.5rem;
            border-bottom: 1px solid #CCCCCC;
          }

          .btn-this {
            font-weight: 500;
            font-size: 18px;
            line-height: 17px;
            text-align: center;
            color: #FFFFFF;
          }
          .checkout-step{
            min-width: 160px;
            width: auto;
            padding:15px 3.5rem;
            display:block;
            background-image: url('/images/ic_milestone_right.png');
            background-size: 100% 100%;

            display: flex;
            justify-content: center;
            align-items:center;
          }
          .disabled-step{
            background-image: url('/images/ic_milestone_right_grey.png');
            color:#868686;
          }
          .active-step{
            min-width: 160px;
            width: auto;
            padding:15px 2.5rem;
            display:block;
            color: white;
            background-image: url('/images/ic_milestone_right_red.png');
            background-size: 100% 100%;

            display: flex;
            justify-content: center;
            align-items:center;
          }

          .product-items{
            align-items: top;
          }
          .th{
            padding:8px 16px;
          }
          .product-note{
            border:1px solid #ccc;
            width:300px;
            height:40px;
            padding:10px;
            display:inline-block;
          }
          .product-note-title{
            display:inline-block;
            margin-right:10px;
          }
          .product-action-wrapper div{
            padding-top:0;
            padding-bottom:0;
          }
          .seller-address{
            font-weight: 400;
            font-size:14px;
            margin-top:10px;
          }
          .table{
            margin-bottom:20px;
          }
          .bulk-action{
            width: 97%;
            background: #FFFFFF;
            box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.25);
            margin-bottom:20px;
          }
          .cart-wrapper{
            padding: 20px 70px;
          }
          .img-product{
            width:140px;
          }
          .td{
            padding:20px 10px;
            font-size:14px;
          }
          .product-items{
            padding: 15px 0;
          }
          .product-action-wrapper label, .product-action-wrapper .product-note{
            color:#696969;
            font-size:14px;
          }
          .form-qty{
            font-size:18px;
            padding: 5px; 
          }
          .fixed-summary{
            position:fixed;
            top:15px;
            right:30px;
            width:23%
          }
          .price-discount{
            text-decoration: line-through;
            line-height:20px;
          }
          .product-name-wrapper{
            text-align: left;
            line-height: 25px;
          }
          .breadcumb{
            padding:0 30px;
          }
          .card-modal .title{
            font-size: 18px;
          }
          .card-modal button{
            font-size: 14px;
            padding: 10px 15px;
          }

          @media (min-width: 320px) and (max-width: 767px) {
            .cart-wrapper{
              padding: 0 20px 20px;
            }
            .td{
              padding: 5px 10px;
            }
            .c-cart, .bulk-action {
              width: 100%;
            }
            .product-note{
              width:100%;
            }
            .product-action-wrapper label, .product-action-wrapper .product-note{
              color:#ccc;
              font-size:14px;
            }
            .product-name-wrapper label{
              margin-bottom:10px;
            }
            .title{
              text-align:center;
            }
            .mobile-cart-note p{
              margin-bottom:5px;
            }
          }
        `
        }
      </style>
    </div>
  );
};

index.getInitialProps = async (ctx) => {
  const { token } = ctx.store.getState().authentication;
  if(!token){
    if(ctx.req) {
      ctx.res.writeHead(302, {
        Location: `/signin?redirect=cart`
      });
      ctx.res.end();
    } else {
      window.location = '/signin?redirect=cart'
    }
  }
  
  const cart = await getCart(token);
  const firstCheckedProduct = [];
  if(cart.data && cart.code == 200 && cart.data.cart){
    cart.data.cart.map(seller => {
      seller.carts.map(product => {
        firstCheckedProduct.push(product.variant_id);
      });
    });
  } 

  const serviceChargeResp = await getServiceCharge(token);
  const serviceCharge = serviceChargeResp.data;

  const productTypeResp = await getProductType();
  const productType = productTypeResp.data;

  return {
    token,
    cartData: cart.data && cart.data.cart ? cart.data.cart : [],
    productType,
    firstCheckedProduct,
    serviceCharge
  };
};

export default connect(
  (state) => state, {}
)(index);
