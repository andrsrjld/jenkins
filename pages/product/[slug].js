import React, { useState, useEffect } from 'react';
import Link from 'next/link';
import PropTypes from 'prop-types';
import BaseLayout from '~/components/BaseLayout';
import CardProduct from '~/components/CardProduct';
import CardProductRelated from '~/components/CardProductRelated';
import SliderImages from '~/components/SliderImages';
import Modal from '~/components/AddCartModal';
import { formatNumber } from '~/util/helper';
import { getProductType } from '~/data/api/product-type';
import { getCart } from '~/data/api/cart';
import ReactPlaceholder from 'react-placeholder';
import { getSellerDetail } from '~/data/api/seller';

// Toastr Notification
import {successNotification, errorNotification} from '~/util/toastr-notif';

// Axios API Request
import axiosSetup from '../../services/api';

const modalStyles = {
  content: {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)',
    width: '50%',
    textAlign: 'center'
  }
}

const Product = ({
  slug, product, relatedProducts, token, productType, cart
}) => {
  const [activeVariant, setActiveVariant] = useState(((product.product_type_name.toLowerCase().includes("merchandise") || product.product_type_name.toLowerCase().includes("elektrik") || product.product_type_name.toLowerCase().includes("manual") ) && product.variants.length == 1) ? product.variants[0].id : null);
  const [activeVariantCOD, setActiveVariantCOD] = useState(false);
  const [activeInfo, setActiveInfo] = useState("information");
  const [activeVariantQuantity, setActiveVariantQuantity] = useState(((product.product_type_name.toLowerCase().includes("merchandise") || product.product_type_name.toLowerCase().includes("elektrik") || product.product_type_name.toLowerCase().includes("manual") ) && product.variants.length == 1) ? product.variants[0].quantity : 0);
  const [variantPrice, setVariantPrce] = useState(((product.product_type_name.toLowerCase().includes("merchandise") || product.product_type_name.toLowerCase().includes("elektrik") || product.product_type_name.toLowerCase().includes("manual") ) && product.variants.length == 1) ? product.variants[0].price : product.price);
  const [quantity, setQuantity] = useState(1);
  const [modalOpen, setModalOpen] = useState(false);
  const [existWishlist, setExistWishlist] = useState(0);
  const [requestProduct, setRequestProduct] = useState(product.product_type_name.toLowerCase().includes('roasted bean') ? 'Whole Bean' : '');
  const [activeImage, setActiveImage] = useState(product.images ? product.images.find(v => v.is_primary).image_url : "/images/dummy_product.png")

  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setLoading(false);
    }, 2000);
  },[]);

  useEffect(() => {
    if(token){
      const api = axiosSetup(token);
      api.get("/restricted/customer/wishlist?layout_type=list_layout&order=product_name,ASC&page=1&limit=10000").then((response) => {
        const userWishlist = response.data.data;
        if(userWishlist){
          const isExistWishlist = userWishlist.find(wishlist => {
            return wishlist.product.id === product.id
          });
          setExistWishlist(isExistWishlist ? isExistWishlist.id : 0);
        }
      });
    }
  });

  const handleSelectVariant = (event) => {
    setActiveVariant(event.currentTarget.attributes.value.value.toString());
    setVariantPrce(event.currentTarget.attributes.price.value);
    setActiveVariantCOD(event.currentTarget.attributes.cod.value === 'true' ? true : false);
    setActiveVariantQuantity(event.currentTarget.attributes.quantity.value);
  }

  const handleSelectInfo = (event) => {
    setActiveInfo(event.currentTarget.id);
  }

  const handleChangeQuantity = (event) => {
    if(event.currentTarget.value === "increase"){
      setQuantity(quantity + 1);
    } else {
      if(quantity > 1){
        setQuantity(quantity - 1);
      }
    }
  }

  const handleCloseModal = () => {
    setModalOpen(false);
  }

  const handleChangeRequestProduct = (event) => {
    setRequestProduct(event.currentTarget.value)
  }

  const handleAddToCart = () => {
    const api = axiosSetup(token);
    if(!token){
      window.location = `/signin?redirect=product/${slug}`;
    } else {
      if(activeVariant){
        if(activeVariantQuantity > 0){
          api.post("/restricted/customer/cart", {
            product_id: product.id,
            quantity: quantity,
            product_prices: parseFloat(variantPrice) - product.discount_amount,
            variant_id: parseInt(activeVariant),
            notes: requestProduct,
            cod: activeVariantCOD,
            is_po: product.is_po,
            po_duration: product.po_duration
          }).then((response) => {
            if(response.data.error && response.data.error == 'Stok Habis') {
              errorNotification("Maaf stok varian produk yang dipilih melebihi dari yang stok yang tersedia");
            } else {
              setModalOpen(true);
            }
          }).catch((error) => {
            console.log(error.response);
            errorNotification(error.response ? error.response.data.message : error.response);
          });
        } else {
          errorNotification("Stok varian produk kosong");
        }
      } else {
        errorNotification("Mohon pilih varian produk")
      }
    }
  }

  const handleAddToWishlist = () => {
    const api = axiosSetup(token);
    if(!token){
      window.location = `/signin?redirect=product/${slug}`;
    } else {
      if(existWishlist != 0){
        api.delete("/restricted/customer/wishlist?id=" + existWishlist).then((response) => {
          console.log(response.data.data);
          setExistWishlist(0);
          successNotification("Berhasil menghapus product dari wishlist");
        }).catch((error) => {
          console.log(error.response);
          errorNotification(error.response ? error.response.data.message : error.response);
        });
      } else {
        api.post("/restricted/customer/wishlist", {
          product_id: product.id,
        }).then((response) => {
          console.log(response.data.data);
          setExistWishlist(response.data.data.id);
          successNotification("Berhasil menambahkan product ke wishlist");
        }).catch((error) => {
          console.log(error.response);
          errorNotification(error.response ? error.response.data.message : error.response)
        });
      }
    }
  }

  const images = [];
  if(product.images) {
    product.images.map(image => {
      images.push(image.image_url);
    })
  }

  let totalStock = product.variants.reduce((total, current) => {return total + current.quantity}, 0);

  return (
    <div>
      <BaseLayout productType={productType} token={token} cart={cart}>
        <div id="breadcumb" className="breadcrumb-wrapper">
          <div className="breadcumb">
            Home &#x0226B;
            {' '}
            <span style={{ color: '#010101' }}>{product.name}</span>
          </div>

          <div className="c product-detail-content">
            <div className="c-content flex flex-wrap w-full" style={{ margin: '24px 0px' }}>

              <div className="w-1/2 xs:w-full sm:w-full md:w-1/2 lg:w-1/2">
                <div clasName="c-images">
                  <div className="c-image w-full">
                    <img className="img" src={activeImage} alt="" />
                  </div>
                  <div className="c-slider-image" style={{ marginTop: '12px' }}>
                    <ReactPlaceholder ready={!loading} type={'rect'} showLoadingAnimation delay={1000} style={{width: '100%', height:'400px'}}>
                      <SliderImages data={images} onClick={(activeImage) => setActiveImage(activeImage)}/>
                    </ReactPlaceholder>
                  </div>
                </div>
              </div>
              <div className="w-1/2 xs:w-full sm:w-full md:w-1/2 lg:w-1/2">
                <div className="c-content-information">
                  <div className="product-name">
                    <ReactPlaceholder ready={!loading} showLoadingAnimation={true} rows={1} style={{width: 'auto'}}>
                      {product.name ? product.name : "-"}
                    </ReactPlaceholder>
                  </div>
                  <div className="product-merchant">
                    <ReactPlaceholder ready={!loading} showLoadingAnimation={true} rows={1} style={{width: 'auto'}}>
                      <Link href={`/brand/${product.seller.slug}`}>
                        {product.seller.store_name ? product.seller.store_name : "-"}
                      </Link>
                    </ReactPlaceholder>
                  </div>

                  <div style={{
                    display: 'flex', alignItems: 'center', textAlign: 'left', marginTop: '8px'
                  }}
                  >
                    <div className="product-price">
                      <ReactPlaceholder ready={!loading} showLoadingAnimation={true} rows={2} style={{width: '100%'}}>
                        <span className="price-discount" hidden={product.discount_amount == 0 ? true : false}>{`Rp ${formatNumber(variantPrice)}`}</span>
                        <span>Rp. {variantPrice ? formatNumber(variantPrice - product.discount_amount) : 0}</span>
                      </ReactPlaceholder>
                    </div>
                    <div style={activeVariant ? {float: "left", marginLeft:"20px"} : {float: "left", marginLeft:"20px", display:"none"}}>
                      <img src="/images/ic_tersedia.png" alt="" />
                      {' '}
                      <span className="product-tersedia">{totalStock > 0 ? "Tersedia" : "Stok Habis"}</span>
                    </div>
                  </div>
                  <ReactPlaceholder ready={!loading} showLoadingAnimation={true} rows={1} style={{width: 'auto'}}>
                  {
                    !((product.product_type_name.toLowerCase().includes('merchandise') || product.product_type_name.toLowerCase().includes('elektrik') || product.product_type_name.toLowerCase().includes('manual')) && product.variants.length == 1) && (
                      <>
                        <label className="w-full" style={{marginTop: '20px', fontSize: '18px'}}>Varian produk :</label>
                        <div style={{
                          display: 'flex', alignItems: 'center', textAlign: 'left', margin: '16px 0 20px 0px'
                        }}
                        >
                          {product.variants.map((variant) => (
                              <div onClick={handleSelectVariant}
                                className={`${activeVariant === variant.id.toString() ? "variant-option active" : "variant-option"}`}
                                value={variant.id}
                                sku={variant.sku}
                                weight={variant.weight}
                                quantity={variant.quantity}
                                price={variant.price}
                                cod={variant.cod.toString()}
                              >
                                {variant.details && variant.details.length > 0 ? variant.details[0].attribute_value : `${variant.sku}`}
                              </div>
                          ))}
                          {
                            product.variants.find(v => activeVariant == v.id) 
                            ? (<span className="comments">{`(${product.variants.find(v => activeVariant == v.id).quantity})`}</span>)
                            : <></>
                          }
                        </div>
                      </>
                    )
                  }
                  </ReactPlaceholder>
                  
                  {
                    product.product_type_name.toLowerCase().includes('roasted bean') ? (
                    <div className='product-request-wrapper'>
                        <h4>Request :</h4><br/>
                        <input type="radio" id="wholebean" name="request" value="Whole Bean" checked={requestProduct === "Whole Bean"} onChange={handleChangeRequestProduct} />
                        <label for="wholebean">Whole Bean</label>
                        <input type="radio" id="ground" name="request" value="Ground" checked={requestProduct === "Ground"} onChange={handleChangeRequestProduct} />
                        <label for="ground">Ground</label>
                    </div>
                    ) : "" 
                  }

                  <div className="flex xs:flex-wrap mt-8 items-center">
                    <div className="flex">
                      <button type="button" className="btn-add-min" onClick={handleChangeQuantity} value="decrease">
                        <img src="/images/ic_minus.png" alt="" />
                      </button>

                      <input className="form-qty" value={quantity} type="number" />

                      <button type="button" className="btn-add-min" onClick={handleChangeQuantity} value="increase">
                        <img src="/images/ic_plus.png" alt="" />
                      </button>
                    </div>

                    <div className="xs:w-full sm:w-full flex items-center xs:mt-4">
                      <button type="button" className="btn-primary btn-add-to-cart" onClick={handleAddToCart}>
                        TAMBAH KERANJANG
                      </button>

                      <button type="button" className={existWishlist != 0 ? "btn-like active-like" : "btn-like"} onClick={handleAddToWishlist}>
                        <img src={existWishlist != 0 ? "/images/ic_like_on.svg" : "/images/ic_action_like.png"} alt="" />
                      </button>
                    </div>
                  </div>

                  <div className="product-desc" style={{ display: 'flex', marginTop: '24px', whiteSpace:'pre-line' }}>
                    <ReactPlaceholder ready={!loading} showLoadingAnimation={true} rows={3} style={{width: '100%'}}>
                      {product.short_description ? product.short_description : "-"}
                    </ReactPlaceholder>
                  </div>

                  {
                    activeVariant != null && (
                      <div className="mt-4 w-full border border-primary px-4 py-3 rounded border-opacity-50">
                        <div className="w-full text-center font-bold mb-4">
                          Simulasi Cicilan
                        </div>
                        
                        <div className="w-full flex justify-between mb-2 text-sm">
                          <div>3x Cicilan</div>
                          <div>{ product.variants.find(v => v.id == Number(activeVariant)).installment_simulation['3x'] }</div>
                        </div>
                        <div className="w-full flex justify-between mb-2 text-sm">
                          <div>6x Cicilan</div>
                          <div>{ product.variants.find(v => v.id == Number(activeVariant)).installment_simulation['6x'] }</div>
                        </div>
                        <div className="w-full flex justify-between mb-2 text-sm">
                          <div>12x Cicilan</div>
                          <div>{ product.variants.find(v => v.id == Number(activeVariant)).installment_simulation['12x'] }</div>
                        </div>
                      </div>
                    )
                  }

                </div>
              </div>
            </div>

            <div className="c-description w-full">
              <div className="flex tab-title-wrapper">
                {/* <div className="w-1/2 h-12 description-title" id="description" onClick={handleSelectInfo}>
                  DESKRIPSI
                  <div className="description-divider" hidden={activeInfo === "description" ? false : true} />
                </div> */}
                <div className="w-full h-12 description-title" id="information" onClick={handleSelectInfo}>
                  INFORMASI
                  <div className="description-divider" hidden={activeInfo === "information" ? false : true} />
                </div>
              </div>
              {/* <div className="description-text description-content" hidden={activeInfo === "description" ? false : true}>
                {product.description ? product.description : "-"}
              </div> */}
              <div className="description-text information-content" hidden={activeInfo === "information" ? false : true} style={{ whiteSpace:'pre-line' }}>
                <ReactPlaceholder ready={!loading} showLoadingAnimation={true} rows={3} style={{width: '100%'}}>
                  {product.description ? product.description : "-"}
                  <br></br>
                  <br></br>
                  <ul>
                  {product.information ? product.information.filter(v =>  v.value).map(info => (
                    <li>{info.name} : {info.value}</li>
                  )) : "-"}
                  </ul>
                </ReactPlaceholder>
              </div>
            </div>

            <div className="c-description w-full">
              <div className="description-title" style={{ color: '#696969' }}>
                REKOMENDASI UNTUK ANDA
                <div className="description-divider" />
              </div>
              <div style={{ display: 'flex', justifyContent: 'space-between', flexWrap: 'wrap' }}>
                {
                  relatedProducts.slice(0,3).map((v) => (
                    <CardProductRelated data={v} loading={loading} />
                  ))
                }
              </div>
            </div>


          </div>
        </div>
        <Modal
          open={modalOpen}
          onClose={handleCloseModal}
          style={modalStyles}
          contentLabel="Cart Modal"
        >
          <div className="card-modal" style={{ padding: '16px 26px', textAlign:"center" }}>
            <div style={{ textAlign: 'right' }}>
              <div style={{ display: 'inline-block' }} onClick={handleCloseModal} onKeyDown={handleCloseModal} className="pointer" role="button" tabIndex={0}>
                <img src="/images/ic_close_red.png" alt="" />
              </div>
            </div>
            <h2 className="product-name">Berhasil ditambahkan ke keranjang</h2>
            <Link href={`/cart`}>
              <button type="button" className="btn-primary mt-4" style={{width:"250px"}}>
                Lihat Keranjang
              </button>
            </Link>
            <div className="c-description w-full modal-related-product">
                <div className="description-title" style={{ color: '#696969' }}>
                  REKOMENDASI UNTUK ANDA
                  <div className="description-divider" />
                </div>
                <div style={{ display: 'flex', justifyContent: 'space-between', flexWrap: 'wrap' }}>
                  {
                    relatedProducts.map((v) => (
                      <CardProduct data={v} loading={loading} />
                    ))
                  }
                </div>
              </div>
          </div>
        </Modal>
      </BaseLayout>
      <style jsx>
        {`
        .breadcumb {
          font-size: 14px;
          color: #696969;
        } 
      
        .c {
          width: 100%;
          display: flex;
          flex-wrap: wrap;
          justify-content: center;
        }
      
        .c-content {
          display: flex;
        }
      
        .img {
          width: 100%;
          object-fit: cover;
        }
      
        .c-slider-image {
          width: 400px;
        }
      
        .c-content-information {
          width: 100%;
          padding: 8px 0px 8px 56px;
        }
      
        .product-name {
          font-weight: bold;
          font-size: 24px;
          letter-spacing: 0.05em;
          color: #696969;
        }
      
        .product-merchant {
          font-weight: 500;
          font-size: 20px;
          color: #696969;
        }
      
        .comments {
          font-size: 14px;
          line-height: 13px;
          color: #868686;
          margin-left: 12px;
        }
      
        .product-price {
          font-weight: 500;
          font-size: 22px;
          color: #010101;
        }
        
        .product-tersedia {
          font-size: 12px;
          color: #6F8EFF;
        }
      
        .btn-add-min {
          background: #FFFFFF;
          border: 1px solid #F1F1F1;
          box-sizing: border-box;
          padding: 10px;
          display: flex;
          flex-wrap: wrap;
          align-items: center;
        }
      
        .form-qty {
          background: #FFFFFF;
          border: 1px solid #F1F1F1;
          box-sizing: border-box;
          font-weight: 500;
          font-size: 22px;
          color: #979797;
          padding: 10px;
          width: 70px;
          text-align: center;
        }
      
        .btn-add-to-cart {
          border-radius: 0px;
          font-size: 16px;
          padding: 8px 26px;
          width: auto;
          margin-left: 24px;
        }
      
        .btn-like {
          background: #F5F5F5;
          border: 0px;
          padding: 8px 12px;
          margin-left: 24px;
        }
        .active-like{
          
        }
      
        .product-desc {
          font-size: 18px;
          color: #696969;
        }
      
        .c-description {
          margin-top: 48px;
        }
      
        .description-title {
          font-weight: bold;
          font-size: 22px;
          line-height: 21px;
          color: #010101;
          text-align: center;
          cursor: pointer;
        }
        
        .description-divider {
          height: 4px;
          background: #010101;
          border-radius: 8px;
          width: 300px;
          margin: 12px auto;
        }
      
        .description-text {
          font-size: 18px;
          color: #696969;
          line-height: 22px;
        }
        .btn-select-variant{
          padding: 5px 10px;
          border:1px solid #f1f1f1;
          margin-right: 10px;
          color:#979797;
        }
        .active-variant{
          background: #010101;
          color:#fff;
        }
        .tab-title-wrapper{
          width: 50%;
          margin:0 auto;
          position:relative;
        }
        .price-discount{
          color:#BDBDBD;
          text-decoration: line-through;
          margin-right: 20px;
        }
        .close-button{
          position:absolute;
          right:10px;
          top:10px;
        }
        .product-request-wrapper{
          margin: 15px 0;
        }
        .product-request-wrapper input{
          margin-right:15px;
        }
        .product-request-wrapper label{
          display:inline-block;
          margin-right: 25px;
        }
        .product-detail-content{
          padding: 20px 0;
        }
        .breadcrumb-wrapper{
          padding: 20px 0;
        }
        .variant-option{
          padding:5px 15px;
          border:1px solid #ddd;
          margin-right:15px;
          cursor:pointer;
          font-size:14px;
          box-shadow:0px 2px 4px rgba(0,0,0,0.17);
        }
        .variant-option.active{
          background:#010101;
          color:#fff;
        }
        .information-content{
          margin-top:20px;
          line-height:25px;
        }
        @media (min-width: 320px) and (max-width: 767px) {
          .product-detail-content{
              padding: 0;
          }
          .breadcrumb-wrapper{
            padding: 20px;
          }
          .c-content-information{
            padding:30px 0 0;
          }
          .tab-title-wrapper{
            width:100%;
          }
          .description-divider{
            width: 100%;
          }
          .wide-modal {
            width: 90%;
            height: 80%;
          }
          .card-modal{
            height:100%;
            overflow-y: auto;
          }
          .btn-add-to-cart{
            margin-left: 0;
          }
          .form-qty{
            padding: 0 10px;
            font-size: 16px;
          }
        }
        `}
      </style>
    </div>
  );
};

Product.propTypes = {
  slug: PropTypes.string,
  product: PropTypes.object,
  relatedProducts: PropTypes.array,
  token: PropTypes.string
};

Product.getInitialProps = async (ctx) => {
  const { slug } = ctx.query;
  const { token } = ctx.store.getState().authentication;
  const api = axiosSetup(token);

  const productParam = `layout_type=detail_layout&slug=${slug}`;
  const productResp = await api.get('/products?' + productParam);
  const product = productResp.data.data;

  const sellerDetailResp = await getSellerDetail(token, product.seller.slug);
  const sellerDetail = sellerDetailResp.data;

  console.log(sellerDetail)

  const relatedProductParam = `layout_type=list_layout&type_slug=${product.product_type_slug}&status=active&page=1&limit=4&order=date,DESC`;
  const relatedProductResp = await api.get('/products?' + relatedProductParam);
  const relatedProducts = relatedProductResp.data.data;

  const productTypeResp = await getProductType();
  const productType = productTypeResp.data;

  const cartResp = await getCart(token);
  const cart = cartResp.data ? cartResp.data.cart : [];

  return {
    slug,
    product,
    relatedProducts,
    token,
    productType,
    cart
  };
};

export default Product;
