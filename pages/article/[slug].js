import React from 'react';
import PropTypes from 'prop-types';
import BaseLayout from '~/components/BaseLayout';
import CardArticle from '~/components/ArticleSection/CardArticle';
import { getArticleDetail, getArticles } from '~/data/api/article';
import { getProductType } from '~/data/api/product-type';
import moment from 'moment';
import { getCart } from '~/data/api/cart';

const articles = [
  {
    image: '/images/dummy_article3.png',
    title: 'Teknik Kopi Terbaik',
    date: '09 July 2020',
    author: 'admin',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tortor tortor, dapibus non luctus ut, efficitur a tortor. Proin diam diam, dignissim nec leo sit amet, eleifend egestas lorem.',
    likes: '7',
    slug: 'slug-article'
  },
  {
    image: '/images/dummy_article3.png',
    title: 'Teknik Kopi Terbaik',
    date: '09 July 2020',
    author: 'admin',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tortor tortor, dapibus non luctus ut, efficitur a tortor. Proin diam diam, dignissim nec leo sit amet, eleifend egestas lorem.',
    likes: '7',
    slug: 'slug-article'
  },
  {
    image: '/images/dummy_article3.png',
    title: 'Teknik Kopi Terbaik',
    date: '09 July 2020',
    author: 'admin',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tortor tortor, dapibus non luctus ut, efficitur a tortor. Proin diam diam, dignissim nec leo sit amet, eleifend egestas lorem.',
    likes: '7',
    slug: 'slug-article'
  }
];

const Category = ({
  slug, productType, token, article, listArticle, cart
}) => {
  const data = {};

  return (
    <div>
      <BaseLayout productType={productType} token={token} cart={cart}>
        <div id="breadcumb" className="breadcrumb">
          <div className="breadcumb">
            Home &#x0226B; Baca - Baca &#x0226B;
            {' '}
            <span style={{ color: '#010101' }}>{slug}</span>
          </div>
        </div>

        <div className="c-content flex flex-wrap">
          <div className="w-5/6 xs:w-full sm:w-full">
            <div className="c-products" style={{ marginTop: '24px' }}>

              <div className="c-content-products">
                <div className="c-title">
                  {article ? article.title : "-"}
                </div>
                <div className="c-date">
                  {article ? moment(article.publish_date).format("DD MMMM YYYY") : "-"} / Oleh :
                  <span style={{ color: '#010101' }}>{article ? article.author_name : "-"}</span>
                </div>
                <div className="c-image">
                  <img src={article ? article.image_url : '/images/dummy_article3.png'} alt="" />
                </div>
                <div className="c-description">
                  {article ? article.content : "-"}
                </div>
                <div className="c-author">
                  <img src={article ? article.author_image : "/images/dummy_customer.png"} alt="" />
                  {article ? article.author_name : "-"}
                </div>
              </div>
            </div>
          </div>
          {/* <div className=" w-1/6 xs:w-full sm:w-full">
            <div className="c-filter">
              <div style={{
                display: 'flex', justifyContent: 'space-between', alignItems: 'center', margin: '8px 0px'
              }}
              >
                <span className="title-filter">Terpopuler</span>
              </div>
              <div className="divider-filter" />
              <div className="c-filtered">
                <div className="c-popular-title">
                  Jangan Sampai Salah, Kenali Cara Memilih Kopi Kesukaanmu
                </div>
                <div className="c-popular-title" style={{ color: '#A5A5A5', marginTop: '6px' }}>
                  09 Desember 2019
                </div>
              </div>
            </div>
          </div> */}
        </div>

        <div className="c-description">
          <div className="description-title" style={{ color: '#696969' }}>
            ARTIKEL LAINNYA
            <div className="description-divider" />
          </div>
          <div className="flex flex-wrap">
            {
              listArticle.map((v) => (
                <CardArticle data={v} />
              ))
            }
          </div>
        </div>
      </BaseLayout>
      <style jsx>
        {`
        .breadcumb {
          font-size: 14px;
          color: #696969;
          padding: 20px 0;
        }
      
        .c-content {
          width: 100%;
          display: flex;
        }
      
        .c-filter {
          width: 100%;
          display: flex;
          flex-wrap: wrap;
          justify-content: flex-start;
          flex-direction: column;
          padding-top: 48px;
        }
      
        .title-filter {
          font-weight: 500;
          font-size: 18px;
          color: #696969;
        }
      
        .c-products {
          width: 100%;
          padding: 0px 48px 0px 0;
        }

        .c-content-products {
          display: flex;
          flex-wrap: wrap;
          padding-bottom: 48px;
        }
        .divider-filter {
          height: 1px;
          margin: 8px 0px;
          background: #F2F1F1;
        }

        .c-title {
          width: 100%;
          font-weight: bold;
          font-size: 24px;
          letter-spacing: 0.05em;
          color: #2E2E2E;
        }

        .c-date {
          margin-top: 16px;
          width: 100%;
          font-weight: 500;
          font-size: 16px;
          letter-spacing: 0.05em;
          color: #A5A5A5;
        }
        
        .c-image {
          margin-top: 16px;
          width: 100%;
          max-height: 504px;
        }

        .c-image img {
          width: 100%;
          max-height: 504px;
          object-fit: cover;
        }

        .c-description {
          width: 100%;
          margin-top: 16px;
          font-size: 18px;
          line-height: 32px;
          letter-spacing: 0.05em;
          color: #696969;
          text-align: justify;
          white-space:pre-line;
        }

        .c-author {
          display: flex;
          align-items: center;
          font-weight: 500;
          font-size: 18px;
          line-height: 22px;
          letter-spacing: 0.05em;
          color: #4B4B4B;
          background: #FFFFFF;
          border: 1px solid #E7E7E7;
          box-sizing: border-box;
          padding: 8px;
          margin-top: 24px;
        }
        .c-author img {
          width: 50px;
          height: 50px;
          margin-right: 16px;
        }

        .c-description {
          margin-top: 48px;
        }
      
        .description-title {
          font-weight: bold;
          font-size: 22px;
          line-height: 21px;
          color: #010101;
          text-align: center;
        }
        
        .description-divider {
          height: 4px;
          background: #010101;
          border-radius: 8px;
          width: 300px;
          margin: 12px auto;
        }

        .c-filtered {
          padding-top: 8px;
        }

        .c-popular-title {
          font-size: 16px;
          line-height: 19px;
          letter-spacing: 0.05em;
          color: #696969;
        }
        .c-content{
          padding:0;
        }
        @media (min-width: 320px) and (max-width: 767px) {
          .c-content{
            padding: 0 20px;
            margin-bottom: 30px;
          }
          .container{
            max-width:100% !important;
          }
          .c-products{
            padding: 0;
          }
          .c-content-products{
            padding-bottom:20px;
          }
          .c-filter{
            padding-top:10px;
          }
          .c-description{
            margin-top:20px;
          }
          .breadcumb {
            padding: 0 20px;
          }
        }
        `}
      </style>
    </div>
  );
};

Category.propTypes = {
  slug: PropTypes.string
};

Category.getInitialProps = async (ctx) => {
  const { slug } = ctx.query;
  const { token } = ctx.store.getState().authentication;

  const productTypeResp = await getProductType();
  const productType = productTypeResp.data;

  const articleResp = await getArticleDetail(token, slug);
  const article = articleResp.data;

  const listArticleResp = await getArticles(token, 'created_at', 'desc', '1', '3');
  const listArticle = listArticleResp.data.filter(item => item.slug !== slug);

  const cartResp = await getCart(token);
  const cart = cartResp.data ? cartResp.data.cart : [];

  return {
    slug,
    productType,
    token,
    article,
    listArticle,
    cart
  };
};

export default Category;
