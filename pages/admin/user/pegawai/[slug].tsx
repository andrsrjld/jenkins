import {useEffect, useState, useRef} from 'react'
import { connect } from 'react-redux'
import BackOfficeBase from 'components/LakkonOfficeBase'
import { uploadImage } from 'data/api/image'
import { postPegawai, TRequestUpdate, getOnePegawai, updatePegawai } from 'data/api/pegawai'
import Loading from 'components/Loading'
import moment from 'moment'
import Router from 'next/router'
import { getRole } from 'data/api/role'

type TProp = {
  user: any,
  cloudinaryUrl: string,
  apiUrl: string,
  token: string,
  paramRoles: any,
  paramPegawai: any
}

const dashboard = ({user, cloudinaryUrl, apiUrl, token, paramRoles, paramPegawai} : TProp) => {
  const authorRef = useRef(null)
  const thumbnailRef = useRef(null)
  const [loading, setLoading] = useState(false)

  const [roles, setRoles] = useState(paramRoles)
  const [location, setLocation] = useState(paramPegawai.location)
  const [name, setName] = useState(paramPegawai.name)
  const [password, setPassword] = useState('')
  const [phone, setPhone] = useState(paramPegawai.phone)
  const [role, setRole] = useState(paramPegawai.role.id)
  const [username, setUsername] = useState(paramPegawai.username)

  const handleSave = async () => {
    console.log(user)
    setLoading(true)
    let request : TRequestUpdate = {
      id: paramPegawai.id,
      location: location,
      name: name,
      phone: phone,
      role_id: Number(role),
      username: username,
    }

    try {
      // Update Account
      const res = await updatePegawai(apiUrl, token, request)
      Router.push('/admin/user/pegawai')
    } catch (err) {
      // TODO HANDLE ERR
    }

    setLoading(false)
  }

  const getBase64 = (file, cb) =>{
    let reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = function () {
        cb(reader.result)
    }
    reader.onerror = function (error) {
        console.log('Error: ', error)
    }
  }

  return (
    <BackOfficeBase>
      <Loading show={loading}/>
      <div className="w-full flex flex-wrap">
        <div className="w-full flex">
          <div className="w-full font-medium text-2xl text-text-primary">
            Pegawai
          </div>
          <div className="w-full">
            <div className="flex justify-end">
              <button className="bg-green-500 text-white font-bold px-5 py-1 mx-1 rounded" onClick={() => handleSave()}>SAVE PEGAWAI</button>
            </div>
          </div>
        </div>
        <div className="w-full text-text-primary">
          <div className="bg-white shadow-lg py-12 mt-12 rounded-lg flex justify-center flex-wrap">
            <div className="w-1/2">
              
              <div id="storeName" className="flex mt-8">
                <label className="w-1/2 text-right">Group<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <select className="w-full border border-gray-300" value={role} onChange={(e) => setRole(e.target.value)}>
                    <option value="" disabled>Pilih Group</option>
                    {
                      roles.map(v => (
                        <option key={v.id} value={v.id}>{v.name}</option>
                      ))
                    }
                  </select>
                </div>
              </div>

              <div id="storeName" className="flex mt-8">
                <label className="w-1/2 text-right">Name<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <input type="text" className="border border-gray-300 px-4 py-2 w-full" placeholder="" value={name} onChange={(e) => setName(e.target.value)}/>
                </div>
              </div>

              <div id="storeName" className="flex mt-8">
                <label className="w-1/2 text-right">Username<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <input type="text" className="border border-gray-300 px-4 py-2 w-full" placeholder="" value={username} onChange={(e) => setUsername(e.target.value)}/>
                </div>
              </div>

              {/* <div id="storeName" className="flex mt-8">
                <label className="w-1/2 text-right">Password<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <input type="password" className="border border-gray-300 px-4 py-2 w-full" placeholder="" value={password} onChange={(e) => setPassword(e.target.value)}/>
                </div>
              </div> */}

              <div id="storeName" className="flex mt-8">
                <label className="w-1/2 text-right">Phone<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <input type="text" className="border border-gray-300 px-4 py-2 w-full" placeholder="" value={phone} onChange={(e) => setPhone(e.target.value)}/>
                </div>
              </div>

              <div id="storeName" className="flex mt-8">
                <label className="w-1/2 text-right">Location<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <input type="text" className="border border-gray-300 px-4 py-2 w-full" placeholder="" value={location} onChange={(e) => setLocation(e.target.value)}/>
                </div>
              </div>
          
            </div>

          </div>

        </div>
      </div>
    </BackOfficeBase>
  )
}

dashboard.getInitialProps = async (ctx) => {
  const { endpoint } = ctx.store.getState().authentication
  const { user, token } = ctx.store.getState().lakkonState
  const slug = ctx.query.slug ? ctx.query.slug : ''

  const roles = await getRole(endpoint.apiUrl, token)
  const pegawai = await getOnePegawai(endpoint.apiUrl, token, slug)

  return {
      user: user,
      cloudinaryUrl: endpoint.cloudinaryUrl,
      apiUrl: endpoint.apiUrl,
      token: token,
      paramRoles: roles.data ? roles.data : [],
      paramPegawai: pegawai.data ? pegawai.data : null
  }
}

export default connect(
  (state) => state, {}
)(dashboard);