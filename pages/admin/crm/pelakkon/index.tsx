import { useEffect, useState } from 'react'
import Link from 'next/link'
import { connect } from 'react-redux'
import BackOfficeBase from 'components/LakkonOfficeBase'
import ContentHeader from 'components/BackOfficeBase/ContentHeader'
import Modal from 'components/Modal'
import Loading from 'components/Loading'
import Select from 'react-select'

import { getArticles,deleteArticle, SORTING, STATUS, getArticlesManagement, updateStatusArticle } from 'data/api/article'
import { formatNumber } from 'util/helper'
import { getPelakkon, deletePelakkon, postPelakkon, updatePelakkon } from 'data/api/pelakkon'
import { getSellers } from 'data/api/seller'
import { getProducts, STATE, ORDERTYPE, ORDER } from 'data/api/product'

const breadcumb = [
  {
    title: 'Home',
    link: '/',
  },
  {
    title: 'CRM',
    link: '/',
  },
  {
    title: 'Pelakkon',
    link: '/',
  },
]

type TProps = {
  apiUrl: string,
  token: string,
  authentication: any
}

const Status = ({status}) => {
  switch (status) {
    case STATUS.APPROVED:
      return (
        <div className="bg-green-500 text-white px-3 py-1 text-center font-medium rounded-md text-sm">
          {status}
        </div>
      )
    case STATUS.DRAFT:
      return (
        <div className="bg-gray-400 text-text-primary px-3 py-1 text-center font-medium rounded-md text-sm">
          {status}
        </div>
      )
    default:
      return (
        <div className="bg-primary text-white px-3 py-1 text-center font-medium rounded-md text-sm">
          {status}
        </div>
      )
  }
}

const Product = ({apiUrl, authentication, token}: TProps) => {
  const [page, setPage] = useState(1)
  const [limit, setLimit] = useState(10)
  const [keyword, setKeyword] = useState('')
  const [tabledDatas, setTableDatas] = useState([])
  const [loading, setLoading] = useState(false)
  const [modalAdd, setModalAdd] = useState(false)

  const [sellers, setSellers] = useState([])
  const [selectedSeller, setSelectedSeller] = useState(null)
  const [products, setProducts] = useState([])
  const [selectedProduct, setSelectedProduct] = useState(null)
  const [urutan, setUrutan] = useState('')

  const [selectedData, setSelectedData] = useState(null)

  useEffect(() => {
    doGetArticles()
    doGetSellers()
  }, [page]);

  // useEffect(() => {
  //   if(selectedSeller) {
  //     doGetProducts()
  //   } else {
  //     setProducts([])
  //   }
  // },[selectedSeller])

  useEffect(() => {
    if(!modalAdd) {
      setSelectedData(null)
    }
  }, [modalAdd]);

  useEffect(() => {
    if(selectedData) {
      console.log(sellers)
      setSelectedSeller(sellers.find(v => v.value == selectedData.seller_id))
      setUrutan(selectedData.urutan)
      setModalAdd(true)
    }
  }, [selectedData]);

  const doGetSellers = async () => {
    const res = await getSellers(token, 'store_name,ASC', '1', `100`, 'Approved')
    setSellers(res.data.map(v => ({...v, label: v.seller.store_name, value: v.seller.id})))
  }

  const doGetProducts = async () => {
    const res =await getProducts(apiUrl, {
      layout_type: 'list_layout',
      seller_slug: selectedSeller.value,
      type_slug: 'All',
      keyword: '',
      status: STATE.ACTIVE,
      origin: '',
      species: '',
      tasted: '',
      roast_level: '',
      price: '',
      page: page,
      limit: limit,
      order: `${ORDERTYPE.DATE},${ORDER.DESC}`
    }, '')
    setProducts(res.data ? res.data.map(v => ({...v, label: v.name, value: v.id})) : [])
  }

  const doGetArticles = async () => {
    setLoading(true)
    const res = await getPelakkon(apiUrl, token, {
      layout_type: 'list_layout',
      limit: limit,
      page: page
    })
    setTableDatas(res.data ? res.data.map(v => ({...v, isChecked: false})) : [])
    setLoading(false)
  }

  const handleSave = async () => {
    setLoading(true)
    setModalAdd(false)
    if(selectedData) {
      //edit
      const res = await updatePelakkon(apiUrl, token, {
        id: selectedData.id,
        urutan: Number(urutan)
      })
    } else {
      //add
      const res = await postPelakkon(apiUrl, token, {
        seller_id: selectedSeller.value,
        urutan: Number(urutan)
      })
    }
    
    setSelectedSeller(null)
    setSelectedProduct(null)
    setUrutan('')
    setLoading(false)

    doGetArticles()
  }

  const handleDelete = async (id) => {
    setLoading(true)
    const res = await deletePelakkon(apiUrl, token, id)
    doGetArticles()
  }

  const handleOpenModal = () => {
    setModalAdd(true)
  }

  return (
    <BackOfficeBase>
      <Loading show={loading} />
      <div className="w-full flex flex-wrap">
        <ContentHeader title="Pelakkon" breadcumb={breadcumb} />

        <div className="w-full flex justify-end mt-4">
          <button className="bg-orange-500 text-white font-bold px-5 py-2 mx-1 rounded" onClick={() => handleOpenModal()}>NEW PELAKKON</button>
        </div>

        <div className="w-full">
          <div className="bg-white shadow-lg p-4 mt-4 rounded-lg flex flex-wrap">

            <div id="table" className="w-full mt-8">
              <table className="w-full">
                <thead>
                  <th>ID</th>
                  <th>Urutan</th>
                  <th>Seller</th>
                  <th className="text-center">Action</th>
                </thead>
                <tbody>
                  {
                    tabledDatas.map((v, i) => (
                      <tr key={v.id} className={`${i%2 == 0 ? 'bg-gray-200' : 'bg-white'} cursor-pointer hover:bg-gray-300`} onClick={(e) => {
                        const target : any = e.target;
                        if(target.id === 'checkbox') return
                        // window.location.href = `/backoffice/products/detail/${v.slug}`
                      }}>
                        <td>
                          {v.id}
                        </td>
                        <td>
                          {v.urutan}
                        </td>
                        <td>
                          {v.seller_name}
                        </td>
                        <td className="text-center">
                          <button className="px-2 py-2 border border-primary rounded bg-white mx-1 hover:bg-gray-200" onClick={() => setSelectedData(v)}>
                            <img src="/images/ic_lakkon_edit_red.png" alt="" style={{ width: '24px', height: '24px' }} />
                          </button>
                          <button className="px-2 py-2 border border-primary rounded bg-white mx-1 hover:bg-gray-200" onClick={() => handleDelete(v.id)}>
                            <img src="/images/ic_trash.png" alt="" style={{ width: '24px', height: '24px' }} />
                          </button>
                        </td>
                      </tr>
                    ))
                  }
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

      <Modal open={modalAdd} onClose={() => setModalAdd(false)}>
        <div className="bg-white rounded shadow" style={{ padding: '16px 26px', minWidth: '400px' }}>
          <div style={{ textAlign: 'right' }}>
            <div style={{ display: 'inline-block' }} onClick={() => setModalAdd(false)} onKeyDown={() => setModalAdd(false)} className="pointer" role="button" tabIndex={0}>
              <img src="/images/ic_close_red.png" alt="" />
            </div>
          </div>
          <div className="w-full flex flex-wrap mt-4">
            <div className="w-full">
              <Select 
                options={sellers}
                placeholder="Pilih Seller"
                value={selectedSeller}
                onChange={(e) => setSelectedSeller(e)}
                isDisabled={selectedData != null}
              />
            </div>
            <div className="w-full mt-4">
              <input className="w-full border border-gray-400 py-2 px-4 rounded" type="number" placeholder="Urutan" value={urutan} onChange={(e) => setUrutan(e.target.value)}/>
            </div>
            <div className="w-full mt-6">
              <button className="w-full rounded py-3 px-4 bg-primary text-white font-medium" onClick={() => handleSave()}>Simpan</button>
            </div>
          </div>
        </div>
      </Modal>
    </BackOfficeBase>
  )
}

Product.getInitialProps = (ctx) => {
  const { endpoint } = ctx.store.getState().authentication;
  const { user, token } = ctx.store.getState().lakkonState
  return {
      apiUrl: endpoint.apiUrl,
      token: token
  }
}

export default connect(
  (state) => state, {}
)(Product);