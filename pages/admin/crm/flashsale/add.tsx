import {useEffect, useState, useRef} from 'react'
import { connect } from 'react-redux'
import BackOfficeBase from 'components/LakkonOfficeBase'
import { uploadImage } from 'data/api/image'
import { updateAccount, TRequest } from 'data/api/seller'
import Loading from 'components/Loading'
import { getShippingList, addShipping, deleteShipping } from 'data/api/shipping'
import { api } from 'data/api/base'

type TProp = {
  user: any,
  cloudinaryUrl: string,
  apiUrl: string,
  token: string
}

const dashboard = ({user, cloudinaryUrl, apiUrl, token} : TProp) => {
  const logoRef = useRef(null)
  const bannerRef = useRef(null)
  const [loading, setLoading] = useState(false)

  const [images, setImages] = useState([])
  const [logoImage, setLogoImage] = useState(user.seller.photo ? { base64:user.seller.photo, file: null } : null)
  const [bannerType, setBannerType] = useState('')
  const [bannerImage, setBannerImage] = useState(user.seller.url_banner ? { base64:user.seller.url_banner, file: null } : null)
  const [storeName, setStoreName] = useState(user.seller.store_name)
  const [storeDescription, setStoreDescription] = useState(user.seller.about_us)
  const [storePhone, setStorePhone] = useState(user.seller.phone)
  const [storeHours, setStoreHours] = useState(user.seller.operation_hours)

  const handleSaveConfig = async () => {
    setLoading(true)
    let request : TRequest = {
      about_us: storeDescription,
      auto_chat: '',
      banner_type: bannerType,
      operation_hours: storeHours,
      phone: storePhone,
      store_name: storeName,
      photo: '',
      url_banner: ''
    }

    try {

      // Upload Logo IF any
      if(logoImage.file) {
        const resLogo = await uploadImage(cloudinaryUrl, {
          file: logoImage.file,
          folder: 'logo',
          tag: ''
        })
        request.photo = resLogo.secure_url
      }

      //Upload Banner IF any
      if(bannerImage?.file) {
        const resBanner = await uploadImage(cloudinaryUrl, {
          file: bannerImage.file,
          folder: 'banner',
          tag: ''
        })
        request.url_banner = resBanner.secure_url
      }

      // Update Account
      const res = await updateAccount(apiUrl, token, request)
      console.log(res)
    } catch (err) {
      // TODO HANDLE ERR
    }

    setLoading(false)
  }

  const getBase64 = (file, cb) =>{
    let reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = function () {
        cb(reader.result)
    }
    reader.onerror = function (error) {
        console.log('Error: ', error)
    }
  }

  const handleChangeLogoImage = (e) => {
    const file = e.target.files[0]
    getBase64(file, (result) => {
      setLogoImage({base64: result, file: file})
    });
  }

  const handleChangeBannerImage = (e) => {
    const file = e.target.files[0]
    getBase64(file, (result) => {
      setBannerImage({base64: result, file: file})
    });
  }
  return (
    <BackOfficeBase>
      <Loading show={loading}/>
      <div className="w-full flex flex-wrap">
        <div className="w-full flex">
          <div className="w-full font-medium text-2xl text-text-primary">
            Flashsale
          </div>
          <div className="w-full">
            <div className="flex justify-end">
              <button className="bg-green-500 text-white font-bold px-5 py-1 mx-1 rounded" onClick={() => {}}>SAVE FLASHSALE</button>
            </div>
          </div>
        </div>
        <div className="w-full text-text-primary">
          <div className="bg-white shadow-lg py-12 mt-4 rounded-lg flex justify-center flex-wrap">
            <div className="w-1/2">
              
              <div id="storeName" className="flex mt-8">
                <label className="w-1/2 text-right">Start Date<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <input type="date" className="border border-gray-300 px-4 py-2 w-full" placeholder=""/>
                </div>
              </div>

              <div id="storeName" className="flex mt-8">
                <label className="w-1/2 text-right">End Date<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <input type="date" className="border border-gray-300 px-4 py-2 w-full" placeholder=""/>
                </div>
              </div>

              <div id="storeName" className="flex mt-8">
                <label className="w-1/2 text-right">Price Before Discount<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <input type="text" className="border border-gray-300 px-4 py-2 w-full" placeholder="Rp"/>
                </div>
              </div>

              <div id="storeName" className="flex mt-8">
                <label className="w-1/2 text-right">Price After Discount<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <input type="text" className="border border-gray-300 px-4 py-2 w-full" placeholder="Rp"/>
                </div>
              </div>

              <div id="logo" className="flex mt-8">
                <label className="w-1/2 text-right">Thumbnail<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <div className="border-2 border-gray-200 w-10 h-10">
                    <img className="w-full h-full object-cover" src={logoImage ? '' : ''} alt="logo image"/>
                  </div>
                  <div className="text-sm ">
                    <button className="border-2 bg-white px-3 rounded-lg mt-2" onClick={() => {logoRef.current.click()}} >Browse...</button>
                    {`${logoImage ? '' : ' No File Selected'}`}
                    <input ref={logoRef} className="hidden" type="file" name="logo" onChange={handleChangeLogoImage}/>
                  </div>
                  {/* <input type="checkbox" name="delete" id="delete" className="mt-2"/> Delete Image */}
                </div>
              </div>
          
            </div>

          </div>

        </div>
      </div>
    </BackOfficeBase>
  )
}

dashboard.getInitialProps = async (ctx) => {
  const { token, endpoint, user } = ctx.store.getState().authentication

  return {
      user: user,
      cloudinaryUrl: endpoint.cloudinaryUrl,
      apiUrl: endpoint.apiUrl,
      token: token
  }
}

export default connect(
  (state) => state, {}
)(dashboard);