import {useEffect, useState, useRef} from 'react'
import { connect } from 'react-redux'
import BackOfficeBase from 'components/LakkonOfficeBase'
import { uploadImage } from 'data/api/image'
import Loading from 'components/Loading'
import { api } from 'data/api/base'
import Router from 'next/router'
import { TRequest, VOUCHER_TYPE, postVoucher, getVoucherDetail, updateVoucher, TRequestUpdate, ENUM_VOUCHER_TYPE } from 'data/api/voucher'
import moment from 'moment'
import { formatCurrencyToNumber, formatCurrencyToPercent, formatNumber } from 'util/helper'

type TProp = {
  user: any,
  cloudinaryUrl: string,
  apiUrl: string,
  token: string,
  voucher: any
}

const dashboard = ({user, cloudinaryUrl, apiUrl, voucher, token} : TProp) => {
  const logoRef = useRef(null)
  const bannerRef = useRef(null)
  const [loading, setLoading] = useState(false)

  const [name, setName] = useState(voucher.name)
  const [code, setCode] = useState(voucher.code)
  const [type, setType] = useState(voucher.voucher_type == VOUCHER_TYPE.TRANSACTION ? VOUCHER_TYPE.TRANSACTION : VOUCHER_TYPE.SHIPPING)
  const [typeDiscount, setTypeDiscount] = useState(voucher.enum_discount_type == ENUM_VOUCHER_TYPE.NOMINAL ? ENUM_VOUCHER_TYPE.NOMINAL : ENUM_VOUCHER_TYPE.PERSENTASE)
  const [discount, setDiscount] = useState(voucher.discount)
  const [minimum, setMinimum] = useState(voucher.minimum_spend)
  const [maximum, setMaximum] = useState(voucher.max_discount)
  const [limit, setLimit] = useState(voucher.use_limit)
  const [startDate, setStartDate] = useState(moment(voucher.start_date).format('YYYY-MM-DD'))
  const [endDate, setEndDate] = useState(moment(voucher.end_date).format('YYYY-MM-DD'))
  const [notes, setNotes] = useState(voucher.notes)
  const [paymentMethod, setPaymentMethod] = useState(voucher.payment_method)

  const [images, setImages] = useState([])
  const [logoImage, setLogoImage] = useState(null)
  const [bannerType, setBannerType] = useState('')
  const [bannerImage, setBannerImage] = useState(null)

  const [showTitle, setShowTitle] = useState(voucher.parsedNote.title)
  const [showTerm, setShowTerm] = useState(voucher.parsedNote.term)
  const [showDate, setShowDate] = useState(voucher.parsedNote.date)

  const handleSave = async () => {
    const _note = {
      title: showTitle,
      term: showTerm,
      date: showDate
    }

    setLoading(true)
    let request : TRequestUpdate = {
      id: voucher.id,
      code: code,
      discount: Number(discount),
      enum_discount_type: typeDiscount,
      voucher_type: type,
      use_limit: Number(limit),
      minimum_spend: Number(minimum),
      payment_method: paymentMethod,
      max_discount: Number(maximum),
      name: name,
      notes: JSON.stringify(_note),
      start_date: moment(startDate).format('YYYY-MM-DD HH:mm:ss'),
      end_date: moment(endDate).format('YYYY-MM-DD HH:mm:ss'),
    }

    try {
      // Update Account
      const res = await updateVoucher(apiUrl, token, request)
      Router.push('/admin/crm/voucher')
    } catch (err) {
      // TODO HANDLE ERR
    }

    setLoading(false)
  }

  const getBase64 = (file, cb) =>{
    let reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = function () {
        cb(reader.result)
    }
    reader.onerror = function (error) {
        console.log('Error: ', error)
    }
  }

  const handleChangeLogoImage = (e) => {
    const file = e.target.files[0]
    getBase64(file, (result) => {
      setLogoImage({base64: result, file: file})
    });
  }

  const handleChangeDiscount = (e) => {
    const {value} = e.target
    let val = '';
    if(typeDiscount == ENUM_VOUCHER_TYPE.NOMINAL) {
      val = formatCurrencyToNumber(value)
    } else {
      val =  formatCurrencyToPercent(value)
      if(Number(val) > 100) {
        val = '100'
      }
    }
    setDiscount(val)
  }

  const handleChangeMinimumSpend = (e) => {
    const {value} = e.target
    const val = formatCurrencyToNumber(value)
    setMinimum(val)
  }

  const handleChangeMaximumDiscount = (e) => {
    const {value} = e.target
    const val = formatCurrencyToNumber(value)
    setMaximum(val)
  }

  return (
    <BackOfficeBase>
      <Loading show={loading}/>
      <div className="w-full flex flex-wrap">
        <div className="w-full flex">
          <div className="w-full font-medium text-2xl text-text-primary">
            Voucher
          </div>
          <div className="w-full">
            <div className="flex justify-end">
              <button className="bg-green-500 text-white font-bold px-5 py-1 mx-1 rounded" onClick={() => handleSave()}>SAVE VOUCHER</button>
            </div>
          </div>
        </div>
        <div className="w-full text-text-primary">
          <div className="bg-white shadow-lg py-12 mt-4 rounded-lg flex justify-center flex-wrap">
            <div className="w-1/2">

              <div id="storeName" className="flex mt-8">
                <label className="w-1/2 text-right">Title<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <input type="text" className="border border-gray-300 px-4 py-2 w-full" placeholder="" value={name} onChange={(e) => setName(e.target.value)} disabled/>
                </div>
              </div>

              <div id="storeName" className="flex mt-8">
                <label className="w-1/2 text-right">Code<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <input type="text" className="border border-gray-300 px-4 py-2 w-full" placeholder="" value={code} onChange={(e) => setCode(e.target.value)} disabled/>
                </div>
              </div>

              <div id="storeName" className="flex mt-8">
                <label className="w-1/2 text-right">Voucher Type<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <select className="border border-gray-300 px-4 py-2 w-full" placeholder="" value={type} onChange={(e) => setType(e.target.value == '1' ? VOUCHER_TYPE.TRANSACTION : VOUCHER_TYPE.SHIPPING)} disabled>
                    <option value={VOUCHER_TYPE.TRANSACTION}>Transaksi</option>
                    <option value={VOUCHER_TYPE.SHIPPING}>Shipping</option>
                  </select>
                </div>
              </div>

              <div id="storeName" className="flex mt-8">
                <label className="w-1/2 text-right">Discount Type<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <select className="border border-gray-300 px-4 py-2 w-full" placeholder="" value={typeDiscount} onChange={(e) => setTypeDiscount(e.target.value == '1' ? ENUM_VOUCHER_TYPE.NOMINAL : ENUM_VOUCHER_TYPE.PERSENTASE)} disabled>
                    <option value={ENUM_VOUCHER_TYPE.NOMINAL}>Nominal</option>
                    <option value={ENUM_VOUCHER_TYPE.PERSENTASE}>Persentase</option>
                  </select>
                </div>
              </div>

              <div id="discount" className="flex mt-8">
                <label className="w-1/2 text-right">Discount<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <input type="text" className="border border-gray-300 px-4 py-2 w-full" placeholder="" value={`${typeDiscount == ENUM_VOUCHER_TYPE.NOMINAL ? `Rp ` : ''}${formatNumber(discount)}${typeDiscount == ENUM_VOUCHER_TYPE.PERSENTASE ? `%` : ''}`} onChange={(e) => handleChangeDiscount(e)}/>
                </div>
              </div>

              <div id="storeName" className="flex mt-8">
                <label className="w-1/2 text-right">Minimum Spend<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <input type="text" className="border border-gray-300 px-4 py-2 w-full" placeholder="" value={`Rp ${formatNumber(minimum)}`} onChange={(e) => handleChangeMinimumSpend(e)}/>
                </div>
              </div>

              <div id="storeName" className="flex mt-8">
                <label className="w-1/2 text-right">Maximum Discount<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <input type="text" className="border border-gray-300 px-4 py-2 w-full" placeholder="" value={`Rp ${formatNumber(maximum)}`} onChange={(e) => handleChangeMaximumDiscount(e)}/>
                </div>
              </div>

              <div id="storeName" className="flex mt-8">
                <label className="w-1/2 text-right">Limit<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <input type="text" className="border border-gray-300 px-4 py-2 w-full" placeholder="" value={limit} onChange={(e) => setLimit(e.target.value)} disabled/>
                </div>
              </div>

              <div id="storeName" className="flex mt-8">
                <label className="w-1/2 text-right">Start Date<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <input type="date" className="border border-gray-300 px-4 py-2 w-full" placeholder="" value={startDate} onChange={(e) => setStartDate(e.target.value)}/>
                </div>
              </div>

              <div id="storeName" className="flex mt-8">
                <label className="w-1/2 text-right">End Date<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <input type="date" className="border border-gray-300 px-4 py-2 w-full" placeholder="" value={endDate} onChange={(e) => setEndDate(e.target.value)}/>
                </div>
              </div>

              <div id="storeName" className="flex mt-8">
                <label className="w-1/2 text-right">Payment Type<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <select className="border border-gray-300 px-4 py-2 w-full" placeholder="" value={paymentMethod} onChange={(e) => setPaymentMethod(e.target.value)}>
                    <option value="">All</option>
                    <option value="7">Mandiri Debit Card</option>
                  </select>
                </div>
              </div>

              {/* <div id="storeName" className="flex mt-8">
                <label className="w-1/2 text-right">Notes<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <input type="text" className="border border-gray-300 px-4 py-2 w-full" placeholder="" value={notes} onChange={(e) => setNotes(e.target.value)}/>
                </div>
              </div> */}

              <div id="title" className="flex mt-8">
                <label className="w-1/2 text-right">Title<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <input type="text" className="border border-gray-300 px-4 py-2 w-full" placeholder="" value={showTitle} onChange={(e) => setShowTitle(e.target.value)}/>
                </div>
              </div>

              <div id="term" className="flex mt-8">
                <label className="w-1/2 text-right">Terms and Condition<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <input type="text" className="border border-gray-300 px-4 py-2 w-full" placeholder="" value={showTerm} onChange={(e) => setShowTerm(e.target.value)}/>
                </div>
              </div>

              <div id="date" className="flex mt-8">
                <label className="w-1/2 text-right">Date Condition<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <input type="text" className="border border-gray-300 px-4 py-2 w-full" placeholder="" value={showDate} onChange={(e) => setShowDate(e.target.value)}/>
                </div>
              </div>
            </div>

          </div>

        </div>
      </div>
    </BackOfficeBase>
  )
}

dashboard.getInitialProps = async (ctx) => {
  const { endpoint } = ctx.store.getState().authentication
  const { token, user } = ctx.store.getState().lakkonState
  const {query} = ctx
  const slug = query.slug ? query.slug : ''
  const voucher = await getVoucherDetail(endpoint.apiUrl, token, {
    layout_type: 'detail_layout',
    id: slug
  })

  return {
      user: user,
      cloudinaryUrl: endpoint.cloudinaryUrl,
      apiUrl: endpoint.apiUrl,
      token: token,
      voucher: {...voucher.data, parsedNote: voucher.data.notes.includes('term') ? JSON.parse(voucher.data.notes) : ''}
  }
}

export default connect(
  (state) => state, {}
)(dashboard);