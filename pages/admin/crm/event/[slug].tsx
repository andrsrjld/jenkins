import {useEffect, useState, useRef} from 'react'
import { connect } from 'react-redux'
import BackOfficeBase from 'components/LakkonOfficeBase'
import { uploadImage } from 'data/api/image'
import Loading from 'components/Loading'
import { TRequest, updateEvent, getOneEvent, TRequestUpdate } from 'data/api/event'
import { api } from 'data/api/base'
import Router from 'next/router'
import CropPicker from 'components/CropPicker'
import Modal from 'components/Modal'
import { formatDate } from 'util/helper'

type TProp = {
  user: any,
  cloudinaryUrl: string,
  apiUrl: string,
  token: string,
  iklan: any
}

const dashboard = ({user, cloudinaryUrl, apiUrl, iklan, token} : TProp) => {
  const logoRef = useRef(null)
  const bannerRef = useRef(null)
  const [loading, setLoading] = useState(false)

  const [title, setTitle] = useState(iklan.nama)
  const [description, setDescription] = useState(iklan.deskripsi)
  const [startDate, setstartDate] = useState(formatDate(iklan.start_date, 'YYYY-MM-DD'))
  const [endDate, setendDate] = useState(formatDate(iklan.end_date, 'YYYY-MM-DD'))

  const [images, setImages] = useState([])
  const [logoImage, setLogoImage] = useState(iklan.thumbnail ? {base64: iklan.thumbnail, file: null} : null)

  const [modalCrop, setModalCrop] = useState({
    image: '',
    open: false,
    cb: (props: any) => setLogoImage({base64: props.croppedImage, file: props.blob})
  })

  const handleSave = async () => {
    setLoading(true)
    let request : TRequestUpdate = {
      id: iklan.id,
      end_date: formatDate(endDate, 'YYYY-MM-DD'),
      event_sellers: [],
      nama: title,
      start_date: formatDate(startDate, 'YYYY-MM-DD'),
      status: true,
      deskripsi: description,
    }

    try {

      // Update Account
      const res = await updateEvent(apiUrl, token, request)
      Router.push('/admin/crm/event')
    } catch (err) {
      // TODO HANDLE ERR
    }

    setLoading(false)
  }

  const getBase64 = (file, cb) =>{
    let reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = function () {
        cb(reader.result)
    }
    reader.onerror = function (error) {
        console.log('Error: ', error)
    }
  }

  const handleChangeLogoImage = (e) => {
    const file = e.target.files[0]
    getBase64(file, (result) => {
      // setLogoImage({base64: result, file: file})
      setModalCrop({
        ...modalCrop,
        image: result,
        open: true
      })
    });
  }

  const handleCroppedImage = async (croppedImage, cb) => {
    setLoading(true)
    let blob = await fetch(croppedImage).then(r => r.blob());
    modalCrop.cb({croppedImage, blob})
    setModalCrop({
      ...modalCrop,
      open: false
    })
    setLoading(false)
  }

  return (
    <BackOfficeBase>
      <Loading show={loading}/>
      <div className="w-full flex flex-wrap">
        <div className="w-full flex">
          <div className="w-full font-medium text-2xl text-text-primary">
            Iklan
          </div>
          <div className="w-full">
            <div className="flex justify-end">
              <button className="bg-green-500 text-white font-bold px-5 py-1 mx-1 rounded" onClick={() => handleSave()}>SAVE IKLAN</button>
            </div>
          </div>
        </div>
        <div className="w-full text-text-primary">
          <div className="bg-white shadow-lg py-12 mt-4 rounded-lg flex justify-center flex-wrap">
            <div className="w-1/2">   

              <div id="storeName" className="flex mt-8">
                <label className="w-1/2 text-right">Title<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <input type="text" className="border border-gray-300 px-4 py-2 w-full" placeholder="" value={title} onChange={(e) => setTitle(e.target.value)}/>
                </div>
              </div>              

              <div id="storeName" className="flex mt-8">
                <label className="w-1/2 text-right">Deskripsi<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <input type="text" className="border border-gray-300 px-4 py-2 w-full" placeholder="" value={description} onChange={(e) => setDescription(e.target.value)}/>
                </div>
              </div>

              <div id="storeName" className="flex mt-8">
                <label className="w-1/2 text-right">Start Date<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <input type="date" className="border border-gray-300 px-4 py-2 w-full" placeholder="" value={startDate} onChange={(e) => setstartDate(e.target.value)}/>
                </div>
              </div>    

              <div id="storeName" className="flex mt-8">
                <label className="w-1/2 text-right">End Date<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <input type="date" className="border border-gray-300 px-4 py-2 w-full" placeholder="" value={endDate} onChange={(e) => setendDate(e.target.value)}/>
                </div>
              </div>
              </div>

          </div>

        </div>
      </div>
    </BackOfficeBase>
  )
}

dashboard.getInitialProps = async (ctx) => {
  const { endpoint } = ctx.store.getState().authentication
  const { token, user } = ctx.store.getState().lakkonState
  const {query} = ctx
  const slug = query.slug ? query.slug : ''
  const iklan = await getOneEvent(endpoint.apiUrl, token, {
    id: slug,
    layout_type: 'detail_layout'
  })

  return {
      user: user,
      cloudinaryUrl: endpoint.cloudinaryUrl,
      apiUrl: endpoint.apiUrl,
      token: token,
      iklan: iklan.data
  }
}

export default connect(
  (state) => state, {}
)(dashboard);