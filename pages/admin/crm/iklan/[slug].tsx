import {useEffect, useState, useRef} from 'react'
import { connect } from 'react-redux'
import BackOfficeBase from 'components/LakkonOfficeBase'
import { uploadImage } from 'data/api/image'
import Loading from 'components/Loading'
import { TRequest, postIklan, getOneIklan, TRequestUpdate } from 'data/api/iklan'
import { api } from 'data/api/base'
import Router from 'next/router'
import CropPicker from 'components/CropPicker'
import Modal from 'components/Modal'

type TProp = {
  user: any,
  cloudinaryUrl: string,
  apiUrl: string,
  token: string,
  iklan: any
}

const dashboard = ({user, cloudinaryUrl, apiUrl, iklan, token} : TProp) => {
  const logoRef = useRef(null)
  const bannerRef = useRef(null)
  const [loading, setLoading] = useState(false)

  const [title, setTitle] = useState(iklan.title)
  const [description, setDescription] = useState(iklan.description)
  const [link, setLink] = useState(iklan.link)
  const [action, setAction] = useState(iklan.action)

  const [images, setImages] = useState([])
  const [logoImage, setLogoImage] = useState(iklan.thumbnail ? {base64: iklan.thumbnail, file: null} : null)

  const [modalCrop, setModalCrop] = useState({
    image: '',
    open: false,
    cb: (props: any) => setLogoImage({base64: props.croppedImage, file: props.blob})
  })

  const handleSave = async () => {
    setLoading(true)
    let request : TRequestUpdate = {
      id: iklan.id,
      action: action,
      description: description,
      link: link,
      show: true,
      thumbnail: '',
      title: title,
    }

    try {

      // Upload Logo IF any
      if(logoImage.file) {
        const resLogo = await uploadImage(cloudinaryUrl, {
          file: logoImage.file,
          folder: 'iklan',
          tag: ''
        })
        request.thumbnail = resLogo.secure_url
      }

      // Update Account
      const res = await postIklan(apiUrl, token, request)
      Router.push('/admin/crm/iklan')
    } catch (err) {
      // TODO HANDLE ERR
    }

    setLoading(false)
  }

  const getBase64 = (file, cb) =>{
    let reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = function () {
        cb(reader.result)
    }
    reader.onerror = function (error) {
        console.log('Error: ', error)
    }
  }

  const handleChangeLogoImage = (e) => {
    const file = e.target.files[0]
    getBase64(file, (result) => {
      // setLogoImage({base64: result, file: file})
      setModalCrop({
        ...modalCrop,
        image: result,
        open: true
      })
    });
  }

  const handleCroppedImage = async (croppedImage, cb) => {
    setLoading(true)
    let blob = await fetch(croppedImage).then(r => r.blob());
    modalCrop.cb({croppedImage, blob})
    setModalCrop({
      ...modalCrop,
      open: false
    })
    setLoading(false)
  }

  return (
    <BackOfficeBase>
      <Loading show={loading}/>
      <div className="w-full flex flex-wrap">
        <div className="w-full flex">
          <div className="w-full font-medium text-2xl text-text-primary">
            Iklan
          </div>
          <div className="w-full">
            <div className="flex justify-end">
              <button className="bg-green-500 text-white font-bold px-5 py-1 mx-1 rounded" onClick={() => handleSave()}>SAVE IKLAN</button>
            </div>
          </div>
        </div>
        <div className="w-full text-text-primary">
          <div className="bg-white shadow-lg py-12 mt-4 rounded-lg flex justify-center flex-wrap">
            <div className="w-1/2">

              <div id="storeName" className="flex mt-8">
                <label className="w-1/2 text-right">Title<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <input type="text" className="border border-gray-300 px-4 py-2 w-full" placeholder="" value={title} onChange={(e) => setTitle(e.target.value)}/>
                </div>
              </div>              

              <div id="storeName" className="flex mt-8">
                <label className="w-1/2 text-right">Description<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <input type="text" className="border border-gray-300 px-4 py-2 w-full" placeholder="" value={description} onChange={(e) => setDescription(e.target.value)}/>
                </div>
              </div>         

              <div id="storeName" className="flex mt-8">
                <label className="w-1/2 text-right">Link<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <input type="text" className="border border-gray-300 px-4 py-2 w-full" placeholder="" value={link} onChange={(e) => setLink(e.target.value)}/>
                </div>
              </div>         

              <div id="storeName" className="flex mt-8">
                <label className="w-1/2 text-right">Action<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <input type="text" className="border border-gray-300 px-4 py-2 w-full" placeholder="" value={action} onChange={(e) => setAction(e.target.value)}/>
                </div>
              </div>    

              <div id="logo" className="flex mt-8">
                <label className="w-1/2 text-right">Thumbnail<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <div className="border-2 border-gray-200 w-10 h-10">
                    <img className="w-full h-full object-cover" src={logoImage ? logoImage.base64 : ''} alt="logo image"/>
                  </div>
                  <div className="text-sm ">
                    <button className="border-2 bg-white px-3 rounded-lg mt-2" onClick={() => {logoRef.current.click()}} >Browse...</button>
                    {`${logoImage ? '' : ' No File Selected'}`}
                    <input ref={logoRef} className="hidden" type="file" name="logo" onChange={handleChangeLogoImage}/>
                  </div>
                  {/* <input type="checkbox" name="delete" id="delete" className="mt-2"/> Delete Image */}
                </div>
              </div>
          
            </div>

          </div>

        </div>
      </div>

      <Modal open={modalCrop.open} onClose={() => setModalCrop({...modalCrop, open: false})}>
        <CropPicker 
          ratio={3/2} 
          onClosed={() => setModalCrop({...modalCrop, open: false})} 
          onSubmit={(croppedImage) => handleCroppedImage(croppedImage, modalCrop.cb)} 
          image={modalCrop.image}
          onCrop={() => setLoading(true)}
          onCropped={() => setLoading(false)}
        />
      </Modal>
    </BackOfficeBase>
  )
}

dashboard.getInitialProps = async (ctx) => {
  const { endpoint } = ctx.store.getState().authentication
  const { token, user } = ctx.store.getState().lakkonState
  const {query} = ctx
  const slug = query.slug ? query.slug : ''
  const iklan = await getOneIklan(endpoint.apiUrl, token, {
    id: slug,
    layout_type: 'detail_layout'
  })

  return {
      user: user,
      cloudinaryUrl: endpoint.cloudinaryUrl,
      apiUrl: endpoint.apiUrl,
      token: token,
      iklan: iklan.data
  }
}

export default connect(
  (state) => state, {}
)(dashboard);