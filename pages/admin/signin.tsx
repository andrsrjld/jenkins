import React, { useState } from 'react';
import Router from 'next/router';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { connect } from 'react-redux';
import {
  authenticateLakkon
} from 'redux/actions/authActions';

const Home = ({ authenticateLakkon, apiUrl }) => {
  const router = useRouter();
  const param = router.query;
  
  const [form, setForm] = useState({
    email: '',
    password: ''
  });
  const [isLoading, setLoading] = useState(false);
  const [error, setError] = useState({
    show: false,
    message: 'Username atau password salah'
  });

  const [tooglePass, setTooglePass] = useState(false)


  const doLogin = async () => {
    setLoading(true);
    const body = {
      username: form.email,
      password: form.password
    };
    const res = await authenticateLakkon(body);
    if (res.code === 500 || res.code === 400) {
      setError((v) => ({ ...v, show: true }));
    } else {
      Router.push(param.redirect ? `/${param.redirect}` : '/admin/dashboard');
    }
    setLoading(false);
  };

  const handleChange = (name) => (e) => {
    const { value } = e.target;
    setError((v) => ({ ...v, show: false }));
    setForm((v) => ({ ...v, [name]: value }));
  };

  return (
    <div className="c">
      <Head>
        <title>Bapera</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className="c">
        <div className="w-100 flex justify-center">
          <img className="h-16" src="/images/logo_bapera.png" alt="" />
        </div>

        <div className="container-login">
          <div className="w-full flex justify-between items-end">
          <div className="text-primary-dark text-3xl font-bold">Masuk</div>
          <div className="text-gray-500 text-xl font-bold mb-1">Dashboard</div>
          </div>

          <div style={{ marginTop: '50px' }}>
            <input
              className="form-control w-100"
              placeholder="Nama Pengguna"
              value={form.email}
              onChange={handleChange('email')}
              disabled={isLoading}
            />
          </div>
          <div style={{ marginTop: '20px' }}>
            <div className="form-control w-100 flex items-center">
              <input
                className="w-100 m-0"
                placeholder="Kata Sandi"
                type={`${tooglePass ? '' : "password"}`}
                value={form.password}
                onChange={handleChange('password')}
                disabled={isLoading}
              />
              <div className="cursor-pointer text-primary" onClick={() => setTooglePass(!tooglePass)}>
                {tooglePass ? 'Tutup' : 'Tampilkan'}
              </div>
            </div>
          </div>
          <div className="text-error" style={{ marginTop: '16px', display: error.show ? 'block' : 'none' }}>
            {error.message}
          </div>

          {/* <div className="text-forgot-password">
            Lupa Password?
          </div> */}

          <div style={{ marginTop: '38px' }}>
            <button
              type="button"
              className="btn-primary"
              onClick={doLogin}
              disabled={isLoading || form.email === '' || form.password === ''}
            >
              Masuk
            </button>
          </div>

        </div>
      </div>

      <style jsx>
        {`
        .c {
          width: 100%;
          height: 100vh;
          display: flex;
          align-items: center;
          flex-wrap: wrap;
          justify-content: center;
        }

        .container-login {
          min-width: 568px;
          min-height: 500px;
          background: #FFFFFF;
          box-shadow: 0px 1px 12px rgba(0, 0, 0, 0.25);
          border-radius: 10px;
          padding: 2.5rem 2.5rem;
        }

        .break {
          flex-basis: 100%;
          height: 0;
        }

        .text-forgot-password {
          font-size: 12px;
          line-height: 14px;
          color: #6F0C06;
          margin-top: 20px;
          text-align: right;
        }

        .container-social-media {
          display: flex;
          justify-content: space-evenly;
          margin-top: 30px;
        }

        .text-register {
          font-size: 16px;
          line-height: 19px;
          color: #AFAFAF;
        }

        .text-register-action {
          font-weight: bold;
          font-size: 16px;
          line-height: 19px;
          color: #730C07;
          cursor: pointer;
        }

        @media only screen and (max-width: 600px) {
          .c {
            width: 100%;
            display: flex;
            align-items: center;
            flex-wrap: wrap;
            justify-content: center;
          }

          .container-login {
            margin-top: 12px;
            min-width: 90%;
            min-height: 596px;
            background: #FFFFFF;
            box-shadow: 0px 1px 12px rgba(0, 0, 0, 0.25);
            border-radius: 10px;
            padding: 2.5rem 2.5rem;
          }
        }
      `}

      </style>
    </div>
  );
};

Home.getInitialProps = (ctx) => {  
  const { token, endpoint } = ctx.store.getState().authentication;
  return {
      apiUrl: endpoint.apiUrl,
      token: token
  }
}

export default connect(
  (state) => state,
  { authenticateLakkon }
)(Home);
