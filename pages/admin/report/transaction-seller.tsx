import { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import BackOfficeBase from 'components/LakkonOfficeBase'
import ContentHeader from 'components/BackOfficeBase/ContentHeader'
import { getProducts, TParams, ORDER, ORDERTYPE, STATE, deleteProduct, updateStatusProduct } from 'data/api/product'
import { formatNumber, exportData } from 'util/helper'
import { reportTransaction, reportTransactionSeller } from 'data/api/report'
import { getSellers } from 'data/api/seller'
import moment from 'moment'

const breadcumb = [
  {
    title: 'Home',
    link: '/',
  },
  {
    title: 'Report',
    link: '/',
  },
  {
    title: 'Seller Transaction',
    link: '/',
  },
]

type TProps = {
  apiUrl: string,
  token: string,
  authentication: any,
  paramSellers: any
}

const Status = ({status}) => {
  switch (status) {
    case STATE.ACTIVE:
      return (
        <div className="bg-green-500 text-white px-3 py-1 text-center font-medium rounded-md text-sm">
          {status}
        </div>
      )
    case STATE.DRAFT:
      return (
        <div className="bg-gray-400 text-text-primary px-3 py-1 text-center font-medium rounded-md text-sm">
          {status}
        </div>
      )
    case STATE.NonActive:
      return (
        <div className="bg-primary text-white px-3 py-1 text-center font-medium rounded-md text-sm">
          {status}
        </div>
      )
    default:
      return (
        <div className="bg-primary text-white px-3 py-1 text-center font-medium rounded-md text-sm">
          {status}
        </div>
      )
  }
}

const Product = ({apiUrl, token, authentication, paramSellers}: TProps) => {
  const [page, setPage] = useState(1)
  const [limit, setLimit] = useState(10)
  const [keyword, setKeyword] = useState('')
  const [tabledDatas, setTableDatas] = useState([])
  const [sellers, setSellers] = useState(paramSellers)
  const [selectedSeller, setSelectedSeller] = useState('')

  useEffect(() => {
    // if(selectedSeller) {
      getReport()
    // }
  }, [selectedSeller])

  const getReport = async () => {
    const res = await reportTransactionSeller(apiUrl, token, selectedSeller)
    setTableDatas(res.data ? res.data : [])
  }

  const handleExport = async () => {
    if (selectedSeller) {
      exportData(tabledDatas.map(v => ({
        Invoice: v.invoice,
        Nomor_Order: v.nomor_order,
        Tanggal: v.date,
        Status: v.status,
        Pelanggan: v.pelanggan,
        Jumlah: v.jumlah,
        Subtotal: v.subtotal,
        Diskon_Transaksi: v.discount.transaksi,
        Diskon_Produk: v.discount.produk,
        Diskon_Voucher: v.discount.voucher,
        Biaya_Layanan: v.biaya_tambahan.biaya_layanan,
        Biaya_Pengiriman: v.biaya_tambahan.pengiriman,
        GrandTotal: v.grandtotal
      })), `Data Transaction Seller`)
    }
    
  }

  return (
    <BackOfficeBase>
      <div className="w-full flex flex-wrap">
        <ContentHeader title="Report Seller Transaction" breadcumb={breadcumb} />

        <div className="w-full flex items-center mt-4 text-text-primary">
          <div className="border border-gray-400 rounded-l flex items-center h-full w-64">
            <select className="py-2 px-5" value={selectedSeller} onChange={(e) => setSelectedSeller(e.target.value)}>
              {/* <option value="" disabled>Pilih Seller</option> */}
              <option value="">Semua Seller</option>
              {
                sellers.map(v => (
                  <option key={v.seller.id} value={v.seller.id}>{v.seller.store_name}</option>
                ))
              }
            </select>
          </div>
          <div className="rounded-r px-3 py-1 bg-gray-500 h-full items-center flex">
            <img src="/images/ic_arrow_down.png" alt=""/>
          </div>
        </div>

        <div className="w-full">
          <div className="bg-white shadow-lg p-4 mt-4 rounded-lg flex flex-wrap">

            <div id="filters" className="w-full flex items-center">
              <div className="w-full">
                <input className="border border-gray-300" type="text" placeholder="search" value={keyword} onChange={(e) => setKeyword(e.target.value)}/>
              </div>
              <div className="w-full flex justify-end">
                {/* <button className="py-1 px-3 border-2 border-gray-300 bg-white rounded text-text-primary flex items-center mr-2">
                  <img className="mr-2" src="/images/ic_filter.png" alt="" />
                  FILTERS
                </button> */}
                <button className="py-1 px-3 border-2 border-gray-300 bg-white rounded text-text-primary flex items-center" onClick={handleExport}>
                  <img className="mr-2" src="/images/ic_export.png" alt="" />
                  EXPORT
                </button>
              </div>
            </div>

            <div id="sortings" className="w-full flex items-center mt-4">
              <div className="w-full flex flex-wrap items-center">
                {/* <div className="flex">
                  <div className="border border-gray-400 rounded-l flex items-center h-full">
                    <select className="py-2 px-5 text-text-primary" value="" onChange={() => {}}>
                      <option value="" disabled>Periode</option>
                    </select>
                  </div>
                  <div className="rounded-r px-3 py-1 bg-gray-300 items-center flex">
                    <img src="/images/ic_arrow_down.png" alt=""/>
                  </div>
                </div> */}
                <div className="text-text-primary font-medium ml-3">
                  {/* 106 records found */}
                </div>
              </div>
              <div className="w-full flex justify-end items-center">
                {/* <select className="w-auto">
                  <option>10</option>
                </select>
                <div className="text-text-primary font-medium ml-3">
                  Per Page
                </div> */}
                {/* <button className="px-3 py-2 bg-gray-400 rounded text-text-primary flex items-center ml-4" onClick={() => setPage(page - 1)}>
                  <img src="/images/ic_chevron_left.png" alt="" />
                </button>
                <input className="ml-3 w-12 text-center border border-gray-300" type="text" value={page}/> */}
                {/* <div className="text-text-primary font-medium ml-3">
                  Of 6
                </div> */}
                {/* <button className="px-3 py-2 bg-gray-400 rounded text-text-primary flex items-center ml-4" onClick={() => setPage(page + 1 )}>
                  <img src="/images/ic_arrow_right.png" alt="" />
                </button> */}
              </div>
            </div>

            <div id="table" className="w-full mt-8">
              <table className="w-full">
                <thead>
                  <th>Invoice</th>
                  <th>Nomor Order</th>
                  <th>Tanggal</th>
                  <th>Status</th>
                  <th>Pelanggan</th>
                  <th>Jumlah</th>
                  <th>Subtotal</th>
                  <th colSpan={3} className="text-center">Discount</th>
                  <th colSpan={2}>Biaya Tambahan</th>
                  <th>Grand Total</th>
                </thead>
                <thead>
                  <th>
                    {/* <input type="text" className="w-full border border-gray-300 px-4 py-2" /> */}
                  </th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th>
                    {/* <select className="w-full border border-gray-300 px-4 py-2 text-text-primary rounded">
                      <option>Semua Status</option>
                    </select> */}
                  </th>
                  <th>
                    {/* <select className="w-full border border-gray-300 px-4 py-2 text-text-primary rounded">
                      <option>Semua Pelanggan</option>
                    </select> */}
                  </th>
                  <th></th>
                  <th>Transaksi</th>
                  <th>Produk</th>
                  <th>Voucher</th>
                  <th>Biaya Layanan</th>
                  <th>Pengiriman</th>
                  <th>Rp ~</th>
                </thead>
                <tbody>
                  {
                    tabledDatas.map((v, i) => (
                      <tr key={v.id} className={`${i%2 == 0 ? 'bg-gray-200' : 'bg-white'} cursor-pointer hover:bg-gray-300`}>
                        <td>
                          {v.invoice}
                        </td><td>
                          {v.nomor_order}
                        </td>
                        <td>
                          {moment(v.date).format('DD MMMM YYYY')}
                        </td>
                        <td>
                          {v.status}
                        </td>
                        <td>
                          {v.pelanggan}
                        </td>
                        <td>
                          {v.jumlah}
                        </td>
                        <td>
                          {`Rp ${formatNumber(v.subtotal)}`}
                        </td>
                        <td>
                          {v.discount.transaksi ? `${formatNumber(v.discount.transaksi)}` : '-'}
                        </td>
                        <td>
                          {v.discount.produk ? `${formatNumber(v.discount.produk)}` : '-'}
                        </td>
                        <td>
                          {v.discount.voucher ? `${formatNumber(v.discount.voucher)}` : '-'}
                        </td>
                        <td>
                          {v.biaya_tambahan.biaya_layanan ? `${formatNumber(v.biaya_tambahan.biaya_layanan)}` : '-'}
                        </td>
                        <td>
                          {v.biaya_tambahan.pengiriman ? `Rp ${formatNumber(v.biaya_tambahan.pengiriman)}` : '-'}
                        </td>
                        <td>
                          {`${formatNumber(v.grandtotal)}`}
                        </td>
                      </tr>
                    ))
                  }
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </BackOfficeBase>
  )
}

Product.getInitialProps = async (ctx) => {
  const {endpoint} = ctx.store.getState().authentication
  const {token} = ctx.store.getState().lakkonState
  const sellers = await getSellers(token, 'store_name,ASC', '1', `100`, 'Approved')

  return {
      apiUrl: endpoint.apiUrl,
      token: token,
      paramSellers: sellers.data ? sellers.data : []
  }
}

export default connect(
  (state) => state, {}
)(Product);