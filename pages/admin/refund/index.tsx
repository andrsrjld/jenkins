import { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import BackOfficeBase from 'components/LakkonOfficeBase'
import ContentHeader from 'components/BackOfficeBase/ContentHeader'
import { getProducts, TParams, ORDER, ORDERTYPE, STATE, deleteProduct, updateStatusProduct } from 'data/api/product'
import { formatNumber, exportData } from 'util/helper'
import { reportTransaction } from 'data/api/report'
import { getRefundAdmin, updateRefundAdmin } from 'data/api/refund'
import { getSellers } from 'data/api/seller'
import moment from 'moment'
import Loading from 'components/Loading'
import { useRouter } from 'next/router'
import { DateRangePicker } from 'react-date-range';
import { startOfMonth, endOfMonth } from 'date-fns';
import Modal from 'components/Modal'

const breadcumb = [
  {
    title: 'Home',
    link: '/',
  },
  {
    title: 'Refund',
    link: '/',
  },
]

type TProps = {
  apiUrl: string,
  token: string,
  authentication: any,
  paramSellers: any
}

const Product = ({apiUrl, token, authentication, paramSellers}: TProps) => {
  const router = useRouter()
  const [loading, setLoading] = useState(false)
  const [page, setPage] = useState(1)
  const [limit, setLimit] = useState(10)
  const [keyword, setKeyword] = useState('')
  const [tabledDatas, setTableDatas] = useState([])
  const [sellers, setSellers] = useState(paramSellers)
  const [selectedSeller, setSelectedSeller] = useState('')

  const [checkedAll, setCheckedAll] = useState(false)

  const [modalDate, setmodalDate] = useState(false);
  const [date, setDate] = useState([
    {
      startDate: startOfMonth(new Date()),
      endDate: endOfMonth(new Date()),
      key: 'selection'
    }
  ]);

  useEffect(() => {
    // if(selectedSeller != '') {
      getReport()
    // }
  }, [page])

  useEffect(() => {
    if(page == 1) {
      getReport()
    } else {
      setPage(1)
    }
  }, [date])

  const getReport = async () => {
    const res = await getRefundAdmin(token, limit, page, '', moment(date[0].startDate).format('YYYY-MM-DD'), moment(date[0].endDate).format('YYYY-MM-DD'))
    console.log(res.data)
    setTableDatas(res.data ? res.data.map(v => ({...v, checked: false})) : [])
  }

  const handleExport = () => {
    exportData(tabledDatas.map(v => ({
        Invoice: v.invoice,
        Tanggal: v.date,
        Status: v.status,
        Pelanggan: v.pelanggan,
        Jumlah: v.jumlah,
        Subtotal: v.subtotal,
        Diskon_Transaksi: v.discount.transaksi,
        Diskon_Produk: v.discount.produk,
        Diskon_Voucher: v.discount.voucher,
        Biaya_Layanan: v.biaya_tambahan.biaya_layanan,
        Biaya_Pengiriman: v.biaya_tambahan.pengiriman,
        GrandTotal: v.grandtotal
      })
    ),
    'Data Transaksi')
  }

  const handleCheckedAll = () => {
    const val = !checkedAll;

    const newData = tabledDatas.map((v, i) => ({...v, checked: val}))
    setTableDatas(newData)

    setCheckedAll(val)
  }

  const handleChecked = (index) => {
    const newData = tabledDatas.map((v, i) => ({...v, checked: i === index ? !v.checked : v.checked}))
    setTableDatas(newData)
  }

  const handleUpdate = (status) => {
    setLoading(true)
    const promises = tabledDatas.filter(v => v.checked && v.status == 'PENDING').map(v => updateRefundAdmin(token, {
      ...v,
      status: status
    }))

    Promise.all(promises)
      .then(v => {})
      .catch(v => {})
      .then(v => {
        setLoading(false)
        getReport()
      }) 
  }

  const renderBuyerDetail = (id) => {
    return (
      <div className="w-full flex justify-center">
        <button className="text-sm py-1 px-2 rounded border border-text-primary hover:bg-gray-100" onClick={() => handleClickDataUser(id)}>Data User</button>
      </div>
    )
  }

  const handleClickDataUser = (id) => {
    router.push(`/admin/account/customer?id=${id}`)
  }

  return (
    <BackOfficeBase>
      <Loading show={loading}/>
      <div className="w-full flex flex-wrap">
        <ContentHeader title="Refund" breadcumb={breadcumb} />

        {/* <div className="w-full flex items-center mt-4 text-text-primary">
          <div className="border border-gray-400 rounded-l flex items-center h-full w-64">
            <select className="py-2 px-5" value={selectedSeller} onChange={(e) => setSelectedSeller(e.target.value)}>
              <option value="" disabled>Pilih Seller</option>
              {
                sellers.map(v => (
                  <option key={v.seller.id} value={v.seller.id}>{v.seller.store_name}</option>
                ))
              }
            </select>
          </div>
          <div className="rounded-r px-3 py-1 bg-gray-500 h-full items-center flex">
            <img src="/images/ic_arrow_down.png" alt=""/>
          </div>
        </div> */}

        <div className="w-full">
          <div className="bg-white shadow-lg p-4 mt-4 rounded-lg flex flex-wrap">

            <div id="filters" className="w-full flex items-center">
              <div className="w-full">
                {/* <input className="border border-gray-300" type="text" placeholder="search" value={keyword} onChange={(e) => setKeyword(e.target.value)}/> */}
              </div>
              <div className="w-full flex justify-end">
                {/* <button className="py-1 px-3 border-2 border-gray-300 bg-white rounded text-text-primary flex items-center mr-2">
                  <img className="mr-2" src="/images/ic_filter.png" alt="" />
                  FILTERS
                </button> */}
                {/* <button className="py-1 px-3 border-2 border-gray-300 bg-white rounded text-text-primary flex items-center" onClick={handleExport}>
                  <img className="mr-2" src="/images/ic_export.png" alt=""/>
                  EXPORT
                </button> */}
              </div>
            </div>

            <div id="sortings" className="w-full flex items-center mt-4">
              <div className="w-full flex flex-wrap items-center">
                <div className="flex">
                  <div className="border border-gray-400 rounded-l flex items-center h-full">
                    <select className="py-2 px-5 text-text-primary" value="" onChange={(e) => handleUpdate(e.target.value)}>
                      <option value="" disabled>Action</option>
                      <option value="DONE">DONE</option>
                      <option value="REJECTED">REJECTED</option>
                    </select>
                  </div>
                  <div className="rounded-r px-3 py-1 bg-gray-300 items-center flex">
                    <img src="/images/ic_arrow_down.png" alt=""/>
                  </div>
                </div>
                <div className="text-text-primary font-medium ml-3">
                  {/* 106 records found */}
                </div>
              </div>
              <div className="w-full flex justify-end items-center">
                {/* <select className="w-auto">
                  <option>10</option>
                </select>
                <div className="text-text-primary font-medium ml-3">
                  Per Page
                </div> */}
                <button className="px-3 py-2 bg-gray-400 rounded text-text-primary flex items-center ml-4" onClick={() => setPage(page - 1)} disabled={ page == 1 }>
                  <img src="/images/ic_chevron_left.png" alt="" />
                </button>
                <input className="ml-3 w-12 text-center border border-gray-300" type="text" value={page} disabled/>
                <button className="px-3 py-2 bg-gray-400 rounded text-text-primary flex items-center ml-4" onClick={() => setPage(page + 1 )}>
                  <img src="/images/ic_arrow_right.png" alt="" />
                </button>
              </div>
            </div>

            <div className="w-full mt-4">
              <button className="w-auto border border-gray-300 px-4 py-2 rounded" onClick={() => setmodalDate(true)}>Periode</button>
            </div>

            <div id="table" className="w-full mt-8">
              <table className="w-full">
                <thead>
                  <th>
                    <input type="checkbox" checked={checkedAll} onChange={(e) => handleCheckedAll()}></input>
                  </th>
                  <th>ID</th>
                  <th>Refund Date</th>
                  <th>Order Date</th>
                  <th>Buyer</th>
                  <th>Seller</th>
                  <th>Product</th>
                  <th>Quantity</th>
                  <th>Subtotal</th>
                  <th>Bank Account</th>
                  <th>Notes</th>
                  <th>Status</th>
                </thead>
                <tbody>
                  {
                    tabledDatas.map((v, i) => (
                      <tr key={v.id} className={`${i%2 == 0 ? 'bg-gray-200' : 'bg-white'} hover:bg-gray-300`}>
                        <td>
                          {
                            v.status == "PENDING" && (
                              <input type="checkbox" checked={v.checked} onChange={(e) => handleChecked(i)}></input>
                            )
                          }
                        </td>
                        <td>{v.id}</td>
                        <td>{moment(v.request_date).format('DD MMMM YYYY')}</td>
                        <td>{moment(v.order_dates).format('DD MMMM YYYY')}</td>
                        <td>{v.user_name}</td>
                        <td>{v.seller_name}</td>
                        <td>{`${v.product_name} - ${v.variant_name}`}</td>
                        <td>{v.quantity}</td>
                        <td>{`Rp ${formatNumber(v.subtotal)}`}</td>
                        <td>{v.bank ? `${v.bank ? `(${(v.bank)}) - ${v.rekening}` : ''}` : renderBuyerDetail(v.user_id)}</td>
                        <td>{v.notes}</td>
                        <td>{v.status}</td>
                      </tr>
                    ))
                  }
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

      <Modal open={modalDate} onClose={() => setmodalDate(false)}>
        <div className="bg-white p-4 shadow rounded">
          <div className="w-full flex justify-end">
            <img className="cursor-pointer" src="/images/ic_close_red.png" alt="" onClick={() => setmodalDate(false)}/>
          </div>
          <div className="mt-4">
            <DateRangePicker
              onChange={(item) => setDate([item.selection])}
              showSelectionPreview={true}
              moveRangeOnFirstSelection={false}
              months={2}
              ranges={date}
              direction="horizontal"
            />
          </div>
        </div>
      </Modal>
    </BackOfficeBase>
  )
}

Product.getInitialProps = async (ctx) => {
  const {endpoint} = ctx.store.getState().authentication
  const {token} = ctx.store.getState().lakkonState
  const sellers = await getSellers(token, 'store_name,ASC', '1', `100`, 'Approved')

  return {
      apiUrl: endpoint.apiUrl,
      token: token,
      paramSellers: sellers.data ? sellers.data : []
  }
}

export default connect(
  (state) => state, {}
)(Product);