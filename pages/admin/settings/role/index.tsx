import { useEffect, useState } from 'react'
import Link from 'next/link'
import { connect } from 'react-redux'
import BackOfficeBase from 'components/LakkonOfficeBase'
import ContentHeader from 'components/BackOfficeBase/ContentHeader'
import { getRole, deleteRole } from 'data/api/role'
import Loading from 'components/Loading'

const breadcumb = [
  {
    title: 'Home',
    link: '/',
  },
  {
    title: 'Settings',
    link: '/',
  },
  {
    title: 'Role',
    link: '/',
  },
]

type TProps = {
  apiUrl: string,
  token: string,
  authentication: any
}

const Product = ({apiUrl, token, authentication}: TProps) => {
  const [page, setPage] = useState(1)
  const [limit, setLimit] = useState(10)
  const [keyword, setKeyword] = useState('')
  const [tabledDatas, setTableDatas] = useState([])
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    doGetData()
  }, []);

  const doGetData = async () => {
    setLoading(true)
    const res = await getRole(apiUrl, token)

    setTableDatas(res.data ? res.data : [])
    setLoading(false)
  }

  const handleDeleteBanner = async (id) => {
    const res = await deleteRole(apiUrl, token, id)
    doGetData()
  }


  const handleChangeChecked = (index) => {
    setTableDatas(tabledDatas.map((v,i) => ({...v, isChecked: index === i ? !v.isChecked : v.isChecked})))
  }

  return (
    <BackOfficeBase>
      <Loading show={loading}/>
      <div className="w-full flex flex-wrap">
        <ContentHeader title="Role" breadcumb={breadcumb} />

        <div className="w-full flex justify-end mt-4">
          <Link href="/admin/settings/role/add">
            <button className="bg-orange-500 text-white font-bold px-5 py-2 mx-1 rounded">ADD NEW</button>
          </Link>
        </div>

        <div className="w-full">
          <div className="bg-white shadow-lg p-4 mt-4 rounded-lg flex flex-wrap">

            <div id="table" className="w-full mt-8">
              <table className="w-full">
                <thead>
                  <th>No</th>
                  <th>Role</th>
                  <th className="text-center">Action</th>
                </thead>
                <tbody>
                  {
                    tabledDatas.map((v, i) => (
                      <tr key={v.id} className={`${i%2 == 0 ? 'bg-gray-200' : 'bg-white'} cursor-pointer hover:bg-gray-300`} onClick={(e) => {
                        const target : any = e.target;
                        if(target.id === 'checkbox') return
                        // window.location.href = `/backoffice/products/detail/${v.slug}`
                      }}>
                        <td>
                          {i+1}
                        </td>
                        <td>
                          {v.name}
                        </td>
                        <td className="text-center">
                          <Link href={`/admin/settings/role/${v.id}`}>
                            <button className="px-2 py-2 border border-primary rounded bg-white mx-1 hover:bg-gray-200">
                              <img src="/images/ic_lakkon_edit_red.png" alt="" style={{ width: '24px', height: '24px' }} />
                            </button>
                          </Link>
                          <button className="px-2 py-2 border border-primary rounded bg-white mx-1 hover:bg-gray-200" onClick={() => handleDeleteBanner(v.id)}>
                            <img src="/images/ic_trash.png" alt="" style={{ width: '24px', height: '24px' }} />
                          </button>
                        </td>
                      </tr>
                    ))
                  }
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </BackOfficeBase>
  )
}

Product.getInitialProps = (ctx) => {
  const { endpoint } = ctx.store.getState().authentication;
  const { user, token } = ctx.store.getState().lakkonState
  return {
      apiUrl: endpoint.apiUrl,
      token: token
  }
}

export default connect(
  (state) => state, {}
)(Product);