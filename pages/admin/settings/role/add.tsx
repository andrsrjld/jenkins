import {useEffect, useState, useRef} from 'react'
import { connect } from 'react-redux'
import BackOfficeBase from 'components/LakkonOfficeBase'
import { uploadImage } from 'data/api/image'
import { postRole, TRequestInsert } from 'data/api/role'
import Loading from 'components/Loading'
import moment from 'moment'
import Router from 'next/router'
import { getRole } from 'data/api/role'
import { getFeature } from 'data/api/feature'
import { postPermission } from 'data/api/permission'

type TProp = {
  user: any,
  cloudinaryUrl: string,
  apiUrl: string,
  token: string,
  paramFeature: any
}

const dashboard = ({user, cloudinaryUrl, apiUrl, token, paramFeature} : TProp) => {
  const authorRef = useRef(null)
  const thumbnailRef = useRef(null)
  const [loading, setLoading] = useState(false)

  const [features, setFeatures] = useState(paramFeature)
  const [name, setName] = useState('')

  const handleSave = async () => {
    console.log(user)
    setLoading(true)
    let request : TRequestInsert = {
      name: name
    }

    try {
      // Create Role
      const res = await postRole(apiUrl, token, request)

      // Create Access
      const promises = []
      features.forEach(v => {
        if(v.isChecked) {
          promises.push(postPermission(apiUrl, token, {
            feature_id: v.id,
            role_id: res.data.id
          }))
        }
      })
      Promise.all(promises)
        .then(v => Router.push('/admin/settings/role')
        ).catch(err => {})
    } catch (err) {
      // TODO HANDLE ERR
    }

    setLoading(false)
  }

  const handleChangeFeature = (id) => {
    setFeatures(v => v.map(k => ({...k, isChecked: k.id===id ? !k.isChecked : k.isChecked})))
  }

  return (
    <BackOfficeBase>
      <Loading show={loading}/>
      <div className="w-full flex flex-wrap">
        <div className="w-full flex">
          <div className="w-full font-medium text-2xl text-text-primary">
            Role
          </div>
          <div className="w-full">
            <div className="flex justify-end">
              <button className="bg-green-500 text-white font-bold px-5 py-1 mx-1 rounded" onClick={() => handleSave()}>SAVE ROLE</button>
            </div>
          </div>
        </div>
        <div className="w-full text-text-primary">
          <div className="bg-white shadow-lg py-12 mt-12 rounded-lg flex justify-center flex-wrap">
            <div className="w-1/2">
              
              <div id="storeName" className="flex mt-8">
                <label className="w-1/2 text-right">Role Name<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <input type="text" className="border border-gray-300 px-4 py-2 w-full" placeholder="" value={name} onChange={(e) => setName(e.target.value)}/>
                </div>
              </div>

              <div id="storeName" className="flex mt-8">
                <label className="w-1/2 text-right">Feature List<span className="text-primary">*</span></label>
                <div className="w-full pl-5 flex flex-wrap">
                  {
                    features.map(v => (
                      <div className="w-1/2 flex items-center">
                        <input key={v.id} type="checkbox" name="feature" checked={v.isChecked} className="mr-2" onChange={() => handleChangeFeature(v.id)}/> {v.name}
                      </div>
                    ))
                  }
                </div>
              </div>

            </div>

          </div>

        </div>
      </div>
    </BackOfficeBase>
  )
}

dashboard.getInitialProps = async (ctx) => {
  const { endpoint } = ctx.store.getState().authentication
  const { user, token } = ctx.store.getState().lakkonState

  const roles = await getFeature(endpoint.apiUrl, token)

  return {
      user: user,
      cloudinaryUrl: endpoint.cloudinaryUrl,
      apiUrl: endpoint.apiUrl,
      token: token,
      paramFeature: roles.data ? roles.data.map(v => ({...v, isChecked: false})) : []
  }
}

export default connect(
  (state) => state, {}
)(dashboard);