import {useEffect, useState, useRef} from 'react'
import { connect } from 'react-redux'
import BackOfficeBase from 'components/LakkonOfficeBase'
import { uploadImage } from 'data/api/image'
import Loading from 'components/Loading'
import moment from 'moment'
import Router from 'next/router'
import { getRole } from 'data/api/role'
import { getFeature } from 'data/api/feature'
import { postPermission } from 'data/api/permission'
import { getServiceFee, updateServiceFee, TRequestUpdate } from 'data/api/service-fee'

type TProp = {
  user: any,
  cloudinaryUrl: string,
  apiUrl: string,
  token: string,
  paramData: any
}

const dashboard = ({user, cloudinaryUrl, apiUrl, token, paramData} : TProp) => {
  const authorRef = useRef(null)
  const thumbnailRef = useRef(null)
  const [loading, setLoading] = useState(false)

  const [charge, setCharge] = useState(paramData.charge)

  const handleSave = async () => {
    console.log(user)
    setLoading(true)
    let request : TRequestUpdate = {
      charge: Number(charge)
    }

    try {
      // Create Role
      const res = await updateServiceFee(apiUrl, token, request)
      alert("Success")
    } catch (err) {
      // TODO HANDLE ERR
    }

    setLoading(false)
  }

  return (
    <BackOfficeBase>
      <Loading show={loading}/>
      <div className="w-full flex flex-wrap">
        <div className="w-full flex">
          <div className="w-full font-medium text-2xl text-text-primary">
            Service Fee
          </div>
          <div className="w-full">
            <div className="flex justify-end">
              <button className="bg-green-500 text-white font-bold px-5 py-1 mx-1 rounded" onClick={() => handleSave()}>SAVE</button>
            </div>
          </div>
        </div>
        <div className="w-full text-text-primary">
          <div className="bg-white shadow-lg py-12 mt-12 rounded-lg flex justify-center flex-wrap">
            <div className="w-1/2">
              
              <div id="storeName" className="flex mt-8">
                <label className="w-1/2 text-right">Service Fee<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <input type="text" className="border border-gray-300 px-4 py-2 w-full" placeholder="" value={charge} onChange={(e) => setCharge(e.target.value)}/>
                </div>
              </div>

            </div>

          </div>

        </div>
      </div>
    </BackOfficeBase>
  )
}

dashboard.getInitialProps = async (ctx) => {
  const { endpoint } = ctx.store.getState().authentication
  const { user, token } = ctx.store.getState().lakkonState

  const data = await getServiceFee(endpoint.apiUrl, token)

  return {
      user: user,
      cloudinaryUrl: endpoint.cloudinaryUrl,
      apiUrl: endpoint.apiUrl,
      token: token,
      paramData: data.data ? data.data : null
  }
}

export default connect(
  (state) => state, {}
)(dashboard);