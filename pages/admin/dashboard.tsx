import Router from 'next/router'
import { useEffect, useState } from 'react'
import { LineChart, Line, CartesianGrid, XAxis, YAxis, ResponsiveContainer } from 'recharts'
import BackOfficeBase from 'components/LakkonOfficeBase'
import ContentHeader from 'components/BackOfficeBase/ContentHeader'
import Link from 'next/link'
import {
  summaryLifetimeSales, 
  summaryAverageOrders, 
  summaryProductOrders, 
  summaryTopSalesProduct, 
  summaryTopQtyProduct, 
  graphPenjualan, 
  graphSales, 
  graphProduct, 
  summaryLifetimeSalesAdmin,
  summaryAverageOrdersAdmin,
  summaryProductOrdersAdmin,
  summaryServiceFee,
  summaryTopSalesProductAdmin,
  summaryTopQtyProductAdmin,
  graphPenjualanAdmin,
  graphSalesAdmin,
  graphProductAdmin,
  graphServiceFee,
  summaryLastOrdersAdmin,
  summaryLastTransactionAdmin
} from 'data/api/summaries'
import { formatNumber, isEmpty } from 'util/helper'
import moment from 'moment'
import { STATE, ORDERTYPE, ORDER, TParams, getProducts } from 'data/api/product'
import { DateRangePicker } from 'react-date-range';
import { startOfMonth, endOfMonth } from 'date-fns';
import Modal from 'components/Modal'

const LineGraph = ({data}) =>  (
  <ResponsiveContainer width="100%" height="80%">
    <LineChart data={data} margin={{left: 40, right: 40, top: 20}}>
      <Line type="monotone" dataKey="uv" stroke="#8884d8" />
      <CartesianGrid stroke="#ccc" />
      <XAxis dataKey="name" />
      <YAxis />
    </LineChart>
  </ResponsiveContainer>
);

const LastOrder = ({data}) => (
  <table className="w-full">
    <thead>
      <th className="bg-white text-text-primary">ID</th>
      <th className="bg-white text-text-primary">Tanggal Penjualan</th>
      <th className="bg-white text-text-primary">Pelanggan</th>
      <th className="bg-white text-text-primary">Total</th>
      <th className="bg-white text-text-primary">Status</th>
    </thead>
    <tbody>
      {
        data.map((v, i) => {
          return (
            <tr key={v} className={`${i%2==0 ? 'bg-gray-100' : ''}`}>
              <td className="font-bold cursor-pointer hover:text-gray-900"><Link href={`/admin/order?trx=${v.slug}`}>{v.slug}</Link></td>
              <td>{moment(v.date).format('DD MMMM YYYY')}</td>
              <td>{v.user_name}</td>
              <td>Rp {formatNumber(v.total_price + v.total_shipping)}</td>
              <td>{v.status}</td>
            </tr>
          )
        })
      }
    </tbody>
  </table>
)

const LastTransaction = ({data}) => (
  <table className="w-full">
    <thead>
      <th className="bg-white text-text-primary">ID</th>
      <th className="bg-white text-text-primary">Tanggal Penjualan</th>
      <th className="bg-white text-text-primary">Pelanggan</th>
      <th className="bg-white text-text-primary">Total</th>
      <th className="bg-white text-text-primary">Status</th>
    </thead>
    <tbody>
      {
        data.map((v, i) => {
          return (
            <tr key={v} className={`${i%2==0 ? 'bg-gray-100' : ''}`}>
              <td className="font-bold cursor-pointer hover:text-gray-900"><Link href={`/admin/order?trx=${v.slug}`}>{v.slug}</Link></td>
              <td>{moment(v.date).format('DD MMMM YYYY')}</td>
              <td>{v.user_name}</td>
              <td>Rp {formatNumber(v.total_price + v.total_shipping)}</td>
              <td>{v.status}</td>
            </tr>
          )
        })
      }
    </tbody>
  </table>
)

const BestSeller = ({data}) => (
  <table className="w-full">
    <thead>
      <th className="bg-white text-text-primary">Nama Produk</th>
      <th className="bg-white text-text-primary">Total Penjualan</th>
    </thead>
    <tbody>
      {
        data.map((v, i) => {
          return (
            <tr key={v.product_name} className={`${i%2==0 ? 'bg-gray-100' : ''}`}>
              <td>{v.product_name}</td>
              <td>Rp {formatNumber(v.total)}</td>
            </tr>
          )
        })
      }
    </tbody>
  </table>
)

const BestQty = ({data}) => (
  <table className="w-full">
    <thead>
      <th className="bg-white text-text-primary">Nama Produk</th>
      <th className="bg-white text-text-primary">Total Penjualan</th>
    </thead>
    <tbody>
      {
        data.map((v, i) => {
          return (
            <tr key={v.product_name} className={`${i%2==0 ? 'bg-gray-100' : ''}`}>
              <td>{v.product_name}</td>
              <td>{formatNumber(v.total)}</td>
            </tr>
          )
        })
      }
    </tbody>
  </table>
)

const dashboard = (props) => {
  const {apiUrl, token} = props

  useEffect(() => {
    doGetSummaryLifeSales()
    doGetSummaryOrders()
    doGetSummaryProductOrders()
    doGetSummaryServiceFee()

    doGetSummaryTopSalesProducts()
    doGetSummaryTopQtyProducts()

    doGetSummaryLastOrders()
    doGetSummaryLastTransaction()

    fetchProducts()
  }, []);

  const [lifeSales, setLifeSales] = useState(0)
  const [averageOrders, setAverageOrders] = useState(0)
  const [productOrders, setProductOrders] = useState(0)
  const [serviceFee, setServiceFee] = useState(0)

  const [productSales, setProductSales] = useState([])
  const [productQty, setProductQty] = useState([])
  const [lastOrders, setLastOrders] = useState([])
  const [lastTransactions, setLastTransactions] = useState([])
  const [table, setTable] = useState(1)

  const [recentProducts, setRecentProducts] = useState([])

  // Date Range
  const [modalDate, setModalDate] = useState(false)
  const [date, setDate] = useState([
    {
      startDate: startOfMonth(new Date()),
      endDate: endOfMonth(new Date()),
      key: 'selection'
    }
  ]);

  useEffect(() => {
    console.log(date)
  }, [date]);
  //\\

  // graph
  const [graph, setGraph] = useState(1)  
  const [dataGraph, setDataGraph] = useState([])
  useEffect(() => {
    switch (graph) {
      case 1:
        doGetGraphSales()
        break;
      case 2:
        doGetGraphOrders()
        break;
      case 3:
        doGetGraphProducts()
        break;
      case 4:
        doGetGrapServiceFee()
        break;
      default:
        doGetGraphOrders()
        break;
    }
  }, [graph, date]);

  const doGetSummaryLifeSales = async () => {
    const res = await summaryLifetimeSalesAdmin(apiUrl, token)
    setLifeSales(res.data ? res.data?.lifetime_sales : 0)
  }

  const doGetSummaryOrders = async () => {
    const res = await summaryAverageOrdersAdmin(apiUrl, token)
    setAverageOrders(res.data ? res.data?.average_order : 0)
  }

  const doGetSummaryProductOrders = async () => {
    const res = await summaryProductOrdersAdmin(apiUrl, token)
    setProductOrders(res.data ? res.data?.product_sales : 0)
  }

  const doGetSummaryServiceFee = async () => {
    const res = await summaryServiceFee(apiUrl, token)
    setServiceFee(!isEmpty(res.data) ? res.data?.total_service_fee : 0)
  }

  const doGetSummaryTopSalesProducts = async () => {
    const res = await summaryTopSalesProductAdmin(apiUrl, token)
    setProductSales(res.data ? res.data?.best_sales : [])
  }

  const doGetSummaryTopQtyProducts = async () => {
    const res = await summaryTopQtyProductAdmin(apiUrl, token)
    setProductQty(res.data ? res.data?.best_qty : [])
  }

  const doGetSummaryLastOrders = async () => {
    const res = await summaryLastOrdersAdmin(apiUrl, token)
    setLastOrders(res.data ? res.data : [])
  }

  const doGetSummaryLastTransaction = async () => {
    const res = await summaryLastTransactionAdmin(apiUrl, token)
    setLastTransactions(res.data ? res.data : [])
  }

  const doGetGraphOrders = async () => {
    const startDate = moment(date[0].startDate).format('YYYY-MM-DD')
    const endDate = moment(date[0].endDate).format('YYYY-MM-DD')
    const res = await graphPenjualanAdmin(apiUrl, token, startDate, endDate)
    setDataGraph(res.data.graph_order ? res.data?.graph_order.map(v => ({name: moment(v.date).format('DD MMM'), uv: v.total})) : [])
  }

  const doGetGraphSales = async () => {
    const startDate = moment(date[0].startDate).format('YYYY-MM-DD')
    const endDate = moment(date[0].endDate).format('YYYY-MM-DD')
    const res = await graphSalesAdmin(apiUrl, token, startDate, endDate)
    setDataGraph(res.data.graph_transaction ? res.data?.graph_transaction.map(v => ({name: moment(v.date).format('DD MMM'), uv: v.total})) : [])
  }

  const doGetGraphProducts = async () => {
    const startDate = moment(date[0].startDate).format('YYYY-MM-DD')
    const endDate = moment(date[0].endDate).format('YYYY-MM-DD')
    const res = await graphProductAdmin(apiUrl, token, startDate, endDate)
    setDataGraph(res.data.graph_product_sales ? res.data?.graph_product_sales.map(v => ({name: moment(v.date).format('DD MMM'), uv: v.total})) : [])
  }

  const doGetGrapServiceFee = async () => {
    const startDate = moment(date[0].startDate).format('YYYY-MM-DD')
    const endDate = moment(date[0].endDate).format('YYYY-MM-DD')
    const res = await graphServiceFee(apiUrl, token, startDate, endDate)
    setDataGraph(res.data.graph_service_fee ? res.data?.graph_service_fee.map(v => ({name: moment(v.date).format('DD MMM'), uv: v.total})) : [])
  }

  const fetchProducts = async () => {
    const params:TParams = {
      layout_type: 'list_layout',
      seller_slug: '',
      type_slug: 'All',
      keyword: '',
      status: STATE.ALL,
      origin: '',
      species: '',
      tasted: '',
      roast_level: '',
      price: '',
      page: 1,
      limit: 3,
      order: `${ORDERTYPE.DATE},${ORDER.DESC}`
    }
    const res = await getProducts(apiUrl, params, '')
    setRecentProducts(res.data ? res.data.map(v => ({...v, isChecked: false})) : [])
  }

  const renderTable = () => {
    switch(table) {
      case 1:
        return <LastOrder data={lastOrders} />
      case 2:
        return <LastTransaction data={lastTransactions} />
      case 3: 
        return <BestSeller data={productSales} />
      case 4:
        return <BestQty data={productQty} />
      default:
        return <LastOrder data={[]} />
    }
  }

  return (
    <BackOfficeBase>
      <div className="w-full flex flex-wrap">
        <ContentHeader title="Dashboard" breadcumb={[]} />

        <div className="w-full mt-6">
          <div className="w-full flex">
            <div className="w-full bg-yellow-500 rounded-lg flex flex-wrap p-5 mr-3">
              <div className="w-full text-white font-bold text-xl mb-1">
                Rp { formatNumber(lifeSales) }
              </div>
              <div className="w-full text-white text-lg">
                Total Penjualan
              </div>
            </div>
            <div className="w-full bg-green-500 rounded-lg flex flex-wrap p-5 mx-3">
              <div className="w-full text-white font-bold text-xl mb-1">
                Rp { formatNumber(averageOrders.toFixed()) }
              </div>
              <div className="w-full text-white text-lg">
                Rata - rata Pembelian
              </div>
            </div>
            <div className="w-full bg-red-500 rounded-lg flex flex-wrap p-5 mx-3">
              <div className="w-full text-white font-bold text-xl mb-1">
                {formatNumber(productOrders)}
              </div>
              <div className="w-full text-white text-lg">
                Total Produk
              </div>
            </div>
            <div className="w-full bg-orange-500 rounded-lg flex-wrap p-5 ml-3 hidden">
              <div className="w-full text-white font-bold text-xl mb-1">
                Rp { formatNumber(serviceFee.toFixed()) }
              </div>
              <div className="w-full text-white text-lg">
                Total Biaya Layanan
              </div>
            </div>
          </div>
        </div>

        <div className="w-full flex flex-wrap mt-8">
          <div className="w-3/5 pr-2">
            <div className="w-full bg-white shadow-md p-4 rounded-lg flex flex-wrap items-start h-full">
              <div className="w-full flex items-center">
                <div className="w-full flex justify-start">
                  <div className={`${graph == 1 ? 'border-b-2 border-primary' : ''} text-text-primary text-sm px-3 py-1 cursor-pointer`} onClick={() => setGraph(1)}>
                    Pembelian
                  </div>
                  <div className={`${graph == 2 ? 'border-b-2 border-primary' : ''} text-text-primary text-sm px-3 py-1 cursor-pointer`} onClick={() => setGraph(2)}>
                    Transaksi
                  </div>
                  <div className={`${graph == 3 ? 'border-b-2 border-primary' : ''} text-text-primary text-sm px-3 py-1 cursor-pointer`} onClick={() => setGraph(3)}>
                    Produk
                  </div>
                  {/* <div className={`${graph == 4 ? 'border-b-2 border-primary' : ''} text-text-primary text-sm px-3 py-1 cursor-pointer`} onClick={() => setGraph(4)}>
                    Service Fee
                  </div> */}
                </div>
                <div className="w-full flex justify-end">
                  {/* <select className="w-auto border border-gray-300 px-4 py-2 rounded">
                    <option className="px-4 py-2">Bulan Ini</option>
                  </select> */}
                  <button className="w-auto border border-gray-300 px-4 py-2 rounded" onClick={() => setModalDate(true)}>Periode</button>
                </div>
              </div>
              <div className="w-full h-full pt-6" style={{ borderRadius: '0!important' }}>
                <LineGraph data={dataGraph} />
              </div>
            </div>
          </div>
          <div className="w-2/5 pl-2">
            <div className="divide-y-2 w-full bg-white shadow-md p-4 rounded-lg flex flex-wrap h-full">
              <div className="w-full font-medium text-text-primary text-lg">
                Produk Baru
              </div>
              <div className="w-full flex flex-wrap divide-y-2 mt-2 ">
                {
                  recentProducts.map(v => (
                    <div className="w-full flex pt-2 pb-1">
                      <img src={v.images ? v.images.find(v => v.is_primary).image_url : ''} alt= "" className="w-24"/>
                      <div className="w-full flex flex-wrap ml-2">
                        <div className="w-full flex justify-between items-center">
                          <div className="font-medium text-text-primary">{v.name}</div>
                          <div className="bg-green-500 text-white rounded px-3 text-center text-sm">{`Rp ${formatNumber(v.price)}`}</div>
                        </div>
                        <div className="w-full text-text-primary">
                          {v.short_description}
                        </div>
                      </div> 
                    </div>
                  ))
                }
              </div>
              <div className="w-full text-orange-500 pt-5 pb-2 text-center">
                Lihat Semua
              </div>
            </div>
          </div>
        </div>
        
        <div className="w-full bg-white shadow-md p-4 rounded-lg flex flex-wrap items-start mt-8">
          <div className="w-full flex items-center">
            <div className={`${table == 1 ? 'border-b-2 border-primary' : '' } text-text-primary text-sm px-3 py-1 cursor-pointer`} onClick={() => setTable(1)}>
              Pembelian Terakhir
            </div>
            <div className={`${table == 2 ? 'border-b-2 border-primary' : '' } text-text-primary text-sm px-3 py-1 cursor-pointer`} onClick={() => setTable(2)}>
              Transaksi Terakhir
            </div>
            <div className={`${table == 3 ? 'border-b-2 border-primary' : '' } text-text-primary text-sm px-3 py-1 cursor-pointer`} onClick={() => setTable(3)}>
              Penjual Populer
            </div>
            <div className={`${table == 4 ? 'border-b-2 border-primary' : '' } text-text-primary text-sm px-3 py-1 cursor-pointer`} onClick={() => setTable(4)}>
              Produk Populer
            </div>
          </div>
          <div className="w-full mt-6">
            { renderTable() }
          </div>
        </div>
      </div>

      <Modal open={modalDate} onClose={() => setModalDate(false)}>
        <div className="bg-white p-4 shadow rounded">
          <div className="w-full flex justify-end">
            <img className="cursor-pointer" src="/images/ic_close_red.png" alt="" onClick={() => setModalDate(false)}/>
          </div>
          <div className="mt-4">
            <DateRangePicker
              onChange={(item) => setDate([item.selection])}
              showSelectionPreview={true}
              moveRangeOnFirstSelection={false}
              months={2}
              ranges={date}
              direction="horizontal"
            />
          </div>
        </div>
      </Modal>
    </BackOfficeBase>
  )
}

dashboard.getInitialProps = (ctx) => {
  const { user, endpoint } = ctx.store.getState().authentication;
  const { token } = ctx.store.getState().lakkonState;
  const { apiUrl, cloudinaryUrl } = endpoint

  return {user, apiUrl, cloudinaryUrl, token}
}

export default dashboard;