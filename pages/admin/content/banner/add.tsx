import {useEffect, useState, useRef} from 'react'
import { connect } from 'react-redux'
import BackOfficeBase from 'components/LakkonOfficeBase'
import { uploadImage } from 'data/api/image'
import { updateBanner, TRequestInsert, postBanner } from 'data/api/banner'
import Loading from 'components/Loading'
import moment from 'moment'
import Router from 'next/router'
import CropPicker from 'components/CropPicker'
import Modal from 'components/Modal'
import { compressImage } from 'util/helper'

type TProp = {
  user: any,
  cloudinaryUrl: string,
  apiUrl: string,
  token: string
}

const dashboard = ({user, cloudinaryUrl, apiUrl, token} : TProp) => {
  const thumbnailRef = useRef(null)
  const [loading, setLoading] = useState(false)

  const [title, setTitle] = useState('')
  const [author, setAuthor] = useState('')
  const [link, setLink] = useState('')
  const [description, setDescription] = useState('')
  const [shortDescription, setShortDescription] = useState('')

  const [images, setImages] = useState([])
  const [thumbnailImage, setThumbnailImage] = useState(null)

  const [modalCrop, setModalCrop] = useState({
    image: '',
    open: false,
    cb: (props: any) => setThumbnailImage({base64: props.croppedImage, file: props.blob})
  })

  const handleSave = async () => {
    console.log(user)
    setLoading(true)
    let request : TRequestInsert = {
      title: title,
      description: description,
      link: link,
      image_url: '',
      publish_date: moment().format('YYYY-MM-DD HH:mm:ss'),
      remove_date: null
    }

    try {

      // Upload Thumbnail IF any
      if(thumbnailImage.file) {
        const fileCompressed = await compressImage(thumbnailImage.file, 1.5)
        const resLogo = await uploadImage(cloudinaryUrl, {
          file: fileCompressed,
          folder: 'banner',
          tag: ''
        })
        request.image_url = resLogo.secure_url
      }

      // Update Account
      const res = await postBanner(apiUrl, token, request)
      Router.push('/admin/content/banner')
    } catch (err) {
      // TODO HANDLE ERR
    }

    setLoading(false)
  }

  const getBase64 = (file, cb) =>{
    let reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = function () {
        cb(reader.result)
    }
    reader.onerror = function (error) {
        console.log('Error: ', error)
    }
  }

  const handleChangeThumbnailImage = (e) => {
    const file = e.target.files[0]
    getBase64(file, (result) => {
      // setThumbnailImage({base64: result, file: file})
      setModalCrop({
        ...modalCrop,
        image: result,
        open: true
      })
    });
  }

  const handleCroppedImage = async (croppedImage, cb) => {
    setLoading(true)
    let blob = await fetch(croppedImage).then(r => r.blob());
    modalCrop.cb({croppedImage, blob})
    setModalCrop({
      ...modalCrop,
      open: false
    })
    setLoading(false)
  }

  return (
    <BackOfficeBase>
      <Loading show={loading}/>
      <div className="w-full flex flex-wrap">
        <div className="w-full flex">
          <div className="w-full font-medium text-2xl text-text-primary">
            Banner
          </div>
          <div className="w-full">
            <div className="flex justify-end">
              <button className="bg-green-500 text-white font-bold px-5 py-1 mx-1 rounded" onClick={() => handleSave()}>SAVE BANNER</button>
            </div>
          </div>
        </div>
        <div className="w-full text-text-primary">
          <div className="bg-white shadow-lg py-12 mt-12 rounded-lg flex justify-center flex-wrap">
            <div className="w-1/2">
              
              <div id="storeName" className="flex mt-8">
                <label className="w-1/2 text-right">Title<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <input type="text" className="border border-gray-300 px-4 py-2 w-full" placeholder="" value={title} onChange={(e) => setTitle(e.target.value)}/>
                </div>
              </div>

              <div id="storeName" className="flex mt-8">
                <label className="w-1/2 text-right">Description<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <input type="text" className="border border-gray-300 px-4 py-2 w-full" placeholder="" value={description} onChange={(e) => setDescription(e.target.value)}/>
                </div>
              </div>

              <div id="storeName" className="flex mt-8">
                <label className="w-1/2 text-right">Link<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <input type="text" className="border border-gray-300 px-4 py-2 w-full" placeholder="" value={link} onChange={(e) => setLink(e.target.value)}/>
                </div>
              </div>

              <div id="logo" className="flex mt-8">
                <label className="w-1/2 text-right">Thumbnail<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <div className="border-2 border-gray-200 w-10 h-10">
                    <img className="w-full h-full object-cover" src={thumbnailImage ? thumbnailImage.base64 : ''} alt="logo image"/>
                  </div>
                  <div className="text-sm ">
                    <button className="border-2 bg-white px-3 rounded-lg mt-2" onClick={() => {thumbnailRef.current.click()}} >Browse...</button>
                    {`${thumbnailImage ? '' : ' No File Selected'}`}
                    <input ref={thumbnailRef} className="hidden" type="file" name="logo" onChange={handleChangeThumbnailImage}/>
                  </div>
                  {/* <input type="checkbox" name="delete" id="delete" className="mt-2"/> Delete Image */}
                </div>
              </div>
          
            </div>

          </div>

        </div>
      </div>

      <Modal open={modalCrop.open} onClose={() => setModalCrop({...modalCrop, open: false})}>
        <CropPicker 
          ratio={2/1} 
          onClosed={() => setModalCrop({...modalCrop, open: false})} 
          onSubmit={(croppedImage) => handleCroppedImage(croppedImage, modalCrop.cb)} 
          image={modalCrop.image}
          onCrop={() => setLoading(true)}
          onCropped={() => setLoading(false)}
        />
      </Modal>
    </BackOfficeBase>
  )
}

dashboard.getInitialProps = async (ctx) => {
  const { endpoint } = ctx.store.getState().authentication
  const { user, token } = ctx.store.getState().lakkonState
  console.log(ctx.store.getState())

  return {
      user: user,
      cloudinaryUrl: endpoint.cloudinaryUrl,
      apiUrl: endpoint.apiUrl,
      token: token
  }
}

export default connect(
  (state) => state, {}
)(dashboard);