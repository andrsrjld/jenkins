import {useEffect, useState, useRef} from 'react'
import { connect } from 'react-redux'
import BackOfficeBase from 'components/LakkonOfficeBase'
import { uploadImage } from 'data/api/image'
import { updateBanner, TRequestInsert, postBanner } from 'data/api/banner'
import Loading from 'components/Loading'
import moment from 'moment'
import Router from 'next/router'
import CropPicker from 'components/CropPicker'
import Modal from 'components/Modal'
import { compressImage } from 'util/helper'
import Select from 'react-select'


import dynamic from 'next/dynamic'
import Editor from 'components/Editor'
import { getSellers } from 'data/api/seller'
import { getProducts, STATE, ORDERTYPE, ORDER } from 'data/api/product'
import { postPage } from 'data/api/pages'

import {successNotification, errorNotification} from 'util/toastr-notif';


type TProp = {
  user: any,
  cloudinaryUrl: string,
  apiUrl: string,
  token: string
}

const dashboard = ({user, cloudinaryUrl, apiUrl, token} : TProp) => {
  const thumbnailRef = useRef(null)
  const [loading, setLoading] = useState(false)

  const [title, setTitle] = useState('')
  const [author, setAuthor] = useState('')
  const [link, setLink] = useState('')
  const [shortDescription, setShortDescription] = useState('')

  const [images, setImages] = useState([])
  const [thumbnailImage, setThumbnailImage] = useState(null)

  const [banners, setbanners] = useState([])

  const [modalCrop, setModalCrop] = useState({
    image: '',
    open: false,
    cb: (props: any) => {
      setbanners(v => [...v, {base64: props.croppedImage, file: props.blob, url: ''}])
    }
  })

  const [type, settype] = useState(0)

  const [pageName, setpageName] = useState('')
  const [sellers, setSellers] = useState([])
  const [selectedSeller, setSelectedSeller] = useState(null)
  const [products, setProducts] = useState([])
  const [selectedProduct, setSelectedProduct] = useState(null)
  const [selectedData, setSelectedData] = useState(null)
  const [description, setdescription] = useState('')

  const [tableDatas, settableDatas] = useState([])

  useEffect(() => {
    doGetSellers()
  }, []);

  useEffect(() => {
    if(selectedSeller) {
      doGetProducts()
    }
  },[selectedSeller])

  const doGetSellers = async () => {
    const res = await getSellers(token, 'store_name,ASC', '1', `100`, 'Approved')
    setSellers(res.data.map(v => ({...v, label: v.seller.store_name, value: v.seller.slug})))
  }

  const doGetProducts = async () => {
    setLoading(true)
    const res =await getProducts(apiUrl, {
      layout_type: 'list_layout',
      seller_slug: selectedSeller.value,
      type_slug: 'All',
      keyword: '',
      status: STATE.ACTIVE,
      origin: '',
      species: '',
      tasted: '',
      roast_level: '',
      price: '',
      page: 1,
      limit: 100,
      order: `${ORDERTYPE.DATE},${ORDER.DESC}`
    }, '')
    setProducts(res.data ? res.data.map(v => ({...v, label: v.name, value: v.id})) : [])
    setLoading(false)
  }

  const handleSave = async () => {
    setLoading(true)
    // validation
    if(pageName == '') {
      errorNotification('Please fill page name!')
      setLoading(false)
      return
    }

    const body = {
      title: pageName,
      products: tableDatas.map(v => ({id: v.id})),
      images: [],
      content: description
    }

    //upload image
    for (const img of banners) {
      const res = await uploadImage(cloudinaryUrl, {
        file: img.file,
        folder: 'banner',
        tag: ''
      })
      body.images.push({url: res.secure_url, link: img.url, thumbnail: ''})
    }


    try {
      const res = postPage(apiUrl, token, body)
      Router.push('/admin/content/page')
    } catch(err) {
      errorNotification("Something went wrong!")
    }
    setLoading(false)
  }

  const getBase64 = (file, cb) =>{
    let reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = function () {
        cb(reader.result)
    }
    reader.onerror = function (error) {
        console.log('Error: ', error)
    }
  }

  const handleChangeThumbnailImage = (e) => {
    const file = e.target.files[0]
    getBase64(file, (result) => {
      setModalCrop({
        ...modalCrop,
        image: result,
        open: true
      })
    });
  }

  const handleCroppedImage = async (croppedImage, cb) => {
    setLoading(true)
    let blob2 = await fetch(croppedImage).then(r => r.blob());
    const blob = await compressImage(blob2, 1)
    modalCrop.cb({croppedImage, blob})
    setModalCrop({
      ...modalCrop,
      open: false
    })
    setLoading(false)
  }

  const modules = {
    toolbar: [
      [{ 'header': [1, 2, false] }],
      ['bold', 'italic', 'underline','strike', 'blockquote'],
      [{'list': 'ordered'}, {'list': 'bullet'}, {'indent': '-1'}, {'indent': '+1'}],
      ['link'],
      ['clean']
    ],
  }

  const formats = [
    'header',
    'bold', 'italic', 'underline', 'strike', 'blockquote',
    'list', 'bullet', 'indent',
    'link'
  ]

  const handleAddProduct = () => {
    if(selectedProduct == null) {
      errorNotification('Harap pilih produk')
      return
    }
    settableDatas([...tableDatas, {
      no: null,
      ...selectedProduct
    }])
    setSelectedProduct(null)
    setSelectedSeller(null)
  }

  return (
    <BackOfficeBase>
      <Loading show={loading}/>
      <div className="w-full flex flex-wrap">
        <div className="w-full flex">
          <div className="w-full font-medium text-2xl text-text-primary">
            Page
          </div>
          <div className="w-full">
            <div className="flex justify-end">
              <button className="bg-green-500 text-white font-bold px-5 py-1 mx-1 rounded" onClick={() => handleSave()}>SAVE PAGE</button>
            </div>
          </div>
        </div>
        <div className="w-full text-text-primary">
          <div className="w-1/2 mt-12 bg-white px-4 py-4 rounded-xl" >
            <div className="font-semibold ml-1 mb-2">Page Name</div>
            <input className="border border-gray-300 px-4 py-2 w-full" placeholder="page name" type="text" value={pageName} onChange={(e) => setpageName(e.target.value)} />
          </div>
          <div className="bg-white shadow-lg mt-6 flex justify-center">
            <div className={`w-full flex justify-center cursor-pointer py-4 border-primary ${type == 0 ? `border-b-2` : ''}`} onClick={() => settype(0)}>
              Banner
            </div>
            <div className={`w-full flex justify-center cursor-pointer py-4 border-primary ${type == 1 ? `border-b-2` : ''}`} onClick={() => settype(1)}>
              Product
            </div>
            <div className={`w-full flex justify-center cursor-pointer py-4 border-primary ${type == 2 ? `border-b-2` : ''}`} onClick={() => settype(2)}>
              Description
            </div>
          </div>

          {
            type == 0 ?
              <div className="bg-white shadow-lg py-12 mt-6 rounded-lg flex justify-center flex-wrap">
                <div className="w-1/2">
                  <div id="logo" className="flex">
                    <label className="w-1/2 text-right">Upload Banner<span className="text-primary">*</span></label>
                    <div className="w-full pl-5">
                      <div className="text-sm ">
                        <button className="border-2 bg-white px-3 rounded-lg" onClick={() => {thumbnailRef.current.click()}} >Browse...</button>
                        <input ref={thumbnailRef} className="hidden" type="file" name="logo" onChange={handleChangeThumbnailImage}/>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="w-full mt-4 px-4">
                  <div className="h-1 w-full bg-text-primary opacity-25 border"></div>
                </div>

                <div className="flex flex-wrap w-full px-4">
                  {
                    banners.map (v => 
                      <div className="w-1/2 flex flex-wrap mt-4 px-2">
                        <div className="w-full border border-gray-500">
                          <img src={v.base64} className="content-center"/>
                        </div>
                        <div className="w-full mt-1 flex">
                          <input className="border border-gray-300 px-4 py-2 w-full" placeholder="Link Url : https://" type="text" value={v.url} onChange={(e)=>{ setbanners(banners.map(k => k == v ? {...k, url: e.target.value}  : k)) }}/>
                        </div>
                        <div className="w-full mt-1 flex">
                          <button className="bg-primary px-4 py-2 text-white w-full ml-2 rounded" onClick={() => setbanners(banners.filter(k => k != v))}>Delete</button>
                        </div>
                      </div>
                    )
                  }
                </div>


              </div>
            : null
          }

          {
            type == 1 ?
              <div className="bg-white shadow-lg py-12 mt-6 rounded-lg flex justify-center flex-wrap">
                <div className="w-1/2">
                  <div className="w-full">
                    <Select 
                      options={sellers}
                      placeholder="Pilih Seller"
                      value={selectedSeller}
                      onChange={(e) => setSelectedSeller(e)}
                      isDisabled={selectedData != null}
                    />
                  </div>
                  <div className="w-full mt-4">
                    <Select 
                      options={products}
                      placeholder="Pilih Produk"
                      value={selectedProduct}
                      onChange={(e) => setSelectedProduct(e)}
                      isDisabled={selectedData != null}
                    />
                  </div>
                  <button className="w-full rounded btn-primary py-4 mt-4" onClick={handleAddProduct}>Add</button>
                </div>

                <div id="table" className="w-full mt-4 px-4">
                  <table className="w-full">
                    <thead>
                      {/* <th className="text-center"></th> */}
                      <th>No</th>
                      <th>Product Name</th>
                      <th>Seller</th>
                      <th className="text-center">Action</th>
                    </thead>
                    <tbody>
                      {
                        tableDatas.map((v, i) => (
                          <tr key={v.id} className={`${i%2 == 0 ? 'bg-gray-200' : 'bg-white'} cursor-pointer hover:bg-gray-300`} onClick={(e) => {
                            const target : any = e.target;
                            if(target.id === 'checkbox') return
                          }}>
                            <td>
                              {i+1}
                            </td>
                            <td>
                              {v.name}
                            </td>
                            <td>
                              {v.seller.store_name}
                            </td>
                            <td className="text-center">
                              <button className="px-2 py-2 border border-primary rounded bg-white mx-1 hover:bg-gray-200" onClick={() => settableDatas(tableDatas.filter(k => k.id != v.id))}>
                                <img src="/images/ic_trash.png" alt="" style={{ width: '24px', height: '24px' }} />
                              </button>
                            </td>
                          </tr>
                        ))
                      }
                    </tbody>
                  </table>
                </div>

              </div>
            : null
          }

          {
            type == 2 ?
              <div className="bg-white shadow-lg py-12 mt-6 rounded-lg flex justify-center flex-wrap">
                <div className="w-5/6">
                  
                  <Editor theme="snow"
                    modules={modules}
                    formats={formats}
                    value={description}
                    onChange={(e) => setdescription(e)} />
              
                </div>

              </div>
            : null
          }

          

        </div>
      </div>

      <Modal open={modalCrop.open} onClose={() => setModalCrop({...modalCrop, open: false})}>
        <CropPicker 
          ratio={2/1} 
          onClosed={() => setModalCrop({...modalCrop, open: false})} 
          onSubmit={(croppedImage) => handleCroppedImage(croppedImage, modalCrop.cb)} 
          image={modalCrop.image}
          onCrop={() => setLoading(true)}
          onCropped={() => setLoading(false)}
        />
      </Modal>
    </BackOfficeBase>
  )
}

dashboard.getInitialProps = async (ctx) => {
  const { endpoint } = ctx.store.getState().authentication
  const { user, token } = ctx.store.getState().lakkonState

  return {
      user: user,
      cloudinaryUrl: endpoint.cloudinaryUrl,
      apiUrl: endpoint.apiUrl,
      token: token
  }
}

export default connect(
  (state) => state, {}
)(dashboard);