import { useEffect, useState } from 'react'
import Link from 'next/link'
import { connect } from 'react-redux'
import BackOfficeBase from 'components/LakkonOfficeBase'
import ContentHeader from 'components/BackOfficeBase/ContentHeader'
import { getArticles,deleteArticle, SORTING, STATUS, getArticlesManagement, updateStatusArticle, SORT_BY } from 'data/api/article'
import Loading from 'components/Loading'

const breadcumb = [
  {
    title: 'Home',
    link: '/',
  },
  {
    title: 'Content',
    link: '/',
  },
  {
    title: 'Article',
    link: '/',
  },
]

type TProps = {
  apiUrl: string,
  token: string,
  authentication: any
}

const Status = ({status}) => {
  switch (status) {
    case STATUS.APPROVED:
      return (
        <div className="bg-green-500 text-white px-3 py-1 text-center font-medium rounded-md text-sm">
          {status}
        </div>
      )
    case STATUS.DRAFT:
      return (
        <div className="bg-gray-400 text-text-primary px-3 py-1 text-center font-medium rounded-md text-sm">
          {status}
        </div>
      )
    default:
      return (
        <div className="bg-primary text-white px-3 py-1 text-center font-medium rounded-md text-sm">
          {status}
        </div>
      )
  }
}

const Product = ({apiUrl, token, authentication}: TProps) => {
  const [page, setPage] = useState(1)
  const [limit, setLimit] = useState(10)
  const [keyword, setKeyword] = useState('')
  const [tabledDatas, setTableDatas] = useState([])
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    doGetArticles()
  }, [page]);

  const doGetArticles = async () => {
    setLoading(true)
    const res = await getArticlesManagement(apiUrl, token, {
      layout_type: 'list_layout',
      limit: limit,
      page: page,
      sort_by: SORT_BY.VIEW_COUNT,
      sort_type: SORTING.DESC,
      status: STATUS.ALL
    })
    setTableDatas(res.data ? res.data.map(v => ({...v, isChecked: false})) : [])
    setLoading(false)
  }

  const handleDelete = async (id) => {
    setLoading(true)
    const res = await deleteArticle(apiUrl, token, id)
    doGetArticles()
  }

  const handleChangeAction = async (e) => {
    setLoading(true)
    const promises = [];
    tabledDatas.filter(v => v.isChecked).forEach(v => {
      promises.push(updateStatusArticle(apiUrl, token, {id: v.id, status: e.target.value}))
    })
    Promise.all(promises)
      .then(res => {
        doGetArticles()
      }).catch(err => {});
  }

  const handleChangeChecked = (index) => {
    setTableDatas(tabledDatas.map((v,i) => ({...v, isChecked: index === i ? !v.isChecked : v.isChecked})))
  }

  return (
    <BackOfficeBase>
      <Loading show={loading} />
      <div className="w-full flex flex-wrap">
        <ContentHeader title="Article" breadcumb={breadcumb} />

        <div className="w-full flex justify-end mt-4">
          <Link href="/admin/content/article/add">
            <button className="bg-orange-500 text-white font-bold px-5 py-2 mx-1 rounded">ADD NEW ARTICLE</button>
          </Link>
        </div>

        <div className="w-full">
          <div className="bg-white shadow-lg p-4 mt-4 rounded-lg flex flex-wrap">

            <div id="filters" className="w-full flex items-center">
              <div className="w-full">
                <input className="border border-gray-300" type="text" placeholder="search" value={keyword} onChange={(e) => setKeyword(e.target.value)}/>
              </div>
              <div className="w-full flex justify-end">
                {/* <button className="py-1 px-3 border-2 border-gray-300 bg-white rounded text-text-primary flex items-center mr-2">
                  <img className="mr-2" src="/images/ic_filter.png" alt="" />
                  FILTERS
                </button>
                <button className="py-1 px-3 border-2 border-gray-300 bg-white rounded text-text-primary flex items-center">
                  <img className="mr-2" src="/images/ic_export.png" alt="" />
                  EXPORT
                </button> */}
              </div>
            </div>

            <div id="sortings" className="w-full flex items-center mt-4">
              <div className="w-full flex flex-wrap items-center">
                <div className="flex">
                  <div className="border border-gray-400 rounded-l flex items-center h-full">
                    <select className="py-2 px-5 text-text-primary" value="" onChange={handleChangeAction}>
                      <option value="" disabled>Action</option>
                      <option value={STATUS.APPROVED}>Approved</option>
                      <option value={STATUS.DRAFT}>Draft</option>
                    </select>
                  </div>
                  <div className="rounded-r px-3 py-1 bg-gray-300 items-center flex">
                    <img src="/images/ic_arrow_down.png" alt=""/>
                  </div>
                </div>
                <div className="text-text-primary font-medium ml-3">
                  {/* 106 records found */}
                </div>
              </div>
              <div className="w-full flex justify-end items-center">
                {/* <select className="w-auto">
                  <option>10</option>
                </select>
                <div className="text-text-primary font-medium ml-3">
                  Per Page
                </div> */}
                <button className="px-3 py-2 bg-gray-400 rounded text-text-primary flex items-center ml-4" onClick={() => setPage(page - 1)}>
                  <img src="/images/ic_chevron_left.png" alt="" />
                </button>
                <input className="ml-3 w-12 text-center border border-gray-300" type="text" value={page}/>
                {/* <div className="text-text-primary font-medium ml-3">
                  Of 6
                </div> */}
                <button className="px-3 py-2 bg-gray-400 rounded text-text-primary flex items-center ml-4" onClick={() => setPage(page + 1 )}>
                  <img src="/images/ic_arrow_right.png" alt="" />
                </button>
              </div>
            </div>

            <div id="table" className="w-full mt-8">
              <table className="w-full">
                <thead>
                  <th className="text-center"></th>
                  <th>ID</th>
                  <th>Judul</th>
                  <th>Author</th>
                  <th>Status</th>
                  <th>Action</th>
                </thead>
                <tbody>
                  {
                    tabledDatas.map((v, i) => (
                      <tr key={v.id} className={`${i%2 == 0 ? 'bg-gray-200' : 'bg-white'} cursor-pointer hover:bg-gray-300`} onClick={(e) => {
                        const target : any = e.target;
                        if(target.id === 'checkbox') return
                        // window.location.href = `/backoffice/products/detail/${v.slug}`
                      }}>
                        <td className="text-center" id="checkbox">
                          <input id="checkbox" type="checkbox" checked={v.isChecked} onChange={() => handleChangeChecked(i)}/>
                        </td>
                        <td>
                          {v.id}
                        </td>
                        <td>
                          {v.title}
                        </td>
                        <td>
                          {v.author_name}
                        </td>
                        <td>
                          <Status status={v.status}/>
                        </td>
                        <td className="text-center">
                          <Link href={`/admin/content/article/${v.slug}`}>
                            <button className="px-2 py-2 border border-primary rounded bg-white mx-1 hover:bg-gray-200">
                              <img src="/images/ic_lakkon_edit_red.png" alt="" style={{ width: '24px', height: '24px' }} />
                            </button>
                          </Link>
                          <button className="px-2 py-2 border border-primary rounded bg-white mx-1 hover:bg-gray-200" onClick={() => handleDelete(v.id)}>
                            <img src="/images/ic_trash.png" alt="" style={{ width: '24px', height: '24px' }} />
                          </button>
                        </td>
                      </tr>
                    ))
                  }
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </BackOfficeBase>
  )
}

Product.getInitialProps = (ctx) => {
  const { endpoint } = ctx.store.getState().authentication;
  const { user, token } = ctx.store.getState().lakkonState
  return {
      apiUrl: endpoint.apiUrl,
      token: token
  }
}

export default connect(
  (state) => state, {}
)(Product);