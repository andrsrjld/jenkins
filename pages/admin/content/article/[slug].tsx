import {useState, useRef} from 'react'
import { connect } from 'react-redux'
import BackOfficeBase from 'components/LakkonOfficeBase'
import { uploadImage } from 'data/api/image'
import { getArticleDetail, updateArticle } from 'data/api/article'
import Loading from 'components/Loading'
import Router from 'next/router'
import {compressImage} from 'util/helper'

import Modal from 'components/Modal'
import CropPicker from 'components/CropPicker'

type TProp = {
  user: any,
  cloudinaryUrl: string,
  apiUrl: string,
  token: string,
  article: any
}

const dashboard = ({user, cloudinaryUrl, apiUrl, token, article} : TProp) => {
  const authorRef = useRef(null)
  const thumbnailRef = useRef(null)
  const [loading, setLoading] = useState(false)

  const [title, setTitle] = useState(article.title)
  const [author, setAuthor] = useState(article.author_name)
  const [description, setDescription] = useState(article.content)
  const [shortDescription, setShortDescription] = useState(article.highlight)

  const [images, setImages] = useState([])
  const [authorImage, setAuthorImage] = useState({base64: article.author_image, file: null})
  const [thumbnailImage, setThumbnailImage] = useState({base64: article.image_url, file: null})
  const [bannerType, setBannerType] = useState('')
  const [bannerImage, setBannerImage] = useState(null)

  const [modalCrop, setModalCrop] = useState({
    image: '',
    open: false,
    cb: (props: any) => {},
    ratio: 1/1
  })

  const handleSave = async () => {
    console.log(user)
    setLoading(true)
    let request = {
      id: article.id,
      title: title,
      content: description,
      highlight: shortDescription,
      image_url: article.image_url,
      author_image: article.author_image,
      author_name: author
    }

    try {

      // Upload Thumbnail IF any
      if(thumbnailImage.file) {
        const resLogo = await uploadImage(cloudinaryUrl, {
          file: thumbnailImage.file,
          folder: 'article',
          tag: ''
        })
        request.image_url = resLogo.secure_url
      }

      // Upload Author Image IF any
      if(authorImage.file) {
        const resAuthor = await uploadImage(cloudinaryUrl, {
          file: authorImage.file,
          folder: 'article',
          tag: ''
        })
        request.author_image = resAuthor.secure_url
      }

      // Update Account
      const res = await updateArticle(apiUrl, token, request)
      Router.push('/admin/content/article')
    } catch (err) {
      // TODO HANDLE ERR
    }

    setLoading(false)
  }

  const getBase64 = (file, cb) =>{
    let reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = function () {
        cb(reader.result)
    }
    reader.onerror = function (error) {
        console.log('Error: ', error)
    }
  }

  const handleChangeAuthorImage = (e) => {
    const file = e.target.files[0]
    getBase64(file, (result) => {
      // setAuthorImage({base64: result, file: file})

      setModalCrop({
        image: result,
        open: true,
        cb: (props : any) => setAuthorImage({base64: props.croppedImage, file: props.blobCompressed}),
        ratio: 1/1
      })
    });
  }

  const handleChangeThumbnailImage = (e) => {
    const file = e.target.files[0]
    getBase64(file, (result) => {
      // setThumbnailImage({base64: result, file: file})
      setModalCrop({
        image: result,
        open: true,
        cb: (props : any) => setThumbnailImage({base64: props.croppedImage, file: props.blobCompressed}),
        ratio: 1/1
      })
    });
  }

  const handleCroppedImage = async (croppedImage, cb) => {
    setLoading(true)
    let blob = await fetch(croppedImage).then(r => r.blob());
    const blobCompressed = await compressImage(blob)
    modalCrop.cb({croppedImage, blobCompressed})
    setModalCrop({
      ...modalCrop,
      open: false
    })
    setLoading(false)
  }

  return (
    <BackOfficeBase>
      <Loading show={loading}/>
      <div className="w-full flex flex-wrap">
        <div className="w-full flex">
          <div className="w-full font-medium text-2xl text-text-primary">
            Article
          </div>
          <div className="w-full">
            <div className="flex justify-end">
              <button className="bg-green-500 text-white font-bold px-5 py-1 mx-1 rounded" onClick={() => handleSave()}>SAVE ARTIKEL</button>
            </div>
          </div>
        </div>
        <div className="w-full text-text-primary">
          <div className="bg-white shadow-lg py-12 mt-12 rounded-lg flex justify-center flex-wrap">
            <div className="w-1/2">
              
              <div id="storeName" className="flex mt-8">
                <label className="w-1/2 text-right">Judul<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <input type="text" className="border border-gray-300 px-4 py-2 w-full" placeholder="" value={title} onChange={(e) => setTitle(e.target.value)}/>
                </div>
              </div>

              <div id="storeName" className="flex mt-8">
                <label className="w-1/2 text-right">Nama Author<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <input type="text" className="border border-gray-300 px-4 py-2 w-full" placeholder="" value={author} onChange={(e) => setAuthor(e.target.value)}/>
                </div>
              </div>

              <div id="logo" className="flex mt-8">
                <label className="w-1/2 text-right">Foto Author<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <div className="border-2 border-gray-200 w-10 h-10">
                    <img className="w-full h-full object-cover" src={authorImage ? authorImage.base64 : ''} alt="logo image"/>
                  </div>
                  <div className="text-sm ">
                    <button className="border-2 bg-white px-3 rounded-lg mt-2" onClick={() => {authorRef.current.click()}} >Browse...</button>
                    {`${authorImage ? '' : ' No File Selected'}`}
                    <input ref={authorRef} className="hidden" type="file" name="logo" onChange={handleChangeAuthorImage}/>
                  </div>
                  {/* <input type="checkbox" name="delete" id="delete" className="mt-2"/> Delete Image */}
                </div>
              </div>

              <div id="logo" className="flex mt-8">
                <label className="w-1/2 text-right">Thumbnail<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <div className="border-2 border-gray-200 w-10 h-10">
                    <img className="w-full h-full object-cover" src={thumbnailImage ? thumbnailImage.base64 : ''} alt="logo image"/>
                  </div>
                  <div className="text-sm ">
                    <button className="border-2 bg-white px-3 rounded-lg mt-2" onClick={() => {thumbnailRef.current.click()}} >Browse...</button>
                    {`${thumbnailImage ? '' : ' No File Selected'}`}
                    <input ref={thumbnailRef} className="hidden" type="file" name="logo" onChange={handleChangeThumbnailImage}/>
                  </div>
                  {/* <input type="checkbox" name="delete" id="delete" className="mt-2"/> Delete Image */}
                </div>
              </div>
              
              <div id="hightligt" className="flex mt-8">
                <label className="w-1/2 text-right">Highlight<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <textarea className="border border-gray-300 px-4 py-2 focus:outline-none w-full rounded" value={shortDescription} onChange={(e) => setShortDescription(e.target.value)}></textarea>
                </div>
              </div>

              <div id="storeDescription" className="flex mt-8">
                <label className="w-1/2 text-right">Konten<span className="text-primary">*</span></label>
                <div className="w-full pl-5">
                  <textarea className="border border-gray-300 px-4 py-2 focus:outline-none w-full rounded" value={description} onChange={(e) => setDescription(e.target.value)}></textarea>
                </div>
              </div>
          
            </div>

          </div>

        </div>
      </div>

      <Modal open={modalCrop.open} onClose={() => setModalCrop({...modalCrop, open: false})}>
        <CropPicker 
          ratio={modalCrop.ratio} 
          onClosed={() => setModalCrop({...modalCrop, open: false})} 
          onSubmit={(croppedImage) => handleCroppedImage(croppedImage, modalCrop.cb)} 
          image={modalCrop.image}
          onCrop={() => setLoading(true)}
          onCropped={() => setLoading(false)}
        />
      </Modal>
    </BackOfficeBase>
  )
}

dashboard.getInitialProps = async (ctx) => {
  const { endpoint } = ctx.store.getState().authentication
  const { user, token } = ctx.store.getState().lakkonState
  const {query} = ctx
  const slug = query.slug ? query.slug : ''
  const article = await getArticleDetail(token, slug)

  return {
      user: user,
      cloudinaryUrl: endpoint.cloudinaryUrl,
      apiUrl: endpoint.apiUrl,
      token: token,
      article: article.data
  }
}

export default connect(
  (state) => state, {}
)(dashboard);