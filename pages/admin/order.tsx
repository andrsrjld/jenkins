import { useState, useEffect } from 'react'
import {connect} from 'react-redux'
import BackOfficeBase from 'components/LakkonOfficeBase'
import ContentHeader from 'components/BackOfficeBase/ContentHeader'
import { motion } from "framer-motion"
import { getOrdersSeller, updateStatusOrder, getOrderDetailManagement, STATUS, getOrdersAdmin, updateStatusOrderAdmin } from 'data/api/order'
import {formatNumber} from 'util/helper'
import { getSellers } from 'data/api/seller'
import OrderItem from 'components/OrderItem'

const breadcumb = [
  {
    title: 'Home',
    link: '/',
  },
  {
    title: 'Sales',
    link: '/',
  }
]

const sliderMenu = [
  {
    content: 0,
    name: 'Belum Dibayar',
    qty: '1',
    isActive: true,
    status: 'Menunggu Konfirmasi'
  },
  {
    content: 1,
    name: 'Dibayar',
    qty: '1',
    isActive: false,
    status: 'Pesanan Sudah Dibayar'
  },
  {
    content: 2,
    name: 'Diproses',
    qty: '1',
    isActive: false,
    status: 'Pesanan Diproses'
  },
  {
    content: 3,
    name: 'Dikirim',
    qty: '1',
    isActive: false,
    status: 'Pesanan Dikirim'
  },
  {
    content: 4,
    name: 'Selesai',
    qty: '2',
    isActive: false,
    status: 'Pesanan Selesai'
  },
  {
    content: 5,
    name: 'Dikembalikan',
    qty: '3',
    isActive: false,
    status: 'Pesanan Dibatalkan'
  },
]
const SliderItem = ({data, cbClick}) => (
  <div className={`font-medium px-2 py-2 bg-white cursor-pointer border-b-2 w-64 text-center ${data.isActive ? 'border-primary text-primary' : 'border-gray-500 text-text-primary'} hover:text-primary`}
    onClick={() => cbClick(data)}>
    {`${data.name}`}
  </div>
)

const ItemProses = ({data, callback, action, callbackDetail}) => (
  <div className="w-full">
    <div className="w-full bg-white shadow-sm p-4 mt-4 rounded flex flex-wrap">
      <div className="w-full flex text-text-primary font-medium">
        <div className="w-full px-4 border-r-2 flex">
          <input className="mr-4 mt-1" type="checkbox" />
          <div className="w-full flex flex-wrap">
            <div className="w-full">
              {data.user_name}
            </div>
            <div className="w-full">
              No Transaksi <span className="font-normal">{data.invoice_no}</span>
            </div>
            {/* <div className="w-full flex mt-2 font-normal">
              <img className="w-24" src={data.image} alt=""/>
              <div>
                {`${data.sellerName} - ${data.productName}`}
              </div>
            </div> */}
          </div>
        </div>
        <div className="w-full px-4 border-r-2">
          Pengiriman <br></br>
          <div className="font-normal">
            <table>
              <tr>
                <tr>
                  <td className="pt-1 pb-0 px-0">Penerima</td>
                  <td className="pt-1 pb-0 px-2">:</td>
                  <td className="pt-1 pb-0 px-0">{data.receiver_name}</td>
                </tr>
                <tr>
                  <td className="pt-1 pb-0 px-0">Kurir</td>
                  <td className="pt-1 pb-0 px-2">:</td>
                  <td className="pt-1 pb-0 px-0">{data.shipping}</td>
                </tr>
              </tr>
            </table>
          </div>
        </div>
        <div className="w-full px-4 flex flex-wrap flex-col">
          <div className="w-full">
            Status Transaksi
          </div>
          <div className="w-full flex mt-2">
            <div className="mx-1 px-2 py-1 rounded-full bg-primary text-white flex items-center">
              <img src="/images/ic_check_white.png" alt="" />
            </div>
            <div className="mx-1 px-2 py-1 rounded-full bg-primary text-white flex items-center">
              <img src="/images/ic_check_white.png" alt="" />
            </div>
            <div className="mx-1 px-3 py-1 rounded-full bg-gray-300">
              3
            </div>
            <div className="mx-1 px-3 py-1 rounded-full bg-gray-300">
              4
            </div>
          </div>
          <div className="w-full mt-4">
            <button className="bg-gray-300 px-3 py-1 w-auto font-medium rounded">{data.status}</button>
          </div>
          <div className="w-full mt-4 font-normal">
            {data.timeLimit}
          </div>
        </div>
        <div className="w-full px-4 items-center spac">
          {/* <button className="w-full border rounded py-2 font-medium">
            <input type="text" placeholder="Masukkan Nomor Resi" value={data.airway_bill ? data.airway_bill : ''} onChange={callback(data.id)}/>
          </button>
          <button className="w-full border rounded py-2 font-medium mt-2" onClick={() => action(data, STATUS.DIKIRIM)}>
            SIMPAN RESI
          </button> */}
          <button className="w-full rounded py-2 font-medium mt-2 flex justify-center items-center text-primary" onClick={() => callbackDetail(data)}>
            LIHAT DETAIL <img className="ml-2" src="/images/ic_arrow_down_red.png" alt="" />
          </button>
        </div>
      </div>
    </div>
    
    <div className={`w-full flex text-text-primary ${data.detail ? '' : 'hidden'}`}>
        <div className="w-full pl-4 pr-2">
          <div className="w-full bg-white shadow-sm p-4 mt-4 rounded">
            <div>
              <span className="font-medium">Detail Barang</span>
            </div>
            {
              data.detail?.order_items.map(v => (
                <div key={v.product_name} className="w-full flex mt-4">
                  <div className="w-full flex">
                    <img className="w-24 mr-4" src={v.img_url} alt=""></img>
                    {`${v.product_name}`}
                  </div>
                  <div className="w-1/2 text-right">
                    {v.quantity} Barang
                  </div>
                  <div className="w-1/2 text-right">
                    {`Rp ${formatNumber(v.subtotal)}`}
                  </div>
                </div>
              ))
            }
            <div className="mt-4 w-full h-px bg-text-primary"/>
            <div className="w-full mt-4 flex justify-end">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Total Berat Barang
              </div>
              <div className="w-1/2 text-right">
                {data.detail?.total_weight} Gram
              </div>
            </div>
            <div className="w-full mt-4 flex justify-end">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Ongkos Kirim
              </div>
              <div className="w-1/2 text-right">
                {`Rp ${formatNumber(data.detail?.total_shipping)}`}
              </div>
            </div>
            {/* <div className="w-full mt-4 flex justify-end">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Biaya Layanan
              </div>
              <div className="w-1/2 text-right">
                Rp 5.000
              </div>
            </div> */}
            <div className="w-full mt-4 flex justify-end">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Diskon
              </div>
              <div className="w-1/2 text-right">
                - Rp 0
              </div>
            </div>
            <div className="w-full mt-4 flex justify-end font-medium">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Grand Total
              </div>
              <div className="w-1/2 text-right">
              {`Rp ${formatNumber(data.detail?.total_paid)}`}
              </div>
            </div>
          </div>
        </div>
        <div className="w-full pl-2 flex flex-col">
          <div className="w-full bg-white shadow-sm p-4 mt-4 rounded">
            <div>
              <span className="font-medium">Alamat Pengiriman</span>
            </div>
            <div className="mt-4">
              {data.buyerName} <br></br>
              {data.detail?.address}
              <br></br>
              <span className="font-medium">Telepon</span> : {data.detail?.phone}
            </div>
          </div>
          <div className="w-full bg-white shadow-sm p-4 mt-4 rounded">
            <div>
              <span className="font-medium">Tindakan</span>
            </div>
            <div className="mt-4">
              <div>
                <button className="bg-gray-400 rounded font-medium px-3 py-2">CETAK PESANAN</button>
                <button className="bg-gray-400 rounded font-medium px-3 py-2 ml-4">CETAK BUKTI PEMBAYARAN</button>
              </div>
            </div>
          </div>
          <div className="w-full bg-white shadow-sm p-4 mt-4 rounded">
            <div>
              <span className="font-medium">Catatan Pembelian</span>
            </div>
            <div className="mt-4">
              Tidak ada catatan
            </div>
          </div>
        </div>
      </div>
  
  </div>
)

const ItemDikirim = ({data, callback, action, callbackDetail}) => (
  <div className="w-full">
    <div className="w-full bg-white shadow-sm p-4 mt-4 rounded flex flex-wrap">
      <div className="w-full flex text-text-primary font-medium">
        <div className="w-full px-4 border-r-2 flex">
          <input className="mr-4 mt-1" type="checkbox" />
          <div className="w-full flex flex-wrap">
            <div className="w-full">
              {data.user_name}
            </div>
            <div className="w-full">
              No Transaksi <span className="font-normal">{data.invoice_no}</span>
            </div>
            {/* <div className="w-full flex mt-2 font-normal">
              <img className="w-24" src={data.image} alt=""/>
              <div>
                {`${data.sellerName} - ${data.productName}`}
              </div>
            </div> */}
          </div>
        </div>
        <div className="w-full px-4 border-r-2">
          Pengiriman <br></br>
          <div className="font-normal">
            <table>
              <tr>
                <tr>
                  <td className="pt-1 pb-0 px-0">Penerima</td>
                  <td className="pt-1 pb-0 px-2">:</td>
                  <td className="pt-1 pb-0 px-0">{data.receiver_name}</td>
                </tr>
                <tr>
                  <td className="pt-1 pb-0 px-0">Kurir</td>
                  <td className="pt-1 pb-0 px-2">:</td>
                  <td className="pt-1 pb-0 px-0">{data.shipping}</td>
                </tr>
              </tr>
            </table>
          </div>
        </div>
        <div className="w-full px-4 flex flex-wrap flex-col">
          <div className="w-full">
            Status Transaksi
          </div>
          <div className="w-full flex mt-2">
            <div className="mx-1 px-2 py-1 rounded-full bg-primary text-white flex items-center">
              <img src="/images/ic_check_white.png" alt="" />
            </div>
            <div className="mx-1 px-2 py-1 rounded-full bg-primary text-white flex items-center">
              <img src="/images/ic_check_white.png" alt="" />
            </div>
            <div className="mx-1 px-2 py-1 rounded-full bg-primary text-white flex items-center">
              <img src="/images/ic_check_white.png" alt="" />
            </div>
            <div className="mx-1 px-3 py-1 rounded-full bg-gray-300">
              4
            </div>
          </div>
          <div className="w-full mt-4">
            <button className="bg-gray-300 px-3 py-1 w-auto font-medium rounded">{data.status}</button>
          </div>
          <div className="w-full mt-4 font-normal">
            {data.timeLimit}
          </div>
        </div>
        <div className="w-full px-4 items-center">
          <button className="w-full border rounded py-2 font-medium">
            <input type="text" placeholder="Masukkan Nomor Resi" value={data.airway_bill ? data.airway_bill : ''} onChange={callback(data.id)} disabled/>
          </button>
          <button className="w-full rounded py-2 font-medium mt-2 flex justify-center items-center text-primary" onClick={() => callbackDetail(data)}>
            LIHAT DETAIL <img className="ml-2" src="/images/ic_arrow_down_red.png" alt="" />
          </button>
        </div>
      </div>
    </div>

    <div className={`w-full flex text-text-primary ${data.detail ? '' : 'hidden'}`}>
        <div className="w-full pl-4 pr-2">
          <div className="w-full bg-white shadow-sm p-4 mt-4 rounded">
            <div>
              <span className="font-medium">Detail Barang</span>
            </div>
            {
              data.detail?.order_items.map(v => (
                <div key={v.product_name} className="w-full flex mt-4">
                  <div className="w-full flex">
                    <img className="w-24 mr-4" src={v.img_url} alt=""></img>
                    {`${v.product_name}`}
                  </div>
                  <div className="w-1/2 text-right">
                    {v.quantity} Barang
                  </div>
                  <div className="w-1/2 text-right">
                    {`Rp ${formatNumber(v.subtotal)}`}
                  </div>
                </div>
              ))
            }
            <div className="mt-4 w-full h-px bg-text-primary"/>
            <div className="w-full mt-4 flex justify-end">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Total Berat Barang
              </div>
              <div className="w-1/2 text-right">
                {data.detail?.total_weight} Gram
              </div>
            </div>
            <div className="w-full mt-4 flex justify-end">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Ongkos Kirim
              </div>
              <div className="w-1/2 text-right">
                {`Rp ${formatNumber(data.detail?.total_shipping)}`}
              </div>
            </div>
            {/* <div className="w-full mt-4 flex justify-end">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Biaya Layanan
              </div>
              <div className="w-1/2 text-right">
                Rp 5.000
              </div>
            </div> */}
            <div className="w-full mt-4 flex justify-end">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Diskon
              </div>
              <div className="w-1/2 text-right">
                - Rp 0
              </div>
            </div>
            <div className="w-full mt-4 flex justify-end font-medium">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Grand Total
              </div>
              <div className="w-1/2 text-right">
              {`Rp ${formatNumber(data.detail?.total_paid)}`}
              </div>
            </div>
          </div>
        </div>
        <div className="w-full pl-2 flex flex-col">
          <div className="w-full bg-white shadow-sm p-4 mt-4 rounded">
            <div>
              <span className="font-medium">Alamat Pengiriman</span>
            </div>
            <div className="mt-4">
              {data.buyerName} <br></br>
              {data.detail?.address}
              <br></br>
              <span className="font-medium">Telepon</span> : {data.detail?.phone}
            </div>
          </div>
          <div className="w-full bg-white shadow-sm p-4 mt-4 rounded">
            <div>
              <span className="font-medium">Tindakan</span>
            </div>
            <div className="mt-4">
              <div>
                <button className="bg-gray-400 rounded font-medium px-3 py-2">CETAK PESANAN</button>
                <button className="bg-gray-400 rounded font-medium px-3 py-2 ml-4">CETAK BUKTI PEMBAYARAN</button>
              </div>
            </div>
          </div>
          <div className="w-full bg-white shadow-sm p-4 mt-4 rounded">
            <div>
              <span className="font-medium">Catatan Pembelian</span>
            </div>
            <div className="mt-4">
              Tidak ada catatan
            </div>
          </div>
        </div>
      </div>

  </div>
)

const ItemSelesai = ({data, callback, action, callbackDetail}) => (
  <div className="w-full">
    <div className="w-full bg-white shadow-sm p-4 mt-4 rounded flex flex-wrap">
      <div className="w-full flex text-text-primary font-medium">
        <div className="w-full px-4 border-r-2 flex">
          <input className="mr-4 mt-1" type="checkbox" />
          <div className="w-full flex flex-wrap">
            <div className="w-full">
              {data.user_name}
            </div>
            <div className="w-full">
              No Transaksi <span className="font-normal">{data.invoice_no}</span>
            </div>
            {/* <div className="w-full flex mt-2 font-normal">
              <img className="w-24" src={data.image} alt=""/>
              <div>
                {`${data.sellerName} - ${data.productName}`}
              </div>
            </div> */}
          </div>
        </div>
        <div className="w-full px-4 border-r-2">
          Pengiriman <br></br>
          <div className="font-normal">
            <table>
              <tr>
                <tr>
                  <td className="pt-1 pb-0 px-0">Penerima</td>
                  <td className="pt-1 pb-0 px-2">:</td>
                  <td className="pt-1 pb-0 px-0">{data.receiver_name}</td>
                </tr>
                <tr>
                  <td className="pt-1 pb-0 px-0">Kurir</td>
                  <td className="pt-1 pb-0 px-2">:</td>
                  <td className="pt-1 pb-0 px-0">{data.shipping}</td>
                </tr>
              </tr>
            </table>
          </div>
        </div>
        <div className="w-full px-4 flex flex-wrap flex-col">
          <div className="w-full">
            Status Transaksi
          </div>
          <div className="w-full flex mt-2">
            <div className="mx-1 px-2 py-1 rounded-full bg-primary text-white flex items-center">
              <img src="/images/ic_check_white.png" alt="" />
            </div>
            <div className="mx-1 px-2 py-1 rounded-full bg-primary text-white flex items-center">
              <img src="/images/ic_check_white.png" alt="" />
            </div>
            <div className="mx-1 px-2 py-1 rounded-full bg-primary text-white flex items-center">
              <img src="/images/ic_check_white.png" alt="" />
            </div>
            <div className="mx-1 px-2 py-1 rounded-full bg-primary text-white flex items-center">
              <img src="/images/ic_check_white.png" alt="" />
            </div>
            <div className="mx-1 px-3 py-1 rounded-full bg-gray-300 opacity-0">
              4
            </div>
          </div>
          <div className="w-full mt-4">
            <button className="bg-gray-300 px-3 py-1 w-auto font-medium rounded">{data.status}</button>
          </div>
          <div className="w-full mt-4 font-normal">
            {data.timeLimit}
          </div>
        </div>
        <div className="w-full px-4 items-center">
          {/* <button className="w-full border rounded py-2 font-medium">
            <input type="text" placeholder="Masukkan Nomor Resi" value={data.airway_bill ? data.airway_bill : ''} onChange={callback(data.id)} disabled/>
          </button> */}
          {/* <button className="w-full border rounded py-2 font-medium mt-2" onClick={() => action(data, STATUS.DIKIRIM)}>
            SIMPAN RESI
          </button> */}
          <button className="w-full rounded py-2 font-medium mt-2 flex justify-center items-center text-primary"onClick={() => callbackDetail(data)}>
            LIHAT DETAIL <img className="ml-2" src="/images/ic_arrow_down_red.png" alt="" />
          </button>
        </div>
      </div>
    </div>

    <div className={`w-full flex text-text-primary ${data.detail ? '' : 'hidden'}`}>
        <div className="w-full pl-4 pr-2">
          <div className="w-full bg-white shadow-sm p-4 mt-4 rounded">
            <div>
              <span className="font-medium">Detail Barang</span>
            </div>
            {
              data.detail?.order_items.map(v => (
                <div key={v.product_name} className="w-full flex mt-4">
                  <div className="w-full flex">
                    <img className="w-24 mr-4" src={v.img_url} alt=""></img>
                    {`${v.product_name}`}
                  </div>
                  <div className="w-1/2 text-right">
                    {v.quantity} Barang
                  </div>
                  <div className="w-1/2 text-right">
                    {`Rp ${formatNumber(v.subtotal)}`}
                  </div>
                </div>
              ))
            }
            <div className="mt-4 w-full h-px bg-text-primary"/>
            <div className="w-full mt-4 flex justify-end">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Total Berat Barang
              </div>
              <div className="w-1/2 text-right">
                {data.detail?.total_weight} Gram
              </div>
            </div>
            <div className="w-full mt-4 flex justify-end">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Ongkos Kirim
              </div>
              <div className="w-1/2 text-right">
                {`Rp ${formatNumber(data.detail?.total_shipping)}`}
              </div>
            </div>
            {/* <div className="w-full mt-4 flex justify-end">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Biaya Layanan
              </div>
              <div className="w-1/2 text-right">
                Rp 5.000
              </div>
            </div> */}
            <div className="w-full mt-4 flex justify-end">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Diskon
              </div>
              <div className="w-1/2 text-right">
                - Rp 0
              </div>
            </div>
            <div className="w-full mt-4 flex justify-end font-medium">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Grand Total
              </div>
              <div className="w-1/2 text-right">
              {`Rp ${formatNumber(data.detail?.total_paid)}`}
              </div>
            </div>
          </div>
        </div>
        <div className="w-full pl-2 flex flex-col">
          <div className="w-full bg-white shadow-sm p-4 mt-4 rounded">
            <div>
              <span className="font-medium">Alamat Pengiriman</span>
            </div>
            <div className="mt-4">
              {data.buyerName} <br></br>
              {data.detail?.address}
              <br></br>
              <span className="font-medium">Telepon</span> : {data.detail?.phone}
            </div>
          </div>
          <div className="w-full bg-white shadow-sm p-4 mt-4 rounded">
            <div>
              <span className="font-medium">Tindakan</span>
            </div>
            <div className="mt-4">
              <div>
                <button className="bg-gray-400 rounded font-medium px-3 py-2">CETAK PESANAN</button>
                <button className="bg-gray-400 rounded font-medium px-3 py-2 ml-4">CETAK BUKTI PEMBAYARAN</button>
              </div>
            </div>
          </div>
          <div className="w-full bg-white shadow-sm p-4 mt-4 rounded">
            <div>
              <span className="font-medium">Catatan Pembelian</span>
            </div>
            <div className="mt-4">
              Tidak ada catatan
            </div>
          </div>
        </div>
      </div>
  </div>
)
const ItemMenungguKonfirmasi = ({data, callback, action, callbackDetail}) => (
  <div className="w-full">
    <div className="w-full bg-white shadow-sm p-4 mt-4 rounded flex flex-wrap">
      <div className="w-full flex text-text-primary font-medium">
        <div className="w-full px-4 border-r-2 flex">
          <input className="mr-4 mt-1" type="checkbox" />
          <div className="w-full flex flex-wrap">
            <div className="w-full">
              {data.user_name}
            </div>
            <div className="w-full">
              No Transaksi <span className="font-normal">{data.invoice_no}</span>
            </div>
            {/* <div className="w-full flex mt-2 font-normal">
              <img className="w-24" src={data.image} alt=""/>
              <div>
                {`${data.sellerName} - ${data.productName}`}
              </div>
            </div> */}
          </div>
        </div>
        <div className="w-full px-4 border-r-2">
          Pengiriman <br></br>
          <div className="font-normal">
            <table>
              <tr>
                <tr>
                  <td className="pt-1 pb-0 px-0">Penerima</td>
                  <td className="pt-1 pb-0 px-2">:</td>
                  <td className="pt-1 pb-0 px-0">{data.receiver_name}</td>
                </tr>
                <tr>
                  <td className="pt-1 pb-0 px-0">Kurir</td>
                  <td className="pt-1 pb-0 px-2">:</td>
                  <td className="pt-1 pb-0 px-0">{data.shipping}</td>
                </tr>
              </tr>
            </table>
          </div>
        </div>
        <div className="w-full px-4 flex flex-wrap flex-col">
          <div className="w-full">
            Status Transaksi
          </div>
          <div className="w-full flex mt-2">
            <div className="mx-1 px-3 py-1 rounded-full bg-gray-300">
              1
            </div>
            <div className="mx-1 px-3 py-1 rounded-full bg-gray-300">
              2
            </div>
            <div className="mx-1 px-3 py-1 rounded-full bg-gray-300">
              3
            </div>
            <div className="mx-1 px-3 py-1 rounded-full bg-gray-300">
              4
            </div>
          </div>
          <div className="w-full mt-4">
            <button className="bg-gray-300 px-3 py-1 w-auto font-medium rounded">{data.status}</button>
          </div>
          <div className="w-full mt-4 font-normal">
            {data.timeLimit}
          </div>
        </div>
        <div className="w-full px-4 items-center">
          {/* <button className="w-full border rounded py-2 font-medium">
            <input type="text" placeholder="Masukkan Nomor Resi" value={data.airway_bill ? data.airway_bill : ''} onChange={callback(data.id)} disabled/>
          </button> */}
          {/* <button className="w-full border rounded py-2 font-medium mt-2" onClick={() => action(data, STATUS.DIKIRIM)}>
            SIMPAN RESI
          </button> */}
          <button className="w-full rounded py-2 font-medium mt-2 flex justify-center items-center text-primary"onClick={() => callbackDetail(data)}>
            LIHAT DETAIL <img className="ml-2" src="/images/ic_arrow_down_red.png" alt="" />
          </button>
        </div>
      </div>
    </div>

    <div className={`w-full flex text-text-primary ${data.detail ? '' : 'hidden'}`}>
        <div className="w-full pl-4 pr-2">
          <div className="w-full bg-white shadow-sm p-4 mt-4 rounded">
            <div>
              <span className="font-medium">Detail Barang</span>
            </div>
            {
              data.detail?.order_items.map(v => (
                <div key={v.product_name} className="w-full flex mt-4">
                  <div className="w-full flex">
                    <img className="w-24 mr-4" src={v.img_url} alt=""></img>
                    {`${v.product_name}`}
                  </div>
                  <div className="w-1/2 text-right">
                    {v.quantity} Barang
                  </div>
                  <div className="w-1/2 text-right">
                    {`Rp ${formatNumber(v.subtotal)}`}
                  </div>
                </div>
              ))
            }
            <div className="mt-4 w-full h-px bg-text-primary"/>
            <div className="w-full mt-4 flex justify-end">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Total Berat Barang
              </div>
              <div className="w-1/2 text-right">
                {data.detail?.total_weight} Gram
              </div>
            </div>
            <div className="w-full mt-4 flex justify-end">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Ongkos Kirim
              </div>
              <div className="w-1/2 text-right">
                {`Rp ${formatNumber(data.detail?.total_shipping)}`}
              </div>
            </div>
            {/* <div className="w-full mt-4 flex justify-end">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Biaya Layanan
              </div>
              <div className="w-1/2 text-right">
                Rp 5.000
              </div>
            </div> */}
            <div className="w-full mt-4 flex justify-end">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Diskon
              </div>
              <div className="w-1/2 text-right">
                - Rp 0
              </div>
            </div>
            <div className="w-full mt-4 flex justify-end font-medium">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Grand Total
              </div>
              <div className="w-1/2 text-right">
              {`Rp ${formatNumber(data.detail?.total_paid)}`}
              </div>
            </div>
          </div>
        </div>
        <div className="w-full pl-2 flex flex-col">
          <div className="w-full bg-white shadow-sm p-4 mt-4 rounded">
            <div>
              <span className="font-medium">Alamat Pengiriman</span>
            </div>
            <div className="mt-4">
              {data.buyerName} <br></br>
              {data.detail?.address}
              <br></br>
              <span className="font-medium">Telepon</span> : {data.detail?.phone}
            </div>
          </div>
          <div className="w-full bg-white shadow-sm p-4 mt-4 rounded">
            <div>
              <span className="font-medium">Tindakan</span>
            </div>
            <div className="mt-4">
              <div>
                <button className="bg-gray-400 rounded font-medium px-3 py-2">CETAK PESANAN</button>
                <button className="bg-gray-400 rounded font-medium px-3 py-2 ml-4">CETAK BUKTI PEMBAYARAN</button>
              </div>
            </div>
          </div>
          <div className="w-full bg-white shadow-sm p-4 mt-4 rounded">
            <div>
              <span className="font-medium">Catatan Pembelian</span>
            </div>
            <div className="mt-4">
              Tidak ada catatan
            </div>
          </div>
        </div>
      </div>
  </div>
)

const dashboard = ({authentication, token, paramSellers, trx}) => {
  const {apiUrl} = authentication.endpoint
  const [sliders, setSliders] = useState(sliderMenu)
  const [selectedContent, setSelectedContent] = useState(0)
  const [tableDatas, setTableDatas] = useState([])
  const [filteredDatas, setFilteredDatas] = useState([])
  const [sellers, setSellers] = useState(paramSellers)
  const [selectedSeller, setSelectedSeller] = useState('')
  const [keyword, setKeyword] = useState(trx)

  useEffect(() => {
    // if(selectedSeller != '') {
      doFetchOrders()
    // }
  }, [sliders])

  useEffect(() => {
    setFilteredDatas(tableDatas)
  }, [tableDatas]);

  useEffect(() => {
    if(selectedSeller != '') {
      doFetchOrders()
    }
  }, [selectedSeller])

  const doFetchOrders = async () => {
    const res = await getOrdersAdmin(apiUrl, token, selectedSeller, sliders.find(v => v.isActive).status)
    setTableDatas(res.data ? res.data : [])
  }

  const doUpdateStatus = async (data, status) => {
    const res = await updateStatusOrderAdmin(apiUrl, token, {
      airway_bill: data.airway_bill ? data.airway_bill : '',
      id: data.id,
      status: status
    })
    doFetchOrders()
  }

  const handleChangeSlider = (data) => {
    setSelectedContent(data.content)
    setSliders(sliders.map(v => ({...v, isActive: v==data })))
  }

  const handleChangeResi = (id) => (e) => {
    setTableDatas(tableDatas.map((v) => ({
      ...v, airway_bill: id === v.id ? e.target.value : v.airway_bill
    })))
  }

  const handleDetail = async(data) => {
    setTableDatas(tableDatas.map(v => ({...v, detail: null })))
    const res = await getOrderDetailManagement(apiUrl, token, data.id)
    setTableDatas(tableDatas.map(v => ({...v, detail: data.id == v.id ? res.data : null })))
  }

  const Item = ({data}) => (
    <div className="w-full">
      <div className="w-full bg-white shadow-sm p-4 mt-4 rounded flex flex-wrap">
        <div className="w-full flex text-text-primary font-medium">
          <div className="w-full px-4 border-r-2 flex">
            <input className="mr-4 mt-1" type="checkbox" />
            <div className="w-full flex flex-wrap">
              <div className="w-full">
                {data.user_name}
              </div>
              <div className="w-full">
                No Transaksi <span className="font-normal">{data.invoice_no}</span>
              </div>
              <div className="w-full flex mt-2 font-normal">
                {/* <img className="w-24" src={data.image} alt=""/>
                <div>
                  {`${data.sellerName} - ${data.productName}`}
                </div> */}
              </div>
            </div>
          </div>
          <div className="w-full px-4 border-r-2">
            Pengiriman <br></br>
            <div className="font-normal">
              <table>
                <tr>
                  <tr>
                    <td className="pt-1 pb-0 px-0">Penerima</td>
                    <td className="pt-1 pb-0 px-2">:</td>
                    <td className="pt-1 pb-0 px-0">{data.receiver_name}</td>
                  </tr>
                  <tr>
                    <td className="pt-1 pb-0 px-0">Kurir</td>
                    <td className="pt-1 pb-0 px-2">:</td>
                    <td className="pt-1 pb-0 px-0">{data.shipping}</td>
                  </tr>
                </tr>
              </table>
            </div>
          </div>
          <div className="w-full px-4 flex flex-wrap flex-col">
            <div className="w-full">
              Status Transaksi
            </div>
            <div className="w-full flex mt-2">
              <div className="mx-1 px-2 py-1 rounded-full bg-primary text-white flex items-center">
                <img src="/images/ic_check_white.png" alt="" />
              </div>
              <div className="mx-1 px-3 py-1 rounded-full bg-gray-300">
                2
              </div>
              <div className="mx-1 px-3 py-1 rounded-full bg-gray-300">
                3
              </div>
              <div className="mx-1 px-3 py-1 rounded-full bg-gray-300">
                4
              </div>
            </div>
            <div className="w-full mt-4">
              <button className="bg-gray-300 px-3 py-1 w-auto font-medium rounded">{data.status}</button>
            </div>
            <div className="w-full mt-4 font-normal">
              {data.timeLimit}
            </div>
          </div>
          <div className="w-full px-4 items-center">
            <button className="w-full rounded py-2 font-medium mt-2 flex justify-center items-center text-primary" onClick={() => handleDetail(data)}>
              LIHAT DETAIL <img className="ml-2" src="/images/ic_arrow_down_red.png" alt=""/>
            </button>
          </div>
        </div>
      </div>
      
      <div className={`w-full flex text-text-primary ${data.detail ? '' : 'hidden'}`}>
        <div className="w-full pl-4 pr-2">
          <div className="w-full bg-white shadow-sm p-4 mt-4 rounded">
            <div>
              <span className="font-medium">Detail Barang</span>
            </div>
            {
              data.detail?.order_items.map(v => (
                <div key={v.product_name} className="w-full flex mt-4">
                  <div className="w-full flex">
                    <img className="w-24 mr-4" src={v.img_url} alt=""></img>
                    {`${v.product_name}`}
                  </div>
                  <div className="w-1/2 text-right">
                    {v.quantity} Barang
                  </div>
                  <div className="w-1/2 text-right">
                    {`Rp ${formatNumber(v.subtotal)}`}
                  </div>
                </div>
              ))
            }
            <div className="mt-4 w-full h-px bg-text-primary"/>
            <div className="w-full mt-4 flex justify-end">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Total Berat Barang
              </div>
              <div className="w-1/2 text-right">
                {data.detail?.total_weight} Gram
              </div>
            </div>
            <div className="w-full mt-4 flex justify-end">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Ongkos Kirim
              </div>
              <div className="w-1/2 text-right">
                {`Rp ${formatNumber(data.detail?.total_shipping)}`}
              </div>
            </div>
            {/* <div className="w-full mt-4 flex justify-end">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Biaya Layanan
              </div>
              <div className="w-1/2 text-right">
                Rp 5.000
              </div>
            </div> */}
            <div className="w-full mt-4 flex justify-end">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Diskon
              </div>
              <div className="w-1/2 text-right">
                - Rp 0
              </div>
            </div>
            <div className="w-full mt-4 flex justify-end font-medium">
              <div className="w-full">
              </div>
              <div className="w-1/2 text-right">
                Grand Total
              </div>
              <div className="w-1/2 text-right">
              {`Rp ${formatNumber(data.detail?.total_price + data.detail?.total_shipping)}`}
              </div>
            </div>
          </div>
        </div>
        <div className="w-full pl-2 flex flex-col">
          <div className="w-full bg-white shadow-sm p-4 mt-4 rounded">
            <div>
              <span className="font-medium">Alamat Pengiriman</span>
            </div>
            <div className="mt-4">
              {data.buyerName} <br></br>
              {data.detail?.address}
              <br></br>
              <span className="font-medium">Telepon</span> : {data.detail?.phone}
            </div>
          </div>
          <div className="w-full bg-white shadow-sm p-4 mt-4 rounded">
            <div>
              <span className="font-medium">Tindakan</span>
            </div>
            <div className="mt-4">
              <div>
                <button className="bg-gray-400 rounded font-medium px-3 py-2">CETAK PESANAN</button>
                <button className="bg-gray-400 rounded font-medium px-3 py-2 ml-4">CETAK BUKTI PEMBAYARAN</button>
              </div>
            </div>
          </div>
          <div className="w-full bg-white shadow-sm p-4 mt-4 rounded">
            <div>
              <span className="font-medium">Catatan Pembelian</span>
            </div>
            <div className="mt-4">
              Tidak ada catatan
            </div>
          </div>
        </div>
      </div>
    
    </div>
  )

  const renderContent = (content) => {
    switch (content) {
      case 0:
        return contentMenungguKonfirmasi()
      case 1:
        return contentDibayar()
      case 2:
        return contentDiproses()
      case 3:
        return contentDikirim()
      case 4:
        return contentSelesai()
      case 5:
        return contentDikembalikan()
      default:
        return contentSelesai()
    }
  }

  const contentDibayar = () => (
    tableDatas.map((v, i) => (<Item data={v} />))
  )

  const contentDiproses = () => (
    tableDatas.map((v, i) => (<ItemProses key={v.id} data={v} callback={(id) => handleChangeResi(id)} action={(data, status) => doUpdateStatus(data, status)} callbackDetail={(data) => handleDetail(data)}/>))
  )
  
  const contentDikirim = () => (
    tableDatas.map((v, i) => (<ItemDikirim key={v.id} data={v} callback={(id) => handleChangeResi(id)} action={(data, status) => doUpdateStatus(data, status)} callbackDetail={(data) => handleDetail(data)}/>))
  )
  
  const contentMenungguKonfirmasi = () => (
    tableDatas.map((v, i) => (<ItemMenungguKonfirmasi key={v.id} data={v} callback={(id) => handleChangeResi(id)} action={(data, status) => doUpdateStatus(data, status)} callbackDetail={(data) => handleDetail(data)}/>))
  )

  const contentSelesai = () => (
    tableDatas.map((v, i) => (<ItemSelesai key={v.id} data={v} callback={(id) => handleChangeResi(id)} action={(data, status) => doUpdateStatus(data, status)} callbackDetail={(data) => handleDetail(data)}/>))
  )
  
  const contentDikembalikan = () => (
    tableDatas.map((v, i) => (<ItemSelesai key={v.id} data={v} callback={(id) => handleChangeResi(id)} action={(data, status) => doUpdateStatus(data, status)} callbackDetail={(data) => handleDetail(data)}/>))
  )

  return (
    <BackOfficeBase>
      <div className="w-full flex flex-wrap">

        <div className="w-full">
          <ContentHeader title="Orders" breadcumb={breadcumb} />
        </div>

        {/* <div className="mt-3 text-text-primary flex items-center">
          <img className="mr-1" src="/images/ic_download.png" alt=""/> Download File Transaksi
        </div> */}

        
        <div className="w-full flex items-center mt-4 text-text-primary">
          <div className="border border-gray-400 rounded-l flex items-center h-full w-64">
            <select className="py-2 px-5" value={selectedSeller} onChange={(e) => setSelectedSeller(e.target.value)}>
              <option value="">Semua Seller</option>
              {
                sellers.map(v => (
                  <option key={v.seller.id} value={v.seller.id}>{v.seller.store_name}</option>
                ))
              }
            </select>
          </div>
          <div className="rounded-r px-3 py-1 bg-gray-500 h-full items-center flex">
            <img src="/images/ic_arrow_down.png" alt=""/>
          </div>
        </div>

        <div className="w-full">
          <motion.div
            animate={{ y: 10 }}
            transition={{ ease: "easeOut", duration: 1 }}
          >
            <div className="w-full bg-white shadow-sm p-4 mt-4 rounded flex flex-wrap">
              <div className="w-full flex">
                {
                  sliders.map(v => <SliderItem key={v.name} data={v} cbClick={handleChangeSlider}/>)
                }
                <div className="w-full border-b-2 border-gray-500"/>
              </div>
              <div className="w-full mt-4 flex">
                <div className="border border-gray-400 rounded flex items-center">
                  <img className="mx-3 my-1" src="/images/ic_search.png" alt="" />
                  <input className="border-0 shadow-none" type="text" placeholder="Cari" value={keyword}></input>
                </div>
                {/* <div className="ml-1 flex items-center h-full">
                  <div className="border border-gray-400 rounded-l flex items-center h-full">
                    <select className="py-1 px-5">
                      <option>Urutkan</option>
                    </select>
                  </div>
                  <div className="rounded-r px-3 py-1 bg-gray-500 h-full items-center flex">
                    <img src="/images/ic_arrow_down.png" alt=""/>
                  </div>
                </div> */}
              </div>
              <div className="h-px bg-gray-500 w-full mt-4"></div>
              <div className="w-full mt-4 flex text-text-primary font-medium">
                <div className="w-full px-4">
                  <input className="mr-4" type="checkbox" />
                  Pilih Semua Transaksi
                </div>
                <div className="w-full px-4">
                  Pengiriman
                </div>
                <div className="w-full px-4">
                  Status
                </div>
                <div className="w-full px-4">
                  Transaksi
                </div>
              </div>
            </div>
            
            {/* {renderContent(selectedContent)} */}
            {
              filteredDatas.map(v => (
                <OrderItem
                  key={v.id} 
                  data={v} 
                  callback={(id) => handleChangeResi(id)} 
                  action={(data, status) => doUpdateStatus(data, status)} 
                  callbackDetail={(data) => handleDetail(data)}
                  callbackReload={() => doFetchOrders()}
                  isAdmin={true}
                />
              ))
            }
          </motion.div>
        </div>
      </div>
    </BackOfficeBase>
  )
}

dashboard.getInitialProps = async (ctx) => {
  const {endpoint} = ctx.store.getState().authentication
  const {token} = ctx.store.getState().lakkonState
  const sellers = await getSellers(token, 'store_name,ASC', '1', `100`, 'Approved')
  const trx = typeof ctx.query.trx != 'undefined' ? ctx.query.trx : ''

  return {
    token: token,
    paramSellers: sellers.data ? sellers.data : [],
    trx
  }
}

export default connect(
  (state) => state, {}
)(dashboard);