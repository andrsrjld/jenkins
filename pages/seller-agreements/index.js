import React from 'react';
import BaseLayout from '~/components/BaseLayout';
import { getProductType } from '~/data/api/product-type';
import { getCart } from '~/data/api/cart';

const index = ({productType, token, cart}) => (
  <div>
    <BaseLayout productType={productType} token={token} cart={cart}>
      <div id="breadcumb" style={{ padding: '20px 70px' }}>
        <div className="breadcumb">
            Home &#x0226B;
          {' '}
          <span style={{ color: '#010101' }}>Seller Agreements</span>
        </div>
      </div>
      <div style={{ padding: '20px 70px 80px 70px' }}>
        <div className="title">
          Seller
          {' '}
          <span style={{ color: '#010101' }}>Agreements</span>
        </div>
        <div className="c" style={{ marginTop: '48px' }}>
          <b>Hal-hal penting yang perlu dipahami oleh Penjual:</b>
          <br />
          1. Penjual memahami dan menyetujui bahwa untuk pertama kali, apabila ingin menjual biji kopi sangray (roasted beans) wajib mengirimkan sample kopi ke pihak Lakkon terlebih dahulu sebanyak 50gram untuk 1 jenis nya. Ke alamat:
          <br />
          <div style={{ marginLeft: '48px' }}>
            <b>
            Lakkon / Bersahaja (+62 822.9998.5858)
              <br />
            Jalan Pinang Emas V Nomor 7, Pondok Pinang – Kebayoran lama 
              <br />
            Jakarta Selatan 12310
              <br />
            </b>
            <i>(mohon melakukan konfirmasi ke nomor hp kami setelah melakukan pengiriman)</i>
            <br />
          </div>
          2. Penjual memahami dan menyetujui untuk berjualan produk makanan dan minuman dianjurkan untuk memiliki izin PIRT.
          <br />
          3. Penjual memahami dan menyetujui untuk memberikan komisi kepada Lakkon sebesar 10% dari harga jual barang antara 70.000 rupiah – 1.000.000 rupiah, dan sebesar 5% dari harga jual barang antara 1.001.000 rupiah – seterusnya, untuk 1 barang.(tidak berlaku akumulasi atau kelipatannya).
          <br />
          4. Penjual memahami dan menyetujui harga barang yang akan dijual di website lakkon harus dengan minimum harga 70.000 rupiah per barang.
          <br />
          5. Penjual memahami dan menyetujui bahwa Penjual wajib memberikan balasan untuk menerima (dengan melakukan konfirmasi pesanan dan memasukan nomor resi di lakkon) atau menolak pesanan barang pihak Pembeli dalam batas waktu 2x24 Jam terhitung sejak adanya notifikasi pesanan barang dari lakkon. Jika dalam batas waktu tersebut tidak ada balasan dari Penjual maka secara otomatis pesanan akan dibatalkan.
          <br />
          6. Apabila terjadi penolakan pesanan dari pihak penjual, maka penjual setuju untuk mengembalikan dana ke Pembeli dengan perhitungan sebagai berikut:
          <br />
          <div style={{ marginLeft: '48px' }}>
            - Harga barang
            <br />
          - Biaya ongkos kirim pembeli
            <br />
          - Biaya penanganan/convenient fee sebesar Rp 5.000,- / Invoice.
            <br />
          </div>

          7. Penjual memahami dan menyetujui bahwa, biaya – biaya seperti yang sudah dijelaskan di nomor 5 akan dibebankan ke Penjual dan dipotong dari rekap penjualan berikutnya.
          <br />
          8. Apabila setelah terjadi penolakan pesanan kepada pembeli dan Penjual belum memiliki transaksi apapun di laman lakkon, maka kami akan mengirimkan invoice terkait dengan Biaya penanganan untuk dibayarkan ke pihak Lakkon melalui bank transfer ke rekening resmi lakkon.
          <br />
        </div>
      </div>
    </BaseLayout>
    <style jsx>
      {
        `
        .title {
          font-weight: bold;
          font-size: 24px;
          line-height: 23px;
          letter-spacing: 0.05em;
          color: #696969;
        }

        .c {
          font-weight: 300;
          font-size: 22px;
          line-height: 36px;
          letter-spacing: 0.05em;
          color: #696969;
        }
        `
      }
    </style>
  </div>
);

index.getInitialProps = async (ctx) => {
  const { token } = ctx.store.getState().authentication;

  const productTypeResp = await getProductType();
  const productType = productTypeResp.data;

  const cartResp = await getCart(token);
  const cart = cartResp.data ? cartResp.data.cart : [];

  return { 
    productType,
    token,
    cart
  };
};

export default index;
