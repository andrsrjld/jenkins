import React, { useState } from 'react';
import BaseLayout from 'components/BaseLayout';
import { getProductType } from 'data/api/product-type';
import fetch from 'node-fetch';

import { getCart } from 'data/api/cart';

// Toastr Notification
import {successNotification, errorNotification} from 'util/toastr-notif';

const ContactUs = ({productType, token, cart}) => {
  const [form, setForm] = useState({
    name: '',
    email: '',
    message: ''
  });

  const handleChangeForm = (name) => (e) => {
    const { value } = e.target;
    setForm({ ...form, [name]: value });
  }

  const submitForm = async (e) => {
    e.preventDefault();
    const prom = await fetch(`/api/contact`, {
      method: 'POST',
      body : JSON.stringify(form),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      }
    })
    console.log(prom);
    if(prom.code === 200){
      successNotification(prom.message);
    } else {
      errorNotification(prom.message);
    }
  }

  return (
    <div>
      <BaseLayout productType={productType} token={token} cart={cart}>
        <div id="breadcumb" className="contact-wrapper">
          <div className="breadcumb">
              Home &#x0226B;
            {' '}
            <span style={{ color: '#010101' }}>Cara Pembayaran</span>
          </div>

          <div className="c flex flex-wrap">
            <div className="xs:w-full sm:w-full">
              <div className="c-information">
                <div className="c-information-title">
                  Cara
                  {' '}
                  <span style={{ color: '#010101' }}>Pembayaran</span>
                </div>
                <div className="text-information">
                  <div>
                    <span className="font-bold">METODE PEMBAYARAN - BNI Virtual Account</span>
                    <ul className="mt-2 list-disc list-inside">
                      <li>Cara Pembarayaran via Mobile Banking BNI / Internet Banking BNI</li>
                      <ol className="list-decimal ml-16">
                       <li>Ketik alamat https://ibank.bni.co.id pada browser, kemudian klik Enter</li>
                       <li>Masukkan User ID dan Password</li>
                       <li>Pilih menu Transfer</li>
                       <li>Pilih Virtual Account Billing</li>
                       <li>Masukan Nomor Virtual Account (contoh: 123456789012XXXX), lalu pilih rekening debet yang akan digunakan</li>
                       <li>Klik lanjut</li>
                       <li>Tagihan yang harus dibayarkan akan muncul pada layar konfirmasi</li>
                       <li>Masukkan Kode Otentikasi Token</li>
                       <li>Pembayaran berhasil</li>
                      </ol>
                    </ul>
                    <ul className="mt-2 list-disc list-inside">
                      <li>Cara Pembayaran via ATM </li>
                      <ol className="list-decimal ml-16">
                       <li>Masukkan Kartu</li>
                       <li>Pilih Bahasa</li>
                       <li>Masukkan PIN ATM</li>
                       <li>Pilih menu Lainnya</li>
                       <li>Pilih Transfer</li>
                       <li>Pilih Jenis rekening yang akan kamu gunakan (contoh: “Dari Rekening Tabungan”)</li>
                       <li>Pilih Virtual Account Billing</li>
                       <li>Masukkan Nomor Virtual Account (contoh: 123456789012XXXX).</li>
                       <li>Tagihan yang harus dibayarkan akan muncul pada layar konfirmasi</li>
                       <li>Konfirmasi jika sudah sesuai, lanjutkan transaksi</li>
                       <li>Transaksi selesai.</li>
                      </ol>
                    </ul>
                  </div>

                  <div className="mt-4">
                    <span className="font-bold">METODE PEMBAYARAN - Permata Virtual Account</span>
                    <ul className="mt-2 list-disc list-inside">
                      <li>Cara Pembarayaran Mobile Banking Permata / Internet Banking Permata</li>
                      <ol className="list-decimal ml-16">
                        <li>Login Internet Banking</li>
                        <li>Pilih Pembayaran Tagihan</li>
                        <li>Pilih Virtual Account</li>
                        <li>Masukkan Nomor Virtual Account, misalnya 8625XXXXXXXXXXXX, sebagai No. Virtual Account</li>
                        <li>Masukkan Nominal, misalnya 10000</li>
                        <li>Klik Kirim</li>
                        <li>Masukkan Token</li>
                        <li>Klik Kirim</li>
                        <li>Bukti bayar akan ditampilkan</li>
                        <li>Selesai</li>
                      </ol>
                    </ul>
                    <ul className="mt-2 list-disc list-inside">
                      <li>Cara Pembayaran via ATM </li>
                      <ol className="list-decimal ml-16">
                        <li>Masukkan kartu ATM dan PIN</li>
                        <li>Pilih menu Transaksi Lainnya</li>
                        <li>Pilih Pembayaran</li>
                        <li>Pilih Pembayaran Lain-Lain</li>
                        <li>Pilih Virtual Account</li>
                        <li>Masukkan Nomor Virtual Account, misalnya 8625XXXXXXXXXXXX</li>
                        <li>Pilih Benar</li>
                        <li>Pilih Ya</li>
                        <li>Ambil bukti bayar</li>
                        <li>Selesai</li>
                      </ol>
                    </ul>
                  </div>

                  <div className="mt-4">
                    <span className="font-bold">METODE PEMBAYARAN - Mandiri Virtual Account</span>
                    <ul className="mt-2 list-disc list-inside">
                      <li>Cara Pembarayaran via Mobile Banking Mandiri / Internet Banking Mandiri</li>
                      <ol className="list-decimal ml-16">
                        <li>Login Internet Banking</li>
                        <li>Pilih Bayar</li>
                        <li>Pilih Multi Payment</li>
                        <li>Masukkan Midtrans sebagai Penyedia Jasa</li>
                        <li>Masukkan Nomor Virtual Account, misalnya 70012XXXXXXXXXXX, sebagai Kode Bayar</li>
                        <li>Ceklis IDR</li>
                        <li>Klik Lanjutkan</li>
                        <li>Bukti bayar ditampilkan</li>
                        <li>Selesai</li>
                      </ol>
                    </ul>
                    <ul className="mt-2 list-disc list-inside">
                      <li>Cara Pembayaran via ATM </li>
                      <ol className="list-decimal ml-16">
                       <li>Masukkan kartu ATM dan PIN</li>
                       <li>Pilih Lainnya</li>
                       <li>Pilih Lainnya sekali lagi</li>
                       <li>Pilih Multi Payment</li>
                       <li>Masukkan 70012 sebagai Kode Perusahaan/Institusi</li>
                       <li>Pilih Benar</li>
                       <li>Masukkan Kode Bayar Midtrans, misalnya 70012XXXXXXXXXXX</li>
                       <li>Pilih Benar</li>
                       <li>Tekan tombol angka 1</li>
                       <li>Pilih Ya</li>
                       <li>Ambil bukti bayar</li>
                       <li>Selesai</li>
                      </ol>
                    </ul>
                  </div>


                </div>
              </div>
            </div>
          </div>

        </div>
      </BaseLayout>
      <style jsx>
        {
          `
          .breadcumb {
            font-size: 14px;
            color: #696969;
          }
          .contact-wrapper{
            padding: 20px 0;
          }
          .c {
            width: 100%;
            display: flex;
            padding: 60px 0;
          }

          .c-information {
            width: 100%;
          }

          .c-information-title {
            font-weight: bold;
            font-size: 20px;
            line-height: 23px;
            letter-spacing: 0.05em;
            color: #696969;
          }

          .c-title-company {
            font-weight: 500;
            font-size: 18px;
            line-height: 26px;
            letter-spacing: 0.05em;
            color: #696969;
            margin-top: 20px;
          }

          .text-information {
            font-size: 18px;
            line-height: 26px;
            letter-spacing: 0.05em;
            color: #868686;
            margin-top: 20px;
            width: 100%;
          }

          .c-contact {
            display: flex;
            align-items: center;
            font-weight: 500;
            font-size: 16px;
            line-height: 26px;
            letter-spacing: 0.05em;
            color: #696969;
          }

          .c-contact img {
            width: 20px;
            height: 20px;
            margin-right: 26px;
          }
          
          .c-form {
            width: 100%;
          }
          @media (min-width: 320px) and (max-width: 767px) {
            .contact-wrapper{
              padding: 0 20px;
              margin-bottom: 30px;
            }
            .c-information{
              margin-bottom:50px;
            }
            .text-information{
              margin-top:20px;
            }
          }
          `
        }
      </style>
    </div>
  );
}

ContactUs.getInitialProps = async (ctx) => {
  const { token } = ctx.store.getState().authentication;

  const productTypeResp = await getProductType();
  const productType = productTypeResp.data;

  const cartResp = await getCart(token);
  const cart = cartResp.data ? cartResp.data.cart : [];

  return { 
    productType,
    token,
    cart
  };
};

export default ContactUs;
