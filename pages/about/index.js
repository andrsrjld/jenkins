import React from 'react';
import BaseLayout from '~/components/BaseLayout';
import { getProductType } from '~/data/api/product-type';

import { getCart } from '~/data/api/cart';

const data = [
  {
    image: '/images/ic_helpful.png',
    step: 'Helpful',
    title: 'Coba Isi Quiz',
    desc: 'Kami senang membantu anda untuk mendapatkan produk-produk yang dapat diandalkan'
  },
  {
    image: '/images/ic_insightful.png',
    step: 'Insightful',
    title: 'Menemukan Kecocokan',
    desc: 'Kami senang menjadi salah satu sumber informasi yang dapat menambah wawasan kita bersama'
  },
  {
    image: '/images/ic_trustworthy.png',
    step: 'Trustworthy',
    title: 'Coba Isi Quiz',
    desc: 'Kami selalu berusaha menjadi sumber informasi dan produk yang terpercaya'
  },
  {
    image: '/images/ic_supportive.png',
    step: 'Supportive',
    title: 'Coba Isi Quiz',
    desc: 'kami sangat antusias turut mendukung kemajuan industri kopi, teh dan cokelat nusantara'
  }
];

const About = ({productType, token, cart}) => (
  <div>
    <BaseLayout productType={productType} token={token} cart={cart}>
      <div id="breadcumb" className="breadcrumb-wrapper">
        <div className="breadcumb">
            Home &#x0226B;
          {' '}
          <span style={{ color: '#010101' }}>Tentang BAPERA</span>
        </div>
      </div>
      <div className="c">
        <div className="title">
          Tentang
          {' '}
          <span style={{ color: '#010101' }}>BAPERA</span>
        </div>

        <div className="desc" style={{ marginTop: '24px' }}>
          Lapak Kopi Nusantara (
          <span style={{ color: '#010101' }}>BAPERA</span>
          ) hadir dalam bentuk pasar digital untuk memenuhi seluruh kebutuhan pelaku kopi, teh dan cokelat (PEBAPERA) di Indonesia.
          Dengan semangat mendukung perdagangan langsung dan jujur dalam industri, para PEBAPERA dapat saling bertransaksi dengan aman dan nyaman.
          Tentunya di sini PEBAPERA dapat menemukan harga terbaik untuk setiap produk yang terpajang di situs BAPERA.
        </div>

        <div className="c-content flex flex-wrap">
          <div className="w-1/2 xs:w-full sm:w-full xs:mb-4 sm:mb-4">
            <div className="desc c-card">
              <div className="card-title">
                KURATOR TERPERCAYA
              </div>
              <div style={{ marginTop: '16px' }}>
                Kami memiliki tim kurasi kopi berkompeten yang dipimpin oleh Hendri Kurniawan demi memastikan setiap produk yang berada di etalase BAPERA berkualitas baik, otentik, dan dengan harga yang masuk akal. Setiap produk kopi dari hulu hingga ke hilir yang terpajang di etalase BAPERA akan melalui proses kurasi.
              </div>
            </div>
          </div>
          
          <div className="w-1/2 xs:w-full sm:w-full xs:mb-4 sm:mb-4">
            <div className="desc c-card">
              <div className="card-title">
                SUMBER INFORMASI
              </div>
              <div style={{ marginTop: '16px' }}>
                <span style={{ color: '#010101' }}>BAPERA</span>
                {' '}
                juga menghadirkan info-info seputar dunia kopi, teh dan cokelat untuk mengajak para PEBAPERA menyelami dunia kopi, teh dan cokelat lebih dalam dan memahami setiap lekuk sebelum meneguk. Melalui tulisan, video, foto, dan gambar kami menyediakan informasi menarik untuk disimak yang mungkin akan menambah pengetahuan para PEBAPERA.
              </div>
            </div>
          </div>
        </div>


        <div className="desc" style={{ marginTop: '24px' }}>
          Dengan ini kami berharap mendukung setiap pihak yang terlibat dari hulu hingga ke hilir untuk dapat merasakan keuntungan dari pertumbuhan industri kopi dan lainnya di nusantara. Semoga BAPERA selalu hadir untuk memenuhi kebutuhan ngopimu.
        </div>

        <div className="flex flex-wrap justify-center mt-12">
          <div className="w-full text-center">
            <span className="title">Core Values</span>
          </div>
          <hr className="divider" />
        </div>
        <div className="content flex flex-wrap mt-6">
          {
            data.map((v) => (
              <div className="w-1/4 xs:w-1/2 sm:w-1/2 px-2">
                <div className="container-content">
                  <img src={v.image} alt="" className="image" />
                  <div className="mt-4 text-center font-bold text-text-primary">
                    {v.step}
                  </div>
                  {/* <div className="title">
                    {v.title}
                  </div> */}
                  <div className="mt-6 text-center text-text-primary">
                    {v.desc}
                  </div>
                </div>
              </div>
            ))
          }
        </div>

        <div className="flex flex-wrap justify-center mt-12">
          <div className="w-full text-center">
            <span className="title">Dibalik <span style={{ color: '#010101' }}>BAPERA</span></span>
          </div>
          <hr className="divider" />
        </div>
        <div className="content flex mt-6">
          <img src="/images/ic_hk.jpeg" alt="Image Hendri" />
          <div className="w-full px-8">
            <div className="text-center">
              HK is an architecture studies graduate who fell in love with coffee so deep
              that he choose coffee for a living.
              <br></br>
              <br></br>
              In 2012 he became the first person in Southeast Asia to be certified as
              a World Barista Championship Judge; and since 2016 serving as World Coffee
              Events Representative which allows him to be the head judge in world coffee
              championship as well as overseeing national competitions all around the world.
              However, his passion for architecture is still alive and helps him in designing
              a cafe or anything related.
              <br></br>
              <br></br>
              <ol className="text-left ml-4" style={{ listStyleType: 'circle' }}>
                <li>Coffee Quality Institute (CQI) Licensed Q Arabica Grader, 2011 - 2016</li>
                <li>WCE World Barista Championship Judge,</li>
                <li>WCE World Latte Art Championship Judge,</li>
                <li>WCE World Coffee in Good Spirits Judge</li>
                <li>WCE World Brewers Cup Judge</li>
                <li>WCE Representative</li>
                <li>Authorized SCA Trainer</li>
                <li>Coffee Science Certification Level 1, 2015</li>
                <li>SCA Roasting Level 1, 2017</li>
                <li>Founder of ABCD School of Coffee</li>
                <li>Founder of Lapak Kopi Nusantara (BAPERA)</li>
                <li>Initiator/Conceptor/Organizer of Jakarta Coffee Week</li>
                <li>Board member of Sustainable Coffee Platform in Indonesia (SCOPI)</li>
              </ol>
            </div>
          </div>
        </div>

      </div>
    </BaseLayout>
    <style jsx>
      {`
        .breadcrumb-wrapper{
          padding: 20px 0;
        }
        .image {
          height: 150px;
          margin:0 auto;
        }

        .c {
          padding: 20px 0 80px 0;
        }

        .c-content {
          display: flex;
          margin: 32px 0px;
        }

        .c-card {
          background: #FFFFFF;
          box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.1);
          border-radius: 5px;
          padding: 24px;
          margin: 0px 10px;
        }

        .card-title {
          font-weight: 500;
          font-size: 20px;
          line-height: 26px;
          text-align: center;
          letter-spacing: 0.05em;
          color: #696969;
        }

        .title {
          font-weight: bold;
          font-size: 18px;
          line-height: 23px;
          letter-spacing: 0.05em;
          color: #696969;
        }
        
        .divider {
          height: 4px;
          background-color: #010101;
          border: none;
          border-radius: 9px;
          width: 100px;
        }

        .desc {
          font-size: 16px;
          line-height: 28px;
          letter-spacing: 0.05em;
          color: #696969;
        }

        .card {
          background: #FFFFFF;
          box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.1);
          border-radius: 5px;
        }
        @media (min-width: 320px) and (max-width: 767px) {
          .c{
            padding:0 20px;
            margin-bottom:30px;
          }
          .breadcrumb-wrapper{
            padding: 0 20px;
            margin-bottom:20px;
            text-align:center;
          }
          .title{
            text-align:center;
          }
          .content{
            flex-wrap: wrap;
          }
          .content img{
            width: 100%;
          }
        }
        `
      }
    </style>
  </div>
);

About.getInitialProps = async (ctx) => {
  const { token } = ctx.store.getState().authentication;

  const productTypeResp = await getProductType();
  const productType = productTypeResp.data;

  const cartResp = await getCart(token);
  const cart = cartResp.data ? cartResp.data.cart : [];

  return { 
    productType,
    token,
    cart
  };
};

export default About;
