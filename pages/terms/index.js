import React, { useEffect } from 'react';
import BaseLayout from '~/components/BaseLayout';
import { getProductType } from '~/data/api/product-type';
import { getCart } from '~/data/api/cart';

const About = ({ productType, token, cart }) => {
  useEffect(() => {
    const acc = document.getElementsByClassName('c-accordion');
    let i;

    for (i = 0; i < acc.length; i++) {
      acc[i].addEventListener('click', function () {
        this.classList.toggle('active');
        const panel = this.nextElementSibling;
        if (panel.style.display === 'flex') {
          panel.style.display = 'none';
        } else {
          panel.style.display = 'flex';
        }
      });
    }
  }, []);

  return (
    <div>
      <BaseLayout productType={productType} token={token} cart={cart}>
        <div id="breadcumb" className="breadcrumb-wrapper">
          <div className="breadcumb">
            Home &#x0226B;
            {' '}
            <span style={{ color: '#010101' }}>Terms Condition</span>
          </div>
        </div>
        <div className="c">
          <div className="title">
          Terms
            {' '}
            <span style={{ color: '#010101' }}>Condition</span>
          </div>

          <div className="desc" style={{ marginTop: '24px' }}>
          Syarat dan ketentuan yang ditetapkan di bawah ini mengatur pemakaian jasa yang ditawarkan oleh BAPERA terkait penggunaan situs www.bapera.id. Pengguna disarankan membaca dengan seksama karena dapat berdampak kepada hak dan kewajiban Pengguna di bawah hukum.
          </div>

          <div className="desc" style={{ marginTop: '24px' }}>
          Dengan mendaftar dan/atau menggunakan situs www.bapera.id, maka pengguna dianggap telah membaca, mengerti, memahami dan menyetujui semua isi dalam syarat dan ketentuan yang berlaku. Syarat dan ketentuan ini merupakan bentuk kesepakatan yang dituangkan dalam sebuah perjanjian yang sah antara penjual dengan BAPERA. Jika penjual tidak menyetujui salah satu, sebagian, atau seluruh isi syarat dan ketentuan, maka PELAKKON tidak diperkenankan menggunakan seluruh layanan yang terdapat pada aplikasi maupun situs www.bapera.id
          </div>

          <div className="c-content" style={{ marginTop: '24px' }}>
            <div className="c-accordion" tabIndex="0" role="button">
              <div className="w-100 c-accordion-header">
                <div>Definisi</div>
                <div>
                  <img src="/images/ic_arrow_down.png" alt="" />
                </div>
              </div>
              <div className="accordion-divider" />
            </div>
            <div className="c-accordion-desc">
              <span>
                1. Bapera adalah suatu perseroan terbatas yang menjalankan kegiatan usaha jasa dan perdagangan secara daring melalui situs www.lakkon.id, yakni situs pencarian toko, produk kopi dan produk penunjang lainnya yang dijual oleh penjual terdaftar. Selanjutnya disebut lakkon.
                <br />
                2. Situs BAPERA adalah www.bapera.id.
                <br />
                3. Syarat dan ketentuan adalah perjanjian antara penjual dan BAPERA yang berisikan seperangkat peraturan yang mengatur hak, kewajiban, tanggung jawab penjual dan BAPERA, serta tata cara penggunaan sistem layanan BAPERA.
                <br />
                4. Penjual adalah pihak yang menggunakan layanan bapera, termasuk namun tidak terbatas pada pembeli, penjual ataupun pihak lain yang sekedar berkunjung ke situs BAPERA.
                <br />
                5. Pembeli adalah Penjual terdaftar yang melakukan permintaan atas barang yang dijual oleh penjual di situs BAPERA.
                <br />
                6. Penjual adalah terdaftar yang melakukan tindakan buka toko dan/atau melakukan penawaran atas suatu barang kepada para pembeli di situs BAPERA.
                <br />
                7. Barang adalah benda yang berwujud / memiliki fisik barang yang dapat diantar / memenuhi kriteria pengiriman oleh perusahaan jasa pengiriman barang.
                <br />
                 {/* 8. Rekening Resmi BAPERA adalah rekening bersama yang disepakati oleh BAPERA dan para pengguna untuk proses transaksi jual beli di situs BAPERA.
                <br />
                 9. Rekening resmi BAPERA adalah
                <br />
                 10. Bank Mandiri
                <br />
                 11. Nomor rekening: XXXXXX
                <br />
                 12. Atas nama: BAPERA
                <br /> */}
                 {/* 13. Cabang: Bank Mandiri KCP Tanjung Duren
                <br /> */}
              </span>
              <div className="accordion-divider" />
            </div>

            <div className="c-accordion" tabIndex="0" role="button">
              <div className="w-100 c-accordion-header">
                <div>Akun, Password dan Keamanan</div>
                <div>
                  <img src="/images/ic_arrow_down.png" alt="" />
                </div>
              </div>
              <div className="accordion-divider" />
            </div>
            <div className="c-accordion-desc">
              <span>
                Content
              </span>
              <div className="accordion-divider" />
            </div>

            <div className="c-accordion" tabIndex="0" role="button">
              <div className="w-100 c-accordion-header">
                <div>Transaksi Pembelian</div>
                <div>
                  <img src="/images/ic_arrow_down.png" alt="" />
                </div>
              </div>
              <div className="accordion-divider" />
            </div>
            <div className="c-accordion-desc">
              <span>
                Content
              </span>
              <div className="accordion-divider" />
            </div>

            <div className="c-accordion" tabIndex="0" role="button">
              <div className="w-100 c-accordion-header">
                <div>Transaksi Penjualan</div>
                <div>
                  <img src="/images/ic_arrow_down.png" alt="" />
                </div>
              </div>
              <div className="accordion-divider" />
            </div>
            <div className="c-accordion-desc">
              <span>
                Content
              </span>
              <div className="accordion-divider" />
            </div>

            <div className="c-accordion" tabIndex="0" role="button">
              <div className="w-100 c-accordion-header">
                <div>Penataan Etalase</div>
                <div>
                  <img src="/images/ic_arrow_down.png" alt="" />
                </div>
              </div>
              <div className="accordion-divider" />
            </div>
            <div className="c-accordion-desc">
              <span>
                Content
              </span>
              <div className="accordion-divider" />
            </div>

            <div className="c-accordion" tabIndex="0" role="button">
              <div className="w-100 c-accordion-header">
                <div>Harga</div>
                <div>
                  <img src="/images/ic_arrow_down.png" alt="" />
                </div>
              </div>
              <div className="accordion-divider" />
            </div>
            <div className="c-accordion-desc">
              <span>
                Content
              </span>
              <div className="accordion-divider" />
            </div>

            <div className="c-accordion" tabIndex="0" role="button">
              <div className="w-100 c-accordion-header">
                <div>Tarif Pengiriman</div>
                <div>
                  <img src="/images/ic_arrow_down.png" alt="" />
                </div>
              </div>
              <div className="accordion-divider" />
            </div>
            <div className="c-accordion-desc">
              <span>
                Content
              </span>
              <div className="accordion-divider" />
            </div>

            <div className="c-accordion" tabIndex="0" role="button">
              <div className="w-100 c-accordion-header">
                <div>Konten</div>
                <div>
                  <img src="/images/ic_arrow_down.png" alt="" />
                </div>
              </div>
              <div className="accordion-divider" />
            </div>
            <div className="c-accordion-desc">
              <span>
                Content
              </span>
              <div className="accordion-divider" />
            </div>

            <div className="c-accordion" tabIndex="0" role="button">
              <div className="w-100 c-accordion-header">
                <div>Jenis Barang</div>
                <div>
                  <img src="/images/ic_arrow_down.png" alt="" />
                </div>
              </div>
              <div className="accordion-divider" />
            </div>
            <div className="c-accordion-desc">
              <span>
                Content
              </span>
              <div className="accordion-divider" />
            </div>

            <div className="c-accordion" tabIndex="0" role="button">
              <div className="w-100 c-accordion-header">
                <div>Promo</div>
                <div>
                  <img src="/images/ic_arrow_down.png" alt="" />
                </div>
              </div>
              <div className="accordion-divider" />
            </div>
            <div className="c-accordion-desc">
              <span>
                Content
              </span>
              <div className="accordion-divider" />
            </div>

            <div className="c-accordion" tabIndex="0" role="button">
              <div className="w-100 c-accordion-header">
                <div>Pengiriman Barang</div>
                <div>
                  <img src="/images/ic_arrow_down.png" alt="" />
                </div>
              </div>
              <div className="accordion-divider" />
            </div>
            <div className="c-accordion-desc">
              <span>
                Content
              </span>
              <div className="accordion-divider" />
            </div>

            <div className="c-accordion" tabIndex="0" role="button">
              <div className="w-100 c-accordion-header">
                <div>Penarikan Dana Untuk Penjual</div>
                <div>
                  <img src="/images/ic_arrow_down.png" alt="" />
                </div>
              </div>
              <div className="accordion-divider" />
            </div>
            <div className="c-accordion-desc">
              <span>
                Content
              </span>
              <div className="accordion-divider" />
            </div>

            <div className="c-accordion" tabIndex="0" role="button">
              <div className="w-100 c-accordion-header">
                <div>Lakkon Damai</div>
                <div>
                  <img src="/images/ic_arrow_down.png" alt="" />
                </div>
              </div>
              <div className="accordion-divider" />
            </div>
            <div className="c-accordion-desc">
              <span>
                Content
              </span>
              <div className="accordion-divider" />
            </div>

            <div className="c-accordion" tabIndex="0" role="button">
              <div className="w-100 c-accordion-header">
                <div>Ketentuan Lain</div>
                <div>
                  <img src="/images/ic_arrow_down.png" alt="" />
                </div>
              </div>
              <div className="accordion-divider" />
            </div>
            <div className="c-accordion-desc">
              <span>
                Content
              </span>
              <div className="accordion-divider" />
            </div>

            <div className="c-accordion" tabIndex="0" role="button">
              <div className="w-100 c-accordion-header">
                <div>Penolakan Jaminan dan Batasan Tanggung Jawab</div>
                <div>
                  <img src="/images/ic_arrow_down.png" alt="" />
                </div>
              </div>
              <div className="accordion-divider" />
            </div>
            <div className="c-accordion-desc">
              <span>
                Content
              </span>
              <div className="accordion-divider" />
            </div>

            <div className="c-accordion" tabIndex="0" role="button">
              <div className="w-100 c-accordion-header">
                <div>Pelepasan</div>
                <div>
                  <img src="/images/ic_arrow_down.png" alt="" />
                </div>
              </div>
              <div className="accordion-divider" />
            </div>
            <div className="c-accordion-desc">
              <span>
                Content
              </span>
              <div className="accordion-divider" />
            </div>

            <div className="c-accordion" tabIndex="0" role="button">
              <div className="w-100 c-accordion-header">
                <div>Ganti Rugi</div>
                <div>
                  <img src="/images/ic_arrow_down.png" alt="" />
                </div>
              </div>
              <div className="accordion-divider" />
            </div>
            <div className="c-accordion-desc">
              <span>
                Content
              </span>
              <div className="accordion-divider" />
            </div>

            <div className="c-accordion" tabIndex="0" role="button">
              <div className="w-100 c-accordion-header">
                <div>Pilihan Hukum</div>
                <div>
                  <img src="/images/ic_arrow_down.png" alt="" />
                </div>
              </div>
              <div className="accordion-divider" />
            </div>
            <div className="c-accordion-desc">
              <span>
                Content
              </span>
              <div className="accordion-divider" />
            </div>

            <div className="c-accordion" tabIndex="0" role="button">
              <div className="w-100 c-accordion-header">
                <div>Pembaharuan</div>
                <div>
                  <img src="/images/ic_arrow_down.png" alt="" />
                </div>
              </div>
              <div className="accordion-divider" />
            </div>
            <div className="c-accordion-desc">
              <span>
                Content
              </span>
              <div className="accordion-divider" />
            </div>
          </div>
        </div>
      </BaseLayout>
      <style jsx>
        {`
        .breadcrumb-wrapper{
          padding:20px 0;
        }
        .c {
          padding: 20px 0 80px 0;
        }

        .c-content {
          background: #FFFFFF;
          box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.1);
          border-radius: 5px;
          font-weight: 500;
          font-size: 18px;
          line-height: 26px;
          letter-spacing: 0.05em;
          color: #696969;
        }

        .c-accordion {
          display: flex;
          justify-content: space-between;
          align-items: center;
          cursor: pointer;
          flex-wrap: wrap;
        }

        .c-accordion-header {
          display: flex;
          justify-content: space-between;
          margin: 25px 48px;
          align-items: center;
        }

        .c-accordion-desc {
          display: none;
          font-weight: normal;
          font-size: 16px;
          line-height: 32px;
          letter-spacing: 0.05em;
          color: #696969;
          width: 100%;
          flex-wrap: wrap;
        }

        .c-accordion-desc span {
          margin: 25px 48px;
        }

        .accordion-divider {
          width: 100%;
          height: 2px;
          background: #E7E7E7;
        }

        .title {
          font-weight: bold;
          font-size: 20px;
          line-height: 23px;
          letter-spacing: 0.05em;
          color: #696969;
        }

        .desc {
          font-size: 16px;
          line-height: 28px;
          letter-spacing: 0.05em;
          color: #696969;
        }

        .card {
          background: #FFFFFF;
          box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.1);
          border-radius: 5px;
        }
        @media (min-width: 320px) and (max-width: 767px) {
          .c{
            padding: 0 20px;
            margin-bottom: 30px;
          }
          .breadcrumb-wrapper{
            padding: 0 20px;
            margin-bottom:20px;
            text-align:center;
          }
          .title{
            text-align:center;
          }
        }
        `
        }
      </style>
    </div>
  );
};

About.getInitialProps = async (ctx) => {
  const { token } = ctx.store.getState().authentication;

  const productTypeResp = await getProductType();
  const productType = productTypeResp.data;

  const cartResp = await getCart(token);
  const cart = cartResp.data ? cartResp.data.cart : [];

  return { 
    productType,
    token,
    cart
  };
};

export default About;
