import { useEffect, useState } from 'react'
import axiosSetup from '../services/api'
import CircleType from 'circletype'
import Head from 'next/head'

const secondData = ['ACEH', 'GAYO', 'SUMATERA', 'JAVA', 'BALI', 'SULAWESI', 'NTT', 'NTB', 'PAPUA', 'AFRICA', 'CENTRAL AMERICA', 'SOUTH AMERICA', 'NORTH AMERICA/HAWAII', 'ASIA', 'OTHERS']
const thirdData = ['MANUAL', 'ELECTRIC']
const fourthData = ['V60', 'WAVE', 'FRENCH PRESS', 'AEROPRESS', 'CLEVER DRIPPER', 'ORIGAMI', 'MOKAPOT', 'TUBRUK']
const fifthData = ['FRUITY', 'FLORAL', 'NUTTY', 'COCOA', 'SWEET', 'SPECIES']

const Wheels = ({ filters }) => {
  const [beginner, setBeginner] = useState(false)
  const [advance, setAdvance] = useState(false)

  const [second, setSecond] = useState(
    filters.find(att => att.slug === "origin-3").values.map(v => ({label: v.value, isChecked: false}))
  )
  const [third, setThird] = useState(
    filters.find(att => att.slug === "bean-species-4").values.map(v => ({label: v.value, isChecked: false}))
  )

  const [fourth1, setFourth1] = useState(
    filters.find(att => att.slug === "processing-5").values.map(v => ({label: v.value, isChecked: false}))
  )
  const [fourth2, setFourth2] = useState(fourthData.map(v => ({label: v, isChecked: false})))

  const [fifth, setFifth] = useState(
    filters.find(att => att.slug === "tasted-6").values.map(v => ({label: v.value, isChecked: false}))
  )

  useEffect(() => {
    if(advance) {
      setBeginner(false)
      setSecond(v => v.map(k => ({...k, isChecked: false})))
      setThird(v => v.map(k => ({...k, isChecked: false})))
      setFourth1(v => v.map(k => ({...k, isChecked: false})))
      setFourth2(v => v.map(k => ({...k, isChecked: false})))
      setFifth(v => v.map(k => ({...k, isChecked: false})))
    }
  }, [advance]);

  useEffect(() => {
    if(beginner) setAdvance(false)
  }, [beginner]);

  useEffect(() => {
    setFourth1(v => v.map(k => ({...k, isChecked: false})))
    setFourth2(v => v.map(k => ({...k, isChecked: false})))
    setFifth(v => v.map(k => ({...k, isChecked: false})))
  }, [third]);

  const handleSecondClick = (data) => {
    setSecond(second.map(v => ({...v, isChecked: data === v})))
  }

  const handleThirdClick = (data) => {
    setThird(third.map(v => ({...v, isChecked: data === v})))
  }

  const handleFifthClick = (data) => {
    setFifth(fifth.map(v => ({...v, isChecked: data === v})))

    const secondData = second.find(v => v.isChecked).label;
    const thirdData = third.find(v => v.isChecked).label;
    const fourthData = fourth1.find(v => v.isChecked).label;
    const fifthData = data.label;
    window.location.href = `/category/roasted-bean-2/?origin=${secondData}&species=${thirdData}&processing=${fourthData}&tasted=${fifthData}`
  }

  return (
    <div className="min-w-screen w-auto flex justify-center min-h-screen relative">
      <Head>
        <title>Lakkon</title>
        <link rel="icon" href="/favicon.ico" />
        <meta name="viewport" content="width=1024"></meta>
      </Head>
      {/* beginner */}
      <div className="w-full flex justify-end items-center bg-white pr-1 relative">

        {/* fifth */}
        {
          (fourth1.filter(v => v.isChecked).length > 0 || fourth2.filter(v => v.isChecked).length > 0) && (
            <>
              <div style={{ height: '864px' }} className="cursor-pointer">
                {
                  fifth.map((v, vI) => {
                    const section = 180/fifth.length
                    const kons = (section * (vI))
                    return(
                      <div onClick={() => handleFifthClick(v)}>
                        {
                          Array((section*10) - 5).fill('').map((k, i) => (
                            <div>
                              <div 
                                className={`${v.isChecked ? 'bg-primary' : 'bg-gray-400'} absolute`}
                                style={{ 
                                  width: '1px',
                                  height: '432px',
                                  transform: `rotate(-${kons + (i/10)}deg)`,
                                  transformOrigin: `center bottom`
                                }}
                              />

                              {
                                i == 0 && (
                                  <div className="absolute"
                                    style={{ 
                                      width: '1px',
                                      height: '432px',
                                      transform: `rotate(-${kons + (i/10)}deg)`,
                                      transformOrigin: `center bottom`,
                                      zIndex: 5,
                                  }}>
                                    <div className={`text-xs text-white absolute`} style={{ fontSize: v.label.length > 8 ? '1.5rem' : '1.5rem', zIndex: 3,transform: 'rotate(-14deg) translate(-150px,-10px)', transformOrigin: 'center left' }}>
                                      {v.label}
                                    </div>
                                  </div>
                                )
                              }
                            </div>
                          ))
                        }
                      </div>
                    )
                  })
                }
              </div>
              <div style={{ marginTop: '-764px'}}>
                {
                  Array(180*10).fill('').map((v, i) => (
                    <div 
                      className="bg-white absolute"
                      style={{ 
                        width: '1px',
                        height: '382px',
                        transform: `rotate(-${i/10}deg)`,
                        transformOrigin: `center bottom`
                      }}
                    />
                  ))
                }
              </div>
            </>
          )
        }

        {/* fourth top */}
        <div style={{ height: '748px', zIndex: 2 }} className="cursor-pointer">
          {
            third.filter(v => v.isChecked).length > 0 && (
              <>
                {
                  fourth1.map((v, vI) => {
                    const section = Number((180/fourth1.length).toFixed())
                    const kons = (section * (vI))
                    return (
                      <div onClick={() => setFourth1(temp => temp.map(k => ({...k, isChecked: v === k})))}>
                        {
                          Array((section*10) - 5).fill('').map((k, i) => (
                            <div className="absolute">
                              <div 
                                className={`${v.isChecked ? 'bg-primary' : 'bg-gray-400'} absolute`}
                                style={{ 
                                  width: '1px',
                                  height: '374px',
                                  transform: `rotate(-${kons + (i/10)}deg)`,
                                  transformOrigin: `center bottom`
                                }}
                              />
                              
                              {
                                i == 0 && (
                                  <div className="absolute"
                                    style={{ 
                                      width: '1px',
                                      height: '374px',
                                      transform: `rotate(-${kons + (i/4)}deg)`,
                                      transformOrigin: `center bottom`,
                                      zIndex: 3,
                                  }}>
                                    <div className={`text-xs text-white absolute`} style={{ fontSize: v.label.length > 8 ? '0.7rem' : '0.8rem', zIndex: 3,transform: 'rotate(85deg) translate(8px, 16px)', transformOrigin: 'top left' }}>
                                      {v.label}
                                    </div>
                                  </div>
                                )
                              }
                            </div>
                          ))
                        }
                      </div>
                    )
                  })
                }
              </>
            )
          }
        </div>
        <div style={{ marginTop: '-548px', zIndex: 2}}>
          {
            Array(180*10).fill('').map((v, i) => (
              <div 
                className="bg-white absolute"
                style={{ 
                  width: '1px',
                  height: '274px',
                  transform: `rotate(-${i/10}deg)`,
                  transformOrigin: `center bottom`
                }}
              />
            ))
          }
        </div>

        {/* third */}
        {
          second.filter(v => v.isChecked).length > 0 && (
            <>
              <div style={{ height: '532px', zIndex: 3 }} className="cursor-pointer">
                {
                  third.map((v, vI) => {
                    const section = 180/third.length
                    const kons = (section * (vI))
                    return (
                      <div className="relative" onClick={() => handleThirdClick(v)}>
                        {
                          Array((section*5) - 5).fill('').map((k, i) => (
                            <div className="absolute">
                              <div 
                                className={`${v.isChecked ? 'bg-primary' : 'bg-gray-400'} absolute`}
                                style={{ 
                                  width: '1px',
                                  height: '266px',
                                  transform: `rotate(-${kons + ((5/5) * vI) + (i/5)}deg)`,
                                  transformOrigin: `center bottom`
                                }}
                              />
                              {
                                  i == 0 && (
                                    <div className="absolute"
                                      style={{ 
                                        width: '1px',
                                        height: '266px',
                                        transform: `rotate(-${kons + ((5/5) * vI) + (i/5)}deg)`,
                                        transformOrigin: `center bottom`,
                                        zIndex: 5,
                                    }}>
                                      <div className={`text-xs text-white absolute`} style={{ fontSize: v.label.length > 8 ? '1.5rem' : '1.5rem', zIndex: 3,transform: 'rotate(-45deg) translate(-230px,-62px)', transformOrigin: 'center left' }}>
                                        {v.label}
                                      </div>
                                    </div>
                                  )
                                }
                            </div>
                          ))
                        }
                      </div>
                    )
                  })
                }
              </div>
              <div style={{ marginTop: '-432px', zIndex: 3}}>
                {
                  Array(180*4).fill('').map((v, i) => (
                    <div 
                      className="bg-white absolute"
                      style={{ 
                        width: '1px',
                        height: '216px',
                        transform: `rotate(-${i/4}deg)`,
                        transformOrigin: `center bottom`
                      }}
                    />
                  ))
                }
              </div>
            </>
          )
        }
        {/* second */}
        {
          beginner && (
            <>
              <div style={{ height: '416px', zIndex: 4, }} className="cursor-pointer">
                {
                  second.map((v, vI) => {
                    const section = 180/second.length
                    const kons = (section * (vI))
                    return (
                      <div onClick={() => handleSecondClick(v)}>
                        {
                          Array((section*4) - 5).fill('').map((k, i) => {
                            return (
                              <div className="absolute">
                                <div 
                                  className={`${v.isChecked ? 'bg-primary' : 'bg-gray-400'} absolute`}
                                  style={{ 
                                    width: '1px',
                                    height: '208px',
                                    transform: `rotate(-${kons + (i/4)}deg)`,
                                    transformOrigin: `center bottom`,
                                  }}
                                />
                                {
                                  i == 0 && (
                                    <div className="absolute"
                                      style={{ 
                                        width: '1px',
                                        height: '208px',
                                        transform: `rotate(-${kons + (i/4)}deg)`,
                                        transformOrigin: `center bottom`,
                                        zIndex: 5,
                                    }}>
                                      <div className={`text-xs text-white absolute`} style={{ fontSize: v.label.length > 8 ? '0.5rem' : '0.8rem', zIndex: 3,transform: 'rotate(85deg) translate(5px, 8px)', transformOrigin: 'top left' }}>
                                        {v.label}
                                      </div>
                                    </div>
                                  )
                                }
                              </div>
                            )
                          })
                        }
                      </div>
                    )
                  })
                }
              </div>
              <div style={{ marginTop: '-216px', zIndex: 4}}>
                {
                  Array(180*4).fill('').map((v, i) => (
                    <div 
                      className="bg-white absolute"
                      style={{ 
                        width: '1px',
                        height: '108px',
                        transform: `rotate(-${i/4}deg)`,
                        transformOrigin: `center bottom`
                      }}
                    />
                  ))
                }
              </div>
            </>
          )
        }
        
        {/* first */}
        <div style={{ height: '200px', zIndex: 5 }} className="cursor-pointer relative" onClick={() => setBeginner(true)}>
          <div className="absolute text-white font-medium h-4" style={{ transform: 'rotate(-90deg)', top: '90px', bottom: 0, left: '-120px', zIndex: 3 }}>BEGINNER</div>
          {
            Array(180*4).fill('').map((v, i) => (
              <div 
                className={`${beginner ? 'bg-primary' : 'bg-gray-400'} absolute`}
                style={{ 
                  width: '1px',
                  height: '100px',
                  transform: `rotate(-${i/4}deg)`,
                  transformOrigin: `center bottom`,
                }}
              />
            ))
          }
        </div>
        <div style={{ marginTop: '-100px',  zIndex: 5}}>
          {
            Array(180*4).fill('').map((v, i) => (
              <div 
                className="bg-white absolute"
                style={{ 
                  width: '1px',
                  height: '50px',
                  transform: `rotate(-${i/4}deg)`,
                  transformOrigin: `center bottom`
                }}
              />
            ))
          }
        </div>
      
        <img src="/images/ic_bapera.png" className="absolute z-10" style={{ right: '-47px', width: '92px' }}/>
      </div>
      <div className="w-full flex items-center pl-1">

        {/* second */}
        {
          advance && (
            <>
              <div style={{ height: '600px', zIndex: 5 }} className="cursor-pointer relative" onClick={() => setAdvance(true)}>
                <div className="absolute text-white font-medium h-4 text-4xl" style={{ width: '600px', transform: 'rotate(90deg)', top: '400px', bottom: 0, right: '-500px', zIndex: 3 }}>BE ADVENTUROUS</div>
                <div className="absolute text-white font-medium h-4 text-2xl" style={{ width: '600px',transform: 'rotate(90deg)', top: '355px', bottom: 0, right: '-450px', zIndex: 3 }}>EXPLORE LAKKON AS YOU PLEASE</div>
                {
                  Array(180*8).fill('').map((v, i) => (
                    <div 
                      className={`${advance ? 'bg-primary' : 'bg-gray-400'} absolute`}
                      style={{ 
                        width: '1px',
                        height: '300px',
                        transform: `rotate(${i/8}deg)`,
                        transformOrigin: `center bottom`,
                      }}
                    />
                  ))
                }
              </div>
              <div style={{ marginTop: '-216px',  zIndex: 5}}>
                {
                  Array(180*8).fill('').map((v, i) => (
                    <div 
                      className="bg-white absolute"
                      style={{ 
                        width: '1px',
                        height: '108px',
                        transform: `rotate(${i/8}deg)`,
                        transformOrigin: `center bottom`
                      }}
                    />
                  ))
                }
              </div>
            </>
          )
        }
        
        {/* first */}
        <div style={{ height: '200px', zIndex: 5 }} className="cursor-pointer relative" onClick={() => setAdvance(true)}>
          <div className="absolute text-white font-medium h-4" style={{ transform: 'rotate(90deg)', top: '90px', bottom: 0, right: '-120px', zIndex: 3 }}>ADVANCED</div>
          {
            Array(180*4).fill('').map((v, i) => (
              <div 
                className={`${advance ? 'bg-primary' : 'bg-gray-400'} absolute`}
                style={{ 
                  width: '1px',
                  height: '100px',
                  transform: `rotate(${i/4}deg)`,
                  transformOrigin: `center bottom`,
                }}
              />
            ))
          }
        </div>
        <div style={{ marginTop: '-100px',  zIndex: 5}}>
          {
            Array(180*4).fill('').map((v, i) => (
              <div 
                className="bg-white absolute"
                style={{ 
                  width: '1px',
                  height: '50px',
                  transform: `rotate(${i/4}deg)`,
                  transformOrigin: `center bottom`
                }}
              />
            ))
          }
        </div>
      </div>
      
      <div className="absolute left-0 bottom-0 mb-4 ml-4 text-3xl">
        <p className={(advance == false && beginner == false) ? '' : 'hidden'}>
          Tingkat pengetahuan kopi anda?
        </p>
        <p className={(beginner == true && second.filter(v => v.isChecked).length == 0) ? '' : 'hidden'}>
          Daerah asal kopi yang diinginkan?
        </p>
        <p className={(beginner == true && second.filter(v => v.isChecked).length > 0 && third.filter(v => v.isChecked).length == 0) ? '' : 'hidden'}>
          Jenis kopi yang diinginkan?
        </p>
        <p className={(beginner == true && second.filter(v => v.isChecked).length > 0 && third.filter(v => v.isChecked).length > 0 && fourth1.filter(v => v.isChecked).length == 0) ? '' : 'hidden'}>
          Proses kopi yang diinginkan?
        </p>
        <p className={(beginner == true && second.filter(v => v.isChecked).length > 0 && third.filter(v => v.isChecked).length > 0 && fourth1.filter(v => v.isChecked).length > 0 && fifth.filter(v => v.isChecked).length == 0) ? '' : 'hidden'}>
          Profil rasa yang diinginkan?
        </p>
      </div>
    </div>
  )
}

Wheels.getInitialProps = async (ctx) => {
  const { token } = ctx.store.getState().authentication
  const api = axiosSetup(token)
  const attributeResp = await api.get(`/product_types_attributes?slug=roasted-bean-2`);
  const filters = attributeResp.data.data

  return {
    filters
  }
}

export default Wheels