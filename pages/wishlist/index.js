import React, { useState } from 'react';
import Link from 'next/link'
import PropTypes from 'prop-types';
import InfiniteScroll from 'react-infinite-scroll-component';
import BaseLayout from '~/components/BaseLayout';
import FilterPrice from '~/components/FilterPrice';
import FilterAttributes from '~/components/FilterAttributes';
import CardProduct from '~/components/CardProduct';
import { getWishlists } from '~/data/api/wishlist';
import { getProductType } from '~/data/api/product-type';
import { getCart } from '~/data/api/cart';

// Axios API Request
import axiosSetup from '../../services/api';

// const origins = [
//   {
//     isChecked: false,
//     name: 'Aceh',
//     total: 8
//   },
//   {
//     isChecked: false,
//     name: 'Semarang',
//     total: 2
//   },
//   {
//     isChecked: false,
//     name: 'Bandung',
//     total: 7
//   },
//   {
//     isChecked: false,
//     name: 'Jawa',
//     total: 5
//   },
//   {
//     isChecked: false,
//     name: 'Amerika Selatan',
//     total: 6
//   },
//   {
//     isChecked: false,
//     name: 'Lain - lain',
//     total: 8
//   }
// ];

// const products = [
//   {
//     discount: 6,
//     slug: 'slug-product',
//     image: '/images/product1.png',
//     productName: 'Flores Colol Coffee Beans Terbaru',
//     merchant: 'ANOMALI COFFEE',
//     rating: 4,
//     comments: 7,
//     price: 150000,
//     priceSelling: 130000
//   },
//   {
//     discount: 6,
//     slug: 'slug-product',
//     image: '/images/product1.png',
//     productName: 'Flores Colol Coffee Beans Terbaru',
//     merchant: 'ANOMALI COFFEE',
//     rating: 4,
//     comments: 7,
//     price: 150000,
//     priceSelling: 130000
//   },
//   {
//     discount: 6,
//     slug: 'slug-product',
//     image: '/images/product1.png',
//     productName: 'Flores Colol Coffee Beans Terbaru',
//     merchant: 'ANOMALI COFFEE',
//     rating: 4,
//     comments: 7,
//     price: 150000,
//     priceSelling: 130000
//   },
//   {
//     discount: 6,
//     slug: 'slug-product',
//     image: '/images/product1.png',
//     productName: 'Flores Colol Coffee Beans Terbaru',
//     merchant: 'ANOMALI COFFEE',
//     rating: 4,
//     comments: 7,
//     price: 150000,
//     priceSelling: 130000
//   },
//   {
//     discount: 6,
//     slug: 'slug-product',
//     image: '/images/product1.png',
//     productName: 'Flores Colol Coffee Beans Terbaru',
//     merchant: 'ANOMALI COFFEE',
//     rating: 4,
//     comments: 7,
//     price: 150000,
//     priceSelling: 130000
//   },
//   {
//     discount: 6,
//     slug: 'slug-product',
//     image: '/images/product1.png',
//     productName: 'Flores Colol Coffee Beans Terbaru',
//     merchant: 'ANOMALI COFFEE',
//     rating: 4,
//     comments: 7,
//     price: 150000,
//     priceSelling: 130000
//   },
//   {
//     discount: 6,
//     slug: 'slug-product',
//     image: '/images/product1.png',
//     productName: 'Flores Colol Coffee Beans Terbaru',
//     merchant: 'ANOMALI COFFEE',
//     rating: 4,
//     comments: 7,
//     price: 150000,
//     priceSelling: 130000
//   },
//   {
//     discount: 6,
//     slug: 'slug-product',
//     image: '/images/product1.png',
//     productName: 'Flores Colol Coffee Beans Terbaru',
//     merchant: 'ANOMALI COFFEE',
//     rating: 4,
//     comments: 7,
//     price: 150000,
//     priceSelling: 130000
//   },
//   {
//     discount: 6,
//     slug: 'slug-product',
//     image: '/images/product1.png',
//     productName: 'Flores Colol Coffee Beans Terbaru',
//     merchant: 'ANOMALI COFFEE',
//     rating: 4,
//     comments: 7,
//     price: 150000,
//     priceSelling: 130000
//   },
//   {
//     discount: 6,
//     slug: 'slug-product',
//     image: '/images/product1.png',
//     productName: 'Flores Colol Coffee Beans Terbaru',
//     merchant: 'ANOMALI COFFEE',
//     rating: 4,
//     comments: 7,
//     price: 150000,
//     priceSelling: 130000
//   },
//   {
//     discount: 6,
//     slug: 'slug-product',
//     image: '/images/product1.png',
//     productName: 'Flores Colol Coffee Beans Terbaru',
//     merchant: 'ANOMALI COFFEE',
//     rating: 4,
//     comments: 7,
//     price: 150000,
//     priceSelling: 130000
//   },
//   {
//     discount: 6,
//     slug: 'slug-product',
//     image: '/images/product1.png',
//     productName: 'Flores Colol Coffee Beans Terbaru',
//     merchant: 'ANOMALI COFFEE',
//     rating: 4,
//     comments: 7,
//     price: 150000,
//     priceSelling: 130000
//   }
// ];

const Category = ({
  slug, wishlist, productType, token, cart
}) => {
  const data = {};
  const [listWishlist, setListWishlist] = useState(wishlist);
  const [sort, setSort] = useState('date,DESC');
  const [limit, setLimit] = useState(12);
  const [page, setPage] = useState(1);
  const [loadMore, setLoadMore] = useState(true);

  const handleSort = (event) => {
    setSort(event.currentTarget.value);
    loadFilteredProduct();
    setLoadMore(true);
  }

  const handleLimit = (event) => {
    setLimit(event.currentTarget.value);
    loadFilteredProduct();
    setLoadMore(true);
  }

  const loadFilteredProduct = () => {
    const api = axiosSetup(token);
    setPage(1);
    const productParam = `layout_type=list_layout&page=${page}&limit=${limit}&order=${sort}`;
    api.get('/restricted/customer/wishlist?' + productParam).then((response) => {
      setListWishlist(response.data.data);
    });
  }

  const hanldeLoadMoreProduct = () => {
    setPage(page + 1);
    const api = axiosSetup(token);
    setTimeout(() => {
      const productParam = `layout_type=list_layout&page=${page + 1}&limit=${limit}&order=${sort}`;
      api.get('/restricted/customer/wishlist?' + productParam).then((response) => {
        if(response.data.data){
          setListWishlist(listWishlist.concat(response.data.data));
        } else {
          setLoadMore(false);
        }
      });
    }, 500);
  }

  return (
    <div>
      <BaseLayout productType={productType} token={token} cart={cart}>
        <div className="c-content flex flex-wrap product-content">
          {/* <div className="c-filter">
            <span className="w-100 title-filter">Urutkan Berdasarkan</span>
            <FilterPrice />
            <FilterAttributes title="Origin" data={origins} />
            <FilterAttributes title="Species" data={origins} />
            <FilterAttributes title="Roast Level" data={origins} />
            <FilterAttributes title="Tasted" data={origins} />
          </div> */}
          <div className="w-1/6 xs:w-full sm:w-full">
            <div className="c-filter">
              <Link href="/account">
                <div className="c-menu">
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <img src="/images/ic_account.png" alt="" />
                    <span style={{ marginLeft: '16px' }}>Detail Akun</span>
                  </div>
                  <img src="/images/ic_arrow_right.png" alt="" />
                  <div className="divider" />
                </div>
              </Link>

              <Link href="/address">
                <div className="c-menu">
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <img src="/images/ic_map.png" alt="" />
                    <span style={{ marginLeft: '16px' }}>Daftar Alamat</span>
                  </div>
                  <img src="/images/ic_arrow_right.png" alt="" />
                  <div className="divider" />
                </div>
              </Link>
              <Link href="/transaction-history">
                <div className="c-menu">
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <img src="/images/ic_truck.png" alt="" />
                    <span style={{ marginLeft: '16px' }}>Pesanan Saya</span>
                  </div>
                  <img src="/images/ic_arrow_right.png" alt="" />
                  <div className="divider" />
                </div>
              </Link>

              <Link href="/history">
                <div className="c-menu">
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <img src="/images/ic_truck.png" alt="" />
                    <span style={{ marginLeft: '16px' }}>Mutasi Rekening</span>
                  </div>
                  <img src="/images/ic_arrow_right.png" alt="" />
                  <div className="divider" />
                </div>
              </Link>

              <Link href="/wishlist">
                <div className="c-menu active">
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <img src="/images/ic_action_like.png" alt="" />
                    <span style={{ marginLeft: '16px' }}>Wishlist</span>
                  </div>
                  <img src="/images/ic_arrow_right_red.png" alt="" />
                  <div className="divider" />
                </div>
              </Link>

            </div>
          </div>
          <div className="w-5/6 xs:w-full sm:w-full md:w-5/6 lg:w-5/6">
            <div className="c-products">
            <div className="c-filter-product">
              <div className="c-filter-page flex items-center">
                Menampilkan
                <select onChange={handleLimit} className="form-filter-page">
                    <option value="12">12</option>
                    <option value="24">24</option>
                    <option value="36">36</option>
                  </select>
                dari <span>&nbsp;{listWishlist ? listWishlist.length : 0}</span>
              </div>
              <div className="c-filter-page flex items-center">
                Urutkan
                <select onChange={handleSort} className="form-filter-page">
                    <option value="product_name">Nama Produk</option>
                    <option value="popular">Popularitas</option>
                    <option value="price,ASC">Harga Terendah</option>
                    <option value="price,DESC">Harga Tertinggi</option>
                  </select>
              </div>
            </div>

            <div className="c-content-products">
              <InfiniteScroll
                dataLength={listWishlist ? listWishlist.length : 0}
                next={hanldeLoadMoreProduct}
                hasMore={loadMore}
                className="flex flex-wrap w-full"
              >
              {
                listWishlist ? listWishlist.map((v) => (
                  <CardProduct data={v.product} />
                )) : <h4 style={{marginTop: "30px", marginBottom:"30px", textAlign:"center", fontSize:"20px", width: "100%"}}>Tidak ada produk yang dalam wishlist anda</h4>
              }
              </InfiniteScroll>
            </div>
          </div>
          </div>
        </div>
      </BaseLayout>
      <style jsx>
        {`
        .breadcumb {
          font-size: 14px;
          color: #696969;
        }
      
        .c-content {
          width: 100%;
          display: flex;
        }
      
        .c-filter {
          width: 350px;
          display: flex;
          flex-wrap: wrap;
          justify-content: flex-start;
          flex-direction: column;
        }
      
        .title-filter {
          font-weight: 500;
          font-size: 18px;
          color: #696969;
        }
      
        .c-products {
          width: 100%;
          padding: 0px 0px 0px 24px;
        }
      
        .c-filter-product {
          width: 100%;
          display: flex;
          justify-content: space-between;
        }
      
        .c-filter-page {
          font-size: 14px;
          color: #696969;
        }
      
        .form-filter-page {
          background: rgba(248, 248, 248, 0.75);
          border: 1px solid #F2F2F2;
          box-sizing: border-box;
          padding: 4px 4px;
          font-size: 16px;
          color: #696969;
          margin: 0px 4px;
          font-family: 'Gotham';
        }
      
        .c-content-products {
          display: flex;
          flex-wrap: wrap;
          justify-content: space-between;
        }
        .c-content{
          padding:48px 0;
        }
        .c-products{
          padding:0 4rem;
        }
        .breadcumb-wrapper{
          padding:20px 0;
        }
        .c-filter {
          width: 100%;
        }
        .divider {
          width: 100%;
          margin-top: 12px;
          height: 1px;
          background: #E1E1E1;
        }

        .c-menu {
          font-weight: 500;
          font-size: 16px;
          line-height: 17px;
          letter-spacing: 0.05em;
          display: flex;
          justify-content: space-between;
          align-items: center;
          flex-wrap: wrap;
          padding: 10px 0px;
          cursor: pointer
        }

        .active {
          color: #010101;
        }
        @media (min-width: 320px) and (max-width: 767px) {
          .c-content{
            padding: 0 20px;
          }
          .c-products{
            padding: 0;
            margin-top:30px;
          }
          .breadcumb-wrapper{
            padding:0 20px;
            text-align:center;
            margin-bottom:30px;
          }
        }
        @media (min-width: 0) and (max-width:372px){
          .c-filter-page{
            font-size:12px;
          }
          .c-filter-page .form-filter-page{
            font-size:14px;
          }
        }
        `}
      </style>
    </div>
  );
};

Category.propTypes = {
  slug: PropTypes.string
};

Category.getInitialProps = async (ctx) => {
  const { slug } = ctx.query;
  const { token } = ctx.store.getState().authentication;

  const productTypeResp = await getProductType();
  const productType = productTypeResp.data;

  const wishlistRes = await getWishlists(token, "product_name,ASC", "1", "12");
  const wishlist = wishlistRes.data;

  const cartResp = await getCart(token);
  const cart = cartResp.data ? cartResp.data.cart : [];

  return { 
    slug,
    wishlist,
    productType,
    token,
    cart
  };
};

export default Category;
