import React from 'react';
import Router from 'next/router';
import { Provider } from 'react-redux';
import App, { Container } from 'next/app';
import withRedux from 'next-redux-wrapper';
import NProgress from 'nprogress';
import { makeStore } from '~/redux';
import { checkServerSideCookie } from '~/redux/actions/authActions';
import { getProfile } from '~/util/ServiceHelper';
import Layout from '../components/Layout';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import '~/public/css/nprogress.css';
import 'rc-slider/assets/index.css';
import '~/styles/index.css';
import Head from 'next/head';

//date range
import 'react-date-range/dist/styles.css'; // main style file
import 'react-date-range/dist/theme/default.css'; // theme css file
import 'react-quill/dist/quill.snow.css';

Router.onRouteChangeStart = () => {
  // console.log('onRouteChnageStart triggered');
  NProgress.start();
};

Router.onRouteChangeComplete = () => {
  // console.log('onRouteChnageComplete triggered');
  NProgress.done();
};

Router.onRouteChangeError = () => {
  // console.log('onRouteChnageError triggered');
  NProgress.done();
};


class MyApp extends App {
  static async getInitialProps({ Component, ctx }) {
    await checkServerSideCookie(ctx);

    const { token } = ctx.store.getState().authentication;
    if (token) {
      if (ctx.isServer) {
        const res = await getProfile(token);
        ctx.store.dispatch({ type: 'SET_USER', payload: res.data });
      }
    }

    // we can dispatch from here too
    const pageProps = Component.getInitialProps ? await Component.getInitialProps(ctx) : {};
    return { pageProps };
  }

  render() {
    const { Component, pageProps, store } = this.props;

    return (
      <Container>
      <Head>
        <script src="/js/InstagramFeed.min.js"></script>
      </Head>
        <Provider store={store}>
          <Layout>
            <Component {...pageProps} />
          </Layout>
        </Provider>
      </Container>
    );
  }
}

export default withRedux(makeStore)(MyApp);
