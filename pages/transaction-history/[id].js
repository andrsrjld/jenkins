import React, { useState, useEffect } from 'react'
import { getOrderDetail } from 'data/api/order'
import moment from 'moment';
import { formatNumber } from 'util/helper'

const invoice = ({ invoices }) => {
    // useEffect(() => {
    //   setTimeout(() => {
    //     window.print()
    //   }, 1000)
    // })
    return (
      <div style={{padding: "20px"}}>
        <div className="container" style={{margin: "0 auto"}}>
          <div className="w-full flex">
            <div className="w-1/2 justify-start">
              <div className="invoice-title">
                <h1>Invoice</h1>
                <label>{invoices[0].invoice_no}</label>
                <p>{moment(invoices[0].date).format("dddd, DD MMMM YYYY")} </p>
              </div>
            </div>
            <div className="w-1/2 justify-end">
              <div className="invoice-user">
                <div className="invoice-user-name">
                  <h1>{invoices[0].user_name}</h1>
                </div>
                <div className="invoice-user-address">
                  <p>{invoices[0].address} </p>
                </div>
              </div>
            </div>
          </div>
          <div className="w-full flex invoice-table" style={{background: "#eee"}}>
            <div className="invoice-table-header flex-1">Penjual</div>
            <div className="invoice-table-header flex-1">Produk</div>
            <div className="invoice-table-header flex-1">Varian</div>
            <div className="invoice-table-header flex-1">Catatan</div>
            <div className="invoice-table-header flex-1">Harga</div>
            <div className="invoice-table-header flex-1">Jumlah</div>
            <div className="invoice-table-header flex-1">Discount</div>
            <div className="invoice-table-header flex-1">Total</div>
          </div>
          <div className="invoice-table-product">
            {
              invoice.order_items.map(item => {
                return (
                <div className="w-full flex">
                  <div className="invoice-table-item flex-1">Penjual</div>
                  <div className="invoice-table-item flex-1">{item.product_name}</div>
                  <div className="invoice-table-item flex-1">{item.variant_id}</div>
                  <div className="invoice-table-item flex-1">{item.notes}</div>
                  <div className="invoice-table-item flex-1">Rp. {formatNumber(item.price)}</div>
                  <div className="invoice-table-item flex-1">{item.quantity}</div>
                  <div className="invoice-table-item flex-1">Total Discount</div>
                  <div className="invoice-table-item flex-1">Rp. {formatNumber(item.subtotal)}</div>
                </div>
                )
              })
            }
          </div>
          <div className="invoice-table-total">
            <div className="invoice-total flex text-right">
              <div className="invoice-total-title flex-1">
                <label>Total Produk</label>
              </div>
              <div className="invoice-total-title flex-1">
                <label>Rp. {formatNumber(invoices[0].total_price)}</label>
              </div>
            </div>
            <div className="invoice-total flex text-right">
              <div className="invoice-total-title flex-1">
                <label>Total Discount</label>
              </div>
              <div className="invoice-total-title flex-1">
                <label>Rp. {formatNumber(invoices[0].total_discount)}</label>
              </div>
            </div>
            <div className="invoice-total flex text-right">
              <div className="invoice-total-title flex-1">
                <label>Total Shipping</label>
              </div>
              <div className="invoice-total-title flex-1">
                <label>Rp. {formatNumber(invoice.total_shipping)}</label>
              </div>
            </div>
            <div className="invoice-total flex text-right">
              <div className="invoice-total-title flex-1">
                <label>Biaya Layanan</label>
              </div>
              <div className="invoice-total-title flex-1">
                <label>Rp. {formatNumber(5000)}</label>
              </div>
            </div>
            <div className="invoice-total flex text-right">
              <div className="invoice-total-title flex-1">
                <label>Total Pembayaran</label>
              </div>
              <div className="invoice-total-title flex-1">
                <label>Rp. {formatNumber(invoice.total_price - 0 + invoice.total_shipping + 5000)}</label>
              </div>
            </div>
          </div>
          <div className="flex justify-center w-full">
            <div className="footer">
              <img src="/images/ic_bapera.png" style={{margin: "0 auto 10px"}} />
              <p>2020 powered by Lakkon.id</p>
            </div>
          </div>
        </div>
        <style jsx>
          {`
            @media screen, print {
              .container{
                width:100%;
                max-width: 100%;
              }
              .invoice-title{
                font-size:16px;
              }
              .invoice-title h1{
                font-size:36px;
              }
              .invoice-user{
                border:1px solid #000;
              }
              .invoice-user-name{
                background-color:#eee;
                padding:10px 20px;
                -webkit-print-color-adjust: exact;
                -moz-print-color-adjust: exact;
              }
              .invoice-user-address{
                padding:10px 20px;
              }
              .invoice-table-header{
                background-color:#ddd;
                font-weight:900;
                text-align:center;
                border:1px solid #000;
                -webkit-print-color-adjust: exact;
                -moz-print-color-adjust: exact;
              }
              .invoice-table-item{
                border:1px solid #000;
                text-align:center;
              }
              .invoice-table{
                margin-top:50px;
              }
              .invoice-table-total{
                float:right;
                width: 450px;
                margin-top:30px;
              }
              .footer{
                margin-top:40px;
              }
            }
          `}
        </style>
      </div>
    )
}

invoice.getInitialProps = async (ctx) => {
  const { id } = ctx.query;
  const { token } = ctx.store.getState().authentication;

  return {
    invoices
  }
}

export default invoice