import React, { useState, useEffect, useRef } from 'react'
import { connect } from 'react-redux'
import Link from 'next/link'
import BaseLayout from 'components/BaseLayout'
import Modal from 'components/Modal'
import { getOrders, getOrderDetail, getWayBill, STATUS, updateStatusOrderUser } from 'data/api/order'
import { getProductType } from 'data/api/product-type'
import { getTransactionStatus } from 'data/api/payment'
import CardTransactionHistory from 'components/CardTransactionHistory'
import moment from 'moment'
import { formatNumber } from 'util/helper'
import Loading from 'components/Loading'
import { getCart } from 'data/api/cart';
import { getRefundUser, postRefund } from 'data/api/refund';
import Select from 'react-select'
import { BankList } from 'util/DataHelper'

// Toastr Notification
import {successNotification, errorNotification} from 'util/toastr-notif';
import { bookingStatus } from 'data/api/gosend'

const sliderMenu = [
  {
    content: 0,
    name: 'Semua',
    qty: '1',
    isActive: true,
    status: ''
  },
  // {
  //   content: 1,
  //   name: 'Belum Dibayar',
  //   qty: '1',
  //   isActive: false,
  //   status: STATUS.MENUNGGU_KONFIRMASI
  // },
  {
    content: 2,
    name: 'Diproses',
    qty: '1',
    isActive: false,
    status: STATUS.DIPROSES
  },
  {
    content: 3,
    name: 'Dikirim',
    qty: '1',
    isActive: false,
    status: STATUS.DIKIRIM
  },
  {
    content: 4,
    name: 'Selesai',
    qty: '2',
    isActive: false,
    status: STATUS.SELESAI
  },
  {
    content: 5,
    name: 'Dibatalkan',
    qty: '3',
    isActive: false,
    status: STATUS.DIBATALKAN
  },
  {
    content: 6,
    name: 'Refund',
    qty: '3',
    isActive: false,
    status: 'refund'
  },
]

const SliderItem = ({data, cbClick}) => (
  <div className={`transaction-tab-menu block xs:inline-block sm:inline-block font-medium px-2 py-2 bg-white cursor-pointer ${data.isActive ? 'border-b-2 border-primary text-primary' : 'border-gray-500 text-text-primary'} hover:text-primary`}
    onClick={() => cbClick(data ? data : "")}>
    {`${data.name}`}
    <style jsx>
      {`
        @media (min-width: 320px) and (max-width: 767px) {
          .transaction-tab-menu{
            padding: 0 20px;
          }
        }
      `}
    </style>
  </div>
)

const index = ({authentication, productType, cart}) => {
  const {token} = authentication
  const {apiUrl} = authentication?.endpoint
  const [modalDetail, setModalDetail] = useState(false);
  const [lacak, setLacak] = useState(false);
  const [sliders, setSliders] = useState(sliderMenu)
  const [selectedContent, setSelectedContent] = useState(sliderMenu[0])
  const [tableData, setTableData] = useState([])
  const [selectedData, setSelectedData] = useState(null)
  const [detailData, setDetailData] = useState(null)
  const [selectedLacakData, setSelectedLacakData] = useState(null)
  const [shippingData, setShippingData] = useState(null)

  const [selectedDataRefund, setSelectedDataRefund] = useState(null)
  const [detailDataRefund, setDetailDataRefund] = useState(null)
  const [modalDetailRefund, setModalDetailRefund] = useState(false);

  const [refundCheckAll, setRefundCheckAll] = useState(false);
  const [refundNotes, setRefundNotes] = useState('')
  const [bank, setBank] = useState(null)
  const [bankAccount, setBankAccount] = useState('')

  const [loading, setLoading] = useState(false)

  const componentRef = useRef();
  
  const [tabledDatas, setTableDatas] = useState([])
  const [checkedAll, setCheckedAll] = useState(false)
  const [page, setPage] = useState(1)
  const [limit, setLimit] = useState(10)

  // Lacak Gosend
  const [gosendData, setGosendData] = useState(null)
  // const [modalGosend, setModalGosend] = useState(false)
  //\\

  useEffect(() => {
    // if(selectedSeller != '') {
      getReport()
    // }
  }, [page])

  const getReport = async () => {
    const res = await getRefundUser(token, limit, page, '')
    
    setTableDatas(res.data ? res.data.map(v => ({...v, checked: false})) : [])
  }

  useEffect(() => {
    if(selectedContent.status == 'refund') {
      
    } else {
      doGetOrders(selectedContent.status ? selectedContent.status : '')
    }
  }, [sliders])

  useEffect(() => {
    if(selectedData) {
      doGetOrderDetail()
    }
  }, [selectedData]);

  useEffect(() => {
    if(detailData) setModalDetail(true)
  }, [detailData]);

  useEffect(() => {
    if(selectedDataRefund) {
      doGetOrderDetailRefund()
    }
  }, [selectedDataRefund]);

  useEffect(() => {
    if(detailDataRefund) setModalDetailRefund(true)
  }, [detailDataRefund]);

  useEffect(() => {
    if(!modalDetailRefund) {
      setSelectedDataRefund(null)
    }
  }, [modalDetailRefund]);

  const handleRefundCheckAll = () => {
    const val = !refundCheckAll
    const newData = {
      ...detailDataRefund,
      order_items: detailDataRefund?.order_items.map(v => ({
       ...v,
       checked: val
      }))
    }

    setDetailDataRefund(newData)
    setRefundCheckAll(val)
  }

  const handleCheckedRefund = (index) => {
    const newData = {
      ...detailDataRefund,
      order_items: detailDataRefund?.order_items.map((v, i) => ({
       ...v,
       checked: i === index ? !v.checked : v.checked
      }))
    }
    setDetailDataRefund(newData)
  }

  const handleSubmitRefund = () => {
    setLoading(true)

    const promises = detailDataRefund?.order_items.filter(v => v.checked).map(v => 
      postRefund(token, {
        "order_seller_id": detailDataRefund?.id,
        "product_id": v.product_id,
        "variant_id": v.variant_id,
        "user_id": detailDataRefund?.address_detail.user_id,
        "seller_id": selectedDataRefund.seller_id,
        "quantity": v.quantity,
        "subtotal":v.subtotal,
        "status": "PENDING",
        "picture": "",
        "notes": `[Dibatalkan oleh BUYER] ${refundNotes}`,
        "bank": bank.value,
        "rekening": bankAccount
      })
    )

    Promise.all(promises)
      .then((e) => {})
      .catch((err) => {}) 
      .then((e) => {
        setLoading(false)
        setModalDetailRefund(false)
        setRefundNotes('')
        successNotification('Permohonan REFUND berhasil dibuat!')
      })
  }

  useEffect(() => {
    if(!modalDetail) setSelectedData(null)
  }, [modalDetail])

  useEffect(() => {
    if(selectedLacakData) {
      doGetWayBill()
    }
  }, [selectedLacakData]);

  useEffect(() => {
    if(shippingData) setLacak(true)
  }, [shippingData]);
  
  useEffect(() => {
    if(!lacak) setSelectedLacakData(null)
  }, [lacak])

  const doGetOrders = async (status) => {
    const data = [];
    const res = await getOrders(apiUrl, token, status)
    
    if(status === '' || status === STATUS.MENUNGGU_KONFIRMASI){
      setTableData(res.data ? res.data.map(v => ({...v, isOpen: false})) : []);
    } else {
      const listData = [];
      if(res.data){
        res.data.map(item => {
          item.order_sellers.map(seller => {
            if(!listData.some(v => v.slug === seller.slug)){
              const dataObj = {
                ...seller,
                isOpen:false,
                date: item.date,
              }
              listData.push(dataObj);
            }
          })
        })
      }
      setTableData(listData);
    }
  }

  const doGetOrderDetail = async () => {
    const res = await getOrderDetail(apiUrl, token, selectedData.id)
    setDetailData(res.data ? res.data : null)
  }

  const doGetOrderDetailRefund = async () => {
    const res = await getOrderDetail(apiUrl, token, selectedDataRefund.id)
    setDetailDataRefund(res.data ? {...res.data, order_items: res.data.order_items.map(v => ({...v, checked: false}))} : null)
  }

  const doGetWayBill = async () => {
    setLoading(true)
    if(selectedLacakData?.shipping.toLowerCase().includes('go-send')) {
      const res = await bookingStatus(apiUrl, token, selectedLacakData?.airway_bill)
      setGosendData(res.data ? res.data : null)
    } else {
      if(selectedLacakData?.shipping.split('-')[0].toLowerCase().trim() == 'jne') {
        window.open('https://www.jne.co.id/id/tracking/trace', '_blank')
        setLoading(false)
        return
      }
      const res = await getWayBill(apiUrl, token, selectedLacakData?.airway_bill, selectedLacakData?.shipping.split('-')[0].toLowerCase().trim().replace('&', 'n'))
      if(res.data.manifest) {
        setShippingData(res.data ? res.data : null)
      } else {
        errorNotification("Pelacakan Gagal!");
      }
    }
    setLoading(false)
  }

  const handleChangeSlider = (data) => {
    setSelectedContent(data)
    setSliders(sliders.map(v => ({...v, isActive: v==data })))
  }

  const renderContent = (content) => {
    switch (content) {
      case 1:
        return contentDibayar()
      case 2:
        return contentDiproses()
      default:
        return contentDibayar()
    }
  }

  const contentDibayar = () => (
    // datas.map((v, i) => (<Item data={v} />))
    {}
  )

  const contentDiproses = () => (
    // datasDiproses.map((v, i) => (<ItemProses data={v} />))
    {}
  )

  const handleOpenDetail = (id) => (e) => {
    setTableData(tableData.map(v => ({...v, isOpen: v.id === id ? !v.isOpen : v.isOpen, order_sellers: v.order_sellers.map(k => ({...k, date: v.date}))})))
  }

  const handleFinishOrder = (data) => async (e) => {
    const body = {
      id: data.id,
      status: "Pesanan Selesai",
      is_paid: true
    }
    const res = await updateStatusOrderUser(apiUrl, token, body);
    if(res.code === 200){
      successNotification("Pesanan telah berhasil diselesaikan");
    } else {
      errorNotification(res.data.status_message);
    }
  }

  const copyToClipboardResi = str => {
    const el = document.createElement('textarea');
    el.value = str;
    el.setAttribute('readonly', '');
    el.style.position = 'absolute';
    el.style.left = '-9999px';
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    successNotification("Berhasil menyalin nomor resi")
  };

  const handlePrintOrder = (data) => (e) =>{
    let divContents = `
    <html>
    <head>
        <style>
            @media screen, print {
                body{
                    font-family: "arial";
                }
                .container{
                    width:100%;
                    max-width: 100%;
                }
                .invoice-title{
                    font-size:16px;
                }
                .invoice-title h1{
                    font-size:36px;
                }
                .invoice-user{
                    border:1px solid #000;
                }
                .invoice-user-name{
                    background-color:#eee;
                    padding:10px 20px;
                    -webkit-print-color-adjust: exact;
                    -moz-print-color-adjust: exact;
                }
                .invoice-user-address{
                    padding:10px 20px;
                }
                .invoice-table-header{
                    background-color:#ddd;
                    font-weight:900;
                    text-align:center;
                    border:1px solid #000;
                    -webkit-print-color-adjust: exact;
                    -moz-print-color-adjust: exact;
                }
                .invoice-table-item{
                    border:1px solid #000;
                    text-align:center;
                }
                .invoice-table{
                    margin-top:50px;
                }
                .invoice-table-total{
                    float:right;
                    width: 450px;
                    margin-top:30px;
                }
                .footer{
                    margin-top:40px;
                }
                .col2{
                    width:50%;
                }
                .justify-start{
                    float:left;
                }
                .justify-end{
                    float:right;
                }
                .w-full{
                    display:block;
                    clear:both;
                }
                h1{
                    margin-top:0;
                    margin-bottom:10px;
                }
                p{
                    margin-top:10px;
                    margin-bottom:10px
                }
                table{
                    width:100%;
                    border-collapse: collapse;
                    margin-top:40px;
                }
                table td.invoice-table-item{
                    padding:10px;
                }
                .invoice-total-title{
                    text-align:right;
                    padding:5px 10px;
                    font-weight: bold;
                }
            }
        </style>
    </head>
    <body>
        <div style="padding: 20px;">
            <div class="container" style="margin: 0 auto;">
                <div class="invoice-header">
                    <div class="col2 justify-start">
                    <div class="invoice-title">
                        <h1>Invoice</h1>
                        <label>${data.payment_number}</label>
                        <p>${moment(data.date).format("dddd, DD MMMM YYYY")}</p>
                    </div>
                    </div>
                    <div class="col2 justify-end">
                    <div class="invoice-user">
                        <div class="invoice-user-name">
                        <h1>${data.order_sellers[0].user_name}</h1>
                        </div>
                        <div class="invoice-user-address">
                        <p>${data.address} </p>
                        </div>
                    </div>
                    </div>
                </div>
                <div style="clear:both;"></div>
                <table>
                    <thead style="background: #eee">
                        <th class="invoice-table-header flex-1">No Transaksi</div>
                        <th class="invoice-table-header flex-1">Produk</div>
                        <th class="invoice-table-header flex-1">Varian</div>
                        <th class="invoice-table-header flex-1">Catatan</div>
                        <th class="invoice-table-header flex-1">Harga</div>
                        <th class="invoice-table-header flex-1">Jumlah</div>
                        <th class="invoice-table-header flex-1">Discount</div>
                        
                        <th class="invoice-table-header flex-1">Total</div>
                    </thead>
                    <tbody class="invoice-table-product">
                        ${data.order_sellers.map(item => {
                          return item.order_items.map(product => {
                            return `<tr>
                                <td class="invoice-table-item flex-1">${item.invoice_no}</td>
                                <td class="invoice-table-item flex-1">${product.product_name}</td>
                                <td class="invoice-table-item flex-1">${product.variant_name}</td>
                                <td class="invoice-table-item flex-1">${product.notes ? product.notes : "-"}</td>
                                <td class="invoice-table-item flex-1">Rp. ${formatNumber(product.price)}</td>
                                <td class="invoice-table-item flex-1">${product.quantity}</td>
                                <td class="invoice-table-item flex-1">${product.product_discount}</td>
                                <td class="invoice-table-item flex-1" style="text-align:right;">Rp. ${formatNumber(product.subtotal)}</td>
                            </tr>`
                          })
                        })}
                        <tr class="invoice-total flex text-right">
                            <td colspan="6" class="invoice-total-title flex-1">
                                <label>Total Produk : </label>
                            </td>
                            <td colspan="2" class="invoice-total-title flex-1">
                                <label>Rp. ${formatNumber(data.total_price)}</label>
                            </td>
                        </tr>
                        <tr class="invoice-total flex text-right">
                            <td colspan="6" class="invoice-total-title flex-1">
                                <label>Total Discount : </label>
                            </td>
                            <td colspan="2" class="invoice-total-title flex-1">
                                <label>Rp. ${formatNumber(data.total_discount)}</label>
                            </td>
                        </tr>
                        <tr class="invoice-total flex text-right">
                            <td colspan="6" class="invoice-total-title flex-1">
                                <label>Total Shipping : </label>
                            </td>
                            <td colspan="2" class="invoice-total-title flex-1">
                                <label>Rp. ${formatNumber(data.total_shipping)}</label>
                            </td>
                        </tr>
                        <tr class="invoice-total flex text-right">
                            <td colspan="6" class="invoice-total-title flex-1">
                                <label>Biaya Layanan : </label>
                            </td>
                            <td colspan="2" class="invoice-total-title flex-1">
                                <label>Rp. ${formatNumber(data.service_charge)}</label>
                            </td>
                        </tr>
                        <tr class="invoice-total flex text-right">
                            <td colspan="6" class="invoice-total-title flex-1">
                                <label>Total Pembayaran : </label>
                            </td>
                            <td colspan="2" class="invoice-total-title flex-1">
                                <label>Rp. ${formatNumber(data.total_paid)}</label>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="flex justify-center w-full">
                    <div class="footer">
                    <img src="/images/ic_bapera.png" style="margin:0 auto 10px; display:block;" />
                    <p style="text-align:center;">2020 powered by Lakkon.id</p>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
    `;

    let win = window.open('','printwindow');
    win.document.write(divContents);
    win.focus();
    win.print();
  }

  const handleBayarSekarang = (data) => async (e) => {
    const res = await getTransactionStatus(token, data.payment_code, data.payment_type);
    if(!res.data.status_code){
      if(res.data.redirect_url){
        window.location = res.data.redirect_url;
      }
    } else {
      if(res.data.va_numbers) {
        copyToClipboardVA(res.data.va_numbers.va_number)
      } else if (res.data.bill_key) {
        copyToClipboardVA(`${res.data.biller_code}${res.data.bill_key}`)
      } else {
        errorNotification(res.data.status_message);
      }
    }
  }

  const copyToClipboard = str => {
    const el = document.createElement('textarea');
    el.value = str;
    el.setAttribute('readonly', '');
    el.style.position = 'absolute';
    el.style.left = '-9999px';
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    successNotification("Berhasil menyalin nomor resi")
  };

  const copyToClipboardVA = str => {
    const el = document.createElement('textarea');
    el.value = str;
    el.setAttribute('readonly', '');
    el.style.position = 'absolute';
    el.style.left = '-9999px';
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    successNotification("Berhasil menyalin nomor VA")
  };


  const handleCheckedAll = () => {
    const val = !checkedAll;

    const newData = tabledDatas.map((v, i) => ({...v, checked: val}))
    setTableDatas(newData)

    setCheckedAll(val)
  }

  const handleChecked = (index) => {
    const newData = tabledDatas.map((v, i) => ({...v, checked: i === index ? !v.checked : v.checked}))
    setTableDatas(newData)
  }

  const renderRefund = () => (
    <div className="w-full">
      {
        tabledDatas.length > 0 && (
        <div className="bg-white shadow-lg p-4 mt-4 rounded-lg flex flex-wrap">
          <div id="filters" className="w-full flex items-center">
            <div className="w-full">
              {/* <input className="border border-gray-300" type="text" placeholder="search" value={keyword} onChange={(e) => setKeyword(e.target.value)}/> */}
            </div>
            <div className="w-full flex justify-end">
              {/* <button className="py-1 px-3 border-2 border-gray-300 bg-white rounded text-text-primary flex items-center mr-2">
                <img className="mr-2" src="/images/ic_filter.png" alt="" />
                FILTERS
              </button> */}
              {/* <button className="py-1 px-3 border-2 border-gray-300 bg-white rounded text-text-primary flex items-center" onClick={handleExport}>
                <img className="mr-2" src="/images/ic_export.png" alt=""/>
                EXPORT
              </button> */}
            </div>
          </div>

          <div id="sortings" className="w-full flex items-center mt-4">
            <div className="w-full flex flex-wrap items-center xs:hidden sm:hidden">
              <div className="flex">
                {/* <div className="border border-gray-400 rounded-l flex items-center h-full">
                  <select className="py-2 px-5 text-text-primary" value="" onChange={(e) => handleUpdate(e.target.value)}>
                    <option value="" disabled>Action</option>
                    <option value="DONE">DONE</option>
                    <option value="REJECTED">REJECTED</option>
                  </select>
                </div> */}
                {/* <div className="rounded-r px-3 py-1 bg-gray-300 items-center flex">
                  <img src="/images/ic_arrow_down.png" alt=""/>
                </div> */}
              </div>
              <div className="text-text-primary font-medium ml-3">
                {/* 106 records found */}
              </div>
            </div>
            <div className="w-full flex xs:justify-center sm:justify-center justify-end items-center">
              {/* <select className="w-auto">
                <option>10</option>
              </select>
              <div className="text-text-primary font-medium ml-3">
                Per Page
              </div> */}
              <button className="px-3 py-2 bg-gray-400 rounded text-text-primary flex items-center ml-4" onClick={() => setPage(page - 1)} disabled={ page == 1 }>
                <img src="/images/ic_chevron_left.png" alt="" />
              </button>
              <input className="ml-3 w-12 text-center border border-gray-300" type="text" value={page} disabled/>
              <button className="px-3 py-2 bg-gray-400 rounded text-text-primary flex items-center ml-4" onClick={() => setPage(page + 1 )}>
                <img src="/images/ic_arrow_right.png" alt="" />
              </button>
            </div>
          </div>

          <div id="table" className="w-full mt-8">
            <div className="w-full refund-table">
              <div className="flex refund-table-head">
                  {/* <div>
                    <input type="checkbox" checked={checkedAll} onChange={(e) => handleCheckedAll()}></input>
                  </div> */}
                  <div className="w-10">ID</div>
                  <div className="w-full">Order Date</div>
                  <div className="w-full">Buyer</div>
                  <div className="w-full">Seller</div>
                  <div className="w-full">Product</div>
                  <div className="w-full">Quantity</div>
                  <div className="w-full">Subtotal</div>
                  <div className="w-full">Notes</div>
                  <div className="w-full">Status</div>
              </div>
              <div className="refund-table-body">
                {
                  tabledDatas.map((v, i) => (
                    <div key={v.id} className={`${i%2 == 0 ? 'bg-gray-200' : 'bg-white'} cursor-pointer hover:bg-gray-300 flex xs:flex-wrap sm:flex-wrap`}>
                      {/* <td>
                        {
                          v.status == "PENDING" && (
                            <input type="checkbox" checked={v.checked} onChange={(e) => handleChecked(i)}></input>
                          )
                        }
                      </td> */}
                      <div className="xs:hidden sm:hidden md:block lg:block w-10">{v.id}</div>
                      <div className="xs:w-full sm:w-full w-full refund-date">{moment(v.order_dates).format('DD MMMM YYYY')}</div>
                      <div className="xs:w-1/2 sm:w-1/2 w-full">{v.user_name}</div>
                      <div className="xs:w-1/2 sm:w-1/2 w-full">{v.seller_name}</div>
                      <div className="xs:w-full sm:w-full w-full refund-product">{`${v.product_name} - ${v.variant_name}`}</div>
                      <div className="xs:w-1/2 sm:w-1/2 w-full">{v.quantity}</div>
                      <div className="xs:w-1/2 sm:w-1/2 w-full">{`Rp ${formatNumber(v.subtotal)}`}</div>
                      <div className="xs:w-full sm:w-full w-full refund-note">{v.notes}</div>
                      <div className="xs:w-full sm:w-full w-full refund-status"><span style={v.status === "DONE" ? {color: "green"} : v.status === "Pending" ? {color: "blue"} : {color: "red"}}>{v.status}</span></div>
                    </div>
                  ))
                }
              </div>
            </div>
          </div>
        </div>
        )
      }
      {
        tabledDatas.length == 0 && (
          <div className="bg-white shadow-lg p-4 my-4 rounded-lg flex flex-wrap justify-center">
            Tidak ada data
          </div>
        )
      }
      <style jsx>
      {`
        .refund-table-head{
          background: #010101;
          color: #fff;
        }
        .refund-table-body > div > div,
        .refund-table-head > div{
          padding:5px 10px;
        }
        @media (min-width: 320px) and (max-width: 767px) {
          .refund-table-head{
            display:none;
          }
          .refund-date,
          .refund-note{
            color: #888;
            font-size:14px;
          }
          .refund-status{
            text-align:center;
          }
          .refund-table-body{
            font-size:14px;
          }
          .refund-table-body{
            text-align:center;
          }
        }
      `}
      </style>
    </div>
  )
  
  return (
    <div>
      <Loading show={loading} />
      <BaseLayout productType={productType} token={authentication.token} cart={cart}>
        <div className="c flex flex-wrap">
          <div className="w-1/6 xs:w-full sm:w-full">
            <div className="c-filter">
              <Link href="/account">
                <div className="c-menu">
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <img src="/images/ic_account.png" alt="" />
                    <span style={{ marginLeft: '16px' }}>Detail Akun</span>
                  </div>
                  <img src="/images/ic_arrow_right.png" alt="" />
                  <div className="divider" />
                </div>
              </Link>

              <Link href="/address">
                <div className="c-menu">
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <img src="/images/ic_map.png" alt="" />
                    <span style={{ marginLeft: '16px' }}>Daftar Alamat</span>
                  </div>
                  <img src="/images/ic_arrow_right.png" alt="" />
                  <div className="divider" />
                </div>
              </Link>
              <Link href="/transaction-history">
                <div className="c-menu active">
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <img src="/images/ic_truck_red.png" alt="" />
                    <span style={{ marginLeft: '16px' }}>Pesanan Saya</span>
                  </div>
                  <img src="/images/ic_arrow_right_red.png" alt="" />
                  <div className="divider" />
                </div>
              </Link>

              <Link href="/history">
                <div className="c-menu">
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <img src="/images/ic_truck.png" alt="" />
                    <span style={{ marginLeft: '16px' }}>Mutasi Rekening</span>
                  </div>
                  <img src="/images/ic_arrow_right.png" alt="" />
                  <div className="divider" />
                </div>
              </Link>

              <Link href="/wishlist">
                <div className="c-menu">
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <img src="/images/ic_love.png" alt="" />
                    <span style={{ marginLeft: '16px' }}>Wishlist</span>
                  </div>
                  <img src="/images/ic_arrow_right.png" alt="" />
                  <div className="divider" />
                </div>
              </Link>

            </div>
          </div>
          <div className="w-5/6 xs:w-full sm:w-full">
            <div className="account-content">
              {
                lacak
                  ? (
                    <div className="card-content">
                      <div className="section">
                        <div className="header">
                          <div className="title">Lacak Pesanan</div>
                          <div className="action " style={{ color: '#010101', fontSize: '13pt', cursor: 'pointer' }} onClick={() => setLacak(false)}>kembali</div>
                        </div>
                      </div>

                      <div className="card-transaction" style={{ marginTop: '2rem' }}>
                        <div className="lacak-header">
                          <div className="lacak-sub-header">
                            <div>
                              <div>Status</div>
                              <div><b>{shippingData?.summary.status}</b></div>
                            </div>
                            <div style={{ marginLeft: '1rem' }}>
                              <div>Nomor Resi</div>
                              <div><b>{shippingData?.summary.waybill_number}</b></div>
                            </div>
                            <div style={{
                              marginLeft: '1rem', display: 'flex', alignItems: 'flex-end', color: '#FF8242'
                            }}
                            >
                              <div />
                              {
                                shippingData?.summary.waybill_number ? (
                                  <div className="cursor-pointer" onClick={() => copyToClipboard(shippingData?.summary.waybill_number)}><b>Salin</b></div>
                                ) : (
                                  <div>Resi belum tersedia</div>
                                )
                              }
                            </div>
                          </div>
                          <img src="/images/ic_arrow_up.png" alt="" />
                        </div>
                        <div className="item">
                          <div id="timeline">
                            <div>
                              {
                                shippingData?.manifest.map(v => {
                                  return (
                                    <section className="year">
                                      <h3>
                                        {v.manifest_date}<br></br>
                                        {v.manifest_time}
                                      </h3>
                                      <section>
                                        <ul>
                                          <li>{`${v.city_name} ${v.manifest_description}`}</li>
                                        </ul>
                                      </section>
                                    </section>
                                  )
                                })
                              }
                            </div>
                          </div>
                        </div>
                      </div>

                    </div>
                  )
                  : (
                    <div className="card-content">
                      <div className="section">
                        <div className="header">
                          <div className="title">Pesanan Saya</div>
                        </div>
                      </div>

                      {/* <div className="c-filter-product flex justify-between" style={{ marginTop: '2rem' }}>
                        <div className="c-filter-page flex items-center">
                          Menampilkan
                          <select className="form-filter-page">
                            <option>12</option>
                            <option>24</option>
                            <option>36</option>
                          </select>
                          per halaman
                        </div>
                        <div className="c-filter-page flex items-center">
                          Urutkan
                          <select className="form-filter-page">
                            <option>Tanggal</option>
                          </select>
                        </div>
                      </div> */}
                      <div className="overflow-tab">
                        <div className="transaction-menu-wrapper w-full flex xs:block md:block mt-4 border-b-2 border-gray-500">
                          {
                            sliders.map(v => <SliderItem key={v.name} data={v} cbClick={handleChangeSlider}/>)
                          }
                        </div>
                      </div>       
                      {
                        selectedContent.status === '' || selectedContent.status === STATUS.MENUNGGU_KONFIRMASI ? (
                          tableData.map(k => (
                            <div className="card-transaction" style={{ marginTop: '1.5rem' }}>
                              <div className="header">
                                { moment(k.date).format('DD MMMM YYYY') }
                              </div>
                              <div className="section flex flex-wrap">
                                <div className="w-1/3 xs:w-1/2 sm:w-1/2">
                                  <div><b>Nomor Pembayaran</b></div>
                                  <div>{ k.payment_number }</div>
                                </div>
                                {/* <div className="w-100">
                                  <div>Status Pesanan</div>
                                  <div style={{ color: '#010101' }}><b>{k.status}</b></div>
                                </div> */}
                                <div className="w-1/3 xs:w-1/2 sm:w-1/2">
                                  <div>Total Pembayaran<span className="text-sm text-gray-400"> (sudah termasuk biaya layanan)</span></div>
                                  <div style={{ color: '#FF8242' }}><b>Rp {formatNumber(k.total_paid)}</b></div>
                                </div>
                                <div className="w-1/3 xs:w-full sm:w-full flex xs:flex-wrap sm:flex-wrap justify-end xs:justify-start sm:justify-start order-detail-action">
                                  {/* <div>Total Pesanan</div>
                                  <div style={{ color: '#FF8242' }}><b>Rp {formatNumber(k.total_paid)}</b></div> */}
                                  {k.order_sellers && k.order_sellers[0].status === STATUS.MENUNGGU_KONFIRMASI ? (
                                    <button className={`btn-action btn-primary`} onClick={handleBayarSekarang(k)} style={{ background:"#010101" }}>BAYAR SEKARANG</button>
                                  ) : ""}
                                  <button onClick={handlePrintOrder(k)} className="btn-action btn-primary">PRINT PESANAN</button>
                                  <button className="py-2 px-4 border border-gray-300 rounded hover:bg-gray-300" onClick={handleOpenDetail(k.id)}>Lihat Detail</button>
                                </div>
                              </div>
                              <div className="divider" />
                              <div className={`w-full flex flex-wrap  ${k.isOpen ? '' : 'hidden'}`} style={{ backgroundColor: '#FAFAFA' }}>
                                {/* <div style={{ width: '50%' }}>
                                  <button className="py-2 px-4 border border-gray-300 rounded">Lihat Detail</button>
                                </div> */}
                                
                                {
                                  k.order_sellers ? k.order_sellers.map(v => (
                                    <CardTransactionHistory
                                      data={v}
                                      callbackBeliLagi={() => {}} 
                                      callbackDetail={(data) => setSelectedData(data)} 
                                      callbackLacak={(data) => {setSelectedLacakData(data); copyToClipboardResi(data.airway_bill);}} 
                                      callbackRefund={(data) => setSelectedDataRefund(data)}
                                    />
                                  )) : ""
                                }
                              </div>
                            </div>
                          ))
                        ) : 
                          (
                            selectedContent.status == 'refund' 
                            ? renderRefund()
                          :
                          tableData.map(k => (
                            <div className="card-transaction" style={{ marginTop: '1.5rem' }}>
                              <div className="header">
                                { moment(k.date).format('DD MMMM YYYY') }
                              </div>
                              <div className="section flex flex-wrap">
                                <div className="w-1/3 xs:w-1/2 sm:w-1/2">
                                  <div><b>Nomor Pesanan</b></div>
                                  <div>{ k.invoice_no }</div>
                                  {
                                    k.status == STATUS.DIKIRIM && (
                                      <>
                                        <div className="mt-2"><b>Nomor Resi</b></div>
                                        <div className="cursor-pointer" onClick={() => copyToClipboardResi(k.airway_bill)}>{k.airway_bill}</div>
                                      </>
                                    )
                                  }
                                </div>
                                {/* <div className="w-100">
                                  <div>Status Pesanan</div>
                                  <div style={{ color: '#010101' }}><b>{k.status}</b></div>
                                </div> */}
                                <div className="w-1/3 xs:w-1/2 sm:w-1/2 xs:text-right sm:text-right">
                                  <div>Total Harga</div>
                                  <div style={{ color: '#FF8242' }}><b>Rp {formatNumber(k.total_price - k.total_product_discount + (k.total_shipping ? k.total_shipping : 0))}</b></div>
                                </div>
                                <div className="w-1/3 xs:w-full sm:w-full flex xs:flex-wrap sm:flex-wrap justify-end xs:justify-start sm:justify-start order-detail-action">
                                  {/* <div>Total Pesanan</div>
                                  <div style={{ color: '#FF8242' }}><b>Rp {formatNumber(k.total_paid)}</b></div> */}
                                  <button className={`btn-finish-transaction btn-action btn-primary ${k.status === STATUS.DIKIRIM ? "block" : "hidden"}`} onClick={handleFinishOrder(k)}>SELESAIKAN PESANAN</button>
                                  <button className="btn-action btn-primary btn-tracking" disabled={k.status === STATUS.MENUNGGU_KONFIRMASI || k.status === STATUS.DIPROSES || k.status === STATUS.DIBATALKAN || k.status === STATUS.DIBAYAR} onClick={() => { setSelectedLacakData(k); copyToClipboardResi(k.airway_bill)}}>LACAK PESANAN</button>
                                  <button className="py-2 px-4 border border-gray-300 rounded hover:bg-gray-300" onClick={() => setSelectedData(k)}>Lihat Detail</button>
                                  {
                                    (k.status == STATUS.DIBAYAR  || k.status === STATUS.DIPROSES || k.status === STATUS.DIKIRIM) &&
                                    <button className="py-2 px-4 border border-gray-300 rounded hover:bg-gray-300 text-white" style={{ background: '#FF8242' }} onClick={() => setSelectedDataRefund(k)}>Refund</button>
                                  }
                                </div>
                              </div>
                              <div className="divider" />
                            </div>
                          ))
                          )
                      }
                      {
                      
                      }

                    </div>
                  )
              }

            </div>
          </div>

          <Modal open={modalDetail} onClose={() => setModalDetail(false)}>
            <div className="card-modal" style={{ padding: '16px 26px' }}>
              <div style={{ textAlign: 'right' }}>
                <div style={{ display: 'inline-block' }} onClick={() => setModalDetail(false)} onKeyDown={() => setModalDetail(false)} className="pointer" role="button" tabIndex={0}>
                  <img src="/images/ic_close_red.png" alt="" />
                </div>
              </div>
              <div className="title" style={{ marginTop: '16px' }}>
                Detail Pesanan
              </div>
              <div style={{ marginTop: '24px', display: 'flex' }}>
                <div className="w-100">
                  <div className="detail-order-wrapper">
                    <div className="flex xs:flex-wrap sm:flex-wrap">
                      <div className="td-small w-1/3 xs:w-1/2 sm:w-1/2">Nomor Pesanan : </div>
                      <div className="td-small w-2/3 xs:w-1/2 sm:w-1/2">{selectedData?.invoice_no}</div>
                    </div>
                    <div className="flex xs:flex-wrap sm:flex-wrap">
                      <div className="td-small w-1/3 xs:w-1/2 sm:w-1/2">Status Pesanan :</div>
                      <div className="td-small w-2/3 xs:w-1/2 sm:w-1/2">{selectedData?.status}</div>
                    </div>
                    <div className="flex xs:flex-wrap sm:flex-wrap">
                      <div className="td-small w-1/3 xs:w-1/2 sm:w-1/2">Tanggal Pesan :</div>
                      <div className="td-small w-2/3 xs:w-1/2 sm:w-1/2">{ moment(selectedData?.date).format('DD MMMM YYYY') }</div>
                    </div>
                    <div className="flex xs:flex-wrap sm:flex-wrap">
                      <div className="td-small w-1/3 xs:w-1/2 sm:w-1/2">Alamat :</div>
                      <div className="td-small w-2/3 xs:w-1/2 sm:w-1/2">{ selectedData?.address_detail.address }</div>
                    </div>
                    <div className="flex xs:flex-wrap sm:flex-wrap">
                      <div className="td-small w-1/3 xs:w-1/2 sm:w-1/2">Pembayaran :</div>
                      <div className="td-small w-2/3 xs:w-1/2 sm:w-1/2">{ selectedData?.payment_method }</div>
                    </div>
                    <div className="flex xs:flex-wrap sm:flex-wrap">
                      <div className="td-small w-1/3 xs:w-1/2 sm:w-1/2">Nama Toko :</div>
                      <div className="td-small w-2/3 xs:w-1/2 sm:w-1/2">{ selectedData?.seller_name }</div>
                    </div>
                    <div className="flex xs:flex-wrap sm:flex-wrap">
                      <div className="td-small w-1/3 xs:w-1/2 sm:w-1/2">No. Resi :</div>
                      <div className="td-small w-2/3 xs:w-1/2 sm:w-1/2">{selectedData?.airway_bill}
                        
                        {
                          selectedData?.airway_bill ? (
                            <span className="cursor-pointer ml-4" onClick={() => copyToClipboardResi(selectedData?.airway_bill)} style={{ color: '#FF8242'}}>Salin</span>
                          ) : (
                            <div>Resi belum tersedia</div>
                          )
                        }
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div style={{ marginTop: '3rem' }}>
                <div className="table">
                  <div className="flex xs:flex-wrap sm:flex-wrap xs:hidden sm:hidden table-header">
                    <div className="text-white w-1/4">Nama Produk</div>
                    <div className="text-white w-1/4">Harga</div>
                    <div className="text-white w-1/4">Jumlah</div>
                    <div className="text-white w-1/4">Subtotal</div>
                  </div>
                  <div className="order-product-table">
                    {
                      detailData && detailData.order_items ? detailData.order_items.map(v => (
                        <div className="flex xs:flex-wrap sm:flex-wrap order-detail-item" key={v.product_name}>
                          <div className="td w-1/4 xs:w-1/2 sm:w-1/2">{v.product_name}</div>
                          <div className="td w-1/4 xs:w-1/2 sm:w-1/2" style={{ whiteSpace: 'nowrap' }}>Rp {formatNumber(v.price)}</div>
                          <div className="td w-1/4 xs:w-1/2 sm:w-1/2" style={{ whiteSpace: 'nowrap' }}>Jumlah: {v.quantity}</div>
                          <div className="td w-1/4 xs:w-1/2 sm:w-1/2" style={{ whiteSpace: 'nowrap' }}>Rp {formatNumber(v.subtotal)}</div>
                        </div>
                      )) : ""
                    }
                  </div>
                </div>
                <div className="divider" />
              </div>
              <div style={{ display: 'flex', justifyContent: 'flex-end', marginTop: '1rem' }}>
                <div>
                  <table>
                    <tr>
                      <td className="td-med" style={{ textAlign: 'right' }}>Total Potongan Produk</td>
                      <td className="td-med" style={{ textAlign: 'right' }}>- Rp {formatNumber(selectedData?.total_product_discount)}</td>
                    </tr>
                    <tr>
                      <td className="td-med" style={{ textAlign: 'right' }}>Subtotal</td>
                      <td className="td-med" style={{ textAlign: 'right' }}>Rp {formatNumber(selectedData?.total_price)}</td>
                    </tr>
                    <tr>
                      <td className="td-med" style={{ textAlign: 'right' }}>Pengiriman</td>
                      <td className="td-med" style={{ textAlign: 'right' }}>Rp {formatNumber(selectedData?.total_shipping)}</td>
                    </tr>
                    {/* <tr>
                      <td className="td-med" style={{ textAlign: 'right' }}>Biaya Layanan</td>
                      <td className="td-med" style={{ textAlign: 'right' }}>Rp {}</td>
                    </tr> */}
                    {/* <tr style={{ borderBottom: '1px solid #CCCCCC' }}>
                      <td className="td-med" style={{ textAlign: 'right', borderBottom: '1px solid #CCCCCC' }}>Store Credit</td>
                      <td className="td-med" style={{ textAlign: 'right', borderBottom: '1px solid #CCCCCC' }}>Rp 0</td>
                    </tr> */}
                    <tr>
                      <td className="td-med" style={{ textAlign: 'right' }}><b>Grand Total</b></td>
                      <td className="td-med" style={{ textAlign: 'right', color: '#FF8242' }}>Rp {formatNumber(selectedData?.total_price + (selectedData?.total_shipping ? selectedData?.total_shipping : 0))}</td>
                    </tr>
                  </table>
                </div>
              </div>

              <div className="divider" style={{ margin: '1rem', width: 'auto' }} />
            </div>
          </Modal>

          <Modal open={modalDetailRefund} onClose={() => setModalDetailRefund(false)}>
            <div className="card-modal" style={{ padding: '16px 26px' }}>
              <div style={{ textAlign: 'right' }}>
                <div style={{ display: 'inline-block' }} onClick={() => setModalDetailRefund(false)} onKeyDown={() => setModalDetailRefund(false)} className="pointer" role="button" tabIndex={0}>
                  <img src="/images/ic_close_red.png" alt="" />
                </div>
              </div>
              <div className="title" style={{ marginTop: '16px' }}>
                Refund Pesanan
              </div>
              <div style={{ marginTop: '24px', display: 'flex' }}>
                <div className="w-100">
                  <div className="detail-order-wrapper">
                    <div className="flex xs:flex-wrap sm:flex-wrap">
                      <div className="td-small w-1/3 xs:w-1/2 sm:w-1/2">Nomor Pesanan : </div>
                      <div className="td-small w-2/3 xs:w-1/2 sm:w-1/2">{selectedDataRefund?.invoice_no}</div>
                    </div>
                    <div className="flex xs:flex-wrap sm:flex-wrap">
                      <div className="td-small w-1/3 xs:w-1/2 sm:w-1/2">Status Pesanan :</div>
                      <div className="td-small w-2/3 xs:w-1/2 sm:w-1/2">{selectedDataRefund?.status}</div>
                    </div>
                    <div className="flex xs:flex-wrap sm:flex-wrap">
                      <div className="td-small w-1/3 xs:w-1/2 sm:w-1/2">Tanggal Pesan :</div>
                      <div className="td-small w-2/3 xs:w-1/2 sm:w-1/2">{ moment(selectedDataRefund?.date).format('DD MMMM YYYY') }</div>
                    </div>
                    <div className="flex xs:flex-wrap sm:flex-wrap">
                      <div className="td-small w-1/3 xs:w-1/2 sm:w-1/2">Alamat :</div>
                      <div className="td-small w-2/3 xs:w-1/2 sm:w-1/2">{ selectedDataRefund?.address_detail.address }</div>
                    </div>
                    <div className="flex xs:flex-wrap sm:flex-wrap">
                      <div className="td-small w-1/3 xs:w-1/2 sm:w-1/2">Pembayaran :</div>
                      <div className="td-small w-2/3 xs:w-1/2 sm:w-1/2">{ selectedDataRefund?.payment_method }</div>
                    </div>
                    <div className="flex xs:flex-wrap sm:flex-wrap">
                      <div className="td-small w-1/3 xs:w-1/2 sm:w-1/2">Nama Toko :</div>
                      <div className="td-small w-2/3 xs:w-1/2 sm:w-1/2">{ selectedDataRefund?.seller_name }</div>
                    </div>
                  </div>
                </div>
              </div>
              <div style={{ marginTop: '3rem' }}>
                <div className="table">
                  <div className="flex xs:flex-wrap sm:flex-wrap xs:hidden sm:hidden table-header">
                    <div className="text-white w-1/12">
                      <input type="checkbox" checked={refundCheckAll} onChange={(e) => handleRefundCheckAll()}/>
                    </div>
                    <div className="text-white w-1/4">Nama Produk</div>
                    <div className="text-white w-1/4">Harga</div>
                    <div className="text-white w-1/4">Jumlah</div>
                    <div className="text-white w-1/4">Subtotal</div>
                  </div>
                  <div className="order-product-table">
                    {
                      detailDataRefund?.order_items.map((v, i) => (
                        <div className="flex xs:flex-wrap sm:flex-wrap order-detail-item" key={v.product_name}>
                          <div className="td w-1/12 xs:w-1/2 sm:w-1/2">
                            <input type="checkbox" checked={v.checked} onChange={(e) => handleCheckedRefund(i)}/>
                          </div>
                          <div className="td w-1/4 xs:w-1/2 sm:w-1/2">{v.product_name}</div>
                          <div className="td w-1/4 xs:w-1/2 sm:w-1/2" style={{ whiteSpace: 'nowrap' }}>Rp {formatNumber(v.price)}</div>
                          <div className="td w-1/4 xs:w-1/2 sm:w-1/2" style={{ whiteSpace: 'nowrap' }}>Jumlah: {v.quantity}</div>
                          <div className="td w-1/4 xs:w-1/2 sm:w-1/2" style={{ whiteSpace: 'nowrap' }}>Rp {formatNumber(v.subtotal)}</div>
                        </div>
                      ))
                    }
                  </div>
                </div>
                <div className="divider" />
              </div>

              <div className="flex-wrap" style={{ display: 'flex', justifyContent: 'center', marginTop: '1rem' }}>
                <button className="w-full py-2 px-2 rounded-md bg-white border-text-primary border " onClick={() => setRefundNotes(`Ingin mengubah rincian & membuat pesanan baru`)}>Ingin mengubah rincian & membuat pesanan baru</button>
                <button className="w-full py-2 px-2 rounded-md bg-white border-text-primary border mt-4" onClick={() => setRefundNotes('Lainnya/berubah pikiran')}>Lainnya/berubah pikiran</button>
              </div>

              {
                refundNotes && (
                  <div className="flex-wrap" style={{ display: 'flex', marginTop: '1rem' }}>
                    <div className="self-start">Alasan Refund :</div>
                    <br></br>
                    <textarea className="w-full p-2 border border-gray-400" rows={5} value={refundNotes} onChange={(e) => setRefundNotes(e.target.value)}>
                      
                    </textarea>
                  </div>
                )
              }
              
              {
                refundNotes && (
                  <div className="flex-wrap" style={{ display: 'flex', justifyContent: 'center', marginTop: '1rem' }}>
                    {/* <input type="text" className="w-full p-2 border border-gray-400" value={bank} onChange={(e) => setBank(e.target.value)} placeholder="Bank"></input> */}
                    <Select 
                      className="w-full"
                      placeholder={`Pilih Bank`}
                      options={BankList()}
                      value={bank}
                      onChange={(v) => setBank(v)}
                    />
                  </div>
                )
              }
              {
                refundNotes && (
                  <div className="flex-wrap" style={{ display: 'flex', justifyContent: 'center', marginTop: '1rem' }}>
                    <input type="number" className="w-full p-2 border border-gray-400" value={bankAccount} onChange={(e) => setBankAccount(e.target.value)} placeholder="Nomor Rekening"></input>
                  </div>
                )
              }

              <div className="divider" style={{ margin: '1rem', width: 'auto' }} />

              
              <button
                type="submit"
                className="btn-primary"
                disabled={detailDataRefund?.order_items.filter(v => v.checked).length == 0 || refundNotes == '' || bank == null || bankAccount == ''}
                onClick={() => handleSubmitRefund()}
              >
                REFUND
              </button>
            </div>
          </Modal>

          <Modal open={gosendData} onClose={() => setGosendData(null)}>
            <div className="card-modal" style={{ padding: '16px 26px' }}>
              <div style={{ textAlign: 'right' }}>
                <div style={{ display: 'inline-block' }} onClick={() => setGosendData(null)} onKeyDown={() => setGosendData(null)} className="pointer" role="button" tabIndex={0}>
                  <img src="/images/ic_close_red.png" alt="" />
                </div>
              </div>
              <div className="title" style={{ marginTop: '16px' }}>
                Pelacakan
              </div>
              <div style={{ marginTop: '24px', display: 'flex' }}>
                <div className="w-100">
                  <table>
                    <tr>
                      <td className="td-small">Nomor Pelacakan</td>
                      <td className="td-small">:</td>
                      <td className="td-small">{gosendData?.orderNo}</td>
                    </tr>
                    <tr>
                      <td className="td-small">Status Driver</td>
                      <td className="td-small">:</td>
                      <td className="td-small">{gosendData?.status}</td>
                    </tr>
                    <tr>
                      <td className="td-small">Nama Driver</td>
                      <td className="td-small">:</td>
                      <td className="td-small">{ gosendData?.driverName }</td>
                    </tr>
                    <tr>
                      <td className="td-small">Telepon Driver</td>
                      <td className="td-small">:</td>
                      <td className="td-small">{ gosendData?.driverPhone }</td>
                    </tr>
                    <tr>
                      <td className="td-small">Nomor Kendaraan</td>
                      <td className="td-small">:</td>
                      <td className="td-small">{ gosendData?.vehicleNumber }</td>
                    </tr>
                    <tr>
                      <td className="td-small">Nama Penerima</td>
                      <td className="td-small">:</td>
                      <td className="td-small">{ gosendData?.receiverName }</td>
                    </tr>
                    <tr>
                      <td className="td-small">Alasan Pembatalan</td>
                      <td className="td-small">:</td>
                      <td className="td-small">{ gosendData?.cancelDescription }</td>
                    </tr>
                    <tr>
                      <td className="td-small text-center" colSpan={3}><a target="_blank" href={gosendData?.liveTrackingUrl} className="text-center text-white bg-primary px-2 py-2 rounded">Lihat Live Tracking</a></td>
                    </tr>
                  </table>
                </div>
              </div>
              <div className="divider" style={{ margin: '1rem', width: 'auto' }} />
            </div>
          </Modal>

          
        </div>
      </BaseLayout>
      <style jsx>
        {
          `
        .c {
          width: 100%;
          display: flex;
          padding: 48px 0;
        }

        .c-filter {
          width: 100%;
        }
        .divider {
          width: 100%;
          margin-top: 12px;
          height: 1px;
          background: #E1E1E1;
        }

        .c-menu {
          font-weight: 500;
          font-size: 16px;
          line-height: 17px;
          letter-spacing: 0.05em;
          display: flex;
          justify-content: space-between;
          align-items: center;
          flex-wrap: wrap;
          padding: 10px 0px;
          cursor: pointer
        }

        .active {
          color: #010101;
        }

        .card-content {
          background: #FFFFFF;
          box-shadow: 0px 0px 4px rgba(0, 0, 0, 0.25);
          border-radius: 0px 7px 7px 7px;
          padding: 26px 32px;
        }

        .header {
          display: flex;
          justify-content: space-between;
        }

        .title {
          font-weight: 500;
          font-size: 22px;
          line-height: 21px;
          letter-spacing: 0.05em;
          color: #696969;
        }

        .btn-action {
          width: auto;
          font-size: 16px;
          padding: 10px 15px;
        }

        .btn-table {
          font-size: 16px;
          line-height: 15px;
          letter-spacing: 0.05em;
          color: #696969;
          background: #FFFFFF;
          border: 1px solid #696969;
          box-sizing: border-box;
          border-radius: 5px;
          padding: 12px 18px;
          cursor: pointer;
        }

        .sub-header {
          font-size: 16px;
          line-height: 15px;
          color: #868686;
        }

        .card-pin {
          background: #FFFFFF;
          box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.25);
          border-radius: 7px;
          display: flex;
          padding: 16px 24px;
          align-items: center;
        }

        .card-pin .text{
          font-size: 16px;
          line-height: 15px;
          color: #BDBDBD;
          padding: 0px 16px;
        }

        .btn-action-outline {
          background: #FFFFFF;
          border: 0.5px solid #010101;
          box-sizing: border-box;
          border-radius: 2px;
          font-size: 16px;
          line-height: 15px;
          color: #010101;
          padding: 12px 18px;
          cursor: pointer;
        }

        
          .c-filter-page {
            font-size: 16px;
            color: #696969;
          }
        
          .form-filter-page {
            background: rgba(248, 248, 248, 0.75);
            border: 1px solid #F2F2F2;
            box-sizing: border-box;
            padding: 4px 4px;
            font-size: 16px;
            color: #696969;
            margin: 0px 4px;
            font-family: 'Gotham';
          }
          .card-transaction {
            background: #FFFFFF;
            box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.15);
            border-radius: 7px;
          }
          .card-transaction .header {
            background: #FAFAFA;
            border-radius: 7px 7px 0px 0px;
            font-size: 16px;
            color: #696969;
            padding: 1rem;
          }
          .card-transaction .section {
            display: flex;
            font-size: 16px;
            color: #696969;
            padding: 1.5rem 2rem;
          }
          .td-small {
            padding: 0.2rem 1rem;
            border: 0px;
          }
          .td-med {
            padding: 0.5rem 1rem;
            border: 0px;
          }

          thead {
            background: #E8E8E8;
            border-radius: 7px;
          }
          .table-header div{
            background:#010101;
            padding:10px 15px;
          }

          th {
            padding: 1rem 1rem;
          }

          .lacak-header {
            display: flex;
            justify-content: space-between;
            align-items: center;
            padding: 1rem 1.5rem;
            background: #FFFFFF;
            box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.15);
            border-radius: 7px;
          }

          .lacak-sub-header {
            display: flex;
            font-size: 16px;
            line-height: 19px;
            letter-spacing: 0.05em;
            color: #696969;
          }
          .account-content{
            padding-left: 24px;
          }
          .order-detail-action .btn-action{
            background: #FF8242;
          }
          .order-detail-action button{
            margin-left: 5px;
          }
          .order-detail-action .btn-finish-transaction{
            background: #2FC142;
          }
          .btn-tracking {
            background-color: #3FAE5E;
          }
          .btn-tracking:disabled {
            background-color: #C4C4C4 !important;
          }
          @media (min-width: 320px) and (max-width: 767px) {
            .c{
              padding: 0 20px;
              margin-bottom: 30px;
            }
            .c-filter{
              margin-bottom:30px;
            }
            .account-content{
              padding-left: 0;
            }
            .order-detail-action .btn-action{
              margin-left:0;
              padding:10px 5px;
              margin-right:5px;
              width:100%;
            }
            .order-detail-action button{
              margin-top:15px;
              width:100%;
            }
            .transaction-menu-wrapper{
              width: auto;
              min-width: 90vh;
            }
            .overflow-tab{
              width:100%;
              overflow-x: auto;
              overflow: -moz-scrollbars-none;
              -ms-overflow-style: none;
            }
            .overflow-tab::-webkit-scrollbar {
              width: 0px;
              background: transparent;
            }
            .card-transaction .section{
              font-size:14px;
              padding:1rem;
            }
            .order-detail-item{
              border-top:1px solid #eee;
            }
            .td{
              padding:10px;
            }
            .td-small{
              padding:0.2rem 10px;
            }
            .{
              height:80px;
              overflow-y:auto;
            }
          }
        `
        }
      </style>
    </div>
  );
};

index.getInitialProps = async (ctx) => {
  const { token } = ctx.store.getState().authentication;

  const productTypeResp = await getProductType();
  const productType = productTypeResp.data;

  const cartResp = await getCart(token);
  const cart = cartResp.data ? cartResp.data.cart : [];

  return { 
    productType,
    cart,
  };
};

export default connect(
  (state) => state, {}
)(index);
