import React, { useState } from 'react'
import Link from 'next/link'
import Select from 'react-select/async'
import BaseLayout from 'components/BaseLayout'
import Modal from 'components/Modal'
import { getAddresses, insertAddress, updateAddress, deleteAddress } from 'data/api/address'
import { apiSearchAddress } from 'util/ServiceHelper'
import MapPicker from 'components/MapPicker'
import { getProductType } from 'data/api/product-type'
import { getCart } from 'data/api/cart';

// Toastr Notification
import {successNotification, errorNotification} from 'util/toastr-notif';
import { getHistory, getUser } from 'data/api/user'
import moment from 'moment'
import { formatNumber } from 'util/helper'

const index = ({ addresses, token, productType, cart, history, user }) => {
  const [tableData, setTableData] = useState(addresses)
  const [modalAdd, setModalAdd] = useState(false)
  const [modalEdit, setModalEdit] = useState(false)
  const [modalDelete, setModalDelete] = useState(false)
  const [map, setMap] = useState(false)
  const [coordinat, setCoordinat] = useState(null)

  const [form, setForm] = useState({
    id: null,
    addressName: '',
    name: '',
    phone: '',
    address: '',
    districtId: null,
    postcode: '',
  });

  const [formUpdate, setFormUpdate] = useState({
    id: null,
    addressName: '',
    name: '',
    phone: '',
    address: '',
    districtId: null,
    postcode: '',
  });

  const doGetAddresses = async () => {
    const res = await getAddresses(token);
    setTableData(res.data ? res.data : [])
  }

  const handleInsertAddress = async (e) => {
    e.preventDefault();
    if(!form.districtId){
      errorNotification("Mohon isi kota / kecamatan");
    } else {
      const body = {
        district_id: form.districtId.value,
        address_name: form.addressName,
        name: form.name,
        phone: form.phone,
        address: form.address,
        lat_long: coordinat ? `${coordinat.lat},${coordinat.lng}` : `0,0`,
        postal_code: form.postcode
      };
  
      const res = await insertAddress(token, body);
  
      if (res.code === 201) {
        doGetAddresses();
        setModalAdd(false);
      }
    }
  };

  const openUpdateModal = (data) => async () => {
    setModalEdit(true);
    setFormUpdate({
      id: data.id,
      addressName: data.address_name,
      name: data.name,
      phone: data.phone,
      address: data.address,
      districtId: {
        label: data.province + ", " + data.city + ", " + data.district,
        value: data.district_id
      },
      postcode: data.postal_code,
    });

    
    const latLng = data.lat_long.split(',')
    setCoordinat(latLng.length > 0 ? {
      lat: latLng[0],
      lng: latLng[1],
    } : null)
  }

  const handleUpdateAddress = async (e) => {
    e.preventDefault();
    if(!formUpdate.districtId){
      errorNotification("Mohon isi kota / kecamatan")
    } else {
      const body = {
        id: formUpdate.id,
        district_id: formUpdate.districtId.value,
        address_name: formUpdate.addressName,
        name: formUpdate.name,
        phone: formUpdate.phone,
        address: formUpdate.address,
        lat_long: coordinat ? `${coordinat.lat},${coordinat.lng}` : `0,0`,
        postal_code: formUpdate.postcode
      };
  
      const res = await updateAddress(token, body);
  
      if (res.code === 200) {
        doGetAddresses();
        setModalEdit(false);
      }
    }
  }

  const handleDeleteAddress = async () => {
    const res = await deleteAddress(token, { id: form.id });

    if (res.code === 200) {
      doGetAddresses();
      setModalDelete(false);
    }
  };

  const loadOptions = (inputValue, callback) => {
    if (inputValue.length > 3) {
      setTimeout(() => {
        apiSearchAddress(inputValue)
          .then((res) => {
            callback(res.data.map((v) => ({
              label: v.result,
              value: v.id_district
            })));
          }).catch(() => callback([]));
      }, 500);
    } else {
      callback([]);
    }
  };

  const handleChangeForm = (name) => (e) => {
    if(name === "phone" || name === "postcode"){
      const re = /^[0-9\b]+$/;
      if (e.target.value === '' || re.test(e.target.value)) {
        const { value } = e.target;
        setForm({ ...form, [name]: value });
      }
    } else {
      const { value } = e.target;
      setForm({ ...form, [name]: value });
    }
  };

  const handleChangeFormUpdate = (name) => (e) => {
    if(name === "phone" || name === "postcode"){
      const re = /^[0-9\b]+$/;
      if (e.target.value === '' || re.test(e.target.value)) {
        const { value } = e.target;
        setFormUpdate({ ...formUpdate, [name]: value });
      }
    } else {
      const { value } = e.target;
      setFormUpdate({ ...formUpdate, [name]: value });
    }
  };

  const handleSubmitMap = (coor) => {
    setMap(false)
    setCoordinat(coor)
  }

  return (
    <div>
      <BaseLayout productType={productType} token={token} cart={cart}>
        <div className="c flex flex-wrap">
          <div className="w-1/6 xs:w-full sm:w-full">
            <div className="c-filter">
              <Link href="/account">
                <div className="c-menu">
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <img src="/images/ic_account.png" alt="" />
                    <span style={{ marginLeft: '16px' }}>Detail Akun</span>
                  </div>
                  <img src="/images/ic_arrow_right.png" alt="" />
                  <div className="divider" />
                </div>
              </Link>

              <Link href="/address">
                <div className="c-menu">
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <img src="/images/ic_map.png" alt="" />
                    <span style={{ marginLeft: '16px' }}>Daftar Alamat</span>
                  </div>
                  <img src="/images/ic_arrow_right.png" alt="" />
                  <div className="divider" />
                </div>
              </Link>
              <Link href="/transaction-history">
                <div className="c-menu">
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <img src="/images/ic_truck.png" alt="" />
                    <span style={{ marginLeft: '16px' }}>Pesanan Saya</span>
                  </div>
                  <img src="/images/ic_arrow_right.png" alt="" />
                  <div className="divider" />
                </div>
              </Link>

              <Link href="/history">
                <div className="c-menu active">
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <img src="/images/ic_truck_red.png" alt="" />
                    <span style={{ marginLeft: '16px' }}>Mutasi Rekening</span>
                  </div>
                  <img src="/images/ic_arrow_right_red.png" alt="" />
                  <div className="divider" />
                </div>
              </Link>

              <Link href="/wishlist">
                <div className="c-menu">
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <img src="/images/ic_love.png" alt="" />
                    <span style={{ marginLeft: '16px' }}>Wishlist</span>
                  </div>
                  <img src="/images/ic_arrow_right.png" alt="" />
                  <div className="divider" />
                </div>
              </Link>

            </div>
          </div>
          <div className="w-5/6 xs:w-full sm:w-full">
            <div className="w-100 account-content">
              <div className="card-content">
                <div className="section">
                  <div className="header">
                    <div className="title">History</div>
                  </div>
                  {/* <div>
                    <button className="btn-action btn-primary" type="button" style={{ marginTop: '24px' }} onClick={() => setModalAdd(true)}>
                      + TAMBAH ALAMAT BARU
                    </button>
                  </div> */}
                  {/* <div className="address-wrapper" style={{ marginTop: '16px' }}>
                    <div className="w-full">
                      <div className="flex xs:flex-wrap sm:flex-wrap address-heading items-center xs:hidden sm:hidden">
                        <div className="address-icon-header"></div>
                        <div className="w-1/4">Penerima</div>
                        <div className="w-1/4">Alamat Pengiriman</div>
                        <div className="w-1/4">Daerah Pengiriman</div>
                        <div className="w-1/4"></div>
                      </div>
                      <div className="w-full address-body">
                        {
                          tableData.map((v) => (
                            <div className="flex xs:flex-wrap sm:flex-wrap address-row items-center">
                              <div className="address-icon block xs:hidden sm:hidden">
                                <img src="/images/ic_check_red.png" alt="" />
                              </div>
                              <div className="w-1/4 xs:w-full sm:w-full">
                                <b>{v.name}</b>
                                <br />
                                <div style={{ marginTop: '12px' }}>
                                  {v.phone}
                                </div>
                              </div>
                              <div className="w-1/4 xs:w-full sm:w-full">
                                <b>{v.address_name}</b>
                                <br />
                                <div style={{ marginTop: '12px' }}>
                                  {v.address}
                                </div>
                              </div>
                              <div className="w-1/4 xs:w-full sm:w-full">
                                {`${v.province}, ${v.city}, ${v.district}, ${v.postal_code} ${v.country}`}
                              </div>
                              <div className="w-1/4 xs:w-full sm:w-full">
                                <button className="btn-table mr-2" onClick={openUpdateModal(v)}>UBAH</button>
                                <button
                                  className="btn-table"
                                  onClick={() => {
                                    setForm({ ...form, id: v.id });
                                    setModalDelete(true);
                                  }}
                                >
  HAPUS
                                </button>
                              </div>
                            </div>
                          ))
                        }
                      </div>
                    </div>
                  </div> */}

                  <div className="w-full bg-primary text-white mt-4 px-4 py-3">
                    <div className="w-full flex justify-between">
                      <div>No Rekening : </div>
                      <div>{ user.ussi_account_number }</div>
                    </div>
                    <div className="w-full flex justify-between">
                      <div>Tanggal Inquiry : </div>
                      <div>{ moment().format('DD/MM/YYYY HH:mm:ss') }</div>
                    </div>
                    <div className="w-full flex justify-between">
                      <div>Jenis Transaksi : </div>
                      <div>Semua</div>
                    </div>
                  </div>

                  {
                    history.map(v => (
                      <div className="w-full mt-4 px-4 py-3">
                        <div className="w-full flex justify-between">
                          <div className="w-full">{ v.tgl_trans}</div>
                          <div className={`w-full text-right ${v.debit === 0 ? 'text-blue-700' : 'text-red-700'}`}>Rp. { formatNumber(v.debit === 0 ? v.kredit : v.debit) }</div>
                        </div>
                        <div className="w-full flex justify-between">
                          <div className="w--full">{ v.deskripsi }</div>
                          <div className={`w-full text-right ${v.debit === 0 ? 'text-blue-700' : 'text-red-700'}`}>{ v.debit === 0 ? 'DB' : 'CR'}</div>
                        </div>
                      </div>
                    ))
                  }
                </div>

              </div>
            </div>
          </div>

          <Modal open={modalAdd} onClose={() => setModalAdd(false)}>
            {
              map
              ?
                <MapPicker 
                  onClose={() => setMap(false)}
                  onSubmit={(coor) => handleSubmitMap(coor)}
                  defaultCenter={coordinat ? {
                    lat: coordinat.lat,
                    lng: coordinat.lng
                  } : null}
                  reverseGeolocation={form.districtId ? form.districtId.label : null}
                /> 
              :
              <div className="card-modal" style={{ padding: '16px 26px' }}>
                <div style={{ textAlign: 'right' }}>
                  <div style={{ display: 'inline-block' }} onClick={() => setModalAdd(false)} onKeyDown={() => setModalAdd(false)} className="pointer" role="button" tabIndex={0}>
                    <img src="/images/ic_close_red.png" alt="" />
                  </div>
                </div>
                <div className="overflow-mobile">
                  <div className="title" style={{ marginTop: '16px' }}>
                    Tambah Alamat Baru
                  </div>
                  <form onSubmit={handleInsertAddress}>
                    <div style={{ marginTop: '24px' }}>
                      <div className="form-control w-full">
                        <input className="w-full" placeholder="Nama Alamat" style={{ width: '100%' }} value={form.addressName} onChange={handleChangeForm('addressName')} required />
                      </div>
                      <div className="form-helper">Contoh: Alamat Rumah, Kantor, Apartemen, Dropship</div>
                    </div>
                    <div style={{ marginTop: '24px' }}>
                      <div className="xs:flex-wrap" style={{ width: '100%', display: 'flex' }}>
                        <div style={{ width: '100%' }}>
                          <div className="form-control w-100">
                            <input className="w-100 h-full" placeholder="Nama Penerima" value={form.name} onChange={handleChangeForm('name')} required />
                          </div>
                        </div>
                        <div className="ml-3 xs:ml-0 xs:mt-4" style={{ width: '100%' }}>
                          <div className="form-control w-100">
                            <input className="w-100 h-full" placeholder="Nomor HP" pattern="[0-9]{5,}" value={form.phone} onChange={handleChangeForm('phone')} required />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div style={{ marginTop: '24px' }}>
                      <div className="xs:flex-wrap" style={{ width: '100%', display: 'flex' }}>
                        <div style={{ width: '100%' }}>
                          <Select
                            styles={{
                              control: (provided) => ({
                                ...provided,
                                background: '#FFFFFF',
                                boxShadow: '0px 1px 4px rgba(0, 0, 0, 0.25)',
                                borderRadius: '7px',
                                padding: '20px 18px',
                                border: '0px'
                              }),
                              valueContainer: (provided) => ({
                                ...provided,
                                padding: '0px'
                              }),
                              menu: (provided) => ({
                                ...provided,
                                padding: 'px'
                              }),
                              indicatorsContainer: (provider) => ({
                                ...provider,
                                display: 'none'
                              })
                            }}
                            placeholder="Kota / Kecamatan"
                            loadOptions={loadOptions}
                            cacheOptions
                            defaultOptions
                            value={form.districtId}
                            onChange={(value) => setForm({ ...form, districtId: value })}
                          />
                        </div>
                        <div className="ml-3 w-1/3 xs:ml-0 xs:mt-4 xs:w-full">
                          <div className="form-control w-100">
                            <input className="w-100 h-full" placeholder="Kode Pos" value={form.postcode} onChange={handleChangeForm('postcode')} required />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div style={{ marginTop: '24px' }}>
                      <div className="form-control w-full">
                        <textarea rows={4} className="w-full" placeholder="Alamat" style={{ width: '100%' }} value={form.address} onChange={handleChangeForm('address')} required />
                      </div>
                    </div>
                    <div style={{ marginTop: '24px' }}>
                      <span className="subheader text-text-primary">Pin Alamat</span>
                    </div>
                    <div style={{ marginTop: '10px' }}>
                      <div className="card-pin xs:flex-wrap xs:items-center">
                        <div className="text w-full flex items-center">
                          <img className="w-6 mr-4" src="/images/ic_map_red.png" alt="" />
                          <span>Tandai lokasi dalam peta jika anda mengirim dengan layanan Pick Up (seperti Go Send, Grab Express)</span>
                        </div>
                        <div className="btn-action-outline rounded xs:w-full xs:mt-4 text-center" onClick={() => setMap(true)}>
                          Tandai Lokasi
                        </div>
                      </div>
                    </div>
                    <div style={{ marginTop: '24px' }}>
                      <button type="submit" className="btn-primary">SIMPAN</button>
                    </div>
                  </form>
                </div>
                <div className="divider my-8"/>
              </div>
            }
            </Modal>

          <Modal open={modalEdit} onClose={() => setModalEdit(false)}>
            {
              map
              ?
                <MapPicker 
                  onClose={() => setMap(false)}
                  onSubmit={(coor) => handleSubmitMap(coor)}
                  defaultCenter={coordinat ? {
                    lat: coordinat.lat,
                    lng: coordinat.lng
                  } : null}
                  reverseGeolocation={formUpdate.districtId ? formUpdate.districtId.label : null}
                /> 
              :
              <div className="card-modal" style={{ padding: '16px 26px' }}>
                <div style={{ textAlign: 'right' }}>
                  <div style={{ display: 'inline-block' }} onClick={() => setModalEdit(false)} onKeyDown={() => setModalEdit(false)} className="pointer" role="button" tabIndex={0}>
                    <img src="/images/ic_close_red.png" alt="" />
                  </div>
                </div>
                <div className="overflow-mobile">
                  <div className="title" style={{ marginTop: '16px' }}>
                    Ubah Alamat
                  </div>
                  <form onSubmit={handleUpdateAddress}>
                    <div style={{ marginTop: '24px' }}>
                      <div className="form-control w-full">
                        <input className="w-full" placeholder="Nama Alamat" style={{ width: '100%' }} value={formUpdate.addressName} onChange={handleChangeFormUpdate('addressName')} />
                      </div>
                      <div className="form-helper">Contoh: Alamat Rumah, Kantor, Apartemen, Dropship</div>
                    </div>
                    <div style={{ marginTop: '24px' }}>
                      <div className="xs:flex-wrap" style={{ width: '100%', display: 'flex' }}>
                        <div style={{ width: '100%' }}>
                          <div className="form-control w-100">
                          <input className="w-100" placeholder="Nama Penerima" value={formUpdate.name} onChange={handleChangeFormUpdate('name')} />
                          </div>
                        </div>
                        <div className="ml-3 xs:ml-0 xs:mt-4" style={{ width: '100%' }}>
                          <div className="form-control w-100">
                            <input className="w-100" placeholder="Nomor HP" value={formUpdate.phone} onChange={handleChangeFormUpdate('phone')} />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div style={{ marginTop: '24px' }}>
                      <div className="xs:flex-wrap" style={{ width: '100%', display: 'flex' }}>
                        <div style={{ width: '100%' }}>
                        <Select
                              styles={{
                                control: (provided) => ({
                                  ...provided,
                                  background: '#FFFFFF',
                                  boxShadow: '0px 1px 4px rgba(0, 0, 0, 0.25)',
                                  borderRadius: '7px',
                                  padding: '20px 18px',
                                  border: '0px'
                                }),
                                valueContainer: (provided) => ({
                                  ...provided,
                                  padding: '0px'
                                }),
                                menu: (provided) => ({
                                  ...provided,
                                  padding: 'px'
                                }),
                                indicatorsContainer: (provider) => ({
                                  ...provider,
                                  display: 'none'
                                })
                              }}
                              placeholder="Kota / Kecamatan"
                              loadOptions={loadOptions}
                              cacheOptions
                              defaultOptions={[{...formUpdate.districtId}]}
                              value={formUpdate.districtId ? formUpdate.districtId : 0}
                              onChange={(value) => setFormUpdate({ ...formUpdate, districtId: value })}
                            />
                        </div>
                        <div className="ml-3 w-1/3 xs:ml-0 xs:mt-4 xs:w-full">
                          <div className="form-control w-100">
                            <input className="w-100 h-full" placeholder="Kode Pos" value={formUpdate.postcode} onChange={handleChangeFormUpdate('postcode')} />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div style={{ marginTop: '24px' }}>
                      <div className="form-control w-100">
                        <textarea rows={4} className="w-100" placeholder="Alamat" style={{ width: '100%' }} value={formUpdate.address} onChange={handleChangeFormUpdate('address')} />
                      </div>
                    </div>
                    <div style={{ marginTop: '24px' }}>
                      <span className="subheader">Pin Alamat (Opsional)</span>
                    </div>
                    <div style={{ marginTop: '10px' }}>
                      <div className="card-pin xs:flex-wrap xs:items-center">
                        <div className="text w-full flex items-center">
                          <img className="w-6 mr-4" src="/images/ic_map_red.png" alt="" />
                          <span>Tandai lokasi dalam peta jika anda mengirim dengan layanan Pick Up (seperti Go Send, Grab Express)</span>
                        </div>
                        <div className="btn-action-outline rounded xs:w-full xs:mt-4 text-center" onClick={() => setMap(true)}>
                          Tandai Lokasi
                        </div>
                      </div>
                    </div>
                    <div style={{ marginTop: '24px' }}>
                      <button type="submit" className="btn-primary">SIMPAN</button>
                    </div>
                  </form>
                </div>
                <div className="divider my-8"/>
              </div>
            }
          </Modal>

          <Modal open={modalDelete} onClose={() => setModalDelete(false)}>
            <div className="card-modal" style={{ padding: '16px 26px' }}>
              <div style={{ textAlign: 'right' }}>
                <div style={{ display: 'inline-block' }} onClick={() => setModalDelete(false)} onKeyDown={() => setModalAdd(false)} className="pointer" role="button" tabIndex={0}>
                  <img src="/images/ic_close_red.png" alt="" />
                </div>
              </div>
              <div className="title" style={{ marginTop: '16px' }}>
                Apakah Anda yakin mau
                <br />
                menghapus data ini?
              </div>
              <div style={{ marginTop: '24px', display: 'flex' }}>
                <button className="btn-primary" style={{ fontSize: '18px' }}>BATALKAN</button>
                <button className="btn-secondary" style={{ marginLeft: '4px', fontSize: '18px' }} onClick={handleDeleteAddress}>HAPUS</button>
              </div>
              <div className="divider" style={{ margin: '24px' }} />
            </div>
          </Modal>

        </div>
      </BaseLayout>
      <style jsx>
        {
          `
        .c {
          width: 100%;
          display: flex;
          padding: 48px 0;
        }

        .c-filter {
          width: 100%;
        }
        .divider {
          width: 100%;
          margin-top: 12px;
          height: 1px;
          background: #E1E1E1;
        }

        .c-menu {
          font-weight: 500;
          font-size: 16px;
          line-height: 17px;
          letter-spacing: 0.05em;
          display: flex;
          justify-content: space-between;
          align-items: center;
          flex-wrap: wrap;
          padding: 10px 0px;
          cursor: pointer;
        }

        .active {
          color: #010101;
        }

        .card-content {
          background: #FFFFFF;
          box-shadow: 0px 0px 4px rgba(0, 0, 0, 0.25);
          border-radius: 0px 7px 7px 7px;
          padding: 26px 32px;
        }

        .header {
          display: flex;
          justify-content: space-between;
        }

        .title {
          font-weight: 500;
          font-size: 22px;
          line-height: 21px;
          letter-spacing: 0.05em;
          color: #696969;
        }

        .btn-action {
          width: auto;
          font-size: 16px;
          padding: 14px 24px;
        }

        .btn-table {
          font-size: 14px;
          line-height: 13px;
          letter-spacing: 0.05em;
          color: #696969;
          background: #FFFFFF;
          border: 1px solid #696969;
          box-sizing: border-box;
          border-radius: 5px;
          padding: 12px 18px;
          cursor: pointer;
        }

        .sub-header {
          font-size: 16px;
          line-height: 15px;
          color: #868686;
        }

        .card-pin {
          background: #FFFFFF;
          box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.25);
          border-radius: 7px;
          display: flex;
          padding: 16px 24px;
          align-items: center;
        }

        .card-pin .text{
          font-size: 16px;
          line-height: 15px;
          color: #BDBDBD;
          padding: 0px 16px;
        }

        .btn-action-outline {
          background: #FFFFFF;
          border: 0.5px solid #010101;
          box-sizing: border-box;
          border-radius: 2px;
          font-size: 16px;
          line-height: 15px;
          color: #010101;
          padding: 12px 18px;
          cursor: pointer;
        }
        .account-content{
          padding-left:24px;
        }
        .address-heading{
          background:#010101;
          color:#fff;
          padding:10px 15px;
        }
        .address-row > div{
          padding:10px 15px;
        }
        .address-heading > div{
          padding: 5px 10px;
          font-weight:900;
          font-size:18px;
        }
        .address-icon img{
          width:30px;
        }
        .address-icon-header{
          width:50px;
        }
        .address-row{
          margin-top:10px;
        }

        .overflow-mobile{
          max-height: 560px;
          overflow-y: auto;
        }
        @media (min-width: 320px) and (max-width: 767px) {
          .c{
            padding: 0 20px;
            margin-bottom: 30px;
          }
          .c-filter{
            margin-bottom:30px;
          }
          .account-content{
            padding-left: 0;
          }
          .c-c .text-key,
          .c-c .text-value{
            width:50%;
          }
          .form-control{
            width:100%;
          }
          .address-row{
            margin-top:0;
            border-bottom:1px solid #e0e0e0;
            padding: 10px 0;
          }
          .address-row:last-child{
            border:none;
          }
          .overflow-mobile{
            max-height: 560px;
            overflow-y: auto;
            padding:0 10px;
          }
          .btn-table{
            margin-right: 8px;
          }
        }
        @media (min-width: 767px) and (max-width: 1375px){
          .flex-wrap{
            padding:48px 50px;
          }
        }
        `
        }
      </style>
    </div>
  );
};

index.getInitialProps = async (ctx) => {
  const { token } = ctx.store.getState().authentication;
  const addresses = await getAddresses(token);

  const productTypeResp = await getProductType();
  const productType = productTypeResp.data;

  const cartResp = await getCart(token);
  const cart = cartResp.data ? cartResp.data.cart : [];

  const historyResp = await getHistory(token);
  const history = historyResp.data;

  const userResp = await getUser(token);
  const user = userResp.data;

  return {
    addresses: addresses.data ? addresses.data : [],
    token,
    productType,
    cart,
    history,
    user
  };
};

export default index;
