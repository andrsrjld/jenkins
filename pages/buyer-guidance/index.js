import React from 'react';
import BaseLayout from '~/components/BaseLayout';
import { getProductType } from '~/data/api/product-type';
import { getCart } from '~/data/api/cart';

const index = ({productType, token, cart}) => (
  <div>
    <BaseLayout productType={productType} token={token} cart={cart}>
    <div id="breadcumb" className="breadcrumb-wrapper">
        <div className="breadcumb">
            Home &#x0226B;
          {' '}
          <span style={{ color: '#010101' }}>Panduan Pembeli</span>
        </div>
      </div>
      <div className="c-wrapper">
        <div className="title">
          Panduan
          {' '}
          <span style={{ color: '#010101' }}>Pembeli</span>
        </div>
        <div className="c">
          <div className="text-center header w-100" style={{ marginTop: '48px' }}>
            4 Langkah mudah daftar menjadi pembeli di bapera.id
          </div>

          <div className="card xs:flex-wrap sm:flex-wrap" style={{ marginTop: '48px' }}>
            <div className="circle-wrapper" style={{ position: 'relative' }}>
              <span className="text-round">1</span>
            </div>
            <div className="w-full circle-desc" style={{
              dispaly: 'flex', flexDirection: 'row', flexWrap: 'wrap'
            }}
            >
              <div className="title">Selamat datang di bapera.id</div>
              <div className="desc">Klik icon user untuk masuk atau membuat akun baru.</div>
            </div>
          </div>

          <div className="card xs:flex-wrap sm:flex-wrap" style={{ marginTop: '24px' }}>
            <div className="circle-wrapper" style={{ position: 'relative' }}>
              <span className="text-round">2</span>
            </div>
            <div className="w-full circle-desc" style={{
              dispaly: 'flex', flexDirection: 'row', flexWrap: 'wrap'
            }}
            >
              <div className="title">Masuk / Daftar Akun Baru</div>
              <div className="desc">Jika sudah memiliki akun dapat langsung menulis email dan password lalu klik masuk. Jika belum punya akun klik buat akun baru.</div>
            </div>
          </div>

          <div className="card xs:flex-wrap sm:flex-wrap" style={{ marginTop: '24px' }}>
            <div className="circle-wrapper" style={{ position: 'relative' }}>
              <span className="text-round">3</span>
            </div>
            <div className="w-full circle-desc" style={{
              dispaly: 'flex', flexDirection: 'row', flexWrap: 'wrap'
            }}
            >
              <div className="title">Pembeli</div>
              <div className="desc">Isi semua form login atau form data diri kemudian klik tombol Buat akun baru.</div>
            </div>
          </div>

          <div className="card xs:flex-wrap sm:flex-wrap" style={{ marginTop: '24px' }}>
            <div className="circle-wrapper" style={{ position: 'relative' }}>
              <span className="text-round">4</span>
            </div>
            <div className="w-full circle-desc" style={{
              dispaly: 'flex', flexDirection: 'row', flexWrap: 'wrap'
            }}
            >
              <div className="title">Selesai Mendaftar</div>
              <div className="desc">Verifikasi akun Anda dengan mengklik tombol verifikasi di email. Jangan lupa untuk mengisi Alamat Pengiriman pada halaman Akun Saya.</div>
            </div>
          </div>

        </div>
      </div>
    </BaseLayout>
    <style jsx>
      {`
        .breadcrumb-wrapper{
          padding: 20px 0;
        }
        .c-wrapper{
          padding: 20px 0 80px 0;
        }
        .card .title{
          margin-bottom:15px;
        }
        .c {
          display: flex;
          flex-wrap: wrap;
          justify-content: center
        }

        .title {
          font-weight: bold;
          font-size: 20px;
          line-height: 23px;
          letter-spacing: 0.05em;
          color: #696969;
        }

        .header {
          font-weight: bold;
          font-size: 20px;
          line-height: 23px;
          letter-spacing: 0.05em;
          color: #696969;
        }

        .card {
          width: 60%;
          display: flex;
          background: #FFFFFF;
          box-shadow: 0px 1px 7px rgba(0, 0, 0, 0.25);
          border-radius: 5px;
          padding: 48px 58px;
          align-items: center;
        }

        .card .title {
          font-weight: bold;
          font-size: 18px;
          line-height: 21px;
          letter-spacing: 0.05em;
          color: #696969;
        }

        .card .desc {
          font-weight: normal;
          font-size: 16px;
          line-height: 21px;
          letter-spacing: 0.05em;
          color: #696969;
        }

        .text-round {
          position: relative;
          font-weight: bold;
          font-size: 36px;
          line-height: 95px;
          color: #FFFFFF;
        }
        .card img{
          width:auto;
          max-width:inherit;
        }
        .circle-wrapper{
          background:#010101;
          border-radius:50%;
          width:120px;
          height:100px;
          text-align:center;
        }
        .circle-desc{
          margin-left:24px;
        }
        @media (min-width: 320px) and (max-width: 767px) {
          .c-wrapper{
            padding: 0 20px;
            margin-bottom: 30px;
          }
          .card{
            width: 90%;
            padding:30px;
          }
          .breadcrumb-wrapper{
            padding:0 20px;
            margin-bottom:20px;
            text-align:center;
          }
          .title{
            text-align:center;
          }
          .circle-wrapper{
            margin: 0 auto 15px;
            display:block;
            width:100px;
          }
          .desc{
            text-align:center;
          }
          .circle-desc{
            margin-left:0;
          }
        }
        `
      }
    </style>
  </div>
);

index.getInitialProps = async (ctx) => {
  const { token } = ctx.store.getState().authentication;

  const productTypeResp = await getProductType();
  const productType = productTypeResp.data;

  const cartResp = await getCart(token);
  const cart = cartResp.data ? cartResp.data.cart : [];

  return { 
    productType,
    token,
    cart
  };
};

export default index;
