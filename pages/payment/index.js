import React, { useState } from 'react';
import Link from 'next/link';
import BaseLayout from '~/components/BaseLayout';
import Modal from '~/components/Modal';

const index = () => {
  const [modalAddress, setModalAddress] = useState(false);
  const [modalCourir, setModalCourir] = useState(false);

  return (
    <div>
      <BaseLayout>
        <div id="breadcumb" style={{ padding: '20px 70px' }}>
          <div className="breadcumb">
            Home &#x0226B;
            {' '}
            Keranjang &#x0226B;
            {' '}
            Shipping &#x0226B;
            {' '}
            <span style={{ color: '#010101' }}>Review & Payment</span>
          </div>

          <div className="c" style={{ marginTop: '48px' }}>

            <div className="c-content">
              <div className="c-1">
                <div className="title">
              Alat Pengiriman
                  {' '}
                  <span style={{ color: '#010101' }}>Belanja</span>
                </div>

                <div className="card" style={{ marginTop: '24px' }}>
                  <table>
                    <thead>
                      <th />
                      <th>Penerima</th>
                      <th>Alamat Pengirim</th>
                      <th>Daerah Pengiriman</th>
                    </thead>
                    <tbody>
                      <tr>
                        <td style={{ padding: '0px 0px 0px 1rem' }}>
                          <img src="/images/ic_check_red.png" alt="" />
                        </td>
                        <td style={{ minWidth: '250px' }}>
                          <b>Lalalisa Hathway</b>
                          <br />
                          <div style={{ marginTop: '1rem' }}>
                            0812134234
                          </div>
                        </td>
                        <td>
                          <b>Rumah</b>
                          <br />
                          <div style={{ marginTop: '1rem', maxWidth: '450px' }}>
                            Jalan Cipaganti No 8 RT/RW 01/ 02 Kel/Desa Setiasari Bandung
                          </div>
                        </td>
                        <td>
                      Jawa Barat, Kota Bandung, Bandung Tengah, 40521 Indonesia
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>

                <div style={{ marginTop: '1rem' }}>
                  <button className="btn-action">UBAH ALAMAT</button>
                  <button className="btn-action" style={{ marginLeft: '1rem' }} onClick={() => setModalAddress(true)}>PILIH ALAMAT LAIN</button>
                </div>

                <div className="title" style={{ marginTop: '3rem' }}>
                  Metode
                  {' '}
                  <span style={{ color: '#010101' }}>Pengiriman</span>
                </div>

                <div className="card" style={{ marginTop: '3rem', border: '0px' }}>
                  <table>
                    <thead>
                      <th />
                      <th>G Coffee Roastery</th>
                      <th />
                      <th />
                    </thead>
                    <tbody>
                      <tr>
                        <td style={{ padding: '0px 1rem' }}>
                          <label className="c-radio" style={{ marginTop: '-9px' }}>
                            <input type="radio" checked name="payment" />
                            <span className="checkmark" />
                          </label>
                        </td>
                        <td>
                          Rp 13.000
                        </td>
                        <td>
                          <b>TIKI | Economy Service</b>
                        </td>
                        <td>
                          4 Hari Kerja
                        </td>
                      </tr>

                    </tbody>
                  </table>
                </div>

                <div style={{ marginTop: '1rem' }}>
                  <button className="btn-action" onClick={() => setModalCourir(true)}>UBAH</button>
                </div>

                <div className="title" style={{ marginTop: '3rem' }}>
                  Pilih Metode
                  {' '}
                  <span style={{ color: '#010101' }}>Pembayaran</span>
                </div>

                <div style={{ marginTop: '2rem' }}>
                  <div className="parent-card">
                    <div className="parent-first">
                      <img className="img-payment" src="/images/ic_visa.png" alt="" />
                      <div className="parent-card-text">
                        <div className="w-100"><b>Credit Card</b></div>
                        <div className="w-100">Pay with Visa or Mastercard</div>
                      </div>
                    </div>
                    <img src="/images/ic_arrow_down.png" alt="" width="20px" />
                  </div>

                  <div className="parent-card">
                    <div className="parent-first">
                      <img className="img-payment" src="/images/ic_cc.png" alt="" />
                      <div className="parent-card-text">
                        <div className="w-100"><b>ATM/Bank Transfer</b></div>
                        <div className="w-100">Pay from ATM Bersama, Prima or Alto</div>
                      </div>
                    </div>
                    <img src="/images/ic_arrow_up.png" alt="" width="12px" />
                  </div>

                  <div className="parent-card">
                    <div className="parent-first">
                      <label className="c-radio" style={{ marginTop: '-20px', marginLeft: '3rem' }}>
                        <input type="radio" name="bank" />
                        <span className="checkmark" />
                      </label>
                      <img className="img-payment" src="/images/ic_bank_mandiri.png" alt="" />
                      <div className="parent-card-text">
                        <div className="w-100"><b>Mandiri</b></div>
                        <div className="w-100">Pay from Mandiri ATMs or Internet Banking</div>
                      </div>
                    </div>
                  </div>

                  <div className="parent-card">
                    <div className="parent-first">
                      <label className="c-radio" style={{ marginTop: '-20px', marginLeft: '3rem' }}>
                        <input type="radio" name="bank" />
                        <span className="checkmark" />
                      </label>
                      <img className="img-payment" src="/images/ic_bank_bni.png" alt="" />
                      <div className="parent-card-text">
                        <div className="w-100"><b>BNI</b></div>
                        <div className="w-100">Pay from BNI ATMs or Internet Banking</div>
                      </div>
                    </div>
                  </div>

                  <div className="parent-card">
                    <div className="parent-first">
                      <label className="c-radio" style={{ marginTop: '-20px', marginLeft: '3rem' }}>
                        <input type="radio" name="bank" />
                        <span className="checkmark" />
                      </label>
                      <img className="img-payment" src="/images/ic_bank_permata.png" alt="" />
                      <div className="parent-card-text">
                        <div className="w-100"><b>Permata</b></div>
                        <div className="w-100">Pay from Permata ATMs or Internet Banking</div>
                      </div>
                    </div>
                  </div>

                  <div className="parent-card">
                    <div className="parent-first">
                      <img className="img-payment" src="/images/ic_gopay.png" alt="" />
                      <div className="parent-card-text">
                        <div className="w-100"><b>QR Payment</b></div>
                        <div className="w-100">Pay with your preferred payment app</div>
                      </div>
                    </div>
                    <img src="/images/ic_arrow_down.png" alt="" width="20px" />
                  </div>
                </div>

                <div style={{ marginTop: '2rem' }}>
                  <label className="label">Voucher Discount</label>
                  <input style={{ marginTop: '1rem' }} className="form-control w-100" placeholder="Masukkan Voucher Discount" />
                </div>

                <div style={{ textAlign: 'center', marginTop: '2rem', padding: '0px 3rem' }}>
                  <Link href="/payment/snap">
                    <button className="btn-primary btn-this" style={{ width: '100%' }}>Lanjut Pembayaran</button>
                  </Link>
                </div>
              </div>

              <div className="c-2">
                <div className="card-2">
                  <div className="summary-header">
                  Ringkasan
                  </div>

                  <div className="c-content-summary">
                    <div className="key">
                    Subtotal
                    </div>
                    <div className="value">
                    Rp 220.000
                    </div>
                  </div>

                  <div className="c-content-summary">
                    <div className="key">
                    Biaya Layanan
                    </div>
                    <div className="value">
                    Rp 5.000
                    </div>
                  </div>


                  <div className="c-content-summary">
                    <div className="key">
                    Pengiriman
                    </div>
                    <div className="value">
                    Rp 13.000
                    </div>
                  </div>


                  <div className="c-content-summary">
                    <div className="key">
                    Total Pesanan
                    </div>
                    <div className="value">
                      <span style={{ color: '#FF8242' }}>Rp 225.000</span>
                    </div>
                  </div>

                  <div className="c-content-summary">
                    <div className="key">
                        1 Produk di Keranjang
                    </div>
                    <div className="value">
                      <img src="/images/ic_arrow_down.png" alt="" />
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </BaseLayout>

      <Modal open={modalAddress} onClose={() => setModalAddress(false)}>
        <div className="card-modal" style={{ padding: '16px 26px' }}>
          <div style={{ textAlign: 'right' }}>
            <div style={{ display: 'inline-block' }} onClick={() => setModalAddress(false)} onKeyDown={() => setModalAdd(false)} className="pointer" role="button" tabIndex={0}>
              <img src="/images/ic_close_red.png" alt="" />
            </div>
          </div>
          <div className="title" style={{ marginTop: '16px' }}>
            Pilih Alamat Lain
          </div>
          <div style={{ marginTop: '24px' }}>
            <table>
              <thead>
                <th />
                <th>Penerima</th>
                <th>Alamat Pengirim</th>
                <th>Daerah Pengiriman</th>
              </thead>
              <tbody>
                <tr>
                  <td style={{ padding: '0px 1rem' }}>
                    <label className="c-radio" style={{ marginTop: '-9px' }}>
                      <input type="radio" checked name="address" />
                      <span className="checkmark" />
                    </label>
                  </td>
                  <td style={{ minWidth: '250px' }}>
                    <b>Lalalisa Hathway</b>
                    <br />
                    <div style={{ marginTop: '1rem' }}>
                        0812134234
                    </div>
                  </td>
                  <td>
                    <b>Rumah</b>
                    <br />
                    <div style={{ marginTop: '1rem', maxWidth: '450px' }}>
                        Jalan Cipaganti No 8 RT/RW 01/ 02 Kel/Desa Setiasari Bandung
                    </div>
                  </td>
                  <td>
                      Jawa Barat, Kota Bandung, Bandung Tengah, 40521 Indonesia
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div style={{ marginTop: '24px', padding: '1rem 3rem', textAlign: 'center' }}>
            <button className="btn-primary btn-this">SIMPAN</button>
          </div>
          <div className="divider" style={{ margin: '24px' }} />
        </div>
      </Modal>

      <Modal open={modalCourir} onClose={() => setModalCourir(false)}>
        <div className="card-modal" style={{ padding: '16px 26px' }}>
          <div style={{ textAlign: 'right' }}>
            <div style={{ display: 'inline-block' }} onClick={() => setModalCourir(false)} onKeyDown={() => setModalCourir(false)} className="pointer" role="button" tabIndex={0}>
              <img src="/images/ic_close_red.png" alt="" />
            </div>
          </div>
          <div className="title" style={{ marginTop: '16px' }}>
            Pilih Metode Pengiriman Lain
          </div>
          <div style={{ marginTop: '24px' }}>
            <table>
              <thead>
                <th />
                <th>G Coffee Roastery</th>
                <th />
                <th />
              </thead>
              <tbody>
                <tr>
                  <td style={{ padding: '0px 1rem' }}>
                    <label className="c-radio" style={{ marginTop: '-9px' }}>
                      <input type="radio" checked name="courir_change" />
                      <span className="checkmark" />
                    </label>
                  </td>
                  <td>
                      Rp 13.000
                  </td>
                  <td>
                    <b>TIKI | Economy Service</b>
                  </td>
                  <td>
                      4 Hari Kerja
                  </td>
                </tr>
                <tr>
                  <td style={{ padding: '0px 1rem' }}>
                    <label className="c-radio" style={{ marginTop: '-9px' }}>
                      <input type="radio" name="courir_change" />
                      <span className="checkmark" />
                    </label>
                  </td>
                  <td>
                      Rp 16.000
                  </td>
                  <td>
                    <b>TIKI | Regular Service</b>
                  </td>
                  <td>
                      2 Hari Kerja
                  </td>
                </tr>
                <tr>
                  <td style={{ padding: '0px 1rem' }}>
                    <label className="c-radio" style={{ marginTop: '-9px' }}>
                      <input type="radio" name="courir_change" />
                      <span className="checkmark" />
                    </label>
                  </td>
                  <td>
                      Rp 17.000
                  </td>
                  <td>
                    <b>JNE | Economy Service</b>
                  </td>
                  <td>
                      2-3 Hari Kerja
                  </td>
                </tr>

              </tbody>
            </table>
          </div>
          <div style={{ marginTop: '24px', padding: '1rem 3rem', textAlign: 'center' }}>
            <button className="btn-primary btn-this">SIMPAN</button>
          </div>
          <div className="divider" style={{ margin: '24px' }} />
        </div>
      </Modal>

      <style jsx>
        {
          `
          .title {
            font-weight: bold;
            font-size: 24px;
            line-height: 23px;
            letter-spacing: 0.05em;
            color: #696969;
            padding: 0px 2rem;
          }

          .c-content {
            display: flex;
            flex-direction: row;
          }

          .c-1 {
            width: 100%;
            margin-right: 28px;
          }

          .c-2 {
            width: 550px;
          }
          .card-2 {
            background: #FFFFFF;
            box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.25);
            height: auto;
          }

          .summary-header {
            padding: 1rem;
            font-weight: bold;
            font-size: 18px;
            line-height: 17px;
            color: #868686;
            box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.25);
          }

          .c-content-summary {
            font-size: 16px;
            line-height: 15px;
            color: #696969;
            display: flex;
            justify-content: space-between;
            padding: 0.7rem;
            margin: 0.8rem 0.5rem;
            border-bottom: 1px solid #CCCCCC;
          }

          .key {
            display: flex;
          }

          .key img {
            width: 85px;
          }

          .card {
            background: #FFFFFF;
            border: 0.5px solid #010101;
            box-sizing: border-box;
            box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.25);
            border-radius: 5px;
          }

          th {
            text-align: left;
            padding: 1rem 1rem;
          }

          .btn-action {
            background: #FFFFFF;
            border: 0.5px solid #696969;
            box-sizing: border-box;
            border-radius: 7px;
            font-weight: 500;
            font-size: 18px;
            line-height: 17px;
            text-align: center;
            color: #696969;
            padding: 1rem 2rem;
            cursor: pointer;
          }

          td {
            border-bottom: 1px solid #CCCCCC;
          }

          .btn-this {
            width: auto;
            font-weight: 500;
            font-size: 18px;
            line-height: 17px;
            text-align: center;
            color: #FFFFFF;
            padding: 1rem 2rem;
          }

          .label {
            font-weight: 500;
            font-size: 16px;
            line-height: 19px;
            color: #868686;
          }

          .parent-card {
            display: flex;
            justify-content: space-between;
            align-items: center;
            border-bottom: 1px solid #CCCCCC;
            margin: 1rem 0px;
            padding: 0.5rem 1px;
            cursor: pointer;
          }

          .img-payment {
            margin: 1px 2rem;
            width: 50px;
          }

          .parent-card-text {
            font-size: 18px;
            line-height: 22px;
            letter-spacing: 0.05em;
            color: #696969;
          }

          .parent-first {
            display: flex;
            flex-wrap: wrap;
            align-items: center;
          }
        `
        }
      </style>
    </div>
  );
};

export default index;
