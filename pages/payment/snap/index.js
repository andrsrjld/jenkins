import React from 'react';
import BaseLayout from '~/components/BaseLayout';
import { getProductType } from '~/data/api/product-type';

const index = ({productType, token}) => (
  <div>
    <BaseLayout productType={productType} token={token}>
      <div className="c">

        <div className="breadcumb w-100" style={{ marginLeft: '70px' }}>
          Home &#x0226B;
          {' '}
          Keranjang &#x0226B;
          {' '}
          Shipping &#x0226B;
          {' '}
          Review & Payment &#x0226B;
          {' '}
          <span style={{ color: '#010101' }}>Menunggu Pembayaran</span>
        </div>

        <div className="c-card">
          <div className="title">
            Menunggu Pembayaran
          </div>
          <div className="subtitle">
            Mohon selesaikan pembayaran Anda sebelum tanggal 05 Mei 2020 11:33 WIB
            <br />
            dengan rincian sebagai berikut.
          </div>
          <div className="divider" style={{ margin: '2rem 3rem' }} />
          <div className="c-code">
            <div className="item">
              <img src="/images/ic_bank_mandiri.png" alt="" />
            </div>
            <div className="item">
              <div className="desc">Kode Unik Pembayaran</div>
              <div className="subtitle" style={{ marginTop: '1.5rem', color: '#010101' }}><b>8870923342135322</b></div>
            </div>
            <div className="item" style={{ display: 'flex', paddingLeft: '2rem' }}>
              <div className="desc" style={{ color: '#FF8242', textAlign: 'left', alignSelf: 'flex-end' }}>SALIN</div>
            </div>
          </div>
          <div className="divider" style={{ margin: '2rem 3rem' }} />
          <div className="c-code">
            <div className="item">
              <div className="desc">Jumlah yang harus di bayar</div>
              <div className="subtitle" style={{ marginTop: '1.5rem', color: '#FF8242' }}><b>Rp 275.000</b></div>
              <div className="desc" style={{ marginTop: '1.5rem' }}>
                Pembelian akan otomatis dibatalkan apabila Anda tidak melakukan pembayaran
                <br />
                lebih dari 1 hari setelah kode pembayaran di berikan.
              </div>
              <div className="c-row bg-row" style={{ marginTop: '3rem' }}>
                <div className="title">
                  Cara Membayar
                </div>
              </div>
              <div className="c-row">
                <div className="title">
                  ATM
                </div>
                <img src="/images/ic_arrow_up.png" alt="" />
              </div>
              <div className="c-row">
                <div className="title">
                  Internet Banking
                </div>
                <img src="/images/ic_arrow_up.png" alt="" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </BaseLayout>
    <style jsx>
      {
        `
        .c {
          display: flex;
          padding: 2rem 0px;
          justify-content: center;
          flex-wrap: wrap;
        }

        .c-card {
          background: #FFFFFF;
          box-shadow: 0px 1px 12px rgba(0, 0, 0, 0.25);
          border-radius: 10px;
          padding: 3rem;
          margin-top: 2rem;
        }

        .title {
          font-weight: bold;
          font-size: 24px;
          line-height: 23px;
          letter-spacing: 0.05em;
          color: #696969;
          text-align: center;
        }

        .subtitle {
          margin-top: 1rem;
          font-size: 24px;
          line-height: 29px;
          text-align: center;
          letter-spacing: 0.05em;
          color: #696969;
          text-align: center;
        }

        .divider {
          height: 1px;
          background: #CCCCCC;
        }

        .c-code {
          display: flex;
          margin: 1rem 3rem;
        }

        .c-code .item {
          width: 100%;
        }

        .desc {
          font-size: 20px;
          line-height: 24px;
          text-align: center;
          letter-spacing: 0.05em;
          color: #696969;          
        }

        .bg-row {
          background: #F5F5F5;
          box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.06);
          border-radius: 5px;
        }

        .c-row {
          display: flex;
          justify-content: space-between;
          align-items: center;
          padding: 1.2rem 2.5rem;
          border-bottom: 1px solid #CCCCCC;
        }
        .c-row .title{
          font-weight: 500;
          font-size: 20px;
          line-height: 24px;
          letter-spacing: 0.05em;
          color: #696969;
        }
        `
      }
    </style>
  </div>
);

index.getInitialProps = async (ctx) => {
  const { token } = ctx.store.getState().authentication;

  const productTypeResp = await getProductType();
  const productType = productTypeResp.data;

  return { 
    productType,
    token
  };
};

export default index;
