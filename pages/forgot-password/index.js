import React, { useState } from 'react';
import Router from 'next/router';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { connect } from 'react-redux';
import Link from 'next/link';
import { login } from '../../util/ServiceHelper';
import {
  authenticate, socialAuthenticate
} from '~/redux/actions/authActions';
import { GoogleLogin } from 'react-google-login';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'
import { validateSocial, loginSocial } from '~/data/api/social'
import { postForgotPassword, postChangeForgotPassword } from '~/data/api/user';

// Toastr Notification
import {successNotification, errorNotification} from '~/util/toastr-notif';

const Home = ({ authenticate, apiUrl, socialAuthenticate, token, email }) => {
  const router = useRouter();
  const param = router.query;
  
  const [form, setForm] = useState({
    email: '',
    password: '',
    emailForgot: ''
  });

  const [isLoading, setLoading] = useState(false);
  const [error, setError] = useState({
    show: false,
    message: 'Username atau password salah'
  });
  const [forgotPassword, setForgotPassword] = useState(false);


  const doLogin = async (e) => {
    e.preventDefault();
    setLoading(true);
    const body = {
      email: email,
      new_password: form.password
    };
    const res = await postChangeForgotPassword(token, body);
    if (res.code === 500 || res.code === 400) {
      setError((v) => ({ ...v, show: true }));
    } else {
      window.location = param.redirect ? `/${param.redirect}` : '/signin';
    }
    setLoading(false);
  };

  const handleChange = (name) => (e) => {
    const { value } = e.target;
    setError((v) => ({ ...v, show: false }));
    setForm((v) => ({ ...v, [name]: value }));
  };

  const responseGoogle = async (response) => {
    if(!response.error) {
      const res = await validateSocial(apiUrl, response.profileObj.email)
      if(!res.data.registered) {
        Router.push(`/register?email=${response.profileObj.email}&firstName=${response.profileObj.givenName}&lastName=${response.profileObj.familyName}`)
      } else {
        const res = await loginSocial(apiUrl, response.profileObj.email, response.tokenObj.id_token)
        if(res.code !== 400 || res.code !== 500) {
          socialAuthenticate(res.data)
        }
      }
    }
  }

  const responseFacebook = async (response) => {
    if(!response.status) {
      const res = await validateSocial(apiUrl, response.email)
      if(!res.data.registered) {
        Router.push(`/register?email=${response.email}&firstName=${response.first_name}&lastName=${response.last_name}`)
      } else {
        const res = await loginSocial(apiUrl, response.email, response.accessToken)
        if(res.code !== 400 || res.code !== 500) {
          socialAuthenticate(res.data)
        }
      }
    }
  }

  const doForgotPassword = async () => {
    setLoading(true);
    const body = {
      email: form.emailForgot,
    };
    const res = await postForgotPassword(body);
    if (res.code === 500 || res.code === 400) {
      errorNotification(res.message);
      setForgotPassword(false);
    } else {
      console.log(res)
      successNotification("Permintaan reset password berhasil, check email kamu untuk langkah selanjutnya. Reset password dikirimkan melalui email mohon cek inbox atau spam");
    }
    setLoading(false);
  };

  return (
    <div className="c">
      <Head>
        <title>Bapera</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className="c">
        <div className="w-100 flex justify-center">
          <Link href="/"><img className="h-16" src="/images/logo_bapera.png" alt="" /></Link>
        </div>

        <div className="container-login" style={!forgotPassword ? {display: "block"} : {display: "none"}}>
          <div className="text-primary-dark text-3xl font-bold">Password Baru</div>
          <form onSubmit={doLogin}>
            <div style={{ marginTop: '50px' }}>
              <input
                className="form-control w-100"
                placeholder="Password Baru"
                type="password"
                value={form.email}
                onChange={handleChange('email')}
                disabled={isLoading}
              />
            </div>
            <div style={{ marginTop: '20px' }}>
              <input
                className="form-control w-100"
                placeholder="Konfirmasi Password Baru"
                type="password"
                value={form.password}
                onChange={handleChange('password')}
                disabled={isLoading}
              />
            </div>
            <div className="text-error" style={{ marginTop: '16px', display: error.show ? 'block' : 'none' }}>
              {error.message}
            </div>

            <div style={{ marginTop: '38px' }}>
              <button
                type="submit"
                className="btn-primary"
                disabled={form.password === '' || form.password !== form.email}
              >
                Simpan
              </button>
            </div>
          </form>

          <hr style={{ margin: '30px 10px 10px 10px' }} />
        </div>
        <div className="container-login" style={forgotPassword ? {display: "block"} : {display: "none"}}>
          <div className="text-primary-dark text-3xl font-bold">Lupa Password</div>

          <div style={{ marginTop: '50px' }}>
            <input
              className="form-control w-100"
              placeholder="Email Address"
              value={form.emailForgot}
              onChange={handleChange('emailForgot')}
              disabled={isLoading}
            />
          </div>

          <div className="text-error" style={{ marginTop: '16px', display: error.show ? 'block' : 'none' }}>
            {error.message}
          </div>

          <div style={{ marginTop: '38px' }}>
            <button
              type="button"
              className="btn-primary"
              onClick={doForgotPassword}
              disabled={isLoading || form.emailForgot === ''}
            >
              Kirim
            </button>
          </div>

          <hr style={{ margin: '30px 10px 10px 10px' }} />

        </div>
      </div>

      <style jsx>
        {`
        .c {
          width: 100%;
          height: 100vh;
          display: flex;
          align-items: center;
          flex-wrap: wrap;
          justify-content: center;
        }

        .container-login {
          min-width: 568px;
          min-height: 396px;
          background: #FFFFFF;
          box-shadow: 0px 1px 12px rgba(0, 0, 0, 0.25);
          border-radius: 10px;
          padding: 2.5rem 2.5rem;
        }

        .break {
          flex-basis: 100%;
          height: 0;
        }

        .text-forgot-password {
          font-size: 12px;
          line-height: 14px;
          color: #6F0C06;
          margin-top: 20px;
          text-align: right;
        }

        .container-social-media {
          display: flex;
          justify-content: space-evenly;
          margin-top: 30px;
        }

        .text-register {
          font-size: 16px;
          line-height: 19px;
          color: #AFAFAF;
        }

        .text-register-action {
          font-weight: bold;
          font-size: 16px;
          line-height: 19px;
          color: #730C07;
          cursor: pointer;
        }

        @media only screen and (max-width: 600px) {
          .c {
            width: 100%;
            display: flex;
            align-items: center;
            flex-wrap: wrap;
            justify-content: center;
          }

          .container-login {
            margin-top: 12px;
            min-width: 90%;
            min-height: 396px;
            background: #FFFFFF;
            box-shadow: 0px 1px 12px rgba(0, 0, 0, 0.25);
            border-radius: 10px;
            padding: 2.5rem 2.5rem;
            width: 90%;
          }
        }
      `}

      </style>
    </div>
  );
};

Home.getInitialProps = (ctx) => {  
  const { endpoint } = ctx.store.getState().authentication;
  const {email, token} = ctx.query

  return {
      apiUrl: endpoint.apiUrl,
      token: token,
      email: email
  }
}

export default connect(
  (state) => state,
  { authenticate, socialAuthenticate }
)(Home);
