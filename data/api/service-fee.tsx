import fetch from 'node-fetch'
import {options} from 'util/helper'

export type TParams = {
  layout_type: string
}

export type TRequestInsert = {
  name: string,
}

export type TRequestUpdate = {
  charge: number
}

export const getServiceFee = async (API_URL, token) => {
  let url = new URL(`${API_URL}order/service_charge`)
  const prom = await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const updateServiceFee = async (API_URL, token, body: TRequestUpdate) => {
  let url = new URL(`${API_URL}management/order/service_charge`)
  const prom = await fetch(url, {
    method: 'PUT',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}