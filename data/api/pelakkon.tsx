import fetch from 'node-fetch'
import {options} from 'util/helper'

export enum STATUS {
  ALL = 'all',
  DRAFT = 'Draft',
  APPROVED = 'Approved'
}

export enum SORTING {
  ASC = 'asc',
  DESC = 'desc'
}

export type TRequest = {
  seller_id: number,
  urutan: number
}

export type TParams = {
  layout_type: string,
  page: number,
  limit: number
}

export type TUpdateStatus = {
  id: number,
  status: STATUS
}


export const getPelakkon = async (API_URL, token, params: TParams) => {
  let url = new URL(`${API_URL}management/crm/flashsale/seller`)
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]))
  const prom = await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const postPelakkon = async (API_URL, token, body: TRequest) => {
  const prom = await fetch(`${API_URL}management/crm/flashsale/seller`, {
    method: 'POST',
    body : JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const updatePelakkon = async (API_URL, token, body) => {
  const prom = await fetch(`${API_URL}management/crm/flashsale/seller`, {
    method: 'PUT',
    body : JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const deletePelakkon = async (API_URL, token, id) => {
  const prom = await fetch(`${API_URL}management/crm/flashsale/seller?id=${id}`, {
    method: 'DELETE',
    body : JSON.stringify({id}),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}