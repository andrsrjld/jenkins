import * as base from './base'
import {options} from 'util/helper'

export const getShipping = (token: string, body: any) : Promise<any> => 
  base.api({
    url: 'restricted/shipping',
    method: 'POST',
    body: body,
    headers: base.getHeaders(token)
  })

export const getShippingList = async (API_URL: string, token: string) : Promise<any> => {
  const prom = await fetch(`${API_URL}restricted/shipment_method?layout_type=list_layout`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const addShipping = async (API_URL, token, id) : Promise<any> => {
  const prom = await fetch(`${API_URL}restricted/seller/shipment`, {
    method: 'POST',
    body : JSON.stringify({
      shipment_method_id: id
    }),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const deleteShipping = async (API_URL, token, id) : Promise<any> => {
  const prom = await fetch(`${API_URL}restricted/seller/shipment`, {
    method: 'DELETE',
    body : JSON.stringify({
      shipment_method_id: id
    }),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}


