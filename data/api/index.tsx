import { TCategory } from '../models/index'
import * as base from './base'

export const getCategories = (token: string) : Promise<any> => 
  base.get({
    url: 'product_type?layout_type=list_layout',
    method: 'GET',
    body: {},
    headers: base.getHeaders(token)
  })

export const getRequests = (token: string) : Promise<any> => 
  base.get({
    url: 'value?layout_type=list_layout&slug=request-2',
    method: 'GET',
    body: {},
    headers: base.getHeaders(token)
  })


export const getOrigins = (token: string) : Promise<any> => 
  base.get({
    url: 'value?layout_type=list_layout&slug=origin-3',
    method: 'GET',
    body: {},
    headers: base.getHeaders(token)
  })

export const getSpecies = (token: string) : Promise<any> => 
  base.get({
    url: 'value?layout_type=list_layout&slug=bean-species-4',
    method: 'GET',
    body: {},
    headers: base.getHeaders(token)
  })

export const getProcessing = (token: string) : Promise<any> => 
  base.get({
    url: 'value?layout_type=list_layout&slug=processing-5',
    method: 'GET',
    body: {},
    headers: base.getHeaders(token)
  })

  
export const getTasted = (token: string) : Promise<any> => 
  base.get({
    url: 'value?layout_type=list_layout&slug=tasted-6',
    method: 'GET',
    body: {},
    headers: base.getHeaders(token)
  })

export const getRoastLevels = (token: string) : Promise<any> => 
  base.get({
    url: 'value?layout_type=list_layout&slug=roast-level-7',
    method: 'GET',
    body: {},
    headers: base.getHeaders(token)
  })

export const getTeaTypes = (token: string) : Promise<any> => 
  base.get({
    url: 'value?layout_type=list_layout&slug=tea-types-8',
    method: 'GET',
    body: {},
    headers: base.getHeaders(token)
  })

export const getChocolateTypes = (token: string) : Promise<any> => 
  base.get({
    url: 'value?layout_type=list_layout&slug=chocolate-species-9',
    method: 'GET',
    body: {},
    headers: base.getHeaders(token)
  })

export const getSizes = (token: string) : Promise<any> => 
  base.get({
    url: 'value?layout_type=list_layout&slug=t-shirt-size-10',
    method: 'GET',
    body: {},
    headers: base.getHeaders(token)
  })
