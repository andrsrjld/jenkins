import fetch from 'node-fetch'
import {options} from 'util/helper'

export type TParams = {
  layout_type: string
}

export type TRequestInsert = {
  name: string,
}

export type TRequestUpdate = {
  id: number,
  name: string
}

export const getRole = async (API_URL, token) => {
  let url = new URL(`${API_URL}management/role?layout_type=list_layout`)
  const prom = await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const getOneRole = async (API_URL, token, id) => {
  let url = new URL(`${API_URL}management/role?layout_type=detail_layout&id=${id}`)
  const prom = await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const postRole = async (API_URL, token, body: TRequestInsert) => {
  let url = new URL(`${API_URL}management/role`)
  const prom = await fetch(url, {
    method: 'POST',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const updateRole = async (API_URL, token, body: TRequestUpdate) => {
  let url = new URL(`${API_URL}management/role`)
  const prom = await fetch(url, {
    method: 'PUT',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const deleteRole   = async (API_URL, token, id) => {
  let url = new URL(`${API_URL}management/role`)
  const prom = await fetch(url, {
    method: 'DELETE',
    body: JSON.stringify({
      id: id
    }),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}
