import fetch from 'node-fetch'
import {options} from 'util/helper'

export enum TGosendShipmentMethod {
  INSTANT = 'Instant',
  SAMEDAY = 'SameDay'
}

export type TRoute = {
  originName: string,
  originNote: string,
  originContactName: string,
  originContactPhone: string,
  originLatLong: string,
  originAddress: string,
  destinationName: string,
  destinationNote: string,
  destinationContactName: string,
  destinationContactPhone: string,
  destinationLatLong: string,
  destinationAddress: string,
  item: string,
  storeOrderId: string,
  insuranceDetails: {
      applied: string,
      fee: string,
      product_description: string,
      product_price: string
  }
}

export type TBody = {
  orderSellerId: number,
  sellerId: number,
  userId: number,
  paymentType: number,
  deviceToken: string,
  collection_location: string,
  shipment_method: TGosendShipmentMethod,
  routes: Array<TRoute>
}

export const bookingDriver = async (API_URL, token, body: TBody) : Promise<any> => {
  const prom = await fetch(`${API_URL}restricted/gosend?action=booking`, {
    method: 'POST',
    body : JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const bookingStatus = async (API_URL, token, orderId: string) : Promise<any> => {
  const prom = await fetch(`${API_URL}restricted/gosend?action=check_status&type=OrderNumber&value=${orderId}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}
