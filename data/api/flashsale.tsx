import fetch from 'node-fetch'
import {options} from 'util/helper'

export enum STATUS {
  ALL = 'all',
  DRAFT = 'Draft',
  APPROVED = 'Approved'
}

export enum SORTING {
  ASC = 'asc',
  DESC = 'desc'
}

export type TRequest = {
  product_id: number,
  urutan: number,
  variant_id: number
}

export type TRequestUpdate = {
  id: number,
  urutan: number
}

export type TParams = {
  layout_type: string,
  page: number,
  limit: number
}

export type TUpdateStatus = {
  id: number,
  status: STATUS
}


export const getFlashSale = async (API_URL, token, params: TParams) => {
  let url = new URL(`${API_URL}management/crm/flashsale/product`)
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]))
  const prom = await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const postFlashsale = async (API_URL, token, body: TRequest) => {
  const prom = await fetch(`${API_URL}management/crm/flashsale/product`, {
    method: 'POST',
    body : JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const updateFlashsale = async (API_URL, token, body: TRequestUpdate) => {
  const prom = await fetch(`${API_URL}management/crm/flashsale/product`, {
    method: 'PUT',
    body : JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const deleteFlashsale = async (API_URL, token, id) => {
  const prom = await fetch(`${API_URL}management/crm/flashsale/product?id=${id}`, {
    method: 'DELETE',
    body : JSON.stringify({id}),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}
import * as base from './base'

export const getFlashSaleProduct = (token: string) : Promise<any> => 
  base.get({
    url: 'flashsale/product?layout_type=list_layout',
    method: 'GET',
    body: {},
    headers: base.getHeaders(token)
  })

export const getFlashSaleSeller = (token: string) : Promise<any> => 
  base.get({
    url: 'flashsale/seller?layout_type=list_layout',
    method: 'GET',
    body: {},
    headers: base.getHeaders(token)
  })
