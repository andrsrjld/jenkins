import * as base from './base'

export const getTestimonials = (token: string) : Promise<any> => 
  base.get({
    url: 'testimonial?layout_type=list_layout',
    method: 'GET',
    body: {},
    headers: base.getHeaders(token)
  })