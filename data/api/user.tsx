import * as base from './base'

export const getUser = (token: string) : Promise<any> => 
  base.get({
    url: 'restricted/user',
    method: 'GET',
    body: {},
    headers: base.getHeaders(token)
  })

export const getBalance = (token: string) : Promise<any> => 
  base.get({
    url: 'restricted/statement/balance',
    method: 'POST',
    body: {},
    headers: base.getHeaders(token)
  })

export const transferByRekening = (token: string, body: any) : Promise<any> => 
  base.api({
    url: 'restricted/transfer/account',
    method: 'POST',
    body: body,
    headers: base.getHeaders(token)
  })

export const transferByPhone = (token: string, body: any) : Promise<any> => 
  base.api({
    url: 'restricted/transfer/phone',
    method: 'POST',
    body: body,
    headers: base.getHeaders(token)
  })

export const getHistory = (token: string) : Promise<any> => 
  base.get({
    url: 'restricted/statement/history',
    method: 'POST',
    body: {},
    headers: base.getHeaders(token)
  })

export const getUsers = (token: string, page: string, limit: string, keyword: string, id: string) : Promise<any> => 
  base.get({
    url: `management/users?layout_type=list_layout&page=${page}&limit=${limit}&keyword=${keyword}&id=${id}`,
    method: 'GET',
    body: {},
    headers: base.getHeaders(token)
  });

export const updateUsers = (token: string, body: any) : Promise<any> => 
  base.api({
    url: `restricted/user/account`,
    method: 'PUT',
    body: body,
    headers: base.getHeaders(token)
  });

export const updateUserPassword = (token: string, body: any) : Promise<any> => 
  base.api({
    url: `restricted/user/password`,
    method: 'PUT',
    body: body,
    headers: base.getHeaders(token)
  });

export const updateUserEmailPhone = (token: string, body: any) : Promise<any> => 
  base.api({
    url: `restricted/user/email_phone`,
    method: 'PUT',
    body: body,
    headers: base.getHeaders(token)
  });

export const postVerificationRequest = (token: string, body: any) : Promise<any> =>
  base.api({
    url: 'auth/verification/request',
    method: 'POST',
    body: body,
    headers: base.getHeaders(token)
  })

export const postVerificationValidation = (token: string, body: any) : Promise<any> => 
  base.api({
      url: 'auth/verification/validation',
      method: 'POST',
      body: body,
      headers: base.getHeaders(token)
  })

export const postForgotPassword = (body: any) : Promise<any> => 
  base.api({
      url: 'auth/forgot_password',
      method: 'POST',
      body: body,
      headers: {
        'Content-Type': 'application/json'
      }
  })

export const postChangeForgotPassword = (token, body: any) : Promise<any> => 
  base.api({
      url: 'restricted/user/reset_password',
      method: 'PUT',
      body: body,
      headers: base.getHeaders(token)
  })