import fetch from 'node-fetch'
import {options} from 'util/helper'

export enum STATUS {
  ALL = 'all',
  PUBLISHED = 'published',
  NOT_PUBLISHED = 'not-published'
}

export type TParams = {
  layout_type: string,
  status: STATUS
}

export type TRequestInsert = {
  title: string,
  description: string,
  image_url: string,
  link: string,
  publish_date: string,
  remove_date: string
}

export type TRequestUpdate = {
  id: number,
  title: string,
  description: string,
  image_url: string,
  link: string,
  publish_date: string,
  remove_date: string
}

export type TRequestUpdateStatus = {
  id: number,
  title: string,
  description: string,
  image_url: string,
  link: string,
  publish_date: string,
  remove_date: string,
  is_published
}


export const getBanners = async (API_URL, token, params: TParams) => {
  let url = new URL(`${API_URL}banner`)
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]))
  const prom = await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const getOneBanner = async (API_URL, token, slug) => {
  let url = new URL(`${API_URL}banner?layout_type=detail_layout&slug=${slug}`)
  const prom = await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const postBanner = async (API_URL, token, body: TRequestInsert) => {
  let url = new URL(`${API_URL}banner`)
  const prom = await fetch(url, {
    method: 'POST',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const updateBanner = async (API_URL, token, body: TRequestUpdate) => {
  let url = new URL(`${API_URL}banner`)
  const prom = await fetch(url, {
    method: 'PUT',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const updateBannerStatus = async (API_URL, token, body: TRequestUpdateStatus) => {
  let url = new URL(`${API_URL}banner`)
  const prom = await fetch(url, {
    method: 'PUT',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const deleteBanner = async (API_URL, token, id) => {
  let url = new URL(`${API_URL}banner`)
  const prom = await fetch(url, {
    method: 'DELETE',
    body: JSON.stringify({
      id: id
    }),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}
