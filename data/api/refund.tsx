import { get, api, getHeaders } from 'data/api/base'

export const getRefundAdmin = async (token, limit, page, status, startDate, endDate) : Promise<any> => {
  return get({
    url: `management/order/refund?layout_type=list_layout&limit=${limit}&page=${page}&status${status}&start_date=${startDate}&end_date=${endDate}`,
    method: "GET",
    body: {},
    headers: getHeaders(token)
  })
}

export const updateRefundAdmin = async (token, body) : Promise<any> => {
  return api({
    url: `management/order/refund`,
    method: 'PUT',
    body: body,
    headers: getHeaders(token)
  })
}

export const getRefundUser = async (token, limit, page, status) : Promise<any> => {
  return get({
    url: `restricted/refund/user?layout_type=list_layout&limit=${limit}&page=${page}&status${status}`,
    method: "GET",
    body: {},
    headers: getHeaders(token)
  })
}

export const postRefund = async (token, body) : Promise<any> => {
  return api({
    url: `restricted/refund`,
    method: 'POST',
    body: body,
    headers: getHeaders(token)
  })
}