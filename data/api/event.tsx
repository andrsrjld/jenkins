import fetch from 'node-fetch'
import {options} from 'util/helper'

export enum STATUS {
  ALL = 'all',
  DRAFT = 'Draft',
  APPROVED = 'Approved'
}

export enum SORTING {
  ASC = 'asc',
  DESC = 'desc'
}

export type TRequest = {
  nama: string,
  deskripsi: string,
  start_date: string,
  end_date: string,
  status: boolean,
  event_sellers: any
}

export type TRequestUpdate = {
  id: number,
  nama: string,
  deskripsi: string,
  start_date: string,
  end_date: string,
  status: boolean,
  event_sellers: any
}

export type TParams = {
  layout_type: string,
  page: number,
  limit: number
}

export type TParamsDetail = {
  layout_type: string,
  id: number
}

export type TUpdateStatus = {
  id: number,
  status: STATUS
}


export const getEvent = async (API_URL, token, params: TParams) => {
  let url = new URL(`${API_URL}management/events`)
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]))
  const prom = await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const getEvents = async (API_URL, params: TParams) => {
  let url = new URL(`${API_URL}events`)
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]))
  const prom = await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    },
    ...options
  })
  return prom.json()
}

export const getOneEvent = async (API_URL, token, params: TParamsDetail) => {
  let url = new URL(`${API_URL}management/events`)
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]))
  const prom = await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const postEvent = async (API_URL, token, body: TRequest) => {
  const prom = await fetch(`${API_URL}management/events`, {
    method: 'POST',
    body : JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const updateEvent = async (API_URL, token, body: TRequestUpdate) => {
  const prom = await fetch(`${API_URL}management/events`, {
    method: 'PUT',
    body : JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const deleteEvent = async (API_URL, token, id) => {
  const prom = await fetch(`${API_URL}management/events?id=${id}`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}