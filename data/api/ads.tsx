import * as base from './base'

export const getAds = (token: string) : Promise<any> => 
  base.get({
    url: 'ads?layout_type=list_layout&limit&page',
    method: 'GET',
    body: {},
    headers: base.getHeaders(token)
  })