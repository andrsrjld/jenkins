import fetch from 'node-fetch'
import * as base from './base'
import {options} from 'util/helper'

export enum STATUS {
  MENUNGGU_KONFIRMASI = 'Menunggu Konfirmasi',
  DIBAYAR = 'Pesanan Sudah Dibayar',
  DIPROSES = 'Pesanan Diproses',
  DIKIRIM = 'Pesanan Dikirim',
  TIBA = 'Pesanan Tiba',
  SELESAI = 'Pesanan Selesai',
  DIBATALKAN = 'Pesanan Dibatalkan'
}

export const getOrders = async (API_URL, token, status) : Promise<any> => {
  const prom = await fetch(`${API_URL}restricted/order?layout_type=list_layout&status=${status}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const getOrdersSeller = async (API_URL, token, status) : Promise<any> => {
  const prom = await fetch(`${API_URL}restricted/order/seller?layout_type=list_layout&status=${status}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const getOrdersAdmin = async (API_URL, token, sellerId, status) : Promise<any> => {
  const prom = await fetch(`${API_URL}management/order/order?layout_type=list_layout&seller_id=${sellerId}&status=${status}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

type TUpdateStatus = {
  id: string,
  status: STATUS,
  airway_bill: string
}
export const updateStatusOrder = async (API_URL, token, body: TUpdateStatus) : Promise<any> => {
  const prom = await fetch(`${API_URL}restricted/order/seller`, {
    method: 'PUT',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const updateStatusOrderAdmin = async (API_URL, token, body: TUpdateStatus) : Promise<any> => {
  const prom = await fetch(`${API_URL}management/order/seller`, {
    method: 'PUT',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const getOrderDetail = async (API_URL, token, id) : Promise<any> => {
  const prom = await fetch(`${API_URL}restricted/order?layout_type=detail_layout&id=${id}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const getOrderDetailManagement = async (API_URL, token, id) : Promise<any> => {
  const prom = await fetch(`${API_URL}management/order/order?layout_type=detail_layout&id=${id}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const getWayBill = async (API_URL, token, resi, courier) : Promise<any> => {
  const prom = await fetch(`${API_URL}restricted/rajaongkir?action=check_waybill`, {
    method: 'POST',
    body: JSON.stringify({
      no_resi: resi,
      courier: courier
    }),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const getServiceCharge = (token: string) : Promise<any> => 
  base.get({
    url: 'order/service_charge',
    method: 'GET',
    body: {},
    headers: base.getHeaders(token)
  })

export const updateStatusOrderUser = async (API_URL, token, body: any) : Promise<any> => {
  const prom = await fetch(`${API_URL}restricted/order/seller`, {
    method: 'PUT',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}