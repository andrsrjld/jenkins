import fetch from 'node-fetch'
import {options} from 'util/helper'

export const summaryLifetimeSales = async (API_URL, token) => {
  const prom = await fetch(`${API_URL}restricted/order/summary/lifetime_sales?layout_type=list_layout`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const summaryLifetimeSalesAdmin = async (API_URL, token) => {
  const prom = await fetch(`${API_URL}management/order/summary/lifetime_sales?layout_type=list_layout`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const summaryAverageOrders = async (API_URL, token) => {
  const prom = await fetch(`${API_URL}restricted/order/summary/average_sales?layout_type=list_layout`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const summaryAverageOrdersAdmin = async (API_URL, token) => {
  const prom = await fetch(`${API_URL}management/order/summary/average_sales?layout_type=list_layout`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const summaryProductOrders = async (API_URL, token) => {
  const prom = await fetch(`${API_URL}restricted/order/summary/product_sold?layout_type=list_layout`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const summaryProductOrdersAdmin = async (API_URL, token) => {
  const prom = await fetch(`${API_URL}management/order/summary/product_sold?layout_type=list_layout`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const summaryServiceFee = async (API_URL, token) => {
  const prom = await fetch(`${API_URL}management/order/summary/service_fee?layout_type=list_layout`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const summaryTopSalesProduct = async (API_URL, token) => {
  const prom = await fetch(`${API_URL}restricted/order/summary/top_sales?layout_type=list_layout`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const summaryTopSalesProductAdmin = async (API_URL, token) => {
  const prom = await fetch(`${API_URL}management/order/summary/top_sales?layout_type=list_layout`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const summaryTopQtyProduct = async (API_URL, token) => {
  const prom = await fetch(`${API_URL}restricted/order/summary/top_order?layout_type=list_layout`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const summaryTopQtyProductAdmin = async (API_URL, token) => {
  const prom = await fetch(`${API_URL}management/order/summary/top_order?layout_type=list_layout`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const graphSales = async (API_URL, token, startDate, endDate) => {
  const prom = await fetch(`${API_URL}restricted/order/graph/transaction?layout_type=list_layout&start_date=${startDate}&end_date=${endDate}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const graphPenjualan = async (API_URL, token, startDate, endDate) => {
  const prom = await fetch(`${API_URL}restricted/order/graph/order?layout_type=list_layout&start_date=${startDate}&end_date=${endDate}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const graphProduct = async (API_URL, token, startDate, endDate) => {
  const prom = await fetch(`${API_URL}restricted/order/graph/product?layout_type=list_layout&start_date=${startDate}&end_date=${endDate}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}


export const graphSalesAdmin = async (API_URL, token, startDate, endDate) => {
  const prom = await fetch(`${API_URL}management/order/graph/transaction?layout_type=list_layout&start_date=${startDate}&end_date=${endDate}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const graphPenjualanAdmin = async (API_URL, token, startDate, endDate) => {
  const prom = await fetch(`${API_URL}management/order/graph/order?layout_type=list_layout&start_date=${startDate}&end_date=${endDate}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const graphProductAdmin = async (API_URL, token, startDate, endDate) => {
  const prom = await fetch(`${API_URL}management/order/graph/product?layout_type=list_layout&start_date=${startDate}&end_date=${endDate}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const graphServiceFee = async (API_URL, token, startDate, endDate) => {
  const prom = await fetch(`${API_URL}management/order/graph/fee?layout_type=list_layout&start_date=${startDate}&end_date=${endDate}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}


export const summaryLastOrdersAdmin = async (API_URL, token) => {
  const prom = await fetch(`${API_URL}management/order/summary/last_order?layout_type=list_layout`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const summaryLastTransactionAdmin = async (API_URL, token) => {
  const prom = await fetch(`${API_URL}management/order/summary/last_transaction?layout_type=list_layout`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const summaryLastOrders = async (API_URL, token) => {
  const prom = await fetch(`${API_URL}restricted/order/summary/last_order?layout_type=list_layout`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const summaryLastTransaction = async (API_URL, token) => {
  const prom = await fetch(`${API_URL}restricted/order/summary/last_transaction?layout_type=list_layout`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}