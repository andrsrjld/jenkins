import * as base from './base'

export const getProductType = () : Promise<any> => 
  base.get({
    url: 'product_type?layout_type=list_layout',
    method: 'GET',
    body: {},
    headers: {}
  });