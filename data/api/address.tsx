import * as base from './base'

export const getAddresses = (token: string) : Promise<any> => 
  base.get({
    url: 'restricted/customer/address?layout_type=list_layout',
    method: 'GET',
    body: {},
    headers: base.getHeaders(token)
  })

export const insertAddress = (token: string, body: any) : Promise<any> => 
  base.api({
    url: 'restricted/customer/address',
    method: 'POST',
    body: body,
    headers: base.getHeaders(token)
  })

export const updateAddress = (token: string, body: any) : Promise<any> => 
  base.api({
    url: 'restricted/customer/address?action=update',
    method: 'PUT',
    body: body,
    headers: base.getHeaders(token)
  })

export const deleteAddress = (token: string, body: any) : Promise<any> => 
  base.api({
    url: 'restricted/customer/address',
    method: 'DELETE',
    body: body,
    headers: base.getHeaders(token)
  })

export const getAddress = (token: string, slug: string) : Promise<any> => 
  base.get({
    url: `restricted/customer/address?layout_type=detail_layout&slug=${slug}`,
    method: 'GET',
    body: {},
    headers: base.getHeaders(token)
  })
