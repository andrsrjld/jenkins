import fetch from 'node-fetch'
import {options} from 'util/helper'

export const getVariants = async (API_URL, slug) => {
  let url = new URL(`${API_URL}variant?layout_type=list_layout&slug=${slug}`)
  const prom = await fetch(url, {
    method: 'GET',
    headers : {
      'Content-Type': 'application/json'
    },
    ...options
  })
  return prom.json()
}