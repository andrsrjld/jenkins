import fetch from 'node-fetch'
import {options} from 'util/helper'

export enum STATUS {
  ALL = 'all',
  DRAFT = 'Draft',
  APPROVED = 'Approved'
}

export enum SORTING {
  ASC = 'asc',
  DESC = 'desc'
}

export type TRequest = {
  title: string,
  description: string,
  link: string,
  thumbnail: string,
  action: string,
  show: boolean
}

export type TRequestUpdate = {
  id: number,
  title: string,
  description: string,
  link: string,
  thumbnail: string,
  action: string,
  show: boolean
}

export type TParams = {
  layout_type: string,
  page: number,
  limit: number
}

export type TParamsDetail = {
  layout_type: string,
  id: number
}

export type TUpdateStatus = {
  id: number,
  status: STATUS
}


export const getIklan = async (API_URL, token, params: TParams) => {
  let url = new URL(`${API_URL}management/crm/ads`)
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]))
  const prom = await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const getOneIklan = async (API_URL, token, params: TParamsDetail) => {
  let url = new URL(`${API_URL}management/crm/ads`)
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]))
  const prom = await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const postIklan = async (API_URL, token, body: TRequest) => {
  const prom = await fetch(`${API_URL}management/crm/ads`, {
    method: 'POST',
    body : JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const updateIklan = async (API_URL, token, body: TRequestUpdate) => {
  const prom = await fetch(`${API_URL}management/crm/ads`, {
    method: 'PUT',
    body : JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const deleteIklan = async (API_URL, token, id) => {
  const prom = await fetch(`${API_URL}management/crm/ads?id=${id}`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}