import * as base from './base'

export const getCart = (token: string) : Promise<any> => 
  base.get({
    url: 'restricted/customer/cart?layout_type=list_layout',
    method: 'GET',
    body: {},
    headers: base.getHeaders(token)
  })

export const deleteCart = (token: string, userId: string, productId: string) : Promise<any> => 
  base.api({
    url: `restricted/customer/cart?user_id=${userId}&variant_id=${productId}`,
    method: 'DELETE',
    body: {
      user_id: userId,
      variant_id: productId
    },
    headers: base.getHeaders(token)
  })

export const updateCart = (token: string, body: any) : Promise<any> => 
  base.api({
    url: 'restricted/customer/cart',
    method: 'PUT',
    body: body,
    headers: base.getHeaders(token)
  })

export const updateCartNote = (token: string, body: any) : Promise<any> => 
  base.api({
    url: 'restricted/customer/cart/notes',
    method: 'PUT',
    body: body,
    headers: base.getHeaders(token)
  })

export const updateCartCheckout = (token: string, body: any) : Promise<any> => 
  base.api({
    url: 'restricted/customer/cart/checkout',
    method: 'PUT',
    body: body,
    headers: base.getHeaders(token)
  })
