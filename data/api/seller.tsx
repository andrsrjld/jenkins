import { AnyARecord } from 'dns'
import fetch from 'node-fetch'
import {options} from 'util/helper'

export enum ENUM_SELLER_STATUS {
  APPROVED = 'Approved',
  PENDING = 'Pending'
}

export type TRequest = {
  store_name: string,
  photo: string,
  banner_type: string,
  url_banner: string,
  about_us: string,
  phone: string,
  operation_hours: string,
  auto_chat: string
}

export const updateAccount = async (API_URL, token, request: TRequest) : Promise<any> => {
  const prom = await fetch(`${API_URL}restricted/seller/account`, {
    method: 'PUT',
    body : JSON.stringify(request),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export type TRequestApprove = {
  status: ENUM_SELLER_STATUS,
  slug: string
}

export const approveSeller = async (API_URL, token, request: TRequestApprove) : Promise<any> => {
  const prom = await fetch(`${API_URL}management/seller/account`, {
    method: 'PUT',
    body : JSON.stringify(request),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export type TRequestAddress = {
  id: number,
  district_id: number,
  address: string,
  postal_code: string,
  lat_long: string
}
export const updateAddressAccount = async (API_URL, token, request: TRequestAddress) : Promise<any> => {
  const prom = await fetch(`${API_URL}restricted/seller/address`, {
    method: 'PUT',
    body : JSON.stringify(request),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}


import * as base from './base'

export const getSellers = (token: string, order: string, page: string, limit: string, status: any, keyword?: any) : Promise<any> => 
  base.get({
    url: `seller/account?layout_type=list_layout${status ? '&status=' + status : '&status=Approved' }&order=${order}&page=${page}&limit=${limit}${keyword ? '&keyword=' + keyword : ''}`,
    method: 'GET',
    body: {},
    headers: base.getHeaders(token)
  });

export const getSellerDetail = (token: string, slug: string) : Promise<any> => 
  base.get({
    url: `seller/account?layout_type=detail_layout&slug=${slug}`,
    method: 'GET',
    body: {},
    headers: base.getHeaders(token)
  });
