import fetch from 'node-fetch'
import {options} from 'util/helper'

export const requestOTP = async (API_URL, phone: string, email: string, name: string) : Promise<any> => {
  const prom = await fetch(`${API_URL}auth/otp/request`, {
    method: 'POST',
    body : JSON.stringify({
      phone_number: phone,
      email: email,
      name: name
    }),
    headers: {
      'Content-Type': 'application/json'
    },
    ...options
  })
  return prom.json()
}

export const validationOTP = async (API_URL, id: string, code: string) : Promise<any> => {
  const prom = await fetch(`${API_URL}auth/otp/validation`, {
    method: 'POST',
    body : JSON.stringify({
      request_id: id,
      otpm_code: code
    }),
    headers: {
      'Content-Type': 'application/json'
    },
    ...options
  })
  return prom.json()
}