import fetch from 'node-fetch'
import {options} from 'util/helper'

export enum STATUS {
  ALL = 'all',
  PUBLISHED = 'published',
  NOT_PUBLISHED = 'not-published'
}

export type TParams = {
  layout_type: string
}

export type TRequestInsert = {
  title: string,
  description: string,
  image_url: string,
  link: string,
  publish_date: string,
  remove_date: string
}

export type TRequestUpdate = {
  id: number,
  title: string,
  description: string,
  image_url: string,
  link: string,
  publish_date: string,
  remove_date: string
}

export const getPages = async (API_URL, token, params: TParams) => {
  let url = new URL(`${API_URL}pages`)
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]))
  const prom = await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const getOnePage = async (API_URL, slug) => {
  let url = new URL(`${API_URL}pages?layout_type=detail_layout&slug=${slug}`)
  const prom = await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    },
    ...options
  })
  return prom.json()
}

export const postPage = async (API_URL, token, body: any) => {
  let url = new URL(`${API_URL}management/pages`)
  const prom = await fetch(url, {
    method: 'POST',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const updateBanner = async (API_URL, token, body: TRequestUpdate) => {
  let url = new URL(`${API_URL}banner`)
  const prom = await fetch(url, {
    method: 'PUT',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const deletePage = async (API_URL, token, slug) => {
  let url = new URL(`${API_URL}management/pages?slug=${slug}`)
  const prom = await fetch(url, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const updatePage = async (API_URL, token, body: any) => {
  let url = new URL(`${API_URL}management/pages`)
  const prom = await fetch(url, {
    method: 'PUT',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const addPageProduct = async (API_URL, token, body: any) => {
  let url = new URL(`${API_URL}management/pages/product`)
  const prom = await fetch(url, {
    method: 'POST',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const deletePageProduct = async (API_URL, token, body: any) => {
  let url = new URL(`${API_URL}management/pages/product`)
  const prom = await fetch(url, {
    method: 'DELETE',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const addPageBanner = async (API_URL, token, body: any) => {
  let url = new URL(`${API_URL}management/pages/images`)
  const prom = await fetch(url, {
    method: 'POST',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const updatePageBanner = async (API_URL, token, body: any) => {
  let url = new URL(`${API_URL}management/pages/images`)
  const prom = await fetch(url, {
    method: 'PUT',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const deletePageBanner = async (API_URL, token, body: any) => {
  let url = new URL(`${API_URL}management/pages/images`)
  const prom = await fetch(url, {
    method: 'DELETE',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

