import fetch from 'node-fetch'
import {options} from 'util/helper'

export enum STATUS {
  ALL = 'all',
  PUBLISHED = 'published',
  NOT_PUBLISHED = 'not-published'
}

export type TParams = {
  layout_type: string
}

export type TRequestInsert = {
	role_id: number,
	name: string,
	username: string,
	password: string,
	phone: string,
	location: string
}

export type TRequestUpdate = {
	id: number,
	role_id: number,
	name: string,
	username: string,
	phone: string,
	location: string
}

export const getPegawai = async (API_URL, token) => {
  let url = new URL(`${API_URL}management/employee?layout_type=list_layout&id=0`)
  const prom = await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const getOnePegawai = async (API_URL, token, id) => {
  let url = new URL(`${API_URL}management/employee?layout_type=detail_layout&id=${id}`)
  const prom = await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const postPegawai = async (API_URL, token, body: TRequestInsert) => {
  let url = new URL(`${API_URL}management/employee`)
  const prom = await fetch(url, {
    method: 'POST',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const updatePegawai = async (API_URL, token, body: TRequestUpdate) => {
  let url = new URL(`${API_URL}management/employee`)
  const prom = await fetch(url, {
    method: 'PUT',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const deletePegawai   = async (API_URL, token, id) => {
  let url = new URL(`${API_URL}management/employee`)
  const prom = await fetch(url, {
    method: 'DELETE',
    body: JSON.stringify({
      id: id
    }),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}
