import * as base from './base'

export const getWishlists = (token: string, order:string, page:string, limit:string) : Promise<any> => 
  base.get({
    url: `restricted/customer/wishlist?layout_type=list_layout&order=product_name,ASC&page=1&limit=10`,
    method: 'GET',
    body: {},
    headers: base.getHeaders(token)
  });