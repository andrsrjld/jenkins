import fetch from 'node-fetch'
import {options} from 'util/helper'

export enum ORDERTYPE {
  PRODUCT_NAME = 'product_name',
  PRICE = 'price',
  DATE = 'date'
}

export enum ORDER {
  ASC = 'ASC',
  DESC = 'DESC'
}

export enum STATE {
  ALL = 'All',
  DRAFT = 'Draft',
  ACTIVE = 'Active',
  NonActive = 'Non-Active'
}

export type TParams = {
  layout_type: string,
  seller_slug: string,
  type_slug: string,
  keyword: string,
  status: string, 
  origin: string,
  species: string,
  tasted: string,
  roast_level: string,
  price: string, // MINIMUMPRICE,MAXPRICE
  page: number,
  limit: number,
  order: string // ORDERTYPE,ORDER
}

export type TDelete = {
  id: string
}

export type TUpdateStatus = {
  id: string,
  status: STATE
}

export const postProduct = async (API_URL, body, token) => {
  const prom = await fetch(`${API_URL}restricted/product/product`, {
    method: 'POST',
    body : JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const postProductAdmin = async (API_URL, body, token) => {
  const prom = await fetch(`${API_URL}management/product/product`, {
    method: 'POST',
    body : JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const getProducts = async (API_URL, params: TParams, token) => {
  let url = new URL(`${API_URL}products`)
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]))
  let headers = null
  if(token) {
    headers = {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    }
  } else {
    headers = {
      'Content-Type': 'application/json'
    }
  }
  const prom = await fetch(url, {
    method: 'GET',
    params: params,
    headers: headers,
    ...options
  })
  return prom.json()
}

export const getOne = async (API_URL, slug, token) => {
  let url = new URL(`${API_URL}products?layout_type=detail_layout&slug=${slug}`)
  const prom = await fetch(url, {
    method: 'GET',
    params: url,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const deleteProduct = async (API_URL, body: TDelete, token) => {
  const prom = await fetch(`${API_URL}restricted/product/product`, {
    method: 'DELETE',
    body : JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const updateStatusProduct = async (API_URL, body: TUpdateStatus, token) => {
  const prom = await fetch(`${API_URL}restricted/product/product?action=change_status`, {
    method: 'PUT',
    body : JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const updateStatusProductManagement = async (API_URL, body: TUpdateStatus, token) => {
  const prom = await fetch(`${API_URL}management/product`, {
    method: 'PUT',
    body : JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export type TUpdateProduct = {
  id: string,
  name: string,
  price: number,
  description: string,
  short_description: string,
  discount_from: string,
  discount_to: string,
  discount_amount: number,
  is_po: any,
  po_duration: any
}

export const updateProduct = async (API_URL, body: TUpdateProduct, token) => {
  const prom = await fetch(`${API_URL}restricted/product/product?action=update`, {
    method: 'PUT',
    body : JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export type TUpdateProductVariant = {
  id: number,
  product_id: number,
  sku: string,
  name: string,
  net_weight: number,
  gross_weight: number,
  quantity: number,
  price: number,
  discount_amount: number,
  discount_from: string,
  discount_to: string,
  details: any
}

export const updateProductVariant = async (API_URL, body: TUpdateProductVariant, token) => {
  const prom = await fetch(`${API_URL}restricted/product/variant`, {
    method: 'PUT',
    body : JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export type TAddProductVariant = {
  product_id: number,
  sku: string,
  name: string,
  net_weight: number,
  gross_weight: number,
  quantity: number,
  price: number,
  discount_amount: number,
  discount_from: string,
  discount_to: string,
  details: any
}

export const addProductVariant = async (API_URL, body: TAddProductVariant, token) => {
  const prom = await fetch(`${API_URL}restricted/product/variant`, {
    method: 'POST',
    body : JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const updateProductInformation = async (API_URL, body: any, token) => {
  const prom = await fetch(`${API_URL}restricted/product/information`, {
    method: 'PUT',
    body : JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const deleteProductImage = async (API_URL, token, id: number) => {
  const prom = await fetch(`${API_URL}restricted/product/image`, {
    method: 'DELETE',
    body : JSON.stringify({id}),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const insertProductImage = async (API_URL, token, product_id: number, image_url: string) => {
  const prom = await fetch(`${API_URL}restricted/product/image`, {
    method: 'POST',
    body : JSON.stringify({product_id, image_url}),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}



