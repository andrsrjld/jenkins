import fetch from 'node-fetch'

const https = require("https");

type TAPI = {
  url: string,
  method: string,
  body: object,
  headers: object
}

const options = {
  // agent: new https.Agent({
  //   rejectUnauthorized: false
  // })
};

export const get = async ({url, method, body, headers}: TAPI) => {
  const prom = await fetch(`${`http://156.0.100.89:5050/api/v1/`}${url}`, {
    method,
    headers,
    ...options
  })

  return prom.json()
}

export const api = async ({url, method, body, headers}: TAPI) => {
  const prom = await fetch(`${`http://156.0.100.89:5050/api/v1/`}${url}`, {
    method,
    body : JSON.stringify(body), 
    headers,
    ...options
  })

  return prom.json()
}

export const getHeaders = (token : string) => ({
  'Content-Type': 'application/json',
  'Authorization': `Bearer ${token}`
})

