import * as base from './base'

export const getPaymentMethods = (token: string) : Promise<any> => 
  base.get({
    url: 'restricted/payment/method?layout_type=list_layout&type_id=0',
    method: 'GET',
    body: {},
    headers: base.getHeaders(token)
  })

export const postPayment = (token: string, body: any) : Promise<any> => 
  base.api({
    url: 'restricted/order',
    method: 'POST',
    body: body,
    headers: base.getHeaders(token)
  })

export const getPaymentTypes = (token: string) : Promise<any> => 
  base.get({
    url: 'restricted/payment/type?layout_type=list_layout',
    method: 'GET',
    body: {},
    headers: base.getHeaders(token)
  })

  export const getTransactionStatus = (token: string, transactionId: string, paymentType: string) : Promise<any> => 
  base.get({
    url: `restricted/payment/status?transaction_id=${transactionId}&payment_type=${paymentType}`,
    method: 'GET',
    body: {},
    headers: base.getHeaders(token)
  })
