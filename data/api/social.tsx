import fetch from 'node-fetch'
import {options} from 'util/helper'

export const validateSocial = async (API_URL, email) => {
  const prom = await fetch(`${API_URL}auth/social/validation`, {
    method: 'POST',
    body : JSON.stringify({
      email: email
    }),
    headers: {
      'Content-Type': 'application/json'
    },
    ...options
  })
  return prom.json()
}

export const loginSocial = async (API_URL, email, token) => {
  const prom = await fetch(`${API_URL}auth/social/login`, {
    method: 'POST',
    body : JSON.stringify({
      email: email,
      socmed_token : token
    }),
    headers: {
      'Content-Type': 'application/json'
    },
    ...options
  })
  return prom.json()
}