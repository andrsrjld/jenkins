import fetch from 'node-fetch'
import {options} from 'util/helper'

export enum STATUS {
  ALL = 'all',
  DRAFT = 'Draft',
  APPROVED = 'Approved'
}

export enum SORTING {
  ASC = 'asc',
  DESC = 'desc'
}

export enum SORT_BY {
  VIEW_COUNT = 'view_count',
  TITLE = 'title',
  CREATED_BY = 'created_by'
}

export type TRequest = {
  title: string,
  highlight: string,
  content: string,
  image_url: string,
  author_name: string,
  author_image: string
}

export type TParams = {
  layout_type: string,
  status: STATUS,
  sort_by: SORT_BY,
  sort_type: SORTING,
  page: number,
  limit: number
}

export type TUpdateStatus = {
  id: number,
  status: STATUS
}


export const getArticlesManagement = async (API_URL, token, params: TParams) => {
  let url = new URL(`${API_URL}article`)
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]))
  const prom = await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const postArticle = async (API_URL, token, body) => {
  const prom = await fetch(`${API_URL}management/article`, {
    method: 'POST',
    body : JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const updateArticle = async (API_URL, token, body) => {
  const prom = await fetch(`${API_URL}management/article?action=update`, {
    method: 'PUT',
    body : JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const updateStatusArticle = async (API_URL, token, body) => {
  const prom = await fetch(`${API_URL}management/article?action=change_status`, {
    method: 'PUT',
    body : JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const deleteArticle = async (API_URL, token, id) => {
  const prom = await fetch(`${API_URL}management/article`, {
    method: 'DELETE',
    body : JSON.stringify({id}),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

import * as base from './base'

export const getArticles = (token: string, sortBy: string, sort: SORT_BY, page: string, limit: string) : Promise<any> => 
  base.get({
    url: `article?layout_type=list_layout&status=approved&sort_type=${sort}&sort_by=${sortBy}&page=${page}&limit=${limit}`,
    method: 'GET',
    body: {},
    headers: base.getHeaders(token)
  });

export const getArticleDetail = (token: string, slug: string) : Promise<any> => 
  base.get({
    url: `article?layout_type=detail_layout&slug=${slug}`,
    method: 'GET',
    body: {},
    headers: base.getHeaders(token)
  });
