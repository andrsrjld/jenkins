import fetch from 'node-fetch'
import {options} from 'util/helper'

export const reportTransaction = async (API_URL, token, sellerId) : Promise<any> => {
  const prom = await fetch(`${API_URL}management/order/report/transaction?layout_type=list_layout&seller_id=${sellerId}&limit=500&start_date=2021-01-01&end_date=2021-12-30`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization' : `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const reportTransactionSeller = async (API_URL, token, sellerId) : Promise<any> => {
  const prom = await fetch(`${API_URL}management/order/report/transaction/seller?layout_type=list_layout&seller_id=${sellerId}&limit=500&start_date=2021-01-01&end_date=2021-12-30`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization' : `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const reportTransactionForSeller = async (API_URL, token, limit, page) : Promise<any> => {
  const prom = await fetch(`${API_URL}restricted/report/transaction?layout_type=list_layout&limit=${limit}&page=${page}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization' : `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const reportTransactionSellerForSeller = async (API_URL, token) : Promise<any> => {
  const prom = await fetch(`${API_URL}restricted/report/transaction/seller?layout_type=list_layout&limit=100`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization' : `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}