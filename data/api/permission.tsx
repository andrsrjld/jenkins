import fetch from 'node-fetch'
import {options} from 'util/helper'

export type TParams = {
  layout_type: string
}

export type TRequest = {
  role_id: number,
  feature_id: string
}

export const getPermission = async (API_URL, token, roleId) => {
  let url = new URL(`${API_URL}management/acl?id=${roleId}`)
  const prom = await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const postPermission = async (API_URL, token, body: TRequest) => {
  let url = new URL(`${API_URL}management/acl`)
  const prom = await fetch(url, {
    method: 'POST',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const deletePermission = async (API_URL, token, body: TRequest) => {
  let url = new URL(`${API_URL}management/acl`)
  const prom = await fetch(url, {
    method: 'DELETE',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}
