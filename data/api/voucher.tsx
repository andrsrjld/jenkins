import fetch from 'node-fetch'
import { options } from 'util/helper'

export enum STATUS {
  ALL = 'all',
  DRAFT = 'Draft',
  APPROVED = 'Approved'
}

export enum SORTING {
  ASC = 'asc',
  DESC = 'desc'
}

export enum VOUCHER_TYPE {
  TRANSACTION = '1',
  SHIPPING = '2'
}

export enum ENUM_VOUCHER_TYPE {
  NOMINAL = '1',
  PERSENTASE = '2'
}

export type TRequest = {
  name: string,
  code: string,
  enum_discount_type: ENUM_VOUCHER_TYPE,
  voucher_type: VOUCHER_TYPE,
  discount: number,
  minimum_spend: number,
  max_discount: number,
  payment_method?: string,
  use_limit: number,
  start_date: string,
  end_date: string,
  notes: string
}

export type TRequestUpdate = {
  id: number,
  name: string,
  code: string,
  enum_discount_type: ENUM_VOUCHER_TYPE,
  voucher_type: VOUCHER_TYPE,
  discount: number,
  minimum_spend: number,
  max_discount: number,
  payment_method?: string,
  use_limit: number,
  start_date: string,
  end_date: string,
  notes: string
}

export type TParams = {
  layout_type: string,
  page: number,
  limit: number
}

export type TParamsDetail = {
  layout_type: string,
  id: number
}

export type TUpdateStatus = {
  id: number,
  status: STATUS
}


export const getVoucher = async (API_URL, token, params: TParams) => {
  let url = new URL(`${API_URL}management/crm/voucher`)
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]))
  const prom = await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const getAvailableVouchers = (token: string): Promise<any> =>
  base.get({
    url: 'voucher?layout_type=list_layout',
    method: 'GET',
    body: {},
    headers: base.getHeaders(token)
  })

export const getVoucherDetail = async (API_URL, token, params: TParamsDetail) => {
  let url = new URL(`${API_URL}voucher`)
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]))
  const prom = await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const postVoucher = async (API_URL, token, body: TRequest) => {
  const prom = await fetch(`${API_URL}management/crm/voucher`, {
    method: 'POST',
    body : JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const updateVoucher = async (API_URL, token, body: TRequestUpdate) => {
  const prom = await fetch(`${API_URL}management/crm/voucher`, {
    method: 'PUT',
    body : JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}

export const deleteVoucher = async (API_URL, token, id) => {
  const prom = await fetch(`${API_URL}management/crm/voucher?id=${id}`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    ...options
  })
  return prom.json()
}
import * as base from './base'

export const getVouchers = (token: string) : Promise<any> => 
  base.get({
    url: 'voucher?layout_type=list_layout',
    method: 'GET',
    body: {},
    headers: base.getHeaders(token)
  })

export const getVoucherById = (token: string, id: string) : Promise<any> => 
  base.get({
    url: `voucher?layout_type=detail_layout&id=${id}`,
    method: 'GET',
    body: {},
    headers: base.getHeaders(token)
  })

export const getVoucherByCode = (token: string, code: string) : Promise<any> => 
  base.get({
    url: `voucher/code?layout_type=detail_layout&code=${code}`,
    method: 'GET',
    body: {},
    headers: base.getHeaders(token)
  })

export const postValidateVoucher = (token: string, body: any) : Promise<any> => 
  base.api({
      url: 'restricted/crm/voucher/validate',
      method: 'POST',
      body: body,
      headers: base.getHeaders(token)
  })
