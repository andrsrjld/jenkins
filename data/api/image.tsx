import fetch from 'node-fetch'
import { TPostImage } from '../models/image'
import {options} from 'util/helper'

export const uploadImage = async (CLOUDINARY_URL, body: TPostImage) => {
  let formData = new FormData();
  formData.append("image_file", body.file)
  formData.append("folder", body.folder)
  formData.append("tag",body.tag)

  const prom = await fetch(`${CLOUDINARY_URL}upload`, {
    method: 'POST',
    body : formData,
    ...options
  })

  return prom.json()
}