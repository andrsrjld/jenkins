export type TPostImage = {
  file: Blob,
  folder: string,
  tag: string
}